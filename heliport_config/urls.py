# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers

from heliport.archive.urls import router as archive_router
from heliport.core.urls import router as core_router
from heliport.cwl_execution.urls import router as cwl_router
from heliport.data_management_plan.urls import router as dmp_router
from heliport.data_source.urls import router as data_source_router
from heliport.digital_objects.urls import router as digital_objects_router
from heliport.documentation.urls import router as documentation_router
from heliport.publication.urls import router as publication_router
from heliport.sharelatex.urls import router as sharelatex_router
from heliport.smb_files.urls import router as smb_router
from heliport.ssh.urls import router as ssh_router
from heliport.version_control.urls import router as version_control_router

urlpatterns = [
    # django.contrib apps
    path("admin/", admin.site.urls),
    # Third-party apps
    path("__debug__/", include("debug_toolbar.urls")),
    path("accounts/", include("allauth.urls")),
    path("health/", include("health_check.urls")),
    path("qr-code/", include("qr_code.urls", namespace="qr_code")),
    # heliport apps
    path("", include("heliport.core.urls")),
    path("", include("heliport.archive.urls")),
    path("", include("heliport.cwl_execution.urls")),
    path("", include("heliport.data_management_plan.urls")),
    path("", include("heliport.data_source.urls")),
    path("", include("heliport.digital_objects.urls")),
    path("", include("heliport.documentation.urls")),
    path("", include("heliport.publication.urls")),
    path("", include("heliport.sharelatex.urls")),
    path("", include("heliport.smb_files.urls")),
    path("", include("heliport.ssh.urls")),
    path("", include("heliport.tables.urls")),
    path("", include("heliport.version_control.urls")),
]

api_routers = [
    core_router,
    archive_router,
    cwl_router,
    dmp_router,
    data_source_router,
    digital_objects_router,
    documentation_router,
    publication_router,
    sharelatex_router,
    smb_router,
    ssh_router,
    version_control_router,
]

if settings.HELIPORT_GATE_ENABLED:
    from heliport.gate_connection.urls import router as gate_connection_router

    urlpatterns.append(path("", include("heliport.gate_connection.urls")))
    api_routers.append(gate_connection_router)

if settings.HELIPORT_UNICORE_ENABLED:
    urlpatterns.append(path("", include("heliport.unicore.urls")))

router = routers.DefaultRouter()
for api_router in api_routers:
    router.registry.extend(api_router.registry)

urlpatterns.append(path("api/", include(router.urls)))
