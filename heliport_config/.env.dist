# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Maintenance. Tuple of message level and message.
# 0 no message
# 1 secondary-gray | 2 success-green  | 3 info-lightblue
# 4 primary-blue   | 5 warning-yellow | 6 error-red
#HELIPORT_CORE_MAINTENANCE_MESSAGE_LEVEL=0
#HELIPORT_CORE_MAINTENANCE_MESSAGE=No maintenance today

# Run HELIPORT in development mode. This setting has security implications and should
# only be set to true for development!
#DEVELOPMENT=False

# Allowed hosts. REQUIRED if not running on localhost!
# Comma-separated list of hostnames. 127.0.0.1 and localhost are added automatically.
# Example: heliport.my-research-center.org,heliport.some-institution.org
#ALLOWED_HOSTS=

# Secret key for cryptographic signing. REQUIRED!
# May be generated using `pwgen -c -n -y -s 32 1` or similar commands.
#SECRET_KEY=

# The base URL under which HELIPORT is running. REQUIRED!
# This needs to include protocol, domain, and port, but not the prefix!
# For a HELIPORT instance running under https://heliport.example.com/app/, this would
# be: https://heliport.example.com
#HELIPORT_CORE_HOST=

# Password for encryption of credentials. REQUIRED!
# This key has a specific format. To generate it run the following command in the
# Poetry environment:
#   python -c 'from cryptography import fernet; print(fernet.Fernet.generate_key().decode())'
#HELIPORT_CORE_CRYPTO_KEY=

# Database connection string.
# Typically in the form of `engine://USER:PASSWORD@HOST:PORT/DATABASE`.
# Example: postgresql://me:mypass@mydomain.org:5432/heliport
# Other allowed database engines are listed here:
# https://django-environ.readthedocs.io/en/latest/types.html
#DB_URL=sqlite:////var/lib/heliport/heliport.sqlite3

# Email settings.
# If enabled, HELIPORT sends email notifications.
#EMAIL_ENABLED=False
#EMAIL_HOST=localhost
#EMAIL_PORT=25
#EMAIL_USE_SSL=False
#EMAIL_USER=
#EMAIL_PASSWORD=
#EMAIL_FROM_ADDRESS=noreply@heliport

# Allow local account signup. Local accounts are stored in HELIPORT only and not backed
# by any authentication service. Setting this to `False` prohibits creation of new local
# accounts; existing local accounts will still be allowed to login.
#LOCAL_ACCOUNT_SIGNUP=True

# LDAP authentication.
# If enabled, the settings beginning with LDAP_ are required except for
# LDAP_*_OBJECT_CLASS and LDAP_GROUP_FILTER.
# LDAP_LOGIN_NAME_SEARCH must contain the placeholder `$(user)s`.
# Example: (uid=%(user)s)
# Setting `LDAP_SIGNUP=False` prohibits creation of HELIPORT accounts for unknown LDAP
# users. Users who have logged into HELIPORT before will still be allowed to login.
#LDAP_ENABLED=False
#LDAP_SIGNUP=True
#LDAP_SERVER_URL=
#LDAP_BIND_USER_DN=
#LDAP_BIND_PASSWORD=
#LDAP_LOGIN_NAME_SEARCH=
#LDAP_USER_BASE_DN=
#LDAP_USER_ID_ATTRIBUTE=
#LDAP_USER_DISPLAY_NAME_ATTRIBUTE=
#LDAP_USER_ATTRIBUTE_MAP=first_name=givenName,last_name=sn,email=mail
#LDAP_USER_OBJECT_CLASS=person
#LDAP_GROUP_BASE_DN=
#LDAP_GROUP_ID_ATTRIBUTE=
#LDAP_GROUP_DISPLAY_NAME_ATTRIBUTE=
#LDAP_GROUP_MEMBER_ATTRIBUTE=
#LDAP_GROUP_FILTER=
#LDAP_GROUP_OBJECT_CLASS=groupOfNames

# OIDC authentication.
# If enabled, OIDC_CLIENT_ID and OIDC_SECRET must be provided.
# Setting `OIDC_SIGNUP=False` prohibits creation of HELIPORT accounts for unknown OIDC
# users. Users who have logged into HELIPORT before will still be allowed to login.
#OIDC_ENABLED=False
#OIDC_SIGNUP=True
#OIDC_CLIENT_ID=
#OIDC_SECRET=

# Celery settings. REQUIRED!
# In order to use Celery, you need to set up a broker. More information can be found here:
# - https://docs.celeryq.dev/en/stable/django/first-steps-with-django.html
# - https://docs.celeryq.dev/en/stable/getting-started/backends-and-brokers/
# The broker URL is required. If no backend URL is given or it is set to django-db, the HELIPORT
# database will be used.
#CELERY_BROKER_URL=amqp://...
#CELERY_BACKEND_URL=django-db

# CORS settings
# Comma-separated list of hosts (with scheme).
# Example: https://hdl.example.com,https://proxy.example.com
#CORS_ALLOWED_ORIGINS=

# Handle server settings.
#HELIPORT_HANDLE_REGISTER_HANDLES=False
#HELIPORT_HANDLE_PREFIX=20.500.12865
#HELIPORT_HANDLE_SERVER_URL=https://handle.hzdr.de
#HELIPORT_HANDLE_CERTIFICATE_PATH=/etc/ssl/certs/heliport_handle_admin.crt
#HELIPORT_HANDLE_PRIVATE_KEY_PATH=/etc/ssl/private/heliport_handle_admin.key

# The time zone in which your HELIPORT instance runs.
# A list of allowed time zone names can be found here:
# https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
#TIME_ZONE=Europe/Berlin

# Use Gate proposal system.
# If True, HELIPORT_GATE_USER and HELIPORT_GATE_PASS are required, and LDAP must be
# configured.
#HELIPORT_GATE_ENABLED=False
#HELIPORT_GATE_USER=
#HELIPORT_GATE_PASS=
