# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
WSGI config for heliport project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see :doc:`django:howto/deployment/asgi/index`.
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "heliport_config.settings")

application = get_wsgi_application()
