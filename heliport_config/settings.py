# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""The overall configuration of the HELIPORT Django application."""

from pathlib import Path

import environ
from celery.schedules import crontab
from django.contrib import messages
from django.core.exceptions import ImproperlyConfigured

BASE_DIR = Path(__file__).resolve().parent.parent

env = environ.Env()
env.read_env()

# Run HELIPORT in development mode. This setting has security implications and should
# only be set to true for development!
DEVELOPMENT = env.bool("DEVELOPMENT", default=False)


# =============================== USERS, AUTHENTICATION ===============================

LOGIN_URL = "account_login"
LOGIN_REDIRECT_URL = "core:project_list"
LOGOUT_REDIRECT_URL = "account_login"

LOCAL_ACCOUNT_SIGNUP = env.bool("LOCAL_ACCOUNT_SIGNUP", default=True)

LDAP_ENABLED = env.bool("LDAP_ENABLED", default=False)
LDAP_SIGNUP = LDAP_ENABLED and env.bool("LDAP_SIGNUP", default=True)

OIDC_ENABLED = env.bool("OIDC_ENABLED", default=False)
OIDC_SIGNUP = OIDC_ENABLED and env.bool("OIDC_SIGNUP", default=True)

# Only allow accounts with at least one of the following entitlements to log in. An
# empty list or None means no restrictions.
# TODO: Allow configuration of this setting via .env file once this is released in
# django-environ 0.11.3: https://github.com/joke2k/django-environ/pull/500
OIDC_EDUPERSON_ENTITLEMENTS = [
    "urn:geant:helmholtz.de:group:Helmholtz-member#login.helmholtz.de",
    "urn:geant:helmholtz.de:res:HELIPORT#login.helmholtz.de",
]

# User sync in heliport.core
if LDAP_ENABLED:
    LDAP_SERVER_URL = env.str("LDAP_SERVER_URL")
    LDAP_BIND_USER_DN = env.str("LDAP_BIND_USER_DN")
    LDAP_BIND_PASSWORD = env.str("LDAP_BIND_PASSWORD")
    LDAP_LOGIN_NAME_SEARCH = env.str("LDAP_LOGIN_NAME_SEARCH")
    if "%(user)s" not in LDAP_LOGIN_NAME_SEARCH:
        raise ImproperlyConfigured(
            "LDAP_LOGIN_NAME_SEARCH must contain the placeholder `$(user)s`."
        )
    LDAP_USER_BASE_DN = env.str("LDAP_USER_BASE_DN")
    LDAP_USER_ID_ATTRIBUTE = env.str("LDAP_USER_ID_ATTRIBUTE")
    LDAP_USER_DISPLAY_NAME_ATTRIBUTE = env.str("LDAP_USER_DISPLAY_NAME_ATTRIBUTE")
    LDAP_USER_ATTRIBUTE_MAP = env.dict(
        "LDAP_USER_ATTRIBUTE_MAP",
        default={
            "first_name": "givenName",
            "last_name": "sn",
            "email": "mail",
        },
    )
    LDAP_USER_OBJECT_CLASS = env.str("LDAP_USER_OBJECT_CLASS", default="person")
    LDAP_GROUP_BASE_DN = env.str("LDAP_GROUP_BASE_DN")
    LDAP_GROUP_ID_ATTRIBUTE = env.str("LDAP_GROUP_ID_ATTRIBUTE")
    LDAP_GROUP_DISPLAY_NAME_ATTRIBUTE = env.str("LDAP_GROUP_DISPLAY_NAME_ATTRIBUTE")
    LDAP_GROUP_MEMBER_ATTRIBUTE = env.str("LDAP_GROUP_MEMBER_ATTRIBUTE")
    LDAP_GROUP_FILTER = env.str("LDAP_GROUP_FILTER", default=None)
    LDAP_GROUP_OBJECT_CLASS = env.str("LDAP_GROUP_OBJECT_CLASS", default="groupOfNames")

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": (
            "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
        ),
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
]

if LDAP_ENABLED:
    AUTHENTICATION_BACKENDS.insert(0, "django_auth_ldap.backend.LDAPBackend")

# Settings for django-auth-ldap
if LDAP_ENABLED:
    import ldap
    from django_auth_ldap.config import LDAPSearch, NestedMemberDNGroupType

    AUTH_LDAP_NO_NEW_USERS = not LDAP_SIGNUP

    AUTH_LDAP_SERVER_URI = LDAP_SERVER_URL
    AUTH_LDAP_BIND_DN = LDAP_BIND_USER_DN
    AUTH_LDAP_BIND_PASSWORD = LDAP_BIND_PASSWORD
    AUTH_LDAP_USER_SEARCH = LDAPSearch(
        LDAP_USER_BASE_DN,
        ldap.SCOPE_SUBTREE,
        LDAP_LOGIN_NAME_SEARCH,
    )
    AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
        LDAP_GROUP_BASE_DN, ldap.SCOPE_SUBTREE, LDAP_GROUP_FILTER
    )
    AUTH_LDAP_GROUP_TYPE = NestedMemberDNGroupType(
        LDAP_GROUP_MEMBER_ATTRIBUTE, name_attr=LDAP_GROUP_DISPLAY_NAME_ATTRIBUTE
    )
    AUTH_LDAP_MIRROR_GROUPS = True
    AUTH_LDAP_USER_ATTR_MAP = LDAP_USER_ATTRIBUTE_MAP

# Settings for django-allauth
ACCOUNT_ADAPTER = "heliport.core.adapters.AccountAdapter"
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = "none"
ACCOUNT_EMAIL_SUBJECT_PREFIX = "[HELIPORT] "
ACCOUNT_FORMS = {
    "change_password": "heliport.core.forms.ChangePasswordForm",
    "reset_password_from_key": "heliport.core.forms.ResetPasswordKeyForm",
    "set_password": "heliport.core.forms.SetPasswordForm",
    "signup": "heliport.core.forms.SignupForm",
}
ACCOUNT_USER_DISPLAY = "heliport.core.adapters.get_display_name"

SOCIALACCOUNT_ADAPTER = "heliport.core.adapters.HelmholtzIDAccountAdapter"
SOCIALACCOUNT_FORMS = {
    "signup": "heliport.core.forms.HelmholtzIDSignupForm",
}

# False == show registration form
SOCIALACCOUNT_AUTO_SIGNUP = False

if OIDC_ENABLED:
    SOCIALACCOUNT_PROVIDERS = {
        "openid_connect": {
            "APPS": [
                {
                    "provider_id": "helmholtz-id",
                    "name": "Helmholtz ID",
                    "client_id": env.str("OIDC_CLIENT_ID"),
                    "secret": env.str("OIDC_SECRET"),
                    "settings": {
                        "server_url": "https://login.helmholtz.de/oauth2",
                        "token_auth_method": "client_secret_basic",
                    },
                },
            ],
        }
    }

# User information managers keyed by the auth backend they belong to.
USER_INFORMATION_MANAGERS = {
    "django.contrib.auth.backends.ModelBackend": (
        "heliport.core.user_logic.user_information_managers.BasicUserInformationManager"
    ),
    "django_auth_ldap.backend.LDAPBackend": (
        "heliport.core.user_logic.user_information_managers.LdapUserInformationManager"
    ),
}

# ================================== HELIPORT SETUP ===================================

HELIPORT_GATE_ENABLED = env.bool("HELIPORT_GATE_ENABLED", default=False)
if HELIPORT_GATE_ENABLED and not LDAP_ENABLED:
    raise ImproperlyConfigured("HELIPORT_GATE_ENABLED requires LDAP_ENABLED")

HELIPORT_UNICORE_ENABLED = env.bool("HELIPORT_UNICORE_ENABLED", default=False)

HELIPORT_SYSTEM_APPS = [
    "heliport.core",
    "heliport.version_control",
    "heliport.data_management_plan",
    "heliport.documentation",
    "heliport.data_source",
    "heliport.archive",
    "heliport.publication",
    "heliport.cwl_execution",
    "heliport.digital_objects",
    "heliport.pid_registration",
    "heliport.sharelatex",
    "heliport.tables",
    "heliport.ssh",
    "heliport.smb_files",
]

if HELIPORT_GATE_ENABLED:
    HELIPORT_SYSTEM_APPS.append("heliport.gate_connection")

if HELIPORT_UNICORE_ENABLED:
    HELIPORT_SYSTEM_APPS.append("heliport.unicore")

HELIPORT_MIDDLEWARE = [
    "heliport.core.middleware.HeliportVersionMiddleware",
    "heliport.core.middleware.DisableClientSideCachingMiddleware",
]

# Items shown in the project nav bar
HELIPORT_PROJECT_NAVBAR_ITEMS = [
    "tags",
    "timeline",
    "object_graph",
]

# Items shown in the "Project" dropdown in the project nav bar
HELIPORT_PROJECT_DROPDOWN_ITEMS = [
    "project_configuration",
    "gate_connection",
    "subprojects",
]

# Project graph
HELIPORT_GRAPH = {
    "Systems": [
        "version_control",
        "data_management_plan",
        "documentation",
        "digital_objects",
        "sharelatex",
    ],
    "Resources": [
        "data_source",
        "arbitrary_cwl",
        "tool",
        "workflow",
        "tables",
        "images",
    ],
    "Automation": [
        "job",
        # this is either not the right place or "Automation" should be renamed
        "unicore_jobs",
    ],
    "Results": ["archive", "publication"],
}

# Dependencies between HELIPORT apps
HELIPORT_DEPENDENCIES = {
    "version_control": ["project_configuration"],
    "data_management_plan": ["project_configuration"],
    "documentation": ["project_configuration"],
    "data_source": ["project_configuration"],
    "archive": ["project_configuration"],
    "publication": ["project_configuration"],
    "job": ["project_configuration"],
    "tool": ["project_configuration"],
    "workflow": ["project_configuration"],
    "arbitrary_cwl": ["project_configuration"],
    "digital_objects": ["project_configuration"],
    "sharelatex": ["project_configuration"],
    "tables": ["project_configuration"],
    "images": ["project_configuration"],
    "timeline": ["project_configuration"],
    "object_graph": ["project_configuration"],
    "tags": ["project_configuration"],
    "subprojects": ["project_configuration"],
    "unicore_jobs": ["project_configuration"],
    "unicore_storages": ["project_configuration"],
    "ssh_files_directories": ["project_configuration"],
    "smb_shares": ["project_configuration"],
}

# Namespaces
# TODO: Maybe these can be moved somewhere so that each app (that needs it) has it
#  automatically? I.e. a HELIPORT-specific AppConfig/AppConf that all apps inherit from?
ARCHIVE_NAMESPACE = "HELIPORT/archive"
EXECUTABLE_ARBITRARY_CWL_NAMESPACE = "HELIPORT/cwl_file"
EXECUTABLE_WORKFLOW_NAMESPACE = "HELIPORT/workflow"
EXECUTABLE_TOOL_NAMESPACE = "HELIPORT/tool"
JOB_NAMESPACE = "HELIPORT/job"
DATA_MANAGEMENT_PLAN_NAMESPACE = "HELIPORT/data_management_plan"
DATA_SOURCE_NAMESPACE = "HELIPORT/data_source"
DOCUMENTATION_NAMESPACE = "HELIPORT/documentation"
PUBLICATION_NAMESPACE = "HELIPORT/publication"
VERSION_CONTROL_NAMESPACE = "HELIPORT/version_control"
PROPOSAL_NAMESPACE = "HELIPORT/proposal"
SHARE_LATEX_NAMESPACE = "HELIPORT/share_latex"
TABLE_NAMESPACE = "HELIPORT/table"
IMAGE_NAMESPACE = "HELIPORT/image"
GRAPH_VISUALIZATION_NAMESPACE = "HELIPORT/graph_visualization"
TAG_NAMESPACE = "HELIPORT/tag"
UNICORE_FILE_NAMESPACE = "UNICORE/file"
UNICORE_DIRECTORY_NAMESPACE = "UNICORE/directory"
SSH_FILE_NAMESPACE = "HELIPORT/file"
SSH_DIRECTORY_NAMESPACE = "HELIPORT/directory"
SMB_FILE_NAMESPACE = "SMB/file"
SMB_DIRECTORY_NAMESPACE = "SMB/directory"
PROPERTY_NAMESPACE = "property"
CLASS_NAMESPACE = "class"
DEFAULT_OBJECT_NAMESPACE = "object"

HELIPORT_SAMPLE_PROJECT = {
    "project": {
        "label": "My Sample HELIPORT Project",
        "description": (
            "A sample project showcasing which kinds of resources can be linked in "
            "HELIPORT. These resources do not belong to a singular real-world "
            "experiment but show a variety. Your own HELIPORT project may look "
            "completely differently."
        ),
    },
    "objects": [
        # Referenced in HELIPORT standard vocabulary:
        {"id": "HELIPORT", "pid": "https://hdl.handle.net/20.500.12865/HELIPORT"},
        {"id": "has_type", "pid": "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"},
        {"id": "has_part", "pid": "http://purl.org/dc/terms/hasPart"},
        {"id": "Prop", "pid": "http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"},
        {
            "id": "is_described_by",
            "pid": "http://purl.org/wf4ever/wfprov#describedByProcess",
        },
        # Shared between all example projects:
        {
            "id": "demonstrates",
            "pid": "https://hdl.handle.net/20.500.12865/Property.3712",
            "namespace": PROPERTY_NAMESPACE,
            "params": {
                "label": "demonstrates",
                "description": "something is used for demonstration of another thing",
            },
            "relations": [("has_type", "Prop")],
        },
        # New for each example project:
        {
            "type": "heliport.cwl_execution.models.Executable",
            "namespace": EXECUTABLE_TOOL_NAMESPACE,
            "init_params": {"type": 1},
            "params": {
                "cwl": {
                    "cwlVersion": "v1.0",
                    "class": "CommandLineTool",
                    "baseCommand": ["echo", "created"],
                    "label": "create something",
                    "doc": "Tries to convince you that it created something",
                    "inputs": {
                        "create": {
                            "type": "string",
                            "inputBinding": {"position": 0},
                            "label": "name of thing to create",
                        }
                    },
                    "outputs": {"result": {"type": "stdout"}},
                    "stdout": "result.txt",
                    "stderr": "",
                },
                "label": "create something",
                "description": "Tries to convince you that it created something",
            },
        },
        {
            "type": "heliport.cwl_execution.models.Executable",
            "namespace": EXECUTABLE_TOOL_NAMESPACE,
            "init_params": {"type": 1},
            "params": {
                "cwl": {
                    "cwlVersion": "v1.0",
                    "class": "CommandLineTool",
                    "baseCommand": "cat",
                    "label": "read prove",
                    "doc": (
                        "Read a proof that something really happened. "
                        "The proof is specified as a file input."
                    ),
                    "inputs": {
                        "proof": {
                            "type": "File",
                            "inputBinding": {"position": 0},
                            "label": "the proof to read",
                        }
                    },
                    "outputs": {},
                },
                "label": "create something",
                "description": "Tries to convince you that it created something",
            },
        },
        {
            "id": "create_image_workflow",
            "type": "heliport.cwl_execution.models.Executable",
            "namespace": EXECUTABLE_ARBITRARY_CWL_NAMESPACE,
            "init_params": {"type": 3},
            "params": {
                "cwl_type": 2,
                "cwl": {
                    "cwlVersion": "v1.0",
                    "class": "Workflow",
                    "label": "create image",
                    "doc": "creates an image",
                    "requirements": [{"class": "SubworkflowFeatureRequirement"}],
                    "inputs": [
                        {"id": "create", "type": "string", "valueFrom": "Image"}
                    ],
                    "outputs": [],
                    "steps": [
                        {
                            "id": "definitely_create_something",
                            "run": "definitely_create_something_3.cwl",
                            "in": [
                                {
                                    "id": "create",
                                    "sbg:type": "string",
                                    "source": "create",
                                }
                            ],
                            "out": [],
                        }
                    ],
                },
                "label": "create image",
                "description": "use custom cwl to definitely create an image",
            },
        },
        {
            "id": "creation_workflow",
            "type": "heliport.cwl_execution.models.Executable",
            "namespace": EXECUTABLE_WORKFLOW_NAMESPACE,
            "init_params": {"type": 2},
            "params": {
                "cwl": {
                    "cwlVersion": "v1.0",
                    "class": "Workflow",
                    "label": "definitely create something",
                    "doc": (
                        'Convinces you to create something by reading a "proof" that'
                        " it definitely created the thing."
                    ),
                    "sbg:x": -3.2159999999999855,
                    "sbg:y": -3.2720000000001166,
                    "requirements": [{"class": "SubworkflowFeatureRequirement"}],
                    "inputs": [
                        {
                            "id": "create",
                            "type": "string",
                            "sbg:x": 226.28399658203125,
                            "sbg:y": 216.44467163085938,
                        }
                    ],
                    "outputs": [],
                    "steps": [
                        {
                            "id": "create_something",
                            "run": "create_something_1.cwl",
                            "sbg:type": "tool",
                            "in": [
                                {
                                    "id": "create",
                                    "sbg:type": "string",
                                    "source": "create",
                                }
                            ],
                            "out": [{"id": "result", "sbg:type": "stdout"}],
                            "sbg:x": 441.01866912841797,
                            "sbg:y": 288.67933654785156,
                        },
                        {
                            "id": "read_prove",
                            "run": "read_prove_2.cwl",
                            "sbg:type": "tool",
                            "in": [
                                {
                                    "id": "proof",
                                    "sbg:type": "File",
                                    "source": "create_something/result",
                                }
                            ],
                            "out": [],
                            "sbg:x": 716.3173065185547,
                            "sbg:y": 298.0279846191406,
                        },
                    ],
                    "$namespaces": {"sbg": "https://heliport/visualization"},
                },
                "label": "definitely create something",
                "description": (
                    'Convinces you to create something by reading a "proof" '
                    "that it definitely created the thing."
                ),
            },
        },
        {
            "type": "heliport.cwl_execution.models.Job",
            "namespace": JOB_NAMESPACE,
            "init_params": {"base_executable": {"id": "creation_workflow"}},
            "params": {
                "output_text": "Success: Created Banana from thin Air",
                "output_code": 0,
                "cwl_json": '{"create": "Banana"}',
                "label": "Create Banana",
            },
        },
        {
            "type": "heliport.core.models.Tag",
            "namespace": TAG_NAMESPACE,
            "params": {
                "attribute": {"id": "is_described_by"},
                "value": {"id": "create_image_workflow"},
                "html_color": "#0000FF",
                "label": "Fancy Image",
                "description": (
                    "Something is considered to be a fancy image if it is "
                    "described by the workflow of this project."
                ),
            },
        },
        {
            "type": "heliport.version_control.models.VersionControl",
            "namespace": VERSION_CONTROL_NAMESPACE,
            "params": {
                "label": "HELIPORT",
                "link": "https://codebase.helmholtz.cloud/heliport/heliport",
            },
        },
        {
            "type": "heliport.version_control.models.VersionControl",
            "namespace": VERSION_CONTROL_NAMESPACE,
            "params": {
                "label": "Alpaka",
                "link": "https://github.com/alpaka-group/alpaka",
            },
        },
        {
            "type": "heliport.documentation.models.Documentation",
            "namespace": DOCUMENTATION_NAMESPACE,
            "init_params": {"system": 2},
            "params": {
                "description": "My Example Experiment Documentation",
                "link": "https://wiki.hzdr.de/wiki/ELN:Showcase_for_static_field",
            },
        },
        {
            "id": "important_documentation",
            "type": "heliport.documentation.models.Documentation",
            "namespace": DOCUMENTATION_NAMESPACE,
            "init_params": {"system": 4},
            "params": {
                "label": "Important documentation",
                "description": "Important documentation",
                "link": "https://notes.desy.de/features",
            },
        },
        {
            "type": "heliport.data_management_plan.models.DataManagementPlan",
            "namespace": DATA_MANAGEMENT_PLAN_NAMESPACE,
            "params": {
                "description": "There is no DMP here yet. But you can create one!",
                "link": "https://rdmo.hzdr.de/projects/create/",
            },
        },
        {
            "type": "heliport.sharelatex.models.ShareLaTeX",
            "namespace": SHARE_LATEX_NAMESPACE,
            "params": {
                "link": "https://collabtex.helmholtz.cloud/read/btbqmgddpqvz",
                "label": "HELIPORT Example LaTeX document",
            },
        },
        {
            "id": "microscope_images",
            "type": "heliport.data_source.models.DataSource",
            "namespace": DATA_SOURCE_NAMESPACE,
            "params": {
                "label": "Microscope images",
                "description": "Supporting microscope images for paper",
                "uri": "https://doi.org/10.14278/rodare.2529",
            },
            "relations": [("is_described_by", "create_image_workflow")],
        },
        {
            "type": "heliport.data_source.models.DataSource",
            "namespace": DATA_SOURCE_NAMESPACE,
            "params": {
                "label": "GenBank",
                "description": "Database containing annotated genetic sequences.",
                "uri": "ftp://ftp.ncbi.nlm.nih.gov/genbank",
            },
        },
        {
            "type": "heliport.publication.models.Publication",
            "namespace": PUBLICATION_NAMESPACE,
            "params": {
                "description": "Paper on electrochemical proton storage",
                "persistent_id": "https://doi.org/10.1002/anie.202310937",
            },
        },
        {
            "id": "my_table",
            "type": "heliport.tables.models.Table",
            "namespace": TABLE_NAMESPACE,
            "params": {"label": "Color Table", "description": "Table of Colors"},
        },
        {
            "id": "time_column",
            "type": "heliport.tables.models.TableColumn",
            "init_params": {
                "label": "added time",
                "index": 0,
                "column_type": 2,
                "parent_table": {"id": "my_table"},
            },
        },
        {
            "id": "name_column",
            "type": "heliport.tables.models.TableColumn",
            "init_params": {
                "label": "name",
                "index": 1,
                "column_type": 1,
                "parent_table": {"id": "my_table"},
            },
        },
        {
            "id": "length_column",
            "type": "heliport.tables.models.TableColumn",
            "init_params": {
                "label": "Wave Length",
                "index": 2,
                "column_type": 4,
                "parent_table": {"id": "my_table"},
            },
        },
        {
            "id": "image_column",
            "type": "heliport.tables.models.TableColumn",
            "init_params": {
                "label": "Example Picture",
                "index": 3,
                "column_type": 3,
                "parent_table": {"id": "my_table"},
            },
        },
        {
            "id": "red_row",
            "type": "heliport.tables.models.TableRow",
            "init_params": {"parent_table": {"id": "my_table"}},
        },
        {
            "id": "yellow_row",
            "type": "heliport.tables.models.TableRow",
            "init_params": {"parent_table": {"id": "my_table"}},
        },
        {
            "type": "heliport.tables.models.TableCell",
            "init_params": {
                "row": {"id": "red_row"},
                "column": {"id": "name_column"},
                "contents": "Red",
            },
        },
        {
            "type": "heliport.tables.models.TableCell",
            "init_params": {
                "row": {"id": "yellow_row"},
                "column": {"id": "name_column"},
                "contents": "Yellow",
            },
        },
        {
            "type": "heliport.tables.models.TableCell",
            "init_params": {
                "row": {"id": "red_row"},
                "column": {"id": "length_column"},
                "contents": "700 nm + 1 nm",
                "evaluated_contents": "701 nm",
            },
        },
        {
            "type": "heliport.tables.models.TableCell",
            "init_params": {
                "row": {"id": "yellow_row"},
                "column": {"id": "length_column"},
                "contents": "580 nm + 1",
                "evaluated_contents": "ERROR: can not add Unit to number",
            },
        },
        {
            "namespace": "data/collection",
            "params": {
                "label": "My Data Collection",
                "description": "A Collection",
            },
            "relations": [
                ("has_part", "microscope_images"),
                ("has_part", "important_documentation"),
            ],
        },
        {
            "id": "part_visualization",
            "type": "heliport.digital_objects.models.ObjectGraph",
            "namespace": GRAPH_VISUALIZATION_NAMESPACE,
            "params": {"label": "Part visualization"},
        },
        {
            "id": "part_path",
            "type": "heliport.digital_objects.models.PropertyPath",
            "init_params": {"property": {"id": "has_part"}},
        },
        {
            "type": "heliport.digital_objects.models.VisualizationEdge",
            "init_params": {
                "hex_color": "FF0000",
                "path": {"id": "part_path"},
                "visualization": {"id": "part_visualization"},
            },
        },
        {
            "id": "workflow_visualization",
            "type": "heliport.digital_objects.models.ObjectGraph",
            "namespace": GRAPH_VISUALIZATION_NAMESPACE,
            "params": {"label": "data - workflow relation visualization"},
        },
        {
            "id": "workflow_path",
            "type": "heliport.digital_objects.models.PropertyPath",
            "init_params": {"property": {"id": "is_described_by"}},
        },
        {
            "type": "heliport.digital_objects.models.VisualizationEdge",
            "init_params": {
                "hex_color": "00FF00",
                "path": {"id": "workflow_path"},
                "visualization": {"id": "workflow_visualization"},
            },
        },
        {
            "id": "combined_visualization",
            "type": "heliport.digital_objects.models.ObjectGraph",
            "namespace": GRAPH_VISUALIZATION_NAMESPACE,
            "params": {"label": "Combined visualization"},
        },
        {
            "id": "workflow_path_2",
            "type": "heliport.digital_objects.models.PropertyPath",
            "init_params": {"property": {"id": "is_described_by"}},
        },
        {
            "id": "part_path_2",
            "type": "heliport.digital_objects.models.PropertyPath",
            "init_params": {"property": {"id": "has_part"}},
        },
        {
            "type": "heliport.digital_objects.models.VisualizationEdge",
            "init_params": {
                "hex_color": "00FF00",
                "path": {"id": "workflow_path_2"},
                "visualization": {"id": "combined_visualization"},
            },
        },
        {
            "type": "heliport.digital_objects.models.VisualizationEdge",
            "init_params": {
                "hex_color": "FF0000",
                "path": {"id": "part_path_2"},
                "visualization": {"id": "combined_visualization"},
            },
        },
    ],
}

# =================================== DJANGO SETUP ====================================

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "rest_framework",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "allauth.socialaccount.providers.openid_connect",
    "knox",
    "corsheaders",
    "django_htmx",
    "django_filters",
    "django_celery_results",
    "drf_spectacular",
    "drf_spectacular_sidecar",
    "debug_toolbar",
    "health_check",
    "health_check.db",
    "health_check.cache",
    "health_check.storage",
    "vendor_files",
    "qr_code",
    "widget_tweaks",
] + HELIPORT_SYSTEM_APPS

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "allauth.account.middleware.AccountMiddleware",
    "django_htmx.middleware.HtmxMiddleware",
] + HELIPORT_MIDDLEWARE

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            # template overrides for third-party apps
            BASE_DIR / "heliport" / "templates",
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "heliport.core.context_processors.auth_settings",
                "heliport.core.context_processors.heliport_version",
            ],
        },
    },
]

STATIC_URL = "/static/"
STATIC_ROOT = "static"
STATICFILES_DIRS = [
    BASE_DIR / "dist",
]

DEFAULT_DATABASE_DIRECTORY = BASE_DIR if DEVELOPMENT else "/var/lib/heliport"
DATABASES = {
    "default": env.db_url(
        "DB_URL",
        default=f"sqlite:///{DEFAULT_DATABASE_DIRECTORY}/heliport.sqlite3",
    )
}

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        "LOCATION": "heliport-default-cache",
    },
    "image_cache": {
        "BACKEND": "django.core.cache.backends.filebased.FileBasedCache",
        "LOCATION": "/var/tmp/heliport_image_cache",
    },
}

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "file": {
            "class": "logging.FileHandler",
            "filename": BASE_DIR / "heliport.log"
            if DEVELOPMENT
            else "/var/log/heliport/heliport.log",
            "formatter": "default",
        },
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "default",
        },
    },
    "loggers": {
        "root": {
            "handlers": ["file"] + (["console"] if DEVELOPMENT else []),
            "level": "DEBUG" if DEVELOPMENT else "INFO",
        },
    },
    "formatters": {
        "default": {
            "format": "{asctime} - {name} - {levelname} :-: {message}",
            "datefmt": "%m/%d/%Y %I:%M:%S %p",
            "style": "{",
        }
    },
}

EMAIL_ENABLED = env.bool("EMAIL_ENABLED", default=False)
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

EMAIL_SUBJECT_PREFIX = "[HELIPORT] "
DEFAULT_FROM_EMAIL = env.str("EMAIL_FROM_ADDRESS", default="noreply@heliport")

if EMAIL_ENABLED:
    ACCOUNT_EMAIL_VERIFICATION = "optional"

if EMAIL_ENABLED and not DEVELOPMENT:
    EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"

    EMAIL_HOST = env.str("EMAIL_HOST", default="localhost")
    EMAIL_PORT = env.int("EMAIL_PORT", default=25)
    EMAIL_USE_SSL = env.bool("EMAIL_USE_SSL", default=False)
    EMAIL_HOST_USER = env.str("EMAIL_USER", default="")
    EMAIL_HOST_PASSWORD = env.str("EMAIL_PASSWORD", default="")

WSGI_APPLICATION = "heliport_config.wsgi.application"
ROOT_URLCONF = "heliport_config.urls"

# In development mode, also show debug messages from contrib.messages
if DEVELOPMENT:
    MESSAGE_LEVEL = messages.constants.DEBUG

# Security settings
DEBUG = DEVELOPMENT
SECRET_KEY = env.str("SECRET_KEY")
if SECRET_KEY == "":
    raise ImproperlyConfigured("SECRET_KEY must not be empty")
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", default=[]) + ["127.0.0.1", "localhost"]
CSRF_COOKIE_SECURE = not DEVELOPMENT
SESSION_COOKIE_SECURE = not DEVELOPMENT
SECURE_SSL_REDIRECT = not DEVELOPMENT
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

LANGUAGES = [("en", "English")]
USE_I18N = False
USE_TZ = True
TIME_ZONE = env.str("TIME_ZONE", default="Europe/Berlin")

REST_KNOX = {
    # Auth tokens start with "heli-". This makes them easier to identify/search for.
    "TOKEN_PREFIX": "heli-",
}

# For image upload
DATA_UPLOAD_MAX_MEMORY_SIZE = 36 * 1024 * 1024  # 36 MiB

# Settings for django-cors-headers
CORS_ALLOWED_ORIGINS = env.list("CORS_ALLOWED_ORIGINS", default=[])
# CORS_ALLOWED_ORIGIN_REGEXES = []
# CORS_ALLOW_ALL_ORIGINS = False

# Settings for django-rest-framework
REST_FRAMEWORK = {
    "PAGE_SIZE": 10,
    "AUTO_REFRESH": True,
    "MIN_REFRESH_INTERVAL": 60,  # seconds
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "knox.auth.TokenAuthentication",
        "rest_framework.authentication.SessionAuthentication",
    ],
    "DEFAULT_CONTENT_NEGOTIATION_CLASS": "heliport.core.negotiation.HeliportContentNegotiation",  # noqa: E501
    "DEFAULT_FILTER_BACKENDS": [
        "django_filters.rest_framework.DjangoFilterBackend",
    ],
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.IsAuthenticated",
    ],
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
}

# Settings for drf-spectacular
SPECTACULAR_SETTINGS = {
    "TITLE": "HELIPORT API",
    "DESCRIPTION": "API for all things …",
    "VERSION": "0.0.0",
    "REDOC_DIST": "SIDECAR",
}

# Settings for django-debug-toolbar
INTERNAL_IPS = ["127.0.0.1"]

# Settings for django-qr-code
CACHES["qr-code"] = {
    "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
    "LOCATION": "heliport-qr-code",
    "TIMEOUT": 3600,
}
QR_CODE_CACHE_ALIAS = "qr-code"

# Settings for django-vendor-files
VENDOR = {
    "codemirror": {
        "url": "https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.62.3",
        "css": [
            {
                "path": "codemirror.min.css",
                "sri": "sha256-5FUSxoAAxj7EawcT91ypKxW1/hqiqk9+iZrC0YVUpXE=",
            },
            {
                "path": "addon/lint/lint.min.css",
                "sri": "sha256-i15v4LqAUUmKbCE8Q3DScydN10CNr2KXQSJYpO4Kn9c=",
            },
        ],
        "js": [
            {
                "path": "codemirror.js",
                "sri": "sha256-RcWlmFozMpt3otHcBDVOQZwKWJVJbZefcL7P8RFMjIU=",
            },
            {
                "path": "mode/yaml/yaml.min.js",
                "sri": "sha256-f4cVO760p74CUg8I1zTABL0Jlh4uBM9e1icXOzul9mw=",
            },
            {
                "path": "mode/javascript/javascript.min.js",
                "sri": "sha256-sT8fvG5+IebZUlN6mimbC2Ey0zYPIxh4KFlsSH9iQSM=",
            },
            {
                "path": "mode/python/python.min.js",
                "sri": "sha256-q0IZzplBc6wvjrvXYEPAKnmo1kAQhNfLK06FTf8alcY=",
            },
            {
                "path": "addon/edit/matchbrackets.min.js",
                "sri": "sha256-6AGEQltFbI7yz43keafz8ApVAKWGqlznxIP4Gp4YbkA=",
            },
            {
                "path": "addon/edit/closebrackets.min.js",
                "sri": "sha256-1kWSbOVWUlRgOkImqaxYLLHf4gybFtn1LiwShRkt9Ps=",
            },
            {
                "path": "addon/selection/active-line.min.js",
                "sri": "sha256-7voGs4hw5lZcnlQJB0JUAZh4Meyq8ZgGYQJSL/HbtBY=",
            },
            {
                "path": "addon/lint/lint.min.js",
                "sri": "sha256-VJbY/FFsVaNPb/q9bRuB3+C+kqFPAT8w23dzrrCsbHQ=",
            },
            {
                "path": "addon/lint/yaml-lint.min.js",
                "sri": "sha256-raDVyGYYpblDA+Zvxvho8ee1bxt/vZwwDnfKadlrorI=",
            },
            {
                "path": "addon/lint/json-lint.min.js",
                "sri": "sha256-PcklWtCA5xmRzUEPmo6zzLcZEwHGHy1F77umhteVjDo=",
            },
        ],
    },
    "js-yaml": {
        "url": "https://cdnjs.cloudflare.com/ajax/libs/js-yaml/4.1.0",
        "js": [
            {
                "path": "js-yaml.min.js",
                "sri": "sha256-Rdw90D3AegZwWiwpibjH9wkBPwS9U4bjJ51ORH8H69c=",
            }
        ],
    },
    "jsonlint": {
        "url": "https://cdnjs.cloudflare.com/ajax/libs/jsonlint/1.6.0",
        "js": [
            {
                "path": "jsonlint.min.js",
                "sri": "sha256-yWZBgNxWVjnXu4Tut5/J0MAvczYc2U3SuK0svKSMaW4=",
            }
        ],
    },
    "plotly": {
        "url": "https://cdn.plot.ly",
        "js": [
            {
                "path": "plotly-2.12.1.min.js",
                "sri": "sha256-EOvE082RJUkrPN39zTSKGVbFltkPWbRDVYYas0yY2QI=",
            }
        ],
    },
    "vis-network": {
        "url": "https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0",
        "css": [
            {
                "path": "vis-network.min.css",
                "sri": "sha384-hv6STAGuk4qTwmryFbZZTn3QrGRyZW1soC9K/Dy68zs8subBFOU69tg/GGZfkIBb",  # noqa: E501
            }
        ],
        "js": [
            {
                "path": "vis-network.min.js",
                "sri": "sha384-9MO63a7p8qVjz/d98HqH2kaxNad8E6VBs7rmVSkwVh+rHlUs72NmQkO+1uYl6rit",  # noqa: E501
            }
        ],
    },
}
VENDOR_CDN = DEVELOPMENT
if not VENDOR_CDN:
    STATICFILES_DIRS.append(BASE_DIR / "vendor")


# =================================== CELERY SETUP ====================================

CELERY_BROKER_URL = env.str("CELERY_BROKER_URL")
CELERY_RESULT_BACKEND = env.str("CELERY_RESULT_BACKEND", default="django-db")
# Deprecated by Celery but still used by django_celery_results
CELERY_CACHE_BACKEND = "django-cache" if CELERY_RESULT_BACKEND == "django-db" else None

CELERY_RESULT_EXPIRES = 5 * 24 * 60 * 60  # 5 days

CELERY_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"
CELERY_TASK_SERIALIZER = "json"
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_TIMEZONE = TIME_ZONE

CELERY_BEAT_SCHEDULE = {
    "periodic_job_execution": {
        "task": "heliport.cwl_execution.tasks.run_periodic_jobs",
        "schedule": crontab(minute=50, hour=2),
    },
}

if HELIPORT_GATE_ENABLED:
    CELERY_BEAT_SCHEDULE["daily_gate_sync"] = {
        "task": "heliport.gate_connection.tasks.update_gate_projects",
        "schedule": crontab(minute=30, hour=2),
    }
