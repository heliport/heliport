# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later
"""Configuration for the Celery runner."""

from __future__ import absolute_import

import os

from celery import Celery
from django.conf import settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "heliport_config.settings")

app = Celery("heliport_config")

# Load task modules from all registered Django app configs.
app.config_from_object("django.conf:settings", namespace="CELERY")

app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

task_annotations = {"heliport.cwl_execution.tasks.job_start": {"rate_limit": "10/m"}}

# Circumvent common problems
# https://steve.dignam.xyz/2023/05/20/many-problems-with-celery/
worker_prefetch_multiplier = 0
task_acks_late = True
task_reject_on_worker_lost = True
