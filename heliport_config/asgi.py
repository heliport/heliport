# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
ASGI config for heliport project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see :doc:`django:howto/deployment/asgi/index`.
"""

import os

from django.core.asgi import get_asgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "heliport_config.settings")

application = get_asgi_application()
