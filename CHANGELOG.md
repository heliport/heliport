<!--
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Changelog

## In Development

**User Interface:**

- The HELIPORT user interface now uses Bootstrap 5. The Bootstrap documentation provides
  a [migration guide](https://getbootstrap.com/docs/5.0/migration/) that can be followed
  to update custom HELIPORT apps.

**API:**

- The API URLs for all apps were completely reworked. This includes changes in
  `urlpattern`s, routers, spelling, wording and overall structure. Now all APIs are
  grouped under a top-level `/api/` path. We also established a document with
  conventions related to URL construction which can be found in the docs.

**Metadata Export:**

- No longer export HELIPORT internal attributes (like the creation date) for external IRIs like `rdf:type`.

**Core App:**

- "Public" landing pages were a workaround which is now removed.
- It is now possible to configure various URLs pointing to external pages. All of these
  URLs have their own endpoint in HELIPORT (e.g. `/about/`) which redirects to that
  page:
  - `HELIPORT_CORE_ABOUT_URL` - About page
  - `HELIPORT_CORE_DOCS_URL` - Documentation page
  - `HELIPORT_CORE_CITE_URL` - Page with citation instructions
  - `HELIPORT_CORE_IMPRINT_URL` - Imprint
  - `HELIPORT_CORE_PRIVACY_URL` - Privacy policy
  - `HELIPORT_CORE_TOS_URL` - Terms of service (terms of use, terms and conditions)
- Login is now possible via OpenID Connect (OIDC). The related HELIPORT settings and
  templates are preconfigured to work with [Helmholtz ID](https://login.helmholtz.de).
- Authentication for non-LDAP accounts (i.e. local, OIDC, and any other configured
  options) is now handled by
  [django-allauth](https://github.com/pennersr/django-allauth) rather than custom forms.
- All login and user registration/signup options are now usable independently of
  eachother; i.e. any combination of the available options (local accounts, LDAP, OIDC)
  can be used. For each of the options, signup can be disabled separately. Local
  accounts can not be disabled in order to keep the admin interface functional. See
  `LOCAL_ACCOUNT_SIGNUP`, `LDAP_ENABLED`, `LDAP_SIGNUP`, `OIDC_ENABLED`, `OIDC_SIGNUP`
  settings.
- OIDC logins can be restricted to accounts with certain eduperson entitlements using
  the `OIDC_EDUPERSON_ENTITLEMENTS` setting.
- Automatic synchronization of LDAP accounts and groups was removed as this doesn't
  work well anymore when different login options are available with the possibility of
  linking accounts.
- HELIPORT is now able to send emails for email address verification and password reset
  (see `EMAIL_*` settings). Even with email settings enabled, verification of email
  addresses is only optional.
- `HELIPORT_SAMPLE_PROJECT` is a new setting that allows administrators to define a
  sample project that users can create at the click of a button. This project is meant
  as a starting point and a showcase of HELIPORT features.
- The `HELIPORT_CORE_PUBLIC_FAVICON_URL` setting was removed as it wasn't used any more.
- The `HELIPORT_CORE_ABOUT_BASE_URL` setting was removed in favour of the more flexible
  new URL options mentioned above.
- The "Create Empty Project" functionality was removed as the feature was too confusing.
- The option to create handles pointing to data URLs (setting
  `HELIPORT_HANDLE_REGISTER_JSON_URL_HANDLES`) was removed. This is due to the fact that
  modern browsers don't allow redirects to data URLs any more and thus, resolving such
  handles and following the redirect creates an error message.
- The new required setting `HELIPORT_CORE_HOST` replaces
  `HELIPORT_HANDLE_DIGITAL_OBJECT_HOST` and `HELIPORT_HANDLE_LOCAL_DIGITAL_OBJECT_HOST`.
- The settings `HELIPORT_HANDLE_URL_FORMAT` and `HELIPORT_HANDLE_RESOLVE_PID_FORMAT_TO`
  were removed. Handles now always point to the digital object landing pages provided by
  HELIPORT.
- Introduce HELIPORT module class {class}`heliport.core.app_interaction.DigitalObjectModule`
  that allows more features and easier implementation of modules that represent a subclass of DigitalObject.
- There is now a template `/core/base/form.html` to display Django forms in a HELIPORT specific style
- Improved breadcrumbs:
  - Can be controlled by `reset_heliport_breadcrumbs` URL parameter
  - Support nested modules, that are displayed in other modules, automatically
- Fixed security problem that allowed logged-in users to delete objects without access control
- Added {class}`heliport.core.views.generic.HeliportCreateAndUpdateView` as new generic base view for views that
  allow creating and editing digital objects
- New class of access rights for deleting digital objects
- New model {class}`heliport.core.models.DigitalObjectIdentifier` allows for
  registration of arbitrary identifiers in HELIPORT. This model will eventually replace
  `DigitalObject.persistend_id` and `DigitalObject.generated_persistent_id`. The method
  {meth}`heliport.core.models.DigitalObject.preferred_identifier` can be used to fetch
  the object's "best" identifier from this model.
- The `hzdr_id` and `uuid` attributes from {class}`heliport.core.models.Project` were
  removed.
  Instead, all digital objects (even non-projects) now receive a HELIPORT-internal
  identifier that can be used as a default if the HELIPORT instance does not register
  PIDs.
- All functionality related to creation of Handle.Net identifiers was removed from the
  `heliport.core` app and re-implemented in the new {mod}`heliport.pid_registration`
  app.
- The root path of the app now shows a welcome page with some basic information about
  HELIPORT and some statistics on the number of users, projects, and digital objects.

**PID Registration App:**

- A new app for registering PIDs for digital objects on first save Currently, there is
  only a Handle.Net implementation available.

**Data Source App:**

- Every file or directory in HELIPORT is now displayed under "Data Source"
- Instances for modules from all apps, that expose a Django form, can directly be edited and created under "Data Source" now

**SSH App:**

- Exposes a Django form for directly editing SSH files and directories in other apps

**Miscellaneous:**

- Support for Python 3.12 and 3.13 was added.
- Support for Python 3.8 was removed.
- The minimum required Poetry version is now 2.0.
- The version reported by HELIPORT is now determined dynamically.
- The HELIPORT version is now shown when hovering over "Powered by" in the footer, and
  it is sent as an HTTP response header `X-HELIPORT-Version`.

## 0.6.0

Release date: 2023-06-15

**User Interface:**

- The navbar was split in two. The top one still holds globally useful navigations like
  user settings and the search. The navbar below it shows project-related settings and
  apps that were previously shown in the left-most column of the project graph so that
  they are now easily accessible from other places.
- HELIPORT now uses Font Awesome 6 icons. Font Awesome 6 is backwards-compatible, thus
  old icons (`fa`, `fas`, `fab`, ... classes) still work.

**Architecture Additions:**

- Introduced the concept of **Actions** that allow to use functionality of one app
  inside another app without dependency between the apps
- Introduce the concept of a DigitalObject that represents a **File**
- Introduce the concept of a DigitalObject that represents a **Directory**
- Introduce the concept of operation **context** that allows to cache values with
  support for python context managers
- Introduce the concept of **user messages** that can be raised as exception to display
  an error to the user
- Introduce the concept of **GeneralValue** to represent something that can be a value
  of some parameter of a digital object and can be serialized
- Introduce the concept of **GeneralDigitalObject** to represent objects not stored in
  the HELIPORT database. A GeneralDigitalObject supports only the most basic
  functionality but can be easily imported as DigitalObject.
- Introduce the concept of object **resolution**, that allows to represent objects not
  stored in the HELIPORT database (e.g. in URL parameters)

**Core App:**

- Added field `is_helper` to `DigitalObject` to allow representing DigitalObjects that
  are not explicitly created by the user.
- Added functions and methods to allow working with tags more easily
- Added view to allow importing a GeneralDigitalObject as DigitalObject into the
  HELIPORT database
- The views are now split into multiple modules within {mod}`heliport.core.views`
- HELIPORT mixins are now found in {mod}`heliport.core.mixins`; `HeliportObjectListView`
  is under {mod}`heliport.core.views.generic`
- Added support for login selection and actions in
  {class}`heliport.core.views.generic.HeliportObjectListView`
- Added {class}`heliport.core.renderers.HeliportPartialRenderer` for rendering of
  HTML partials to be sent via the API
- Added {class}`heliport.core.negotiation.HeliportContentNegotiation` which handles
  API requests sent from HTMX and returns partials to them
- Implement downloading files and showing contents of directories via the
  `/api/digital-objects/complete/` API
- Support for showing E-Mail of contributors and project members

**Data Source App:**

- Extend data source list to support actions
- New view to show arbitrary directories and allow file download
- Data sources with ssh protocol are now deprecated in favour of the ssh app
- New actions to open arbitrary files and directories

**UNICORE App:**

- UNICORE storages can now be added to projects
- Implement file and directory interfaces for UNICORE storages (this allows for example
  a preview)
- The UNICORE "overview" view was removed in favor of an interface that looks more like
  other apps

**SSH App:**

- Introduced this as the new app to handle all ssh functionality
- Allows adding SSH files and directories to project
- Implement file and directory interfaces for files and directories accessible via SSH
  (this allows for example a preview)
- Add action to migrate old ssh data sources to this app
- Implement API to download files and show contents of directories

**SMB Files App:**

- Introduce this as a new app to handle SMB shares
- Allow adding SMB shares to project
- Implement file and directory interfaces for SMB files and
  directories (this allows for example a preview)
- Implement API to download files and show contents of directories

**Version Control App:**

- Implement file and directory interfaces for GitLab and GitHub files
  and directories (this allows e.g. download)

**Gate Connection App:**

- E-Mail and affiliation information is now imported from the proposal

**Miscellaneous:**

- The default database location in production mode (`DEVELOPMENT == False`) changed from
  `${BASE_DIR}/heliport.sqlite3` to `/var/lib/heliport/heliport.sqlite3`.
- The `monitoring` extra for the [Flower](https://github.com/mher/flower) monitor was
  removed as it wasn't well integrated in the repo and lacked instructions due to not
  being used by the maintainers.
- HELIPORT is now running on
  [Django 4.2](https://docs.djangoproject.com/en/4.2/releases/4.2/).
- The main JavaScript and CSS libraries are now installed with yarn and bundled with
  webpack. Some CSS and JavaScript is managed using
  [django-vendor-files](https://github.com/jochenklar/django-vendor-files)
  and will be moved into the webpack setup eventually.
- HELIPORT now provides `heliport-cli` as a convenience command for the Django
  `manage.py` script.
- The minimum required Poetry version is now 1.2.
- Added alpinejs to be used in frontend
- Added RootedRDFGraph to collections to store RDF data about something specific (that
  is the root)
- Renamed `Project.digitalobject_set` to `Project.parts`
- Project list now supports pagination and sorting
- Projects can now be tagged

## 0.5.0

Release date: 2022-11-24

This is the first release after some major restructuring in the HELIPORT codebase and
infrastructure. Future releases will provide more detailed information about the changes
that were introduced.
