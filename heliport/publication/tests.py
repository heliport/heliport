# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from heliport.core.models import HeliportUser, Project

from .models import Publication


class PublicationTests(TestCase):  # noqa: D101
    def setUp(self):
        """Set up project and logged-in heliport user."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.client.force_login(self.auth_user)
        self.project = Project.objects.create(owner=self.user)

    def test_list(self):  # noqa: D102
        pub = Publication.objects.create(description="pub_label")
        pub.projects.add(self.project)
        pub_handle = Publication.objects.create(
            persistent_id="https://hdl.handle.net/pub_handle"
        )
        pub_url = Publication.objects.create()
        pub_url.link = "hdl.handle.net/pub_url"
        pub_handle.projects.add(self.project)
        pub_url.projects.add(self.project)

        response = self.client.get(
            reverse("publication:list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(response.status_code, 200)
        test_objs = Publication.objects.filter(
            projects=response.context_data["project"]
        )
        self.assertGreater(len(test_objs), 0)

        self.assertContains(response, "pub_label")
        self.assertContains(response, 'href="https://hdl.handle.net/pub_handle"')
        self.assertContains(response, 'href="https://hdl.handle.net/pub_url"')

    def test_create(self):  # noqa: D102
        self.client.post(
            reverse("publication:list", kwargs={"project": self.project.pk}),
            data=urlencode({"form_data_field_description": "test_publication321"}),
            content_type="application/x-www-form-urlencoded",
        )
        test_obj = Publication.objects.filter(description="test_publication321").first()
        self.assertIsNotNone(test_obj)
        self.assertIn(self.project, test_obj.projects.all())

    def test_url(self):  # noqa: D102
        p = Publication()
        p.save()
        p.link = "www.google.de"
        self.assertEqual("https://www.google.de", p.link)


class SearchAndAPITest(TestCase):
    """Test search including via API."""

    def setUp(self):
        """Set up project and logged-in heliport user."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.project, is_new = Project.objects.get_or_create(owner=self.user)
        self.client.force_login(self.auth_user)

    def test_access_search(self):
        """Test that search URL is accessible."""
        response = self.client.get(reverse("publication:search"))
        self.assertEqual(200, response.status_code)

    def test_publication_findable(self):
        """Test that publication is findable."""
        pub = Publication.objects.create(description="PUBLICATION321")
        pub.projects.add(self.project)
        response = self.client.get(f"{reverse('publication:search')}?q=321")
        self.assertContains(response, "PUBLICATION321")

    def test_api(self):
        """Test api."""
        pub = Publication.objects.create(description="PUBLICATION654")
        pub.projects.add(self.project)
        response = self.client.get("/api/publications/")
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "PUBLICATION654")

        link = "https://example.com/publication/"
        response = self.client.post(
            "/api/publications/",
            data={"url": link, "system": 5},  # 5 == other
        )
        self.assertEqual(201, response.status_code)
        self.assertEqual(Publication.objects.last().link, link)


class ProjectSerializeTest(TestCase):  # noqa: D101
    def setUp(self):  # noqa: D102
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.project, is_new = Project.objects.get_or_create(owner=self.user)
        self.client.force_login(self.auth_user)
        self.publication_description = "PUBLICATION TO SERIALIZE"
        self.publication = Publication(
            description=self.publication_description,
            persistent_id="https://hdl.handle.net/aaa",
        )
        self.publication.save()
        self.publication.link = "https://www.publication.de"
        self.publication.projects.add(self.project)

    @staticmethod
    def landing_page_url(obj):  # noqa: D102
        return reverse("core:landing_page", kwargs={"pk": obj.digital_object_id})

    def test_json_ld(self):  # noqa: D102
        response = self.client.get(
            f"{self.landing_page_url(self.project)}?format=jsonld"
        )
        self.assertContains(response, self.publication_description)
        response = self.client.get(
            f"{self.landing_page_url(self.publication)}?format=jsonld"
        )
        publication = response.json()
        self.assertIn("@id", publication)
        self.assertEqual(
            self.publication_description, publication["dcterms:description"]
        )
        self.assertContains(response, "https://www.publication.de")
        self.assertContains(response, "https://hdl.handle.net/aaa")

    def test_datacite(self):  # noqa: D102
        response = self.client.get(
            f"{self.landing_page_url(self.publication)}?format=datacite_json"
        )
        metadata = response.json()
        self.assertEqual(
            [
                {
                    "description": self.publication_description,
                    "descriptionType": "Abstract",
                    "lang": "en",
                }
            ],
            metadata["descriptions"],
        )
        self.assertEqual(
            {"resourceType": "publication", "resourceTypeGeneral": "Dataset"},
            metadata["types"],
        )

    def test_rest_api(self):  # noqa: D102
        response = self.client.get("/api/publications/")
        publication = response.json()["results"][0]
        self.assertEqual(
            {
                "publication_id": 1,
                "description": self.publication_description,
                "persistent_id": "https://hdl.handle.net/aaa",
                "url": "https://www.publication.de",
                "projects": [self.project.pk],
            },
            publication,
        )
