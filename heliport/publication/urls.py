# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path
from rest_framework import routers

from .views import PublicationView, PublicationViewSet, SearchView

app_name = "publication"
urlpatterns = [
    path(
        "project/<int:project>/publication/list/",
        PublicationView.as_view(),
        name="list",
    ),
    path(
        "project/<int:project>/publication/<int:pk>/update/",
        PublicationView.as_view(),
        name="update",
    ),
    path("publication/search/", SearchView.as_view(), name="search"),
]

# REST API
router = routers.DefaultRouter()
router.register(r"publications", PublicationViewSet, basename="publications")
