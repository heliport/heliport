# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

import django_filters
from django.conf import settings
from django.db.models import Q
from django.views.generic import TemplateView
from rest_framework import filters as rest_filters

from heliport.core.mixins import HeliportLoginRequiredMixin
from heliport.core.permissions import is_object_member
from heliport.core.views import HeliportModelViewSet, HeliportObjectListView

from .models import Publication
from .serializers import PublicationSerializer


class PublicationView(HeliportObjectListView):  # noqa: D101
    model = Publication
    category = settings.PUBLICATION_NAMESPACE
    columns = [
        ("ID", "publication_id", "small"),
        ("DOI/Handle/...", "persistent_id", "normal"),
        ("Description", "description", "large"),
    ]
    actions = [
        ("Open", "action_open", "link"),
        ("Edit", "action_edit", "link_secondary"),
        ("Delete", "action_delete", "danger"),
    ]
    edit_fields = [
        ("DOI/Handle/...", "persistent_id", "normal"),
        ("Link", "link", "normal"),
        ("Description", "description", "large"),
    ]
    after_save_attributes = {"link"}
    list_url = "publication:list"
    update_url = "publication:update"
    list_heading = "Publications"
    create_heading = "Add a Publication"
    add_text = [
        "or create one first with",
        {"text": "RODARE", "link": "https://rodare.hzdr.de/deposit/new"},
        "or",
        {"text": "ROBIS", "link": "https://www.hzdr.de/db/!Publications"},
        "or",
        {
            "text": "ROBIS directly",
            "link": "https://www.hzdr.de/db/!WebAppl.Publ_Input_V5.Data",
        },
    ]

    def action_open(self, obj):  # noqa: D102
        if obj.link:
            return obj.link
        if obj.persistent_id:
            return obj.persistent_id
        return None


class SearchView(HeliportLoginRequiredMixin, TemplateView):  # noqa: D101
    template_name = "publication/search.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the search results.
        """
        context = super().get_context_data(**kwargs)
        query = self.request.GET.get("q", "")

        publication_results = set()

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            publication_results.update(
                Publication.objects.filter(
                    Q(deleted__isnull=True)
                    & (
                        Q(publication_id=num)
                        | Q(description__icontains=word)
                        | Q(attributes__value__icontains=word)
                    )
                )
            )

        context["publication_results"] = publication_results
        return context


##########################################
#               REST API                 #
##########################################


class PublicationViewSet(HeliportModelViewSet):
    """Publication."""

    serializer_class = PublicationSerializer
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    filterset_fields = ["publication_id", "persistent_id"]
    search_fields = ["attributes__value"]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return Publication.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()
