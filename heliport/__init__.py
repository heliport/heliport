# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""The main heliport module.

Its submodules contain the heliport apps. Among them heliport.core, which provides
facilities for all other apps to use.
"""

# NOTE: Do not try to use ``__version__`` or ``__version_tuple__``.
#
# These variables can theoretically be determined and set by the
# ``poetry-dynamic-versioning`` plugin. However, since we never really "install"
# HELIPORT (we copy the whole directory and run it from within the directory), the
# version number is always taken from this file rather than the installed file in which
# the replacement was made.
#
# The real version number is available in ``heliport.core.utils.version`` as
# ``heliport_version_string`` and ``heliport_version_tuple`` which can be used
# throughout the codebase.
#
# __version__ = "0.0.0"
# __version_tuple__ = (0, 0, 0)
