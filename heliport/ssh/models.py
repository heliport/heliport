# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains :class:`django.db.models.Model` classes for Django ORM.

See :ref:`django:topics/db/models:quick example` from Django documentation.
In HELIPORT the :class:`heliport.core.models.DigitalObject` can be subclassed for models
containing metadata in a project.
"""

from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass
from mimetypes import guess_type
from pathlib import Path
from stat import S_ISDIR
from typing import Dict, Iterator, Optional, Union

from django.conf import settings
from django.db import models
from django.urls import reverse

from heliport.core.app_interaction import DigitalObjectModule
from heliport.core.digital_object_aspects import (
    Directory,
    DirectoryObj,
    File,
    FileObj,
    FileOrDirectory,
)
from heliport.core.digital_object_interface import GeneralDigitalObject
from heliport.core.models import DigitalObject, LoginInfo, Vocabulary, assert_relation
from heliport.core.permissions import PermissionChecker, allowed_objects
from heliport.core.utils.collections import RootedRDFGraph
from heliport.core.utils.context import Context
from heliport.core.utils.exceptions import AccessDenied, UserMessage
from heliport.core.utils.string_tools import format_byte_count, format_time


class DBSSHPath(models.Model):
    """
    Abstract base class (in the Django Model way) to contain common functionality for
    SSH file and SSH directory Django models.
    """  # noqa: D205

    path_str = models.TextField()
    login = models.ForeignKey(
        LoginInfo, on_delete=models.SET_NULL, null=True, blank=True
    )
    is_file = False
    is_directory = False

    class Meta:
        """class to specify additional options for this django model."""

        #: specify that this model is abstract (has no database table)
        abstract = True

    @property
    def path(self):
        """Access path as pathlib path object instead of string."""
        return Path(self.path_str)

    @path.setter
    def path(self, value: Path):
        """Path setter."""
        self.path_str = str(value)

    def access(self, context: Context):
        """Get permission checker for this object and context.

        See :meth:`heliport.core.models.DigitalObject.access` for base implementation.

        There is also a check that only gives write permission if the current user owns
        the login.
        """
        from heliport.core.permissions import LoginPermissionChecker

        return LoginPermissionChecker(self, context)


class DBSSHFile(DigitalObject, DBSSHPath, FileObj):
    """Django model to store SSH file references in the HELIPORT database."""

    file_id = models.AutoField(primary_key=True)
    is_file = True

    def as_file(self, context: Context) -> File:
        """
        This is what makes this class into a file digital object in HELIPORT.

        Constructs and returns a :class:`SSHFile` instance.
        """  # noqa: D401
        self.access(context).assert_read_permission()
        return SSHFile(self.login, self.path, context)


class DBSSHDirectory(DigitalObject, DBSSHPath, DirectoryObj):
    """Django model to store SSH directory references in the HELIPORT database."""

    directory_id = models.AutoField(primary_key=True)
    is_directory = True

    def as_directory(self, context: Context) -> Directory:
        """
        This is what makes this class into a directory digital object in HELIPORT.

        Constructs and returns a :class:`SSHDirectory` instance.
        """  # noqa: D401
        self.access(context).assert_read_permission()
        return SSHDirectory(self, Path(), context)

    def sub_path(self, relative_path: Union[Path, str], allow_hidden=False):
        """
        Helper function to get a pathlib path object that represents a path inside
        this directory.

        Includes a check if the path is really inside the directory and does not access
        hidden directories.
        """  # noqa: D205, D401
        if isinstance(relative_path, str):
            relative_path = Path(relative_path)

        if not allow_hidden and any(n.startswith(".") for n in relative_path.parts):
            raise AccessDenied("Access of hidden directory")

        sub_path = self.path / relative_path
        if self.path != sub_path and self.path not in sub_path.parents:
            raise AccessDenied("Access of parent directory not allowed")
        return sub_path


def create_new(context: Context, parameters: SSHFileStat):
    """Create new file or directory.

    Creates either :class:`DBSSHFile` or :class:`DBSSHDirectory`.
    The resource is accessed via ssh to determine which.
    """
    stat = context[parameters]
    if hasattr(stat, "st_mode") and S_ISDIR(stat.st_mode):
        result = DBSSHDirectory()
        result.category_str = settings.SSH_DIRECTORY_NAMESPACE
    else:
        result = DBSSHFile()
        result.category_str = settings.SSH_FILE_NAMESPACE
    return result


class SSHModule(DigitalObjectModule):
    """HELIPORT module that lists and manages ssh files and directories."""

    #: name in HELIPORT project graph
    name = "SSH Files/Directories"
    #: name in settings
    module_id = "ssh_files_directories"
    object_class = (DBSSHDirectory, DBSSHFile)
    icon = "fa-solid fa-server"

    @property
    def form_class(self):
        """Specify :class:`heliport.ssh.forms.SSHDataSourceForm` for this module."""
        from heliport.ssh.forms import SSHDataSourceForm

        return SSHDataSourceForm

    def get_url(self, project):
        """Url when user clicks on this in project graph."""
        return reverse("ssh:list", kwargs={"project": project.pk})


class SSHPathObj(GeneralDigitalObject, ABC):
    """
    Abstract base class for common functionality for ssh files and directories not
    stored in the heliport database.
    This mostly implements simple serialization functions and handles resolving the
    objects not stored in the database.
    """  # noqa: D205

    #: this class represents something inside this base directory
    directory: DBSSHDirectory
    #: the path of this object inside :attr:`directory`; this may be ../../a/secret.txt!
    relative_path: str

    def as_tuple(self):
        """To implement ``__hash__`` and ``__eq__`` easily."""
        return self.directory, self.relative_path

    def __hash__(self):
        """Hashable."""
        return hash(self.as_tuple())

    def __eq__(self, other):
        """Comparable."""  # noqa: D401
        if isinstance(other, SSHPathObj):
            return self.as_tuple() == other.as_tuple()
        return False

    def __init__(  # noqa: D107
        self, base_directory: DBSSHDirectory, relative_path: str
    ):
        self.directory = base_directory
        self.relative_path = relative_path

    @property
    def absolute_path(self):
        """Including checking if :attr:`relative_path` really is a sub path."""
        return self.directory.sub_path(self.relative_path)

    def get_identifying_params(self) -> Dict[str, str]:
        """Parameters to resolve this object from."""
        return {
            "type": self.type_id(),
            "base_directory": str(self.directory.directory_id),
            "path": self.relative_path,
        }

    @classmethod
    def resolve(cls, params: Dict, context: Context) -> Optional[GeneralDigitalObject]:
        """
        Get the object from its identifying parameters
        (see :meth:`get_identifying_params`).

        Make sure to return a :class:`heliport.core.models.DigitalObject` subclass
        instance if one already exists.
        """  # noqa: D205
        assert params.get("type") == cls.type_id()
        directory_id = params.get("base_directory")
        path = params.get("path")

        base = DBSSHDirectory.objects.filter(
            directory_id=directory_id, login__isnull=False
        ).first()
        if base is None:
            return None
        if not path:
            return base

        absolute_path = base.sub_path(path)
        sub_data_source = (
            allowed_objects(context.user_or_none, cls.related_db_class())
            .filter(path_str=str(absolute_path), login=base.login, deleted__isnull=True)
            .first()
        )
        if sub_data_source is not None:
            return sub_data_source

        return cls(base, path)

    def access(self, context: Context):
        """
        Define who can access how. No access if :meth:`DBSSHDirectory.sub_path` raises
        :class:`heliport.core.utils.exceptions.AccessDenied` otherwise normal access.
        See also :class:`heliport.core.permissions.PermissionChecker`.
        """  # noqa: D205
        try:
            self.directory.sub_path(self.relative_path)
            return PermissionChecker(self.directory, context)
        except AccessDenied:
            return PermissionChecker(None, context)

    def as_digital_object(self, context) -> DigitalObject:
        """
        Return a :class:`heliport.core.models.DigitalObject` instance representing the
        same file or directory; return an existing one or create a new one.
        """  # noqa: D205
        result = self.create_db_object()
        result.path = self.absolute_path
        result.login = self.directory.login
        result.owner = self.directory.owner
        result.label = str(self)
        result.save()

        result.projects.add(*self.directory.projects.all())
        result.co_owners.add(*self.directory.co_owners.all())
        assert_relation(self.directory, Vocabulary().has_part, result)
        return result

    @abstractmethod
    def create_db_object(self) -> DigitalObject:
        """Select django model and set category_str in subclass."""

    @staticmethod
    @abstractmethod
    def related_db_class():
        """Get the django model that stores the same information as this class."""

    def as_text(self) -> str:
        """
        Implements :class:`heliport.core.utils.serialization.GeneralValue` interface.

        Files need to be represented as string by their filename.
        """  # noqa: D401
        return Path(self.relative_path).name

    def as_rdf(self) -> RootedRDFGraph:
        """
        Represent just as string in rdf for now.
        In the future this should probably construct a node of type file with the string
        as filename.
        """  # noqa: D205
        return RootedRDFGraph.from_atomic(self.as_text())

    def as_html(self) -> str:
        """Represent in html just by the filename."""
        return self.as_text()


class SSHFileObj(SSHPathObj, FileObj):
    """Represent an SSH file not stored in the HELIPORT database."""

    @staticmethod
    def type_id() -> str:
        """String to represent this type when resolving an object of this type."""  # noqa: D401, E501
        return "SSHFile"

    @staticmethod
    def related_db_class():
        """:class:`DBSSHFile` stores the same information as this obj but in the DB."""
        return DBSSHFile

    def create_db_object(self) -> DigitalObject:
        """Create an :class:`DBSSHFile` instance and set the namespace for pid generation."""  # noqa: E501
        result = DBSSHFile()
        result.category_str = settings.SSH_FILE_NAMESPACE
        return result

    def as_file(self, context: Context) -> File:
        """
        This method is what makes this class into something that represents a file in
        HELIPORT. Checks permission and returns an :class:`SSHFile` instance.
        """  # noqa: D205, D401
        self.access(context).assert_read_permission()
        return SSHFile(self.directory.login, self.absolute_path, context)


class SSHDirectoryObj(SSHPathObj, DirectoryObj):
    """Represent an SSH directory not stored in the HELIPORT database."""

    @staticmethod
    def type_id() -> str:
        """String to represent this type when resolving an object of this type."""  # noqa: D401, E501
        return "SSHDirectory"

    @staticmethod
    def related_db_class():  # noqa: D102
        return DBSSHDirectory

    def create_db_object(self) -> DigitalObject:
        """Create an :class:`DBSSHDirectory` instance and set the namespace for pid generation."""  # noqa: E501
        result = DBSSHDirectory()
        result.category_str = settings.SSH_DIRECTORY_NAMESPACE
        return result

    def as_directory(self, context: Context) -> Directory:
        """
        This method is what makes this class into something that represents a directory
        in HELIPORT. Checks permission and returns an :class:`SSHDirectory` instance.
        """  # noqa: D205, D401
        self.access(context).assert_read_permission()
        return SSHDirectory(self.directory, Path(self.relative_path), context)


class SSHPath(FileOrDirectory, ABC):
    """
    Common functionality to represent actual ssh files and directories. See also
    HELIPORT docs on :doc:`/development/special-digital-objects`.
    """  # noqa: D205

    def __init__(self, context: Context):  # noqa: D107
        self.context = context

    @property
    @abstractmethod
    def login(self) -> LoginInfo:
        """The ssh login to access this path."""

    @property
    @abstractmethod
    def path(self) -> Path:
        """The absolute path under :attr:`login`."""

    def error(self, message):
        """
        Raise an error containing the message and additional infos about where the
        error was raised. The raised error message is intended to be shown to the user.
        """  # noqa: D205
        raise UserMessage(f'{message}: "{self.path}" at "{self.login.name}"')

    def connection(self):
        """Get or create an SSH connection."""
        return self.context[SSHConnection(self.login)]

    def stat(self):
        """Get or download the file stat file system metadata for this path."""
        return self.context[SSHFileStat(self.login, self.path)]


class SSHFile(SSHPath, File):
    """
    The actual SSHFile that can be used to get file contents. You should make sure the
    user has access before returning such a file.
    """  # noqa: D205

    def __init__(  # noqa: D107
        self, login: LoginInfo, absolute_path: Path, context: Context
    ):
        super().__init__(context)
        self._login = login
        self.absolute_path = absolute_path

        self.ssh_file = None

    @property
    def login(self) -> LoginInfo:
        """The ssh login that can be used to access the file."""
        return self._login

    @property
    def path(self) -> Path:
        """Path where this file is stored at the location specified by :attr:`login`."""
        return self.absolute_path

    def mimetype(self) -> str:
        """Guesses the mimetype based on file name."""
        mimetype, encoding = guess_type(self.path)
        if mimetype is None:
            mimetype = "application/octet-stream"  # just binary data
        return mimetype

    def read(self, number_of_bytes=None) -> bytes:
        """Get contents of the file; file needs to be :meth:`open`."""
        assert self.ssh_file is not None, "please open file before reading"
        return self.ssh_file.read(number_of_bytes)

    def open(self):
        """
        Establishes an ssh connection if none exists yet in the current context
        and opens the file. See also :meth:`close`, which needs to be called to free the
        resources again.

        Note that :class:`heliport.core.digital_object_aspects.File` implements also
        a context manager, so you can use a ``with`` block to automatically open and
        close the file.
        """  # noqa: D205, D401
        if self.ssh_file is not None:
            self.close()

        self.ssh_file = self.connection().file(str(self.path), "rb")

    def close(self):
        """Closes the file and frees resources; does nothing if the file is not open."""  # noqa: D401, E501
        if self.ssh_file is not None:
            self.ssh_file.close()
            self.ssh_file = None

    def get_file_info(self) -> Dict[str, str]:
        """Show size and last modified date to the user if available."""
        stat = self.stat()
        result = {}
        if hasattr(stat, "st_size"):
            result["size"] = format_byte_count(stat.st_size)
        if hasattr(stat, "st_mtime"):
            result["last modified"] = format_time(stat.st_mtime)
        return result

    def size(self) -> Optional[int]:
        """Get file size in bytes if available."""
        return getattr(self.stat(), "st_size", None)


class SSHDirectory(SSHPath, Directory):
    """
    The actual SSH directory that can be used to get directory contents.
    You should make sure the user has access before returning such a directory.
    """  # noqa: D205

    base_dir: DBSSHDirectory
    relative_path: Path

    def __init__(  # noqa: D107
        self, base_dir: DBSSHDirectory, relative_path: Path, context: Context
    ):
        super().__init__(context)
        self.base_dir = base_dir
        self.relative_path = relative_path

    @property
    def path(self):
        """
        Get pathlib path; checks if :attr:`relative_path` is really inside this
        directory. See also :meth:`DBSSHDirectory.sub_path` that is used for
        implementation.
        """  # noqa: D205
        return self.base_dir.sub_path(self.relative_path)

    @property
    def login(self) -> LoginInfo:
        """The ssh login to access this directory."""
        return self.base_dir.login

    def get_parts(self) -> Iterator[Union[FileObj, DirectoryObj]]:
        """
        This is the most important method of a directory (similar to ``file.read()``).
        Yield all the file or directory objects in this directory as
        :class:`SSHDirectoryObj` or :class:`SSHFileObj`. Omits hidden files/directories.
        """  # noqa: D205, D401
        stat = self.stat()
        if not hasattr(stat, "st_mode"):
            self.error("Invalid File")
        if not S_ISDIR(stat.st_mode):
            self.error("Not a Directory")

        for sub in self.connection().list_dir(str(self.path)):
            if sub.filename.startswith("."):
                continue
            relative_sub_path = self.relative_path / sub.filename
            absolute_sub_path = self.path / sub.filename
            self.context.cache(SSHFileStat(self.login, absolute_sub_path), sub)
            if hasattr(sub, "st_mode") and S_ISDIR(sub.st_mode):
                yield SSHDirectoryObj(self.base_dir, str(relative_sub_path))
            else:
                yield SSHFileObj(self.base_dir, str(relative_sub_path))

    def get_file_info(self) -> Dict[str, str]:
        """No extra information for the user implemented right now."""
        return {}


@dataclass(frozen=True)
class SSHConnection:
    """
    Immutable dataclass to describe an SSH connection.
    Implements the :class:`heliport.core.utils.context.Description` protocol.

    Note that the SSH connection is user specific since the login is user specific.
    This reduces the chance that somebody can use the connection of someone else on top
    of the fact that the context is specific for a request (and therefore user) anyway.
    """  # noqa: D205

    #: SSH login for the connection
    login: LoginInfo

    def generate(self, context):
        """
        Build a :class:`heliport.core.utils.command_line_execution.SSHCommandExecutor`
        as connection.

        :raises UserMessage: if some error occurs.
        """  # noqa: D205
        connection = self.login.build_command_executor()
        message = connection.connect_error_message(
            f' - try reconnecting the "{self.login.name}" ssh connection'
        )
        if message is not None:
            connection.close()
            raise UserMessage(message)
        return connection


@dataclass(frozen=True)
class SSHFileStat:
    """
    Immutable dataclass to describe an SSH filesystem metadata for caching.
    Implements the :class:`heliport.core.utils.context.Description` protocol.

    Note that this is user specific since the login is user specific.
    This reduces the chance that somebody can use the connection of someone else on top
    of the fact that the context is specific for a request (and therefore user) anyway.
    """  # noqa: D205

    #: ssh login to get this stat
    login: LoginInfo
    #: path of the file for which to get the stat metadata for
    path: Path

    def generate(self, context):
        """Download stat of file or directory; reuses existing :class:`SSHConnection`."""  # noqa: E501
        connection = context[SSHConnection(self.login)]
        try:
            file = connection.file(str(self.path))
        except FileNotFoundError:
            raise UserMessage("The specified path does not exist") from None
        return file.stat()
