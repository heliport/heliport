# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Django forms of this app."""

from typing import Union

from django import forms
from django.core.exceptions import ValidationError

from heliport.core.forms import LoginSelectWidget
from heliport.core.models import LoginInfo
from heliport.core.utils.context import Context
from heliport.core.utils.exceptions import AccessDenied
from heliport.ssh.models import DBSSHDirectory, DBSSHFile, SSHFileStat, create_new


class SSHDataSourceForm(forms.Form):
    """Form for creating SSH data sources.

    This is used in the :mod:`heliport.data_source.views.DataSourceView` to create ssh
    files and directories directly.
    """

    template_name = "core/base/form.html"

    name = forms.CharField(
        widget=forms.TextInput(attrs={"class": "form-control"}), required=True
    )
    path = forms.CharField(
        widget=forms.TextInput(attrs={"class": "form-control"}), required=True
    )
    login = forms.ModelChoiceField(
        queryset=LoginInfo.objects.none(),
        widget=LoginSelectWidget(attrs={"class": "form-select"}),
        required=True,
        empty_label="Choose a Login",
    )
    description = forms.CharField(
        widget=forms.Textarea(attrs={"class": "form-control", "rows": 5}),
        required=False,
    )

    def __init__(
        self,
        *args,
        context: Context,
        instance: Union[DBSSHFile, DBSSHDirectory] = None,
        **kwargs,
    ):
        """Initialize."""
        if instance:
            initial = kwargs.get("initial", {})
            initial["name"] = instance.label
            initial["path"] = instance.path
            initial["login"] = instance.login
            initial["description"] = instance.description
            kwargs["initial"] = initial

        super().__init__(*args, **kwargs)
        self.context = context
        self.instance = instance
        self.fields["login"].queryset = self.context.user.logininfo_set.filter(
            type=LoginInfo.LoginTypes.SSH
        )
        self.label_suffix = ""

    def clean_path(self):
        """Make sure path is absolute."""
        if self.cleaned_data["path"].startswith("~"):
            return ValidationError('"~" is not supported', code="~_in_path")
        if not (
            self.cleaned_data["path"].startswith("/")
            or self.cleaned_data["path"].startswith("\\")
        ):
            return ValidationError(
                'Use absolute paths that start with "/"', code="relative_path"
            )
        return self.cleaned_data["path"]

    def save(self):
        """Handle saving.

        Either creating a new or updating an existing SSH file or directory.
        """
        if self.instance is None:
            instance = self.create_new()
            self.set_parameters(instance)
            instance.save_with_namespace(instance.namespace_str, self.context.project)
        else:
            self.instance.access(self.context).assert_permission()
            self.set_parameters(self.instance)
            self.instance.save()

    def set_parameters(self, instance: Union[DBSSHFile, DBSSHDirectory]):
        """Update parameters at the given instance form cleaned form data."""
        instance.label = self.cleaned_data["name"]
        instance.path_str = self.cleaned_data["path"]
        instance.login = self.get_login()
        instance.description = self.cleaned_data["description"]

    def get_login(self):
        """Get login from post data."""
        login = self.cleaned_data["login"]
        if self.context.user != login.user:
            raise AccessDenied("Login not found for user.")
        return login

    def create_new(self) -> Union[DBSSHFile, DBSSHDirectory]:
        """Create new file or directory in database and set namespace for pid."""
        return create_new(
            self.context, SSHFileStat(self.get_login(), self.cleaned_data["path"])
        )
