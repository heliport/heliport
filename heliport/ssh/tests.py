# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

from django.urls import reverse

from heliport.core.models import LoginInfo
from heliport.core.tests import WithProject
from heliport.ssh.interface import get_search_url
from heliport.ssh.models import DBSSHDirectory, DBSSHFile


class SSHTests(WithProject):  # noqa: D101
    def test_list(self):  # noqa: D102
        file = DBSSHFile.objects.create()
        directory = DBSSHDirectory.objects.create()
        file.projects.add(self.project)
        directory.projects.add(self.project)

        response = self.client.get(
            reverse("ssh:list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(200, response.status_code)
        test_file = DBSSHFile.objects.get(pk=file.pk)
        test_directory = DBSSHDirectory.objects.get(pk=directory.pk)
        self.assertEqual(file, test_file)
        self.assertEqual(directory, test_directory)
        self.assertIn(response.context_data["project"], file.projects.all())
        self.assertIn(file, response.context_data["paths"])
        self.assertIn(directory, response.context_data["paths"])

        response = self.client.get(
            reverse(
                "ssh:update",
                kwargs={"project": self.project.pk, "pk": file.pk, "type": "files"},
            )
        )
        self.assertEqual(200, response.status_code)
        test_obj = DBSSHFile.objects.get(pk=file.pk)
        self.assertEqual(file, test_obj)
        self.assertIn(response.context_data["project"], test_obj.projects.all())
        self.assertIn(file, response.context_data["paths"])
        self.assertEqual(file, response.context_data["instance"])

        response = self.client.get(
            reverse(
                "ssh:update",
                kwargs={
                    "project": self.project.pk,
                    "pk": directory.pk,
                    "type": "directories",
                },
            )
        )
        self.assertEqual(200, response.status_code)
        self.assertIn(directory, response.context_data["paths"])
        self.assertEqual(directory, response.context_data["instance"])

    def test_update(self):  # noqa: D102
        file, is_new = DBSSHFile.objects.get_or_create(label="test_file456")
        file.category_str = "abc"
        file.save()
        file.projects.add(self.project)
        login = LoginInfo.objects.create(user=self.user)
        response = self.client.post(
            reverse(
                "ssh:update",
                kwargs={
                    "project": self.project.pk,
                    "pk": file.pk,
                    "type": "files",
                },
            ),
            data={
                "label": "test_file789",
                "description": "some file",
                "path": "abc",
                "login": login.pk,
            },
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        file = DBSSHFile.objects.get(pk=file.pk)
        self.assertIn(response.context_data["project"], file.projects.all())
        self.assertEqual("test_file789", file.label)
        self.assertEqual("some file", file.description)
        self.assertEqual("abc", file.path_str)

    def test_delete(self):  # noqa: D102
        directory = DBSSHDirectory.objects.create()
        directory.projects.add(self.project)
        response = self.client.post(
            reverse("ssh:list", kwargs={"project": self.project.pk}),
            data={"delete": directory.digital_object_id},
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        test_obj = DBSSHDirectory.objects.filter(
            pk=directory.pk, deleted__isnull=True
        ).first()
        self.assertIsNone(test_obj)


class SearchAndAPITest(WithProject):
    """Test search including via API."""

    def test_access_search(self):
        """Test that search URL is accessible."""
        response = self.client.get(get_search_url())
        self.assertEqual(200, response.status_code)

    def test_findable(self):
        """Test that files and directories are findable."""
        file = DBSSHFile.objects.create(label="FILE321")
        directory = DBSSHFile.objects.create(label="DIRECTORY321")
        file.projects.add(self.project)
        directory.projects.add(self.project)
        response = self.client.get(f"{reverse('ssh:search')}?q=321")
        self.assertContains(response, "FILE321")
        self.assertContains(response, "DIRECTORY321")

    def test_api(self):
        """Test api."""
        file = DBSSHFile.objects.create(label="FILE654")
        directory = DBSSHDirectory.objects.create(label="DIRECTORY654")
        file.projects.add(self.project)
        directory.projects.add(self.project)
        response = self.client.get("/api/files/")
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "FILE654")
        response = self.client.get("/api/directories/")
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "DIRECTORY654")
