# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Registers Django admin pages.

See :class:`django.contrib.admin.ModelAdmin` from Django documentation.
"""

from django.contrib import admin

from .models import DBSSHDirectory, DBSSHFile

admin.site.register(DBSSHFile)
admin.site.register(DBSSHDirectory)
