# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path
from rest_framework import routers

from .views import (
    DirectoryViewSet,
    FileViewSet,
    MigrateDataSourceView,
    SearchView,
    SSHListView,
)

app_name = "ssh"
urlpatterns = [
    path("project/<int:project>/ssh/list/", SSHListView.as_view(), name="list"),
    path(
        "project/<int:project>/ssh/<str:type>/<int:pk>/update/",
        SSHListView.as_view(),
        name="update",
    ),
    path("ssh/search/", SearchView.as_view(), name="search"),
    path(
        "project/<int:project>/ssh/migrate/",
        MigrateDataSourceView.as_view(),
        name="migrate",
    ),
]

# REST API
router = routers.DefaultRouter()
router.register(r"files", FileViewSet, basename="files")
router.register(r"directories", DirectoryViewSet, basename="directories")
