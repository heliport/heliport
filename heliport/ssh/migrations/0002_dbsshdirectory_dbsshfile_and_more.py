# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Generated by Django 4.1.4 on 2023-02-07 12:52

import django.db.models.deletion
from django.db import migrations, models

import heliport.core.digital_object_aspects


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0021_digitalobject_is_helper"),
        ("ssh", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="DBSSHDirectory",
            fields=[
                (
                    "digitalobject_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        to="core.digitalobject",
                    ),
                ),
                ("path_str", models.TextField()),
                ("directory_id", models.AutoField(primary_key=True, serialize=False)),
                (
                    "login",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="core.logininfo",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=(
                "core.digitalobject",
                models.Model,
                heliport.core.digital_object_aspects.DirectoryObj,
            ),
        ),
        migrations.CreateModel(
            name="DBSSHFile",
            fields=[
                (
                    "digitalobject_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        to="core.digitalobject",
                    ),
                ),
                ("path_str", models.TextField()),
                ("file_id", models.AutoField(primary_key=True, serialize=False)),
                (
                    "login",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="core.logininfo",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=(
                "core.digitalobject",
                models.Model,
                heliport.core.digital_object_aspects.FileObj,
            ),
        ),
        migrations.RemoveField(
            model_name="sshfile",
            name="digitalobject_ptr",
        ),
        migrations.RemoveField(
            model_name="sshfile",
            name="generated_inside",
        ),
        migrations.RemoveField(
            model_name="sshfile",
            name="login",
        ),
        migrations.DeleteModel(
            name="SSHDirectory",
        ),
        migrations.DeleteModel(
            name="SSHFile",
        ),
    ]
