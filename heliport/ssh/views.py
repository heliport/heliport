# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

from pathlib import Path
from stat import S_ISDIR
from typing import Union

import django_filters
from django.conf import settings
from django.contrib import messages
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect
from django.urls import NoReverseMatch, reverse
from django.utils import timezone
from django.views.generic import DetailView, TemplateView
from rest_framework import filters as rest_filters
from rest_framework.decorators import action

from heliport.core.digital_object_actions import Action
from heliport.core.digital_object_resolution import resolve, url_for
from heliport.core.mixins import HeliportLoginRequiredMixin, HeliportProjectMixin
from heliport.core.models import (
    DigitalObject,
    DigitalObjectAttributes,
    DigitalObjectRelation,
    LoginInfo,
    Project,
    Vocabulary,
    assert_relation,
)
from heliport.core.permissions import is_object_member
from heliport.core.utils.context import Context
from heliport.core.utils.exceptions import UserMessage
from heliport.core.utils.queries import delete_by_id
from heliport.core.views import HeliportModelViewSet

from ..core.utils.http import (
    directory_api_response,
    directory_from_description_api_response,
    file_api_response,
    file_from_description_api_response,
)
from .models import DBSSHDirectory, DBSSHFile, SSHFileObj, SSHFileStat, create_new
from .serializers import DirectorySerializer, FileSerializer


class SSHListView(HeliportProjectMixin, DetailView):
    """List ssh files and directories and allow creation and edit."""

    model = Project
    pk_url_kwarg = "project"
    template_name = "ssh/list.html"
    directory_type = "directories"

    def get_context_data(self, **kwargs):
        """Get files and directories for rendering as well as the instance for edit."""
        context = super().get_context_data(**kwargs)
        project = self.object
        user = self.request.user.heliportuser

        files = DBSSHFile.objects.filter(projects=project, deleted__isnull=True)
        dirs = DBSSHDirectory.objects.filter(projects=project, deleted__isnull=True)
        context["paths"] = list(dirs) + list(files)
        context["helper_exists"] = any(p.is_helper for p in context["paths"])
        context["actions"] = [obj.actions(user, project) for obj in context["paths"]]
        context["logins"] = LoginInfo.objects.filter(
            user=user, type=LoginInfo.LoginTypes.SSH
        )
        context["update"] = "pk" in self.kwargs
        context["directory_type"] = self.directory_type

        if "pk" in self.kwargs:
            if self.kwargs.get("type") == self.directory_type:
                context["instance"] = get_object_or_404(
                    DBSSHDirectory, directory_id=self.kwargs["pk"]
                )
            else:
                context["instance"] = get_object_or_404(
                    DBSSHFile, file_id=self.kwargs["pk"]
                )

        return context

    def post(self, *args, **kwargs):
        """Handle post request, called by Django; handle delete directly."""
        delete = self.request.POST.get("delete")
        if delete:
            delete_by_id(delete, user=self.request.user.heliportuser)
        else:
            self.handle_update()
        return redirect("ssh:list", project=self.kwargs["project"])

    def get_login(self):
        """Get login from post data."""
        return get_object_or_404(LoginInfo, pk=self.request.POST["login"])

    def handle_update(self):
        """Get or create instance and set attributes."""
        project = self.get_object()
        user = self.request.user.heliportuser
        try:
            with Context(user=user, project=project) as context:
                if "pk" in self.kwargs:
                    instance = self.get_instance()
                    instance.access(context).assert_permission()
                else:
                    instance = self.create_new(context)

                self.set_attributes(instance)
        except UserMessage as e:
            messages.error(self.request, e.message)

    def create_new(self, context) -> Union[DBSSHFile, DBSSHDirectory]:
        """Create new file or directory in database and set namespace for pid."""
        return create_new(
            context, SSHFileStat(self.get_login(), self.request.POST["path"])
        )

    def get_instance(self):
        """Get existing file or directory from database."""
        if self.kwargs.get("type") == self.directory_type:
            return get_object_or_404(DBSSHDirectory, directory_id=self.kwargs["pk"])
        return get_object_or_404(DBSSHFile, file_id=self.kwargs["pk"])

    def set_attributes(self, instance):
        """Set parameters from post request on file or directory."""
        instance.path_str = self.request.POST["path"]
        instance.login = self.get_login()
        instance.label = self.request.POST.get("label")
        instance.description = self.request.POST.get("description")
        instance.save()
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        instance.projects.add(project)


class MigrateDataSourceView(HeliportProjectMixin, DetailView):
    """
    View for migrating old :class:`heliport.data_source.models.DataSource` to this app.
    This is used for :class:`MigrateSSHDatasource` action.
    """  # noqa: D205

    model = Project
    pk_url_kwarg = "project"

    def get(self, request, *args, **kwargs):
        """Handle get request; setup context for :meth:`do_migration`."""
        project = self.get_object()
        user = None
        if hasattr(request.user, "heliportuser"):
            user = request.user.heliportuser
        with Context(user, project) as context:
            try:
                obj = resolve(self.request.GET, context)
                self.do_migration(obj, context)
            except UserMessage as e:
                messages.error(self.request, f"Migration Failed: {e.message}")

        try:
            return redirect("data_source:list", project=project.pk)
        except NoReverseMatch:
            return redirect("ssh:list", project=project.pk)

    def do_migration(self, obj, context):
        """
        Creates a new SSH file and directory by connecting via ssh and checking
        if the path is a file or a directory. Raises a
        :class:`heliport.core.utils.exceptions.UserMessage` if obj is not a valid SSH
        file or directory. Marks the old obj as deleted.
        """  # noqa: D205, D401
        if not isinstance(obj, DigitalObject):
            raise UserMessage("Incorrect Type")

        try:
            path = obj.path
            login = obj.login
            protocol = obj.protocol
        except AttributeError:
            raise UserMessage("Incorrect Type") from None

        if protocol != "ssh":
            raise UserMessage("Not an SSH data source")

        if not isinstance(path, str) or not isinstance(login, LoginInfo):
            raise UserMessage("Incorrect attribute Type")

        stat = context[SSHFileStat(login, Path(path))]
        if hasattr(stat, "st_mode") and S_ISDIR(stat.st_mode):
            new = DBSSHDirectory()
            new.category_str = settings.SSH_DIRECTORY_NAMESPACE
        else:
            new = DBSSHFile(path_str=path, login=login)
            new.category_str = settings.SSH_FILE_NAMESPACE

        new.path_str = path
        new.login = login
        new.label = obj.label
        new.description = obj.description
        new.owner = obj.owner
        new.is_helper = bool(obj.generated_inside)
        new.save()
        new.projects.add(*obj.projects.all())
        new.co_owners.add(*obj.co_owners.all())
        for rel in DigitalObjectRelation.objects.filter(subject=obj):
            DigitalObjectRelation.objects.create(
                subject=new,
                predicate=rel.predicate,
                object=rel.object,
                is_public=rel.is_public,
            )
        for attr in DigitalObjectAttributes.objects.filter(subject=obj):
            DigitalObjectAttributes.objects.create(
                subject=new,
                predicate=attr.predicate,
                value=attr.value,
                is_public=attr.is_public,
            )
        vocab = Vocabulary()
        assert_relation(new, vocab.is_version_of, obj)

        obj.deleted = timezone.now()
        obj.save()

        messages.success(self.request, "Migration Successful")


class MigrateSSHDatasource(Action):
    """Action to migrate old ssh data sources to this app."""

    name = "Migrate"
    icon = "fa-wrench"

    @property
    def link(self) -> str:
        """Construct url for :class:`MigrateDataSourceView`."""
        return url_for("ssh:migrate", self.obj, project=self.project.pk)

    @property
    def applicable(self) -> bool:
        """Applicable on digital objects having path, login and ``protocol="ssh"``."""
        return (
            isinstance(self.obj, DigitalObject)
            and self.obj.deleted is None
            and hasattr(self.obj, "path")
            and isinstance(self.obj.path, str)
            and hasattr(self.obj, "login")
            and isinstance(self.obj.login, LoginInfo)
            and hasattr(self.obj, "protocol")
            and self.obj.protocol == "ssh"
        )


class SearchView(HeliportLoginRequiredMixin, TemplateView):
    """search SSH files and directories; render results."""

    template_name = "ssh/search.html"

    def get_context_data(self, **kwargs):
        """Called by django ``TemplateView``; search files and directories."""  # noqa: D401, E501
        context = super().get_context_data(**kwargs)
        query = self.request.GET.get("q", "")

        file_results = set()
        directory_results = set()

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            file_results.update(
                DBSSHFile.objects.filter(
                    Q(deleted__isnull=True)
                    & (
                        Q(file_id=num)
                        | Q(description__icontains=word)
                        | Q(label__icontains=word)
                        | Q(attributes__value__icontains=word)
                    )
                )
            )
            directory_results.update(
                DBSSHDirectory.objects.filter(
                    Q(deleted__isnull=True)
                    & (
                        Q(directory_id=num)
                        | Q(description__icontains=word)
                        | Q(label__icontains=word)
                        | Q(attributes__value__icontains=word)
                    )
                )
            )

        context["file_results"] = file_results
        context["directory_results"] = directory_results
        return context


##########################################
#               REST API                 #
##########################################


class FileViewSet(HeliportModelViewSet):
    """SSH File.

    File contents is available for download at /api/files/<file_id>/download
    """

    serializer_class = FileSerializer
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    filterset_fields = ["file_id", "persistent_id"]
    search_fields = ["attributes__value"]

    def get_queryset(self):
        """Get users non-deleted SSH files."""
        user = self.request.user.heliportuser
        return DBSSHFile.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()

    @action(detail=True, methods=["get"])
    def download(self, request, pk=None):
        """Get streaming file response to download file contents."""
        db_object = self.get_object()
        return file_api_response(request, db_object, db_object.path.name)

    @action(detail=False, methods=["get"])
    def download_from_description(self, request):
        """Get streaming file response to download file contents.

        The file to download is specified by request parameters.
        This enables downloading files not directly stored in heliport e.g. inside
        a directory.
        """

        def get_file_name(file_obj):
            if isinstance(file_obj, DBSSHFile):
                return str(file_obj.path.name)
            if isinstance(file_obj, SSHFileObj):
                return str(file_obj.absolute_path.name)

            raise UserMessage(
                f"{file_obj} is a {type(file_obj).__name__} and not an SSH file"
            )

        return file_from_description_api_response(request, get_file_name)


class DirectoryViewSet(HeliportModelViewSet):
    """SSH Directory.

    Files in directory are available at /api/directories/<directory_id>/contents
    """

    serializer_class = DirectorySerializer
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    filterset_fields = ["directory_id", "persistent_id"]
    search_fields = ["attributes__value"]

    def get_queryset(self):
        """Get users non-deleted SSH directories."""
        user = self.request.user.heliportuser
        return DBSSHDirectory.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()

    @action(detail=True, methods=["get"])
    def contents(self, request, pk=None):
        """Get files and directories in directory."""
        db_object = self.get_object()
        file_url = request.build_absolute_uri(
            reverse("files-download-from-description")
        )
        directory_url = request.build_absolute_uri(
            reverse("directories-contents-from-description")
        )

        return directory_api_response(request, db_object, file_url, directory_url)

    @action(detail=False, methods=["get"])
    def contents_from_description(self, request):
        """Get files and directories in directory.

        directory is specified via describing parameters to allow download of
        subdirectories
        """
        file_url = request.build_absolute_uri(
            reverse("files-download-from-description")
        )
        directory_url = request.build_absolute_uri(
            reverse("directories-contents-from-description")
        )
        return directory_from_description_api_response(request, file_url, directory_url)
