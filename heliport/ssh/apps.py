# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Django app configuration.

Some HELIPORT hooks can be registered in app config :meth:`django.apps.AppConfig.ready`.

See also :ref:`Django documentation <django:ref/applications:applications>`
"""

from django.apps import AppConfig

from heliport.core.app_interaction import heliport_modules
from heliport.core.digital_object_actions import actions
from heliport.core.digital_object_resolution import object_types


class SshConfig(AppConfig):
    """App configuration for ssh app."""

    name = "heliport.ssh"

    def ready(self):
        """Register object types and actions, and import settings."""
        from .models import SSHDirectoryObj, SSHFileObj, SSHModule
        from .views import MigrateSSHDatasource

        object_types.register(SSHFileObj)
        object_types.register(SSHDirectoryObj)
        actions.register(MigrateSSHDatasource)
        heliport_modules.register(SSHModule)
