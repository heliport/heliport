# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""This module is for serialization into Datacite, RDF and JSON.

The JSON format is used for API endpoints via
`Django rest framework <https://www.django-rest-framework.org/>`_. Using this, it is
typical to put the serializer classes into a "serializers.py" file.

For the serialization into RDF, attributes are described.
See :class:`heliport.core.attribute_description.BaseAttribute` for more detail.
"""

from django.conf import settings
from rest_framework import serializers

from heliport.core.models import LoginInfo
from heliport.core.permissions import projects_of
from heliport.core.serializers import NamespaceField
from heliport.ssh.models import DBSSHDirectory, DBSSHFile


class FileSerializer(serializers.ModelSerializer):
    """Django rest framework serializer for SSH files."""

    login = serializers.PrimaryKeyRelatedField(queryset=LoginInfo.objects.none())
    namespace_str = NamespaceField(settings.SSH_FILE_NAMESPACE)

    def __init__(self, *args, **kwargs):
        """Override querysets to only allow current user's objects."""
        super().__init__(*args, **kwargs)
        user = self.context["request"].user.heliportuser
        self.fields["login"].queryset = LoginInfo.objects.filter(user=user)
        self.fields["projects"].child_relation.queryset = projects_of(user)

    class Meta:
        """Description of the serializer."""

        model = DBSSHFile
        fields = [
            "file_id",
            "digital_object_id",
            "path_str",
            "description",
            "label",
            "login",
            "projects",
            "persistent_id",
            "namespace_str",
        ]


class DirectorySerializer(serializers.ModelSerializer):
    """Django rest framework serializer for SSH directories."""

    login = serializers.PrimaryKeyRelatedField(queryset=LoginInfo.objects.none())
    namespace_str = NamespaceField(settings.SSH_DIRECTORY_NAMESPACE)

    def __init__(self, *args, **kwargs):
        """Override querysets to only allow current user's objects."""
        super().__init__(*args, **kwargs)
        user = self.context["request"].user.heliportuser
        self.fields["login"].queryset = LoginInfo.objects.filter(user=user)
        self.fields["projects"].queryset = projects_of(user)

    class Meta:
        """Description of the serializer."""

        model = DBSSHDirectory
        fields = [
            "directory_id",
            "digital_object_id",
            "path_str",
            "description",
            "label",
            "login",
            "projects",
            "persistent_id",
            "namespace_str",
        ]
