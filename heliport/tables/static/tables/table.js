/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { Expression } from "./expressions.js";

function icon(symbol) {
  return $(`<i class="fa-solid fa-${symbol}"></i>`);
}

function icon_button(symbol, text) {
  let the_icon = icon(symbol);
  let icon_div = $(`<div></div>`);
  icon_div.append(the_icon);
  icon_div.prop("title", text);
  icon_div.addClass("icon-button");
  return icon_div;
}

class Column {
  constructor(parent_table, dict) {
    this.name = "";
    this.type = column_type_text;
    this.column_id = dict.column_id;

    this.parent_table = parent_table;

    this.container = $(`<th></th>`);
    this.label = $(`<span></span>`);
    this.position_select_option = $(`<option></option>`).attr(
      "value",
      this.column_id,
    );
    this.edit_button = icon_button("pencil", "Edit");
    this.edit_button.on("click", () => this.parent_table.update_column(this));
    this.delete_button = icon_button("trash", "Delete");
    this.delete_button.on("click", () => this.delete());
    this.container.append(this.label);
    this.container.append(this.edit_button);
    this.container.append(this.delete_button);

    this.dict = dict;
  }

  get dict() {
    return {
      label: this.name,
      column_id: this.column_id,
    };
  }

  set dict(value) {
    if (this.column_id !== value.column_id) {
      console.log("ID of column changed!");
    }

    this.name = value.label;
    this.type = value.column_type.toString();
    this.column_id = value.column_id;

    this.label.text(this.name);
    let new_text = `Behind ${this.name}`;
    if (this.position_select_option.text() !== new_text)
      this.position_select_option.text(new_text);
  }

  remove() {
    this.container.remove();
    this.position_select_option.remove();
  }

  delete() {
    if (confirm("Deleting the column including all cells.")) {
      $.post(column_delete_url, {
        column_id: this.column_id,
        csrfmiddlewaretoken: csrf_middleware_token,
      }).done((data) => this.parent_table.update());
    }
  }

  prepend_position_select_option(dom_element) {
    if (dom_element.prev()[0] !== this.position_select_option[0]) {
      dom_element.before(this.position_select_option);
    }
    return this.position_select_option;
  }
}

class Cell {
  constructor(column, dict = null) {
    this.cell_id = null;
    this._contents = "";

    this.original_contents = "";
    this.column = column;
    this._edit_mode = false;
    this._special_content_shown = false;
    this.image_src = null;
    this.image_error = false;
    this.image_uploading = true;
    this._image_label = "";
    this.evaluated_contents = "";

    this.container = $(`<td></td>`);
    this.special_content_row = $(`<tr style="display: none;"></tr>`);
    this.special_content = $(`<td colspan=100 class="text-center"></td>`);
    this.image = $(`<img style="max-height: 40vh;" loading="lazy"/>`);
    this.image.slideUp("fast");
    this.uploading_spinner = $(
      `<div class="spinner-border text-success" style="width: 1.5em; height: 1.5em;"><span class="visually-hidden"></span></div>`,
    );
    this.error_message = $(`<span>ERROR</span>`);
    this.hide_button = icon_button("eye", "hide image");
    this.hide_button.hide();
    this.hide_button.on("click", () => (this.special_content_shown = false));
    this.show_button = icon_button("eye-slash", "show image");
    this.show_button.hide();
    this.show_button.on("click", () => (this.special_content_shown = true));
    this.image_add_button = icon_button("file-image", "add image");
    this.image_add_button.hide();
    this.image_add_button.on("click", () => this.add_image());
    this.image_add_later_message = $(`<div>Add later</div>`);
    this.image_add_later_message.hide();
    this.image_delete_button = icon_button("trash", "delete image");
    this.image_delete_button.hide();
    this.image_delete_button.on("click", () => this.delete_image());
    this.evaluated_content_span = $(`<span></span>`);
    this.evaluated_content_span.hide();
    this.content_span = $(`<span></span>`);
    this.content_edit = $(`<input/>`);
    this.content_edit.on(
      "input",
      () => (this.contents = this.content_edit.val()),
    );
    this.content_edit.hide();
    this.content_edit.addClass("form-control");
    this.container.append(this.content_span);
    this.container.append(this.evaluated_content_span);
    this.container.append(this.content_edit);
    this.container.append(this.show_button);
    this.container.append(this.hide_button);
    this.container.append(this.image_add_button);
    this.container.append(this.image_delete_button);
    this.container.append(this.image_add_later_message);
    this.special_content_row.append(this.special_content);
    this.special_content.append(this.image);
    this.special_content.append(this.uploading_spinner);
    this.special_content.append(this.error_message);

    if (dict !== null) this.dict = dict;
  }

  get contents() {
    return this._contents;
  }

  set contents(value) {
    this._contents = value;
    this.content_edit.val(value);
    this.content_span.text(value);
  }

  get modified() {
    return this.original_contents !== this.contents;
  }

  get dict() {
    return {
      contents: this.contents,
      evaluated_contents: "",
      cell_id: this.cell_id,
      column_id: this.column.column_id,
      modified: this.modified,
    };
  }

  set dict(value) {
    if (this.cell_id !== null && this.cell_id !== value.cell_id)
      console.log("ID of cell changed!");

    this.cell_id = value.cell_id;
    if (!this.edit_mode || !this.modified) {
      this.contents = value.contents;
      this.original_contents = value.contents;
    }

    if (this.column.type === column_type_image) {
      this.image_src = value.image_src;
      this.image_label = value.image_label;
      this.image_uploading = value.image_uploading;
      this.image_error = value.image_error;
      this.image_message = value.image_message;
      this.evaluated_contents = "";
    } else {
      this.image_src = null;
      this.image_label = null;
      this.image_uploading = true;
      this.image_error = false;
      this.image_message = "ERROR";
      this.evaluated_contents = value.evaluated_contents;
    }

    this.update_ui();
  }

  update_ui() {
    if (this.column.type === column_type_timestamp) {
      this.content_edit.hide();
      this.content_span.show();
      this.special_content_row.hide();
      this.show_button.hide();
      this.hide_button.hide();
      this.image_add_button.hide();
      this.image_delete_button.hide();
      this.image_add_later_message.hide();
      this.image.attr("src", "");
      this.evaluated_content_span.attr("title", "");
      this.evaluated_content_span.text("");
      this.evaluated_content_span.hide();
    } else if (this.column.type === column_type_image) {
      this.evaluated_content_span.attr("title", "");
      this.evaluated_content_span.text("");
      this.evaluated_content_span.hide();

      this.content_edit.hide();
      this.content_span.hide();

      if (this.edit_mode && this.image_src === null)
        this.image_add_later_message.show();
      else this.image_add_later_message.hide();

      if (this.special_content_shown && this.image_src !== null) {
        this.special_content_row.show();
        this.hide_button.show();
        this.show_button.hide();
      } else {
        this.special_content_row.hide();
        this.hide_button.hide();
        if (this.image_src !== null) this.show_button.show();
        else this.show_button.hide();
      }
      if (this.image_src === null && !this.edit_mode)
        this.image_add_button.show();
      else this.image_add_button.hide();
      if (this.image_src !== null && !this.edit_mode)
        this.image_delete_button.show();
      else this.image_delete_button.hide();

      this.image.attr(
        "src",
        this.image_src === null || this.image_uploading || this.image_error
          ? ""
          : this.image_src,
      );
      if (this.image_uploading) this.uploading_spinner.show();
      else this.uploading_spinner.hide();
      if (this.image_error) this.error_message.show();
      else this.error_message.hide();
    } else if (this.column.type === column_type_number) {
      if (this.edit_mode) {
        this.content_edit.show();
        this.content_span.hide();
        this.evaluated_content_span.attr("title", "");
        this.evaluated_content_span.text("");
        this.evaluated_content_span.hide();
      } else {
        this.content_edit.hide();
        this.content_span.hide();
        this.evaluated_content_span.attr("title", this.contents);
        this.evaluated_content_span.text(this.evaluated_contents);
        this.evaluated_content_span.show();
      }

      this.special_content_row.hide();
      this.image_add_button.hide();
      this.image_delete_button.hide();
      this.image_add_later_message.hide();
      this.image.attr("src", "");
    } else {
      // default column_type_text
      if (this.edit_mode) {
        this.content_edit.show();
        this.content_span.hide();
      } else {
        this.content_edit.hide();
        this.content_span.show();
      }
      this.special_content_row.hide();
      this.image_add_button.hide();
      this.image_delete_button.hide();
      this.image_add_later_message.hide();
      this.image.attr("src", "");

      this.evaluated_content_span.attr("title", "");
      this.evaluated_content_span.text("");
      this.evaluated_content_span.hide();
    }
  }

  get edit_mode() {
    return this._edit_mode;
  }

  set edit_mode(value) {
    this._edit_mode = value;
    this.update_ui();
  }

  get special_content_shown() {
    return this._special_content_shown;
  }

  set special_content_shown(value) {
    this._special_content_shown = value;
    if (value) {
      this.special_content_row.show();
      this.image.slideDown("fast");
      if (this.image_src !== null) this.hide_button.show();
      else this.hide_button.hide();
      this.show_button.hide();
    } else {
      this.image.slideUp("fast", () => this.special_content_row.hide());
      this.hide_button.hide();
      if (this.image_src !== null) this.show_button.show();
      else this.show_button.hide();
    }
  }

  get image_label() {
    return this._image_label;
  }

  set image_label(value) {
    this._image_label = value;
    if (value === null) this.image.attr("alt", "an image");
    else this.image.attr("alt", value);
  }

  set image_message(value) {
    this.error_message.text(value);
  }

  remove() {
    this.container.remove();
    this.special_content_row.remove();
  }

  special_row_after(dom_element) {
    if (dom_element.next()[0] !== this.special_content_row[0]) {
      dom_element.after(this.special_content_row);
    }

    return this.special_content_row;
  }

  after(dom_element) {
    // returns last ui element of this cell
    if (dom_element.next()[0] !== this.container[0]) {
      dom_element.after(this.container);
    }

    return this.container;
  }

  as_first_child(dom_element) {
    // returns last ui element of this cell
    if (dom_element.children()[0] !== this.container[0]) {
      dom_element.prepend(this.container);
    }

    return this.container;
  }

  add_image() {
    if (this.cell_id !== null) {
      let callback_url = `${table_edit_url}?to_cell=${this.cell_id}&add_image=`;
      let encoded_url = encodeURIComponent(callback_url);
      window.location = `${take_image_url}?next=${encoded_url}`;
    } else console.log("tried to add image to cell with cell_id null");
  }

  delete_image() {
    if (confirm(`Deleting image from this cell`)) {
      $.post(cell_reset_url, {
        csrfmiddlewaretoken: csrf_middleware_token,
        cell_id: this.cell_id,
      }).done((data) => this.column.parent_table.update());
    }
  }
}

class Row {
  constructor(parent_table, dict = null) {
    this.cells = new Map();
    this.row_id = null;

    this.parent_table = parent_table;
    this.is_empty = dict === null;
    this._edit_mode = false;

    this.container = $(`<tr></tr>`);
    this.buttons = $(`<td></td>`);
    this.edit_button = icon_button("pencil", "Edit");
    this.edit_button.on("click", () => (this.edit_mode = true));
    this.delete_button = icon_button("trash", "Delete");
    this.delete_button.on("click", () => this.delete());
    this.save_button = $(`<button>Save</button>`);
    this.save_button.on("click", () => this.save());
    this.save_button.hide();
    this.save_button.addClass("btn btn-primary");
    this.buttons.append(this.edit_button);
    this.buttons.append(this.delete_button);
    this.buttons.append(this.save_button);

    if (dict !== null) this.dict = dict;
    this.update_ui();
  }

  get dict() {
    let expressions = [];
    let dicts = [];

    for (let cell of this.cells.values()) {
      let dict = cell.dict;
      dicts.push(dict);
      if (cell.column.type === column_type_number && dict.contents !== "") {
        let expr = new Expression(dict.contents, cell.column.name, dict);
        expressions.push(expr);
      }
    }

    let context = new Map();
    for (let i = 0; i < expressions.length; i++) {
      let change = false;
      for (let expr of expressions) {
        if (expr.evaluate(context)) {
          change = true;
          context.set(expr.name, expr.result);
        }
      }
      if (!change) break;
    }

    for (let expr of expressions) {
      expr.target.evaluated_contents = expr.formatted_result();
    }

    return {
      row_id: this.row_id,
      cells: dicts,
    };
  }

  set dict(value) {
    if (this.row_id !== null && this.row_id !== value.row_id)
      console.log("ID of row changed!");

    this.row_id = value.row_id;
    let old_cells = this.cells;
    this.cells = new Map();
    for (let cell_dict of value.cells) {
      let column_id = cell_dict.column_id;
      let cell = old_cells.get(column_id);
      old_cells.delete(column_id);
      if (cell === undefined) {
        let column = this.parent_table.get_column_by_id(column_id);
        cell = new Cell(column);
      }

      cell.dict = cell_dict;
      this.cells.set(column_id, cell);
    }
    for (let column of this.parent_table.columns) {
      let cid = column.column_id;
      if (old_cells.has(cid) && !this.cells.has(cid)) {
        let unsaved_cell = old_cells.get(cid);
        this.cells.set(cid, unsaved_cell);
        old_cells.delete(cid);
      }
    }
    for (let garbage of old_cells.values()) garbage.remove();
    this.update_ui();
  }

  save() {
    this.is_empty = false;
    $.post(row_save_url, {
      row_json: JSON.stringify(this.dict),
      csrfmiddlewaretoken: csrf_middleware_token,
    }).done((data) => this.handle_row_save_response(data));
  }

  async handle_row_save_response(data) {
    if (!("row_id" in data)) throw Error("Backend did not respond with row_id");
    this.row_id = data.row_id;
    await this.parent_table.update();
    this.edit_mode = false;
  }

  get edit_mode() {
    return this._edit_mode;
  }

  set edit_mode(value) {
    this._edit_mode = value;
    for (let cell of this.cells.values()) cell.edit_mode = value;
    if (this.edit_mode) {
      this.edit_button.hide();
      this.delete_button.hide();

      this.save_button.show();
    } else {
      this.edit_button.show();
      this.delete_button.show();

      this.save_button.hide();
    }
  }

  update_ui() {
    let garbage = new Set(this.cells.values());
    let last_cell_ui = this.container;
    for (let column of this.parent_table.columns) {
      let column_id = column.column_id;
      if (!this.cells.has(column_id))
        this.cells.set(column_id, new Cell(column));
      let cell = this.cells.get(column_id);
      garbage.delete(cell);
      cell.edit_mode = this.edit_mode;
      if (last_cell_ui === this.container)
        last_cell_ui = cell.as_first_child(last_cell_ui);
      else last_cell_ui = cell.after(last_cell_ui);
    }
    for (let cell of garbage) {
      this.cells.delete(cell.column.column_id);
      cell.remove();
    }
    if (last_cell_ui === this.container) this.container.append(this.buttons);
    else if (last_cell_ui.next()[0] !== this.buttons[0]) {
      last_cell_ui.after(this.buttons);
    }
    if (this.cells.size === 0) this.buttons.hide();
    else this.buttons.show();
  }

  remove() {
    for (let cell of this.cells.values()) cell.remove();
    this.container.remove();
  }

  delete() {
    if (confirm("Deleting this row.")) {
      $.post(row_delete_url, {
        csrfmiddlewaretoken: csrf_middleware_token,
        row_id: this.row_id,
      }).done((data) => this.parent_table.update());
    }
  }

  after(dom_element) {
    // returns last ui element of this row
    if (dom_element.next()[0] !== this.container[0]) {
      dom_element.after(this.container);
    }

    let current_last = this.container;
    for (let cell of this.cells.values())
      current_last = cell.special_row_after(current_last);

    return current_last;
  }
}

export class Table {
  constructor(table_id) {
    this.columns = [];
    this.rows = [];
    this.table_id = table_id;
    this._edit_column = null;

    this.container = $(`<div></div>`);
    this.table = $(`<table></table>`);
    this.table.addClass("table");
    this.table_body = $(`<tbody></tbody>`);
    this.table_head = $(`<tr></tr>`);
    this.table_body.append(this.table_head);
    this.table.append(this.table_body);
    this.container.append(this.table);

    this.column_add_heading = $("#column_add_heading");
    this.column_update_heading = $("#column_update_heading");
    this.column_name = $("#column_label");
    this.column_type = $("#column_type");
    this.column_type.on("change", () => {
      this.update_column_position();
      this.update_column_type_text();
    });
    this.column_position = $("#column_position");
    this.column_position_modified = false;
    this.column_position.on(
      "change",
      () => (this.column_position_modified = true),
    );
    this.column_save = $("#column_save");
    this.column_save.on("click", () => this.save_column());
    this.column_add = $("#column_add");
    this.column_add.on("click", () => this.save_column());
    this.column_cancel = $("#column_cancel");
    this.column_cancel.on("click", () => (this.edit_column = null));

    this.update_ui();
  }

  get dict() {
    return {
      columns: this.columns.map((column) => column.dict),
      rows: this.rows.filter((row) => !row.is_empty).map((row) => row.dict),
      table_id: this.table_id,
    };
  }

  set dict(value) {
    this.table_id = value.table_id;

    let column_map = new Map();
    for (let column of this.columns) column_map.set(column.column_id, column);
    this.columns = [];
    for (let column_dict of value.columns) {
      let column = column_map.get(column_dict.column_id);
      if (column === undefined) column = new Column(this, column_dict);
      else column.dict = column_dict;
      column_map.delete(column_dict.column_id);
      this.columns.push(column);
    }
    for (let garbage of column_map.values()) garbage.remove();

    let row_map = new Map();
    for (let row of this.rows) row_map.set(row.row_id, row);
    let unsaved_rows = this.rows.filter((row) => row.row_id === null);
    this.rows = [];
    for (let row_dict of value.rows) {
      let row = row_map.get(row_dict.row_id);
      if (row === undefined) row = new Row(this, row_dict);
      else row.dict = row_dict;
      row_map.delete(row_dict.row_id);
      this.rows.push(row);
    }
    row_map.delete(null);
    for (let garbage of row_map.values()) garbage.remove();
    for (let row of unsaved_rows) {
      row.update_ui();
      this.rows.push(row);
    }

    this.update_empty_row();
    this.update_ui();
  }

  get edit_column() {
    return this._edit_column;
  }

  set edit_column(value) {
    this._edit_column = value;
    this.column_position_modified = false;
    if (value === null) {
      this.column_update_heading.hide();
      this.column_cancel.hide();
      this.column_save.hide();

      this.column_add_heading.show();
      this.column_add.show();

      this.column_name.val("");
      this.column_type.val(column_type_text);
      this.update_column_position();
    } else {
      this.column_update_heading.show();
      this.column_cancel.show();
      this.column_save.show();

      this.column_add_heading.hide();
      this.column_add.hide();

      this.column_name.val(value.name);
      this.column_type.val(value.type);

      let column_index = this.columns.findIndex(
        (c) => c.column_id === value.column_id,
      );
      if (column_index > 0)
        this.column_position.val(this.columns[column_index - 1].column_id);
      else this.column_position.val("");
    }
    this.update_column_type_text();
  }

  save_column() {
    let column_id = null;
    if (this.edit_column !== null) column_id = this.edit_column.column_id;
    $.post(column_save_url, {
      column_id: column_id,
      column_label: this.column_name.val(),
      column_type: this.column_type.val(),
      column_position: this.column_position.val(),
      csrfmiddlewaretoken: csrf_middleware_token,
    }).done((data) => {
      this.edit_column = null;
      this.update();
    });
  }

  update_column(column) {
    this.edit_column = column;
  }

  update_empty_row() {
    let empty_exists = this.rows.some((r) => r.is_empty);
    if (!empty_exists) {
      let new_row = new Row(this);
      new_row.edit_mode = true;
      this.rows.push(new_row);
    }
  }

  get_column_by_id(column_id) {
    let result = this.columns.find((column) => column.column_id === column_id);
    if (result === undefined)
      throw Error(`Column "${column_id}" was not found in table!`);
    return result;
  }

  update() {
    return $.get(table_data_url).done((data) => {
      if ("columns" in data) this.dict = data;
      else console.log("got invalid table data", data);
    });
  }

  update_ui() {
    for (let column of this.columns) this.table_head.append(column.container);

    let current_row_ui = this.table_head;
    for (let row of this.rows) current_row_ui = row.after(current_row_ui);

    let option = this.column_position.find('option[value=""]');
    for (let column of this.columns)
      option = column.prepend_position_select_option(option);
    this.update_column_position();
  }

  update_column_position() {
    if (!this.column_position_modified && this.edit_column === null) {
      let target_val;
      if (
        this.columns.length === 0 ||
        this.column_type.val() === column_type_timestamp
      )
        target_val = "";
      else target_val = this.columns[this.columns.length - 1].column_id;
      if (this.column_position.val() !== target_val)
        this.column_position.val(target_val);
    }
  }

  update_column_type_text() {
    let help_div = $("#number_help_text");
    if (this.column_type.val() === column_type_number)
      help_div.slideDown("fast");
    else help_div.slideUp("fast", () => help_div.hide());
  }
}
