/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { create, all } from "mathjs";

const math = create(all);
const limitedEvaluate = math.evaluate;

const format_precision = 7;

math.import(
  {
    import: function () {
      throw new Error("Function import is disabled");
    },
    createUnit: function () {
      throw new Error("Function createUnit is disabled");
    },
    evaluate: function () {
      throw new Error("Function evaluate is disabled");
    },
    parse: function () {
      throw new Error("Function parse is disabled");
    },
    simplify: function () {
      throw new Error("Function simplify is disabled");
    },
    derivative: function () {
      throw new Error("Function derivative is disabled");
    },
  },
  { override: true },
);

class ExpressionResult {
  constructor(contents, error) {
    this.contents = contents;
    this.error = error;
  }
}

export class Expression {
  get is_expression() {
    return true;
  }

  constructor(expression, name = "", target = null) {
    this.expression = expression;
    this.name = name;
    this.target = target;
    this.result = null;
    this.last_unmet_dependency = "error";
  }

  evaluate(context) {
    // returns if result changed
    if (this.result !== null) return false;

    let my_context = new Map();
    for (let [name, value] of context) {
      if (this.expression.indexOf(name) < 0) continue;
      if (value.error) {
        this.set_error(Error(`recursive error via ${name}`));
        return true;
      } else {
        my_context.set(name, value.contents);
      }
    }

    try {
      this.set_result(limitedEvaluate(this.expression, my_context));
    } catch (e) {
      const prefix = "Undefined symbol ";
      if (e.message.startsWith(prefix)) {
        this.last_unmet_dependency = e.message.slice(prefix.length);
        return false;
      }
      this.set_error(e);
    }

    return true;
  }

  set_error(e) {
    this.result = new ExpressionResult(e.message, true);
  }

  set_result(contents) {
    this.result = new ExpressionResult(contents, false);
  }

  formatted_result() {
    if (this.result === null)
      return `dependency error ${this.last_unmet_dependency}`;
    if (this.result.error) return this.result.contents;
    return math.format(this.result.contents, format_precision);
  }
}
