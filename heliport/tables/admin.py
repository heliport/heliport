# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Registers Django admin pages.

See :class:`django.contrib.admin.ModelAdmin` from Django documentation.
"""

from django.contrib import admin

from .models import Table, TableCell, TableColumn, TableRow

admin.site.register(Table)
admin.site.register(TableColumn)
admin.site.register(TableRow)
admin.site.register(TableCell)
