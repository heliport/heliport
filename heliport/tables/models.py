# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains :class:`django.db.models.Model` classes for Django ORM.

See :ref:`django:topics/db/models:quick example` from Django documentation.
In HELIPORT the :class:`heliport.core.models.DigitalObject` can be subclassed for models
containing metadata in a project.
"""

from django.db import models
from django.urls import reverse

from heliport.core.models import DigitalObject, Image


class Table(DigitalObject):  # noqa: D101
    table_id = models.AutoField(primary_key=True)
    disable_copy = True

    def as_dict(self):  # noqa: D102
        rows = self.tablerow_set.order_by("creation_date")

        return {
            "table_id": self.table_id,
            "columns": [column.as_dict() for column in self.columns],
            "rows": [row.as_dict() for row in rows],
        }

    @property
    def columns(self):  # noqa: D102
        return self.tablecolumn_set.order_by("index")

    @property
    def columns_reversed(self):  # noqa: D102
        return self.tablecolumn_set.order_by("-index")


class TableColumn(models.Model):  # noqa: D101
    class ColumnTypes(models.IntegerChoices):  # noqa: D106
        TEXT = 1
        TIMESTAMP = 2
        IMAGE = 3
        NUMBER = 4

    column_id = models.AutoField(primary_key=True)
    label = models.TextField(blank=True)
    index = models.IntegerField()
    column_type = models.IntegerField(
        choices=ColumnTypes.choices, default=ColumnTypes.TEXT
    )
    parent_table = models.ForeignKey(Table, on_delete=models.CASCADE)

    def __str__(self):  # noqa: D105
        return f'column "{self.label}" of "{self.parent_table}" ({self.pk})'

    def as_dict(self):  # noqa: D102
        return {
            "column_id": self.column_id,
            "label": self.label,
            "column_type": self.column_type,
        }


class TableRow(models.Model):  # noqa: D101
    row_id = models.AutoField(primary_key=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    parent_table = models.ForeignKey(Table, on_delete=models.CASCADE)

    def __str__(self):  # noqa: D105
        return f'row of "{self.parent_table}" ({self.pk})'

    def as_dict(self):  # noqa: D102
        return {
            "row_id": self.row_id,
            "cells": [cell.as_dict() for cell in self.tablecell_set.all()],
        }


class TableCell(models.Model):  # noqa: D101
    cell_id = models.AutoField(primary_key=True)
    row = models.ForeignKey(TableRow, on_delete=models.CASCADE)
    column = models.ForeignKey(TableColumn, on_delete=models.CASCADE)
    contents = models.TextField(blank=True)
    image = models.ForeignKey(Image, null=True, on_delete=models.SET_NULL)
    evaluated_contents = models.TextField(blank=True)
    last_update = models.DateTimeField(auto_now=True)

    def __str__(self):  # noqa: D105
        return f"cell of [{self.column}] and [{self.row}] ({self.pk})"

    def as_dict(self):  # noqa: D102
        contents = self.contents
        if self.column.column_type == self.column.ColumnTypes.TIMESTAMP:
            contents = self.row.creation_date.strftime("%y-%m-%d %H:%M")
        image_url = None
        image_label = None
        if (
            self.column.column_type == self.column.ColumnTypes.IMAGE
            and self.image is not None
        ):
            image_url = reverse("core:image_data", kwargs={"pk": self.image.pk})
            image_label = self.image.label

        image_status = None
        if self.image is not None:
            image_status = self.image.status
        image_message = ""
        if self.image is not None:
            image_message = self.image.message

        return {
            "column_id": self.column_id,
            "cell_id": self.cell_id,
            "contents": contents,
            "image_src": image_url,
            "image_label": image_label,
            "image_uploading": image_status == Image.ImageStates.UPLOADING,
            "image_error": image_status == Image.ImageStates.FAILED,
            "image_message": image_message,
            "evaluated_contents": self.evaluated_contents,
        }
