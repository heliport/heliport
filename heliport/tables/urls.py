# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path

from .views import (
    TableDataView,
    TableDeleteColumnView,
    TableDeleteRowView,
    TableEditView,
    TableResetCellView,
    TablesView,
    TableUpdateColumnView,
    TableUpdateRowView,
)

app_name = "tables"
urlpatterns = [
    path("project/<int:project>/table/list/", TablesView.as_view(), name="list"),
    path(
        "project/<int:project>/table/<int:pk>/edit/",
        TableEditView.as_view(),
        name="edit",
    ),
    path(
        "project/<int:project>/table/<int:pk>/edit/in-list/",
        TablesView.as_view(),
        name="edit_in_list",
    ),
    path(
        "project/<int:project>/table/<int:pk>/edit/column/",
        TableUpdateColumnView.as_view(),
        name="edit_column",
    ),
    path(
        "project/<int:project>/table/<int:pk>/edit/table-data/",
        TableDataView.as_view(),
        name="table_data",
    ),
    path(
        "project/<int:project>/table/<int:pk>/edit/row/",
        TableUpdateRowView.as_view(),
        name="edit_row",
    ),
    path(
        "project/<int:project>/table/<int:pk>/edit/delete-column/",
        TableDeleteColumnView.as_view(),
        name="delete_column",
    ),
    path(
        "project/<int:project>/table/<int:pk>/edit/delete-row/",
        TableDeleteRowView.as_view(),
        name="delete_row",
    ),
    path(
        "project/<int:project>/table/<int:pk>/edit/reset-cell/",
        TableResetCellView.as_view(),
        name="reset_cell",
    ),
]
