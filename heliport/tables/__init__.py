# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""App to store tabular information including images, text and calculations.

The ``interface`` module is imported to the top level of the package for HELIPORT app
interface discovery (see :func:`heliport.core.app_interaction.get_heliport_apps`).
"""

from . import interface
