# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

import json
import logging

from django.conf import settings
from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import DetailView

from heliport.core.mixins import HeliportObjectMixin
from heliport.core.views import HeliportObjectListView

from .models import Image, Table, TableCell, TableColumn, TableRow

logger = logging.getLogger(__name__)


class TablesView(HeliportObjectListView):  # noqa: D101
    model = Table
    category = settings.TABLE_NAMESPACE
    columns = [("ID", "table_id", "small"), ("Name", "label", "large")]
    actions = [
        ("Open", "action_open", "link"),
        ("Edit", "action_edit", "link_secondary"),
        ("Delete", "action_delete", "danger"),
    ]
    edit_fields = [("Name", "label", "normal"), ("Description", "description", "large")]
    list_url = "tables:list"
    update_url = "tables:edit_in_list"
    list_heading = "Tables"
    create_heading = "Add a Table"

    def action_open(self, obj):  # noqa: D102
        return reverse("tables:edit", kwargs={"project": self.project.pk, "pk": obj.pk})


class TableEditView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Table
    template_name = "tables/table.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        As a side effect an image is added to a cell if requested via URL arguments.
        This is required because the :class:`heliport.core.views.images.TakePictureView`
        links back to this view including GET parameters of the picture that was taken.
        """
        image_pk = self.request.GET.get("add_image")
        cell_pk = self.request.GET.get("to_cell")
        if image_pk and cell_pk:
            image = get_object_or_404(Image, pk=image_pk)
            cell = get_object_or_404(TableCell, pk=cell_pk)
            cell.image = image
            cell.save()

        context = super().get_context_data(**kwargs)
        context["ColumnTypes"] = TableColumn.ColumnTypes
        return context


class TableUpdateColumnView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Table

    def post(self, request, *args, **kwargs):  # noqa: D102
        table = self.get_object()
        column_id = request.POST.get("column_id")
        column_label = request.POST.get("column_label", "")
        column_type = request.POST.get("column_type", TableColumn.ColumnTypes.TEXT)
        column_position = request.POST.get("column_position", "")
        if column_position:
            column_index = get_object_or_404(TableColumn, pk=column_position).index + 1
        else:
            column_index = 0

        if not column_id:
            column = TableColumn()
            column.parent_table = table
        else:
            column = get_object_or_404(TableColumn, column_id=column_id)
            if column.parent_table != table:
                logger.error("TableColumn has incorrect parent_table")

        for i, column_after in enumerate(table.columns.filter(index__gte=column_index)):
            column_after.index = i + column_index + 1
            column_after.save()

        column.label = column_label
        column.column_type = column_type
        column.index = column_index
        column.save()

        for row in table.tablerow_set.all():
            TableCell.objects.get_or_create(row=row, column=column)

        return JsonResponse({"column_id": column.column_id})


class TableUpdateRowView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Table

    def post(self, request, *args, **kwargs):  # noqa: D102
        table = self.get_object()
        cells, row_id = self.extract_request_data(request)
        if row_id is None:
            row = TableRow()
            row.parent_table = table
            row.save()
        else:
            row = TableRow.objects.get(row_id=row_id)
            if row.parent_table != table:
                logger.error("Row belongs to wong table")

        for cell_data in cells:
            contents = cell_data.get("contents", "")
            evaluated_contents = cell_data.get("evaluated_contents", "")
            cell_id = cell_data.get("cell_id")
            column_id = cell_data.get("column_id")
            modified = cell_data.get("modified")

            column = get_object_or_404(TableColumn, column_id=column_id)

            if cell_id is None:
                cell = TableCell()
                cell.column = column
            else:
                cell = get_object_or_404(TableCell, cell_id=cell_id)
                if cell.column != column:
                    logger.error("Frontend changed the column a table cell belongs to")

            if modified:
                cell.contents = contents

            if cell.column.column_type == TableColumn.ColumnTypes.NUMBER:
                # Update numbers every time because things like random().
                # However, this can lead to problems with simultaneous updates
                # at the same column by multiple users.
                cell.evaluated_contents = evaluated_contents
                cell.contents = contents

            cell.row = row
            cell.save()

        return JsonResponse({"row_id": row.row_id})

    @staticmethod
    def extract_request_data(request):  # noqa: D102
        row_json = request.POST.get("row_json")
        if row_json is None:
            raise Http404("No row_json")
        row_data = json.loads(row_json)
        row_id = row_data.get("row_id")
        cells = row_data.get("cells")
        if not isinstance(cells, list):
            raise Http404("Invalid row_json")
        return cells, row_id


class TableDataView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Table

    def get(self, request, *args, **kwargs):  # noqa: D102
        table = self.get_object()
        return JsonResponse(table.as_dict())


class TableDeleteColumnView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Table

    def post(self, request, *args, **kwargs):  # noqa: D102
        table = self.get_object()
        column = get_object_or_404(TableColumn, column_id=request.POST.get("column_id"))
        if column.parent_table != table:
            logger.error(
                "Frontend changed the table a column belongs to when deleting column"
            )
        else:
            column.delete()
        return JsonResponse({"message": "ok"})


class TableDeleteRowView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Table

    def post(self, request, *args, **kwargs):  # noqa: D102
        table = self.get_object()
        row = get_object_or_404(TableRow, row_id=request.POST.get("row_id"))
        if row.parent_table != table:
            logger.error(
                "Frontend changed the table a row belongs to when deleting row"
            )
        else:
            row.delete()
        return JsonResponse({"message": "ok"})


class TableResetCellView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Table

    def post(self, request, *args, **kwargs):  # noqa: D102
        table = self.get_object()
        cell = get_object_or_404(
            TableCell,
            cell_id=request.POST.get("cell_id"),
            column__parent_table=table,
            row__parent_table=table,
        )

        if cell.column.column_type == TableColumn.ColumnTypes.IMAGE:
            cell.image = None
            cell.save()
        return JsonResponse({"message": "ok"})
