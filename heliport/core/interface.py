# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Module with special name "interface" hooks into HELIPORT.

Some functions and :class:`heliport.core.app_interaction.Module` subclasses are detected
by HELIPORT and control how HELIPORT uses this app.

Note that this module must be imported in __init__.py of the django app.

This particular module is the HELIPORT interface of the HELIPORT core app itself.
"""

from django.urls import reverse

from heliport.core.app_interaction import DigitalObjectModule, Module


class ProjectConfigurationModule(Module):  # noqa: D101
    name = "Project Configuration"
    module_id = "project_configuration"
    icon = "fa-solid fa-gear"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("core:project", kwargs={"project": project.pk})

    def is_configured(self, project):
        """Return whether the module is configured, i.e. to show it in the graph."""
        return project.label


class Images(DigitalObjectModule):  # noqa: D101
    name = "Images"
    module_id = "images"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("core:image_list", kwargs={"project": project.pk})

    @property
    def object_class(self):
        """Image."""
        from .models import Image

        return Image


class Timeline(Module):  # noqa: D101
    name = "Project Timeline"
    module_id = "timeline"
    icon = "fa-solid fa-clock"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("core:project_timeline", kwargs={"project": project.pk})

    def is_configured(self, project):
        """Return whether the module is configured, i.e. to show it in the graph."""
        return True


class Tags(DigitalObjectModule):  # noqa: D101
    name = "Tags"
    module_id = "tags"
    icon = "fa-solid fa-tags"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("core:tag_list", kwargs={"project": project.pk})

    @property
    def object_class(self):
        """Tag."""
        from .models import Tag

        return Tag


class SubProjects(Module):  # noqa: D101
    name = "Subprojects"
    module_id = "subprojects"
    icon = "fa-solid fa-table-cells-large"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("core:subprojects", kwargs={"project": project.pk})

    def is_configured(self, project):
        """Return whether the module is configured, i.e. to show it in the graph."""
        from .models import Project

        return Project.objects.filter(projects=project, deleted__isnull=True).exists()


def get_search_url():
    """Return the search URL for this app.

    This URL is used to implement the global HELIPORT string search.
    """
    return reverse("core:search")
