# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Account adapters for usage with allauth."""

from allauth.account.adapter import DefaultAccountAdapter
from allauth.core.exceptions import ImmediateHttpResponse
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import BACKEND_SESSION_KEY
from django.contrib.auth.models import User
from django.shortcuts import redirect

from heliport.core.models import HeliportUser


def get_display_name(user: User):
    """Return a user's display name."""
    if hasattr(user, "heliportuser"):
        return user.heliportuser.display_name
    name = f"{user.first_name} {user.last_name}".strip()
    return name or user.username


class AccountAdapter(DefaultAccountAdapter):
    """Adapter that allows enabling/disabling registration via settings."""

    def is_open_for_signup(self, request):
        """Allow creation of local accounts based on settings."""
        return settings.LOCAL_ACCOUNT_SIGNUP

    def save_user(self, request, user, form, commit=True):
        """Save the new user.

        This takes the user created in the parent class and creates a
        :class:`HeliportUser` object for it.
        """
        user = super().save_user(request, user, form, commit)

        name = (
            form.cleaned_data.get("first_name", "")
            + " "
            + form.cleaned_data.get("last_name", "")
        ).strip()

        heliportuser = HeliportUser()
        heliportuser.auth_user = user
        heliportuser.display_name = name or user.username

        if orcid := form.cleaned_data["orcid"]:
            heliportuser.orcid = orcid

        heliportuser.save()

        return user


class HelmholtzIDAccountAdapter(DefaultSocialAccountAdapter):
    """Adapter for use with Helmholtz ID."""

    def is_open_for_signup(self, request, sociallogin):
        """Allow new Helmholtz ID accounts based on settings."""
        return settings.OIDC_SIGNUP

    def pre_social_login(self, request, sociallogin):
        """Disallow login based on configurable eduperson entitlements."""
        extra_data = sociallogin.account.extra_data
        account_entitlements = extra_data.get("eduperson_entitlement")
        allowed = account_entitlements is not None and any(
            entitlement in account_entitlements
            for entitlement in settings.OIDC_EDUPERSON_ENTITLEMENTS
        )

        # There are no restrictions in place
        if not settings.OIDC_EDUPERSON_ENTITLEMENTS:
            allowed = True

        if not allowed:
            messages.add_message(
                request,
                messages.ERROR,
                "Access is not allowed using this login provider.",
            )
            raise ImmediateHttpResponse(redirect("account_login"))

        return super().pre_social_login(request, sociallogin)

    def save_user(self, request, sociallogin, form=None):
        """Save the new user.

        The parent class internally calls :meth:`AccountAdapter.save_user`, thus the
        user's HELIPORT user already exists. Here, we enrich it with more information
        provided by the sociallogin.
        """
        user = super().save_user(request, sociallogin, form)

        extra_data = sociallogin.account.extra_data

        if display_name := extra_data.get("name"):
            user.heliportuser.display_name = display_name
            user.heliportuser.save()

        return user

    def validate_disconnect(self, account, accounts):
        """Allow LDAP users to disconnect their socialaccounts."""
        if (
            self.request.session[BACKEND_SESSION_KEY]
            == "django_auth_ldap.backend.LDAPBackend"
        ):
            return

        super().validate_disconnect(account, accounts)
