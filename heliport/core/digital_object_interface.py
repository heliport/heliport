# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
This module contains the base that define an interface between heliport apps via
digital objects.

The interface is centered around the :class:`GeneralDigitalObject`.
Digital object implementations may inherit from one or multiple digital object aspects
like :class:`heliport.core.digital_object_aspects.FileObj`.
Note that these aspect classes do not share any method names
(even including ``__init__``) which allows multiple inheritance without special care.

HELIPORT apps can register their digital object types, in apps.py like so::

    from django.apps import AppConfig
    from heliport.core.digital_object_resolution import object_types

    class MyConfig(AppConfig):
        name = "my_app_name"

        def ready(self):
            from .models import MyGeneralDigitalObject
            object_types.register(MyGeneralDigitalObject)

where MyGeneralDigitalObject is a
:class:`heliport.core.digital_object_interface.GeneralDigitalObject`.
"""  # noqa: D205

from __future__ import annotations

from abc import abstractmethod
from typing import TYPE_CHECKING

from .digital_object_actions import get_actions
from .digital_object_resolution import Resolvable
from .utils.context import Context
from .utils.serialization import GeneralValue

if TYPE_CHECKING:
    from .models import DigitalObject, HeliportUser, Project


class GeneralDigitalObject(GeneralValue, Resolvable):
    """Represent an object.

    This is an abstract class. It doesn't inherit from ABC because this would cause
    metaclass conflicts when inheriting from it in Django models.

    For methods to implement, also check the baseclasses.
    """

    def access(self, context: Context):
        """Access control. Always no access by default. Subclass to change this."""
        from .permissions import PermissionChecker

        return PermissionChecker(None, context)

    @abstractmethod
    def as_digital_object(self, context: Context) -> DigitalObject:
        """Import and save the object as :class:`heliport.core.models.DigitalObject`."""

    def actions(self, user: HeliportUser = None, project: Project = None):
        """Actions available for this object given the parameters.

        Note that ``user`` and ``project`` are optional but should be available in most
        cases.

        To generate a button for an action returned from this function, use the
        following attributes:

        - ``action.name`` - the text you can display on the button
        - ``action.link`` - the url to link to when someone clicks the button.

        :param user: is the :class:`heliport.core.models.HeliportUser` that is currently
            authenticated. You can get this e.g. from a Django request like
            ``request.user.heliportuser``.
        :param project: is the current :class:`heliport.core.models.Project`.
        :return: list of :class:`heliport.core.models.HeliportAction` objects that are
            available for this object in the context of ``user`` and ``project``.
        """
        return get_actions(self, project, user)
