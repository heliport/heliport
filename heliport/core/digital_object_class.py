# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Functions to use if importing :class:`heliport.core.models.DigitalObject` would result
in errors because of cyclical imports.
"""  # noqa: D205

from __future__ import annotations

from typing import TYPE_CHECKING, Type

if TYPE_CHECKING:
    from heliport.core.models import DigitalObject


def digital_object_class() -> Type[DigitalObject]:
    """Returns the :class:`heliport.core.models.DigitalObject` class."""  # noqa: D401
    from .models import DigitalObject

    return DigitalObject


def digital_objects():
    """Use this if you would write ``DigitalObject.objects`` normally."""
    return digital_object_class().objects
