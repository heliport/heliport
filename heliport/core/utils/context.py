# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later
from __future__ import annotations

import logging
from typing import TYPE_CHECKING, Any, Protocol

from django.urls import reverse

from heliport.core.utils.collections import ExitContext

if TYPE_CHECKING:
    from heliport.core.models import HeliportUser, Project


logger = logging.getLogger(__name__)


class Description(Protocol):
    """
    Can be used to get or create values in some :class:`Context`::

        from dataclasses import dataclass

        @dataclass(frozen=True)
        class MyConnection:
            my_connection_param: str

            def generate(self, context):
                ...  # open connection
                return connection  # connection may be a context manager

        def f(context):
            connection = context[MyConnection("foo")]
            ...

        with Context() as context:
            f(context)  # opens new connection
            f(context)  # takes existing connection from context
        # if generate returned a context manager, connection is closed after leaving with block
    """  # noqa: D400, E501

    def generate(self, context: Context) -> Any:
        """
        Generate the value that the dataclass is describing.
        Program with caution, handle all errors to close everything before raising.
        Automatic closing only works after the context manager is returned.
        """  # noqa: D205


class Context:
    """
    A context can be used for optimizations where expensive setup and teardown steps
    can be shared between operations or to access the current logged-in user.

    See :class:`heliport.core.utils.collections.ExitContext` for more details.
    """  # noqa: D205

    def __init__(  # noqa: D107
        self,
        user: HeliportUser = None,
        project: Project = None,
        context: ExitContext = None,
    ):
        self.user_or_none = user
        self.project_or_none = project
        self.context = context
        self._manual_closing = False

    def __enter__(self):
        """Create a context."""
        if self.context is None:
            self.context = ExitContext()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Close the context."""
        if not self._manual_closing:
            self.close()
        self._manual_closing = False

    def __getitem__(self, key: Description):
        """Get a cached value or caching a new value."""
        if self.context is None:
            result = key.generate(self)
            assert not hasattr(result, "__enter__"), "This context has no ExitContext"
        else:
            result = self.context.get(key, key.generate, self)

        return result

    def close(self):
        """Close the context and all contexts registered on it."""
        if self.context is not None:
            self.context.close()

    @property
    def user(self):
        """The current user; raises an assertion error if there is none."""
        assert self.user_or_none is not None, "Set user before calling this"
        return self.user_or_none

    @property
    def project(self):
        """The current project; raises an assertion error if there is none."""
        assert self.project_or_none is not None, "Set project before calling this"
        return self.project_or_none

    def activate_manual_closing(self):
        """Disable automatic closing when exiting a with statement once.

        This allows to return to a calling function that is trusted to close this
        context::

            def create_context():
                with Context() as c:
                    maybe_raise_error(c)  # c is closed if error is raised
                    c.activate_manual_closing()
                    # an error here would not close c
                    # c.close() would still work normally
                    return c  # exiting with block is ignored

            with create_context() as context:
                print(context, "is not closed yet")
                # an error here would close context properly again

            # now the context is closed

        Instead of another ``with`` block, :meth:`close` could be called manually
        in the calling context.
        """  # noqa: D205
        self._manual_closing = True

    def cache(self, key, value):
        """Just cache the value - nothing fancy - nothing is entered, closed or generated."""  # noqa: E501
        # maybe implement __setitem__ for that?
        assert self.context is not None, 'Context not set. Use "with context:" block?'
        self.context.set_value(key, value)

    def get_cached(self, key):
        """Just get a cached value; returns ``None`` if not found."""
        assert self.context is not None, 'Context not set. Use "with context:" block?'
        return self.context.get(key)


def digital_object_breadcrumbs(session, url_query_params, label, link):
    """
    Manage breadcrumbs in sessions and add ``label`` pointing to ``link``.
    Current breadcrumb path is reset if ``url_query_params`` don't include
    ``"breadcrumb" == True``.
    """  # noqa: D205
    session_key = "DIGITAL_OBJECT_BREADCRUMBS"

    items = []
    if str(url_query_params.get("breadcrumb")).lower() == "true":
        for existing_label, existing_link in session.get(session_key, []):
            if existing_link == link:
                break

            items.append((existing_label, existing_link))

    items.append((str(label), link))
    session[session_key] = items
    return items


# TODO: Move somewhere else? Maybe it could become its own mixin. Then it doesn't need
# the session passed along to it, either.
def project_header_breadcrumbs(request, label, link, reset=False):
    """Manage breadcrumbs for project header in session.

    If the ``reset`` parameter is ``True``, only the first breadcrumb is kept.
    """
    session_key = "PROJECT_HEADER_BREADCRUMBS"
    session_key_last_link = "PROJECT_HEADER_BREADCRUMBS_LAST_LINK"

    # Stores breadcrumbs for each link that was visited
    breadcrumb_store = request.session.get(session_key, {})
    last_link = request.session.get(session_key_last_link, "/")

    existing_breadcrumbs = breadcrumb_store.get(link) or breadcrumb_store.get(
        last_link, []
    )

    items = []

    # If the newest link in the breadcrumbs points to the current page, we don't need to
    # do anything. This is important in case we stay on the same link (with changing URL
    # parameters) for multiple clicks (e.g. when clicking through a data source).
    # However, we do want to change the label in that case because data sources of a
    # project all share the same "open" URL and we might actually be in a different
    # data source now.
    if existing_breadcrumbs:
        newest_label, newest_link = existing_breadcrumbs[-1]
        if newest_link == link:
            items = existing_breadcrumbs
            items[-1] = (str(label) or newest_label, newest_link)

    if not items:
        view_name = request.resolver_match.view_name
        is_project_graph_view = view_name == "core:project_graph"
        is_project_redirect_view = view_name == "core:open_in_project"
        for existing_label, existing_link in existing_breadcrumbs:
            if (
                existing_link == link
                or is_project_graph_view
                or is_project_redirect_view
            ):
                break
            items.append((existing_label, existing_link))
            if reset:
                break

        if is_project_redirect_view:
            link = reverse(
                "core:project_graph",
                kwargs={"project": request.resolver_match.kwargs["project"]},
            )
        items.append((str(label), link))

    # Write back
    breadcrumb_store[link] = items
    request.session[session_key] = breadcrumb_store
    request.session[session_key_last_link] = link

    return items
