# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""url helper functions."""

from urllib.parse import SplitResult, parse_qsl, quote, urlencode, urlsplit, urlunsplit

not_allowed_in_turtle = '<>" {}|\\^`'


def url_encode(url):
    """
    Remove characters not allowed in turtle encoding from the url.
    Swap host for ``www.example.org`` and protocol for ``https`` if they contain erroneous (not allowed) characters.
    Other characters are removed by url encoding them.
    """  # noqa: D205, E501
    scheme, netloc, path, query, fragment = urlsplit(url)
    not_allowed = not_allowed_in_turtle
    q_netloc = "www.example.org" if any(c in not_allowed for c in netloc) else netloc
    q_scheme = "https" if any(c in not_allowed for c in scheme) else scheme
    q_path = quote(path)
    q_query = urlencode(parse_qsl(query, keep_blank_values=True), quote_via=quote)
    q_fragment = quote(fragment)

    return urlunsplit(SplitResult(q_scheme, q_netloc, q_path, q_query, q_fragment))
