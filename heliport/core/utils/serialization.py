# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later
from abc import abstractmethod

from heliport.core.utils.collections import RootedRDFGraph


class GeneralValue:
    """
    Abstract base class for values that some property of a digital object can take on.
    This is an abstract class but doesn't inherit form ABC because metaclass conflict
    when inheriting from it in Django models.
    """  # noqa: D205

    @abstractmethod
    def as_text(self) -> str:
        """Represent the value as a string for the user."""

    @abstractmethod
    def as_rdf(self) -> RootedRDFGraph:
        """Represent the value in RDF."""

    @abstractmethod
    def as_html(self) -> str:
        """
        Represent the value in HTML. Everything is escaped by default. If you want to
        use HTML tags return a :class:`django.utils.safestring.SafeString`
        (e.g. obtained by using Django rendering functions).
        """  # noqa: D205

    @abstractmethod
    def __hash__(self):
        """Hashable."""

    @abstractmethod
    def __eq__(self, other):
        """Comparable."""  # noqa: D401

    def __str__(self):
        """__str__."""
        return self.as_text()
