# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import re
import string
import uuid
from collections import namedtuple

import requests

from heliport.core.utils.http import heliport_user_agent


def generate_object_id():  # noqa: D103
    return uuid.uuid4()


def remove_special_chars(raw_id):  # noqa: D103
    result = re.sub("[^A-Za-z0-9]", "X", str(raw_id))
    if len(result) == 0:
        result = "X"
    if result[0] not in string.ascii_letters:
        result = f"X{result}"
    return result


#: Tuple to hold a resource idenfier scheme as `defined by the DataCite Ontology
#: <http://purl.org/spar/datacite/ResourceIdentifierScheme>`_.
IdentifierScheme = namedtuple("IdentifierScheme", "prefix datacite_id label")

#: Local resource identifier scheme as `defined by the DataCite Ontology
#: <http://purl.org/spar/datacite/local-resource-identifier-scheme>`_. We use this
#: identifier scheme as a fallback for unknown schemes, e.g. created from user input.
default_identifier_scheme = IdentifierScheme(
    "", "local-resource-identifier-scheme", "Other Identifier"
)

#: List of resource identifier schemes as `defined by the DataCite Ontology
#: <http://purl.org/spar/datacite/ResourceIdentifierScheme>`_.
identifier_schemes = [
    IdentifierScheme("https://doi.org/", "doi", "DOI"),
    IdentifierScheme("https://hdl.handle.net/", "handle", "Handle"),
    IdentifierScheme("https://n2t.net/ark:", "ark", "ark:"),
    IdentifierScheme(
        "https://opencitations.net/oci/", "oci", "Open Citation Identifier"
    ),
    IdentifierScheme("http://purl.org/", "purl", "purl"),
    IdentifierScheme("https://d-nb.info/", "dnb", "DNB catalogue number"),
    IdentifierScheme("https://www.wikidata.org/wiki/", "wikidata", "Wikidata"),
    IdentifierScheme(
        "https://d-nb.info/gnd/", "gnd", "Gemeinsame Normdatei identifier"
    ),
    IdentifierScheme(
        "https://id.loc.gov/authorities/",
        "loc",
        "Library of Congress authority identifier",
    ),
    IdentifierScheme("https://www.worldcat.org/oclc/", "oclc", "OCLC control number"),
    IdentifierScheme("https://", "url", "URL"),
    IdentifierScheme("http://", "url", "URL"),
]


def find_identifier_scheme(identifier):  # noqa: D103
    if identifier is None:
        return default_identifier_scheme

    for s in identifier_schemes:
        if identifier.startswith(s.prefix):
            return s
    return default_identifier_scheme


def normalize_datacite_resource_identifier_scheme(datacite_identifier_scheme):
    """Normalize a given DataCite ontology resource identifier scheme.

    The DataCite ontology provides a `list of resource identifier schemes
    <http://purl.org/spar/datacite/ResourceIdentifierScheme>`_. However, these don't
    always use the name as the identifier system itself considers the canonical prefix
    representation. This function can be used to normalize the scheme.
    """
    replacements = {
        # According to RFC 3651 <https://doi.org/10.17487/RFC3651>, ``hdl`` is the
        # correct scheme.
        "handle": "hdl",
    }
    return replacements.get(datacite_identifier_scheme) or datacite_identifier_scheme


def test_resolvability(identifier):  # noqa: D103
    try:
        response = requests.head(
            identifier, timeout=10, headers={"User-Agent": heliport_user_agent}
        )
        return response.ok
    except requests.RequestException:
        return False


def is_resolvable(identifier: str):  # noqa: D103
    return identifier.startswith("http://") or identifier.startswith("https://")


def is_globally_unique(identifier: str):  # noqa: D103
    return is_resolvable(identifier)
