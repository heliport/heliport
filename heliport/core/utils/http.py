# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later
"""Functionality for working with http requests and responses."""

from mimetypes import guess_extension
from typing import Callable
from urllib.parse import quote, urlencode

from django.http import FileResponse
from django.urls import reverse
from rest_framework import status
from rest_framework.response import Response

from heliport.core.digital_object_aspects import DirectoryObj, File, FileObj
from heliport.core.digital_object_resolution import params_for, resolve
from heliport.core.utils.context import Context
from heliport.core.utils.exceptions import UserMessage
from heliport.core.utils.version import (
    heliport_homepage,
    heliport_name,
    heliport_version_string,
)

# TODO: Indicate HELIPORT instance and/or app here?
heliport_user_agent = f"{heliport_name}/{heliport_version_string} ({heliport_homepage})"


def get_streaming_file_response(file: File, filename, context: Context):
    """Construct a streaming response for downloading the file."""
    old_close = file.close

    def new_close():
        """
        Close file and context.
        There is no known documented way to set
        `_resource_closers <https://github.com/django/django/blob/26a395f27d86bbf65f330851c8136c33694ac867/django/http/response.py#L570>`_
        otherwise.
        """  # noqa: D205
        old_close()
        context.close()

    file.open()
    file.close = new_close
    response = FileResponse(
        file,
        filename=filename,
        as_attachment=True,
        content_type=file.mimetype(),
    )
    size = file.size()
    if size is not None:
        response["Content-Length"] = size

    # prevent that context is closed when exiting with block in get method
    # instead it is made sure that context is closed using new_close function
    context.activate_manual_closing()
    return response


def file_api_response(request, file_obj: FileObj, file_name: str, add_extension=False):
    """Get download response for digital object that represents a file.

    The response is either a streaming file response or a JSON response of an error.
    """
    try:
        with Context(request.user.heliportuser) as context:
            file = file_obj.as_file(context)
            if add_extension:
                extension = guess_extension(file.mimetype())
                if extension and not file_name.endswith(extension):
                    file_name = f"{file_name}{extension}"
            return get_streaming_file_response(file, file_name, context)
    except UserMessage as m:
        return Response({"error": m.message}, status=status.HTTP_400_BAD_REQUEST)


def file_from_description_api_response(
    request, file_name_callback: Callable[[FileObj], str], add_extension=False
):
    """Get download response for digital object that represents a file.

    The digital object is resolved from GET parameters of the request. This allows to
    download also files that are not stored in heliport directly e.g. files in
    subdirectories.
    """
    try:
        with Context(request.user.heliportuser) as context:
            obj = resolve(request.GET, context)
            if obj is None:
                raise UserMessage(f"Object not found: {request.GET}")
            obj.access(context).assert_read_permission()
            if not isinstance(obj, FileObj):
                raise UserMessage(
                    f"{obj} is a {type(obj).__name__} and not a recognized file"
                )

            filename = file_name_callback(obj)
            file = obj.as_file(context)
            if add_extension:
                extension = guess_extension(file.mimetype())
                if extension and not filename.endswith(extension):
                    filename = f"{filename}{extension}"

            return get_streaming_file_response(file, filename, context)
    except UserMessage as m:
        return Response({"error": m.message}, status=status.HTTP_400_BAD_REQUEST)


def directory_api_response(
    request,
    directory_obj: DirectoryObj,
    file_url=None,
    directory_url=None,
    context=None,
):
    """Render directory response.

    file_url and directory_url are completely optional but can be used to keep the user
    in a particular endpoint. If they are not supplied the generic complete digital
    object api is used.
    """
    if file_url is None:
        file_url = request.build_absolute_uri(
            reverse("digital-objects-complete-download-from-description")
        )
    if directory_url is None:
        directory_url = request.build_absolute_uri(
            reverse("digital-objects-complete-contents-from-description")
        )

    if context is None:
        context = Context(request.user.heliportuser)
    try:
        with context:
            directory = directory_obj.as_directory(context)
            files = []
            subdirectories = []

            for part in directory.get_parts():
                part = part.as_digital_object_if_exists(context)
                params = params_for(part)
                if isinstance(part, FileObj):
                    url = f"{file_url}?{urlencode(params, quote_via=quote)}"
                    files.append(
                        {
                            "name": part.as_text(),
                            **params,
                            "download_url": url,
                        }
                    )
                else:
                    url = f"{directory_url}?{urlencode(params, quote_via=quote)}"
                    subdirectories.append(
                        {
                            "name": part.as_text(),
                            **params,
                            "contents_url": url,
                        }
                    )
            return Response(
                {
                    "dir_info": params_for(directory_obj),
                    "files": files,
                    "subdirectories": subdirectories,
                }
            )
    except UserMessage as m:
        return Response({"error": m.message}, status=status.HTTP_400_BAD_REQUEST)


def directory_from_description_api_response(request, file_url=None, directory_url=None):
    """Generate directory response where directory is specified in GET params."""
    with Context(request.user.heliportuser) as context:
        obj = resolve(request.GET, context)
        if obj is None:
            Response(
                {"error": f"Object not found: {request.GET}"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if not isinstance(obj, DirectoryObj):
            Response(
                {"error": f"{obj} is a {type(obj).__name__} and not a directory"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        obj.access(context).assert_read_permission()
        return directory_api_response(request, obj, file_url, directory_url, context)
