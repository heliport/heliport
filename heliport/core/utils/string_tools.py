# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Helper functions to work with strings."""

from datetime import datetime
from typing import List, Optional, Union

from django.template import Context, Template
from django.utils import timezone


def remove_prefix(
    chars: str, prefix_options: Union[str, List[str]], raise_on_error=False
) -> str:
    """
    Remove a prefix (potentially from a list of prefixes) from ``chars``.
    Note that this function should be used instead of builtin ``str.removeprefix`` as long
    as we support versions that don't have this function.

    :param chars: The string before removal of the prefix.
    :param prefix_options: The first matching prefix option is removed.
        Supplying a string is equivalent to a list with one element.
    :param raise_on_error: If ``False`` nothing happens when no prefix matches
    :raise ValueError: If ``raise_on_error`` is True and no prefix matches
    :return: ``chars`` but without the prefix
    """  # noqa: D205, E501
    if isinstance(prefix_options, str):
        prefix_list = [prefix_options]
    else:
        prefix_list = prefix_options

    for prefix in prefix_list:
        if chars.startswith(prefix):
            return chars[len(prefix) :]

    if raise_on_error:
        raise ValueError(f"{chars} does not start with {', '.join(prefix_list)}")
    return chars


def remove_trailing_newline(text: str):
    """Remove a single trailing newline.

    `CR LF`, `LF`, and `CR` are tried in this order, returning after the first
    successful find.
    """
    for suffix in ["\r\n", "\n", "\r"]:
        if text.endswith(suffix):
            return text[: -len(suffix)]
    return text


def parse_date_time(string_date) -> Optional[datetime]:
    """Try different time formats to parse the string into a python datetime object."""
    if not isinstance(string_date, str):
        return None
    if string_date in {"now", "today"}:
        return datetime.now()
    for fmt in [
        "%Y-%m-%dT%H:%M:%S%z",
        "%b %d, %Y",
        "%b %d, %Y %H:%M",
        "%Y-%m-%d",
        "%Y-%m-%d %H-%M",
        "%d-%b-%y",
        "%m/%d/%Y",
        "%Y-%m-%d %H:%M",
        "%Y-%m-%d %H:%M:%S",
        "%d. %m. %Y",
    ]:
        try:
            return datetime.strptime(string_date, fmt)
        except ValueError:
            continue
    return None


def format_byte_count(size):
    """Format size in bytes into human-readable unit."""
    for unit in ["Bytes", "KiB", "MiB", "GiB", "TiB", "PiB"]:
        if size < 1024.0 or unit == "PiB":
            break
        size /= 1024.0
    return f"{size:.0f} {unit}"


def format_time(time: Union[Union[float, int], str]):
    """
    Format time like YYYY-mm-dd HH:MM:SS.

    :param time: use :func:`parse_date_time` to interpret time, interpret as POSIX time
        if instance of float or int (e.g. ``time.time()`` returns a POSIX time).
    """
    if isinstance(time, (float, int)):
        date = timezone.datetime.fromtimestamp(time)
    else:
        date = parse_date_time(time)
    if date is None:
        return "invalid date"
    return date.strftime("%Y-%m-%d %H:%M:%S")


def bool_from_url(potential_bool):
    """Parse a boolean value that originated e.g. form request.GET.get("abc")."""
    if isinstance(potential_bool, str):
        return potential_bool.lower() not in ["", "f", "false"]
    return bool(potential_bool)


def conditional_text(text: str, condition: str):
    """Render a span that has ``condition`` as ``x-show`` attribute.

    This can be used e.g. for conditional help text for Django forms.
    """
    template = Template('<span x-show="{{condition}}">{{text}}</span>')
    context = Context({"condition": condition, "text": text})
    return template.render(context)
