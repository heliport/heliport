# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later
import re
from collections import namedtuple

from heliport.core.utils.collections import HashableDictWrapper, IndexedSet


class LdContextEntry:  # noqa: D101
    def __init__(self, original_key, context_id, key_counter=0):  # noqa: D107
        self._original_key = original_key
        self._context_id = context_id
        self._key_counter = key_counter
        self.containing_dicts = set()

    def format_key(self, offset=0):  # noqa: D102
        count = self._key_counter + offset
        if count == 0:
            return self.original_key
        return f"{self.original_key}_{count}"

    @property
    def current_key(self):  # noqa: D102
        return self.format_key()

    @property
    def original_key(self):  # noqa: D102
        return self._original_key

    @property
    def context_id(self):  # noqa: D102
        return self._context_id

    def rename(self, offset=1):  # noqa: D102
        if offset == 0:
            return
        old_key = self.current_key
        self._key_counter += offset
        for containing_dict in self.containing_dicts:
            value = containing_dict[old_key]
            del containing_dict[old_key]
            containing_dict[self.current_key] = value

    def tentative_set(self, the_dict, the_value, reduce=None):  # noqa: D102
        if reduce is not None and self.current_key in the_dict:
            the_value = reduce(the_value, the_dict[self.current_key])

        the_dict[self.current_key] = the_value
        self.containing_dicts.add(HashableDictWrapper(the_dict))

    @property
    def match_condition(self):  # noqa: D102
        return {"original_key": self.original_key, "context_id": self.context_id}

    def __repr__(self):  # noqa: D105
        return f"{type(self).__name__}({self.original_key!r}, {self.context_id!r}, {self._key_counter!r})"  # noqa: E501


class LdContext:  # noqa: D101
    def __init__(self):  # noqa: D107
        self.entries = IndexedSet()

    def tentative_set(  # noqa: D102
        self, the_dict, the_key, the_value, id_for_context, reduce=None
    ):
        entry = LdContextEntry(the_key, id_for_context)

        candidate_entry = self.entries.get_any_or_none(**entry.match_condition)
        if candidate_entry is not None:
            entry = candidate_entry
        else:
            self.resolve_conflicts(entry)
            self.entries.add(entry)

        entry.tentative_set(the_dict, the_value, reduce)

    def resolve_conflicts(self, entry, offset=0):  # noqa: D102
        try_key = entry.format_key(offset)
        conflict = self.entries.get_any_or_none(current_key=try_key)
        assert conflict is not entry, "offset=0 only if entry not in self.entries"

        if conflict is None:
            # there is no conflict; entry can simply be renamed
            self.rename(entry, offset)
        elif conflict.context_id > entry.context_id:
            # rename the conflict
            self.resolve_conflicts(conflict, offset=1)
            self.rename(entry, offset)
        else:
            # go along the chain of entries with the same name
            self.resolve_conflicts(entry, offset + 1)

    def rename(self, entry, offset):
        """Rename all dicts that contain entry and update index."""
        if entry in self.entries:
            self.entries.remove(entry)
            entry.rename(offset)
            self.entries.add(entry)
        else:
            entry.rename(offset)

    def get_context_dict(self):  # noqa: D102
        return dict(
            sorted((entry.current_key, entry.context_id) for entry in self.entries)
        )


TargetDescription = namedtuple("TargetDescription", "name uri mapping_target")


def reverse_context_mapping(value_descriptions, data_dict):
    """
    Yields tuples of the form (value_descriptions[i].mapping_target, data_dict[key])
    where key is a key that LdContext would generate for the Target Description value_descriptions[i]
    given a list of TargetDescription and a dictionary of values.
    """  # noqa: D205, D401, E501
    data_dict = dict(data_dict)
    assert None not in data_dict, "The value None is not supported as a key"
    for target_description in sorted(value_descriptions, key=lambda t: t.uri):
        key = lowest_matching_key(target_description.name, data_dict)
        if key is not None:
            yield target_description.mapping_target, data_dict[key]
            del data_dict[key]


def lowest_matching_key(the_key, candidates):  # noqa: D103
    pattern = re.compile(re.escape(the_key) + "(_([0-9]+))?")
    matches = filter(bool, map(pattern.fullmatch, candidates))
    min_count, min_key = min(
        ((int(m.group(2) or 0), m.string) for m in matches), default=(None, None)
    )
    return min_key
