# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Collection of functions that work with colors."""

import random


def pick_color() -> tuple:
    """Return color from uniform distribution as tuple of three numbers in ``range(255)``."""  # noqa: E501
    return tuple(random.choice(range(255)) for i in range(3))


def color_dist(c1, c2):
    """
    Compute distance of colors based on some heuristic.

    :param c1: A color represented by three values e.g. (1, 2, 3)
    :param c2: A color represented by three values e.g. (1, 2, 3)
    :return: Score how different ``c1`` and ``c2`` are. Larger score means more different.
    """  # noqa: E501
    r1, g1, b1 = c1
    r2, g2, b2 = c2
    return 0.3 * (r1 - r2) ** 2 + 0.59 * (g1 - g2) ** 2 + 0.11 * (b1 - b2) ** 2


def color_dist_array(c1, colors):
    """
    Compute minimum distance of ``c1`` to any color in ``colors``.
    Input and output format is the same as in :func:`color_dist`.
    """  # noqa: D205
    return min((color_dist(c1, c2) for c2 in colors), default=0)


def pick_color_not_in(colors):
    """
    Pick random color. Colors more different from all colores in ``colors`` are preferred.
    For color representation see :func:`pick_color` and :func:`color_dist` functions.
    """  # noqa: D205, E501
    color = pick_color()
    current_dist = color_dist_array(color, colors)
    for _ in range(200):
        new_color = pick_color()
        new_dist = color_dist_array(new_color, colors)
        if new_dist > current_dist:
            current_dist = new_dist
            color = new_color
    return color


def pick_foreground_color(background_html_color: str) -> str:
    """
    Return a color visible on some ``background_html_color``.
    Colores are represented like in HTML e.g. ``#FFF`` (different length, lowercase or without ``#`` is also supported).
    """  # noqa: D205, E501
    if not background_html_color or " " in background_html_color:
        # invalid colors are rendered as white so we chose black
        return "#000000"
    color = background_html_color
    if color.startswith("#"):
        color = color[1:]
    if len(color) not in [3, 6, 8]:
        return "#000000"
    if len(color) > 3:
        color = color[::2]
    light_count = sum(c >= "9" for c in color)
    if light_count >= 2 or any(c.lower() > "c" for c in color[:2]):
        return "#000000"
    return "#FFFFFF"


class Color:  # alternative? https://github.com/vaab/colour
    """Represent a color. Translating between different color representations."""

    #: red value as a float between 0 and 1
    r: float
    #: green value as a float between 0 and 1
    g: float
    #: blue value as a float between 0 and 1
    b: float

    def __init__(self, color: "Color" = None):  # noqa: D107
        if color is None:
            self.r, self.g, self.b = 0, 0, 0
        else:
            self.r, self.g, self.b = color.r, color.g, color.b

    @property
    def rgb(self):
        """red, green, blue tuple of float values between 0 and 1."""
        return self.r, self.g, self.b

    @classmethod
    def from_rgb(cls, r: float, g: float, b: float):
        """Color from r, g, b floats between 0 and 1."""
        c = cls()
        c.r, c.g, c.b = r, g, b
        return c

    @property
    def rgb255(self):
        """red, gree, blue tuple of values as int between 0 and 255."""
        return tuple(int(x * 255) for x in self.rgb)

    @classmethod
    def from_rgb255(cls, r: int, g: int, b: int):
        """Color from r, g, b ints between 0 and 255."""
        return cls.from_rgb(r / 255, g / 255, b / 255)

    @property
    def html(self):
        """Color like in HTML. e.g. ``#00ff33``."""
        r, g, b = self.rgb255
        return f"#{r:02x}{g:02x}{b:02x}"

    @classmethod
    def from_html(cls, html: str):
        """Formats like ``#0055FF`` or ``#05F`` are allowed for ``html``."""  # noqa: D401, E501
        html = html.lstrip("#")
        if len(html) == 3:
            rgb_str = tuple(html)
            max_val = int("f", base=16)
        elif len(html) == 6:
            rgb_str = html[0:2], html[2:4], html[4:6]
            max_val = int("ff", base=16)
        else:
            raise ValueError("Invalid html color string")
        rgb = (int(v, base=16) / max_val for v in rgb_str)
        return cls.from_rgb(*rgb)

    @property
    def matching_foreground(self) -> "Color":
        """Use :func:`pick_foreground_color` to select a color with high contrast."""
        return self.from_html(pick_foreground_color(self.html))
