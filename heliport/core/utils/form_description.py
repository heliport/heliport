# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Classes for describing an input form.

These are used in the :class:`heliport.custom_views.HeliportObjectListView`.
"""

import typing


class FormItem:
    """
    Represents a key value pair in a :class:`FormDescription`.
    If the ``FormItem`` is not :attr:`bound`, the value may be ``None`` or ``""``.
    """  # noqa: D205

    def __init__(self, original, value=None, unbound=False):  # noqa: D107
        self.display_name = original.display_name
        self.column_name = original.column_name
        self.attrs = original.attrs
        self.value = value
        self.unbound = unbound

    @property
    def bound(self) -> bool:
        """Determines if the ``FormItem`` is bound to some value. A property "unbound" also exists."""  # noqa: E501
        return not self.unbound

    @property
    def html_id(self) -> str:
        """Something that is usable as id in html documents."""
        return self.column_name


class FormColumn:
    """
    Represents the keys that a user is asked for in an :class:`FormDescription`.
    Information how to render a form input for this field is available via :meth:`attr` or the ``attrs`` property.
    """  # noqa: D205, E501

    def __init__(self, display_name, column_name, attrs=None):  # noqa: D107
        self.display_name = display_name
        self.column_name = column_name
        self.attrs = attrs

    def bind(self, obj: typing.Any) -> FormItem:
        """Return a :class:`FormItem` where the attribute described by this ``FormColumn`` takes on the value from ``obj``."""  # noqa: E501
        return FormItem(self, getattr(obj, self.column_name))

    def unbound(self) -> FormItem:
        """
        Return a :class:`FormItem` where the attribute described by this ``FormColumn`` takes on no value.
        (The :class:`FormItem` is "unbound").
        This is useful when a new object should be created with a form, where no current values exist.
        """  # noqa: D205, E501
        return FormItem(self, "", unbound=True)

    def attr(self, key):
        """
        Return some attribute named ``key`` intended for display of this ``FormColumn``
        taken from ``attrs`` parameter.
        """  # noqa: D205
        if self.attrs is None:
            return None
        return self.attrs.get(key)


class FormDescription:
    """
    Class for building up the description of a form including the relevant ``columns``, a potential ``obj``
    that should be edited by the form, some ``add_text`` for display next to the submit button
    (see :meth:`get_add_text`) and a ``cancel_url`` when the form is canceled.
    """  # noqa: D205, E501

    #: fields of the form
    columns: typing.List[FormColumn]

    def __init__(self):  # noqa: D107
        self.columns = []
        self.obj = None
        self.add_text = None
        self.cancel_url = "#"

    def generate_items(self) -> typing.List[FormItem]:
        """Returns bound :class:`FormItem` s if ``obj`` is specified unbound ones otherwise."""  # noqa: D401, E501
        if self.obj is None:
            return [c.unbound() for c in self.columns]
        return [c.bind(self.obj) for c in self.columns]

    def add_column(self, display_name: str, column_name: str, attrs: dict = None):
        """
        Appends a :class:`FormColumn` with the given attributes to the form.

        :param display_name: Name to be shown to the user as label for this input.
        :param column_name: Name of the attribute where the value for this input is stored on some object.
        :param attrs: Attributes to store information about presenting this input.
        """  # noqa: D401, E501
        self.columns.append(FormColumn(display_name, column_name, attrs=attrs))

    def remove_column(self, column_name: str):
        """Remove all columns with ``column_name``."""
        self.columns = [c for c in self.columns if c.column_name != column_name]

    def get_add_text(self):
        """
        The ``add_text`` property should be set to an iterable or None.
        This function returns the ``add_text`` as a list, where strings are represented as ``{"text": the_string}``
        for example::

            f = FormDescription()
            f.add_text = ["see", {"text": "this", "link": "https://..."}, "docu"]
            f.get_add_text() == [{"text": "see"}, {"text": "this", "link": "https://..."}, {"text": "docu"}]
        """  # noqa: D205, D400, D401, E501
        if self.add_text is None:
            return []

        return [t if not isinstance(t, str) else {"text": t} for t in self.add_text]

    def has_required_elements(self) -> bool:
        """Decides if a column has the :meth:`FormColumn.attr` ``"required"`` set to True."""  # noqa: E501
        return any(item.attr("required") for item in self.columns)
