# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Custom collections similar to builtin collections like `dict` or `list` but with
additional or different functionality.
"""  # noqa: D205

import typing
from collections import UserDict, defaultdict
from contextlib import ExitStack
from itertools import islice

from rdflib import Graph
from rdflib.term import BNode, Literal, URIRef
from rdflib.term import Node as RdfNode

from heliport.core.utils.url import url_encode

T = typing.TypeVar("T")


def first(collection: typing.Iterable[T]) -> typing.Optional[T]:
    """Return first element of an iterable or ``None`` if the iterable is empty."""
    try:
        return next(iter(collection))
    except StopIteration:
        return None


def merge_iterables(*iterables, block_size=1):
    """
    Yields all elements from all iterables similar to ``itertools.chain()``.
    Only ``block_size`` many items are taken from each iterable at a time.
    This allows to get elements from all iterables even if one iterable is very large
    and the result is truncated. See this example::

        from itertools import islice

        ab_generator = merge_iterables("A"*100, "BBB", block_size=2)
        ab_max_len_10 = islice(ab_generator, 10)  # still contains all Bs
        print(''.join(ab_max_len_10))  # example output: BBAABAAAAA
    """  # noqa: D205, D400, D401
    iterators = {iter(i) for i in iterables}
    while iterators:
        to_remove = []
        for i in iterators:
            block = list(islice(i, block_size))
            if block:
                yield from block
            else:
                to_remove.append(i)
        for i in to_remove:
            iterators.discard(i)


def key_to_end(d: dict, key):
    """Move ``key`` to last position in the given ``dict``."""
    d[key] = d.pop(key)


def key_to_start(d: dict, key):
    """Move ``key`` to first position in the given ``dict``."""
    for k in list(d):
        if k != key:
            key_to_end(d, k)


class SingleAssignDict(dict):
    """A `dict` that has an option to disallow reassigning keys."""

    def __init__(self, allow_reassign_key=False):  # noqa: D107
        super().__init__()
        self.allow_reassign_key = allow_reassign_key

    def __setitem__(self, key, value):  # noqa: D105
        if not self.allow_reassign_key:
            assert key not in self or value is self[key], (
                f"re-registering {key} with {value} forbidden"
            )
        super().__setitem__(key, value)


class DecoratorDict(SingleAssignDict):
    """
    A `dict` that provides :func:`register` decorator and can be used like this::

        function_collection = DecoratorDict()

        @function_collection.register("a nice function")
        def a_nice_function():
            pass

        function_collection["a nice function"]()
    """  # noqa: D400

    def __init__(self, allow_reassign_key=False):  # noqa: D107
        super().__init__(allow_reassign_key)

    def register(self, key):
        """Decorator to assign a function to `key`."""  # noqa: D401

        def register_key(value):
            self[key] = value
            return value

        return register_key


class DecoratorIndex(SingleAssignDict):
    """
    A `dict` that provides :func:`register` decorator for classes. It can be used like
    this::

        numbered_classes = DecoratorIndex("x")

        @numbered_classes.register
        class OneClass:
            x = 1

        one_instance = numbered_classes[1]()
    """  # noqa: D205, D400

    def __init__(self, field, allow_reassign_key=False):  # noqa: D107
        super().__init__(allow_reassign_key)
        self.field = field

    def register(self, value):
        """Decorator to assign a class to the key specified by `field`."""  # noqa: D401
        key = self.transform_key(getattr(value, self.field))
        self[key] = value
        return value

    def transform_key(self, key):
        """This allows to add transformations to the key in subclasses."""  # noqa: D401
        return key


class FDecoratorIndex(DecoratorIndex):
    """
    A `dict` that provides :func:`register` decorator for classes. It can be used like
    this::

        numbered_classes = FDecoratorIndex("x")

        @numbered_classes.register
        class OneClass:
            @staticmethod
            def x():
                return 1

        one_instance = numbered_classes[1]()
    """  # noqa: D205, D400

    def transform_key(self, key):  # noqa: D102
        return key()


class DecoratorList(list):
    """
    A `list` that provides :func:`register` decorator to append a function or class to the list::

        my_functions = DecoratorList()

        @my_functions.register
        def f():
            pass

        my_functions[0]()
    """  # noqa: D400, E501

    register = list.append


class IndexedSet:
    """
    ``IndexedSet`` behaves similar to pythons ``set`` however it provides additional query functions::

        Fruit = namedtuple("Fruit", "name color")
        fruits = IndexedSet()
        fruits.add(Fruit("Apple", "red"))
        fruits.add(Fruit("Walnut", "brown"))
        fruits.get_by(color="red")  # returns Fruit("Apple", "red")

    All members in an ``IndexedSet`` must be hashable.
    All methods (except :meth:`build_index`) return in constant amortized time
    for a fixed number of key combinations used in queries.
    """  # noqa: D400, E501

    def __init__(self):  # noqa: D107
        self.data = set()
        self.indexes = {}

    def add(self, item: typing.Hashable):
        """
        Add ``item`` to this ``IndexedSet``.
        ``add`` takes time linear in the number of indexes (see :meth:`filter_by`).
        """  # noqa: D205
        for index_name, index in self.indexes.items():
            index_entry = tuple(getattr(item, n) for n in index_name)
            index[index_entry].add(item)
        self.data.add(item)

    def discard(self, item: typing.Hashable):
        """
        Remove ``item`` from this ``IndexedSet`` if present.
        ``discard`` takes time linear in the number of indexes (see :meth:`filter_by`).
        """  # noqa: D205
        for index_name, index in self.indexes.items():
            index_entry = tuple(getattr(item, n) for n in index_name)
            index[index_entry].discard(item)
        self.data.discard(item)

    def remove(self, item):
        """
        Remove ``item`` from this ``IndexedSet``.
        ``remove`` takes time linear in the number of indexes (see :meth:`filter_by`).

        :raise KeyError: if not present
        """  # noqa: D205
        if item not in self:
            raise KeyError(item)
        return self.discard(item)

    def __iter__(self):  # noqa: D105
        return iter(self.data)

    def __contains__(self, item):  # noqa: D105
        return item in self.data

    def filter_by(self, **conditions) -> set:
        """
        Returns all items in this ``IndexedSet``, that meet the specified conditions, as a ``set``.
        Normally this function runs in constant time,
        however the first time, some combination of keys is used, an index is created in linear time.
        """  # noqa: D205, D401, E501
        index_name = tuple(sorted(conditions))
        index_entry = tuple(conditions[n] for n in index_name)
        index = self.indexes.get(index_name)
        if index is None:
            index = self.build_index(index_name)
            self.indexes[index_name] = index

        return index[index_entry]

    def get_by(self, **conditions):
        """
        Similar to :meth:`find_by` but returns only one item.

        :raise AssertionError: if there is no or more than one item matching the ``conditions``
        """  # noqa: E501
        result = self.filter_by(**conditions)
        assert len(result) == 1
        return next(iter(result))

    def get_any_or_none(self, **conditions):
        """
        Similar to :meth:`find_by` but returns only one item.
        If no items match ``conditions``, ``None`` is returned.
        """  # noqa: D205
        result = self.filter_by(**conditions)
        if result:
            return next(iter(result))
        return None

    def build_index(self, index_name: typing.Tuple[str]):
        """
        Used by :meth:`find_by` to build indices.

        :param index_name: Note that order matters. Please sort the tuple before passing into this function.
        """  # noqa: D401, E501
        index = defaultdict(set)
        for value in self:
            index_entry = tuple(getattr(value, n) for n in index_name)
            index[index_entry].add(value)
        return index


class HashableDictWrapper(UserDict):
    """
    Normally ``dict`` s are not hashable in python because they are mutable for the default notion of equality.
    Two ``HashableDictWrapper`` s are equal only if they are the same object
    (like classes that don't implement ``__eq__``).
    Under this notion dictionaries are immutable and can be hashable.
    """  # noqa: D205, E501

    def __init__(self, the_dict):  # noqa: D107
        super().__init__()
        self.data = the_dict

    def __eq__(self, other):  # noqa: D105
        if not isinstance(other, HashableDictWrapper):
            return False
        return self.data is other.data

    def __hash__(self):  # noqa: D105
        return hash(id(self))


class ExitContext(ExitStack):
    """Allows optimizations where expensive setup and teardown operations are reused.

    Contexts are handled safely by using Pythons ``contextlib.ExitStack``.

    In the following example the file is only opened and closed once::

        @dataclass(frozen=True)
        class File:
            name: str

        def read_file_at(pos, file_name, context):
            file = context.get(File(file_name))
            if file is None:
                file = context.set_context(File(file_name), open(file_name))
            file.seek(pos)
            print(file.read())

        with ExitContext() as context:
            read_file_at(0, "hello.txt", context)
            read_file_at(4, "hello.txt", context)
    """

    def __init__(self):  # noqa: D107
        super().__init__()
        self.data = {}

    def discard(self, key):
        """
        Prematurely exit context with this ``key``.
        The context is exited again when the ``ExitContext`` itself is closed later.
        """  # noqa: D205
        value = self.data.pop(key, None)
        if value is not None:
            context, _ = value
            if context is not None:
                context.__exit__()

    def set_context(self, key, context: typing.ContextManager[T]) -> T:
        """Enter ``context`` and store at ``key``."""
        result = self.enter_context(context)  # enter context as soon as possible
        self.discard(key)
        self.data[key] = (context, result)
        return result

    def set_value(self, key, value: T) -> T:
        """
        Similar to :meth:`set_context` but allows to insert things that are not a
        ``ContextManager`` into the ``ExitContext``.
        """  # noqa: D205
        self.discard(key)
        self.data[key] = (None, value)
        return value

    def get(self, key, default_func=None, *args, **kwargs):
        """Get value associated with key or ``None`` if no value was set.

        The exact same object is returned as from the :meth:`set` function when setting
        the key.
        If a ``default_func`` is supplied and the ``key`` was not found,
        it is called with ``*args`` and ``**kwargs``.
        The return value is than passed to :meth:`set_context` or :meth:`set_value`
        depending on if the returned value has an ``__enter__`` method.
        Finally, the result is returned. Note that the result is not always the value
        returned by ``default_func``. Given that ``f`` returns a context manager the
        following lines are equivalent with respect to the value stored in ``x``::

            x = f().__enter__()
            x = exit_context.get(new_key, f)
        """  # noqa: D205
        value = self.data.get(key)
        if value is None:
            if default_func is None:
                return None
            value = default_func(*args, **kwargs)
            try:  # using try because hasattr check may raise an error
                return self.set_context(key, value)
            # Since Python 3.11, TypeError is raised instead of AttributeError
            except (AttributeError, TypeError):
                if hasattr(value, "__enter__"):
                    raise
            return self.set_value(key, value)
        context, result = value
        return result


class RootedRDFGraph:
    """
    Represent a rdflib Graph that has a special :attr:`root` node. For example the root
    could be the thing that is described in the rdf graph.

    Example usage::

        a = rdflib.URIRef("https://example.org/a")
        graph_a = rdflib.Graph()
        graph_a.add((a, rdflib.RDFS.label, rdflib.Literal("a")))
        roted_graph_a = RootedRDFGraph(a, graph_a)  # a is described in graph_a

        b = rdflib.URIRef("https://example.org/b")
        rooted_graph_b = RootedRDFGraph(b)  # b is not described

        root_b, graph_b = rooted_graph_b  # tuple unpacking is supported
    """  # noqa: D205

    #: Root of the graph (the node that is described in the graph)
    root: RdfNode
    #: An rdflib graph that describes the :attr:`root` (may be empty)
    graph: Graph

    def __init__(self, root: RdfNode, graph: Graph = None):
        """Graph == None means empty graph."""
        if graph is None:
            graph = Graph()
        self.root = root
        self.graph = graph

    def __iter__(self):
        """Yield root then graph."""
        yield self.root
        yield self.graph

    @staticmethod
    def from_uri_str(uri: str) -> "RootedRDFGraph":
        """Generate a ``RootedRDFGraph`` that represents the ``uri``."""
        return RootedRDFGraph(URIRef(url_encode(uri)))

    @staticmethod
    def from_atomic(value) -> "RootedRDFGraph":
        """
        Generate a ``RootedRDFGraph`` that represents a simple python ``value``
        such as a string or int.
        """  # noqa: D205
        return RootedRDFGraph(Literal(value))

    @staticmethod
    def from_blank() -> "RootedRDFGraph":  # noqa: D102
        return RootedRDFGraph(BNode())
