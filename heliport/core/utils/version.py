# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Provides the HELIPORT version and other package metadata."""

from importlib.metadata import metadata
from typing import TYPE_CHECKING, Optional, Tuple

if TYPE_CHECKING:
    from importlib_metadata import PackageMetadata


def _version_tuple(string: str) -> Tuple:
    """Convert the version string into a tuple.

    Example::

       "0.6.0.post427.dev0+b868dae3"
        (0, 6, 0, 'post427', 'dev0', 'b868dae3')
    """
    major, minor, patch, *rest = string.split(".", maxsplit=3)
    if not rest:
        return int(major), int(minor), int(patch)
    rest = rest[0].replace("+", ".").split(".")
    return int(major), int(minor), int(patch), *rest


def get_project_url(project_metadata: "PackageMetadata", name: str) -> Optional[str]:
    """Get the project URL of name ``name`` from ``project_metadata``.

    If no such URL exists, ``None`` is returned.

    Project URLs are specified in the ``[project.urls]`` section of the
    ``pyproject.toml`` file. See examples for the core metadata representation here:
    https://packaging.python.org/en/latest/specifications/well-known-project-urls/#example-behavior
    """
    for project_url in project_metadata.get_all("project-url", []):
        field, value = project_url.split(", ", maxsplit=1)
        if field.casefold() == name.casefold():
            return value
    return None


#: Metadata of the HELIPORT Python package.
heliport_metadata = metadata("heliport")
#: Name of the HELIPORT Python package.
heliport_name = heliport_metadata["name"]
#: Version of the HELIPORT Python package as a dotted string.
heliport_version_string = heliport_metadata["version"]
#: Version of the HELIPORT Python package as a tuple.
heliport_version_tuple = _version_tuple(heliport_version_string)
#: URL to the HELIPORT homepage.
heliport_homepage = get_project_url(heliport_metadata, "homepage")
