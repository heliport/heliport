# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later


class UserMessage(Exception):
    """An exception that caries a message that should be shown to the user."""

    def __init__(self, message):  # noqa: D107
        super().__init__(message)
        self.message = message


class LoginInfoMissing(UserMessage):
    """
    The exception when a user has not provided a valid login info.
    In the future support for linking to a prefilled login info
    creation page may be added.
    """  # noqa: D205

    def __init__(self, message):  # noqa: D107
        super().__init__(message)


class AccessDenied(UserMessage):
    """For when permission check fails."""

    def __init__(self, message=None):  # noqa: D107
        if message is None:
            message = "You are not allowed to access this."
        super().__init__(message)
