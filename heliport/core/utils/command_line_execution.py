# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

# This might move to the ssh app in the future?

import abc
import io
import json
import logging
import socket
import typing

import cryptography
import paramiko

from heliport.core.utils import encryption
from heliport.core.utils.string_tools import remove_trailing_newline

logger = logging.getLogger(__name__)


class CommandResults:  # noqa: D101
    def __init__(self, stdout: str, stderr: str, status: int):  # noqa: D107
        self.stdout = stdout
        self.stderr = stderr
        self.status = status

    @property
    def stdout_stderr(self) -> typing.Tuple[str, str]:  # noqa: D102
        return self.stdout, self.stderr

    @property
    def status_result(self) -> typing.Tuple[int, str]:  # noqa: D102
        return self.status, self.result

    @property
    def result(self) -> str:  # noqa: D102
        if self.stdout and self.stderr:
            return self.stdout + self.stderr
        return self.stdout or self.stderr or ""

    @property
    def last_line(self) -> str:  # noqa: D102
        return self.result.splitlines()[-1]

    @property
    def last_complete_line(self) -> str:  # noqa: D102
        for line in reversed(self.result.splitlines(keepends=True)):
            clean_line = line.rstrip("\r\n")
            if line != clean_line:
                return clean_line
        return ""

    def __str__(self):  # noqa: D105
        return self.result

    def extract_http_content(self):  # noqa: D102
        http = self.stdout.split("\r\n\r\n", 1)
        if len(http) < 2:
            raise ValueError(
                f"command does not produce http response\n{self.stdout}\n{self.stderr}"
            )
        header, data = http
        return data

    def extract_http_json(self):  # noqa: D102
        return json.loads(self.extract_http_content())


class CommandExecutor:  # noqa: D101
    def __init__(  # noqa: D107
        self,
        allow_shell_syntax: bool = True,
        shell: bool = False,
        host: str = None,
        username: str = None,
        password: str = None,
        key: str = None,
        indirection: "CommandExecutor" = None,
        auto_connect: bool = True,
    ):
        self.allow_shell_syntax = allow_shell_syntax
        self.shell = shell
        self.host = host
        self.username = username
        self.password = password
        self.key = key
        self.indirection = indirection

        # use ExitStack instead
        self.to_close = []

        self.connected = False
        if auto_connect:
            self.connect()

    def __enter__(self):  # noqa: D105
        if not self.connected:
            self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):  # noqa: D105
        self.close()

    def close(self):  # noqa: D102
        while self.to_close:
            self.to_close.pop().close()

        self.connected = False

    @abc.abstractmethod
    def execute(self, command: str) -> CommandResults:  # noqa: D102
        pass

    @abc.abstractmethod
    def connect(self):  # noqa: D102
        pass

    @abc.abstractmethod
    def file(self, path: str, mode: str = "r"):  # noqa: D102
        pass


class SSHCommandExecutor(CommandExecutor):  # noqa: D101
    def __init__(  # noqa: D107
        self,
        host: str = None,
        username: str = None,
        password: str = None,
        key: str = None,
        indirection: "CommandExecutor" = None,
        auto_connect: bool = True,
        missing_host_key_policy=None,
        set_shell_to_true: bool = False,
    ):
        self.ssh = None
        self.file_server = None
        if missing_host_key_policy is None:
            missing_host_key_policy = paramiko.AutoAddPolicy()
        self.missing_host_key_policy = missing_host_key_policy

        super().__init__(
            allow_shell_syntax=True,
            shell=set_shell_to_true,
            host=host,
            username=username,
            password=password,
            key=key,
            indirection=indirection,
            auto_connect=auto_connect,
        )

    def execute(  # noqa: D102
        self, command: str, command_input: str = None
    ) -> CommandResults:
        if not self.connected:
            raise ValueError("please call connect() before executing commands")

        stdin, stdout, stderr = self.ssh.exec_command(command)
        if command_input is not None:
            print(command_input, file=stdin)
        return CommandResults(
            stdout.read().decode(),
            stderr.read().decode(),
            stdout.channel.recv_exit_status(),
        )

    @property
    def decrypted_key(self) -> typing.Optional[paramiko.RSAKey]:  # noqa: D102
        if self.key is None:
            return None

        try:
            decrypted_private_key = encryption.decrypt(self.key)
        except cryptography.fernet.InvalidToken:
            logger.warning("using unencrypted ssh key")
            decrypted_private_key = self.key

        private_key_string = io.StringIO(decrypted_private_key)
        return paramiko.RSAKey.from_private_key(private_key_string)

    def connect(self, max_depth: int = 10):  # noqa: D102
        if max_depth <= 0:
            raise ValueError(
                "max_depth is reached - this may be caused by cyclical ssh connections"
            )

        self.setup_new_ssh()
        self.ssh.connect(
            self.host,
            username=self.username,
            password=self.password,
            pkey=self.decrypted_key,
            sock=self.get_parent_channel(max_depth),
            # Never discover private keys in ~/.ssh
            look_for_keys=False,
        )

        self.to_close.append(self.ssh)
        self.connected = True

    def connect_error_message(self, hint=None):  # noqa: D102
        if hint is None:
            hint = ""
        error_message = None

        assert not self.connected, "executor is already connected"
        try:
            if self.key == "":
                error_message = f"no ssh key specified{hint}"
            else:
                self.connect()
        except cryptography.fernet.InvalidToken:
            error_message = f"could not decrypt ssh key{hint}"
        except paramiko.ssh_exception.BadAuthenticationType as e:
            allowed_types = ""
            if ";" in str(e):
                allowed_types = f" ({str(e).split(';')[-1].strip()})"
            error_message = f"authentication type not allowed{allowed_types}{hint}"
        except paramiko.ssh_exception.AuthenticationException:
            error_message = f"could not authenticate{hint}"
        except (paramiko.ssh_exception.NoValidConnectionsError, socket.gaierror):
            error_message = f'could not connect to host "{self.host}" - maybe it is temporarily not available'  # noqa: E501
        except paramiko.ssh_exception.SSHException as e:
            error_message = f"uncategorized error during ssh connection: {e}"

        return error_message

    def get_parent_channel(self, max_depth: int):  # noqa: D102
        if isinstance(self.indirection, SSHCommandExecutor):
            if not self.indirection.connected:
                self.indirection.connect(max_depth=max_depth - 1)
                self.to_close.append(self.indirection)

            channel = self.indirection.ssh.get_transport().open_session()
            channel.exec_command(f"nc {self.host} {paramiko.config.SSH_PORT}")
            return channel
        return None

    def setup_new_ssh(self):  # noqa: D102
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(self.missing_host_key_policy)

    def file(self, path: str, mode: str = "r"):  # noqa: D102
        if not self.connected:
            raise ValueError("please call connect() before opening files")

        if self.file_server is None:
            self.file_server = self.ssh.open_sftp()
        f = self.file_server.file(path, mode)
        self.to_close.append(f)
        return f

    def list_dir(self, path: str):  # noqa: D102
        if not self.connected:
            raise ValueError("please call connect() before opening files")

        if self.file_server is None:
            self.file_server = self.ssh.open_sftp()
        return self.file_server.listdir_attr(path)


class ShellSSHCommandExecutor(SSHCommandExecutor):  # noqa: D101
    def __init__(  # noqa: D107
        self,
        host: str = None,
        username: str = None,
        password: str = None,
        key: str = None,
        indirection: "CommandExecutor" = None,
        auto_connect: bool = True,
        default_result_callback: typing.Callable[[str], None] = None,
        missing_host_key_policy=None,
    ):
        self.stdin = None
        self.stdout = None
        self.command_separator = "end of stdOUT buffer. finished with exit status:"
        self.default_result_callback = default_result_callback
        super().__init__(
            host=host,
            username=username,
            password=password,
            key=key,
            indirection=indirection,
            auto_connect=auto_connect,
            missing_host_key_policy=missing_host_key_policy,
            set_shell_to_true=True,
        )

    def connect(self, max_depth: int = 10):  # noqa: D102
        super().connect(max_depth)
        channel = self.ssh.invoke_shell(width=1000)
        channel.settimeout(1)
        self.stdin = channel.makefile("wb")
        self.stdout = channel.makefile("r")

    def read_output(self) -> str:  # noqa: D102
        result = b""
        while True:
            try:
                result += self.stdout.read(1)
            except paramiko.channel.socket.timeout:
                break
        try:
            return result.decode()
        except UnicodeDecodeError:
            return "[UnicodeDecodeError shell probably paused half way through sending a multibyte UTF-8 character]"  # noqa: E501

    @property
    def echo_command_separator(self):  # noqa: D102
        return rf'printf "\n{self.command_separator} $?\n"'

    def execute(  # noqa: D102
        self, command: str, output_callback: typing.Callable[[str], None] = None
    ):
        if not self.connected:
            raise ValueError("please call connect() before executing commands")
        if output_callback is None:
            output_callback = self.default_result_callback

        command = command.strip()
        sep = "; "
        if "\n" in command:
            sep = ""
        to_write = f"{command}{sep}{self.echo_command_separator}\n"
        self.stdin.write(to_write)
        self.stdin.flush()

        exit_code, result = self.await_command_result(output_callback)
        return CommandResults(result, "", exit_code)

    def await_command_result(self, output_callback=None):  # noqa: D102
        total_result = ""
        exit_code = -1
        waiting = True
        while waiting:
            result = self.read_output()
            batch_result = ""
            for line in result.splitlines(keepends=True):
                if line.startswith(self.command_separator):
                    batch_result = remove_trailing_newline(batch_result)
                    exit_code = int(line.rsplit(maxsplit=1)[1])
                    waiting = False
                elif line.rstrip("\n\r").endswith(self.echo_command_separator):
                    line_separator = line[len(line.rstrip("\n\r")) :]
                    if ";" in line:
                        batch_result += line.rsplit(";", maxsplit=1)[0] + line_separator
                else:
                    batch_result += line

            total_result += batch_result
            if output_callback is not None:
                output_callback(batch_result)

        return exit_code, total_result


def create_command_executor(  # noqa: D103
    allow_shell_syntax: bool = True,
    shell: bool = False,
    host: str = None,
    username: str = None,
    password: str = None,
    key: str = None,
    indirection: CommandExecutor = None,
    auto_connect: bool = True,
    default_result_callback: typing.Callable[[str], None] = None,
    missing_host_key_policy=None,
) -> CommandExecutor:
    if host is None:
        raise RuntimeError("Local command execution is not allowed")

    assert username is not None, "please specify the username"

    if shell:
        error_message = "shell does not support"
        executor = ShellSSHCommandExecutor(
            host=host,
            username=username,
            password=password,
            key=key,
            indirection=indirection,
            auto_connect=auto_connect,
            missing_host_key_policy=missing_host_key_policy,
            default_result_callback=default_result_callback,
        )
    else:
        error_message = "ssh executor with shell=False does not support"
        executor = SSHCommandExecutor(
            host=host,
            username=username,
            password=password,
            key=key,
            indirection=indirection,
            auto_connect=auto_connect,
            missing_host_key_policy=missing_host_key_policy,
        )
    parameters = {
        "allow_shell_syntax": allow_shell_syntax,
        "shell": shell,
        "host": host,
        "username": username,
        "password": password,
        "key": key,
        "indirection": indirection,
        "connected": auto_connect,
    }
    for name, value in parameters.items():
        if getattr(executor, name) != value:
            raise ValueError(f"Invalid configuration: {error_message} {name}={value}")
    return executor
