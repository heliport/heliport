# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import typing
from contextlib import contextmanager
from importlib import import_module
from itertools import chain

from django.conf import settings
from django.db.models import QuerySet
from django.shortcuts import get_object_or_404
from django.utils import timezone

from heliport.core.attribute_description import ConstantAttribute
from heliport.core.models import (
    DigitalObject,
    DigitalObjectRelation,
    HeliportGroup,
    HeliportUser,
    Project,
    Tag,
    Vocabulary,
    assert_relation,
    digital_object_form_label_and_pk,
)
from heliport.core.serializers import static_attributes
from heliport.core.user_logic.user_information import get_user_information_manager
from heliport.core.utils.colors import pick_color_not_in


def create_empty_project(request, authentication_backend_manager=None, metadata=None):
    """Create an empty HELIPORT project in the database and return it.

    The following parameters are already set:
    - owner
    - group
    - category (pid namespace)
    - persistent_id (if configured in settings).
    """  # noqa: D205
    if authentication_backend_manager is None:
        authentication_backend_manager = get_user_information_manager(request)
    else:
        authentication_backend_manager = authentication_backend_manager()

    auth_user = request.user
    heliport_user = get_object_or_404(HeliportUser, auth_user=auth_user)
    metadata = metadata or {}
    project = Project(owner=heliport_user, **metadata)

    if (
        authentication_backend_manager is not None
        and (group_name := authentication_backend_manager.get_group(auth_user))
        is not None
    ):
        try:
            project.group = HeliportGroup.objects.filter(display_name=group_name).get()
        except HeliportGroup.DoesNotExist:
            new_group = HeliportGroup(display_name=group_name)
            new_group.save()
            project.group = new_group

    group_str = ""
    if project.group is not None:
        group_str = f"/{project.group}"
    project.namespace_str = (
        f"HELIPORT/Projects/{timezone.localtime().year}{group_str}/Project"
    )
    project.save()

    return project


def create_sample_project(request, authentication_backend_manager=None):
    """Create a sample HELIPORT project in the database and return it."""
    project_metadata = settings.HELIPORT_SAMPLE_PROJECT.get("project")
    project = create_empty_project(
        request,
        authentication_backend_manager=authentication_backend_manager,
        metadata=project_metadata,
    )

    object_descriptions = settings.HELIPORT_SAMPLE_PROJECT.get("objects")
    objects_by_id = {"the_project_itself": project}

    def resolve_obj(desc):
        if isinstance(desc, dict) and list(desc) == ["id"]:
            assert desc["id"] in objects_by_id, desc["id"] + " not previously defined"
            return objects_by_id[desc["id"]]
        return desc

    for description in object_descriptions:
        if "type" in description:
            module_name, _sep, class_name = description["type"].rpartition(".")
            module = import_module(module_name)
            object_class = getattr(module, class_name)
        else:
            object_class = DigitalObject

        obj = None
        pid = description.get("pid")
        if pid:
            existing = object_class.objects.filter(persistent_id=pid).first()
            if existing is not None:
                obj = existing

        if not obj:
            obj = object_class()
            for key, value in description.get("init_params", {}).items():
                setattr(obj, key, resolve_obj(value))
            if issubclass(object_class, DigitalObject):
                assert "namespace" in description, "digital object must have namespace"
                if pid:
                    obj.persistent_id = pid
                obj.save_with_namespace(description["namespace"], project)
                for key, value in description.get("relations", []):
                    assert key in objects_by_id, f"{key} not previously defined"
                    assert value in objects_by_id, f"{value} not previously defined"
                    assert_relation(obj, objects_by_id[key], objects_by_id[value])
            else:
                obj.save()
            for key, value in description.get("params", {}).items():
                setattr(obj, key, resolve_obj(value))
            obj.save()

        if "id" in description:
            objects_by_id[description["id"]] = obj

    return project


@contextmanager
def at_least_one_group():
    """
    Context with at least one group in the database::

        with at_least_one_group():
            HeliportGroup.objects.first()  # does not return None

    The group is not deleted after the context is exited.
    """  # noqa: D400
    yield HeliportGroup.get_default_group()


def get_or_create_from_label(
    label, pk, request, c_category=None, c_type=None, c_project=None, default=None
):
    """Get digital object instance by the first applicable method.

        - get by digital object id (if ``pk`` is truthy)
        - get by ``label`` (if ``label`` uniquely identifies the digital object)
        - create new digital object with label (if ``label`` is truthy)
        - return ``default``

    :param label: The label of the digital object (when need)
    :param pk: digital object id of the digital object (always used if valid)
    :param request: the current request
    :param c_category: Used when creating a new digital object.
        Sets :attr:`core.models.DigitalObject.category_str`.
        Defaults to ``DEFAULT_OBJECT_NAMESPACE`` in settings.
    :param c_type: Used when creating a new digital object.
        Must be a :class:`core.models.DigitalObject` instance.
        No type is set if not provided.
    :param c_project: Used when creating a new digital object.
        Must be a :class:`core.models.Project` instance.
        No project is added if not provided.
    :param default: the value that is returned if nothing else is applicable
    :return: a saved :class:`core.models.DigitalObject` instance or the value of
        the ``default`` parameter.

    Note: This uses :func:`core.models.digital_object_from_label_and_pk`.
    """
    label_object = digital_object_form_label_and_pk(label, pk)
    if label_object is None and not label:
        return default
    if label_object is None:
        label_object = DigitalObject(label=label)
        if c_category is None:
            c_category = settings.DEFAULT_OBJECT_NAMESPACE
        label_object.category_str = c_category
        label_object.save()
        if c_project is not None:
            label_object.projects.add(c_project)
        if c_type is not None:
            assert_relation(label_object, Vocabulary().type, c_type)
    return label_object


def digital_objects_by_type(
    the_type: typing.Union[int, str, DigitalObject],
) -> typing.List[QuerySet]:
    """
    Returns a list of QuerySets of objects of the_type.
    Each QuerySet is either a filter() of digital objects or all() objects of a model.

    :param the_type: DigitalObject that is the type. int or decimal str queries digital object by digital_object_id ; other string queries by persistent_id
    """  # noqa: D205, D401, E501
    result = []
    type_attr = Vocabulary().type

    if isinstance(the_type, str) and the_type.isdecimal():
        the_type = int(the_type)

    if isinstance(the_type, int):
        the_type = DigitalObject.objects.get(digital_object_id=the_type)

    if isinstance(the_type, str):
        type_objects = DigitalObject.objects.filter(persistent_id=the_type)
    else:
        type_objects = [the_type]

    dynamic_results = DigitalObject.objects.filter(
        relations__in=DigitalObjectRelation.objects.filter(
            predicate=type_attr, object__in=type_objects
        )
    )
    if dynamic_results.exists():
        result.append(dynamic_results)

    for a_class, attributes in static_attributes.items():
        if issubclass(a_class, DigitalObject) and any(
            isinstance(attr, ConstantAttribute)
            and attr.attribute == type_attr
            and (attr.value in type_objects or str(attr.value) == str(the_type))
            for attr in attributes()
        ):
            result.append(a_class.objects.all())

    return result


class ListOfQuerySets:
    """
    Manage a list of Django query sets. This is useful to query different kinds
    of DigitalObject subclasses simultaneously.
    """  # noqa: D205

    def __init__(self, list_of_query_sets: typing.List[QuerySet] = None):  # noqa: D107
        self.data = list_of_query_sets
        if self.data is None:
            self.data = []

    def extend(self, list_of_query_sets: typing.List[QuerySet]):
        """Add the query sets form a list of django query sets to this ``ListOfQuerySets``."""  # noqa: E501
        if isinstance(list_of_query_sets, ListOfQuerySets):
            list_of_query_sets = list_of_query_sets.data
        self.data.extend(list_of_query_sets)

    def all_digital_objects(self):
        """
        Bring this ``ListOfQuerySets`` into a state to include all
        :class:`heliport.core.models.DigitalObject` instances.
        """  # noqa: D205
        self.data = [DigitalObject.objects.exclude(label=12334555)]

    def filter(self, *args, **kwargs):
        """Filter all query sets."""
        self.data = [q.filter(*args, **kwargs) for q in self.data]

    def as_list(self, max_count=None):
        """
        Get first ``max_count`` many items from the query sets.
        ``max_count`` of ``None`` means all items.
        """  # noqa: D205
        if max_count is None:
            return list(set(chain(*self.data)))
        objects = set()
        for q in self.data:
            objects.update(q[:max_count])
            if len(objects) > max_count:
                break
        return list(objects)[:max_count]


def tags_of(
    obj: DigitalObject, limit_to_project: Project = None
) -> typing.Iterable[Tag]:
    """
    Get the tags some digital object is tagged with.
    The result can be limited to only tags belonging to some project.
    """  # noqa: D205
    tag_ids = set(
        DigitalObjectRelation.objects.filter(
            subject=obj,
            predicate=Vocabulary().has_tag,
            object__deleted__isnull=True,
        ).values_list("object", flat=True)
    )
    tags = {Tag.objects.filter(digital_object_id=tag_id).first() for tag_id in tag_ids}
    tags.discard(None)
    tags.update(inferred_tags_of(obj, limit_to_project))
    return tags


def inferred_tags_of(obj: DigitalObject, limit_to_project: Project = None):
    """Helper function to get tags inferred from metadata attributes."""  # noqa: D401
    projects = (
        [limit_to_project] if limit_to_project is not None else obj.projects.all()
    )
    if isinstance(obj, Project):
        projects = [*projects, obj]

    for possible_tag in Tag.objects.filter(projects__in=projects, deleted__isnull=True):
        if has_inferred_tag(obj, possible_tag):
            yield possible_tag


# TODO: `request` parameter can be removed
def get_or_create_tag(
    project: Project,
    request,
    label: str,
    attribute_label: str = None,
    attribute_pk: typing.Union[str, int] = None,
    value_label: str = None,
    value_pk: typing.Union[str, int] = None,
):
    """Get or create a tag based on ``label``.

    Create a new :class:`core.models.Tag` or g

    Get an existing :class:`core.models.Tag` with label ``label``, or, if none exists,
    create a new one.
    Uses :func:`get_or_create_from_label` to get/create ``attribute`` and ``value``.
    Tries to generate a ``color`` for the tag that is most different from existing
    colors of tags in the project.

    :param project: project where tag should be created or gotten from
    :param request: the current request
    :param label: label to get or create new tag.
    :param attribute_label: label of attribute that is set by setting tag
        (ignored if attribute_pk is valid)
    :param attribute_pk: digital object id of attribute that is set by setting tag.
    :param value_label: label of value that is set as attribute by setting tag
        (ignored if value_pk is valid)
    :param value_pk: digital object id of value that is set as attribute by setting tag.
    """
    tag = (
        Tag.objects.filter(projects=project, label=label, deleted__isnull=True)
        .order_by("-pk")
        .first()
    )
    if tag is not None:
        return tag

    tag = Tag.objects.create(label=label)
    tag.rgb_color = pick_tag_color(project)
    tag.category_str = settings.TAG_NAMESPACE
    tag.attribute = get_tag_attribute(attribute_label, attribute_pk, project, request)
    tag.value = get_tag_value(value_label, value_pk, tag, project, request)
    tag.save()
    tag.projects.add(project)
    return tag


def pick_tag_color(project):
    """Pick a new tag color in the context of ``project``."""
    return pick_color_not_in(
        [
            t.rgb_color
            for t in Tag.objects.filter(projects=project, deleted__isnull=True)
        ]
    )


def get_tag_attribute(label, pk, project, request):
    """Get the tag attribute from ``label`` or ``pk``."""
    vocab = Vocabulary()
    return get_or_create_from_label(
        label,
        pk,
        request,
        settings.PROPERTY_NAMESPACE,
        vocab.Property,
        project,
        default=vocab.has_tag,
    )


def get_tag_value(label, pk, tag: Tag, project: Project, request):
    """Get the tag value from ``label`` or ``pk``.

    Important: ``tag.attribute`` should already be set. The tag should be saved before
    setting ``tag.value``.
    """
    vocab = Vocabulary()
    return get_or_create_from_label(
        label,
        pk,
        request,
        settings.CLASS_NAMESPACE if tag.attribute == vocab.type else None,
        vocab.Class if tag.attribute == vocab.type else None,
        project,
        default=tag,
    )


def has_inferred_tag(obj: DigitalObject, tag: Tag):
    """Test if ``obj`` has tag ``tag`` (direct or inferred)."""
    return DigitalObjectRelation.objects.filter(
        subject=obj, predicate=tag.attribute, object=tag.value
    ).exists()


def delete_by_id(digital_object_id: typing.Union[int, str], context=None, user=None):
    """Mark the digital object with specified id as deleted."""
    assert user is not None or context is not None, "set user or context for permission"
    obj = get_object_or_404(DigitalObject, digital_object_id=digital_object_id)
    obj.set_context(context)
    obj.mark_deleted(user)
