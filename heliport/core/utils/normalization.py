# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later


def url_normalizer(value):  # noqa: D103
    if (
        not value.startswith("http://")
        and not value.startswith("https://")
        and value != ""
    ):
        return f"https://{value}"
    return value
