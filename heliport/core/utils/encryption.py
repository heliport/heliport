# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from cryptography.fernet import Fernet
from django.conf import settings


def encrypt(value: str):  # noqa: D103
    encryption = Fernet(settings.HELIPORT_CORE_CRYPTO_KEY)
    encrypted_value = encryption.encrypt(value.encode())
    return encrypted_value.decode()


def decrypt(data: str):  # noqa: D103
    encryption = Fernet(settings.HELIPORT_CORE_CRYPTO_KEY)
    decrypted_value = encryption.decrypt(data.encode())
    return decrypted_value.decode()
