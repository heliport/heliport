# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""HELIPORT template tags.

To use these include this in your template: ``{% load heliport %}``
"""

from urllib.parse import urlencode, urlsplit, urlunsplit

from django import template
from django.contrib.staticfiles.storage import staticfiles_storage
from django.http import HttpRequest
from django.urls import reverse

from heliport.core.digital_object_resolution import Resolvable
from heliport.core.digital_object_resolution import params_for as params
from heliport.core.models import DigitalObject

register = template.Library()


@register.filter(name="zip")
def zip_lists(a, b):
    """
    Pythons zip function inside templates.
    This is very useful when you have a list of objects and need some additional
    computed information for each object to render it.
    """  # noqa: D205
    return zip(a, b)


@register.filter(name="zip_dict")
def zip_dict(iterable, dict_or_single_value):
    """
    Same as :func:`zip_lists`, but the second parameter is a dict mapping indices to
    additional values. If an index is not provided in the dict, ``None`` is used as
    value.
    """  # noqa: D205, D401
    if isinstance(dict_or_single_value, dict):
        the_dict = dict_or_single_value
    else:
        the_dict = {0: dict_or_single_value}
    for i, obj in enumerate(iterable):
        yield obj, the_dict.get(i)


@register.simple_tag
def url_query(*_, **kwargs):
    """Construct a URL query string from the kwargs that is url encoded."""
    safe_args = {k: v for k, v in kwargs.items() if v is not None}
    if safe_args:
        return "?{}".format(urlencode(safe_args))
    return ""


@register.simple_tag
def project_url(url_pattern_name, project_id, *args, **kwargs):
    """
    Similar to Django's url template tag. Generates a URL for the given pattern name.
    However, this generates a URL for the
    :class:`heliport.core.views.project.OpenInProjectView` that redirects to the actual
    URL but first resets the breadcrumbs to the given project.
    It is expected that the given ``url_pattern`` takes the ``project_id`` as its first
    argument. Further arguments can be specified using more positional arguments.
    All keyword arguments are passed as query arguments.
    """  # noqa: D205
    url = f"{reverse(url_pattern_name, args=(project_id, *args))}{url_query(**kwargs)}"
    redirect = reverse("core:open_in_project", kwargs={"project": project_id})
    return f"{redirect}{url_query(next=url)}"


@register.simple_tag
def params_for(resolvable: Resolvable, **other_params):
    """
    Construct a URL query string so that ``resolve(request.GET)`` returns the original
    ``resolvable`` (using the :func:`heliport.core.digital_object_resolution.resolve`
    function) when a view handles the resulting request. You can also use
    ``other_parameters`` to additionally attach to the query string.
    """  # noqa: D205
    ps = params(resolvable)
    ps.update(other_params)
    return "?{}".format(urlencode(ps))


@register.filter
def is_digital_object(obj):
    """Determines if something is a :class:`heliport.core.models.DigitalObject`."""  # noqa: D401, E501
    return isinstance(obj, DigitalObject)


@register.filter
def type_name(obj):
    """Return the name of the type of something."""
    return str(type(obj).__name__)


@register.filter(name="repr")
def as_repr(obj):
    """Return the repr of something."""
    return repr(obj)


@register.simple_tag(takes_context=True)
def static_absolute_url(context, static_file_name: str):
    """Return the absolute URL for a static file."""
    request: HttpRequest = context["request"]
    static_file_url = staticfiles_storage.url(static_file_name)
    return request.build_absolute_uri(static_file_url)


@register.filter
def reset_breadcrumbs(url, do_reset=True):
    """Add a get parameter to the url to reset the HELIPORT breadcrumbs.

    If do_reset=False is provided breadcrumbs are blocked from resetting.
    """
    parts = urlsplit(url)
    query = f"{parts.query}&" if parts.query else ""
    parts = parts._replace(query=f"{query}reset_heliport_breadcrumbs={do_reset}")
    return urlunsplit(parts)
