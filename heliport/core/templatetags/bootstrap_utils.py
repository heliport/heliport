# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Template tags for integration with Bootstrap.

To use these include this in your template: ``{% load bootstrap_utils %}``
"""  # noqa: D205

from django import template
from django.contrib import messages

register = template.Library()


@register.simple_tag
def alert_classes_for_message_level(message_level):
    """Generate Bootstrap alert classes from Django message level."""
    levels = {
        messages.constants.DEBUG: "dark",
        messages.constants.INFO: "primary",
        messages.constants.SUCCESS: "success",
        messages.constants.WARNING: "warning",
        messages.constants.ERROR: "danger",
    }
    alert_type = levels.get(message_level, "warning")
    return f"alert alert-{alert_type}"


@register.simple_tag
def alert_classes_for_maintenance_message_level(level):
    """Generate Bootstrap alert classes from maintenance message level."""
    levels = {
        1: "secondary",
        2: "success",
        3: "info",
        4: "primary",
        5: "warning",
        6: "danger",
    }
    alert_type = levels.get(level, "dark")
    return f"alert alert-{alert_type}"
