# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.conf import settings
from django.urls import include, path, re_path, reverse_lazy
from django.views.decorators.cache import cache_page
from django.views.generic import RedirectView
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView
from rest_framework import routers

from .views.api import (
    ContributionViewSet,
    DeleteAllTokensView,
    DigitalObjectAPI,
    GroupViewSet,
    LoginNamedTokenView,
    LogoutTokenView,
    MaintenanceMessageView,
    ProjectViewSet,
    TokenViewSet,
    UserViewSet,
)
from .views.digital_objects import AsDigitalObjectView, DigitalObjectAutocompleteView
from .views.images import ImagesView, ImageView, TakePictureView
from .views.landing_page import IndexView, LandingPageView, ResolveDigitalObjectView
from .views.project import (
    CreateProjectView,
    CreateSampleProjectView,
    ImportFromProjectView,
    OpenInProjectView,
    ProjectGraphView,
    ProjectListView,
    ProjectTimelineView,
    ProjectUpdateView,
    ProjectView,
    SubprojectsView,
    get_users_autocomplete,
)
from .views.search import SearchBaseView, SearchView
from .views.tags import TagAutocompleteView, TagJsonView, TagsView
from .views.user import (
    LoginInfoUpdateView,
    LoginInfoView,
    RemoteServerLoginView,
    RemoteServerLogoutView,
    TokenListView,
    UserProfileView,
)

app_name = "core"
urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path(
        "about/",
        RedirectView.as_view(url=settings.HELIPORT_CORE_ABOUT_URL),
        name="about",
    ),
    path(
        "docs/", RedirectView.as_view(url=settings.HELIPORT_CORE_DOCS_URL), name="docs"
    ),
    path(
        "imprint/",
        RedirectView.as_view(url=settings.HELIPORT_CORE_IMPRINT_URL),
        name="imprint",
    ),
    path(
        "privacy/",
        RedirectView.as_view(url=settings.HELIPORT_CORE_PRIVACY_URL),
        name="privacy",
    ),
    path(
        "terms/", RedirectView.as_view(url=settings.HELIPORT_CORE_TOS_URL), name="tos"
    ),
    path(
        "cite/", RedirectView.as_view(url=settings.HELIPORT_CORE_CITE_URL), name="cite"
    ),
    # Auth is handled by allauth. The login, logout, and register URLs are kept for
    # their nice paths and potentially widely used names.
    path(
        "login/", RedirectView.as_view(url=reverse_lazy("account_login")), name="login"
    ),
    path(
        "logout/",
        RedirectView.as_view(url=reverse_lazy("account_logout")),
        name="logout",
    ),
    path(
        "register/",
        RedirectView.as_view(url=reverse_lazy("account_signup")),
        name="register",
    ),
    path("user/profile/", UserProfileView.as_view(), name="user_profile"),
    path("user/token/list/", TokenListView.as_view(), name="user_token_list"),
    path(
        "user/logout-all-tokens/",
        DeleteAllTokensView.as_view(),
        name="logout_all_tokens",
    ),
    path("user/login/list/", LoginInfoView.as_view(), name="user_login_list"),
    path(
        "user/login/<int:pk>/update/",
        LoginInfoUpdateView.as_view(),
        name="user_login_update",
    ),
    path(
        "user/login/remote-server-login/<int:pk>/",
        RemoteServerLoginView.as_view(),
        name="remote_server_login",
    ),
    path(
        "user/login/remote-server-logout/<int:pk>/",
        RemoteServerLogoutView.as_view(),
        name="remote_server_logout",
    ),
    path(
        "digital-object-autocomplete/",
        DigitalObjectAutocompleteView.as_view(),
        name="digital_object_autocomplete",
    ),
    # Redirect landing page URLs with missing "H" prefix.
    path(
        "object/<int:pk>/",
        RedirectView.as_view(
            permanent=True, pattern_name="core:landing_page", query_string=True
        ),
        name="legacy_landing_page",
    ),
    # Prefix "H" is required for XML serialization to work!
    path("object/H<int:pk>/", LandingPageView.as_view(), name="landing_page"),
    re_path(
        "object/(?P<identifier>.+)$",  # No trailing slash!
        ResolveDigitalObjectView.as_view(),
        name="resolve_digital_object_id",
    ),
    path("project/<int:project>/image/list/", ImagesView.as_view(), name="image_list"),
    path(
        "project/<int:project>/image/take/",
        TakePictureView.as_view(),
        name="take_image",
    ),
    path(
        "image/<int:pk>/data/",
        cache_page(4 * 24 * 60 * 60, cache="image_cache")(ImageView.as_view()),
        name="image_data",
    ),
    path("project/<int:project>/tag/list/", TagsView.as_view(), name="tag_list"),
    path(
        "project/<int:project>/tag/<int:pk>/update/",
        TagsView.as_view(),
        name="tag_update",
    ),
    path(
        "project/<int:project>/tag/list/json/",
        TagJsonView.as_view(),
        name="tag_json",
    ),
    path(
        "project/<int:project>/tag/autocomplete/",
        TagAutocompleteView.as_view(),
        name="tag_autocomplete",
    ),
    path(
        "project/<int:project>/subprojects/",
        SubprojectsView.as_view(),
        name="subprojects",
    ),
    path(
        "project/<int:project>/import/",
        ImportFromProjectView.as_view(),
        name="import_project",
    ),
    path("project/list/", ProjectListView.as_view(), name="project_list"),
    path(
        "project/create/sample/",
        CreateSampleProjectView.as_view(),
        name="project_create_sample",
    ),
    path("project/create/", CreateProjectView.as_view(), name="project_create"),
    path(
        "project/<int:project>/graph/", ProjectGraphView.as_view(), name="project_graph"
    ),
    path(
        "project/<int:project>/timeline/",
        ProjectTimelineView.as_view(),
        name="project_timeline",
    ),
    path("project/<int:project>/", ProjectView.as_view(), name="project"),
    path(
        "project/<int:project>/update/",
        ProjectUpdateView.as_view(),
        name="project_update",
    ),
    path("project/<int:project>/autocomplete-user/", get_users_autocomplete),
    path("search-base/", SearchBaseView.as_view(), name="search_base"),
    path("search/", SearchView.as_view(), name="search"),
    path(
        "project/<int:project>/as-digital-object/",
        AsDigitalObjectView.as_view(),
        name="as_digital_object",
    ),
    path(
        "project/<int:project>/redirect/",
        OpenInProjectView.as_view(),
        name="open_in_project",
    ),
]

# REST API
router = routers.DefaultRouter()
router.register(r"projects", ProjectViewSet, basename="projects")
router.register(r"users", UserViewSet, basename="users")
router.register(r"groups", GroupViewSet, basename="groups")
router.register(r"contributions", ContributionViewSet, basename="contributions")
router.register(r"tokens", TokenViewSet, basename="tokens")
router.register(
    r"digital-objects/complete", DigitalObjectAPI, basename="digital-objects-complete"
)

urlpatterns += [
    path("api/auth/delete-token/", LogoutTokenView.as_view(), name="delete_token"),
    path("api/auth/login-named/", LoginNamedTokenView.as_view(), name="login_named"),
    path("api/auth/", include("knox.urls")),
    path(
        "api/maintenance-message/",
        MaintenanceMessageView.as_view(),
        name="maintenance_message",
    ),
    path("openapi/", SpectacularAPIView.as_view(), name="openapi-schema"),
    path(
        "redoc/",
        SpectacularRedocView.as_view(url_name="core:openapi-schema"),
        name="redoc",
    ),
]
