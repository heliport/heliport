// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
//
// SPDX-License-Identifier: GPL-3.0-or-later

import $ from "jquery";
import "jquery-ui";
import "jquery-ui/ui/widgets/autocomplete";
import "jquery-ui/ui/widgets/draggable";
import Alpine from "alpinejs";
import "bootstrap";
import "htmx.org";
import "htmx.org/dist/ext/json-enc";

const _hyperscript = require("hyperscript.org");
_hyperscript.browserInit();

window.Alpine = Alpine;
Alpine.start();
