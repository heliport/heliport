// SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
//
// SPDX-License-Identifier: GPL-3.0-or-later

/*
 Adds value from data-row-show to x-show of nearest table row.

 This is useful because in Django specifying attributes for form elements is
 easy while the same is not true for their parent rows.

 Because some forms might not be shown as table, also elements that have
 data-is-row="true" are counted as rows.
 */

for (let element of $("[data-row-show]")) {
  let node = $(element);
  let condition = node.attr("data-row-show");
  let tr = node.closest("tr");
  let row = node.closest("[data-is-row='true']");
  if (row.length === 0 || tr.parents().is(row)) row = tr;
  row.attr("x-show", condition);
}
