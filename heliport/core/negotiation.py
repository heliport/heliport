# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Custom content negotiation."""

from rest_framework.negotiation import DefaultContentNegotiation


class HeliportContentNegotiation(DefaultContentNegotiation):
    """Custom content negotiation for the HELIPORT API.

    This class reuses most functionality of Django REST Framework's
    :class:`DefaultContentNegotiation` but for requests made with HTMX, it selects the
    :class:`heliport.core.renderers.HeliportPartialRenderer` when appropriate.

    More info on custom content negotiation can be found in the `Django REST Framework
    docs <https://www.django-rest-framework.org/api-guide/content-negotiation/#custom-content-negotiation>`_.
    """

    def select_renderer(self, request, renderers, format_suffix=None):
        """Select the appropriate renderer for the response to ``request``.

        The implementation reuses logic from :class:`DefaultContentNegotiation` but
        explicitly asks it to return a renderer with ``format_suffix="partial"`` if
        appropriate and supported by the view. We consider it appropriate to use a
        partial renderer for requests made with HTMX that either accept all media types
        (``*/*``, the HTMX default) or specifically ask for ``text/html``.
        """
        if (
            request.htmx
            and request.accepts("text/html")
            and self.filter_renderers(renderers, "partial")
        ):
            format_suffix = "partial"
        return super().select_renderer(request, renderers, format_suffix)
