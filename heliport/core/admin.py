# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Registers Django admin pages.

See :class:`django.contrib.admin.ModelAdmin` from Django documentation.
"""

from django.contrib import admin

from heliport.core.models import (
    Contribution,
    DigitalObject,
    DigitalObjectIdentifier,
    DigitalObjectRelation,
    HeliportGroup,
    HeliportUser,
    Image,
    LoginInfo,
    Project,
)

admin.site.register(HeliportGroup)
admin.site.register(DigitalObjectRelation)
admin.site.register(Contribution)
admin.site.register(Image)


@admin.register(DigitalObject)
class DigitalObjectAdmin(admin.ModelAdmin):
    """Admin interface to list digital objects with labels and IDs."""

    list_display = [
        "digital_object_id",
        "label",
        "persistent_id",
        "generated_persistent_id",
    ]


@admin.register(DigitalObjectIdentifier)
class DigitalObjectIdentifierAdmin(admin.ModelAdmin):  # noqa: D101
    list_display = [
        "digital_object_identifier_id",
        "scheme",
        "immutable",
        "identifier",
        "display_text",
        "url",
        "digital_object",
    ]


@admin.register(HeliportUser)
class HeliportUserAdmin(admin.ModelAdmin):
    """Display and make searchable the given attributes on the admin site."""

    list_display = ["display_name", "affiliation", "email", "orcid"]
    search_fields = [
        "display_name",
        "affiliation",
        "orcid",
        "stored_email",
        "auth_user__email",
    ]


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):  # noqa: D101
    filter_horizontal = ("co_owners",)


@admin.register(LoginInfo)
class LoginInfoAdmin(admin.ModelAdmin):
    """Admin interface that only shows non-sensitive information."""

    fields = ["user", "name", "type"]
