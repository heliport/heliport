# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later
from __future__ import annotations

import abc
import logging
from string import ascii_letters
from typing import TYPE_CHECKING, Optional, Type, Union

from django.apps import apps
from django.conf import settings
from django.forms import BaseForm

from heliport.core.digital_object_interface import GeneralDigitalObject
from heliport.core.utils.collections import DecoratorIndex

if TYPE_CHECKING:
    from heliport.core.models import DigitalObject

logger = logging.getLogger(__name__)


#: Register heliport modules to this in apps.py of your django app.
#: Call :meth:`heliport.core.utils.collections.DecoratorIndex.register` with your
#: :class:`Module` or :class:`DigitalObjectModule` for this:
#: ``heliport_modules.register(MyModule)``.
heliport_modules: DecoratorIndex[str, Module] = DecoratorIndex("module_id")


class Module(abc.ABC):
    """Base class for HELIPORT modules.

    A HELIPORT module is the most basic way to add an extension to heliport.
    A module can for example be configured to be shown in the HELIPORT project graph.

    If your module represents a type of :class:`heliport.core.models.DigitalObject`,
    it is better to use :class:`DigitalObjectModule` instead.
    """

    @property
    def icon(self) -> str:
        """The icon to be used when displaying the module.

        Usually a fontawesome class string.
        """
        first_letter = self.name[0].lower()
        if first_letter not in ascii_letters:
            return "fa-solid fa-question"
        return f"fa-solid fa-{first_letter}"

    @property
    @abc.abstractmethod
    def name(self) -> str:
        """The name displayed to a user representing this module."""

    @property
    @abc.abstractmethod
    def module_id(self) -> str:
        """The id that can be used to refer to this module in settings."""

    @abc.abstractmethod
    def get_url(self, project) -> str:
        """Return url to lead a user to when clicking on the module."""

    @abc.abstractmethod
    def is_configured(self, project) -> bool:
        """Whether the module is in use for the project.

        This affects if the module is shown in the project graph. Modules that are
        configured to have this module as dependency will only be available if this
        returns ``True``. Modules in the navbar or project drop-down menu are always
        shown.

        This may for example check if the user created at least one object in the
        module for that project.
        """


class DigitalObjectModule(Module):
    """A :class:`Module` that enables managing a subclass of Digital Object.

    Subclass this and set :attr:`Module.name`, :attr:`Module.module_id`,
    :attr:`Module.icon`, :attr:`object_class` and implement :meth:`Module.get_url`.
    """

    @property
    @abc.abstractmethod
    def object_class(self) -> Union[Type[DigitalObject], tuple[Type[DigitalObject]]]:
        """Define the types of objects that this module can manage.

        This can be a type, or a ``tuple`` of types, if a module manages multiple
        classes (e.g. files and directories).

        All returned types must be subclasses of
        :class:`heliport.core.models.DigitalObject`.
        """

    @property
    def form_class(self) -> Optional[Type[BaseForm]]:
        """Return form that enables creating and updating objects for this Module.

        This is optional and defaults to ``None``. If ``None`` is returned, users of
        this function may fall back to linking to the result of :meth:`get_url`.

        The returned form class should accept the following key word parameters:

        - ``context``: Can be used to get current user or project. Additionally, it
            allows cacheing within the context of a request and closing resources later.
        - ``instance``: The instance to be edited or ``None`` to create a new instance.
           Django ``ModelForms`` already accept this parameter. Make sure to check that
           the current module :meth:`can_handle` the instance before passing it to the
           form.

        The returned form class should have a ``save()`` method. Django ``ModelForms``
        already provide this method.

        Using Media elements like javascript in the form is fully supported.
        Additionally, you can have an attribute ``alpine_data`` on the form. This is
        passed as an ``x-data`` attribute to a parent element enabling reactive alpine
        functionality. For example this form synchronizes two text inputs::

            class MyForm(Form):
                alpine_data = "{val: ''}"
                a = CharField(widget=TextInput(attrs={"x-model": "val"}))
                b = CharField(widget=TextInput(attrs={"x-model": "val"}))
        """
        return None

    def is_configured(self, project):
        """Whether the module is used in the given project.

        Returns ``True`` if any of the digital objects handled by this module are
        part of the project and not deleted.
        """
        return any(
            cls.objects.filter(projects=project, deleted__isnull=True).exists()
            for cls in self.object_classes
        )

    def matches(self, digital_object_class):
        """Determine whether this module manages a specific class.

        Both exact matches and subclasses of the managed class are considered a match.
        """
        return any(issubclass(cls, digital_object_class) for cls in self.object_classes)

    def can_handle(self, instance):
        """Determine whether this module manages a specific instance.

        Both exact matches and superclasses of the managed class are considered.
        """
        return any(isinstance(instance, cls) for cls in self.object_classes)

    @property
    def object_classes(self) -> tuple[Type[DigitalObject]]:
        """Same as :attr:`object_class` but is always a tuple."""
        if isinstance(self.object_class, tuple):
            return self.object_class
        return (self.object_class,)


def is_module_cls(potential_module_class):
    """Determine whether the given class is a HELIPORT module.

    Checks if the given class is a subclass of :class:`Module` and not abstract.
    """
    return (
        isinstance(potential_module_class, type)
        and issubclass(potential_module_class, Module)
        and not getattr(potential_module_class, "__abstractmethods__", set())
    )


class ModuleSection:  # noqa: D101
    def __init__(self, name, project, is_configured_cache):  # noqa: D107
        self.name = name
        self.project = project
        self.modules = []
        self.is_configured_cache = is_configured_cache

    def is_configured(self, module):  # noqa: D102
        if module.name not in self.is_configured_cache:
            self.is_configured_cache[module.name] = module.is_configured(self.project)
        return self.is_configured_cache[module.name]

    def add(self, module):  # noqa: D102
        if module is not None:
            self.modules.append(module)

    @property
    def included(self):  # noqa: D102
        for module in self.modules:
            if self.is_configured(module):
                module.url = module.get_url(self.project)
                yield module

    @property
    def included_list(self):  # noqa: D102
        return list(self.included)

    @property
    def addable(self):  # noqa: D102
        module_index = get_module_index()
        configured_modules = {
            name for name, module in module_index.items() if self.is_configured(module)
        }
        for module in self.modules:
            fulfilled = all(
                module in configured_modules
                for module in settings.HELIPORT_DEPENDENCIES.get(module.module_id, [])
            )
            if not self.is_configured(module) and fulfilled:
                module.url = module.get_url(self.project)
                yield module

    @property
    def addable_list(self):  # noqa: D102
        return list(self.addable)


class PreconfiguredModuleSection(ModuleSection):  # noqa: D101
    def is_configured(self, module):  # noqa: D102
        return True


interface_warning_apps = set()
heliport_system_apps = set(settings.HELIPORT_SYSTEM_APPS)


def get_heliport_apps():  # noqa: D103
    for name, app in apps.app_configs.items():
        if hasattr(app.module, "interface"):
            yield name, app.module.interface
        elif (
            app.name in heliport_system_apps and app.name not in interface_warning_apps
        ):
            logger.warning(
                f'App "{app.name}" has no interface. '
                'Did you forget "from . import interface" in __init__.py?'
            )
            interface_warning_apps.add(app.name)


is_first_index = True


def get_module_index():  # noqa: D103
    index = {id_: module_cls() for id_, module_cls in heliport_modules.items()}

    for _, interface in get_heliport_apps():
        for module_cls in vars(interface).values():
            if is_module_cls(module_cls):
                module = module_cls()
                index[module.module_id] = module

    global is_first_index
    if is_first_index:
        is_first_index = False
        logger.debug(f"installed modules: {list(index)}")

    return index


def get_modules_for_type(
    digital_object_type: Union[Type[DigitalObject], Type[GeneralDigitalObject]],
) -> dict[str, DigitalObjectModule]:
    """Get all registered HELIPORT modules that manage a given type.

    The result is a dict mapping :attr:`Module.module_id` to a
    :class:`DigitalObjectModule` instance for each matching module.

    :param digital_object_type: The type of digital objects to look for.
        :attr:`DigitalObjectModule.object_class` can only be a subclass of DigitalObject
        currently. But
        :class:`heliport.core.digital_object_interface.GeneralDigitalObject` is also
        part of the type hint to allow search for specific digital object aspects.
        (see :mod:`heliport.core.digital_object_aspects`)
    """
    return {
        module_id: module_instance
        for module_id, module_instance in get_module_index().items()
        if isinstance(module_instance, DigitalObjectModule)
        and module_instance.matches(digital_object_type)
    }


def get_module_for_url(url: str, project) -> Optional[Module]:
    """Find a module under the given ``url``.

    Checks each module if ``url`` is returned by :meth:`Module.get_url` given
    ``project``.

    Returns ``None`` if no module matches.
    """
    results = [
        module
        for module in get_module_index().values()
        if module.get_url(project) == url
    ]
    if not results:
        return None
    return results[0]


def generate_graph(project):  # noqa: D103
    module_index = get_module_index()
    graph = []
    is_configured_cache = {}

    for section_name, module_names in settings.HELIPORT_GRAPH.items():
        section = ModuleSection(section_name, project, is_configured_cache)
        for module_name in module_names:
            section.add(module_index.get(module_name))

        graph.append(section)

    return graph


def project_graph_section_for(module: Module) -> Optional[str]:
    """Determine the section a heliport module is in.

    Returns the first section if a project is in multiple sections.
    Returns ``None`` if the project is in no section.
    Returns ``"project dropdown"`` if a module is in the project dropdown.
    Returns ``"project navbar"`` if a module is configured to be in the project navbar.
    """
    if module.module_id in settings.HELIPORT_PROJECT_DROPDOWN_ITEMS:
        return "project dropdown"
    if module.module_id in settings.HELIPORT_PROJECT_NAVBAR_ITEMS:
        return "project navbar"
    for section_name, module_names in settings.HELIPORT_GRAPH.items():
        if module.module_id in module_names:
            return section_name
    return None


def generate_project_dropdown(project):
    """Get :class:`Module` instances that are configured for dropdown in settings.

    The "project" dropdown is in the HELIPORT base template in the navbar.
    Use the setting ``HELIPORT_PROJECT_DROPDOWN_ITEMS`` to set this.
    """
    module_index = get_module_index()
    is_configured_cache = {}

    modules = PreconfiguredModuleSection("", project, is_configured_cache)
    for module_name in settings.HELIPORT_PROJECT_DROPDOWN_ITEMS:
        modules.add(module_index.get(module_name))

    return modules


def generate_project_navbar(project):
    """Get :class:`Module` instances that are configured for navbar in settings.

    The navbar is in the HELIPORT base template at the top right.
    Use the setting ``HELIPORT_PROJECT_NAVBAR_ITEMS`` to set this.

    For contents of the "project" navbar-item see :func:`generate_project_dropdown`.
    """
    module_index = get_module_index()
    is_configured_cache = {}

    modules = PreconfiguredModuleSection("", project, is_configured_cache)
    for module_name in settings.HELIPORT_PROJECT_NAVBAR_ITEMS:
        modules.add(module_index.get(module_name))

    return modules


def get_search_urls():  # noqa: D103
    results = []
    for _, interface in get_heliport_apps():
        if hasattr(interface, "get_search_url"):
            results.append(interface.get_search_url())
    return results


def get_project_create_urls():  # noqa: D103
    results = {}
    for _, interface in get_heliport_apps():
        if hasattr(interface, "get_project_create_urls"):
            results.update(interface.get_project_create_urls())
    return results
