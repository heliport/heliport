# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Django app configuration.

Some HELIPORT hooks can be registered in app config :meth:`django.apps.AppConfig.ready`.

See also :ref:`Django documentation <django:ref/applications:applications>`
"""

import sys

from django.apps import AppConfig
from django.contrib.auth.signals import user_logged_in
from django.db.models.signals import post_migrate, post_save

from heliport.core.vocabulary_core import insert_core_vocabulary


def post_migrate_handler(**kwargs):  # noqa: D103
    insert_core_vocabulary()


class HeliportConfig(AppConfig):
    """App configuration for the main HELIPORT app.

    This is part of Django's plugin system.
    """

    name = "heliport.core"

    def ready(self):
        """Register hooks and import settings."""
        from heliport.core.conf import HeliportAppConf
        from heliport.core.models import DigitalObject
        from heliport.core.signal_handlers import register_heliport_identifier_handler

        assert HeliportAppConf
        post_migrate.connect(post_migrate_handler, sender=self)

        # Only connect this handler if tests are not running. This is necessary in order
        # to allow force_login on a user during tests without triggering this update.
        if not any("test" in arg for arg in sys.argv):
            from heliport.core.user_logic.user_information import (
                update_heliport_user_and_group,
            )

            user_logged_in.connect(update_heliport_user_and_group)

        # NOTE: This only handles immediate subclasses!
        post_save.connect(register_heliport_identifier_handler, sender=DigitalObject)
        for subclass in DigitalObject.__subclasses__():
            post_save.connect(register_heliport_identifier_handler, subclass)
