# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf import settings
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import DetailView

from heliport.core.mixins import HeliportProjectMixin
from heliport.core.models import (
    DigitalObject,
    DigitalObjectRelation,
    Project,
    Tag,
    Vocabulary,
)
from heliport.core.permissions import is_object_member
from heliport.core.utils.form_description import FormDescription
from heliport.core.utils.queries import (
    get_or_create_tag,
    get_tag_attribute,
    get_tag_value,
    tags_of,
)
from heliport.core.utils.string_tools import bool_from_url
from heliport.core.views import HeliportObjectListView


class TagsView(HeliportObjectListView):  # noqa: D101
    #: Specify that this View shows lists of class :class:`heliport.core.models.Tag`.
    model = Tag
    category = settings.TAG_NAMESPACE
    columns = [
        ("ID", "tag_id", "small"),
        ("Label", "label", "small"),
        ("Property", "attribute", "normal"),
        ("Value", "value", "normal"),
        ("Used on", "requires_type_display", "small"),
    ]
    actions = [
        ("Edit", "action_edit", "link_secondary"),
        ("Delete", "action_delete", "danger"),
    ]
    list_url = "core:tag_list"
    update_url = "core:tag_update"
    list_heading = "Tags"
    create_heading = "Add a Tag"
    edit_tags = True

    def register_form_columns(self, form: FormDescription):
        """Register custom form columns. Called by :meth:`heliport.core.custom_views.HeliportObjectListView.get_table`."""  # noqa: E501
        vocab = Vocabulary()
        form.add_column("Label", "label", attrs={"required": True})
        form.add_column(
            "Color", "html_color", attrs={"widget": "color", "required": True}
        )
        form.add_column(
            "Property",
            "attribute",
            attrs={
                "widget": "digital_object",
                "type": vocab.Property.pk,
                "help_text": "Optional property of tag: Property. A tag can describe that "  # noqa: E501
                "the tagged object has <Property> set to <Value>. "
                'For example a tag "Raw Data" describes "type" is set to "Raw Data".',
            },
        )
        form.add_column(
            "Value",
            "value",
            attrs={
                "widget": "digital_object",
                "help_text": "Optional property of tag: Value. A tag can describe that "
                "the tagged object has <Property> set to <Value>. "
                'For example a tag "Raw Data" describes "type" is set to "Raw Data".',
            },
        )
        form.add_column(
            "Used on",
            "requires_type",
            attrs={
                "widget": "digital_object",
                "type": vocab.Class.pk,
                "help_text": "the type on which the tag should be used",
            },
        )

    def set_update_attributes(self, obj, form_fields, obj_saved=False):
        """Custom implementation of setting attributes."""  # noqa: D401
        super().set_update_attributes(obj, form_fields, obj_saved=obj_saved)

        if obj_saved:
            return

        if not hasattr(obj, "attribute") or obj.attribute is None:
            obj.attribute = get_tag_attribute(
                label=self.request.POST.get("form_data_object_label_attribute"),
                pk=None,
                project=self.project,
                request=self.request,
            )

        if not hasattr(obj, "value") or obj.value is None:
            obj.save()
            obj.value = get_tag_value(
                label=self.request.POST.get("form_data_object_label_value"),
                pk=None,
                tag=obj,
                project=self.project,
                request=self.request,
            )


class TagJsonView(HeliportProjectMixin, DetailView):
    """
    Handle Querying and updating tags of a digital object.
    This view is used in javascript of tag_manager.html.
    """  # noqa: D205

    #: Django model handled in this view - used by django ``DetailView``
    model = Project
    #: In :mod:`heliport.core.urls` project id is called "project" not "pk"
    pk_url_kwarg = "project"

    def get_project_and_object(self, object_pk):
        """
        Get the project that contains the tags and the digital object that is tagged.
        ``object_pk`` is used to get the digital object. It is checked that the
        digital object is part of the project or is itself the project as auth check.
        """  # noqa: D205
        project = self.get_object()
        if str(project.digital_object_id) == str(object_pk):
            digital_object = project
        else:
            digital_object = get_object_or_404(
                DigitalObject, pk=object_pk, projects=project
            )
        return project, digital_object

    def get(self, request, *args, **kwargs):
        """
        Return list of tags including "label", "tag_id", "color", "foreground", ... for
        a given DigitalObject in the context of the project.

        Called by django on http get.
        """  # noqa: D205
        project, digital_object = self.get_project_and_object(self.request.GET["pk"])
        return JsonResponse(self.get_all_tags(digital_object, project), safe=False)

    @staticmethod
    def get_all_tags(digital_object, project):
        """
        Get direct (has_tag property) and indirect (There exists a tag with
        predicate-object-combination also found in attributes of ``digital_object`` in
        ``project``) Tags. Tags are returned as dict of "label",
        "tag_id", "attribute", "value", "requires_type", "color", "foreground".
        """  # noqa: D205
        tags = [
            tag.as_dict for tag in tags_of(digital_object, limit_to_project=project)
        ]
        return sorted(tags, key=lambda t: t["label"])

    def post(self, request, *args, **kwargs):
        """
        Adds (or removes when ``"delete"`` is set to a truthy value) a tag on a digital
        object specified by ``"pk"``.
        If ``"check_type"`` is set to a truthy value the tag is only set when digital
        object has the correct type for tag.
        The tag can be explicitly specified by ``"tag"`` or created from
        ``"label"``, ``"attribute_pk"`` and ``"value_pk"``.
        ``"attribute_label"`` and ``"value_label"`` are also allowed and their
        information is combined with attribute_pk and value_pk using
        :func:`heliport.core.models.digital_object_form_label_and_pk`.

        :return: Same as :meth:`get`

        Called by django on http post.
        """  # noqa: D205, D401
        project, digital_object = self.get_project_and_object(request.POST["pk"])
        if "tag" in request.POST or request.POST.get("delete"):
            tag = get_object_or_404(Tag, pk=request.POST.get("tag"))
        else:
            tag = get_or_create_tag(
                project,
                request,
                request.POST.get("label", "new tag"),
                request.POST["attribute_label"],
                request.POST["attribute_pk"],
                request.POST["value_label"],
                request.POST["value_pk"],
            )
        tag.update_usage_date()

        if request.POST.get("delete"):
            tag.unset_on(digital_object)
        elif not request.POST.get("check_type") or tag.applicable_for(digital_object):
            tag.set_on(digital_object)
            if isinstance(digital_object, Project):
                tag.projects.add(digital_object)

        return JsonResponse(self.get_all_tags(digital_object, project), safe=False)


class TagAutocompleteView(HeliportProjectMixin, DetailView):
    """Handle autocompletion when specifying tags."""

    #: Django model handled in this view - used by django ``DetailView``
    model = Project
    #: In :mod:`heliport.core.urls` project id is called "project" not "pk"
    pk_url_kwarg = "project"

    def get(self, request, *args, **kwargs):
        """
        Partial input can be submitted via "q" or "term".
        If "pk" is specified, only tags allowed for this DigitalObject are returned unless there are none.

        Tags in result json are returned with "label" and "pk" ordered by least recently used.

        Called by django on http get.
        """  # noqa: D205, E501
        project = self.get_object()
        digital_object = DigitalObject.objects.filter(
            projects=project, pk=request.GET.get("pk")
        ).first()
        pattern = request.GET.get("q") or request.GET.get("term", "")
        project_independent = bool_from_url(request.GET.get("project_independent"))

        tags = Tag.objects.filter(
            deleted__isnull=True,
            label__icontains=pattern,
        )
        if project_independent:
            tags = tags.filter(is_object_member(request.user.heliportuser))
        else:
            tags = tags.filter(projects=project)

        if digital_object is not None:
            obj_types = DigitalObjectRelation.objects.filter(
                subject=digital_object, predicate=Vocabulary().type
            ).values_list("object", flat=True)
            filtered_tags = tags.filter(
                Q(requires_type__in=obj_types) | Q(requires_type__isnull=True)
            )
            if len(filtered_tags) > 0:
                tags = filtered_tags

        return JsonResponse(
            [
                {"label": tag.label, "pk": tag.pk}
                for tag in tags.order_by("-last_use_date")
            ],
            safe=False,
        )
