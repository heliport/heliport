# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later
import logging
from itertools import chain

from django.conf import settings
from django.contrib import messages
from django.db.models import F, Max
from django.db.models.functions import Coalesce, Greatest, Lower
from django.forms import ValidationError
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils import timezone
from django.views.generic import (
    DetailView,
    ListView,
    RedirectView,
    TemplateView,
    UpdateView,
    View,
)

from heliport.core.app_interaction import generate_graph, get_project_create_urls
from heliport.core.forms import validate_orcid
from heliport.core.mixins import HeliportLoginRequiredMixin, HeliportProjectMixin
from heliport.core.models import (
    Contribution,
    DigitalObject,
    DigitalObjectRelation,
    HeliportGroup,
    HeliportUser,
    Project,
    Tag,
    Vocabulary,
)
from heliport.core.permissions import (
    has_read_permission,
    has_write_permission,
    projects_of,
)
from heliport.core.serializers import types
from heliport.core.user_logic.user_information import get_user_information_manager
from heliport.core.utils.context import project_header_breadcrumbs
from heliport.core.utils.queries import (
    at_least_one_group,
    create_empty_project,
    create_sample_project,
)
from heliport.core.utils.string_tools import remove_prefix

logger = logging.getLogger(__name__)


class ProjectListView(HeliportLoginRequiredMixin, ListView):
    """Handle listing a users HELIPORT project."""

    #: Django model handled in this view - used by django ``ListView``
    model = Project
    #: HTML template rendered by this View
    template_name = "core/project/project_list.html"
    #: Pagination
    paginate_by = 10

    def get_context_data(self, **kwargs):
        """Called by django to get context when rendering template."""  # noqa: D401
        context = super().get_context_data(**kwargs)
        context["project_create_urls"] = get_project_create_urls()
        context["sort"] = self.request.GET.get("sort")
        tag_id = self.request.GET.get("tag")
        if tag_id:
            context["filter_tag"] = Tag.objects.filter(tag_id=tag_id).first()
        return context

    def get_queryset(self):
        """Called by ListView to get the list to show.
        Query: projects of user that are not deleted; handle sorting; no subprojects.
        """  # noqa: D205, D401
        user = self.request.user.heliportuser
        query = projects_of(user).filter(deleted__isnull=True)

        tag_id = self.request.GET.get("tag")
        if tag_id:
            tag = Tag.objects.filter(tag_id=self.request.GET.get("tag")).first()
            if tag is not None:
                query = query.filter(relations__in=tag.relation_query_set())

        query_including_subprojects = query
        query = query.exclude(projects__deleted__isnull=True, projects__isnull=False)
        if not query.exists():
            query = query_including_subprojects

        sort = self.request.GET.get("sort", "")
        if remove_prefix(sort, "-") == "modified":
            query = query.annotate(
                sub_last_modified=Coalesce(Max("parts__last_modified"), "last_modified")
            )
            query = query.annotate(
                modified=Greatest("sub_last_modified", "last_modified")
            )

        if remove_prefix(sort, "-") in [
            "label",
            "modified",
            "created",
            "owner__display_name",
        ]:
            return query.order_by(sort)
        return query.order_by("-created")


class ProjectGraphView(HeliportProjectMixin, DetailView):
    """Handle showing the HELIPORT project graph."""

    #: HTML template rendered by this View
    template_name = "core/project/project_graph.html"
    #: Django model handled in this view - used by django ``DetailView``
    model = Project
    #: Expect url parameter "project" instead of "pk"
    pk_url_kwarg = "project"

    def get_breadcrumb_label_and_reset(self):  # noqa: D102
        return (self.get_object().label, True)

    def get_context_data(self, **kwargs):
        """Generate heliport project graph."""
        context = super().get_context_data(**kwargs)
        graph = generate_graph(self.object)
        context["graph"] = graph
        sections_count = len(list(graph))
        context["graph_size_large"] = 480 * sections_count
        context["graph_size_small"] = 370 * sections_count

        return context


class ProjectTimelineView(HeliportProjectMixin, TemplateView):
    """Show a timeline of all digital objects in a project, including the project."""

    #: HTML template rendered by this View
    template_name = "core/project/timeline.html"

    def get_context_data(self, **kwargs):
        """Called by django to get context when rendering template.
        Query all not deleted digital objects in project ordered by creation date.
        """  # noqa: D205, D401
        context = super().get_context_data(**kwargs)
        project = context["project"]
        context["digital_objects"] = chain(
            project.parts.filter(deleted__isnull=True).order_by("-created"), [project]
        )
        return context


class CreateSampleProjectView(HeliportLoginRequiredMixin, View):
    """Create sample HELIPORT project."""

    def post(self, request):
        """Create sample project in HELIPORT database."""
        project = create_sample_project(request)
        return redirect("core:project_graph", project=project.pk)


class CreateProjectView(HeliportLoginRequiredMixin, TemplateView):
    """Create new HELIPORT project.

    Including options for specifying initial project settings and importing contents of
    other projects.
    """

    #: HTML template rendered by this View (page for entering initial project settings)
    template_name = "core/project/create_project.html"

    def get_context_data(self, **kwargs):
        """Called by django to get context when rendering template.

        Context includes group information and list of projects of current user.
        """  # noqa: D401
        context = super().get_context_data(**kwargs)

        with at_least_one_group() as g:
            context["groups"] = HeliportGroup.objects.order_by("display_name")
            if (mgr := get_user_information_manager(self.request)) is not None:
                context["default_group"] = mgr.get_group(self.request.user)
            else:
                context["default_group"] = g
        context["projects"] = projects_of(self.request.user.heliportuser).order_by(
            Lower("label")
        )

        return context

    def post(self, request, *args, **kwargs):
        """Create project and configure settings.

        If template project is selected to import digital objects from, redirect to
        :class:`ImportFromProjectView`.

        Called by django on http post.
        """
        group_pk = request.POST.get("group")
        project = create_empty_project(request)
        project.label = request.POST.get("label")
        project.description = request.POST.get("description")
        project.group = get_object_or_404(HeliportGroup, pk=group_pk)
        project.save()
        template_project = request.POST.get("template")
        if template_project:
            url = reverse("core:import_project", kwargs={"project": project.pk})
            return redirect(f"{url}?from={template_project}")
        return redirect("core:project_graph", project=project.pk)


class ProjectView(HeliportProjectMixin, DetailView):
    """Show general information about a HELIPORT project ("Project Configuration").

    This includes key value pairs like the Title, description or identifiers of a
    project and information about project participants.

    Basic project actions, like deleting the project, can be done via this view too.
    """

    #: Django model handled in this view - used by django ``DetailView``
    model = Project
    #: HTML template rendered by this View
    template_name = "core/project/project_update.html"
    #: In :mod:`heliport.core.urls` project id is called "project" not "pk"
    pk_url_kwarg = "project"

    def get_context_data(self, **kwargs):
        """
        Called by django to get context when rendering template.

        The context includes:

            - "isOwner", "update" to configure the template for what should be shown
            - preprocessed information about project participants
            - "groups" the list of all groups to display a select box
        """  # noqa: D401
        context = super().get_context_data(**kwargs)
        context["isOwner"] = self.object.owner == self.request.user.heliportuser
        with at_least_one_group():
            context["groups"] = HeliportGroup.objects.order_by("display_name")
        context["co_owners"] = sorted(context["project"].co_owners.all(), key=str)
        context["contributions"] = sorted(
            context["project"].contribution_set.all(), key=str
        )
        context["contribution_types"] = Contribution.ContributionTypes.choices
        context["contribution_type_default"] = Contribution._meta.get_field(
            "type"
        ).get_default()
        context["vocab"] = Vocabulary()
        context["update"] = False
        context["identifiers"] = context["project"].identifiers.all()
        return context

    def post(self, request, *args, **kwargs):
        """Respond to POST request.

        Handles one of:

            - Delete project
            - Add file system user group to project
            - Add / Remove co-owner
            - Add all members of a group as co-owners
            - Add / Delete Contribution
            - Edit contribution type (request is send to this view but ui is rendered in
              :class:`ProjectUpdateView`)
        """
        remove = request.POST.get("remove")
        user_id = request.POST.get("add")
        delete_owner = self.request.POST.get("user")
        group = self.request.POST.get("group_to_import")
        contribution_delete = request.POST.get("contribution_delete")
        contributor_name = request.POST.get("contributor_name")
        contributor_affiliation = request.POST.get("contributor_affiliation")
        contributor_orcid = request.POST.get("contributor_orcid")
        contributor_email = request.POST.get("contributor_email")
        contribution_type = request.POST.get("contribution_type")
        contribution_modify = request.POST.get("contribution_modify")
        contribution_type_update = request.POST.get("contribution_type_update")

        project = self.get_object()

        # hand-made validation as we're not using a Django form here.
        if contributor_orcid:
            try:
                validate_orcid(contributor_orcid)
            except ValidationError as e:
                messages.add_message(request, messages.ERROR, e.messages[0])
                return redirect("core:project", project=project.pk)

        if remove:
            if project.owner == request.user.heliportuser:
                project.deleted = timezone.now()
                project.save()
            return JsonResponse({"Location": reverse("core:project_list")})

        if user_id:
            user = get_object_or_404(HeliportUser, pk=int(user_id))
            if project.owner != user:
                project.co_owners.add(user)

        elif delete_owner:
            user = get_object_or_404(HeliportUser, pk=int(delete_owner))
            project.co_owners.remove(user)

        elif group:
            db_group = get_object_or_404(HeliportGroup, pk=int(group))
            if (mgr := get_user_information_manager(request)) is not None:
                for db_user in mgr.get_users_by_group(db_group):
                    if db_user != project.owner:
                        project.co_owners.add(db_user)

        elif contribution_delete:
            # This will leave HeliportUser objects that are essentially useless. But at
            # this point we can't really decide if there was another purpose for this
            # user.
            contribution = get_object_or_404(Contribution, pk=int(contribution_delete))
            contribution.delete()

        elif contributor_name:
            user = HeliportUser(
                display_name=contributor_name,
                affiliation=contributor_affiliation,
            )
            if contributor_orcid:
                user.orcid = contributor_orcid
            if contributor_email:
                user.stored_email = contributor_email
            user.save()
            Contribution(
                contributor=user,
                contribution_to=project,
                type=contribution_type,
            ).save()

        elif contribution_modify and contribution_type_update:
            contribution = get_object_or_404(Contribution, pk=int(contribution_modify))
            contribution.type = contribution_type_update
            contribution.save()

        project.update_modified_date()

        return redirect("core:project", project=project.pk)


class ProjectUpdateView(HeliportProjectMixin, UpdateView):
    """Update project title, description and group.

    This renders the same template as :class:`ProjectView`.
    """

    #: Django model handled in this view - used by django ``UpdateView``
    model = Project
    #: HTML template rendered by this View
    template_name = "core/project/project_update.html"
    #: Used by django ``UpdateView`` to generate input form for editing project
    fields = ["label", "description", "group"]
    #: In :mod:`heliport.core.urls` project id is called "project" not "pk"
    pk_url_kwarg = "project"

    def get_context_data(self, **kwargs):
        """See :meth:`ProjectView.get_context_data` for more information."""
        context = super().get_context_data(**kwargs)
        context["isOwner"] = self.object.owner == self.request.user.heliportuser
        with at_least_one_group():
            context["groups"] = HeliportGroup.objects.order_by("display_name")
        context["co_owners"] = sorted(context["project"].co_owners.all(), key=str)
        context["contributions"] = sorted(
            context["project"].contribution_set.all(), key=str
        )
        context["contribution_types"] = Contribution.ContributionTypes.choices
        context["vocab"] = Vocabulary()
        context["update"] = True
        return context

    def get_success_url(self):
        """Return to :class:`ProjectView` after updating and saving project."""
        return reverse("core:project", kwargs={"project": self.object.pk})


class SubprojectsView(HeliportProjectMixin, DetailView):
    """Handle other HELIPORT projects that are part of the current project."""

    #: HTML template rendered by this View
    template_name = "core/project/subprojects.html"
    #: Django model handled in this view - used by django ``DetailView``
    model = Project
    #: In :mod:`heliport.core.urls` project id is called "project" not "pk"
    pk_url_kwarg = "project"

    def get_context_data(self, **kwargs):
        """Called by django to get context when rendering template.
        Subprojects, all projects of user and user are included in context.
        """  # noqa: D205, D401
        context = super().get_context_data(**kwargs)
        project = self.object
        context["subprojects"] = Project.objects.filter(
            projects=self.object, deleted__isnull=True
        )
        context["user_projects"] = list(projects_of(self.request.user.heliportuser))
        parents = project.get_parent_projects()
        context["projects_is_valid"] = [
            project not in parents for project in context["user_projects"]
        ]
        context["deleted_user_projects_exist"] = any(
            project.deleted for project in context["user_projects"]
        )
        context["heliport_user"] = self.request.user.heliportuser
        return context

    def post(self, request, **kwargs):
        """Add or remove a subproject."""
        project = self.get_object()
        user = self.request.user.heliportuser
        to_remove = request.POST.get("remove")
        to_add = request.POST.get("add")
        if to_remove:
            subproject = get_object_or_404(Project, pk=to_remove)
            assert user in project.members()
            subproject.projects.remove(project)
        if to_add:
            subproject = get_object_or_404(Project, pk=to_add)
            assert user in project.members()
            assert user in subproject.members()
            assert subproject not in project.get_parent_projects(), "cyclic subproject"
            subproject.projects.add(project)
        return redirect("core:subprojects", project=project.pk)


class ImportFromProjectView(HeliportProjectMixin, TemplateView):
    """Handle importing or copying digital objects from another project."""

    #: HTML template rendered by this View
    template_name = "core/project/import_from.html"

    def test_func(self):
        """
        Called by django to test if user has access to this view.
        In addition to normal test if user has access to current project also test if user has read access to project
        where things should be imported from.
        """  # noqa: D205, E501
        if not super().test_func():
            return False

        other_project = get_object_or_404(Project, pk=self.request.GET.get("from"))
        user = self.request.user.heliportuser
        return has_read_permission(user, other_project)

    def get_context_data(self, **kwargs):
        """
        Called by django to get context when rendering template.

        Include list of object that can be imported in context, including their label, types and id.
        Also for each object, the "default_type" (completely different meaning than "types";
        one of ignore, copy, import) and "copy_disabled" (bool; not every object is copyable) is included.
        """  # noqa: D401, E501
        vocab = Vocabulary()

        def get_default_import_type(obj, past_count):
            """Compute default type based on usage pattern in the past."""
            copy_count = DigitalObjectRelation.objects.filter(
                object=obj, predicate=vocab.is_version_of
            ).count()
            import_count = obj.projects.count() - 1
            if copy_count * 4 <= past_count and import_count * 4 <= past_count:
                return "ignore"
            if copy_count >= import_count:
                return "copy"
            return "import"

        context = super().get_context_data(**kwargs)
        other_project = get_object_or_404(Project, pk=self.request.GET.get("from"))
        past_import_projects = set()
        for obj in DigitalObject.objects.filter(projects=other_project):
            past_import_projects.update(obj.projects.all())
        for relation in DigitalObjectRelation.objects.filter(
            object__projects=other_project, predicate=vocab.is_version_of
        ):
            past_import_projects.update(relation.subject.projects.all())
        past_import_projects.discard(other_project)

        context["objects_to_import"] = sorted(
            [
                {
                    "label": obj.label or obj.description,
                    "types": types(obj.casted_or_self(), bound_attributes=True),
                    "default_type": get_default_import_type(
                        obj, len(past_import_projects)
                    ),
                    "id": obj.digital_object_id,
                    "copy_disabled": hasattr(obj.casted_or_self(), "disable_copy")
                    and obj.casted_or_self().disable_copy,
                }
                for obj in other_project.parts.filter(deleted__isnull=True)
                if has_read_permission(self.request.user.heliportuser, obj)
            ],
            key=lambda x: str(x["label"] or "").lower(),
        )
        context["objects_to_import_len"] = len(context["objects_to_import"])
        context["from"] = other_project
        return context

    def post(self, request, *args, **kwargs):
        """Do the importing / copying. "import_objects" is the list of all objects.
        E.g. for obj with pk 7 a value import-type-7 (import, copy, ignore) specifies the kind of import.
        """  # noqa: D205, E501
        user = self.request.user.heliportuser
        current_project = get_object_or_404(Project, pk=self.kwargs["project"])
        if not has_write_permission(user, current_project):
            return redirect("account_login")

        for obj_id in self.request.POST.getlist("import_objects"):
            obj = get_object_or_404(DigitalObject, digital_object_id=obj_id)
            obj = obj.casted_or_self()
            if not has_read_permission(user, obj):
                continue
            import_type = self.request.POST.get(f"import-type-{obj_id}")
            if import_type == "import":
                obj.projects.add(current_project)
            elif import_type == "copy" and not (
                hasattr(obj, "copy_disabled") and obj.copy_disabled
            ):
                new_obj = obj.create_copy()
                new_obj.projects.add(current_project)

        return redirect("core:project_graph", project=current_project.pk)


class OpenInProjectView(RedirectView):
    """
    This view has special handling in the
    :func:`heliport.core.utils.context.project_header_breadcrumbs` function
    to allow resetting the breadcrumbs to the project name.
    """  # noqa: D205

    def get_redirect_url(self, *args, **kwargs):
        """Reset breadcrumbs and redirect to ``next`` specified by query parameter."""
        project = get_object_or_404(Project, project_id=self.kwargs["project"])
        project_header_breadcrumbs(
            self.request, project.label, self.request.path, reset=True
        )
        return self.request.GET["next"]


def get_users_autocomplete(request, **kwargs):
    """
    View that handles user autocomplete. The query string is taken from get parameter `q`.
    Respects ``HELIPORT_CORE_AUTOCOMPLETE_COUNT`` in settings.py.

    Returns json like::

        [
            {"name": "ax", "pk": 1},
            {"name": "xyyy", "pk": 55}
        ]
    """  # noqa: D205, E501
    query = request.GET.get("q")

    if query:
        sql_query = HeliportUser.objects.filter(display_name__icontains=query)
    else:
        sql_query = HeliportUser.objects.all()
    sql_query = sql_query.order_by(F("auth_user__last_login").desc(nulls_last=True))

    results = []
    names = set()
    for guy in sql_query[: settings.HELIPORT_CORE_AUTOCOMPLETE_COUNT]:
        if guy.display_name not in names:
            results.append({"name": guy.display_name, "pk": guy.pk})
            names.add(guy.display_name)

    return JsonResponse(results, safe=False)
