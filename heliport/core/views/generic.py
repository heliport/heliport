# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later
import logging
from typing import Generic, Optional, Type, TypeVar

from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import PermissionDenied
from django.db.models import QuerySet
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.views.generic import TemplateView
from django.views.generic.edit import ModelFormMixin, ProcessFormView

from heliport.core.app_interaction import (
    generate_project_dropdown,
    generate_project_navbar,
)
from heliport.core.mixins import HeliportBreadcrumbsMixin, HeliportLoginRequiredMixin
from heliport.core.models import (
    DigitalObject,
    HeliportUser,
    Project,
    Vocabulary,
    digital_object_form_label_and_pk,
)
from heliport.core.utils.context import Context
from heliport.core.utils.exceptions import AccessDenied, UserMessage
from heliport.core.utils.form_description import FormDescription
from heliport.core.utils.table_description import (
    BoundTableAction,
    TableAction,
    TableDescription,
)

logger = logging.getLogger(__name__)


T = TypeVar("T", bound=DigitalObject)


class HeliportCreateAndUpdateView(
    HeliportLoginRequiredMixin, HeliportBreadcrumbsMixin, TemplateView, Generic[T]
):
    """For writing a HELIPORT view that can create and update digital objects.

    This combines the functionality of
    :class:`heliport.core.mixins.HeliportProjectMixin` and
    :class:`heliport.core.mixins.HeliportObjectMixin` allowing to have two URLs handled
    by the same view instead of having a separate create and update view with
    potentially lots of duplicated code.

    By default, the URL patterns could look something like this::

        project/<int:project>/my-object/<int:pk>/update/
        project/<int:project>/my-object/list

    Look at :attr:`project_url_kwarg`, :attr:`pk_url_kwarg` and :attr:`model` for more
    detail.

    A typical view could look like this::

        class MyView(HeliportCreateAndUpdateView[MyDigitalObjectSubclass]):
            model = MyDigitalObjectSubclass
            list_url = "myapp:list"
            update_url = "myapp:update"
            template_name = "myapp/my_object_template.html"

            def get_context_data(self, **kwargs):
                context = super().get_context_data(**kwargs)
                context["my_key"] = "my_value"
                return context

            def post(self, request, *args, **kwargs):
                # handle post data
                self.object.save_with_namespace("my/namespace", self.project)
                return self.get_response()

    This view provides quite a few helpful properties to use:

      - :attr:`project`
      - :attr:`object`
      - :attr:`user`
      - :attr:`context`
      - :attr:`is_create` / :attr:`is_update`

    The following auth checks are always automatically performed:

      - read and write access to :attr:`project`
      - read and write access to :attr:`object` object is not ``None``
      - check that :attr:`object` is part of the :attr:`project`

    The view automatically handles when
    :class:`heliport.core.utils.exceptions.UserMessage` instances are raised in your
    code.
    """

    #: The name of the url parameter where the project pk is taken from.
    #: See also :meth:`get_project`.
    project_url_kwarg = "project"
    #: The name of the url parameter where the digital object is taken from.
    #: See also :attr:`model` and :meth:`get_object`.
    pk_url_kwarg = "pk"
    #: The type of :class:`heliport.core.models.DigitalObject` to use in this View.
    #: This is important for what id is used as :attr:`pk_url_kwarg`.
    #: The default is :class:`heliport.core.models.DigitalObject` which means the
    #: pk argument of the URL is the
    #: :attr:`heliport.core.models.DigitalObject.digital_object_id`.
    #: If you set a model class the primary key of your class is used.
    #: Note that :attr:`object` is always converted to the most specific class no matter
    #: this parameter.
    model: Type[T] = DigitalObject
    #: This is required by the default implementation of :meth:`get_response`
    #: to redirect back to the list after successful operation.
    #: See also :attr:`HeliportObjectListView.list_url` for a similar concept.
    list_url: str
    #: This is required by the default implementation of :meth:`get_response`
    #: to redirect back to the list after unsuccessful operation.
    #: See also :attr:`HeliportObjectListView.update_url` for a similar concept.
    update_url: str
    #: The current HELIPORT Project. This is taken from :meth:`get_project`.
    project: Project
    #: The current digital object to update, or ``None`` if this view is not called with
    #: :attr:`pk_url_kwarg` in the URL.
    object: Optional[T]
    #: All digital objects of the provided model for the current project as Django
    #: query set. Deleted objects are not included.
    objects: QuerySet[T]
    #: The current user taken from the request.
    user: HeliportUser
    #: A context usable for all kinds of operations in HELIPORT. See also the
    #: documentation of :class:`heliport.core.utils.context.Context`. Note that the
    #: context manager is already handled for you. This also means if you need the
    #: context after the request is handled (e.g. for streaming download) you need to
    #: make sure the context is not exited.
    context: Context

    def get_project(self) -> Project:
        """Get project based on :attr:`project_url_kwarg`.

        Overwrite this for custom behaviour.
        """
        return get_object_or_404(Project, pk=self.kwargs[self.project_url_kwarg])

    def get_object(self) -> Optional[T]:
        """Get the object if :attr:`pk_url_kwarg` is in the URL.

        Overwrite this for custom behaviour.

        Note that it is not required to call
        :meth:`heliport.core.models.DigitalObject.casted_or_self`.
        DigitalObject subclasses may have additional security checks, therefore this
        cast is critical and performed automatically when :attr:`object` is set.
        """
        if self.pk_url_kwarg not in self.kwargs:
            return None

        return get_object_or_404(self.model, pk=self.kwargs[self.pk_url_kwarg])

    def get_response(self, error=False):
        """Get default response returned by :meth:`post`.

        This is also used when a :class:`heliport.core.utils.exceptions.UserMessage`
        is raised during get or post to return a response that shows the message on it.

        The default behaviour is to redirect to the :attr:`list_url`. When ``error`` is
        ``True`` a redirect to the next item of the following list is returned to avoid
        infinite redirect loops:

        - update view
        - list view
        - heliport project graph

        Note that the kwarg :attr:`project_url_kwarg` is automatically supplied when
        generating the URL from :attr:`list_url`.
        """
        if not error:
            return redirect(self.list_url, **{self.project_url_kwarg: self.project.pk})

        if self.is_update and self.request.method == "POST":
            return redirect(
                self.update_url,
                **{
                    self.project_url_kwarg: self.project.pk,
                    self.pk_url_kwarg: self.object.pk,
                },
            )
        if self.is_update or self.request.method == "POST":
            return redirect(self.list_url, **{self.project_url_kwarg: self.project.pk})
        return redirect("core:project_graph", project=self.project.pk)

    @property
    def is_create(self):
        """Whether this view is in create mode (creating a new object on POST).

        This is ``True`` if :attr:`pk_url_kwarg` is not provided in the current URL.

        Opposite of :attr:`is_update`.
        """
        return not self._is_update

    @property
    def is_update(self):
        """Whether this view is in update mode (updating an object on POST).

        This is ``True`` if :attr:`pk_url_kwarg` is provided in the current URL.

        Opposite of :attr:`is_update`.
        """
        return self._is_update

    def __init__(self, *args, **kwargs):
        """Initialize."""
        super().__init__(*args, **kwargs)
        self.project = None
        self.user = None
        self.object = None
        self.objects = None
        self.context = None
        self._is_update = None

    def get_context_data(self, **kwargs):
        """Add some useful values to the template rendering context.

        The following are added:

        - ``project``: :attr:`project`
        - ``object``: :attr:`object`
        - ``objects``: :attr:`objects`
        - ``heliport_user``: :attr:`user`
        - ``is_create``: :attr:`is_create`
        - ``is_update``: :attr:`is_update`
        - ``update``: :attr:`is_update` - the old convention

        Also, the following are added and are used by the heliport base template:
        ``project_dropdown_modules``
        (see :func:`heliport.core.app_interaction.generate_project_dropdown`)
        and ``project_navbar_modules``
        (see :func:`heliport.core.app_interaction.generate_project_navbar`)
        """
        context = super().get_context_data(**kwargs)
        context["project"] = self.project
        context["object"] = self.object
        context["objects"] = self.objects
        context["heliport_user"] = self.user
        context["is_update"] = self.is_update
        context["update"] = self.is_update
        context["is_create"] = self.is_create
        context["project_dropdown_modules"] = generate_project_dropdown(self.project)
        context["project_navbar_modules"] = generate_project_navbar(self.project)
        return context

    def assert_permission(self):
        """Raise error if current user has not the right permissions.

        This is automatically called during dispatch of any HTTP methods.

        The following permissions are checked:

        - permission for :attr:`project`
        - permission for :attr:`object` when not ``None``
        - :attr:`object` must be part of :attr:`project`
        - permission for the :attr:`objects` is not checked as it is assumed a user
          always is allowed to access all the objects in a project.

        Permission is checked via
        :meth:`heliport.core.permission.PermissionChecker.assert_permission` on the
        :class:`heliport.core.permission.PermissionChecker` returned by
        :meth:`heliport.core.models.DigitalObject.access`.
        If a DigitalObject subclass overwrites this method it is essential that the
        object is casted to the correct subclass before the permission check. This is
        done by calling :meth:`heliport.core.models.DigitalObject.casted_or_self` during
        every dispatch on the DigitalObject returned by :meth:`get_object` if it is not
        ``None``.
        """
        self.project.access(self.context).assert_permission()
        if self.object is not None:
            self.object.access(self.context).assert_permission()
        if self.object is not None and self.project not in self.object.projects.all():
            logger.error(
                f"""incorrect project when opening {self.object}.
  Project: {self.project.pk}; Available: {[p.pk for p in self.object.projects.all()]}"""
            )
            raise AccessDenied(
                f"The object {self.object.pk} is not in the project {self.project.pk}."
            )

    def dispatch(self, request, *args, **kwargs):
        """Dispatch a HTTP request.

        This is called by Django and handles the following things:

        - set :attr:`user`, :attr:`project` and :attr:`object`
        - set :attr:`context` and handle it properly using a context manger
        - set context on :attr:`object` if not ``None``
        - catch :class:`heliport.core.utils.exceptions.AccessDenied` - raise HTTP error
        - handle :class:`heliport.core.utils.exceptions.UserMessage` - show to user
        - check permission using :meth:`assert_permission`
        - convert result of :meth:`get_object` to correct subclass
        """
        self.user = request.user.heliportuser
        self.project = self.get_project()
        uncasted_object = self.get_object()
        if uncasted_object is None:
            self.object = None
        else:
            self.object = uncasted_object.casted_or_self()
        self.objects = self.model.objects.filter(
            projects=self.project, deleted__isnull=True
        )
        self._is_update = self.object is not None
        try:
            with Context(self.user, self.project) as context:
                self.context = context
                if self.object is not None:
                    self.object.set_context(self.context)
                self.assert_permission()
                return super().dispatch(request, *args, **kwargs)

        except AccessDenied as m:
            messages.error(request, m.message)
            raise PermissionDenied(m.message) from m
        except UserMessage as m:
            messages.error(request, m.message)
            return self.get_response(error=True)
        finally:
            self.context = None


class HeliportFormView(
    HeliportCreateAndUpdateView[T], ModelFormMixin, ProcessFormView, Generic[T]
):
    """A thin wrapper around :class:`HeliportCreateAndUpdateView`.

    The purpose is to also support forms by using Django ``ModelFormMixin``.

    A typical view could look like this::

        class MyView(HeliportCreateAndUpdateView[MyDigitalObjectSubclass]):
            model = MyDigitalObjectSubclass
            form_class = MyDigitalObjectSubclassForm
            list_url = "myapp:list"
            update_url = "myapp:update"
            template_name = "myapp/my_object_template.html"

    You can use all the functionality of normal model form views in Django. For example
    ``fields = ["field1", "field2"]`` instead of ``form_class`` works.
    """

    def get_success_url(self):
        """Go to list url after successful form saving."""
        return reverse(self.list_url, kwargs={self.project_url_kwarg: self.project.pk})


class HeliportObjectListView(
    HeliportLoginRequiredMixin,
    UserPassesTestMixin,
    HeliportBreadcrumbsMixin,
    TemplateView,
):
    """Base view to get a simple list view of some type of Digital Objects.

    No template is needed, just some simple configuration.
    There is still some flexibility and hence alot of configuration options.
    To get something quickly, coppy and paste an existing example and modify it for your
    needs.

    This view requires to set the :attr:`list_url` to a registered url that points to
    this view.

    It is usually required to set the :attr:`model` property to the subclass of
    :class:`heliport.core.models.DigitalObject` that should be listed and to create two
    urls that point to this view which can be passed as :attr:`list_url` and
    :attr:`update_url`.
    """

    #: Instances of this model are shown in the list and created/edited
    model = DigitalObject
    #: HTML templated rendered by this View
    template_name = "core/base/simple_table_view.html"
    #: Columns shown in the table. Triples of column name, attribute shown in column, column size.  # noqa: E501
    #: For more control over columns overwrite :meth:`register_columns`.
    #: See implementation of :meth:`register_columns` for definition of column sizes
    columns = [("ID", "pk", "small"), ("Name", "label", "normal")]
    #: Actions that are available for each column. Triples of action name, method name called on the HeliportObjectListView  # noqa: E501
    #: and style information. For more control over action overwrite :meth:`register_actions`.  # noqa: E501
    #: See implementation of :meth:`register_actions` for definitions of style information.  # noqa: E501
    actions = [
        ("Edit", "action_edit", "link_secondary"),
        ("Delete", "action_delete", "danger"),
    ]
    #: Fields editable in this view.
    #: Triple of key name, attribute name ware value is stored and field style
    #: For more control over form fields overwrite :meth:`register_form_columns`.
    #: See implementation of :meth:`register_form_columns` for definitions of field styles.  # noqa: E501
    edit_fields = [("Name", "label", "normal")]
    #: List of attribute names (second value in triples of :attr:`edit_fields`) that should  # noqa: E501
    #: be set after ``obj.save()`` is called when creating an ``obj``.
    after_save_attributes = ()
    #: Text displayed as heading
    list_heading = "HELIPORT Object List"
    #: Text displayed as heading over form when creating obj. See also :meth:`get_create_heading`.  # noqa: E501
    create_heading = None
    #: Text displayed as heading over form when editing obj. See also :meth:`get_update_heading`.  # noqa: E501
    update_heading = None
    #: An HTML template containing a snippet to be used to provide additional
    #: information to the "Add" section.
    info_snippet_add = None
    #: Namespace used for handle generation
    category = "HELIPORT/thing"
    #: Url that leads to this view for updating.
    #: The "update_url" string is passed to :func:`django.urls.reverse`
    #: with "project" and "pk" url arguments to get the actual url.
    #: ``update_url`` is required by the default :meth:`action_edit` implementation.
    update_url = None
    #: Url that leads to this view for listing objects.
    #: The `list_url` attribute should be a string that is valid when passed to Django's
    #: `reverse()` function with kwarg "project".
    #: ``list_url`` is required by the default :meth:`action_edit` implementation to
    #: link back to original page after editing.
    list_url = None
    #: List of strings that are concatenated and put next to the add button for creating a new object.  # noqa: E501
    #: See :meth:`utils.form_description.FormDescription.get_add_text` for more information.  # noqa: E501
    add_text = []
    #: True if tags should be editable in this view
    edit_tags = True

    def __init__(self):
        """Set project to None. It is set to the real project in :meth:`get` and :meth:`post`."""  # noqa: E501
        super().__init__()
        self.project = None

    def test_func(self):
        """Called by Djangos ``UserPassesTestMixin``. Make sure that user is a project member."""  # noqa: E501
        user = self.request.user.heliportuser
        return user in self.get_object().members()

    def get_object(self):
        """Return the project for which a list of objects should be shown."""
        return get_object_or_404(Project, pk=self.kwargs["project"])

    def get_update_heading(self):
        """
        Return :attr:`update_heading`. Generate an update heading if this is not specified.
        Generation is done for example based on :attr:`create_heading` by replacing a prefix "Add" or "Create" with "Update".
        """  # noqa: D205, E501
        if self.update_heading is not None:
            return self.update_heading
        if self.create_heading is not None:
            create_heading = self.create_heading
            create_prefixes = ["Add ", "Create "]
            for p in create_prefixes:
                if create_heading.startswith(p):
                    create_heading = create_heading[len(p) :]
            return f"Update {create_heading}"
        return "Update an Object"

    def get_create_heading(self):
        """
        Return :attr:`create_heading`. Generate a creation heading if this is not specified.
        Generation is done for example based on :attr:`update_heading` by replacing a prefix of "Update" or "Edit" with "Add".
        """  # noqa: D205, E501
        if self.create_heading is not None:
            return self.create_heading
        if self.update_heading is not None:
            update_heading = self.update_heading
            update_prefixes = ["Update ", "Edit "]
            for p in update_prefixes:
                if update_heading.startswith(p):
                    update_heading = update_heading[len(p) :]
            return f"Add {update_heading}"
        return "Add an Object"

    def get_table(self) -> TableDescription:
        """Helper function for table generation. This calls :meth:`register_columns` and
        :meth:`register_actions` to populate the table description.
        """  # noqa: D205, D401
        table = TableDescription()
        self.register_columns(table)
        self.register_actions(table)
        table.dynamic_action_callback = self.dynamic_actions

        table.data = self.model.objects.filter(
            projects=self.project, deleted__isnull=True
        )
        return table

    def get_form(self) -> FormDescription:
        """Helper function for form generation. This calls :meth:`register_form_columns`
        to populate the form description.
        """  # noqa: D205, D401
        form = FormDescription()
        self.register_form_columns(form)

        form.add_text = self.add_text
        if "pk" in self.kwargs:
            form.obj = self.get_update_instance(self.kwargs["pk"])
        if self.list_url is not None:
            form.cancel_url = reverse(
                self.list_url, kwargs={"project": self.project.pk}
            )
        return form

    def get_update_instance(self, pk):
        """Returns an instance of :attr:`model` to be updated."""  # noqa: D401
        return get_object_or_404(self.model, pk=pk)

    def get(self, request, *args, **kwargs):
        """Called by django on http get. Sets ``project`` for which the list should be shown."""  # noqa: D401, E501
        self.project = self.get_object()
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        Called by django to get context when rendering template.
        Collects information like :attr:`list_heading` and generates table (:meth:`get_table`) and
        form (:meth:`get_form`) into context.
        """  # noqa: D205, D401, E501
        context = super().get_context_data(**kwargs)
        form = self.get_form()
        table = self.get_table()
        context["project"] = self.project
        context["project_dropdown_modules"] = generate_project_dropdown(self.project)
        context["project_navbar_modules"] = generate_project_navbar(self.project)
        context["list_heading"] = self.list_heading
        context["update_heading"] = self.get_update_heading()
        context["create_heading"] = self.get_create_heading()
        context["info_snippet_add"] = self.info_snippet_add
        context["update"] = "pk" in self.kwargs
        context["edit_tags"] = self.edit_tags
        context["table"] = table
        context["form_description"] = form
        context["vocab"] = Vocabulary()
        if any(c.attr("widget") == "login" for c in form.columns):
            context["logins"] = self.request.user.heliportuser.logininfo_set.all()
        return context

    def dynamic_actions(self, obj):  # noqa: D102
        if isinstance(obj, DigitalObject):
            casted = obj.casted_or_self()
            actions = casted.actions(self.request.user.heliportuser, self.project)
            return [
                BoundTableAction(TableAction(action.name, "dummy"), action.link)
                for action in actions
            ]
        return []

    def register_columns(self, table: TableDescription):
        """
        Register the columns on a ``table`` by calling `utils.table_description.TableDescription.add_column`.
        The default implementation uses :attr:`columns` to get information about what columns to add.
        Feel free to implement ``register_columns`` in a subclass.
        """  # noqa: D205, E501
        for display_name, row_name, column_type in self.columns:
            styles = {
                "small": {"width": 10},
                "normal": {"width": 20},
                "large": {"width": 40},
            }
            table.add_column(display_name, row_name, style=styles[column_type.lower()])

    def register_actions(self, table: TableDescription):
        """
        Register the actions on a ``table`` by calling `utils.table_description.TableDescription.add_action`.
        The default implementation uses :attr:`actions` to get information about what actions to add.
        Feel free to implement ``register_actions`` in a subclass.
        """  # noqa: D205, E501
        for display_name, my_property, action_type in self.actions:
            callback = getattr(self, my_property)
            types = {
                "link": {"link_callback": callback},
                "link_secondary": {
                    "link_callback": callback,
                    "style": {"class": "btn-secondary"},
                },
                "button": {"action_callback": callback},
                "danger": {
                    "action_callback": callback,
                    "style": {"class": "btn-danger"},
                },
            }
            table.add_action(display_name, my_property, **types[action_type.lower()])

    def register_form_columns(self, form: FormDescription):
        """
        Register the columns on a ``form`` by calling `utils.form_description.FormDescription.add_column`.
        The default implementation uses :attr:`edit_fields` to get information about what columns to add.
        Feel free to implement ``register_form_columns`` in a subclass.
        """  # noqa: D205, E501
        for display_name, column_name, column_type in self.edit_fields:
            t = column_type.lower()
            if t == "normal":
                form.add_column(display_name, column_name)
            elif t == "large":
                form.add_column(display_name, column_name, attrs={"widget": "textarea"})
            elif t == "color":
                form.add_column(display_name, column_name, attrs={"widget": "color"})
            elif t == "digital_object":
                form.add_column(
                    display_name, column_name, attrs={"widget": "digital_object"}
                )
            else:
                raise ValueError(
                    f"Unsupported type {column_type}! supported types: "
                    "normal, large, color, digital_object"
                )

    def post(self, request, *args, **kwargs):
        """
        Set ``project`` and call the correct one of :meth:`handle_update`, :meth:`handle_create` or :meth:`handle_action`.

        Called by django on http post.
        """  # noqa: E501
        try:
            self.project = self.get_object()

            action_name, target_pk = self.extract_action()
            if action_name is not None and target_pk is not None:
                response = self.handle_action(action_name, target_pk)
            else:
                if "pk" in self.kwargs:
                    response = self.handle_update()
                else:
                    response = self.handle_create()
        except UserMessage as m:
            response = self.handle_error(m.message)

        return response

    def handle_error(self, message):
        """Called by :meth:`post` if a :class:`UserMessage` is raised. It renders the list including the error message."""  # noqa: D401, E501
        context = self.get_context_data()
        context["message"] = message
        return render(self.request, self.template_name, context)

    def handle_create(self):
        """Handles creation of a new :attr:`model` instance. Including setting attributes."""  # noqa: D401, E501
        obj = self.get_create_instance()
        form_fields = self.extract_form_fields()
        self.set_update_attributes(obj, form_fields)
        obj.category_str = self.category
        obj.save()
        self.set_update_attributes(obj, form_fields, obj_saved=True)
        obj.projects.add(self.project)
        return HttpResponseRedirect(self.request.build_absolute_uri())

    def handle_update(self):
        """Handles updating a :attr:`model` instance."""  # noqa: D401
        obj = self.get_update_instance(self.kwargs["pk"])
        form_fields = self.extract_form_fields()
        self.set_update_attributes(obj, form_fields)
        obj.save()
        self.set_update_attributes(obj, form_fields, obj_saved=True)
        if self.list_url is None:
            raise ValueError("specify list_url to allow sending form data")
        return redirect(self.list_url, project=self.project.pk)

    def extract_form_fields(self):
        """
        Helper function to extract form fields out of the posted data.
        Data from autocomplete inputs with data separated into a label input and an hidden id input is supported.
        """  # noqa: D205, D401, E501
        form_fields = {}
        form_data_prefix = "form_data_field_"
        form_object_label_prefix = "form_data_object_label_"
        form_object_id_prefix = "form_data_object_id_"
        for param, value in self.request.POST.items():
            if param.startswith(form_data_prefix):
                form_fields[param[len(form_data_prefix) :]] = value

            if param.startswith(form_object_label_prefix):
                key = param[len(form_object_label_prefix) :]
                object_id = self.request.POST[f"{form_object_id_prefix}{key}"]
                object_label = value
                form_fields[key] = digital_object_form_label_and_pk(
                    object_label, object_id
                )

        return form_fields

    def handle_action(self, action_name: str, target_pk: str):
        """Dispatches an action on a table row."""
        obj = get_object_or_404(self.model, pk=target_pk)
        table = self.get_table()
        response = table.execute(action_name, obj)

        if response is None:
            response = HttpResponseRedirect(self.request.build_absolute_uri())

        return response

    def extract_action(self):
        """Helper function to extract the action to be dispatched from POST data."""  # noqa: D401, E501
        action = None
        pk = None
        prefix = "action_trigger_"
        for param, value in self.request.POST.items():
            if param.startswith(prefix):
                action = param[len(prefix) :]
                pk = value
                break
        return action, pk

    def get_create_instance(self):
        """Create a new instance of :attr:`model`."""
        return self.model()

    def set_update_attributes(self, obj, form_fields, obj_saved=False):
        """
        Is called by :meth:`handle_create` and :meth:`handle_update` after the ``obj`` is saved.
        The default implementation sets all attributes in :attr:`after_save_attributes`.
        """  # noqa: D205, E501
        for key, value in form_fields.items():
            if (key in self.after_save_attributes) == obj_saved:
                setattr(obj, key, value)

    def action_edit(self, obj):
        """
        Implements the edit action so that the string "action_edit" can be used in :attr:`actions`.
        The edit action is a link action that links to :attr:`update_url`, which points normally to this view itself.
        """  # noqa: D205, D401, E501
        if self.list_url is None:
            raise ValueError(
                "the default editing function requires the list_url to be specified"
            )
        return reverse(
            self.update_url, kwargs={"project": self.project.pk, "pk": obj.pk}
        )

    def action_delete(self, obj: DigitalObject):
        """
        Implements the delete action so that the string "action_delete" can be used in :attr:`actions`.
        The default implementation sets ``deleted`` to the current time and saves the object.
        """  # noqa: D205, D401, E501
        obj.mark_deleted(self.request.user.heliportuser)
