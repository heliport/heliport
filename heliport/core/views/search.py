# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later


from django.db.models import Q
from django.views.generic import TemplateView

from heliport.core.app_interaction import get_search_urls
from heliport.core.mixins import HeliportLoginRequiredMixin
from heliport.core.models import HeliportGroup, HeliportUser, LoginInfo, Project, Tag


class SearchBaseView(HeliportLoginRequiredMixin, TemplateView):
    """
    Base view for the HELIPORT search feature.
    This view uses javascript to get the actual search results from each apps search view.
    """  # noqa: D205, E501

    #: HTML template rendered by this View
    template_name = "core/search/search_base.html"

    def get_context_data(self, **kwargs):
        """Called by django to get context when rendering template. Includes urls of all search views."""  # noqa: D401, E501
        context = super().get_context_data(**kwargs)
        context["search_urls"] = list(get_search_urls())
        return context


class SearchView(HeliportLoginRequiredMixin, TemplateView):
    """Handle searching objects in the HELIPORT core app."""

    #: HTML template rendered by this View
    template_name = "core/search/search.html"

    def get_context_data(self, **kwargs):
        """Query all objects matching "q" parameter."""
        context = super().get_context_data(**kwargs)
        user = self.request.user.heliportuser
        query = self.request.GET.get("q", "")

        project_results = set()
        group_results = set()
        user_results = set()
        login_info_results = set()
        tag_results = set()

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            project_results.update(
                Project.objects.filter(
                    (Q(owner=user) | Q(co_owners=user))
                    & Q(deleted__isnull=True)
                    & (
                        Q(project_id=num)
                        | Q(label__icontains=word)
                        | Q(owner__display_name__icontains=word)
                        | Q(co_owners__display_name__icontains=word)
                        | Q(description__icontains=word)
                        | Q(persistent_id__icontains=word)
                        | Q(group__display_name=word)
                        | Q(identifiers__identifier__contains=word)
                    )
                )
            )

            group_results.update(
                HeliportGroup.objects.filter(display_name__icontains=word)
            )

            user_results.update(
                HeliportUser.objects.filter(
                    Q(display_name__icontains=word) | Q(user_id=num)
                )
            )

            login_info_results.update(
                LoginInfo.objects.filter(
                    Q(user=user)
                    & (
                        Q(username__icontains=word)
                        | Q(machine__icontains=word)
                        | Q(name__icontains=word)
                        | Q(type__icontains=word)
                    )
                )
            )

            tag_results.update(
                Tag.objects.filter(
                    (Q(projects__owner=user) | Q(projects__co_owners=user))
                    & Q(deleted__isnull=True)
                    & (
                        Q(label__icontains=word)
                        | Q(description__icontains=word)
                        | Q(requires_type__label__icontains=word)
                        | Q(attribute__label__icontains=word)
                        | Q(value__label__icontains=word)
                    )
                )
            )

        context["project_results"] = project_results
        context["group_results"] = group_results
        context["user_results"] = user_results
        context["login_info_results"] = login_info_results
        context["tag_results"] = tag_results
        return context
