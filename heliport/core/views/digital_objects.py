# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf import settings
from django.db.models import Q
from django.http import JsonResponse
from django.views.generic import DetailView, View
from rest_framework import status

from heliport.core.digital_object_resolution import resolve
from heliport.core.mixins import HeliportLoginRequiredMixin, HeliportProjectMixin
from heliport.core.models import Project
from heliport.core.utils.context import Context
from heliport.core.utils.exceptions import UserMessage
from heliport.core.utils.queries import ListOfQuerySets, digital_objects_by_type


class AsDigitalObjectView(HeliportProjectMixin, DetailView):
    """
    Creates a :class:`heliport.core.models.DigitalObject` instance given a
    :class:`heliport.core.digital_object_resolution.Resolvable` provided as GET or POST
    parameters and returns it as JSON using ``"pk"``, ``"digital_object_id"``, ``"url"``
    and ``"label"``. If an error occurs the JSON contains an ``"error"`` attribute.

    This uses
    :meth:`heliport.core.digital_object_interface.GeneralDigitalObject.as_digital_object``
    internally.

    If you set POST parameter `is_helper` to ``True`` the resulting digital object is
    marked as "helper". This can be used when querying digital objects to not show
    automatically generated helpers to the user.
    """  # noqa: D205

    model = Project
    pk_url_kwarg = "project"

    def post(self, request, *args, **kwargs):
        """Handle post."""
        project = self.get_object()
        user = None
        if hasattr(request.user, "heliportuser"):
            user = request.user.heliportuser
        with Context(user, project) as context:
            try:
                params = request.GET.copy()
                params.update(request.POST)
                obj = resolve(params, context)
                if obj is None:
                    raise UserMessage("Object not found")
                maybe_digital_object = obj.as_digital_object_if_exists(context)
                digital_object = maybe_digital_object.as_digital_object(context)
                if str(request.POST.get("is_helper")).lower() == "true":
                    digital_object.is_helper = True
                    digital_object.save()
                digital_object.projects.add(project)
                return JsonResponse(
                    {
                        "pk": digital_object.pk,
                        "digital_object_id": digital_object.digital_object_id,
                        "url": digital_object.local_url(request),
                        "label": digital_object.label,
                    }
                )
            except UserMessage as e:
                return JsonResponse(
                    {"error": e.message}, status=status.HTTP_400_BAD_REQUEST
                )


class DigitalObjectAutocompleteView(HeliportLoginRequiredMixin, View):
    """Handles getting information for autocomplete inputs for :class:`heliport.core.models.DigitalObject`."""  # noqa: E501

    def get(self, request, *args, **kwargs):
        """Responde to GET request.

        Returns json list like::

            [
                {"label": "aa", "pk": 1},
                {"label": "xa", "pk": 55}
            ]

        Excludes deleted objects and respects user rights.
        Maximum dumber of results is configurable in settings.py ``HELIPORT_CORE_AUTOCOMPLETE_COUNT``

        Supports the following get parameters:

            - ``type`` - Restrict autocomplete to a type. See :func:`heliport.core.utils.queries.digital_objects_by_type` for allowed values. ``...?type=1&type=2`` results in union of results to ``...?type=1`` and ``...?type=2``.
            - ``q`` or ``term`` - search term for label and persistent_id
            - ``exact`` - false (default): case-insensitive and allow part match; case_insensitive: case-insensitive full match; true: case-sensitive full match. psersistent_id is always matched exact
        """  # noqa: D401, E501
        query = ListOfQuerySets()

        obj_types = request.GET.getlist("type")
        if obj_types:
            for obj_type in obj_types:
                query.extend(digital_objects_by_type(obj_type))
        else:
            query.all_digital_objects()

        auth_user = request.user
        user = None
        if hasattr(auth_user, "heliportuser"):
            user = auth_user.heliportuser
        if user is None:
            query.filter(owner__isnull=True)
        else:
            query.filter(Q(owner__isnull=True) | Q(owner=user) | Q(co_owners=user))

        query.filter(deleted__isnull=True)

        pattern = request.GET.get("q") or request.GET.get("term", "")
        if pattern:
            exact = str(request.GET.get("exact", "false")).lower()
            if exact == "false":
                q = Q(label__icontains=pattern)
            elif exact == "case_insensitive":
                q = Q(label__iexact=pattern)
            else:
                q = Q(label__exact=pattern)

            query.filter(q | Q(persistent_id=pattern))

        return JsonResponse(
            [
                {"label": obj.label, "pk": obj.digital_object_id}
                for obj in query.as_list(settings.HELIPORT_CORE_AUTOCOMPLETE_COUNT)
            ],
            safe=False,
        )
