# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
This module contains :doc:`Django class based views <django:topics/class-based-views/index>`
to handle incoming http requests. Each ``View`` is mapped to a URL in :mod:`heliport.core.urls`.

Each django app can have its own views. This module contains views for all main heliport functionality.
"""  # noqa: D205, E501

from .api import HeliportModelViewSet
from .generic import HeliportObjectListView
