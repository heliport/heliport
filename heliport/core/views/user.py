# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
from datetime import timedelta
from typing import Any

from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.forms import ValidationError
from django.http import HttpRequest, HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils import timezone
from django.views.generic import (
    CreateView,
    DetailView,
    TemplateView,
    UpdateView,
    View,
)
from knox import models as knox_models
from knox.settings import knox_settings

from heliport.core.app_interaction import (
    generate_project_dropdown,
    generate_project_navbar,
)
from heliport.core.mixins import HeliportLoginRequiredMixin
from heliport.core.models import LoginInfo, Project
from heliport.core.user_logic.remote_login import delete_ssh_key, deploy_ssh_key
from heliport.core.utils.context import project_header_breadcrumbs

logger = logging.getLogger(__name__)


class UserProfileView(HeliportLoginRequiredMixin, TemplateView):
    """Handle showing and editing settings of a HELIPORT user. (most information is read only)."""  # noqa: E501

    #: HTML template rendered by this View
    template_name = "core/user/profile.html"

    def get_context_data(self, **kwargs):
        """Called by django to get context when rendering template.
        Context includes the user and information for HELIPORT API token creation form (like the current time).
        """  # noqa: D205, D401, E501
        context = super().get_context_data(**kwargs)
        user = self.request.user

        # This is a proof of concept for showing a project-related navbar in a
        # non-project page. For now it is here so that we can have some navigation to
        # get from the user settings page back to the project. In the future, this needs
        # to be refactored.
        project_pk = self.request.session.get("activeHeliportProject")
        if project_pk is not None:
            project = get_object_or_404(Project, pk=project_pk)
            context["project"] = project
            context["project_dropdown_modules"] = generate_project_dropdown(project)
            context["project_navbar_modules"] = generate_project_navbar(project)
            context["project_header_breadcrumbs"] = project_header_breadcrumbs(
                self.request, "Settings", self.request.path, reset=True
            )

        context["heliport_user"] = user.heliportuser

        return context

    def post(self, request, *args, **kwargs):
        """Set orcid for user. Called by django on http post."""
        user = request.user.heliportuser
        orcid = request.POST.get("orcid")

        try:
            user.set_orcid(orcid)
            user.save()
        except ValidationError as e:
            messages.add_message(request, messages.ERROR, e.messages[0])

        return redirect("core:user_profile")


class TokenListView(HeliportLoginRequiredMixin, TemplateView):
    """Display the user's API tokens."""

    #: HTML template rendered by this View
    template_name = "core/user/tokens.html"

    def get_context_data(self, **kwargs):
        """Update the rendering context with token data."""
        context = super().get_context_data(**kwargs)
        user = self.request.user

        # TODO: This is copy-pasted from UserProfileView. Build abstraction!
        project_pk = self.request.session.get("activeHeliportProject")
        if project_pk is not None:
            project = get_object_or_404(Project, pk=project_pk)
            context["project"] = project
            context["project_dropdown_modules"] = generate_project_dropdown(project)
            context["project_navbar_modules"] = generate_project_navbar(project)
            context["project_header_breadcrumbs"] = project_header_breadcrumbs(
                self.request, "Settings", self.request.path, reset=True
            )

        context["now"] = timezone.now()
        context["tomorrow"] = (timezone.now() + timedelta(days=1)).strftime("%Y-%m-%d")
        context["tokens"] = knox_models.AuthToken.objects.filter(user=user)
        context["api_url"] = self.request.build_absolute_uri(reverse("api-root"))
        context["auth_header_prefix"] = knox_settings.AUTH_HEADER_PREFIX
        context["token_prefix"] = knox_settings.TOKEN_PREFIX

        return context


class RemoteServerLoginView(HeliportLoginRequiredMixin, DetailView):
    """Show username and password prompt for an SSH login and put public SSH key in allowed hosts on the server
    specified in the SSH login (:class:`heliport.core.models.LoginInfo`) while private key is stored encrypted in the
    HELIPORT database. When the key of an SSH LoginInfo is on the remote server we say it is "connected".

    The corresponding view for logout is the :class:`RemoteServerLogoutView`.
    """  # noqa: D205, E501

    #: Django model handled in this view - used by django ``DetailView``
    model = LoginInfo
    #: HTML template rendered by this View
    template_name = "core/user/remote_server_login.html"

    @staticmethod
    def validate_login(login, request):
        """Check if SSH :class:`heliport.core.models.LoginInfo` can be connected.
        This is the case if the login info belongs to the user and all necessary SSH indirections are connected.
        """  # noqa: D205, E501
        if request.user.heliportuser != login.user:
            return HttpResponseForbidden()

        logger.debug("check if all parents are connected")
        try:
            connect_first = login.get_unconnected_parents()
        except ValueError:
            return HttpResponse("<b> ERROR: </b> infinite chain of ssh connections")
        if connect_first:
            request.session["message"] = (
                f"{login.name} can not be connected. Please connect {connect_first[0]} first!"  # noqa: E501
            )
            return redirect("core:remote_server_login", pk=connect_first[0].pk)

        return None

    def get_context_data(self, **kwargs):
        """Called by django to get context when rendering template. In addition to the LoginInfo
        (automatically included by ``DetailView``) include error message in context.
        """  # noqa: D205, D401, E501
        context = super().get_context_data(**kwargs)
        context["message"] = self.request.session.get("message", "")
        self.request.session["message"] = ""
        return context

    def post(self, request, *args, **kwargs):
        """Connect LoginInfo. Called by django on http post.

        As a side effect the username is stored in the ``LoginInfo``. This removes the need to enter the username
        twice (when creating the login info and when connecting the login info). The username is used e.g. for searching
        login infos.
        """  # noqa: E501
        logger.debug("user requested ssh key connection")
        login_info = self.get_object()
        error = self.validate_login(login_info, request)
        if error is not None:
            return error

        username = request.POST.get("username")
        password = request.POST.get("password")
        login_info.username = username
        login_info.save()

        error = deploy_ssh_key(login_info, username, password)
        if error is not None:
            messages.error(request, error)
        return redirect("core:user_login_list")


class RemoteServerLogoutView(HeliportLoginRequiredMixin, View):
    """
    Handle removing SSH key from external server. For more details see :class:`RemoteServerLoginView`.
    This view does not render a template. The key is deleted without any questions asked and the user is redirected to
    :class:`LoginInfoView`.
    """  # noqa: D205, E501

    # TODO: This should only be done on POST
    def get(self, request, *args, **kwargs):
        """Disconnect SSH :class:`heliport.core.models.LoginInfo`. Called by django on http get."""  # noqa: E501
        logger.debug("user requested ssh disconnect")
        login_info = get_object_or_404(LoginInfo, pk=kwargs.get("pk"))
        # Hacky...
        error = RemoteServerLoginView.validate_login(login_info, request)
        if error is not None:
            return error

        delete_ssh_key(login_info)
        return redirect("core:user_login_list")


class LoginInfoView(HeliportLoginRequiredMixin, CreateView):
    """
    Handle creating/showing login details of other web services in HELIPORT so that HELIPORT can make requests on
    behalf of the user. There exists a very similar view for editing/showing such login details:
    :class:`LoginInfoUpdateView`.
    """  # noqa: D205, E501

    #: Django model handled in this view - used by django ``CreateView``
    model = LoginInfo
    #: HTML template rendered by this View
    template_name = "core/user/login_infos.html"
    #: Used by django ``CreateView`` to generate input form for creating new login infos
    fields = ["name", "type", "username", "key", "machine", "via", "slurm"]

    def get_success_url(self):
        """Called by django to get url to redirect to after successful :class:`heliport.core.models.LoginInfo` creation.

        :return: URL of this view itself.
        """  # noqa: D401, E501
        return reverse("core:user_login_list")

    def get_context_data(self, **kwargs):
        """Called by django to get context when rendering template."""  # noqa: D401
        user = self.request.user.heliportuser
        context = super().get_context_data(**kwargs)
        context["logins"] = user.logininfo_set.all()
        context["login_type"] = self.request.GET.get("login_type")
        context["login_types"] = LoginInfo.LoginTypes
        context["ssh_logins"] = user.logininfo_set.filter(type=LoginInfo.LoginTypes.SSH)
        context["update"] = False
        return context

    def post(self, request, *args, **kwargs):
        """Handle deletion of login infos here and creation in superclass ``CreateView``.
        Called by django on http post.
        """  # noqa: D205, E501
        login_to_remove = request.POST.get("remove")
        if login_to_remove:
            login_info = get_object_or_404(LoginInfo, pk=login_to_remove)
            if login_info.user != self.request.user.heliportuser:
                return HttpResponseForbidden()
            login_info.delete()
            return redirect(self.get_success_url())
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        """Validate form.

        Called by django ``CreateView`` after form has been deemed valid. Set additional
        attributes before saving LoginInfo:

        - **user** - current heliport user added automatically (not from input form)
        - **password** - password field can not be generated automatically in form
          because `password` is a python property and not a django model field
        """
        form.instance.user = self.request.user.heliportuser
        form.instance.password = self.request.POST.get("password", "")
        return super().form_valid(form)


class LoginInfoUpdateView(HeliportLoginRequiredMixin, UpdateView):
    """
    Handle editing/showing login details, of other web services, in HELIPORT so that HELIPORT can make requests on
    behalf of the user. There exists a very similar view for creation/showing such login details:
    :class:`LoginInfoView`.
    """  # noqa: D205, E501

    #: Django model handled in this view - used by django ``UpdateView``
    model = LoginInfo
    #: HTML template rendered by this View
    template_name = "core/user/login_infos.html"
    #: Used by django ``UpdateView`` to generate input form for editing login infos
    fields = ["name", "type", "username", "key", "machine", "via", "slurm"]

    def dispatch(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        """Don't allow users to edit a connected or someone else's LoginInfo."""
        if (login := self.get_object()) is not None and (
            login.connected or login.user != request.user.heliportuser
        ):
            return HttpResponseForbidden()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        """Redirect to :class:`LoginInfoView` after updating login info."""
        return reverse("core:user_login_list")

    def get_context_data(self, **kwargs):
        """Called by django to get context when rendering template."""  # noqa: D401
        user = self.request.user.heliportuser
        context = super().get_context_data(**kwargs)
        context["logins"] = user.logininfo_set.all()
        context["login_type"] = self.request.GET.get("login_type", self.object.type)
        context["login_types"] = LoginInfo.LoginTypes
        context["ssh_logins"] = user.logininfo_set.filter(type=LoginInfo.LoginTypes.SSH)
        context["update"] = True
        return context

    def form_valid(self, form):
        """Assert that :class:`heliport.core.models.LoginInfo` belongs to current user and set password.
        See :meth:`LoginInfoView.form_valid` for more details.
        """  # noqa: D205, E501
        if form.instance.user != self.request.user.heliportuser:
            raise PermissionDenied()
        form.instance.password = self.request.POST.get("password", "")
        return super().form_valid(form)
