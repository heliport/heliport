# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from collections import OrderedDict
from datetime import datetime

import django_filters.rest_framework
import knox.views
from django.conf import settings
from django.contrib.auth import user_logged_out
from django.shortcuts import get_object_or_404
from django.utils import timezone
from knox import models as knox_models
from rest_framework import filters as rest_filters
from rest_framework import permissions as rest_permissions
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.renderers import BrowsableAPIRenderer, JSONRenderer
from rest_framework.views import APIView, Response

from heliport.core.digital_object_aspects import DirectoryObj, FileObj
from heliport.core.models import (
    Contribution,
    DigitalObject,
    HeliportGroup,
    HeliportUser,
    LoginInfo,
    NamedToken,
)
from heliport.core.permissions import (
    HeliportObjectPermission,
    HeliportProjectPermission,
    StaffOrReadOnly,
    projects_of,
)
from heliport.core.renderers import HeliportPartialRenderer
from heliport.core.serializers import (
    ContributionSerializer,
    DigitalObjectAPISerializer,
    GroupSerializer,
    ProjectSerializer,
    TokenSerializer,
    UserSerializer,
)
from heliport.core.utils.http import (
    directory_api_response,
    directory_from_description_api_response,
    file_api_response,
    file_from_description_api_response,
)
from heliport.core.utils.json_ld import LdContext


class MaintenanceMessageView(APIView):
    """Returns the maintenance message which can be specified in the settings."""

    # These should be moved in a common base class in the future so that HELIPORT API
    # views that support partials can inherit from it.
    renderer_classes = [JSONRenderer, BrowsableAPIRenderer, HeliportPartialRenderer]
    template_name = "core/partials/maintenance_message.html"

    def get(self, request, *args, **kwargs):
        """Handle GET request."""
        level = settings.HELIPORT_CORE_MAINTENANCE_MESSAGE_LEVEL
        message = settings.HELIPORT_CORE_MAINTENANCE_MESSAGE
        return Response({"level": level, "message": message if level else None})


class LoginNamedTokenView(knox.views.LoginView):
    """Customized |LoginView|_ for named HELIPORT API tokens.

    .. _LoginView: https://github.com/James1345/django-rest-knox/blob/3a1bc584f9691f4bc19d8a04a98c68c293be9ca6/knox/views.py#L15
    .. |LoginView| replace:: ``LoginView``
    """

    def get_token_ttl(self):
        """Get the token's TTL.

        The TTL is calculated from the "expiry" request parameter which must use the
        format "YYYY-MM-DD". An empty string means the token does not expire.
        """
        expiry = self.request.POST.get("expiry")
        if expiry is None:
            return super().get_token_ttl()
        if expiry == "":
            return None

        try:
            expire_time = datetime.strptime(expiry, "%Y-%m-%d")
        except ValueError:
            return Response(
                {
                    "error": (
                        '"expiry" has invalid format; supported format is YYYY-MM-DD'
                    )
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        return timezone.make_aware(expire_time) - timezone.now()

    def get_post_response_data(self, request, token, instance):
        """Put "hash" into response for displaying token."""
        result = super().get_post_response_data(request, token, instance)
        result["hash"] = instance.pk

        return result

    def create_token(self):  # noqa: D102
        instance, token = super().create_token()
        name = self.request.POST.get("name")
        if name:
            NamedToken.objects.create(auth_token=instance, name=name)
        return instance, token


class LogoutTokenView(APIView):
    """Custom |LogoutView|_ that can delete tokens other than the currently used one.

    The |LogoutView|_ provided by ``django-rest-knox`` can only delete the token that is
    authenticating the current request, or all tokens of a user. This view however
    allows deletion of any single one of the user's tokens.

    .. _LogoutView: https://github.com/James1345/django-rest-knox/blob/3a1bc584f9691f4bc19d8a04a98c68c293be9ca6/knox/views.py#L78
    .. |LogoutView| replace:: ``LogoutView``
    """

    permission_classes = [rest_permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        """Delete token specified by "pk" parameter."""
        token = get_object_or_404(
            knox_models.AuthToken, pk=self.request.POST.get("pk"), user=request.user
        )
        token.delete()
        user_logged_out.send(
            sender=request.user.__class__, request=request, user=request.user
        )
        return Response(None, status=status.HTTP_204_NO_CONTENT)


class DeleteAllTokensView(APIView):
    """Custom |LogoutAllView|_ using the default authentication classes.

    .. _LogoutAllView: https://github.com/James1345/django-rest-knox/blob/3a1bc584f9691f4bc19d8a04a98c68c293be9ca6/knox/views.py#L89
    .. |LogoutAllView| replace:: ``LogoutAllView``
    """

    permission_classes = [rest_permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):  # noqa: D102
        request.user.auth_token_set.all().delete()
        user_logged_out.send(
            sender=request.user.__class__, request=request, user=request.user
        )
        return Response(None, status=status.HTTP_204_NO_CONTENT)


class HeliportModelViewSet(viewsets.ModelViewSet):
    """Base class for API ViewSets in HELIPORT for DigitalObjects."""

    #: Make sure user is authenticated and is allowed to access the DigitalObject
    permission_classes = [rest_permissions.IsAuthenticated, HeliportObjectPermission]

    def perform_destroy(self, instance):
        """Deleting objects by setting `deleted` to the current time."""  # noqa: D401
        instance.deleted = timezone.now()
        instance.save()


class ProjectViewSet(HeliportModelViewSet):
    """
    Showing the most general HELIPORT project properties.
    Request more detailed information by appending urls/ to the url
    Go to a specific project by appending its id/ to the url
    For more information on how to authenticate look in HELIPORT (user > settings).
    """  # noqa: D205

    serializer_class = ProjectSerializer
    permission_classes = [rest_permissions.IsAuthenticated, HeliportProjectPermission]
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    search_fields = ["label", "description", "owner__display_name"]
    filterset_fields = ["group", "owner", "persistent_id"]

    def get_queryset(self):
        """Get projects of current user that are not deleted."""
        user = self.request.user.heliportuser
        return projects_of(user).filter(deleted__isnull=True).distinct()


class TokenViewSet(viewsets.ModelViewSet):
    """Showing HELIPORT user tokens."""

    permission_classes = [rest_permissions.IsAuthenticated]
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    filterset_fields = ["login_info_id"]
    search_fields = ["name"]
    serializer_class = TokenSerializer

    def get_queryset(self):
        """Get tokens (stored in heliport as LoginInfos, not HELIPORT API tokens) of current user."""  # noqa: E501
        user = self.request.user.heliportuser
        return LoginInfo.objects.filter(user=user, type=LoginInfo.LoginTypes.TOKEN)


class UserViewSet(viewsets.ModelViewSet):
    """Showing HELIPORT user model."""

    permission_classes = [rest_permissions.IsAuthenticated, StaffOrReadOnly]
    filterset_fields = ["user_id", "authentication_backend_id", "display_name"]

    queryset = HeliportUser.objects.all()
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """Showing user groups."""

    permission_classes = [rest_permissions.IsAuthenticated, StaffOrReadOnly]

    queryset = HeliportGroup.objects.all()
    serializer_class = GroupSerializer


class ContributionViewSet(viewsets.ModelViewSet):
    """Showing contributions to projects of the current user."""

    permission_class = [rest_permissions.IsAuthenticated, StaffOrReadOnly]
    serializer_class = ContributionSerializer

    def get_queryset(self):
        """Return contributions to projects of the user that are not deleted."""
        user = self.request.user.heliportuser
        projects = projects_of(user).filter(deleted__isnull=True)
        # note __in=projects computes no intermediate result in python
        # but __in=list(project) respects the fact that Project is a subclass of DigitalObject  # noqa: E501
        return Contribution.objects.filter(
            contribution_to__in=list(projects)
        ).distinct()


class DigitalObjectAPI(HeliportModelViewSet):
    """API for all digital objects in HELIPORT.

    For digital objects that are files it is also possible to download them at
    /api/digital-objects/complete/<digital_object_id>/download/

    For digital objects that are directories it is also possible to get contents at
    /api/digital-objects/complete/<digital_object_id>/contents/
    """

    #: :class:`DigitalObjectAPISerializer` contains the code that makes this general api possible.  # noqa: E501
    serializer_class = DigitalObjectAPISerializer
    #: This api allows access to all DigitalObjects. Permissions (read/write) are respected per attribute.  # noqa: E501
    queryset = DigitalObject.objects.all()
    #: Even without login public attributes are accessible.
    permission_classes = [rest_permissions.IsAuthenticatedOrReadOnly]

    def __init__(self, *args, **kwargs):
        """Initialize json-ld context to collect property namespaces."""
        super().__init__(*args, **kwargs)
        self.ld_context = LdContext()

    def finalize_response(self, request, response, *args, **kwargs):
        """Add json-ld context to response to make this api more interoperable."""
        response = super().finalize_response(request, response, *args, **kwargs)
        if not hasattr(response, "data") or not self.ld_context.get_context_dict():
            return response
        if isinstance(response.data, OrderedDict):
            response.data["@context"] = self.ld_context.get_context_dict()
            response.data.move_to_end("@context", last=False)
        elif isinstance(response.data, dict):
            response.data = dict(
                **{"@context": self.ld_context.get_context_dict()}, **response.data
            )
        return response

    @action(detail=True, methods=["get"])
    def download(self, request, pk=None):
        """Get streaming file response to download file contents."""
        digital_object: DigitalObject = self.get_object()
        file_name = digital_object.label
        if not file_name:
            file_name = f"object_{digital_object.digital_object_id}"

        digital_object = digital_object.casted_or_self()
        if not isinstance(digital_object, FileObj):
            class_name = type(digital_object).__name__
            return Response(
                {"error": f"File download is not implemented for {class_name}"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        return file_api_response(request, digital_object, file_name, add_extension=True)

    @action(detail=False, methods=["get"])
    def download_from_description(self, request):
        """Get streaming file response to download file contents.

        The file to download is specified by request parameters.
        This enables downloading files not directly stored in heliport e.g. inside
        a directory.
        """

        def get_file_name(file_obj: FileObj) -> str:
            return str(file_obj.as_text())

        return file_from_description_api_response(
            request, get_file_name, add_extension=True
        )

    @action(detail=True, methods=["get"])
    def contents(self, request, pk=None):
        """Get files and directories in directory."""
        digital_object = self.get_object()

        digital_object = digital_object.casted_or_self()
        if not isinstance(digital_object, DirectoryObj):
            class_name = type(digital_object).__name__
            return Response(
                {"error": f"Directory interface is not implemented for {class_name}"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        return directory_api_response(request, digital_object)

    @action(detail=False, methods=["get"])
    def contents_from_description(self, request):
        """Get files and directories in directory.

        directory is specified via describing parameters to allow download of
        subdirectories
        """
        return directory_from_description_api_response(request)
