# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf import settings
from django.http import HttpResponseForbidden
from django.shortcuts import HttpResponse, get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import DetailView, TemplateView

from heliport.core.mixins import (
    HeliportLoginRequiredMixin,
    HeliportObjectMixin,
    HeliportProjectMixin,
)
from heliport.core.models import HeliportUser, Image, LoginInfo, Project
from heliport.core.permissions import is_object_member


class ImageView(HeliportObjectMixin, DetailView):
    """View for showing :class:`heliport.core.models.Image` s to use them in html."""

    model = Image

    def get(self, request, *args, **kwargs):
        """
        Image pk is taken from url. This view may be cached (configured in :mod:`heliport.core.urls`).
        Called by django on http get.
        """  # noqa: D205, E501
        image = self.get_object()
        byte_data = image.download_data()

        if image.status == image.ImageStates.UPLOADING:
            # AssertionError here causes data not to be cached
            assert byte_data != b"", "image not uploaded"
        return HttpResponse(byte_data, content_type=image.mimetype)


class ImagesView(HeliportProjectMixin, TemplateView):
    """List all :class:`heliport.core.models.Image` s in a project."""

    #: HTML template rendered by this View
    template_name = "core/digital_object/picture_list.html"

    def get_context_data(self, **kwargs):
        """
        Called by django to get context when rendering template.
        Includes "images", the list of relevant images, in context.
        """  # noqa: D205, D401
        user = get_object_or_404(HeliportUser, auth_user=self.request.user)
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        context = super().get_context_data(**kwargs)
        context["images"] = Image.objects.filter(
            is_object_member(user), deleted__isnull=True, projects=project
        ).distinct()
        return context

    def post(self, request, *args, **kwargs):
        """Handles image deletion. Called by django on http post."""  # noqa: D401
        image_to_delete = get_object_or_404(Image, pk=request.POST.get("delete"))
        image_to_delete.mark_deleted(self.request.user.heliportuser)
        return redirect("core:image_list", project=self.kwargs["project"])


class TakePictureView(HeliportLoginRequiredMixin, TemplateView):
    """Handles taking pictures in HELIPORT these pictures are stored as :class:`heliport.core.models.Image`."""  # noqa: E501

    #: HTML template rendered by this View
    template_name = "core/digital_object/take_picture.html"

    def get_context_data(self, **kwargs):
        """
        Generate default values for storing image and include them in context.
        Support ``next`` get parameter to redirect to arbitrary page after image has been taken.
        """  # noqa: D205, E501
        context = super().get_context_data(**kwargs)
        context["project"] = get_object_or_404(Project, pk=self.kwargs.get("project"))
        last_image = (
            Image.objects.filter(login__user__auth_user=self.request.user)
            .order_by("-created")
            .first()
        )
        context["default_login"] = None if last_image is None else last_image.login
        context["default_directory"] = (
            "" if last_image is None else last_image.directory
        )
        context["logins"] = LoginInfo.objects.filter(
            user__auth_user=self.request.user, type=LoginInfo.LoginTypes.SSH
        )
        context["next"] = self.request.GET.get("next")
        return context

    def post(self, request, *args, **kwargs):
        """
        Create new :class:`heliport.core.models.Image` with user supplied metadata
        and upload image itself to file system specified by login info (selected by user).

        Return redirect to image list or view specified by "next" parameter.
        If "next" parameter ends with "=" append pk of newly taken image.
        For example ``https://some/url?image=`` becomes ``https://some/url?imgae=13`` if ``13`` is the newly taken image.

        Called by django on http post.
        """  # noqa: D205, E501
        project = get_object_or_404(Project, pk=self.kwargs.get("project"))
        login = get_object_or_404(LoginInfo, pk=request.POST.get("login"))

        # Don't allow usage of arbitrary LoginInfo
        if login.user.auth_user != request.user:
            return HttpResponseForbidden()

        image_data = request.POST["data_url"]
        assert image_data.startswith("data:image/"), "not an image"

        new_image = Image()
        new_image.filename = request.POST["filename"]
        new_image.directory = request.POST["directory"]
        new_image.login = login
        new_image.label = request.POST.get("label", "")
        new_image.description = request.POST.get("description", "")
        new_image.category_str = settings.IMAGE_NAMESPACE
        new_image.save()
        new_image.projects.add(project)
        new_image.upload_image(image_data)

        redirect_url = request.GET.get("next", "")
        if redirect_url.endswith("="):
            redirect_url = f"{redirect_url}{new_image.pk}"
        if not redirect_url:
            redirect_url = reverse("core:image_list", kwargs={"project": project.pk})

        return redirect(redirect_url)
