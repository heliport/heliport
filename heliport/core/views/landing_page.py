# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import base64
from datetime import datetime, timedelta, timezone
from io import BytesIO
from typing import Any, Iterable

import matplotlib
import pandas
from django.db.models import Q
from django.http import Http404, HttpResponse, HttpResponseForbidden
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie
from django.views.generic import DetailView, RedirectView, TemplateView
from matplotlib import pyplot

from heliport.core.models import DigitalObject, HeliportUser, Project
from heliport.core.permissions import has_write_permission
from heliport.core.serializers import (
    HtmlRenderer,
    LandingPageRenderer,
    get_all_attributes,
    get_attributes_by_category,
    get_unique_renderers,
    select_renderer,
)
from heliport.core.utils.string_tools import remove_prefix


class LandingPageView(DetailView):
    """Landing page for Digital Objects.

    Landing pages are rendered in different formats using custom content negotiation.
    """

    #: HTML template rendered by this View
    template_name = "core/digital_object/landingpage.html"
    #: Django model handled in this view - used by django ``DetailView``
    model = DigitalObject

    def get_context_data(self, **kwargs):
        """Get relevant infos in addition to the Digital Object itself.
        Called by django to get context when rendering html template.
        """  # noqa: D205
        context = super().get_context_data(**kwargs)
        context["renderers"] = get_unique_renderers()
        obj = self.object.casted_or_self()
        user = None
        if hasattr(self.request.user, "heliportuser"):
            user = self.request.user.heliportuser
        context["attributes_by_category"] = get_attributes_by_category(
            obj, user
        ).items()
        context["has_write_permission"] = has_write_permission(user, obj)
        context["host"] = self.request.get_host()
        context["protocol"] = self.request.scheme
        return context

    @staticmethod
    def get_custom_url(obj, request):
        """Get the url of a special page in heliport for showing ``obj``. This information is defined on ``obj``
        via a ``get_custom_url()`` function. For example a project is shown as the heliport project graph. (see
        :meth:`heliport.core.models.Project.get_custom_view_url`).
        """  # noqa: D205, E501
        user = None
        if hasattr(request.user, "heliportuser"):
            user = request.user.heliportuser

        if hasattr(obj, "get_custom_view_url"):
            return obj.get_custom_view_url(user)
        return None

    def get(self, request, *args, **kwargs):
        """
        Renderer selection based on `Accept header <https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept>`_
        and handling of special renderers like showing individual page with :meth:`get_custom_url`.

        Called by django on http get.
        """  # noqa: D205, D401, E501
        digital_object = self.get_object().casted_or_self()
        user = None
        if hasattr(request.user, "heliportuser"):
            user = request.user.heliportuser

        renderer, error = select_renderer(
            request.GET.get("format"), request.META.get("HTTP_ACCEPT")
        )
        if error is not None:
            return HttpResponse(error, status=406, content_type="text/plain")

        if renderer == LandingPageRenderer:
            return super().get(request, *args, **kwargs)
        if renderer == HtmlRenderer:
            custom_url = self.get_custom_url(digital_object, request)
            if custom_url is None:
                return super().get(request, *args, **kwargs)
            return redirect(custom_url)

        return renderer.render(digital_object, user)

    def post(self, request, *args, **kwargs):
        """Handle editing what attributes are publicly shown. Called by django on http post."""  # noqa: E501
        public_selection = request.POST.getlist("public_attributes")
        obj = self.get_object().casted_or_self()
        user = None
        if hasattr(request.user, "heliportuser"):
            user = request.user.heliportuser
        if not has_write_permission(user, obj):
            return HttpResponseForbidden()
        attributes = {
            attr.get_id(obj): attr.bind_to(obj) for attr in get_all_attributes(obj)
        }

        todo = []
        for attr_is_public_select in public_selection:
            is_public = request.POST.get(attr_is_public_select) == "public"
            attr_id = remove_prefix(attr_is_public_select, "public_attributes_")
            attr = attributes.get(attr_id)
            if attr is not None and attr.is_public != is_public:
                # Not setting is_public directly and checking if something changed
                # helps when two attributes share an is_public boolean
                todo.append((attr, is_public))

        for attr, is_public in todo:
            attr.is_public = is_public

        return redirect("core:landing_page", **kwargs)


class ResolveDigitalObjectView(RedirectView):
    """Redirect to Digital Object landing page.

    Try to resolve a given identifier to a digital object, then redirect to its landing
    page. If the digital object is not found, return 404.
    """

    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        """Resolve the identifier and return the landing page URL."""
        identifier = kwargs["identifier"]

        possible_ids = [
            identifier,
            f"https://hdl.handle.net/{identifier}",
            f"https://doi.org/{identifier}",
        ]

        for possible_id in possible_ids:
            digital_object = (
                DigitalObject.objects.filter(
                    Q(persistent_id__iexact=possible_id)
                    | Q(generated_persistent_id__iexact=possible_id)
                    | Q(identifiers__identifier__iexact=possible_id)
                )
                .annotate(is_good=Q(persistent_id=possible_id))
                .order_by("-is_good")
                .first()
            )
            if digital_object is not None:
                break

        if digital_object is None:
            raise Http404(f"Identifier '{identifier}' not found")

        url = reverse("core:landing_page", kwargs={"pk": digital_object.pk})
        query = self.request.META.get("QUERY_STRING")

        if query and self.query_string:
            url = f"{url}?{query}"

        return url


@method_decorator([cache_page(4 * 60 * 60), vary_on_cookie], name="dispatch")  # 4h
class IndexView(TemplateView):
    """View for the HELIPORT front page.

    The page shows some basic information about HELIPORT similarly to the about page,
    as well as some stats on digital objects on the instance.

    The page is cached to prevent bots and scrapers from straining the database.
    However, since the cache is based on the client's cookies, this mechanism is only
    effective for clients that do not malicously send random cookies.
    """

    template_name = "core/base/index.html"

    def __init__(self, **kwargs: Any) -> None:
        """Initialize member variables."""
        super().__init__(**kwargs)

        # figsize in inches, mainly important for the aspect ratio
        self.sparkline_figsize = (6, 1)
        self.now = datetime.now(tz=timezone.utc)
        self.sparkline_start_date = self.now - timedelta(days=3 * 365)

        # use non-interactive backend
        matplotlib.use("Agg")

    def create_sparkline(self, data: pandas.DataFrame) -> str:
        """Create a sparkline for the given data and return it as an image data URL."""
        figure, axes = pyplot.subplots(1, 1, figsize=self.sparkline_figsize)
        data.plot(ax=axes, color="gray")

        for spine in axes.spines.values():
            spine.set_visible(False)
        axes.get_xaxis().set_visible(False)
        axes.get_yaxis().set_visible(False)
        axes.set_xticks([])
        axes.set_yticks([])
        axes.get_legend().remove()

        image = BytesIO()

        # use tight bounding box and transparent background, we want only the line
        pyplot.savefig(image, format="svg", transparent=True, bbox_inches="tight")
        pyplot.close()

        image.seek(0)
        b64_data = base64.b64encode(image.read()).decode()
        return f"data:image/svg+xml;base64,{b64_data}"

    def time_series_to_dataframe(self, time_series: dict) -> pandas.DataFrame:
        """Massage "time series" into dataframe.

        "Time series" in this case is a dictionary mapping from timestamps to +1 / -1.
        """
        if not time_series:
            return pandas.DataFrame.from_records(
                {"date": [self.now], "count": 0}, index="date"
            )
        data = pandas.DataFrame.from_records(
            {"date": time_series.keys(), "count": time_series.values()}, index="date"
        )
        data.index = pandas.to_datetime(data.index)
        data = data.sort_index()
        data["count"] = data["count"].cumsum()
        return data[data.index > self.sparkline_start_date]

    def get_user_sparkline(self, users: Iterable[HeliportUser]) -> str:
        """Return a sparkline showing the number of existing users.

        The number is calculated by a cumulative summation of +1 at the respective
        ``date_joined`` of each given user's auth user.
        """
        time_series = {user.auth_user.date_joined: 1 for user in users}
        data = self.time_series_to_dataframe(time_series)
        return self.create_sparkline(data)

    def get_digital_object_sparkline(self, objects: Iterable[DigitalObject]) -> str:
        """Return a sparkline showing the number of existing digital objects.

        The number is calculated by a cumulative summation of +1 or -1 at the respective
        creation and deletion dates of the given digital objects
        """
        time_series = {
            **{o.created: 1 for o in objects if o.created is not None},
            **{o.deleted: -1 for o in objects if o.deleted is not None},
        }
        data = self.time_series_to_dataframe(time_series)
        return self.create_sparkline(data)

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """Add digital object stats to the rendering context."""
        context = super().get_context_data(**kwargs)

        users = HeliportUser.objects.filter(auth_user__last_login__isnull=False)
        projects = Project.objects.all()
        dos = DigitalObject.objects.all()

        context.update(
            {
                "num_users": users.count(),
                "num_projects": projects.filter(deleted__isnull=True).count(),
                "num_digital_objects": dos.filter(deleted__isnull=True).count(),
                "sparkline_users": self.get_user_sparkline(users),
                "sparkline_projects": self.get_digital_object_sparkline(projects),
                "sparkline_digital_objects": self.get_digital_object_sparkline(dos),
            }
        )
        return context
