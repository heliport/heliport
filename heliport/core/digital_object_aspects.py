# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later
from abc import abstractmethod
from typing import Dict, Iterable, Optional, Union

from heliport.core.digital_object_interface import GeneralDigitalObject
from heliport.core.utils.context import Context


class FileOrDirectory:
    """Baseclass for actual files or directories."""

    is_file = False
    is_directory = False

    @abstractmethod
    def get_file_info(self) -> Dict[str, str]:
        """
        Get key value pairs as string for information like file size or last modified
        date.
        """  # noqa: D205


class File(FileOrDirectory):
    """base class for actual files."""

    is_file = True

    @abstractmethod
    def mimetype(self) -> str:
        """
        Mimetype of file content. Note that the file may need to be opened before the
        mimetype is available.
        """  # noqa: D205

    @abstractmethod
    def read(self, number_of_bytes=None) -> bytes:
        """
        Get next ``number_of_bytes`` from the file or the entire file.
        Note that the actual number of bytes returned may be different from the number
        of bytes requested.
        """  # noqa: D205

    @abstractmethod
    def open(self):
        """
        Bring the file into a state that :meth:`read` starts from the beginning of
        the file. This may allocate resources that need to be freed using :meth:`close`.
        It is allowed to reopen a closed file or an already opened file to start from
        the beginning.
        """  # noqa: D205

    @abstractmethod
    def close(self):
        """Free allocated resources. Call :meth:`open` in between ``close`` and use."""

    @abstractmethod
    def size(self) -> Optional[int]:
        """The exact size of this file in bytes. ``None`` if no exact size is known."""  # noqa: D401, E501

    def __enter__(self):
        """Contextmanager."""
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Contextmanager."""
        self.close()


class FileObj(GeneralDigitalObject):
    """A GeneralDigitalObject that represents a File."""

    is_file = True
    is_directory = False

    @abstractmethod
    def as_file(self, context: Context) -> File:
        """Return the :class:`File` that this object represents."""


class Directory(FileOrDirectory):
    """base class for actual directories."""

    is_directory = True

    @abstractmethod
    def get_parts(self) -> "Iterable[Union[FileObj, DirectoryObj]]":
        """Get the parts of the digital object."""


class DirectoryObj(GeneralDigitalObject):
    """A GeneralDigitalObject that represents a Directory."""

    is_file = False
    is_directory = True

    @abstractmethod
    def as_directory(self, context: Context) -> Directory:
        """Return the :class:`Directory` that this object represents."""
