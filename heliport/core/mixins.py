# noqa: D100
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import get_object_or_404

from heliport.core import permissions
from heliport.core.app_interaction import (
    generate_project_dropdown,
    generate_project_navbar,
    get_module_for_url,
    project_graph_section_for,
)
from heliport.core.models import Project
from heliport.core.utils.context import Context, project_header_breadcrumbs

logger = logging.getLogger(__name__)


class HeliportLoginRequiredMixin(LoginRequiredMixin):
    """Mixin to make a view only available for authenticated users.

    This is also already part of the :class:`HeliportObjectMixin` and
    :class:`HeliportProjectMixin`.

    Note that this should be the first Mixin in the inheritance list to not do anything
    before ensuring access rights. See also Django's note on this in
    :class:`django.contrib.auth.mixins.LoginRequiredMixin`.
    """

    login_url = "/login"
    permission_denied_message = "Please log in to view this site"
    raise_exception = False


class HeliportBreadcrumbsMixin:
    """Mixin to handle generating breadcrumbs for navigation in HELIPORT.

    Usually there is no need to inherit form this Mixin explicitly because
    :class:`HeliportProjectMixin` and :class:`HeliportObjectMixin` already inherit from
    it.
    """

    def get_context_data(self, **kwargs):  # noqa: D102
        context = super().get_context_data(**kwargs)
        project = Project.objects.filter(pk=self.kwargs["project"]).first()

        if project is None:
            return context

        reset = False
        breadcrumb_label = ""

        # Manually implemented method has the highest priority
        if hasattr(self, "get_breadcrumb_label_and_reset"):
            breadcrumb_label, reset = self.get_breadcrumb_label_and_reset()
        # This is the case if the current view is the entry point to an app
        elif (module := get_module_for_url(self.request.path, project)) is not None:
            # Reset if module is in project graph or navbar
            reset = bool(project_graph_section_for(module))
            breadcrumb_label = module.name
        # This is the case in views that inherit from the generic HeliportObjectListView
        elif hasattr(self, "get_digital_object"):
            # This doesn't have the cache effect it is supposed to have. When
            # refactoring the mixins, make this Context a class member
            with Context(
                getattr(self.request.user, "heliportuser", None), project
            ) as cache:
                obj = self.get_digital_object(cache)
                if hasattr(obj, "label") and obj.label:
                    breadcrumb_label = obj.label
                elif hasattr(obj, "description") and obj.description:
                    breadcrumb_label = obj.description
        # This is the case in generic views deriving from Django's SingleObjectMixin
        elif hasattr(self, "get_object") and (obj := self.get_object()) != project:
            if hasattr(obj, "label") and obj.label:
                breadcrumb_label = obj.label
            elif hasattr(obj, "description") and obj.description:
                breadcrumb_label = obj.description

        # overwrite reset when explicitly provided in url parameter
        if (
            reset_param := self.request.GET.get("reset_heliport_breadcrumbs")
        ) is not None:
            reset = str(reset_param).lower() == "true"

        context["project_header_breadcrumbs"] = project_header_breadcrumbs(
            self.request, breadcrumb_label, self.request.path, reset=reset
        )

        return context


class HeliportProjectMixin(
    HeliportLoginRequiredMixin, UserPassesTestMixin, HeliportBreadcrumbsMixin
):
    """Mixin vor a Django view that shows an aspect of a HELIPORT project.

    It automatically adds the ``project`` to the rendering context and makes sure that
    the current user is allowed to access the project.

    It is required that there is a URL argument called "project" that is the id of the
    project.

    Note that this should be the first Mixin in the inheritance list to not do anything
    before ensuring access rights.
    """

    def get_context_data(self, **kwargs):  # noqa: D102
        context = super().get_context_data(**kwargs)
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        context["project"] = project
        context["project_dropdown_modules"] = generate_project_dropdown(project)
        context["project_navbar_modules"] = generate_project_navbar(project)
        self.request.session["activeHeliportProject"] = project.pk

        return context

    def test_func(self):  # noqa: D102
        user = self.request.user.heliportuser
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        return user in project.members()


class HeliportObjectMixin(
    HeliportLoginRequiredMixin, UserPassesTestMixin, HeliportBreadcrumbsMixin
):
    """Mixin vor a Django view that shows an aspect of a Digital Object in HELIPORT.

    It automatically adds the current ``project`` to the rendering context and makes
    sure that the current user is allowed to access the digital object and that the
    digital object is actually part of the project.

    It is required that there is a URL argument called "project" that is the id of the
    project. The object is taken from ``get_object()`` of the View that inherits form
    this Mixin many Django views, like DetailView, already provide this method.

    Note that this should be the first Mixin in the inheritance list to not do anything
    before ensuring access rights.
    """

    def test_func(self):  # noqa: D102
        user = self.request.user.heliportuser
        return permissions.has_permission_for(user, self.get_object())

    def get_context_data(self, **kwargs):  # noqa: D102
        context = super().get_context_data(**kwargs)
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        self.request.session["activeHeliportProject"] = project.pk

        context["project"] = project
        context["project_dropdown_modules"] = generate_project_dropdown(project)
        context["project_navbar_modules"] = generate_project_navbar(project)

        obj = self.get_object()
        if project not in obj.projects.all():
            logger.error(
                f"""incorrect project when opening {obj}.
            Project: {project.pk}; Available: {[p.pk for p in obj.projects.all()]}"""
            )
        return context
