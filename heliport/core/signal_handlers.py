# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Signal handlers for the core HELIPORT app."""

from heliport.core.models import DigitalObject, DigitalObjectIdentifier


def register_heliport_identifier_handler(
    sender, instance: DigitalObject, created: bool, using, update_fields, **kwargs
):
    """Register a default identifier for a digial object.

    An identifier for the digital object is created from
    :attr:`DigitalObject.suffix_for_pid_generation` and registered in the
    :class:`DigitalObjectIdentifier` model using the scheme `"heliport"`.

    This identifier can be used if no external PID is known for the object and no PID
    system is hooked up to HELIPORT.
    """
    if (
        not created
        or not instance.pk
        or not instance.category
        or instance.persistent_id
    ):
        return

    DigitalObjectIdentifier.objects.create(
        scheme="heliport",
        identifier=instance.suffix_for_pid_generation,
        display_text=instance.suffix_for_pid_generation,
        url=instance.full_url(),
        digital_object=instance,
    )
