# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import rdflib
from rdflib import URIRef
from rdflib.namespace import DefinedNamespace, Namespace

wikidata_namespace = Namespace("http://www.wikidata.org/wiki/")
FRBR = Namespace("http://purl.org/vocab/frbr/core#")
SCORO = Namespace("http://purl.org/spar/scoro/")
FRAPO = Namespace("http://purl.org/cerif/frapo/")
HELIPORT_NS = Namespace("https://hdl.handle.net/20.500.12865/heliport_vocab.v1#")


class WIKIDATA:  # noqa: D101
    prefix = wikidata_namespace["Q23585486"]


# fmt: off
class DATACITE(DefinedNamespace):
    """DataCite2RDF http://dx.doi.org/10.6084/m9.figshare.2075356."""

    # NOT INCLUDED DUE TO SYNTAX ERROR IN PYTHON:
    # dblp-record  # DBLP publication identifier PREFIX: https://dblp.org/rec/
    # google-scholar: URIRef  # Google Scholar author identifier. PREFIX: https://scholar.google.com/citations?user=
    # local-funder-identifier-scheme: URIRef  # only unique locally
    # local-organization-identifier-scheme: URIRef  # only unique locally
    # local-personal-identifier-scheme: URIRef  # only unique locally
    # math-genealogy: URIRef  # Mathematics Genealogy identifier. PREFIX: https://mathgenealogy.org/id.php?id=
    # national-insurance-number: URIRef  # United Kingdom National Insurance Number scheme.  # noqa: E501
    # research-gate: URIRef  # ResearchGate profile identifier. PREFIX: https://www.researchgate.net/profile/
    # social-security-number: URIRef  # United States of America social security number scheme  # noqa: E501
    # series-information: URIRef  # Used to charactierize a description of a resource that is part of a series.  # noqa: E501
    # table-of-content: URIRef  # a description type

    _NS = Namespace("http://purl.org/spar/datacite/")

    AgentIdentifier: URIRef  # An identifier that uniquely identities an individual agent.  # noqa: E501
    AgentIdentifierScheme: URIRef  # The identifier scheme used to identify an agent.
    AlternateResourceIdentifier: URIRef  # An identifier other than a DOI.
    DescriptionType: URIRef  # This class permits classification of the type of description.  # noqa: E501
    FunderIdentifier: URIRef  # An identifier that uniquely identities a funding agency.
    FunderIdentifierScheme: (
        URIRef  # The identifier scheme used to identify a funding agency.  # noqa: E501
    )
    Identifier: URIRef  # An identifier that uniquely identities an entity.
    IdentifierScheme: URIRef  # The identifier scheme used to identify an entity.
    MetadataScheme: URIRef  # The class that describe metadata schemes.
    OrganizationIdentifier: URIRef  # An identifier that uniquely identities an individual organization.  # noqa: E501
    OrganizationIdentifierScheme: (
        URIRef  # The identifier scheme used to identify an organization.  # noqa: E501
    )
    PersonalIdentifier: URIRef  # An identifier that uniquely identities an individual person.  # noqa: E501
    PersonalIdentifierScheme: URIRef  # The identifier scheme used to identify a person.
    PrimaryResourceIdentifier: (
        URIRef  # An identifier that is used as the primary identifier.  # noqa: E501
    )
    ResourceIdentifier: URIRef  # An identifier that is used to uniquely identifies a resource.  # noqa: E501
    ResourceIdentifierScheme: (
        URIRef  # The identifier scheme used to identify a resource.  # noqa: E501
    )
    hasCreatorList: URIRef  # This property allows one to specify the list of the creators of a certain resource.  # noqa: E501
    hasDescription: URIRef  # An object property permitting specification of an entity used to describe a resource.  # noqa: E501
    hasDescriptionType: URIRef  # An object property permitting specification of the type of description.  # noqa: E501
    hasGeneralResourceType: URIRef  # An object property permitting specification of the general type of a resource.  # noqa: E501
    hasIdentifier: URIRef  # An object property specifying a datacite:Identifier.
    usesIdentifierScheme: URIRef  # An object property permitting specification of the identifier scheme.  # noqa: E501
    usesMetadataScheme: URIRef  # The link between a metadata document and the scheme followed.  # noqa: E501
    abstract: URIRef  # A brief summary of a textual work.
    acm: URIRef  # ACM Digital Library author ID. PREFIX: https://dl.acm.org/profile/
    ark: URIRef  # Archival Resource Key.
    arxiv: URIRef  # Identifier for ArXiv (http://arxiv.org/).
    bibcode: URIRef  # It is the Astrophysics Data System bibliographic codes.
    crossref: (
        URIRef  # Crossref member identifier. PREFIX: https://api.crossref.org/members/
    )
    dblp: URIRef  # DBLP author identifier. PREFIX: https://dblp.org/pid/
    dia: URIRef  # The Digital Author Identification system (Netherlands).
    dnb: URIRef  # DNB catalogue number. PREFIX: https://d-nb.info/
    doi: URIRef  # Digital Object Identier
    ean13: URIRef  # International Article Number
    eissn: URIRef  # Electronic International Standard Serial Number
    fundref: URIRef  # Identifying funding sources for published scholarly research.
    gepris: (
        URIRef  # GEPRIS person identifier. PREFIX: http://gepris.dfg.de/gepris/person/
    )
    github: URIRef  # GitHub username. PREFIX: https://github.com/
    gnd: URIRef  # Gemeinsame Normdatei identifier. PREFIX: https://d-nb.info/gnd/
    handle: URIRef  # The Handle system
    ieee: URIRef  # IEEE Xplore author ID. PREFIX: https://ieeexplore.ieee.org/author/
    infouri: URIRef  # Info URI scheme
    isbn: URIRef  # International Standard Book Number
    isni: URIRef  # International Standard Name Identifier scheme
    issn: URIRef  # International Standard Serial Number
    istc: URIRef  # International Standard Text Code
    jst: URIRef  # Japanese Science and Technology Agency identifier scheme
    lattes: URIRef  # Lattes Platform number. PREFIX: http://lattes.cnpq.br/
    linkedin: URIRef  # LinkedIn personal profile identifier. PREFIX: https://www.linkedin.com/in/
    lissn: URIRef  # Linking International Standard Serial Number
    loc: URIRef  # Library of Congress authority identifier. PREFIX: https://id.loc.gov/authorities/
    lsid: URIRef  # Life Science Identifier
    methods: URIRef  # A description in a research paper documenting the specialized methods used in the work described.  # noqa: E501
    nihmsid: URIRef  # NIH Manuscript Submission Identifier
    nii: URIRef  # National Individual Identifier scheme.
    oci: URIRef  # Open Citation Identifier.
    oclc: URIRef  # OCLC control number. PREFIX: https://www.worldcat.org/oclc/
    openid: URIRef  # Standard where users can be authenticated in a decentralized manner.  # noqa: E501
    orcid: URIRef  # Open Researcher and Contributor Identifier.
    other: URIRef  # A catch-all description type.
    pii: URIRef  # Publisher Item Identifier scheme.
    pmcid: URIRef  # PubMed Central Identifier
    pmid: URIRef  # PubMed Identifier
    purl: URIRef  # Persistent Uniform Resource Locator
    repec: URIRef  # RePEc Short identifier. PREFIX: https://authors.repec.org/pro/
    researcherid: URIRef  # ResearcherID for scientific authors created and owned by Thomson Reuters.  # noqa: E501
    ror: URIRef  # The Research Organization Registry Community identifier. PREFIX: https://ror.org/
    sici: URIRef  # Serial Item and Contribution Identifier
    spar: URIRef  # Semantic Publishing and Referencing Ontologies (metadata scheme)
    twitter: URIRef  # Twitter handle. PREFIX: https://twitter.com/
    upc: URIRef  # Universal Product Code
    uri: URIRef  # Uniform Resource Identifier
    url: URIRef  # Uniform Resource Locator.
    urn: URIRef  # Uniform Resource Name.
    viaf: URIRef  # The Virtual International Authority File a personal identifier scheme  # noqa: E501
    wikidata: URIRef  # Wikidata QID. PREFIX: https://www.wikidata.org/wiki/
    wikipedia: URIRef  # Wikipedia page name. PREFIX: https://en.wikipedia.org/wiki/
    zbmath: (
        URIRef  # zbMATH author identifier. PREFIX: https://zbmath.org/authors/?q=ai:
    )
# fmt: on


# fmt: off
class PRO(DefinedNamespace):
    """PRO, the Publishing Roles Ontology."""

    _NS = Namespace("http://purl.org/spar/pro/")

    holdsRoleInTime: URIRef  # Property relating an agent to a role that the agent holds.  # noqa: E501
    RoleInTime: URIRef  # A particular situation that describe a role an agent may have.
    withRole: URIRef  # Property connecting agent's role in time to definition of the type of role held  # noqa: E501
    relatesToDocument: URIRef  # Property relating role to document that represents the context for that situation  # noqa: E501
    relatesToOrganization: URIRef
    distributor: URIRef
    editor: URIRef
    producer: URIRef
# fmt: on


class SemanticDesktop:
    """NEPOMUK Ontology."""

    class NIE(DefinedNamespace):  # noqa: D106
        _NS = Namespace("https://www.semanticdesktop.org/ontologies/2007/01/19/nie/#")
        mimeType: URIRef

    class NFO(DefinedNamespace):  # noqa: D106
        _NS = Namespace("https://www.semanticdesktop.org/ontologies/2007/03/22/nfo/#")
        fileName: URIRef
        Image: URIRef


class FABIO(DefinedNamespace):  # noqa: D101
    _NS = Namespace("http://purl.org/spar/fabio/")

    ArchivalDocument: URIRef
    Repository: URIRef
    Manuscript: URIRef
    PersonalCommunication: URIRef
    DataMangementPlan: URIRef  # Typo in ontology.
    Workflow: URIRef


namespaces = [
    ("datacite", DATACITE),
    ("frbr", FRBR),
    ("scoro", SCORO),
    ("frapo", FRAPO),
    ("fabio", FABIO),
    ("pro", PRO),
    ("nie", SemanticDesktop.NIE),
    ("nfo", SemanticDesktop.NFO),
    ("heliport", HELIPORT_NS),
    ("dcterms", rdflib.DCTERMS),
    ("dcmitype", rdflib.DCMITYPE),
    ("foaf", rdflib.FOAF),
    ("sdo", rdflib.SDO),
]
