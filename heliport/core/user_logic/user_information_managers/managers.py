# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import abc

from django.contrib.auth.models import Group

from heliport.core.models import HeliportGroup, HeliportUser


class UserInformationManager:  # noqa: D101
    @abc.abstractmethod
    def update(self, user):  # noqa: D102
        pass

    @abc.abstractmethod
    def get_display_name(self, user):  # noqa: D102
        pass

    @abc.abstractmethod
    def get_group(self, user):  # noqa: D102
        pass

    @abc.abstractmethod
    def get_authentication_backend_id(self, user):  # noqa: D102
        pass

    @abc.abstractmethod
    def import_groups(self):  # noqa: D102
        pass

    @abc.abstractmethod
    def import_users_and_groups(self):  # noqa: D102
        pass

    @abc.abstractmethod
    def get_users_by_group(self, group):  # noqa: D102
        pass

    def fetch_user(self, user):  # noqa: D102
        self.update(user)
        display_name = self.get_display_name(user)
        authentication_backend_id = self.get_authentication_backend_id(user)

        hp_user = HeliportUser.objects.filter(auth_user=user).first()
        if hp_user is None:
            hp_user = HeliportUser.objects.filter(
                authentication_backend_id=authentication_backend_id
            ).first()
        if hp_user is None:
            hp_user, user_is_new = HeliportUser.objects.get_or_create(auth_user=user)

        if (
            hp_user.display_name != display_name
            or hp_user.authentication_backend_id != authentication_backend_id
            or hp_user.auth_user != user
        ):
            hp_user.display_name = display_name
            hp_user.authentication_backend_id = authentication_backend_id
            hp_user.auth_user = user
            hp_user.save()

        group_name = self.get_group(user)
        group = Group.objects.filter(name=group_name).first()

        hp_group = HeliportGroup.objects.filter(display_name=group_name).first()
        if hp_group is None:
            if group_name is not None:
                hp_group = HeliportGroup.objects.create(
                    display_name=group_name, auth_group=group
                )
        elif hp_group.auth_group is None:
            hp_group.auth_group = group
            hp_group.save()


class BasicUserInformationManager(UserInformationManager):  # noqa: D101
    def get_display_name(self, user):  # noqa: D102
        if hasattr(user, "heliportuser"):
            result = user.heliportuser.display_name
        else:
            result = f"{user.first_name} {user.last_name}"
        if not result:
            result = user.username
        return result.strip()

    def get_group(self, user):  # noqa: D102
        return None

    def get_authentication_backend_id(self, user):  # noqa: D102
        return f"BASIC{user.pk}"

    def import_groups(self):  # noqa: D102
        pass

    def import_users_and_groups(self):  # noqa: D102
        pass

    def get_users_by_group(self, group):  # noqa: D102
        return []

    def update(self, user):  # noqa: D102
        pass
