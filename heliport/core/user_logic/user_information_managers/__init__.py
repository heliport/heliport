# noqa: D104
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .managers import BasicUserInformationManager

try:
    from .manager_ldap import LdapUserInformationManager
except ImportError:
    LdapUserInformationManager = None

__all__ = ["BasicUserInformationManager", "LdapUserInformationManager"]
