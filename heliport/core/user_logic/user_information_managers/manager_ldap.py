# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

import ldap
from django.conf import settings
from django.db.models import Q
from django_auth_ldap.backend import LDAPBackend

from heliport.core.models import HeliportGroup, HeliportUser

from .managers import UserInformationManager

logger = logging.getLogger(__name__)


class LdapUserInformationManager(UserInformationManager):  # noqa: D101
    def __init__(self):  # noqa: D107
        super().__init__()

        self._server_url = settings.LDAP_SERVER_URL
        self._bind_user = settings.LDAP_BIND_USER_DN
        self._bind_password = settings.LDAP_BIND_PASSWORD

        self._user_base_dn = settings.LDAP_USER_BASE_DN
        self._user_id_attribute = settings.LDAP_USER_ID_ATTRIBUTE
        self._user_display_name_attribute = settings.LDAP_USER_DISPLAY_NAME_ATTRIBUTE
        self._user_email_attribute = settings.LDAP_USER_ATTRIBUTE_MAP.get("email")
        self._user_object_class = settings.LDAP_USER_OBJECT_CLASS

        self._group_base_dn = settings.LDAP_GROUP_BASE_DN
        self._group_id_attribute = settings.LDAP_GROUP_ID_ATTRIBUTE
        self._group_display_name_attribute = settings.LDAP_GROUP_DISPLAY_NAME_ATTRIBUTE
        self._group_member_attribute = settings.LDAP_GROUP_MEMBER_ATTRIBUTE
        self._group_filter = settings.LDAP_GROUP_FILTER
        self._group_object_class = settings.LDAP_GROUP_OBJECT_CLASS

        self.connection = ldap.initialize(self._server_url)
        self.connection.simple_bind_s(self._bind_user, self._bind_password)

    def get_users_by_group(self, group, updated_users_byref=None):  # noqa: D102
        try:
            group_results = self.connection.search_s(
                self._group_base_dn,
                ldap.SCOPE_SUBTREE,
                filterstr=f"({self._group_id_attribute}={group.backend_id})",
                attrlist=[self._group_member_attribute],
            )
        except ldap.SIZELIMIT_EXCEEDED:
            logger.error(f"LDAP search for users in group {group} exceeded size limit")
            group_results = []

        if not group_results:
            return

        dn, attributes = group_results[0]
        for member in attributes[self._group_member_attribute]:
            member_dn = member.decode()
            try:
                member_results = self.connection.search_s(
                    member_dn,
                    ldap.SCOPE_BASE,
                    filterstr=f"(objectClass={self._user_object_class})",
                    attrlist=[
                        self._user_id_attribute,
                        self._user_display_name_attribute,
                    ]
                    + [self._user_email_attribute]
                    if self._user_email_attribute
                    else [],
                )
            except ldap.NO_SUCH_OBJECT:
                logger.warning(f"LDAP search could not find {member_dn}")
                member_results = []

            if not member_results:
                continue

            user_dn, user_attributes = member_results[0]
            user_id = user_attributes[self._user_id_attribute][0].decode()
            user_name = user_attributes.get(
                self._user_display_name_attribute, [b"Unknown User"]
            )[0].decode()
            user_email = user_attributes.get(self._user_email_attribute, [b""])[
                0
            ].decode()

            db_user = HeliportUser.objects.filter(
                authentication_backend_id=user_id
            ).first()
            if db_user is None:
                db_user = HeliportUser.objects.filter(display_name=user_name).first()
            if db_user is None:
                db_user = HeliportUser()

            if (
                db_user.display_name != user_name
                or db_user.authentication_backend_id != user_id
                or db_user.stored_email != user_email
            ):
                db_user.display_name = user_name
                db_user.authentication_backend_id = user_id
                db_user.stored_email = user_email
                db_user.save()
                if updated_users_byref is not None:
                    updated_users_byref.append(db_user)

            yield db_user

    def import_groups(self):  # noqa: D102
        group_filterstr = f"(objectClass={self._group_object_class})"
        if self._group_filter is not None:
            group_filterstr = f"(&{group_filterstr}{self._group_filter})"
        try:
            results = self.connection.search_s(
                self._group_base_dn,
                ldap.SCOPE_SUBTREE,
                filterstr=group_filterstr,
                attrlist=[self._group_id_attribute, self._group_display_name_attribute],
            )
        except ldap.SIZELIMIT_EXCEEDED:
            logger.error("LDAP group search exceeded size limit")
            results = []

        groups = (
            (
                attributes[self._group_id_attribute][0].decode(),
                attributes[self._group_display_name_attribute][0].decode(),
            )
            for dn, attributes in results
        )

        new_group_count = 0
        for group_id, group_name in groups:
            group = HeliportGroup.objects.filter(
                Q(backend_id=group_id) | Q(backend_id=None, display_name=group_name)
            ).first()
            if group is None:
                HeliportGroup.objects.create(
                    backend_id=group_id, display_name=group_name
                )
                new_group_count += 1
            elif group.backend_id is None:
                group.backend_id = group_id
                group.save()

        logger.info(f"{new_group_count} new groups imported")
        return new_group_count

    def get_user_by_id(self, user_id: str, force_fetch=False):  # noqa: D102
        assert isinstance(user_id, str), "user_id must be str"

        db_user = HeliportUser.objects.filter(authentication_backend_id=user_id).first()
        if db_user and not force_fetch:
            return db_user

        try:
            oc_filterstr = f"(objectClass={self._user_object_class})"
            id_filterstr = f"({self._user_id_attribute}={user_id})"
            user_result = self.connection.search_s(
                self._user_base_dn,
                ldap.SCOPE_SUBTREE,
                filterstr=f"(&{oc_filterstr}{id_filterstr})",
                attrlist=[self._user_display_name_attribute]
                + [self._user_email_attribute]
                if self._user_email_attribute
                else [],
            )
        except ldap.NO_SUCH_OBJECT:
            return db_user
        if len(user_result) == 0:
            return db_user

        user_dn, user_attributes = user_result[0]
        user_name = user_attributes[self._user_display_name_attribute][0].decode()
        user_email = user_attributes.get(self._user_email_attribute, [b""])[0].decode()
        if db_user is None:
            return HeliportUser.objects.create(
                display_name=user_name,
                authentication_backend_id=user_id,
                stored_email=user_email,
            )

        db_user.display_name = user_name
        db_user.stored_email = user_email
        db_user.save()
        return db_user

    def import_users_and_groups(self):  # noqa: D102
        self.import_groups()
        new_user_count = 0
        groups = list(HeliportGroup.objects.all())
        for i, group in enumerate(groups):
            new_users = []
            total = len(list(self.get_users_by_group(group, new_users)))
            new_user_count += len(new_users)
            logger.debug(
                f"imported users of group {i + 1:3}/{len(groups)}"
                f"({group} -> {len(new_users)}/{total} new)"
            )
        logger.info(f"{new_user_count} new users imported or updated")
        return new_user_count

    def update(self, user):  # noqa: D102
        LDAPBackend().populate_user(str(user))

    def get_authentication_backend_id(self, user):  # noqa: D102
        return user.ldap_user.attrs[self._user_id_attribute][0]

    def get_display_name(self, user):  # noqa: D102
        return user.ldap_user.attrs[self._user_display_name_attribute][0]

    def get_group(self, user):  # noqa: D102
        return user.ldap_user.attrs["ou"][0]
