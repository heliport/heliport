# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import re
import string
from io import StringIO

from paramiko import RSAKey

from heliport.core.utils import encryption
from heliport.core.utils.command_line_execution import create_command_executor


def deploy_ssh_key(login_info, username, password):  # noqa: D103
    machine = login_info.machine
    indirection = login_info.build_command_executor_indirection()

    key = RSAKey.generate(bits=4096)
    key_file = StringIO()
    key.write_private_key(key_file)
    key_str = key_file.getvalue()
    comment_user = re.sub(
        rf"[^{string.digits + string.ascii_letters}]",
        "_",
        login_info.user.auth_user.username,
    )
    public_key = f"{key.get_name()} {key.get_base64()} {comment_user}@HELIPORT"

    executor = create_command_executor(
        host=machine,
        username=username,
        password=password,
        indirection=indirection,
        auto_connect=False,
    )
    error_message = executor.connect_error_message()
    if error_message is not None:
        executor.close()
        return error_message
    with executor:
        executor.execute("mkdir -p ~/.ssh")
        executor.execute(f'echo "{public_key}" >> ~/.ssh/authorized_keys')
        executor.execute("chmod 600 ~/.ssh/authorized_keys")
        executor.execute("chmod 700 ~/.ssh")

    login_info.key = encryption.encrypt(key_str)
    login_info.data = public_key
    login_info.save()
    return None


def delete_ssh_key(login_info):  # noqa: D103
    executor = login_info.build_command_executor()
    public_key = login_info.data.strip()
    login_info.data = ""
    login_info.key = ""
    login_info.save()

    with executor:
        escaped_public_key = "".join(
            rf"\{c}" if c in r"$.*/[\]^" else c for c in public_key
        )
        executor.execute(f"sed -i '/{escaped_public_key}/d' ~/.ssh/authorized_keys")

        # HELIPORT used to place the key files on the remote. Remove them!
        executor.execute(
            "rm -f ~/.ssh/heliport_rsa ~/.ssh/heliport_rsa.pub"
            "~/.ssh/heliport_rsa_development ~/.ssh/heliport_rsa_development.pub"
        )
