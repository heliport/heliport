# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from importlib import import_module
from typing import Optional

from django.conf import settings
from django.contrib.auth import BACKEND_SESSION_KEY
from django.contrib.auth.models import AbstractUser
from django.http.request import HttpRequest

from heliport.core.user_logic.user_information_managers import (
    BasicUserInformationManager,
    LdapUserInformationManager,
)
from heliport.core.user_logic.user_information_managers.managers import (
    UserInformationManager,
)

possible_backends = {
    "basic": BasicUserInformationManager,
}

if settings.LDAP_ENABLED:
    assert LdapUserInformationManager is not None, "LDAP support is not installed"
    possible_backends["ldap"] = LdapUserInformationManager


def get_user_information_manager(
    request: HttpRequest,
) -> Optional[UserInformationManager]:
    """Get the user information manager for the currently logged-in user.

    The manager is chosen by getting the user's auth backend from their session and
    using it to look up the manager class in ``settings.USER_INFORMATION_MANAGERS``. If
    no auth backend is found in the session, or if, for the given backend, no manager
    class can be found in the settings, ``None`` is returned.
    """
    auth_backend = request.session.get(BACKEND_SESSION_KEY)
    if auth_backend is None:
        return None

    manager_name = settings.USER_INFORMATION_MANAGERS.get(auth_backend)
    if manager_name is None:
        return None

    module, _sep, cls = manager_name.rpartition(".")
    manager_module = import_module(module)
    manager_class = getattr(manager_module, cls)
    return manager_class()


def update_heliport_user_and_group(**kwargs):
    """Update HELIPORT user and group when a user logs in.

    This signal handler uses a user information manager to update the
    :class:`HeliportUser` and :class:`HeliportGroup` of a user instance.
    """
    user: AbstractUser = kwargs["user"]
    request: HttpRequest = kwargs["request"]
    manager = get_user_information_manager(request)
    if manager is not None:
        manager.fetch_user(user)
