# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import abc
import logging
import re
import typing
from datetime import datetime
from itertools import chain

from django.template.loader import render_to_string
from rdflib import URIRef
from rest_framework.serializers import ValidationError

from heliport.core import models
from heliport.core.utils.string_tools import parse_date_time
from heliport.core.utils.url import url_encode
from heliport.core.vocabulary_descriptor import (
    LiteralTriple,
    RelationTriple,
    TripleDescription,
)

logger = logging.getLogger(__name__)


class AttrObj:
    """Attribute Object to hold information that a normal DigitalObject would have for
    the purpose of supplying a simple constant value to the attribute_digital_object
    parameter when creating instances of :class:`BaseAttribute`:

    - ``label`` - Short string to be shown as the property name
    - ``persistent_id`` - Globally unique persistent identifier for the property (in the
      format of a URL)
    - ``description`` (optional) - Longer string to describe this property
    """  # noqa: D205, D400

    def __init__(self, label, persistent_id, description=""):
        """See class itself."""
        self.label = label
        self.persistent_id = persistent_id
        self.description = description

    def to_rdflib_node(self):
        """Return an :class:`rdflib:rdflib.Node` for this object.

        This method is needed when describing a graph in
        :meth:`AttributeType.rdf_triples`. See
        :func:`heliport.core.vocabulary_descriptor.TripleDescription.to_node`.
        :meth:`heliport.core.models.DigitalObject.to_rdflib_node` implements this
        interface for DigitalObjects.
        """
        return URIRef(url_encode(self.persistent_id))

    def __str__(self):  # noqa: D105
        return f"{self.label} ({self.persistent_id})"


class InternalValueDescription:
    """
    Contains the information to get or create some object.
    This is used for implementations of :meth:`AttributeType.deserialize`.
    There are two reasons to return an InternalValueDescription instead of the value itself:

    - The value does not exist yet (no persistent object should be created during validation)
    - The value depends on things unknown during validation (the parent where this value becomes an attribute)

    For an example see :class:`ContributionT.ContributionDescription`.
    """  # noqa: D205, D400, E501

    #: Specifies if :meth:`get_value` returns a list or a single object
    #: It might not be possible to deduce this simply from the result of :meth:`get_value` because  # noqa: E501
    #: :meth:`get_value` might make persistent changes and therefore should be called only once.  # noqa: E501
    describes_list = False

    @abc.abstractmethod
    def get_value(self, parent):
        """
        A function that is called when actually needing the value this object is describing.

        :param parent: The object where the returned object is assigned to an attribute.
        """  # noqa: D401, E501
        pass


class DeserializationError(ValidationError):
    """
    Indicates that the data provided to an implementation of :meth:`AttributeType.deserialize` is invalid.
    ``DeserializationError`` being a subclass of ``ValidationError`` leads to special handling in Django Rest Framework,
    where ``ValidationError`` s are used to generate an error response for the user.
    """  # noqa: D205, E501

    pass


class AttributeType:
    """
    The abstract base class for types used in a :class:`BaseAttribute`.
    The ``AttributeType`` controls how values of some property behave and are presented/encoded.
    Different HELIPORT modules can implement additional subclasses of this class to be able to
    represent any object they need. Many basic types are already implemented.
    """  # noqa: D205, E501

    #: Path to a django template for showing an object of this type.
    #: The template is rendered with context from :meth:`get_context_data`.
    template_name = "core/types/string.html"

    #: Path to a django template for editing an object of this type in a form.
    #: The template is rendered with context from :meth:`get_form_context_data`
    form_template_name = "core/form_field/input.html"

    @abc.abstractmethod
    def rdf_triples(
        self, sub, pred, the_object, options
    ) -> typing.Iterator[TripleDescription]:
        """
        Generator of :class:`heliport.core.vocabulary_descriptor.TripleDescription` s describing objects of this type in rdf.
        It is necessary to use ``TripleDescriptions`` instead of returning an :class:`rdflib:rdflib.Graph` directly
        because ``TripleDescription`` s allow recursive encoding, of the objects used in returned triples, later.
        """  # noqa: D205, D401, E501
        pass

    def get_context_data(self, obj, options):
        """
        The context (a dict) to be used when rendering :attr:`template_name` to render objects of this type in html.
        The function can return ``None`` to indicate that the object should not be shown at all.
        The following things are in the context by default:

        - ``value`` (string representation of th object)
        - ``options``
        - ``obj``

        :param obj: the object to be rendered as html
        :param options: other kwargs when instantiating a :class:`BaseAttribute`
        """  # noqa: D205, D400, D401, E501
        if obj is None:
            return None
        the_string = self.to_string(obj, options)
        if not the_string:
            return None
        return {"value": the_string, "options": options, "obj": obj}

    def get_form_context_data(self, key, obj, options):
        """
        The context (a dict) to be used when rendering :attr:`form_template_name` to render objects of this type as an html form input.
        The following things are in the context by default:

        - ``value`` (string representation of th object) if available
        - ``key``
        - ``options``

        :param key: the name under which the input should be posted when submitting the form
        :param obj: the object to be shown as an input
        :param options: other kwargs when instantiating a :class:`BaseAttribute`
        """  # noqa: D205, D400, D401, E501
        context = {"key": key, "options": options}
        if obj is not None:
            the_string = self.to_string(obj, options)
            if the_string is not None:
                context["value"] = the_string
        return context

    def to_string(self, obj, options) -> typing.Optional[str]:
        """
        Represent an object of this type as a human-readable string. Return value of ``None`` signals to not show the object at all.

        :param obj: the object to be shown as an input
        :param options: other kwargs when instantiating a :class:`BaseAttribute`
        """  # noqa: E501
        if obj is not None:
            return str(obj)
        return None

    def to_html(self, obj, options):
        """
        Represent an object of this type in HTML. Returning ``None`` signals to not show the object at all.
        By default, the template specified with :attr:`template_name` is rendered using result form :meth:`get_context_data`.

        :param obj: the object to be shown as an input
        :param options: other kwargs when instantiating a :class:`BaseAttribute`
        """  # noqa: D205, E501
        context = self.get_context_data(obj, options)
        if context is None:
            return None
        return render_to_string(self.template_name, context)

    def to_inputs(self, key, obj, options):
        """
        Represent an object of this type as HTML form input.
        By default, the template specified with :attr:`form_template_name` is rendered using result form :meth:`get_form_context_data`.

        :param key: the name under which the input should be posted when submitting the form
        :param obj: the object to be shown as an input or ``None`` if not object exists yet
        :param options: other kwargs when instantiating a :class:`BaseAttribute`
        """  # noqa: D205, E501
        context = self.get_form_context_data(key, obj, options)
        return [render_to_string(self.form_template_name, context)]

    def render_empty_input(self, key, options):
        """
        Represent an empty object of this type as HTML form input. This is similar to :meth:`to_input` but for when the object is not created yet.
        By default, the template specified with :attr:`form_template_name` is rendered using result form :meth:`get_form_context_data`.

        :param key: the name under which the input should be posted when submitting the form
        :param options: other kwargs when instantiating a :class:`BaseAttribute`
        """  # noqa: D205, E501
        one_input_as_list = self.to_inputs(key, None, options)
        return one_input_as_list[0]

    def to_id(self, obj, options):
        """Generate an id for the ``obj``. This is used in :meth:`BaseAttribute.get_id` to generate an id for an attribute-value pair."""  # noqa: E501
        return self.to_string(obj, options)

    def to_json(self, obj, options):
        """Represent an ``obj`` of this type in json."""
        return self.to_string(obj, options)

    def test_exists(self, obj, options):
        """
        Test if ``obj`` is considered existent. In some cases only existent objects might be shown.
        By default, an ``obj`` is considered existent if it is not ``None``.
        """  # noqa: D205, E501
        return obj is not None

    def deserialize(self, obj, options):
        """
        Generate an object of this type from ``obj``.

        :param obj: description of an object of this type as a string or primitive type (like ``list`` of ``dict``)
        :param options: other kwargs when instantiating a :class:`BaseAttribute`
        """  # noqa: E501
        return obj


class UrlT(AttributeType):
    """A string that encodes a URL."""

    template_name = "core/types/url.html"

    def rdf_triples(self, sub, pred, the_url, options):  # noqa: D102
        if the_url:
            yield RelationTriple(sub, pred, URIRef(url_encode(the_url)))

    def test_exists(self, obj, options):
        """A URL is considered existed if it is not ``None`` and not ``""``."""
        return bool(obj)


class EnumStringT(AttributeType):
    """
    Type that represents values that are tuples of the actual value and a string representation of the value.
    ``EnumStringT`` is used in conjunction with :class:`EnumAttribute` to describe attributes that are
    :ref:`Django enumeration types <django:field-choices-enum-types>`.
    """  # noqa: D205, E501

    #: Maps actual values to their string representations, allowing to replace some default string representations.  # noqa: E501
    #: When deserializing, it is allowed to supply string representations that are converted to values via this dict.  # noqa: E501
    value_dict: dict

    def __init__(self, value_dict=None):
        """See class itself."""
        if value_dict is None:
            self.value_dict = {}
        else:
            self.value_dict = value_dict

    def rdf_triples(self, sub, pred, value_tuple, options):  # noqa: D102
        if value_tuple is not None:
            value, value_str = value_tuple
            yield LiteralTriple(sub, pred, self.value_dict.get(value, value_str))

    def to_string(self, value_tuple, options):  # noqa: D102
        if value_tuple is not None:
            value, value_str = value_tuple
            return value_str
        return None

    def to_id(self, value_tuple, options):  # noqa: D102
        if value_tuple is not None:
            value, value_str = value_tuple
            return value
        return "no_value"

    def to_json(self, value_tuple, options):
        """Objects of ``EnumStringT`` are represented like ``{"value": ..., "description": ...}`` in json."""  # noqa: E501
        if value_tuple is not None:
            value, value_str = value_tuple
            return {"value": value, "description": value_str}
        return None

    def deserialize(self, obj, options):  # noqa: D102
        if obj is None:
            return None
        reverse_value_dict = {value: key for key, value in self.value_dict}
        if obj in self.value_dict:
            # already deserialized
            return obj
        if obj in reverse_value_dict:
            return reverse_value_dict[obj]
        return super().deserialize(obj, options)


class BasicLiteralType(AttributeType):
    """
    Something that can be passed directly to :class:`rdflib:rdflib.Literal`
    in order to represent it in an :class:`rdflib:rdflib.Graph`.
    """  # noqa: D205

    def rdf_triples(self, sub, pred, the_object, options):  # noqa: D102
        if the_object:
            yield LiteralTriple(sub, pred, the_object)


class StringT(BasicLiteralType):
    """Represent an object that is a python string."""

    def deserialize(self, obj, options):
        """
        Deserialize like the base implementation but return ``""`` instead of ``None``
        if not ``"allow_null"`` is set to ``True`` in ``options``.
        """  # noqa: D205
        result = super().deserialize(obj, options)
        if result is None and not options.get("allow_null", False):
            result = ""
        return result


class BasicObjType(AttributeType):
    """
    Represent objects like :class:`AttrObj` or :class:`heliport.core.models.DigitalObject` that might have the following Attributes:

    - persistent_id
    - digital_object_id
    - identifier_link
    - label
    - description

    Don't use this ``AttributeType`` directly. Use a specific subclass like :class:`DigitalObjectT` or :class:`AttrObjT`.
    """  # noqa: D400, E501

    def rdf_triples(self, sub, pred, the_object, options):  # noqa: D102
        if the_object is not None:
            yield RelationTriple(
                sub, pred, the_object, recursive=options.get("recursive", True)
            )

    def to_id(self, obj, options):  # noqa: D102
        if hasattr(obj, "persistent_id"):
            return obj.persistent_id
        if hasattr(obj, "digital_object_id"):
            return f"heliport_digital_object_{obj.digital_object_id}"
        return super().to_id(obj, options)

    def to_json(self, obj, options):  # noqa: D102
        if obj is None:
            return None

        result = {}

        if hasattr(obj, "identifier_link"):
            result["@id"] = obj.identifier_link
        elif hasattr(obj, "persistent_id") and obj.persistent_id:
            result["@id"] = obj.persistent_id

        do_id = getattr(obj, "digital_object_id", None)
        if do_id:
            result["digital_object_id"] = do_id
        label = getattr(obj, "label", None)
        if label:
            result["label"] = label
        description = getattr(obj, "description", None)
        if description:
            result["description"] = description

        if (
            hasattr(obj, "identifier_label")
            and hasattr(obj, "identifier_display")
            and obj.identifier_display != result.get("@id")
        ):
            result[obj.identifier_label] = obj.identifier_display
        return result


class UriRefT(AttributeType):
    """Represent values that are :class:`rdflib:rdflib.URIRef` s."""

    template_name = "core/types/url.html"

    def rdf_triples(self, sub, pred, the_object, options):  # noqa: D102
        if the_object:
            yield RelationTriple(sub, pred, the_object)


class DateT(BasicLiteralType):
    """Represent a value that is a python ``datetime`` object."""

    def to_string(self, obj, options):
        """
        Use ``strftime`` to format ``obj`` that is a ``datetime`` as ``string``.
        The time is included only if ``"include_time"`` is set to ``True`` in ``options``.
        """  # noqa: D205, E501
        if obj:
            if not hasattr(obj, "strftime"):
                logger.warning(f"DateT that is not a date: {obj}")
                return super().to_string(obj, options)
            time_format = ""
            if options.get("include_time"):
                time_format = " %H:%M"
            return obj.strftime(f"%b %d, %Y{time_format}")
        return None

    def deserialize(self, obj, options):
        """
        Uses :func:`utils.string_tools.parse_date_time` to generate a datetime from ``obj``.

        :raise DeserializationError: if no valid format could be found.
        """  # noqa: D401, E501
        if obj == "" or obj is None:
            return None
        result = parse_date_time(obj)
        if result is None:
            raise DeserializationError(
                f"Failed to parse date. Try something like {datetime.now().strftime('%Y-%m-%d %H-%M')}"  # noqa: E501
            )
        return result

    def get_form_context_data(self, key, obj, options):
        """Includes additionally to the base implementation ``type`` of ``date`` in context to set it on html input."""  # noqa: E501
        context = super().get_form_context_data(key, obj, options)
        context["type"] = "date"
        return context


class DigitalObjectT(BasicObjType):
    """Represent a value that is a :class:`heliport.core.models.DigitalObject`."""

    #: Path to a django template for showing a DigitalObject.
    #: The template is rendered with context from :meth:`AttributeType.get_context_data()`.  # noqa: E501
    template_name = "core/types/digital_object.html"
    form_template_name = "core/form_field/digital_object.html"

    def to_string(self, obj, options):  # noqa: D102
        if obj is None:
            return None
        if obj.label:
            return obj.label
        if obj.description:
            return obj.description
        if obj.persistent_id:
            return obj.persistent_id
        return None

    def get_form_context_data(self, key, obj, options):
        """
        In addition to base implementation include ``"label"`` and ``"value"`` in context.

        - ``label`` - Human readable representation of the DigitalObject
        - ``value`` - ``digital_object_id`` to be used as value in an html input
        """  # noqa: E501
        context = super().get_form_context_data(key, obj, options)

        context["label"] = self.to_string(obj, options)
        try:
            context["value"] = obj.digital_object_id
        except AttributeError:
            context["value"] = ""

        return context

    def deserialize(self, obj, options):  # noqa: D102
        if obj is None or obj == "":
            return None

        tries = []
        digital_object_id = None
        uri = None
        label = None
        description = None

        if isinstance(obj, int):
            digital_object_id = obj

        elif isinstance(obj, str):
            digital_object_id = obj
            uri = obj
            label = obj
            description = obj

        elif isinstance(obj, dict):
            tried_keys = []
            digital_object_id = obj.get("digital_object_id", obj.get("pk"))
            if digital_object_id is None:
                tried_keys.append("digital_object_id")
                tried_keys.append("pk")
            uri_keys = ["@id", "persistent_id", "pid", "uri", "unique_id"]
            for uri_key in uri_keys:
                uri = obj.get(uri_key, uri)
            if uri is None:
                tried_keys.extend(uri_keys)
            description = obj.get("description")
            label = obj.get("label")
            if label is None:
                label = description
            if label is None:
                label = obj.get("name")
            if description is None:
                description = label
            if label is None and description is None:
                tried_keys.extend(["description", "label", "name"])

        else:
            tries.append("Tried int, str, dict but nothing matched")

        if digital_object_id is not None:
            if str(digital_object_id).isdecimal():
                try:
                    return models.DigitalObject.objects.get(
                        digital_object_id=str(digital_object_id)
                    )
                except models.DigitalObject.DoesNotExist:
                    tries.append("Tried digital_object_id but nothing found")
            else:
                tries.append(
                    "Tried digital_object_id but value is not a decimal number"
                )

        if uri is not None:
            candidate = (
                models.DigitalObject.objects.filter(persistent_id=uri)
                .order_by("digital_object_id")
                .first()
            )
            if candidate is not None:
                return candidate
            tries.append("Tried uri but found nothing.")

        queries = []
        query_suffixes = ["", "__iexact", "__contains", "__icontains"]
        if label is not None:
            for s in query_suffixes:
                queries.append({f"label{s}": label})
        if description is not None:
            for s in query_suffixes:
                queries.append({"label__isnull": True, f"description{s}": description})
        for query in queries:
            candidates = models.DigitalObject.objects.filter(**query)
            if len(candidates) == 1:
                return candidates.first()
            if len(candidates) > 1:
                tries.append(
                    f"Tried {query} but found more than one ({len(candidates)}) matches."  # noqa: E501
                )
            else:
                tries.append(f"Tried {query} but found nothing.")

        raise DeserializationError(f"Object not found: {' '.join(tries)}")


class ProjectT(DigitalObjectT):
    """Represent a value that is a :class:`heliport.core.models.Project`."""

    template_name = "core/types/digital_object.html"

    def to_string(self, obj, options):  # noqa: D102
        if obj is None:
            return None
        if obj.label:
            return obj.label
        return "New Project"

    def deserialize(self, obj, options):  # noqa: D102
        result = super().deserialize(obj, options)
        if result is None:
            return None
        try:
            return models.Project.objects.get(
                digital_object_id=result.digital_object_id
            )
        except models.Project.DoesNotExist as e:
            raise DeserializationError("was not a project") from e

    def get_form_context_data(self, key, obj, options):
        """
        In addition to the base implementation include ``"type_constraint"`` to digital_object_id of the project type.
        This is used when autocompleting in an html form input.
        """  # noqa: D205, E501
        context = super().get_form_context_data(key, obj, options)
        context["type_constraint"] = models.Vocabulary().Project.digital_object_id
        return context


class AttrObjT(BasicObjType):
    """Represent a value that is an :class:`AttrObj`."""

    def to_string(self, obj, options):  # noqa: D102
        if obj is None:
            return None
        if obj.label:
            return obj.label
        if obj.description:
            return obj.description
        if obj.persistent_id:
            return obj.persistent_id
        return None


class PersonT(BasicObjType):
    """Represent a value that is a :class:`heliport.core.models.HeliportUser`."""

    def to_id(self, heliport_user, options):  # noqa: D102
        if heliport_user:
            return f"heliport_user_{heliport_user.pk}"
        return super().to_id(heliport_user, options)

    def to_json(self, heliport_user, options):  # noqa: D102
        if heliport_user is None:
            return None
        result = {
            "user_id": heliport_user.user_id,
            "display_name": heliport_user.display_name,
        }
        if heliport_user.authentication_backend_id:
            result["authentication_backend_id"] = (
                heliport_user.authentication_backend_id
            )
        if heliport_user.orcid:
            result["orcid"] = heliport_user.orcid
        if heliport_user.affiliation:
            result["affiliation"] = heliport_user.affiliation
        if heliport_user.public_email:
            result["email"] = heliport_user.public_email
        return result

    def deserialize(self, obj, options):  # noqa: D102
        tries = []
        user_id = None
        display_name = None
        authentication_backend_id = None
        orcid = None
        email = None

        if obj == "" or obj is None:
            return None

        if isinstance(obj, int):
            user_id = obj
            authentication_backend_id = obj

        elif isinstance(obj, str):
            user_id = display_name = obj
            authentication_backend_id = obj
            orcid = email = obj

        elif isinstance(obj, dict):
            key_tries = set()
            user_id = obj.get(
                "user_id", obj.get("id", obj.get("authentication_backend_id"))
            )
            if user_id is None:
                key_tries.update(["user_id", "id", "authentication_backend_id"])
            display_name = obj.get("display_name", obj.get("name", user_id))
            if display_name is None:
                key_tries.update(["display_name", "name"])
            authentication_backend_id = obj.get("authentication_backend_id", user_id)
            if authentication_backend_id is None:
                key_tries.add("authentication_backend_id")
            orcid = obj.get("orcid", user_id)
            if orcid is None:
                key_tries.add("orcid")
            email = obj.get("email", obj.get("e-mail"))
            if email is None:
                key_tries.add("email")
            tries.append(f"Tried keys {', '.join(key_tries)} but nothing matched")

        else:
            tries.append("Tried int str and dict, but nothing matched.")

        queries = []
        if user_id is not None and str(user_id).isdecimal():
            queries.append({"user_id": user_id})
        if display_name is not None:
            queries.append({"display_name": display_name})
            queries.append({"display_name__iexact": display_name})
            queries.append({"display_name__icontains": display_name})
        if authentication_backend_id is not None:
            queries.append({"authentication_backend_id": authentication_backend_id})
        if orcid is not None:
            queries.append({"orcid": orcid})
        if email is not None:
            queries.append({"auth_user__email": email, "email_is_public": True})
            queries.append({"stored_email": email, "email_is_public": True})

        for query in queries:
            candidates = models.HeliportUser.objects.filter(**query)
            if len(candidates) == 1:
                return candidates.first()
            if len(candidates) == 0:
                tries.append(f"tried {query} but nothing matched")
            elif len(candidates) < 5:
                tries.append(
                    f"tried {query} but could not decide between "
                    + ", ".join(user.display_name for user in candidates)
                )
            else:
                tries.append(f"tried {query} but many users matched")

        raise DeserializationError("Could not find value: " + "; ".join(tries))


class ContributionT(BasicObjType):
    """Represent a value that is a :class:`heliport.core.models.Contribution`."""

    form_template_name = "core/form_field/contribution.html"

    def to_string(self, obj, options):  # noqa: D102
        if obj is None:
            return None
        return f"{obj.contributor.display_name} ({obj.get_type_display()})"

    def to_id(self, obj, options):  # noqa: D102
        if obj is None:
            return super().to_id(obj, options)
        return f"heliport_contribution_{obj.pk}"

    def to_json(self, obj, options):
        """Represent the contribution ``obj`` as json. This uses :class:`PersonT` to represent the contributor as json."""  # noqa: E501
        if obj is None:
            return None
        person_t = PersonT()
        return {
            "contribution_id": obj.contribution_id,
            "type": obj.type,
            "contributor": person_t.to_json(obj.contributor, options),
        }

    def get_form_context_data(self, key, obj, options):
        """
        In addition to the base implementation this includes
        the contribution ``"type"`` and all potentially possible ``"contribution_types"``.
        """  # noqa: D205, E501
        context = super().get_form_context_data(key, obj, options)
        if isinstance(obj, models.Contribution):
            context["type"] = obj.type
        context["contribution_types"] = models.Contribution.ContributionTypes.choices
        return context

    def deserialize(self, obj, options):  # noqa: D102
        if obj == "" or obj is None:
            return None

        tries = []
        id_error = "failed"
        if isinstance(obj, dict):
            if "contribution_id" in obj:
                cid = obj["contribution_id"]
            else:
                cid = None
                id_error = "was not found in dict"
        else:
            cid = obj
        if str(cid).isdecimal():
            try:
                return models.Contribution.objects.get(contribution_id=str(cid))
            except models.Contribution.DoesNotExist:
                id_error = "nothing matched"
        elif cid is not None:
            id_error = "value is not a decimal number"
        tries.append(f'Tried "contribution_id" but {id_error}.')

        errors = []
        if isinstance(obj, str):
            contribution_match = re.match(r"\s*\)([^(]*)\( (.*)", obj[::-1])
            obj = {"contributor": obj}
            if contribution_match is not None:
                c_type = models.Contribution.type_from_str(contribution_match[1][::-1])
                if c_type is not None:
                    obj = {
                        "contributor": contribution_match[2][::-1],
                        "contribution_type": c_type,
                    }
                else:
                    tries.append(
                        f"Found contribution_type suffix but {contribution_match[1][::-1]} is not valid."  # noqa: E501
                    )

        if isinstance(obj, dict):
            person_t = PersonT()
            types = {
                human_readable: actual
                for actual, human_readable in models.Contribution.ContributionTypes.choices  # noqa: E501
            }
            contribution_type = obj.get(
                "type",
                obj.get(
                    "contribution_type", models.Contribution.ContributionTypes.OTHER
                ),
            )
            if contribution_type not in types.values() and contribution_type in types:
                contribution_type = types[contribution_type]
            if contribution_type not in types.values():
                errors.append("contribution_type was invalid")
                contribution_type = None

            contributor = obj.get(
                "contributor", obj.get("name", obj.get("user", obj.get("person")))
            )
            if contributor is None:
                errors.append("contributor not specified")
            else:
                try:
                    contributor = person_t.deserialize(contributor, options)
                except DeserializationError as e:
                    errors.append(", ".join(e.detail))
                    contributor = None

            if contribution_type and contributor:
                return self.ContributionDescription(contribution_type, contributor)
        else:
            errors.append("was not a dict")
        if not errors:
            errors = ["failed"]
        tries.append(
            f"Tried to create contribution from description but {' and '.join(errors)}."
        )

        raise DeserializationError("Could not find value: " + " ".join(tries))

    class ContributionDescription(InternalValueDescription):
        """
        Description of a :class:`heliport.core.models.Contribution`.
        This is the type returned by deserialization. Deserialization can not return a Contribution directly, because
        the :class:`heliport.core.models.DigitalObject`, where a contribution is for, is sometimes unknown during
        deserialization but is always required when storing a :class:`heliport.core.models.Contribution` in the database.
        Additionally, it is bad to create persistent objects during deserialization because deserialization is also
        used as validation in Django Rest framework.
        """  # noqa: D205, E501

        def __init__(self, contribution_type, contributing_heliport_user):
            """See class itself."""
            self.contribution_type = contribution_type
            self.contributor = contributing_heliport_user

        def get_value(self, parent):
            """
            Generate a real :class:`heliport.core.models.Contribution` from this description.

            :param parent: The object where the attribute of type :class:`ContributionT` is set.
                This is the object where the contribution is for.
            """  # noqa: E501
            contribution, is_new = models.Contribution.objects.get_or_create(
                contributor=self.contributor, contribution_to=parent
            )
            contribution.type = self.contribution_type
            contribution.save()
            return contribution


class ListT(AttributeType):
    """
    Represent a value that is a ``list`` where all elements have the type ``item_type``.
    Similar to when instantiating a :class:`BaseAttribute` specifying ``item_type``
    as an instance of :class:`AttributeType` or as a callable that returns an instance of :class:`AttributeType`
    (like a subclass of :class:`AttributeType` itself) is possible::

        string_list1 = ListT(StringT())
        string_list2 = ListT(StringT)
    """  # noqa: D205, D400, E501

    template_name = "core/types/list.html"

    def __init__(
        self, item_type: typing.Union[AttributeType, typing.Type[AttributeType]]
    ):
        """See class itself."""
        super().__init__()
        if isinstance(item_type, AttributeType):
            self.item_type = item_type
        else:
            self.item_type = item_type()

    def rdf_triples(self, sub, pred, the_objects, options):  # noqa: D102
        if not the_objects:
            return
        for the_object in the_objects:
            yield from self.item_type.rdf_triples(sub, pred, the_object, options)

    def to_string(self, object_list, options):
        """Items of the list are separated by newline characters."""
        if object_list:
            return "\n".join(
                sorted(self.item_type.to_string(obj, options) for obj in object_list)
            )
        return None

    def get_context_data(self, obj, options):
        """
        Ignores base implementation and returns instead:

        - ``values`` - A list of html representations of all items in the list
        - ``options`` - like in the base implementation, other kwargs when instantiating a :class:`BaseAttribute`
        """  # noqa: D400, E501
        if not obj:
            return None
        return {
            "values": [self.item_type.to_html(value, options) for value in obj],
            "options": options,
        }

    def test_exists(self, obj, options):  # noqa: D102
        return bool(obj)

    def to_inputs(self, key, obj, options):
        """Return a list of rendered html inputs. See also :meth:`AttributeType.to_inputs`."""  # noqa: E501
        if not obj:
            return [self.render_empty_input(key, options)]
        t = self.item_type
        return chain(*(t.to_inputs(key, item, options) for item in obj))

    def render_empty_input(self, key, options):  # noqa: D102
        return self.item_type.render_empty_input(key, options)

    def to_json(self, obj, options):  # noqa: D102
        if obj is None:
            return None
        return sorted([self.item_type.to_json(item, options) for item in obj], key=str)

    def deserialize(self, obj, options):  # noqa: D102
        if obj is None:
            return None

        if not isinstance(obj, list):
            if isinstance(obj, str):
                obj = obj.splitlines()
            else:
                raise DeserializationError("Value was not a list")
        result = []
        exceptions = {}
        for i, item in enumerate(obj):
            try:
                result.append(self.item_type.deserialize(item, options))
            except DeserializationError as e:
                exceptions[i] = e.detail
        if not exceptions:
            if any(isinstance(i, InternalValueDescription) for i in result):
                return self.ListDescription(result)
            return result
        raise DeserializationError(exceptions)

    class ListDescription(InternalValueDescription):
        """
        :meth:`ListT.deserialize` returns an instance of this class.
        This is needed because items might return an instance of :class:`InternalValueDescription` when deserialized
        so :class:`ListT.ListDescription` is used to evaluate :class:`InternalValueDescription` s recursively.
        """  # noqa: D205, E501

        describes_list = True

        def __init__(self, values):
            """See class itself."""
            self.values = values

        def get_value(self, parent):
            """Return a list where all :class:`InternalValueDescription` s are evaluated."""  # noqa: E501
            return [
                value.get_value(parent)
                if isinstance(value, InternalValueDescription)
                else value
                for value in self.values
            ]


def get_attribute_type(obj: typing.Any) -> typing.Type[AttributeType]:
    """
    This function is used for example to allow the ``attribute_type`` to be optional for :class:`ConstantAttribute`,
    where the AttributeType can be deduced from the value using :func:`get_attribute_type`.

    :param obj: The instance for which an AttributeType should be found.
    :return: Appropriate subclass of :class:`AttributeType` to be used with the ``obj`` instance.
    :raise AttributeError: if no matching AttributeType was found.
    """  # noqa: D205, D401, E501
    if isinstance(obj, models.DigitalObject):
        return DigitalObjectT
    if isinstance(obj, AttrObj):
        return AttrObjT
    if isinstance(obj, URIRef):
        return UriRefT
    # consider adding the unknown type if error occurs
    raise AttributeError(f"unknown type: {type(obj)}")


class BaseAttribute:
    """
    Base class for all attribute descriptions.

    As an example the description of ``distance_from_sun``::

        class Planet:
            distance_from_sun: str

        @static_attributes.register(Planet)
        def planet_attributes() -> typing.List[BaseAttribute]:
            return [
                Attribute(
                    AttrObj("Distance from the sun", "https://example.org/distance_from_sun"), StringT, "distance_from_sun"
                )
            ]

    .. note::
        The ``DecoratorDict`` :data:`heliport.core.serializers.static_attributes` can be imported and used to register the
        metadata attribute description in heliport. If ``Planet`` were a ``DigitalObject`` the decorator
        :func:`heliport.core.serializers.register_digital_object_attributes` should be used.

    Attribute descriptions contain:

    Description of the metadata property
        In the example this is detailed information what the "distance from the sun" is. This can include

        - information like persistent identifier of the property in some metadata standard
        - human-readable description
        - detailed description using key value pairs. (Example: `Distance from earth <https://www.wikidata.org/wiki/Property:P2583>`_)

        This information is specified via the first argument ``attribute_digital_object`` of the constructor
        and available via the ``attribute`` property.

        In the most basic case, like just a persistent identifier and a human-readable label,
        an :class:`AttrObj` can be used. For more complex descriptions a
        :class:`heliport.core.models.DigitalObject` can be used. To reference DigitalObjects in code you can create a class
        similar to :class:`heliport.core.models.Vocabulary`. To describe DigitalObjects in code
        (needed before they can be referenced) you can write a function similar to
        :func:`heliport.core.vocabulary_core.insert_core_vocabulary` and call it in your apps ``post_migrate`` hook
        (see :class:`heliport.core.apps.HeliportConfig` for an example).

    How to get the value given an object
        In the example this is the information that the value for the distance from the sun can be gotten from the
        ``"distance_from_sun"`` parameter of a ``Planet`` object.

        This information is determined by the subclass of :class:`BaseAttribute` that is used. For example the subclass
        :class:`Attribute` uses :attr:`Attribute.object_field` (the attribute name) to get the value.
        Feel free to write additional subclasses in your app to be able to access complex attributes.

    How to work with the value (type specific information)
        In the example this is how distance should be displayed and serialized. A generic type or a more useful, but with
        more effort to implement, specific type for distances could be used. Feel free to implement subclasses of
        :class:`AttributeType` in your app to control how your objects are displayed and serialized.

        The type is available via :attr:`attribute_type` and specified via the second argument ``attribute_type`` of the
        constructor. when specifying ``attribute_type`` an instance of :class:`AttributeType` or a callable
        that returns an instance of :class:`AttributeType` (like a subclass of :class:`AttributeType` itself)
        can be used::

            attr1 = Attribute(..., StringT(), ...)
            attr2 = Attribute(..., StringT, ...)

    Additional parameters
        Other ``kwargs`` are stored and available to :class:`AttributeType` and in subclasses
        of :class:`BaseAttribute`. Both can define what additional parameters they understand. Some parameters are
        defined in :class:`BaseAttribute`. You can look through the attributes and methods to find them. They can
        be identified by the following style: "**ADDITIONAL PARAMETER** ``"<parameter name>"``".

    Helper functions
        A lot of functions are implemented in this base class. These functions call corresponding functions in the
        subcomponents of the description in the appropriate way.
    """  # noqa: E501

    #: the type all values of the described attribute have
    attribute_type: AttributeType

    def __init__(self, attribute_digital_object, attribute_type, **kwargs):
        """See class itself."""
        if isinstance(attribute_type, AttributeType):
            self.attribute_type = attribute_type
        else:
            self.attribute_type = attribute_type()
        self.attribute = attribute_digital_object
        self.kwargs = kwargs

    @property
    def handles_list(self) -> bool:
        """Determines if the value of the described attribute is a list."""
        return isinstance(self.attribute_type, ListT)

    @property
    def label(self) -> str:
        """Short human-readable text to describe the metadata attribute."""
        if self.attribute.label:
            return self.attribute.label
        if self.attribute.description:
            return self.attribute.description
        if self.attribute.persistent_id:
            return self.attribute.persistent_id
        return "Property"

    @property
    def property_category(self) -> str:
        """
        Properties can be grouped into categories to split a long list of properties into smaller ones for ease of
        viewing. This attribute takes its value from **ADDITIONAL PARAMETER** ``"property_category"``.
        """  # noqa: D205, E501
        return self.kwargs.get("property_category")

    @property
    def overwrite_property_id(self) -> str:
        """
        Property value pairs can be identified via an id returned from :meth:`get_id`. This id is normally generated
        automatically but can be specified explicitly via the **ADDITIONAL PARAMETER** ``"property_id"``.
        `overwrite_property_id` contains the explicitly specified id or ``None`` if it does not exist.
        """  # noqa: D205, E501
        return self.kwargs.get("property_id")

    @property
    def persistent_id(self) -> str:
        """The persistent identifier of the metadata attribute."""
        if hasattr(self.attribute, "identifier_link"):
            return self.attribute.identifier_link
        return self.attribute.persistent_id

    @property
    def description(self) -> str:
        """A longer human-readable description of the metadata attribute."""
        return self.attribute.description

    @property
    def read_only(self) -> bool:
        """Specified if the attribute is read only.

        The value is taken from the **ADDITIONAL PARAMETER** ``"read_only"``.
        """
        return self.kwargs.get("read_only", False)

    @property
    def omit_from_external_objects(self) -> bool:
        """Is ``True`` if attribute is omitted from metadata of external objects.

        An object is external if it represents an external identifier.
        (:attr:`heliport.core.models.DigitalObject.represents_external_identifier`)

        This is needed for example to exclude the heliport creation date for external
        objects from the metadata export since the object already existed before it was
        "created" in HELIPORT.

        The value is taken from the **ADDITIONAL PARAMETER** ``"omit_from_external"``.
        """
        return self.kwargs.get("omit_from_external", False)

    @property
    def is_public_name(self) -> str:
        """
        Attributes can be specified to be publicly visible. This information could be stored as a separate property on
        some object::

            class A:
                b: int
                b_is_public: bool

        The name of that attribute ("b_is_public" in the example) is this property. The value is taken from the
        **ADDITIONAL PARAMETER** ``"is_public_name"``. Note that subclasses of :class:`Attribute` generate this
        parameter by default as "<attribute_name>_is_public".
        """  # noqa: D205, D400, E501
        return self.kwargs.get("is_public_name")

    @property
    def is_always_public(self):
        """
        Some information, like a globally unique id, is always publicly known about any object.
        ``is_always_public`` contains if the described property is such an always publicly known property.
        This is specified via the **ADDITIONAL PARAMETER** ``"is_public"``.
        """  # noqa: D205, E501
        return self.kwargs.get("is_public")

    def value_from(self, obj):
        """Get the value of this attribute from the ``obj``.

        Implements the "How to get the value given an object" part of the attribute
        description. ``value_from`` uses :meth:`getattr` internally to get the value.
        ``value_from`` should always be used when a value needs to be gotten while
        :meth:`getattr` should always be overwritten in subclasses to implement the
        actual getting functionality. The corresponding method for setting attribute
        values is :meth:`value_to`.

        :param obj: The object of which the attribute value should be determined.
        :return: The value of the described attribute on ``obj``
        """
        if self.omit_from_external_objects and (
            hasattr(obj, "represents_external_identifier")
            and obj.represents_external_identifier
        ):
            return None

        return self.getattr(obj)

    def value_to(self, obj, value):
        """
        Implements the "How to set the value given an object" part of the attribute description.
        ``value_to`` uses :meth:`setattr` internally to set the value. ``value_to`` should always be used when a
        value needs to be set while :meth:`setattr` should always be overwritten in subclasses to implement the actual
        setting functionality. The corresponding method for getting attribute values is :meth:`value_from`.

        :param obj: The object of which the attribute value should be set.
        :param value: The new value that should be used for the attribute on ``obj``.
        """  # noqa: D205, D401, E501
        if not self.read_only:
            return self.setattr(obj, value)
        logger.warning(f"tried to write to read only field {self.label}")
        return None

    def exists_on(self, obj):
        """Determine if the attributes exists on ``obj``.

        See :meth:`AttributeType.test_exists` for more details.
        """
        if self.omit_from_external_objects and (
            hasattr(obj, "represents_external_identifier")
            and obj.represents_external_identifier
        ):
            return False
        value = self.value_from(obj)
        return self.attribute_type.test_exists(value, self.kwargs)

    @abc.abstractmethod
    def getattr(self, obj):
        """
        Don't use this function but instead used :meth:`value_from`.
        Overwrite this function in subclasses to implement how the described attribute is accessed on some object.

        :param obj: The object where the attribute value should be gotten from
        :return: The value of the attribute on ``obj`` or ``None`` if ``obj`` does not have a value for this attribute
        """  # noqa: D205, E501
        pass

    def setattr(self, obj, value):
        """
        Don't use this function but instead use :meth:`value_to`.
        Overwrite this function in subclasses to implement the way the described attribute sets the value on some object.

        :param obj: The object where the attribute value should be set to ``value``
        :param value: The value that the described attribute should be set to
        """  # noqa: D205, E501
        logger.warning(f"setattr not implemented for {self}")

    def rdf_triples(self, obj):
        """Serialize ``obj`` into RDF triples. For more details see :meth:`AttributeType.rdf_triples`."""  # noqa: E501
        value = self.value_from(obj)
        yield from self.attribute_type.rdf_triples(
            obj, self.attribute, value, self.kwargs
        )

    def to_string(self, obj) -> typing.Optional[str]:
        """Serialize ``obj`` as human-readable string. For more details see :meth:`AttributeType.to_string`."""  # noqa: E501
        attribute_value = self.value_from(obj)
        return self.attribute_type.to_string(attribute_value, self.kwargs)

    def to_html(self, obj):
        """Serialize ``obj`` as html. For more details see :meth:`AttributeType.to_html`."""  # noqa: E501
        attribute_value = self.value_from(obj)
        return self.attribute_type.to_html(attribute_value, self.kwargs)

    def to_inputs(self, key, obj=None):
        """
        Serialize ``obj`` as list of html inputs.
        For more details see :meth:`AttributeType.to_inputs`.

        :param key: The key that should be used for this attribute in POST data.
        :param obj: The object for which the inputs should be generated.
            If ``obj`` is not provided empty form inputs are generated.
        :return: List of rendered html inputs.
        """  # noqa: D205
        attribute_value = None if obj is None else self.value_from(obj)
        return self.attribute_type.to_inputs(key, attribute_value, self.kwargs)

    def render_empty_input(self, key):
        """
        Similar to :meth:`to_inputs` without `obj` parameter but behaviour could be customized independently in some
        :class:`AttributeType`. See :meth:`AttributeType.render_empty_input` for more information.
        """  # noqa: D205, E501
        return self.attribute_type.render_empty_input(key, self.kwargs)

    def to_json(self, obj):
        """Serialize ``obj`` to json. See :meth:`AttributeType.to_json` for more details."""  # noqa: E501
        attribute_value = self.value_from(obj)
        return self.attribute_type.to_json(attribute_value, self.kwargs)

    def deserialize(self, data):
        """
        Deserialize ``data`` to an instance of the type described by :attr:`attribute_type`.
        See :meth:`AttributeType.deserialize` for more information.
        """  # noqa: D205, E501
        return self.attribute_type.deserialize(data, self.kwargs)

    def get_is_public(self, obj) -> bool:
        """
        By default, :attr:`is_always_public` and :attr:`is_public_name` are considered. Subclasses may implement
        additional behaviour. See also :meth:`set_is_public` and :meth:`public_settable`.

        :param obj: The object of which the decision if the described attribute is public should be made.
        :return: Decision if the described attribute is public on ``obj``.
        """  # noqa: D205, E501
        if self.is_always_public:
            return True
        if self.is_public_name and hasattr(obj, self.is_public_name):
            return getattr(obj, self.is_public_name)
        return False

    def set_is_public(self, obj, value: bool):
        """
        By default, :attr:`is_public_name` is used to set the public status of the described attribute.
        If ``obj`` has an attribute ``save``, it is called after attribute specified by :attr:`is_public_name` is set to ``value``.
        Subclasses may implement additional behaviour. See also :meth:`get_is_public`.

        :param obj: The object for which the public status of the described attribute should be changed.
        :param value: The new public status. (``True`` means the attribute should be public)
        :raise AssertionError: If attribute :attr:`is_public_name` can not be found.
        :raise AssertionError: If set :attr:`is_always_public` attribute to private.
        """  # noqa: D205, E501
        if self.is_public_name and hasattr(obj, self.is_public_name):
            setattr(obj, self.is_public_name, value)
            if hasattr(obj, "save"):
                obj.save()
        elif self.is_always_public:
            assert value, "can't set public to False"
        else:
            assert not value, "can't set public to True"

    def public_settable(self, obj):
        """Determine if :meth:`set_is_public` is going to succeed on ``obj``.

        See also :meth:`get_is_public`.
        """
        if self.is_always_public:
            return False
        return self.is_public_name and hasattr(obj, self.is_public_name)

    def bind_to(self, obj):
        """
        An attribute description does not contain information about a concrete object that might have the attribute.
        This enables the construction of queries based on attribute descriptions. However, this means that many
        methods require an ``obj`` parameter. :class:`BoundAttribute` stores a :class:`BaseAttribute` together with an
        ``obj`` to address this.

        :param obj: The attribute description may describe attributes of ``obj``.
        :return: A :class:`BoundAttribute` where ``obj`` is used and not needed any more as parameter.
        """  # noqa: D205, D401, E501
        return BoundAttribute(self, obj)

    def get_id(self, obj):
        """
        ID of a property value pair to identify it e.g. in html forms.
        The ID is generated based on :attr:`persistent_id`, :attr:`label`, :attr:`property_category`,
        :attr:`overwrite_property_id` and :meth:`AttributeType.to_id`.
        """  # noqa: D205, E501
        if self.overwrite_property_id is not None:
            return self.overwrite_property_id

        attr_id = self.persistent_id
        if not attr_id:
            attr_id = self.label
        value = self.value_from(obj)
        return f"{self.property_category}_{attr_id}_{self.attribute_type.to_id(value, self.kwargs)}"  # noqa: E501


class Attribute(BaseAttribute):
    """
    Get value from attribute name as string. In the following example "distance_from_sun" is the attribute name::

        class Planet:
            distance_from_sun: str

        @static_attributes.register(Planet)
        def planet_attributes() -> typing.List[BaseAttribute]:
            return [
                Attribute(
                    AttrObj("Distance from the sun", "https://example.org/distance_from_sun"), StringT, "distance_from_sun"
                )
            ]

    See also :class:`BaseAttribute` for more information.
    """  # noqa: D400, E501

    #: The name of the attribute where to get the value from.
    object_field: str

    def __init__(
        self, attribute_digital_object, attribute_type, object_field, **kwargs
    ):
        """See class itself."""
        if "is_public_name" not in kwargs:
            kwargs["is_public_name"] = f"{object_field}_is_public"
        if "property_id" not in kwargs:
            kwargs["property_id"] = f"heliport_static_attribute_{object_field}"
        super().__init__(attribute_digital_object, attribute_type, **kwargs)
        self.object_field = object_field

    def getattr(self, obj):
        """Get attribute by name (:attr:`object_field`)."""
        if not hasattr(obj, self.object_field):
            return None
        return getattr(obj, self.object_field)

    def setattr(self, obj, value):
        """
        Set attribute by name.

        :raise TypeError: if attribute specified by :attr:`object_field` is a read only property
        """  # noqa: E501
        try:
            setattr(obj, self.object_field, value)
        except TypeError as e:
            raise TypeError(
                f'{type(self).__name__} can not set "{self.object_field}" to "{value}". \n{e}'  # noqa: E501
            ) from None


class ConstantAttribute(BaseAttribute):
    """Get always the same constant ``value`` without looking at the object that has this attribute."""  # noqa: E501

    #: The constant value always used for this attribute
    value: typing.Any

    def __init__(self, attribute, value, attribute_type=None, **kwargs):
        """See class itself."""
        if attribute_type is None:
            attribute_type = get_attribute_type(value)
        super().__init__(attribute, attribute_type, is_public=True, **kwargs)
        self.value = value

    def getattr(self, obj):
        """
        :param obj: The object that has this constant attribute (ignored)
        :return: Always value specified by :attr:`value`
        """  # noqa: D205
        return self.value


class TypeAttribute(ConstantAttribute):
    """
    An attribute that specifies the type of the object.

    Normally subclasses of :class:`BaseAttribute` have a description of the property as first parameter of the
    constructor. ``TypeAttribute`` fills this information automatically to describe the property `is of type`.
    ``the_type`` can be anything (but is a :class:`heliport.core.models.DigitalObject` in most cases;
    see documentation for description of metadata property in :class:`BaseAttribute` for information how to access
    ``DigitalObject`` s in code). If the subclass of :class:`AttributeType` can not be deduced for ``the_type``,
    the type of ``the_type`` can be specified by the ``attribute_type`` argument.
    """  # noqa: E501

    def __init__(self, the_type, attribute_type=None, **kwargs):
        """See class itself."""
        super().__init__(models.Vocabulary().type, the_type, attribute_type, **kwargs)


class ManyAttribute(Attribute):
    """
    Get the value for this attribute from a :ref:`Django many-to-many relationship
    <django:topics/db/examples/many_to_many:many-to-many relationships>`.

    ``attribute_type`` is always a :class:`ListT`. If the passed ``attribute_type`` is not a kind of :class:`ListT`,
    the ``attribute_type`` is assumed to be a :class:`ListT` of the specified ``attribute_type``.

    An example usage of ``ManyAttribute``::

        class Planet(models.Model):
            orbits = models.ManyToManyField(Star)

        @static_attributes.register(Planet)
        def planet_attributes() -> typing.List[BaseAttribute]:
            return [
                ManyAttribute(
                    AttrObj("orbits", "https://example.org/orbits"), StarT, "orbits"
                )
            ]

    See :class:`BaseAttribute` for details where things like ``static_attributes`` come from in the example.
    """  # noqa: D205, E501

    def __init__(
        self, attribute_digital_object, attribute_type, object_field, **kwargs
    ):
        """See class itself."""
        if not isinstance(attribute_type, ListT):
            attribute_type = ListT(attribute_type)
        super().__init__(
            attribute_digital_object, attribute_type, object_field, **kwargs
        )

    def getattr(self, obj):
        """
        Get value from many attribute.

        If the **ADDITIONAL PARAMETER** ``"casted"`` is set to True, the ``casted_or_self()`` method is called
        on all resulting values. :class:`heliport.core.models.DigitalObject` implements such a
        :meth:`heliport.core.models.DigitalObject.casted_or_self` method. This is necessary because django returns
        objects that are not casted to their most specific type but instead to a base class.
        """  # noqa: E501
        manager = super().getattr(obj)
        if manager is not None:
            result = manager.all()
            if self.kwargs.get("casted", False):
                result = [r.casted_or_self() for r in result]
            return result
        return None

    def setattr(self, obj, value):
        """Set value via many attribute."""
        current = set(self.getattr(obj))
        target = set(value)
        manager = super().getattr(obj)
        for new in target - current:
            manager.add(new)
        for old in current - target:
            manager.remove(old)


class FunctionResultAttribute(Attribute):
    """
    Get the value for this attribute by calling a method of an object specified by ``object_field``.
    This is very similar to the base class but :class:`Attribute` would return a function whereas this class returns
    the result when calling this function. ``FunctionResultAttribute`` is a read only attribute.
    """  # noqa: D205, E501

    def __init__(
        self, attribute_digital_object, attribute_type, object_field, **kwargs
    ):
        """See class itself."""
        kwargs["read_only"] = True
        super().__init__(
            attribute_digital_object, attribute_type, object_field, **kwargs
        )

    def getattr(self, obj):
        """Get value by calling the method, specified by ``object_field``, on ``obj`` with no arguments."""  # noqa: E501
        function = super().getattr(obj)
        if function is not None:
            return function()
        return None


class EnumAttribute(Attribute):
    """
    Get value as a tuple of actual value and human-readable string for attributes that
    are :ref:`Django enumeration types <django:field-choices-enum-types>`.
    Should be used with an :class:`AttributeType`, like :class:`EnumStringT`, that supports this format of value.
    """  # noqa: D205, E501

    def getattr(self, obj):
        """
        Use :meth:`get_[object_field]_display() <django:django.db.models.Model.get_FOO_display>`
        to get the human-readable component of the tuple. Get the first component like the base class :class:`Attribute`.
        """  # noqa: D205, E501
        value = super().getattr(obj)
        if value is None:
            return None
        return value, getattr(obj, f"get_{self.object_field}_display")()


class DynamicAttribute(BaseAttribute):
    """
    Get value for a :class:`heliport.core.models.DigitalObject` via its :class:`heliport.core.models.DigitalObjectRelation` s
    or :class:`heliport.core.models.DigitalObjectAttributes`.

    ``attribute_model`` is an instance of `heliport.core.models.DigitalObjectRelation` or :class:`heliport.core.models.DigitalObjectAttributes`.
    Specifying ``attribute_type`` is optional. The following rules are used to generate the default:

    - :class:`heliport.core.models.DigitalObjectRelation` → :class:`DigitalObjectT`
    - :class:`heliport.core.models.DigitalObjectAttributes` → :class:`StringT`
    - is ``DigitalObjectAttribute`` + predicate or value indicate url → :class:`UrlT`

    .. warning::
        The object needs to be known (it is the ``subject`` of ``attribute_model``) when
        instantiating a ``DynamicAttribute`` class, whereas other :class:`BaseAttribute` subclasses are general.
        So by the nature of `dynamic attributes` ``DynamicAttribute`` can only be used when generating attributes
        dynamically at runtime for a given object.

    .. note::
        Why is this class called "DynamicAttribute"?

        HELIPORT distinguishes between `dynamic attributes` that are
        attributes editable by users at runtime without conforming to any schema and `static attributes` that are defined in a
        schema and stored in specialized tables.
    """  # noqa: D205, E501

    #: The django model where this attribute gets its value from.
    attribute_model: typing.Union[
        models.DigitalObjectAttributes, models.DigitalObjectRelation
    ]

    def __init__(self, attribute_model, attribute_type=None, **kwargs):
        """See class itself."""
        if attribute_type is None and hasattr(attribute_model, "object"):
            attribute_type = DigitalObjectT

        if attribute_type is None:
            predicate = attribute_model.predicate
            vocab = models.Vocabulary()

            pids_match = (
                predicate is not None
                and predicate.persistent_id == vocab.primary_topic.persistent_id
            )
            value_startswith_http_s = str(attribute_model.value).startswith(
                "http://"
            ) or str(attribute_model.value).startswith("https://")
            value_no_spaces = " " not in str(attribute_model.value)

            predicate_indicates_url = pids_match and value_startswith_http_s
            value_indicates_url = value_no_spaces and value_startswith_http_s

            if predicate_indicates_url or value_indicates_url:
                attribute_type = UrlT

        if attribute_type is None:
            attribute_type = StringT

        super().__init__(attribute_model.predicate, attribute_type, **kwargs)
        self.attribute_model = attribute_model

    def getattr(self, obj):
        """Get value from :attr:`attribute_model`."""
        if self.attribute_model is None:
            return None
        if hasattr(self.attribute_model, "object"):
            return self.attribute_model.object
        return self.attribute_model.value

    def setattr(self, obj, value):
        """
        Set value on :attr:`attribute_model`.

        If ``value`` for :class:`heliport.core.models.DigitalObjectAttributes` is ``""``
        or ``value`` is ``None`` :attr:`attribute_model` is deleted. If :attr:`attribute_model` is ``None`` a new instance
        of :class:`heliport.core.models.DigitalObjectRelation` or :class:`heliport.core.models.DigitalObjectAttributes` is created
        in the database based on if ``value`` is a :class:`heliport.core.models.DigitalObject` or not.

        :raise AssertionError: if ``obj`` does not match subject of :attr:`attribute_model`
        """  # noqa: E501
        if value is None or value == "":
            if self.attribute_model is not None:
                self.attribute_model.delete()
        else:
            if self.attribute_model is None:
                if isinstance(value, models.DigitalObject):
                    self.attribute_model = models.DigitalObjectRelation()
                else:
                    self.attribute_model = models.DigitalObjectAttributes()
                self.attribute_model.predicate = self.attribute
                self.attribute_model.subject = obj

            assert self.attribute_model.subject == obj
            if hasattr(self.attribute_model, "object"):
                self.attribute_model.object = value
            else:
                self.attribute_model.value = value
            self.attribute_model.save()

    def public_settable(self, obj):
        """
        Because it is known how to set :attr:`heliport.core.models.DigitalObjectAttributes.is_public` and
        :attr:`heliport.core.models.DigitalObjectRelation.is_public` this always returns `True`.

        :raise AssertionError: if ``obj`` does not match subject of :attr:`attribute_model`
        """  # noqa: D205, E501
        assert self.attribute_model.subject == obj
        return True

    def get_is_public(self, obj):
        """
        Get public availability from :attr:`attribute_model`.

        :raise AssertionError: if ``obj`` does not match subject of :attr:`attribute_model`
        """  # noqa: E501
        assert self.attribute_model.subject == obj
        return self.attribute_model.is_public

    def set_is_public(self, obj, value: bool):
        """
        Set public availability of :attr:`attribute_model` to ``value``.

        :raise AssertionError: if ``obj`` does not match subject of :attr:`attribute_model`
        """  # noqa: E501
        assert self.attribute_model.subject == obj
        self.attribute_model.is_public = value
        self.attribute_model.save()

    def get_id(self, obj) -> str:
        """Generate string to identify this attribute in html forms. See :meth:`BaseAttribute.get_id` for more information."""  # noqa: E501
        if hasattr(self.attribute_model, "digital_object_relation_id"):
            return f"heliport_digital_object_relation_{self.attribute_model.digital_object_relation_id}"  # noqa: E501
        if hasattr(self.attribute_model, "digital_object_attribute_id"):
            return f"heliport_digital_object_attribute_{self.attribute_model.digital_object_attribute_id}"  # noqa: E501
        return super().get_id(obj)


class BoundAttribute:
    """
    Contains information about a subclass of :class:`BaseAttribute` (``attribute``) and a specific object (``bound_to``).
    This allows it to offer properties in many places where ``BaseAttribute`` has methods that have an ``obj`` parameter.
    In django templates this is more than a way to simplify code, because only functions without arguments can be called
    there.
    One way to create a ``BoundAttribute`` is using :meth:`BaseAttribute.bind_to`.
    """  # noqa: D205, E501

    def __init__(self, attribute, bound_to):
        """See class itself."""
        self.attr = attribute
        self.obj = bound_to

    @property
    def label(self):
        """See :meth:`BaseAttribute.label`."""
        return self.attr.label

    @property
    def value(self):
        """See :meth:`BaseAttribute.value_from`."""
        return self.attr.value_from(self.obj)

    @value.setter
    def value(self, new_value):
        """See :meth:`BaseAttribute.value_to`."""
        self.attr.value_to(self.obj, new_value)

    @property
    def rdf_triples(self):
        """See :meth:`BaseAttribute.rdf_triples`."""
        return self.attr.rdf_triples(self.obj)

    @property
    def as_string(self):
        """See :meth:`BaseAttribute.to_string`."""
        return self.attr.to_string(self.obj)

    @property
    def as_html(self):
        """See :meth:`BaseAttribute.to_html`."""
        return self.attr.to_html(self.obj)

    @property
    def as_json(self):
        """See :meth:`BaseAttribute.to_json`."""
        return self.attr.to_json(self.obj)

    @property
    def is_public(self):
        """See :meth:`BaseAttribute.get_is_public`."""
        return self.attr.get_is_public(self.obj)

    @is_public.setter
    def is_public(self, value):
        """See :meth:`BaseAttribute.set_is_public`."""
        self.attr.set_is_public(self.obj, value)

    @property
    def public_settable(self):
        """See :meth:`BaseAttribute.public_settable`."""
        return self.attr.public_settable(self.obj)

    @property
    def exists(self):
        """See :meth:`BaseAttribute.exists_on`."""
        return self.attr.exists_on(self.obj)

    @property
    def property_category(self):
        """See :attr:`BaseAttribute.property_category`."""
        return self.attr.property_category

    @property
    def property_id(self):
        """See :meth:`BaseAttribute.get_id`."""
        return self.attr.get_id(self.obj)
