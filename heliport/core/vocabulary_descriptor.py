# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from abc import abstractmethod

from rdflib import BNode, Literal, URIRef
from rdflib.term import Node


class TripleDescription:  # noqa: D101
    def __init__(self, recursive=True):  # noqa: D107
        self.recursive = recursive

    @abstractmethod
    def add_to_graph(self, graph):  # noqa: D102
        pass

    @abstractmethod
    def objects(self):  # noqa: D102
        pass

    @staticmethod
    def to_node(the_object):  # noqa: D102
        if isinstance(the_object, Node):
            return the_object
        return the_object.to_rdflib_node()

    @staticmethod
    def filter_nodes(digital_objects):  # noqa: D102
        return [d for d in digital_objects if not isinstance(d, Node)]


class LiteralTriple(TripleDescription):  # noqa: D101
    def __init__(self, subj, pred, literal, recursive=True):  # noqa: D107
        super().__init__(recursive)
        assert subj is not None
        assert pred is not None
        assert literal is not None
        self.subject = subj
        self.predicate = pred
        self.literal = literal

    def add_to_graph(self, graph):  # noqa: D102
        graph.add(
            (
                self.to_node(self.subject),
                self.to_node(self.predicate),
                Literal(self.literal),
            )
        )

    def objects(self):  # noqa: D102
        return self.filter_nodes([self.subject, self.predicate])


class RelationTriple(TripleDescription):  # noqa: D101
    def __init__(self, subj, pred, obj, recursive=True):  # noqa: D107
        super().__init__(recursive)
        assert subj is not None
        assert pred is not None
        assert obj is not None
        self.subject = subj
        self.predicate = pred
        self.object = obj

    def add_to_graph(self, graph):  # noqa: D102
        graph.add(
            (
                self.to_node(self.subject),
                self.to_node(self.predicate),
                self.to_node(self.object),
            )
        )

    def objects(self):  # noqa: D102
        return self.filter_nodes([self.subject, self.predicate, self.object])


class PropertyList(TripleDescription):  # noqa: D101
    def __init__(  # noqa: D107
        self, subj, pred, key_values, intermediate=None, recursive=True
    ):
        super().__init__(recursive)
        assert subj is not None
        assert pred is not None
        assert all(not (k is None or v is None) for k, v in key_values.items())
        self.subject = subj
        self.predicate = pred
        self.key_values = key_values
        self.intermediate = BNode() if intermediate is None else URIRef(intermediate)

    def add_to_graph(self, graph):  # noqa: D102
        graph.add(
            (
                self.to_node(self.subject),
                self.to_node(self.predicate),
                self.intermediate,
            )
        )
        for key, value in self.key_values:
            graph.add(
                (
                    self.intermediate,
                    self.to_node(key),
                    Literal(value),
                )
            )

    def objects(self):  # noqa: D102
        return self.filter_nodes(
            [self.subject, self.predicate, *(k[0] for k in self.key_values)]
        )
