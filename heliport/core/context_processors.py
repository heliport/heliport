# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Additional context processors for HELIPORT."""

from django.conf import settings

from heliport.core.utils.version import heliport_version_string


def auth_settings(request):
    """Add auth-related settings into context.

    This is required in order to access these settings from templates without adapting
    allauth views.
    """
    return {
        "EMAIL_ENABLED": settings.EMAIL_ENABLED,
        "LOCAL_ACCOUNT_SIGNUP": settings.LOCAL_ACCOUNT_SIGNUP,
        "LDAP_ENABLED": settings.LDAP_ENABLED,
        "LDAP_SIGNUP": settings.LDAP_SIGNUP,
        "OIDC_ENABLED": settings.OIDC_ENABLED,
        "OIDC_SIGNUP": settings.OIDC_SIGNUP,
    }


def heliport_version(request):
    """Add HELIPORT version string into context.

    This makes the string available in all templates so that it can be used in the
    footer.
    """
    return {"HELIPORT_VERSION_STRING": heliport_version_string}
