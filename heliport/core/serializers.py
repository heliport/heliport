# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""This module is for serialization into Datacite, RDF and JSON.

The JSON format is used for API endpoints via
`Django rest framework <https://www.django-rest-framework.org/>`_. Using this, it is
typical to put the serializer classes into a "serializers.py" file.

For the serialization into RDF, attributes are described.
See :class:`heliport.core.attribute_description.BaseAttribute` for more detail.

This particular serializer module of the HELIPORT core app also contains renderer
classes for rendering the RDF into a particular representation as well as other helper
functionality for serialization.
"""

import hashlib
import json
import re
import typing
from abc import abstractmethod
from collections import defaultdict
from itertools import chain, zip_longest

from accept_types import get_best_match
from datacite import schema43
from django.conf import settings
from django.http import HttpResponse, JsonResponse
from django.urls import reverse
from django.utils import timezone
from rdflib import DCTERMS, FOAF, RDF, SDO, BNode, Graph, Literal, URIRef
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import Field as RestField
from rest_framework.fields import empty as empty_rest_data
from rest_framework.fields import html as html_utils

from heliport.core.utils.collections import DecoratorDict
from heliport.core.utils.IDs import remove_special_chars

from .attribute_description import (
    Attribute,
    AttrObj,
    BaseAttribute,
    BoundAttribute,
    ContributionT,
    DateT,
    DigitalObjectT,
    DynamicAttribute,
    FunctionResultAttribute,
    InternalValueDescription,
    ListT,
    ManyAttribute,
    PersonT,
    ProjectT,
    StringT,
    TypeAttribute,
    UrlT,
)
from .models import (
    Contribution,
    DigitalObject,
    DigitalObjectAttributes,
    DigitalObjectRelation,
    HeliportGroup,
    HeliportUser,
    Image,
    LoginInfo,
    Project,
    Vocabulary,
)
from .permissions import has_read_permission, has_write_permission
from .utils.json_ld import TargetDescription, reverse_context_mapping
from .vocabulary import FRAPO, FRBR, PRO, SCORO, SemanticDesktop
from .vocabulary import namespaces as custom_namespaces
from .vocabulary_descriptor import LiteralTriple, RelationTriple, TripleDescription

#: Metadata attributes for each class.
#: Register a function that returns a list of
#: :class:`heliport.core.attribute_description.BaseAttribute` subclass instances.
#: For registering attributes for a subclass of DigitalObject use
#: :func:`register_digital_object_attributes`.
#: The extension point is called "static" because it is mostly used to describe
#: properties from the static database model which causes these attributes to be
#: included in the metadata export and in principle even enables these attributes to be
#: used by other apps just based on the underlying metadata standard. For dynamic
#: attributes, without hardcoded scheme, see
#: :func:`heliport.core.models.assert_relation`. For working with dynamic attributes
#: similar to static attributes just like ``my_instance.my_property`` see
#: :class:`heliport.core.models.MetadataAttribute` - so these are almost as simple as
#: normal django fields but don't require to be described/registered as
#: ``static_attributes`` separately.
static_attributes = DecoratorDict()
#: Register a :class:`DATACITESerializer` or :class:`DigitalObjectDATACITESerializer`
#: subclass here for metadata export in DataCite format.
datacite_serializers = DecoratorDict()
#: Low level rdf serializers. Register a subclass of :class:`RDFSerializer` for your
#: custom class. For subclasses of digital object the default serializer uses the
#: the :attr:`static_attributes` for serialization.
rdf_serializers = DecoratorDict()
#: The most low level registry of serializers that just return any django response
#: object. The dict is keyed by a short string a user can add to the url to use this
#: renderer for metadata serialization. Register your custom renderer to support
#: metadata formats that are not DataCite or RDF or additional serializations of those.
#: Look at the implementation of :func:`select_renderer` to see how content negotiation
#: is performed to select the renderer.
renderers = DecoratorDict()


def register_digital_object_attributes(cls):
    """Decorate a function to register attributes.

    ``cls`` is a subclass of :class:`heliport.core.models.DigitalObject`.
    This decorator includes some general attributes automatically.
    """

    def decorator(func):
        """Decorate."""

        def attributes():
            """Compute the attribute list, including some general attributes."""
            return digital_object_attributes() + func()

        static_attributes[cls] = attributes
        return attributes

    return decorator


class NamespaceField(serializers.Field):
    """This is a constant field for setting the namespace in API serializers.

    The field is by default write-only, which means the namespace is not returned as
    json but is only set when data is posted or patched.
    """

    def __init__(self, namespace: str, **kwargs):
        """Initialize."""
        kwargs.setdefault("write_only", True)
        kwargs.setdefault("required", False)
        kwargs.setdefault(
            "help_text",
            "A namespace for registering PIDs. "
            'Parts are separated by "/" e.g. "My/Nested/Namespace". '
            "Usually this doesn't need to be set manually. "
            f'Defaults to "{namespace}".',
        )
        self.namespace = namespace
        super().__init__(**kwargs)

    def to_representation(self, value):
        """Transform the *outgoing* native value into primitive data."""
        return value

    def to_internal_value(self, data):
        """Transform the *incoming* primitive data into a native value."""
        return self.namespace

    def get_value(self, dictionary):
        """Given the *incoming* primitive data, return the value for this field.

        This should be the value that should be validated and transformed to a native
        value.
        """
        return self.namespace


class ProjectSerializer(serializers.ModelSerializer):  # noqa: D101
    namespace_str = NamespaceField("HELIPORT/Project")

    class Meta:  # noqa: D106
        model = Project
        fields = [
            "project_id",
            "label",
            "created",
            "deleted",
            "description",
            "group",
            "owner",
            "co_owners",
            "persistent_id",
            "namespace_str",
        ]


class UserSerializer(serializers.ModelSerializer):  # noqa: D101
    class Meta:  # noqa: D106
        model = HeliportUser
        fields = [
            "user_id",
            "authentication_backend_id",
            "display_name",
            "orcid",
            "affiliation",
        ]


class GroupSerializer(serializers.ModelSerializer):  # noqa: D101
    class Meta:  # noqa: D106
        model = HeliportGroup
        fields = ["display_name"]


class ContributionSerializer(serializers.ModelSerializer):  # noqa: D101
    class Meta:  # noqa: D106
        model = Contribution
        fields = ["contribution_id", "type", "contributor", "contribution_to"]


class TokenSerializer(serializers.ModelSerializer):  # noqa: D101
    token = serializers.CharField(source="key")

    class Meta:  # noqa: D106
        model = LoginInfo
        fields = ["login_info_id", "name", "token"]


class DigitalObjectListSerializer(serializers.ListSerializer):  # noqa: D101
    def to_representation(self, digital_objects):  # noqa: D102
        result = []
        for digital_object in digital_objects:
            digital_object = digital_object.casted_or_self()
            serializer = DigitalObjectAPISerializer(
                digital_object, context=self.context
            )
            serializer.bind("", self)
            result.append(serializer.to_representation(digital_object))
        return result

    def update(self, instance, validated_data):  # noqa: D102
        raise NotImplementedError("update digital object list")

    def create(self, validated_data):  # noqa: D102
        raise NotImplementedError("create digital object list")


class DigitalObjectAPISerializer(serializers.ModelSerializer):  # noqa: D101
    class ID_KEY:  # noqa: D106
        in_json = "@id"
        in_obj = "identifier_link"

    @property
    def user(self):  # noqa: D102
        auth_user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            auth_user = request.user
        result = None
        if hasattr(auth_user, "heliportuser"):
            result = auth_user.heliportuser
        return result

    def validate(self, data):  # noqa: D102
        if self.instance is not None and not has_write_permission(
            self.user, self.instance
        ):
            raise ValidationError("You don't have write permission for this object.")
        return super().validate(data)

    def __init__(self, instance=None, data=empty_rest_data, **kwargs):  # noqa: D107
        if isinstance(instance, DigitalObject):
            instance = instance.casted_or_self()
        super().__init__(instance, data, **kwargs)
        self.fields[self.ID_KEY.in_json] = serializers.CharField(
            source=self.ID_KEY.in_obj, required=False
        )

        self.view = self.context.get("view")
        context = self.view.ld_context

        if isinstance(instance, DigitalObject):
            attributes = get_attributes_as(self.user, instance)
        else:
            attributes = get_static_attributes(DigitalObject)

        attr_index = defaultdict(list)
        for attr in attributes:
            key = (attr.label, attr.persistent_id)
            attr_index[key].append(attr)

        key_mapping = {}
        if data is not empty_rest_data:
            key_mapping = dict(
                reverse_context_mapping(
                    [
                        TargetDescription(label, persistent_id, label)
                        for label, persistent_id in attr_index
                    ],
                    {key: key for key in data},
                )
            )

        for (label, persistent_id), attrs in attr_index.items():
            context.tentative_set(
                self.fields,
                key_mapping.get(label, label),
                MetadataRestField(label, persistent_id, attrs, self.user),
                persistent_id,
            )

        if data is not empty_rest_data:
            data_context = data.get("@context")
            if not isinstance(data_context, dict):
                data_context = {}
            for key in data:
                key = str(key)
                if (
                    key.startswith("https://")
                    or key.startswith("http://")
                    or key in data_context
                ):
                    context.tentative_set(
                        self.fields,
                        key,
                        MetadataRestField(
                            key, data_context.get(key, key), [], self.user
                        ),
                        data_context.get(key, key),
                    )
        self.fields["New Property"] = TemplateField("core/api/new_property.html")

    def set_values(self, instance, validated_data):  # noqa: D102
        assert has_write_permission(self.user, instance), "no write permission"
        for key, field in self.fields.items():
            if key == self.ID_KEY.in_json:
                instance.persistent_id = validated_data.get(self.ID_KEY.in_obj)
            elif isinstance(field, MetadataRestField):
                field.set_values(instance, validated_data.get(key))
        instance.save()

    def update(self, instance, validated_data):  # noqa: D102
        self.set_values(instance, validated_data)
        return instance

    def create(self, validated_data):  # noqa: D102
        assert isinstance(self.user, HeliportUser)
        new_do = DigitalObject()
        new_do.save()
        new_do.co_owners.add(self.user)
        self.set_values(new_do, validated_data)
        return new_do

    def __getitem__(self, key):  # noqa: D105
        field = self.fields[key]
        value = self.data.get(key)
        error = self.errors.get(key) if hasattr(self, "_errors") else None

        if isinstance(field, MetadataRestField):
            return MetadataBoundField(field, value, error, self.instance)
        return super().__getitem__(key)

    class Meta:  # noqa: D106
        model = DigitalObject
        list_serializer_class = DigitalObjectListSerializer
        fields = [
            "digital_object_id",
        ]


class MetadataBoundField(serializers.BoundField):  # noqa: D101
    def __init__(self, field, value, error, instance):  # noqa: D107
        super().__init__(field, value, error)
        self.instance = instance

    def as_form_field(self):  # noqa: D102
        value = "" if (self.value is None or self.value is False) else self.value
        return self.__class__(self._field, value, self.errors, self.instance)

    def render_form_inputs(self):  # noqa: D102
        for attr in self.attrs:
            yield from attr.to_inputs(self.field_name, self.instance)


class TemplateField(RestField):  # noqa: D101
    def __init__(self, template):  # noqa: D107
        super().__init__(required=False, allow_null=True, write_only=True)
        self.style["template"] = template

    def to_internal_value(self, data):  # noqa: D102
        return None

    def to_representation(self, value):  # noqa: D102
        return None

    def get_attribute(self, instance):  # noqa: D102
        return None


class MetadataRestField(RestField):  # noqa: D101
    def __init__(  # noqa: D107
        self,
        label,
        persistent_id,
        attrs: typing.List[BaseAttribute],
        user: HeliportUser,
    ):
        super().__init__(
            required=False,
            allow_null=True,
            read_only=attrs and all(attr.read_only for attr in attrs),
        )
        self.key = label
        self.label = label
        self.persistent_id = persistent_id
        self.attrs = attrs
        self.user = user
        self.style["template"] = "core/form_field/metadata_rest_field.html"
        if len(self.attrs) > 0:
            self.label = self.attrs[0].label
        predicates = [
            attr.attribute
            for attr in self.attrs
            if isinstance(attr.attribute, DigitalObject)
        ]
        self.predicate_obj = min(predicates, key=lambda p: p.pk, default=None)

    @property
    def source_attrs(self):  # noqa: D102
        return [self.field_name]

    @source_attrs.setter
    def source_attrs(self, value):
        pass  # source_attrs are set to self.field_name.split(".") in bind() -> ignore it  # noqa: E501

    def get_or_generate_predicate_obj(self):  # noqa: D102
        if self.predicate_obj is None:
            self.predicate_obj = (
                DigitalObject.objects.filter(persistent_id=self.persistent_id)
                .order_by("digital_object_id")
                .first()
            )
        if self.predicate_obj is None:
            self.predicate_obj = DigitalObject(
                persistent_id=self.persistent_id, label=self.label
            )
            self.predicate_obj.save()
        return self.predicate_obj

    @property
    def writable_attrs(self):  # noqa: D102
        return (a for a in self.attrs if not a.read_only)

    @property
    def read_only_attrs(self):  # noqa: D102
        return (a for a in self.attrs if a.read_only)

    @property
    def writable_list_attrs(self):  # noqa: D102
        return (a for a in self.writable_attrs if a.handles_list)

    @property
    def writable_single_attrs(self):  # noqa: D102
        return (a for a in self.writable_attrs if not a.handles_list)

    def split_value_list(self, value_list):  # noqa: D102
        i = len(list(self.writable_single_attrs))
        return value_list[:i], value_list[i:]

    def get_value(self, dictionary):  # noqa: D102
        if html_utils.is_html_input(dictionary):
            html_result = dictionary.getlist(self.field_name, [])
            for i, v in enumerate(html_result):
                if isinstance(v, str) and v.isdecimal():
                    html_result[i] = int(v)
            return html_result
        result = super().get_value(dictionary)
        if not isinstance(result, list):
            result = [result]
        return result

    def to_internal_value(self, data):  # noqa: D102
        result = []

        single_data, list_data = self.split_value_list(data)
        for attr, d in zip_longest(self.writable_single_attrs, single_data):
            if d is empty_rest_data:
                d = None
            result.append(attr.deserialize(d))

        try:
            list_attrs = iter(self.writable_list_attrs)
            deserialized = next(list_attrs).deserialize(
                [None if d is empty_rest_data else d for d in list_data]
            )
            if isinstance(deserialized, InternalValueDescription):
                result.append(deserialized)
            else:
                result.extend(deserialized)
        except StopIteration:
            for d in list_data:
                t = DigitalObjectT() if isinstance(d, (int, dict)) else StringT()
                result.append(t.deserialize(d, {}))

        return result

    def to_representation(self, value):  # noqa: D102
        result = []
        has_list_attribute = False
        for attr in chain(
            self.writable_single_attrs,
            self.writable_list_attrs,
            self.read_only_attrs,
        ):
            serialized = attr.to_json(value)
            if attr.handles_list:
                has_list_attribute = True
                if serialized is not None:
                    result.extend(serialized)
            else:
                result.append(serialized)

        if len(result) == 1 and not has_list_attribute:
            result = result[0]
        return result

    def get_attribute(self, instance):  # noqa: D102
        return instance

    def set_values(self, instance, validated_value_list):  # noqa: D102
        assert has_write_permission(self.user, instance), "no permission"
        if validated_value_list is None:
            validated_value_list = []
        value_list = []
        for value in validated_value_list:
            if not isinstance(value, InternalValueDescription):
                value_list.append(value)
            elif value.describes_list:
                value_list.extend(value.get_value(instance))
            else:
                value_list.append(value.get_value(instance))

        single_values, list_values = self.split_value_list(value_list)

        for attr, value in zip_longest(self.writable_single_attrs, single_values):
            if attr is None:
                continue
            attr.value_to(instance, value)
            if value is None and isinstance(attr, DynamicAttribute):
                self.attrs.remove(attr)

        try:
            list_attrs = iter(self.writable_list_attrs)
            next(list_attrs).value_to(instance, list_values)
            for other_list_attr in list_attrs:
                other_list_attr.value_to(instance, None)
        except StopIteration:
            predicate = self.get_or_generate_predicate_obj()
            for value in list_values:
                if isinstance(value, DigitalObject):
                    rel = DigitalObjectRelation.objects.create(
                        subject=instance, predicate=predicate, object=value
                    )
                else:
                    rel = DigitalObjectAttributes.objects.create(
                        subject=instance, predicate=predicate, value=str(value)
                    )
                self.attrs.append(DynamicAttribute(rel))

    def render_new_input(self):  # noqa: D102
        try:
            list_attrs = iter(self.writable_list_attrs)
            return next(list_attrs).render_empty_input(self.field_name)
        except StopIteration:
            return DigitalObjectT().render_empty_input(self.field_name, {})


@static_attributes.register(DigitalObject)
def digital_object_attributes() -> typing.List[BaseAttribute]:  # noqa: D103
    vocab = Vocabulary()
    return [
        Attribute(vocab.label, StringT, "label"),
        Attribute(vocab.description, StringT, "description"),
        Attribute(
            AttrObj("Date Created", DCTERMS.created),
            DateT,
            "created",
            is_public=True,
            read_only=True,
            omit_from_external=True,
        ),
        Attribute(
            AttrObj("Date Modified", SDO.dateModified),
            DateT,
            "last_modified",
            read_only=True,
            omit_from_external=True,
        ),
        Attribute(
            AttrObj("Date Deleted", SDO.dateDeleted),
            DateT,
            "deleted",
            is_public=True,
            omit_from_external=True,
        ),
        Attribute(
            AttrObj("Owner", FRBR.owner),
            PersonT,
            "owner",
            property_category="Involved People",
            is_public_name="members_is_public",
            omit_from_external=True,
        ),
        FunctionResultAttribute(
            AttrObj("Members", DCTERMS.creator),
            ListT(PersonT),
            "members",
            property_category="Involved People",
            is_public_name="members_is_public",
            omit_from_external=True,
        ),
        Attribute(
            AttrObj("Contributors", PRO.relatesToDocument),
            ListT(ContributionT),
            "contributions",
            property_category="Involved People",
            is_public_name="members_is_public",
        ),
        ManyAttribute(
            AttrObj("is part of", DCTERMS.isPartOf),
            ListT(ProjectT),
            "projects",
            recursive=False,
            property_category="Projects",
        ),
    ]


class ImageUrlAttribute(BaseAttribute):  # noqa: D101
    def __init__(self, **kwargs):  # noqa: D107
        super().__init__(
            AttrObj("URL", FOAF.primaryTopic),
            UrlT,
            read_only=True,
            is_public_name="download_url_is_public",
            **kwargs,
        )

    def getattr(self, image):  # noqa: D102
        return f"{settings.HELIPORT_CORE_HOST}{reverse('core:image_data', kwargs={'pk': image.pk})}"  # noqa: E501


@register_digital_object_attributes(Image)
def image_attributes():  # noqa: D103
    return [
        TypeAttribute(SemanticDesktop.NFO.Image),
        Attribute(
            AttrObj("mime type", SemanticDesktop.NIE.mimeType), StringT, "mimetype"
        ),
        Attribute(
            AttrObj("Filename", SemanticDesktop.NFO.fileName), StringT, "filename"
        ),
        ImageUrlAttribute(),
    ]


@register_digital_object_attributes(Project)
def project_attributes():  # noqa: D103
    vocab = Vocabulary()
    return [
        TypeAttribute(vocab.Project),
        ManyAttribute(
            AttrObj("has Part", DCTERMS.hasPart),
            DigitalObjectT,
            "parts",
            casted=True,
            is_public_name="parts_is_public",
            property_category="Related",
        ),
    ]


def get_static_attributes(cls):  # noqa: D103
    if cls in static_attributes:
        return static_attributes[cls]()
    if issubclass(cls, DigitalObject):
        return static_attributes[DigitalObject]()
    return []


def get_dynamic_attributes(obj):  # noqa: D103
    return [
        DynamicAttribute(attr, property_category="Attributes")
        for attr in obj.attributes.order_by("pk")
    ] + [
        DynamicAttribute(rel, property_category="Relations")
        for rel in obj.relations.order_by("pk")
    ]


def get_all_attributes(obj):  # noqa: D103
    return get_static_attributes(type(obj)) + get_dynamic_attributes(obj)


def get_all_attributes_bound(obj):  # noqa: D103
    return [attr.bind_to(obj) for attr in get_all_attributes(obj)]


def get_attributes_by_category(obj, user):  # noqa: D103
    user_has_read_access = has_read_permission(user, obj)
    result = defaultdict(list)
    for attr in get_all_attributes(obj):
        if attr.exists_on(obj) and (attr.get_is_public(obj) or user_has_read_access):
            result[attr.property_category].append(attr.bind_to(obj))
    return result


def get_attributes_as(user, obj):  # noqa: D103
    read_access = has_read_permission(user, obj)
    return [
        attr
        for attr in get_all_attributes(obj)
        if read_access or attr.get_is_public(obj)
    ]


def get_bound_attributes_as(user, obj):  # noqa: D103
    return [attr.bind_to(obj) for attr in get_attributes_as(user, obj)]


def types(obj, bound_attributes=False):  # noqa: D103
    results = []
    for attribute in get_all_attributes(obj):
        if str(attribute.persistent_id) == str(RDF.type) and attribute.exists_on(obj):
            if bound_attributes:
                results.append(BoundAttribute(attribute, obj))
            else:
                results.append(attribute.value_from(obj))
    return results


class DATACITESerializer:  # noqa: D101
    @abstractmethod
    def resource_type(self, object_to_serialize):  # noqa: D102
        pass

    @abstractmethod
    def related_identifiers(self, object_to_serialize, user):  # noqa: D102
        pass

    @abstractmethod
    def as_dict(self, object_to_serialize, user):  # noqa: D102
        pass

    def as_json(self, object_to_serialize, user):  # noqa: D102
        return json.dumps(self.as_dict(object_to_serialize, user))

    def resource_type_dict(self, object_to_serialize):  # noqa: D102
        concrete_type, general_type = self.resource_type(object_to_serialize)
        assert general_type in [
            "Audiovisual",
            "Collection",
            "DataPaper",
            "Dataset",
            "Event",
            "Image",
            "InteractiveResource",
            "Model",
            "PhysicalObject",
            "Service",
            "Software",
            "Sound",
            "Text",
            "Workflow",
            "Other",
        ]

        return {"resourceType": concrete_type, "resourceTypeGeneral": general_type}


@datacite_serializers.register(DigitalObject)
class DigitalObjectDATACITESerializer(DATACITESerializer):
    """https://github.com/datacite/schema/blob/master/source/json/kernel-4.3/datacite_4.3_schema.json."""

    def resource_type(self, object_to_serialize):  # noqa: D102
        return "digital object", "Other"

    def related_identifiers(self, object_to_serialize, user):  # noqa: D102
        if (
            has_read_permission(user, object_to_serialize)
            or object_to_serialize.projects_is_public
        ):
            return [
                {
                    "relatedIdentifier": project.persistent_id,
                    "relatedIdentifierType": project.identifier_scheme.datacite_id,
                    "relationType": "IsPartOf",
                }
                for project in object_to_serialize.projects.all()
                if project.persistent_id
            ]
        return []

    def as_dict(self, digital_object, user):  # noqa: D102
        read_access = has_read_permission(user, digital_object)
        creators = []
        for creator in digital_object.members():
            creator_dict = {
                "nameType": "Personal",
                "name": creator.display_name,
            }
            if creator.orcid:
                creator_dict["nameIdentifiers"] = [
                    {
                        "nameIdentifier": creator.orcid,
                        "nameIdentifierScheme": "ORCID",
                        "schemeURI": "http://orcid.org",
                    }
                ]
            creators.append(creator_dict)

        contributors = []
        for contribution in Contribution.objects.filter(contribution_to=digital_object):
            contributor = contribution.contributor
            contribution_dict = {
                "contributionType": contribution.type,
                "name": contributor.display_name,
                "nameType": "Personal",
            }
            if contributor.affiliation:
                contribution_dict["affiliations"] = [
                    {"affiliation": contributor.affiliation}
                ]
            if contributor.orcid:
                contribution_dict["nameIdentifiers"] = [
                    {
                        "nameIdentifier": contributor.orcid,
                        "nameIdentifierScheme": "ORCID",
                        "schemeURI": "http://orcid.org",
                    }
                ]
            contributors.append(contribution_dict)

        identifier = digital_object.persistent_id
        if not identifier:
            identifier = digital_object.full_url()
        identifier_scheme = digital_object.identifier_scheme
        result = {
            "identifiers": [
                {
                    "identifier": identifier,
                    "identifierType": identifier_scheme.datacite_id,
                }
            ],
            "dates": [
                {
                    "dateType": "Created",
                    "date": digital_object.created.strftime("%Y-%m-%d"),
                }
            ],
            "types": self.resource_type_dict(digital_object),
            "schemaVersion": "http://datacite.org/schema/kernel-4",
            "publisher": "HELIPORT",
            "publicationYear": str(timezone.now().year),
        }
        if read_access or digital_object.members_is_public:
            result["creators"] = creators + contributors
        if read_access or digital_object.label_is_public:
            result["titles"] = [{"title": digital_object.label, "lang": "en"}]
        if read_access or digital_object.description_is_public:
            result["descriptions"] = [
                {
                    "description": digital_object.description,
                    "descriptionType": "Abstract",
                    "lang": "en",
                }
            ]

        related_identifiers = self.related_identifiers(digital_object, user)
        if related_identifiers:
            result["relatedIdentifiers"] = related_identifiers

        return result


@datacite_serializers.register(Project)
class ProjectDATACITESerializer(DigitalObjectDATACITESerializer):  # noqa: D101
    def related_identifiers(self, object_to_serialize, user):  # noqa: D102
        related = super().related_identifiers(object_to_serialize, user)
        if (
            has_read_permission(user, object_to_serialize)
            or object_to_serialize.parts_is_public
        ):
            for digital_object in DigitalObject.objects.filter(
                projects=object_to_serialize, persistent_id__isnull=False
            ):
                if not digital_object.persistent_id:
                    continue
                related.append(
                    {
                        "relatedIdentifier": digital_object.persistent_id,
                        "relatedIdentifierType": digital_object.identifier_scheme.datacite_id,  # noqa: E501
                        "relationType": "HasPart",
                    }
                )
        return related

    def resource_type(self, object_to_serialize):  # noqa: D102
        return "project", "Collection"


@datacite_serializers.register(Image)
class ImageDATACITESerializer(DigitalObjectDATACITESerializer):  # noqa: D101
    def resource_type(self, image):  # noqa: D102
        return image.mimetype, "Image"


def to_datacite(obj, user):  # noqa: D103
    if type(obj) in datacite_serializers:
        serializer = datacite_serializers[type(obj)]()
    elif isinstance(obj, DigitalObject):
        serializer = datacite_serializers[DigitalObject]()
    else:
        raise TypeError(f"No DATACITE serializer registered for {obj}")
    return serializer.as_dict(obj, user)


class RDFSerializer:  # noqa: D101
    @abstractmethod
    def triples(self, the_object, user) -> typing.Iterable[TripleDescription]:
        """Yield triples for the given object.

        See :mod:`heliport.core.vocabulary_descriptor` for some kinds of triple
        description you could return, or implement your own.
        """


@rdf_serializers.register(AttrObj)
class AttrObjRDFSerializer(RDFSerializer):  # noqa: D101
    def triples(self, the_object, user):  # noqa: D102
        yield from ()


@rdf_serializers.register(DigitalObject)
class DigitalObjectRDFSerializer(RDFSerializer):  # noqa: D101
    @staticmethod
    def static_triples(digital_object, user):  # noqa: D102
        for attr in get_static_attributes(type(digital_object)):
            if has_read_permission(user, digital_object) or attr.get_is_public(
                digital_object
            ):
                yield from attr.rdf_triples(digital_object)

    @staticmethod
    def dynamic_triples(digital_object, user):  # noqa: D102
        for rel in DigitalObjectRelation.objects.filter(subject=digital_object):
            if rel.is_public or has_read_permission(user, digital_object):
                yield RelationTriple(digital_object, rel.predicate, rel.object)

        for attr in DigitalObjectAttributes.objects.filter(subject=digital_object):
            if attr.is_public or has_read_permission(user, digital_object):
                yield LiteralTriple(digital_object, attr.predicate, attr.value)

    def triples(self, digital_object, user):  # noqa: D102
        yield from self.static_triples(digital_object, user)
        yield from self.dynamic_triples(digital_object, user)


@rdf_serializers.register(HeliportUser)
class UserRDFSerializer(RDFSerializer):  # noqa: D101
    def triples(self, user, user_that_wants_to_know):  # noqa: D102
        if user.display_name:
            yield LiteralTriple(user, FOAF.name, user.display_name)

        if user.auth_user is not None:
            auth_user = user.auth_user
            if auth_user.first_name:
                yield LiteralTriple(user, FOAF.firstName, auth_user.first_name)
            if auth_user.last_name:
                yield LiteralTriple(user, FOAF.lastName, auth_user.last_name)

        if user.email:
            mail_uri = f"mailto:{user.email}"
            if user.email_is_public:
                yield RelationTriple(user, FOAF.mbox, URIRef(mail_uri))
            if user.email_sha1_is_public:
                sha1sum = hashlib.sha1(mail_uri.encode())
                yield LiteralTriple(user, FOAF.mbox_sha1sum, sha1sum.hexdigest())

        if user.affiliation:
            affiliation_node = BNode()
            organization_node = BNode(
                remove_special_chars(
                    f"THE ORGANIZATION FOR AFFILIATION {user.affiliation}"
                )
            )
            yield RelationTriple(user, PRO.holdsRoleInTime, affiliation_node)
            yield RelationTriple(affiliation_node, RDF.type, PRO.RoleInTime)
            yield RelationTriple(affiliation_node, PRO.withRole, SCORO.affiliate)
            yield RelationTriple(
                affiliation_node, PRO.relatesToOrganization, organization_node
            )
            yield RelationTriple(organization_node, RDF.type, FOAF.Organization)
            yield LiteralTriple(organization_node, FOAF.name, user.affiliation)


@rdf_serializers.register(Contribution)
class ContributionRDFSerializer(RDFSerializer):  # noqa: D101
    def triples(self, contribution, user):  # noqa: D102
        t = Contribution.ContributionTypes
        yield RelationTriple(
            contribution.contribution_to,
            DCTERMS.contributor,
            contribution.contributor,
        )
        if contribution.type == t.RESEARCH_GROUP:
            yield RelationTriple(
                contribution.contributor, RDF.type, FRAPO.ResearchGroup
            )
        elif contribution.type not in self.type2role():
            yield RelationTriple(contribution.contributor, RDF.type, FOAF.Person)
        else:
            yield RelationTriple(
                contribution.contributor,
                PRO.holdsRoleInTime,
                contribution,
            )
            yield RelationTriple(contribution, RDF.type, PRO.RoleInTime)
            yield RelationTriple(
                contribution, PRO.relatesToDocument, contribution.contribution_to
            )
            yield RelationTriple(
                contribution, PRO.withRole, self.type2role()[contribution.type]
            )

    @staticmethod
    def type2role():  # noqa: D102
        t = Contribution.ContributionTypes
        # http://dx.doi.org/10.6084/m9.figshare.2075356
        return {
            t.CONTACT_PERSON: SCORO["contact-person"],
            t.DATA_COLLECTOR: SCORO["data-creator"],
            t.DATA_CURATOR: SCORO["data-curator"],
            t.DATA_MANAGER: SCORO["data-manager"],
            t.DISTRIBUTOR: PRO.distributor,
            t.EDITOR: PRO.editor,
            t.HOSTING_INSTITUTION: SCORO["host-institution"],
            t.PRODUCER: PRO.producer,
            t.PROJECT_LEADER: SCORO["project-leader"],
            t.PROJECT_MANAGER: SCORO["project-manager"],
            t.PROJECT_MEMBER: SCORO["project-member"],
            t.REGISTRATION_AGENCY: SCORO["registrar"],
            t.REGISTRATION_AUTHORITY: SCORO["registration-authority"],
            t.RESEARCHER: SCORO["researcher"],
            t.RIGHTS_HOLDER: SCORO["rights-holder"],
            t.SPONSOR: SCORO["sponsor"],
            t.SUPERVISOR: SCORO["supervisor"],
            t.WORK_PACKAGE_LEADER: SCORO["workpackage-leader"],
        }


def to_rdf(digital_object, user, max_depth=100):
    """Serialize any object as rdflib graph.

    The type of object needs to be registered at :attr:`rdf_serializers` to be
    serialized.
    """
    serializer_dict = {k: serializer() for k, serializer in rdf_serializers.items()}
    graph = Graph()
    for prefix, namespace in custom_namespaces:
        graph.bind(prefix, namespace)
    done = set()
    todo = [(digital_object, 0)]
    while todo:
        current, depth = todo.pop()
        if current in done or depth > max_depth:
            continue

        if type(current) in serializer_dict:
            serializer = serializer_dict[type(current)]
        elif isinstance(current, DigitalObject):
            serializer = serializer_dict[DigitalObject]
        else:
            raise TypeError(f"No RDFSerializer registered for {current}")
        for triple in serializer.triples(current, user):
            if triple.recursive:
                todo.extend((o, depth + 1) for o in triple.objects())
            triple.add_to_graph(graph)

        done.add(current)

    return graph


@renderers.register("datacite_json")
class DataCiteJSONRenderer:  # noqa: D101
    name = "Datacite JSON"

    @staticmethod
    def render(obj, user):  # noqa: D102
        return JsonResponse(to_datacite(obj, user), json_dumps_params={"indent": 2})


@renderers.register("datacite")
class DataCiteRenderer:  # noqa: D101
    name = "Datacite XML"

    @staticmethod
    def render(obj, user):  # noqa: D102
        json_datacite = to_datacite(obj, user)
        xml_datacite = schema43.tostring(json_datacite)
        return HttpResponse(
            xml_datacite, content_type="application/vnd.datacite.datacite+xml"
        )


@renderers.register("jsonld")
@renderers.register("json")
class JsonLdRenderer:  # noqa: D101
    name = "JSON-LD"

    @staticmethod
    def len_sum(d: dict):  # noqa: D102
        return sum(
            len(potential_list) if isinstance(potential_list, list) else 1
            for potential_list in d.values()
        )

    @classmethod
    def find_id_usages(cls, item, parent, id_usages):  # noqa: D102
        recursion = None
        if isinstance(item, dict):
            if "@id" in item:
                id_usages[item["@id"]].append((parent, item))
            recursion = item.values()
        if isinstance(item, list):
            recursion = item

        if recursion is not None:
            for i in recursion:
                cls.find_id_usages(i, parent, id_usages)

    @classmethod
    def inline(cls, graph):  # noqa: D102
        if not isinstance(graph, list):
            return graph
        if not all(isinstance(d, dict) and d.get("@id") is not None for d in graph):
            return graph
        id_usages = defaultdict(list)
        for d in graph:
            for i in d.values():
                cls.find_id_usages(i, d["@id"], id_usages)
        result = []
        parent_relation = {}
        for d in sorted(graph, key=cls.len_sum):
            d_id = d.get("@id")
            usages = id_usages[d_id]
            if "@id" in d and len(usages) == 1:
                parent, usage = usages[0]
                while parent in parent_relation:
                    parent = parent_relation[parent]
                if d_id != parent:
                    assert usage == {"@id": d_id}, "unexpected situation"
                    for k, v in d.items():
                        usage[k] = v
                    parent_relation[d_id] = parent
                else:
                    result.append(d)
            else:
                result.append(d)

        return result

    @staticmethod
    def filter_prefixes(graph, prefixes):  # noqa: D102
        min_length = min(len(p) for p in prefixes.values())
        index = defaultdict(list)
        for key, prefix in prefixes.items():
            index[prefix[:min_length]].append((key, prefix))
        prefix_index = dict(index)
        confirmed_prefixes = {}

        for triple in graph:
            for node in triple:
                if isinstance(node, Literal) and isinstance(node.datatype, str):
                    node = node.datatype
                prefix_list = prefix_index.get(node[:min_length], [])
                for key, prefix in list(prefix_list):
                    if node.startswith(prefix):
                        confirmed_prefixes[key] = prefix
                        prefix_list.remove((key, prefix))
        return confirmed_prefixes

    @classmethod
    def render(cls, obj, user):  # noqa: D102
        graph = to_rdf(obj, user)
        result_json = graph.serialize(format="json-ld", indent=2, auto_compact=True)

        d = json.loads(result_json)
        if set(d) == {"@context", "@graph"}:
            d["@graph"] = cls.inline(d["@graph"])
            if isinstance(d["@context"], dict):
                d["@context"] = cls.filter_prefixes(graph, d["@context"])
        result_json = json.dumps(d, indent=2)

        return HttpResponse(
            result_json,
            content_type="application/ld+json",
        )


@renderers.register("turtle")
class TurtleRenderer:  # noqa: D101
    name = "Turtle"

    @staticmethod
    def render(obj, user):  # noqa: D102
        graph = to_rdf(obj, user)
        return HttpResponse(graph.serialize(), content_type="text/turtle")


@renderers.register("xml")
class RdfXmlRenderer:  # noqa: D101
    name = "RDF XML"

    @staticmethod
    def render(obj, user):  # noqa: D102
        is_landing_page = re.compile("(.*)/object/([0-9]+)/?")

        def clean(node):
            match = is_landing_page.match(node)
            if match is not None:
                return URIRef(f"{match.group(1)}/object/H{match.group(2)}")
            if isinstance(node, URIRef) and node.endswith("/"):
                return URIRef(node[:-1])
            return node

        graph = to_rdf(obj, user)
        clean_graph = Graph()
        for s, p, o in graph:
            clean_graph.add((clean(s), clean(p), clean(o)))
        return HttpResponse(
            clean_graph.serialize(format="pretty-xml"),
            content_type="application/rdf+xml",
        )


@renderers.register("nt")
class TripleRenderer:  # noqa: D101
    name = "N-Triples"

    @staticmethod
    def render(obj, user):  # noqa: D102
        graph = to_rdf(obj, user)
        return HttpResponse(
            graph.serialize(format="nt"), content_type="application/n-triples"
        )


@renderers.register("landing_page")
class LandingPageRenderer:  # noqa: D101
    name = "Default Landing Page"

    @classmethod
    def render(cls, obj, user):  # noqa: D102
        raise AssertionError("Please handle this renderer explicitly in your view")


@renderers.register("html")
class HtmlRenderer:  # noqa: D101
    name = "Specific Landing Page"

    @classmethod
    def render(cls, obj, user):  # noqa: D102
        raise AssertionError("Please handle this renderer explicitly in your view")


def get_unique_renderers():  # noqa: D103
    # note that, if multiple renderers are equal, this selects the format
    # that was registered last (the topmost decorator)
    unique_renderers = {renderer: format for format, renderer in renderers.items()}
    return {format: renderer for renderer, format in unique_renderers.items()}


def select_renderer(format_code, accept_header):  # noqa: D103
    if not format_code:
        format_dict = {
            "application/ld+json": "jsonld",
            "application/vnd.datacite.datacite+json": "datacite_json",
            "application/vnd.datacite.datacite+xml": "datacite",
            "application/rdf+xml": "xml",
            "application/n-triples": "nt",
            "text/turtle": "turtle",
            "text/html": "landing_page",
            "application/xml": "xml",
            "text/xml": "xml",
            "application/json": "json",
            "text/plain": "turtle",
        }
        if accept_header is None:
            accept_header = "*/*"
        mime_type = get_best_match(accept_header, format_dict)
        try:
            format_code = format_dict[mime_type]
        except KeyError:
            return None, f"supported mime types are: {list(format_dict)}"

    try:
        result = renderers[format_code]
    except KeyError:
        return None, f"supported formats are: {list(renderers)}"
    return result, None
