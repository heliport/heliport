# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later
import typing

from django.db.models import Q
from rest_framework import permissions

from .models import DigitalObject, HeliportUser, Project
from .utils.context import Context
from .utils.exceptions import AccessDenied


class HeliportProjectPermission(permissions.BasePermission):
    """Permission class to be used in django rest framework views."""

    def has_object_permission(self, request, view, obj):
        """Permission is granted if user is member of the project."""
        if not hasattr(request.user, "heliportuser"):
            return False
        return request.user.heliportuser in obj.members()


class HeliportObjectPermission(permissions.BasePermission):
    """Permission class to be used in django rest framework views."""

    def has_object_permission(self, request, view, obj):
        """Permission is granted if user :func:`has_permission_for` the digital object."""  # noqa: E501
        if not hasattr(request.user, "heliportuser"):
            return False
        user = request.user.heliportuser
        return has_permission_for(user, obj)


class StaffOrReadOnly(permissions.BasePermission):
    """Permission class to be used in django rest framework views."""

    def has_object_permission(self, request, view, obj):
        """Only admins can edit; everybody can read."""
        return request.user.is_staff or request.method in permissions.SAFE_METHODS


def has_permission_for(user: HeliportUser, obj: DigitalObject):
    """
    Check if a ``user`` is allowed to access non-public attributes of ``obj``.

    Currently, this is implemented by checking if the user is a member of a project that
    contains ``obj``. This might change in the future.

    See also :func:`object_of`.
    """
    if user is None:
        return False
    for project in obj.projects.all():
        if user in project.members():
            return True
    return user in obj.members()


def projects_of(heliport_user):
    """QuerySet of projects where user is a member."""
    return direct_objects_of(heliport_user, Project)


def direct_objects_of(heliport_user, obj_type=DigitalObject):
    """
    QuerySet of :class:`heliport.core.models.DigitalObject` instances of type
    ``obj_type`` where user is a member.
    """  # noqa: D205
    return obj_type.objects.filter(
        Q(owner=heliport_user) | Q(co_owners=heliport_user)
    ).distinct()


def objects_of(heliport_user, obj_type=DigitalObject):
    """
    QuerySet of objects where ``heliport_user`` has access to. Currently, a user
    :func:`has_permission_for` an object if the object is in a project where the user
    is member of. This might change in the future.
    """  # noqa: D205
    return obj_type.objects.filter(is_object_member(heliport_user))


def allowed_objects(heliport_user, obj_type=DigitalObject):
    """
    QuerySet of all objects a user is allowed to access. This is the same as
    :func:`objects_of` but also includes all public projects.
    """  # noqa: D205
    return obj_type.objects.filter(is_object_member(heliport_user) | is_public())


def is_object_member(heliport_user):
    """
    Query that can be used to construct filters on objects.
    This filters the same objects as :func:`objects_of`.
    """  # noqa: D205
    return Q(projects__owner=heliport_user) | Q(projects__co_owners=heliport_user)


def is_public():
    """
    Query that can be used to construct filters on objects.
    This filters objects that are public and is used in :func:`allowed_objects`.
    Currently, a ``DigitalObject`` is public if it has no owner. Because most objects
    have no owner, most objects are public. This is subject to change.
    """  # noqa: D205
    return Q(owner__isnull=True)


def has_read_permission(user: typing.Optional[HeliportUser], obj: DigitalObject):
    """
    Check if ``user`` has read permission to non-public attributes of ``obj``.
    This is currently the same as :func:`has_permission_for` but might change in the
    future.
    """  # noqa: D205
    return has_permission_for(user, obj)


def has_write_permission(user: typing.Optional[HeliportUser], obj: DigitalObject):
    """
    Check if ``user`` has write permission for ``obj``.
    This is currently the same as :func:`has_permission_for`.
    """  # noqa: D205
    # it is debatable if this is even needed. Maybe we should only distinguish between
    # has_read_permission and has_permission_for
    return has_permission_for(user, obj)


class PermissionChecker:
    """
    Class that checks permission given an object
    and a context (:class:`heliport.core.utils.context.Context`, which may provide a
    current user and current project).
    This is used for return value of
    :meth:`heliport.core.digital_object_interface.GeneralDigitalObject.access`.
    It might be necessary to implement a subclass that overwrites some methods if you
    want to use permission with objects that are not
    :class:`heliport.core.models.DigitalObject` instances.
    """  # noqa: D205

    #: Context in which to check permission. Contains e.g. current user.
    context: Context

    #: The object where permission should be checked for.
    obj: typing.Optional[DigitalObject]

    def __init__(  # noqa: D107
        self, obj: typing.Optional[DigitalObject], context: Context
    ):
        """Initialize."""
        self.obj = obj
        self.context = context

    def check_read_permission(self) -> bool:
        """
        Check if current user (from :attr:`context`) has permission to read this
        digital object.
        """  # noqa: D205
        if self.obj is not None:
            return has_read_permission(self.context.user_or_none, self.obj)
        return False

    def check_delete_permission(self) -> bool:
        """Check if current user has permission to delete this object.

        The user is taken from :attr:`context`.

        This defaults to :meth:`check_permission`.
        """
        return self.check_permission()

    def check_permission(self) -> bool:
        """
        Check if current user (from :attr:`context`) has read and write permissions for
        this digital object.
        """  # noqa: D205
        if self.obj is not None:
            return has_permission_for(self.context.user_or_none, self.obj)
        return False

    def assert_permission(self):
        """Raises :class:`heliport.core.utils.exceptions.AccessDenied` if no permission."""  # noqa: D401, E501
        if not self.check_permission():
            raise AccessDenied()

    def assert_read_permission(self):
        """Raises :class:`heliport.core.utils.exceptions.AccessDenied` if no permission."""  # noqa: D401, E501
        if not self.check_read_permission():
            raise AccessDenied("You are not allowed to read this")

    def assert_delete_permission(self):
        """Raise error if no delete permission.

        :class:`heliport.core.utils.exceptions.AccessDenied` is Raised.
        """
        if not self.check_delete_permission():
            raise AccessDenied("You are not allowed to delete this")


class LoginPermissionChecker(PermissionChecker):
    """A :class:`PermissionChecker` that also checks logins.

    Checks read permission like the base class but only allows to write if additionally
    the current user owns the login.

    The model field that stores the :class:`heliport.core.models.LoginInfo` is by
    default called ``login``. You may change this and also check multiple logins by
    supplying a tuple of strings to the ``login_fields`` parameter of the constructor.

    If the current object has the login ``None``, no additional constraints are checked.
    """

    def __init__(self, obj, context, login_fields=("login",)):
        """Initialize."""
        super().__init__(obj, context)
        self.login_fields = login_fields

    def check_permission(self):
        """Check read and write permission.

        Same as base class, but with additional check for logins.
        """
        if not super().check_permission():
            return False

        for field in self.login_fields:
            login = getattr(self.obj, field)
            if login is not None and login.user != self.context.user_or_none:
                return False

        return True

    def check_delete_permission(self) -> bool:
        """Check delete permission.

        Delete permission is not affected by whether the user owns the login.
        """
        if self.obj is not None:
            return has_permission_for(self.context.user_or_none, self.obj)
        return False


class OnlyOwnerCanDeleteChecker(PermissionChecker):
    """Permission checker that allows only the owner to delete an object.

    Note that objects without owner can be deleted by people that have regular write
    permission.
    """

    def check_delete_permission(self) -> bool:
        """Check delete permission by comparing current user to owner."""
        if self.obj is None:
            return False
        if not self.obj.owner:
            return self.check_permission()
        return self.obj.owner == self.context.user_or_none
