# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains :class:`django.db.models.Model` classes for Django ORM.

See :ref:`django:topics/db/models:quick example` from Django documentation.

This module provides classes like :class:`DigitalObject`, that can be subclassed in
HELIPORT apps.
"""

import logging
import re
import typing
from base64 import b64decode
from collections import namedtuple
from mimetypes import guess_type
from pathlib import Path
from typing import Dict, Optional, Union

from django.conf import settings
from django.contrib.auth.models import Group, User
from django.db import models
from django.db.models import Case, Q, When
from django.template import Context as TemplateContext
from django.template import Template
from django.urls import reverse
from django.utils import timezone
from knox import models as knox_models
from rdflib import BNode, URIRef

from heliport.core.forms import validate_orcid
from heliport.core.utils.collections import RootedRDFGraph
from heliport.core.utils.colors import pick_foreground_color
from heliport.core.utils.command_line_execution import create_command_executor
from heliport.core.utils.IDs import (
    IdentifierScheme,
    default_identifier_scheme,
    find_identifier_scheme,
    generate_object_id,
    normalize_datacite_resource_identifier_scheme,
    remove_special_chars,
    test_resolvability,
)
from heliport.core.utils.url import url_encode

from .digital_object_interface import GeneralDigitalObject
from .utils import encryption
from .utils.context import Context

logger = logging.getLogger(__name__)


class HeliportGroup(models.Model):
    """HELIPORT specific group information.

    The actual group from authentication is available as `auth_group`.

    The user model is extended in a similar way in the :class:`HeliportUser` model.
    """

    _DEFAULT_GROUP_NAME = "default_group"

    group_id = models.AutoField(primary_key=True)
    backend_id = models.CharField(max_length=100, null=True)  # noqa: DJ001
    auth_group = models.OneToOneField(
        Group, on_delete=models.SET_NULL, null=True, blank=True
    )
    display_name = models.CharField(max_length=100, blank=True)

    def __str__(self):
        """Represent group."""
        if self.display_name:
            assert isinstance(self.display_name, str), f"invalid: {self.display_name}"
            return self.display_name
        if self.backend_id:
            return f"<HeliportGroup {self.backend_id}>"
        return f"<HeliportGroup {self.group_id}>"

    @classmethod
    def get_default_group(cls):
        """Create a default group if it doesn't exist."""
        group, is_new = cls.objects.get_or_create(display_name=cls._DEFAULT_GROUP_NAME)
        return group


class HeliportUser(models.Model):
    """HELIPORT specific user information.

    The actual user from authentication is available as `auth_user`.

    The group model is extended in a similar way in the :class:`HeliportGroup` model.
    """

    user_id = models.AutoField(primary_key=True)
    authentication_backend_id = models.CharField(  # noqa: DJ001
        max_length=100, null=True
    )
    auth_user = models.OneToOneField(
        User, on_delete=models.SET_NULL, null=True, blank=True
    )
    display_name = models.CharField(max_length=100, default="<User>")
    affiliation = models.CharField(max_length=500, blank=True)
    orcid = models.CharField(  # noqa: DJ001
        max_length=100, null=True, blank=True, validators=[validate_orcid]
    )
    #: Email to use when no auth_user exists
    stored_email = models.EmailField(blank=True)
    email_is_public = models.BooleanField(default=False)
    #: Whether to show sha1 of email in public foaf metadata.
    #: Note that email hash can be reversed especially if you have the users name.
    email_sha1_is_public = models.BooleanField(default=False)
    uuid = models.UUIDField(
        primary_key=False, default=generate_object_id, editable=False
    )

    def __str__(self):
        """Represent by display name."""
        return str(self.display_name)

    @property
    def email(self) -> Optional[str]:
        """The user's email address.

        Taken from authentication backend with fall back to :attr:`stored_email`.
        """
        if self.auth_user:
            return self.auth_user.email
        if self.stored_email:
            return self.stored_email
        return None

    @property
    def public_email(self) -> Optional[str]:
        """The users :attr:`email` or ``None`` if it is not public."""
        if not self.email_is_public:
            return None
        return self.email

    def to_rdflib_node(self):
        """Represent user by orcid or uuid.

        This method is needed when describing a graph in
        :meth:`heliport.core.attribute_description.AttributeType.rdf_triples`. See
        :func:`heliport.core.vocabulary_descriptor.TripleDescription.to_node`.
        """
        if self.orcid:
            return URIRef(url_encode(self.orcid))
        return BNode(remove_special_chars(self.uuid))

    def set_orcid(self, orcid):
        """Validate and set ORCID.

        If the given ORCID is not valid, a ValidationError is raised.

        This function is kept for backwards compatibility.
        """
        if orcid:
            validate_orcid(orcid)
            self.orcid = orcid
        else:
            self.orcid = None


class DigitalObject(models.Model, GeneralDigitalObject):
    """Representing an arbitrary thing that can have metadata.

    This is a core model for HELIPORT as basically everything that is added to HELIPORT
    is a ``DigitalObject``. The main purpose of many apps is to subclass the
    ``DigitalObject`` model and implement respective functionality.
    """

    class ObjectPermissions(models.IntegerChoices):
        """Defines levels of permissions.

        Note that these are not enforced at the moment.
        """

        #: Everybody can see and edit
        PUBLIC = 1
        #: Private + Members of a project that this object is in can see and edit
        PROJECT = 2
        #: only members of this Digital Object can see and edit
        PRIVATE = 3

    digital_object_id = models.AutoField(primary_key=True)
    #: URI that identifies this digital object e.g. the Handle
    persistent_id = models.CharField(  # noqa: DJ001
        max_length=500, null=True, blank=True
    )
    generated_persistent_id = models.CharField(  # noqa: DJ001
        max_length=500, null=True, blank=True
    )
    category = models.TextField()
    label = models.TextField()
    label_is_public = models.BooleanField(default=False)
    description = models.TextField(blank=True, default="")
    description_is_public = models.BooleanField(default=False)
    created = models.DateTimeField("date created", auto_now_add=True)
    last_modified = models.DateTimeField("date last modified", auto_now=True)
    last_modified_is_public = models.BooleanField(default=False)
    deleted = models.DateTimeField("date deleted", null=True, blank=True)
    permission = models.IntegerField(
        choices=ObjectPermissions.choices, default=ObjectPermissions.PROJECT
    )
    special_heliport_role = models.CharField(
        max_length=60, blank=True, null=True, unique=True
    )
    is_helper = models.BooleanField(default=False)

    projects = models.ManyToManyField("Project", blank=True, related_name="parts")
    projects_is_public = models.BooleanField(default=False)
    co_owners = models.ManyToManyField(
        HeliportUser, related_name="co_owned_digital_objects", blank=True
    )
    owner = models.ForeignKey(HeliportUser, on_delete=models.SET_NULL, null=True)
    members_is_public = models.BooleanField(default=False)

    def __str__(self):
        """Represent a digital object by its label."""
        return self.label or str(self.digital_object_id)

    def __init__(self, *args, **kwargs):
        """Set current context to ``None``."""
        super().__init__(*args, **kwargs)
        self._context = None

    @staticmethod
    def type_id() -> str:
        """For resolution."""
        return "DigitalObject"

    def get_identifying_params(self) -> Dict[str, str]:
        """For resolution."""
        return {"digital_object_id": str(self.digital_object_id)}

    @staticmethod
    def resolve(
        params: Dict[str, str], context: Context
    ) -> Optional[GeneralDigitalObject]:
        """For resolution."""
        obj = DigitalObject.objects.filter(
            digital_object_id=params.get("digital_object_id")
        ).first()
        if obj is not None:
            obj = obj.casted_or_self()
        return obj

    def preferred_identifier(self) -> Union["DigitalObjectIdentifier", str, None]:
        """Return the preferred identifier for the digital object.

        Identifiers are ordered by their ``immutable`` field first, then by scheme.
        "Immutable" identifiers are preferred over non-"immutable" ones. Scheme
        preference is given in ``preference``. If multiple identifiers of the same
        "immutability" and scheme are preferred, the first one to be found is returned.
        If no identifier with a scheme from the preferences list is found, the first
        identifer with a different scheme is returned, again ordered by "immutability".
        """
        identifiers = DigitalObjectIdentifier.objects.filter(digital_object=self)

        preference = [
            "doi",
            "hdl",
            "url",
            "heliport",
            normalize_datacite_resource_identifier_scheme(
                default_identifier_scheme.datacite_id
            ),
        ]
        if (
            identifier := identifiers.filter(scheme__in=preference)
            .annotate(
                preference_index=Case(
                    *[
                        When(scheme=value, then=index)
                        for index, value in enumerate(preference)
                    ],
                    output_field=models.IntegerField(),
                )
            )
            .order_by("-immutable", "preference_index")  # immutable==True first
            .first()
        ) is not None:
            return identifier

        if (identifier := identifiers.order_by("-immutable").first()) is not None:
            return identifier

        # Older digital objects will not yet have identifiers in
        # DigitalObjectIdentifiers. Thus, we need to fall back to their own attributes.
        return self.persistent_id or self.generated_persistent_id

    def as_digital_object(self, context: Context):
        """Save this object and cast to subclass if necessary.

        For serialization as a
        :class:`heliport.core.digital_object_interface.GeneralDigitalObject`.
        """
        if self.digital_object_id is None:
            self.save()
        correctly_casted = self.casted_or_self()
        correctly_casted.set_context(context)
        return correctly_casted

    def as_digital_object_if_exists(self, context):
        """Return ``self`` because this is already a digital object that exists.

        For serialization as a
        :class:`heliport.core.digital_object_resolution.Resolvable`.
        """
        return self

    def as_text(self) -> str:
        """Get a string representation of this object.

        For serialization as a
        :class:`heliport.core.utils.serialization.GeneralValue`.
        """
        if self.label:
            return self.label
        if self.description:
            return self.description
        if self.persistent_id:
            return self.persistent_id
        return "digital object"

    def as_rdf(self) -> RootedRDFGraph:
        """Get rdf graph of this object.

        This currently returns a blank node if no ``persistent_id`` is set.

        For serialization as a
        :class:`heliport.core.utils.serialization.GeneralValue`.
        """
        # Maybe also use :meth:`full_url` like in :meth:`to_rdflib_node`?
        if self.persistent_id:
            return RootedRDFGraph.from_uri_str(self.persistent_id)
        return RootedRDFGraph.from_blank()

    def as_html(self) -> str:
        """Get html representing this object to be rendered into some template.

        For serialization as a :class:`heliport.core.utils.serialization.GeneralValue`.
        """
        t = Template('<a href="{{ obj.identifier_link }}">{{ obj.as_text }}</a>')
        c = TemplateContext({"obj": self})
        return t.render(c)

    @property
    def viewing_user(self) -> typing.Optional[HeliportUser]:
        """The user that currently deals with this digital object.

        Information may be restricted to what the user can see. None means to only
        include public infos.
        """
        if self.context is not None:
            return self.context.user_or_none
        return None

    def set_context(self, context: Context):
        """Set context later accessible as :attr:`context`.

        Set an :class:`heliport.core.utils.context.Context` to be used by operations
        of this ``DigitalObject``. By setting the context you guarantee that,
        and you are responsible for, closing the context after all operations are done.
        It is not allowed to change the user or downgrade the permission back to public
        again. Create a new digital object instance for that.
        """
        assert self.viewing_user is None or self.viewing_user == context.user_or_none, (
            "It is not save to change the user like this, "
            "because some code might not recheck permissions. "
            "Please create a new instance for each user including the public user None."
        )
        self._context = context

    def update_modified_date(self):
        """Set last_modified to now."""
        self.save()

    @property
    def context(self) -> typing.Optional[Context]:
        """Get context previously set using :meth:`set_context`.

        If this function does not return ``None`` the return value can be used for
        optimizations where expensive setup and teardown steps can be shared between
        operations.
        See :class:`heliport.core.utils.collections.ExitContext` for more details.
        """
        return self._context

    def access(self, context: Context):
        """Get permission checker for this object and context.

        For example::

            obj.access(context).assert_read_permission()  # raise error otherwise
            if obj.access(context).check_permission():
                obj.label = None  # do some update

        See also :class:`heliport.core.permissions.PermissionChecker`.

        For serialization as a
        :class:`heliport.core.digital_object_interface.GeneralDigitalObject`.
        """
        from .permissions import PermissionChecker

        return PermissionChecker(self, context)

    @property
    def represents_external_identifier(self):
        """Whether this object stands for itself or is an external resource.

        An external resource is something that has an identifier that does not resolve
        to the HELIPORT landing page of this object. For example a publication may be
        identified in HELIPORT by a DOI that resolves to a publication repository, or a
        term in an ontology may be identified by a URI that resolves to its definition.
        """
        if not self.persistent_id:
            return False

        # Handle special case when identifier is set to just the suffix.
        # NOTE: This used to be the case when no Handle.Net server was set up for PID
        # registration. Now, this field is left blank.
        if self.persistent_id == self.suffix_for_pid_generation:
            return False

        return self.persistent_id != self.generated_persistent_id

    @property
    def category_list(self):
        """Get the category path as list of digital objects.

        The category is internally stored as string of ids separated by "/".

        Note that the name "category" is deprecated in favour of "namespace".
        """
        pk_list = self.category.split("/")
        return [
            DigitalObject.objects.filter(pk=pk).first() if pk.isdecimal() else None
            for pk in pk_list
        ]

    @category_list.setter
    def category_list(self, values):
        """Set the category path from list of digital objects.

        The category is internally stored as string of ids separated by "/".

        Note that the name "category" is deprecated in favour of "namespace".
        """
        self.category = "/".join(str(c.pk) for c in values)

    @property
    def last_category(self):
        """The last category of the digital object.

        The last category itself is also a digital object.
        """
        cl = self.category_list
        if not cl or not cl[-1]:
            return None
        return cl[-1]

    @property
    def category_str(self):
        """The category of the digital object formatted for the user.

        Each category is represented by its label. The labels are separated by "/".

        This is deprecated, use :attr:`namespace_str` instead.
        """
        return "/".join(
            c.label if c is not None else "None" for c in self.category_list
        )

    @category_str.setter
    def category_str(self, value):
        """Set the category of a digital object from a string of category labels.

        Category labels are separated by "/".

        This was required to migrate from the old implementation that stored the
        category as string of labels but turned also out to be useful when setting a
        category that comes from a setting that is a string.

        Note that the name "category" is deprecated in favour of "namespace" because
        the category is part of the PID generation and a namespace conveys that better
        and a category is basically a type and types can in principle change but the PID
        not. The concept of a type exists separately anyway as a normal relation.
        """
        vocab = Vocabulary()

        namespace_list = value.split("/")
        namespace_category_str = f"HELIPORT/{vocab.Namespace.label}"
        last_namespace = None
        category_list = []
        for i, namespace_label in enumerate(namespace_list):
            if last_namespace is None:
                this_namespace = get_or_create_with_relation(
                    namespace_label,
                    namespace_category_str,
                    vocab.type,
                    vocab.Namespace,
                )
            else:
                this_namespace = get_or_create_with_relation(
                    namespace_label,
                    namespace_category_str,
                    vocab.subnamespace_of,
                    last_namespace,
                )
            assert_relation(this_namespace, vocab.type, vocab.Namespace)

            is_type = i == len(namespace_list) - 1
            if is_type:
                assert_relation(this_namespace, vocab.type, vocab.Class)

            category_list.append(this_namespace)
            last_namespace = this_namespace
        self.category_list = category_list

    @property
    def namespace_str(self):
        """A more appropriately named alias for :attr:`category_str`.

        Category labels are separated by "/".

        Note that the name "category" is deprecated in favour of "namespace" because
        the category is part of the PID generation and a namespace conveys that better
        and a category is basically a type and types can in principle change but the PID
        not also there can be multiple types.
        The concept of a type exists separately in HELIPORT as a normal relation.
        """
        return self.category_str

    @namespace_str.setter
    def namespace_str(self, value: str):
        """Setter."""
        self.category_str = value

    @property
    def category_in_persistent_id(self):
        """Determine if the namespace a.k.a. category is part of the PID."""
        pattern = ".*".join(c.label.lower() for c in self.category_list)
        return bool(re.search(pattern, str(self.persistent_id).lower()))

    def test_category_migrated(self):
        """Check whether the is migrated from the old format of labels instead of IDs.

        I think this function can be deleted by now.
        """
        pk_list = self.category.split("/")
        return all(str(pk).isdecimal() for pk in pk_list)

    @property
    def relative_url(self):
        """Get landing page url for this ``DigitalObject`` at HELIPORT instance."""
        return reverse("core:landing_page", kwargs={"pk": self.digital_object_id})

    def local_url(self, request):
        """Get absolute landing page url as the user see it.

        For example the user might access HELIPORT through a proxy.
        """
        return f"{request.scheme}://{request.get_host()}{self.relative_url}"

    def full_url(self):
        """Get absolute landing page url as configured in settings.

        This url may identify the ``DigitalObject`` if no other PID exists.
        This is also the url a PID may redirect to during PID resolution.
        """
        return f"{settings.HELIPORT_CORE_HOST}{self.relative_url}"

    @property
    def identifier_link(self):
        """The PID of digital object.

        It is made sure that this works as URL. Fallback is :func:`full_url`.
        """
        if self.persistent_id and "://" in self.persistent_id:
            return self.persistent_id
        return self.full_url()

    @property
    def identifier_scheme(self):
        """Scheme of the PID of this object.

        The scheme has a URL prefix an id for use in datacite and a scheme label.
        See :mod:`heliport.core.utils.IDs` for more information.
        """
        if self.identifier_link == self.full_url():
            return IdentifierScheme("", "url", "HELIPORT URL")
        return find_identifier_scheme(self.identifier_link)

    @property
    def identifier_label(self):
        """Human-readable label of the identifier scheme of the PID of this object."""
        return self.identifier_scheme.label

    @property
    def identifier_display(self):
        """Shorter version of the PID for nicer display.

        It is common to use this when in html link tags where the actual link refers to
        :attr:`identifier_link`.
        """
        prefix = self.identifier_scheme.prefix
        result = self.identifier_link
        if result.startswith(prefix):
            result = result[len(prefix) :]
        return result

    def set_identifier(self, new_identifier: Optional[str]):
        """Set the PID but normalize it first.

        The given identifier will be set on the digital object directly. Additionally,
        a :class:`DigitalObjectIdentifier` will be created that links the identifier to
        the digital object.

        In some cases this function may make network requests (HEAD request) to
        determine if a variation of the identifier is actually valid.
        Therefore, this function should ideally be called in a celery job.
        """
        if new_identifier is None:
            self.persistent_id = None
            return

        identifier_scheme = find_identifier_scheme(new_identifier)
        canonical_identifier_scheme = normalize_datacite_resource_identifier_scheme(
            identifier_scheme.datacite_id
        )

        scheme = canonical_identifier_scheme
        url = ""
        identifier = ""
        display_text = ""

        if identifier_scheme == default_identifier_scheme:
            if test_resolvability(f"https://api.datacite.org/dois/{new_identifier}"):
                scheme = "doi"
                identifier = new_identifier
                display_text = identifier
                url = f"https://doi.org/{new_identifier}"

            elif test_resolvability(f"https://hdl.handle.net/{new_identifier}"):
                scheme = "hdl"
                identifier = new_identifier
                display_text = identifier
                url = f"https://hdl.handle.net/{new_identifier}"

            elif test_resolvability(f"https://n2t.net/{new_identifier}"):
                # By doing a GET request rather than HEAD, we could actually find out
                # things (scheme) about this identifier.
                scheme = "url"
                identifier = new_identifier
                url = f"https://n2t.net/{new_identifier}"

        elif canonical_identifier_scheme == "url":
            identifier = new_identifier
            url = new_identifier

        else:
            identifier = new_identifier.removeprefix(identifier_scheme.prefix)
            display_text = identifier
            url = new_identifier

        self.persistent_id = url or identifier or new_identifier

        DigitalObjectIdentifier.objects.create(
            scheme=scheme,
            identifier=identifier,
            display_text=display_text,
            url=url,
            digital_object=self,
            immutable=True,  # We don't want HELIPORT to attempt to update this!
        )

    @property
    def suffix_for_pid_generation(self):
        """A string to use as a suffix in PID generation.

        This string can be used by PID registration code as a baseline for this digital
        object's PID. Essentially, this is the object's own suggestion for what it would
        like to be called. The caller may deviate from this for any and all reasons.
        """
        category_list = self.category_str.split("/")
        return f"{'.'.join(category_list)}.{self.pk}"

    def members(self):
        """All people where this ``DigitalObject`` belongs to.

        This information is frequently used for access control.
        """
        result = set(self.co_owners.all())
        if self.owner:
            result.add(self.owner)
        return result

    @property
    def contributions(self):
        """All known :class:`Contribution` instances to this object.

        This is also settable.
        """
        return self.contribution_set.all()

    @contributions.setter
    def contributions(self, contribution_values):
        """Setter."""
        if contribution_values is None:
            contribution_values = []
        current = set(self.contributions)
        target = set(contribution_values)
        target.discard(None)
        for old in current - target:
            old.delete()
        for new in target - current:
            new.contribution_to = self
            new.save()

    @staticmethod
    def not_subclass_query():
        """Django ORM query parameters to test if an object is not a subclass.

        This functionality could be much improved by using django-polymorphic.
        """
        return {
            f"{s.__name__.lower()}__isnull": True
            for s in DigitalObject.__subclasses__()
        }

    @property
    def is_subclass(self):
        """Test if this object should be casted to a subclass.

        This functionality could be much improved by using django-polymorphic.
        """
        return any(
            hasattr(self, s.__name__.lower()) for s in DigitalObject.__subclasses__()
        )

    def casted_as_subclass(self):
        """Cast a generic DigitalObject instance to the subclass it should be.

        Returns ``None`` if this object really is just a generic digital object.

        Note that this works only for direct subclasses. Also note that this is hugely
        inefficient right now, especially when casting many objects during one request.

        This functionality could be much improved by using django-polymorphic.
        """
        # this works only for direct subclasses
        # because __subclasses__() lists only direct subclasses
        for s in DigitalObject.__subclasses__():
            if hasattr(self, s.__name__.lower()):
                return getattr(self, s.__name__.lower())
        return None

    def casted_or_self(self):
        """Cast a generic DigitalObject instance to the subclass or return itself.

        Same as :meth:`casted_as_subclass` but doesn't cast if there is no subclass.

        This functionality could be much improved by using django-polymorphic.
        """
        potential_subclass = self.casted_as_subclass()
        if potential_subclass is not None:
            return potential_subclass
        return self

    def to_rdflib_node(self):
        """May be deprecated in the future in favour of :meth:`as_rdf`."""
        if self.persistent_id:
            return URIRef(url_encode(self.persistent_id))
        return URIRef(url_encode(self.full_url()))

    def save_with_namespace(self, namespace, project):
        """Save the object while adding some common extra information.

        This encapsulates a very common pattern to save a DigitalObject.

        :param namespace: The namespace as string of labels separated by "/"
        :param project: A project to add to the DigitalObject.
        """
        self.namespace_str = namespace
        self.save()
        self.projects.add(project)

    def create_copy(self):
        """Related Django docs: :ref:`django:topics/db/queries:copying model instances`."""  # noqa: E501
        obj = DigitalObject.objects.get(digital_object_id=self.digital_object_id)
        obj = obj.casted_or_self()
        obj._state.adding = True
        obj.pk = None
        obj.digital_object_id = None
        obj.generated_persistent_id = None
        obj.persistent_id = None
        if hasattr(obj, "digitalobject_ptr"):
            base_obj = DigitalObject.objects.get(
                digital_object_id=obj.digitalobject_ptr_id
            )
            base_obj._state.adding = True
            base_obj.pk = None
            base_obj.save()
            obj.digitalobject_ptr = base_obj
        obj.save()
        if obj.pk is None:
            if obj.digital_object_id:
                logger.warning(
                    f"Django somehow failed to set pk of object {obj.digital_object_id} when saving the object"  # noqa: E501
                )
                obj = DigitalObject.objects.get(digital_object_id=obj.digital_object_id)
                obj = obj.casted_or_self()
            else:
                logger.error(
                    "Django somehow failed to set obj.pk and obj.digital_object_id"
                )

        for rel in self.relations.all():
            rel._state.adding = True
            rel.pk = None
            rel.subject = obj
            rel.save()
        for attr in self.attributes.all():
            attr._state.adding = True
            attr.pk = None
            attr.subject = obj
            attr.save()

        DigitalObjectRelation.objects.create(
            subject=obj, predicate=Vocabulary().is_version_of, object=self
        )
        return obj

    def add_tag(self, tag):
        """
        Tag the object with ``tag``. See also :meth:`Tag.set_on`.

        :type tag: Tag
        """
        tag.set_on(self)

    def remove_tag(self, tag):
        """
        Object is no longer tagged with ``tag``. See also :meth:`Tag.unset_on`.

        :type tag: Tag
        """
        tag.unset_on(self)

    def get_tags(self, project=None):
        """Get all tags of this DigitalObject.

        See also :meth:`add_tag`, :meth:`remove_tag` and
        :func:`heliport.core.utils.queries.tags_of`.

        :param project: if provided the result is limited to tags in this project.
        """
        from .utils.queries import tags_of

        return tags_of(self, project)

    def mark_deleted(self, user: HeliportUser = None):
        """Mark the DigitalObject as deleted.

        Includes final access rights check.
        """
        assert self.context is not None or user is not None, (
            "set context before calling mark_delete"
        )
        if user is not None and self.context is not None:
            assert self.context.user_or_none == user, "user should be the current user"

        # cast because the type may influence the access rights
        casted = self.casted_or_self()
        if self.context is not None:
            casted.access(self.context).assert_delete_permission()
        else:
            with Context(user=user) as context:
                casted.access(context).assert_delete_permission()
        casted.deleted = timezone.now()
        casted.save()


class DigitalObjectRelation(models.Model):
    """Triple that describes the subject using a key value pair.

    The subject, predicate (key) and object (value) must all be digital objects.
    See :class:`DigitalObjectAttributes` for when the value is a string.
    """

    digital_object_relation_id = models.AutoField(primary_key=True)
    subject = models.ForeignKey(
        DigitalObject, on_delete=models.CASCADE, related_name="relations"
    )
    predicate = models.ForeignKey(
        DigitalObject, on_delete=models.CASCADE, related_name="predicate_relations"
    )
    object = models.ForeignKey(
        DigitalObject, on_delete=models.CASCADE, related_name="reverse_relations"
    )
    is_public = models.BooleanField(default=False)
    special_heliport_role = models.CharField(  # noqa: DJ001
        max_length=15, blank=True, null=True
    )

    def __str__(self):
        """Get string representation."""
        return f"{self.pk}: {self.subject.label} --[{self.predicate.label}]--> {self.object.label}"  # noqa: E501


class DigitalObjectAttributes(models.Model):
    """Describes digital objects using arbitrary properties where values are a strings.

    See also :class:`DigitalObjectRelation`.
    """

    digital_object_attribute_id = models.AutoField(primary_key=True)
    subject = models.ForeignKey(
        DigitalObject, on_delete=models.CASCADE, related_name="attributes"
    )
    predicate = models.ForeignKey(
        DigitalObject, on_delete=models.CASCADE, related_name="predicate_attributes"
    )
    value = models.TextField()
    is_public = models.BooleanField(default=False)
    special_heliport_role = models.CharField(  # noqa: DJ001
        max_length=15, blank=True, null=True
    )

    class Meta:
        """Define constraints.

        The ``special_heliport_role`` property of the relation is for use by
        :class:`MetadataField` each object can only have one of any kind of
        MetadataField since metadata fields behave like normal python attributes.
        """

        constraints = [
            models.UniqueConstraint(
                fields=["subject", "predicate", "special_heliport_role"],
                condition=models.Q(special_heliport_role__isnull=False),
                name="unique_special_attributes_per_subject",
            )
        ]

    def __str__(self):
        """Get value as string representation."""
        return self.value


class DigitalObjectIdentifier(models.Model):
    """Model representing identifiers for :class:`DigitalObject`.

    This is not to be confused with `DOIs <https://www.doi.org/>`_. While DOIs can and
    will appear in this model, it may store any kind of identifier that HELIPORT apps
    whish to store. A digital object may be associated with any number of identifiers
    via this model (including 0).
    """

    #: The primary key of the model.
    digital_object_identifier_id = models.AutoField(primary_key=True)

    #: The scheme of the identifier. This must be a uniquely identifying string from
    #: which the correct implementation that handles this identifier can be found.
    scheme = models.TextField()

    #: An immutable identifier means that its metadata (e.g. on a handle.net server or
    #: any other registry) may not be updated. Typically, this field will be set to
    #: ``True`` on user-provided identifiers not managed by HELIPORT, e.g. to prevent
    #: hijacking of identifiers.
    immutable = models.BooleanField(default=False)

    #: The identifier, in a format that is useful to the provider. E.g., in the case of
    #: a Handle, this could be ``20.500.99999/HELIPORT.handle.123``.
    identifier = models.TextField()

    #: The human-readable version of the ``identifier``. E.g., for the identifier
    #: ``https://doi.org/10.14278/rodare.946``, the ``display_text`` could be set to
    #: ``10.14278/rodare.946``.
    display_text = models.TextField(blank=True)

    #: The identifier as a full URL. E.g., in the case of a Handle, this could be
    #: ``https://hdl.handle.net/20.500.99999/HELIPORT.handle.123``.
    url = models.URLField(blank=True)

    #: The :class:`DigitalObject` referenced by this identifier.
    digital_object = models.ForeignKey(
        DigitalObject, on_delete=models.CASCADE, related_name="identifiers"
    )

    def __str__(self) -> str:
        """Represent the identifier as a string.

        Returns the :attr:`display_text` if it is set, otherwise :attr:`identifier`.
        """
        return self.display_text or self.identifier


class Project(DigitalObject):
    """Class for storing a HELIPORT project.

    ``Project`` is the main container for all kinds of :class:`DigitalObject` instances.
    A project is also the main way of managing access to digital objects.
    """

    project_id = models.AutoField(primary_key=True)
    group = models.ForeignKey(HeliportGroup, on_delete=models.SET_NULL, null=True)
    parts_is_public = models.BooleanField(default=False)

    original_id = models.CharField(max_length=100, null=True, blank=True)  # noqa: DJ001

    def get_custom_view_url(self, user=None):
        """This defines the custom landing page to be the object graph."""  # noqa: D401
        if user in self.members():
            return reverse("core:project_graph", kwargs={"project": self.pk})
        return None

    def digital_object_lookup(self):
        """Get parts as dict of query sets keyed by type as string."""
        result = {}
        for s in DigitalObject.__subclasses__():
            name = s.__name__.lower()
            query = {f"{name}__isnull": False}

            result[name] = self.parts.filter(**query)
        return result

    def last_modified_aggregate(self):
        """Get the last date this project or one of its parts was modified.

        Note that parts in subprojects are not counted.
        """
        if self.project_id is None:
            return None

        last_modified = self.parts.aggregate(models.Max("last_modified"))[
            "last_modified__max"
        ]
        if last_modified is None or self.last_modified is None:
            return self.last_modified
        return max(last_modified, self.last_modified)

    def sub_project_list(self, max_level=8, level=1, parent_path=None):
        """
        Get a list of named tuples with the following attributes:

            - path: list of parent project ids
            - level: how deep is the subproject nested (as iterable python range)
            - project: the subproject model instance
            - has_child: boolean indicating if there are more subprojects under this

        for each subproject
        """  # noqa: D400
        if level > max_level:
            return []
        if parent_path is None:
            parent_path = []

        SubprojectItem = namedtuple("SubprojectItem", "path level project has_child")

        result = []
        for sub_project in Project.objects.filter(projects=self, deleted__isnull=True):
            sub_result = sub_project.sub_project_list(
                max_level, level + 1, [*parent_path, f"_{sub_project.project_id}"]
            )
            result.append(
                SubprojectItem(parent_path, range(level), sub_project, bool(sub_result))
            )
            result.extend(sub_result)

        return result

    def get_parent_projects(self, exclude: typing.Iterable = None):
        """Get set of all projects that have this project as subproject.

        Ignore subprojects and subprojects of subprojects that are in the ``exclude``
        set. The project itself is also part of the result.
        """
        new_exclude = {self}
        if exclude is not None:
            new_exclude.update(exclude)

        result = {self}
        for parent_project in self.projects.all():
            if parent_project not in new_exclude:
                result.update(parent_project.get_parent_projects(new_exclude))

        return result

    def access(self, context: Context):
        """Get permission checker for this object and context.

        See :meth:`heliport.core.models.DigitalObject.access` for base implementation.

        There is an additional check that current user must be owner to delete the
        object.
        """
        from .permissions import OnlyOwnerCanDeleteChecker

        return OnlyOwnerCanDeleteChecker(self, context)


class LoginInfo(models.Model):
    """Encrypted user credentials and SSH keys.

    Note that tokens are stored as ``key`` attribute.
    """

    class LoginTypes(models.TextChoices):
        """Define allowed login types using Django `TextChoices`."""

        SSH = "ssh connection"
        USER_PASSWORD = "username and password"
        TOKEN = "authentication token"

    login_info_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(HeliportUser, on_delete=models.CASCADE)
    username = models.CharField(max_length=100, blank=True)
    encrypted_password = models.CharField(max_length=500, blank=True)
    key = models.TextField(blank=True)
    machine = models.CharField(max_length=100, blank=True)
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=50, choices=LoginTypes.choices)
    data = models.TextField(blank=True)
    via = models.ForeignKey("self", on_delete=models.SET_NULL, null=True, blank=True)
    slurm = models.BooleanField(default=False)

    def __str__(self):
        """Get name property as string representation."""
        return self.name

    @property
    def password(self):
        """Decrypted password."""
        if self.encrypted_password:
            return encryption.decrypt(self.encrypted_password)
        return ""

    @password.setter
    def password(self, value):
        """Setter for setting unencrypted password.

        The password is then stored encrypted.
        See also :mod:`heliport.core.utils.encryption`.
        """
        if value:
            self.encrypted_password = encryption.encrypt(value)
        else:
            self.encrypted_password = ""

    def build_command_executor(self, depth=10):
        """Generate :class:`heliport.core.utils.command_line_execution.CommandExecutor`.

        Only applicable to SSH keys. The connection is performed to the `machine` using
        the SSH `key`. SSH logins can be recursive by the `via` property up to a maximum
        depth specified by the `depth` parameter.
        """
        if self.type != self.LoginTypes.SSH:
            raise TypeError("not a ssh login")
        if depth <= 0:
            raise ValueError("max depth reached: may be caused by cyclic ssh login")

        indirection = None
        if self.via is not None:
            indirection = self.via.build_command_executor(depth=depth - 1)

        return create_command_executor(
            host=self.machine,
            username=self.username,
            key=self.key,
            indirection=indirection,
            auto_connect=False,
        )

    def build_command_executor_indirection(self):
        """Build command executor for the SSH indirection specified via ``via``.

        Returns ``None`` if no indirection is specified.
        See also :meth:`build_command_executor`.
        """
        if self.via is None:
            return None

        return self.via.build_command_executor()

    @property
    def connected(self) -> bool:
        """Connected status of SSH logins.

        SSH logins are considered connected if there is an SSH key generated for them.
        If the connection gets disconnected by the User the SSH key is deleted also from
        the allowed hosts on the specified machine.
        """
        if self.type != self.LoginTypes.SSH:
            return False
        return bool(self.data) and bool(self.key)

    def get_to_connect_list(self, depth=10, exclude_self=False) -> list:
        """Get list of SSH Logins parameters that need to be connected before this one.

        :param depth: Max recursion depth. Raise ``ValueError`` when exceeded.
        :param exclude_self: exclude the current login from the list.
        """
        if self.type != self.LoginTypes.SSH:
            raise TypeError("not a ssh login")
        if depth <= 0:
            raise ValueError("max depth reached: may be caused by cyclic ssh login")

        result = []
        if self.via is not None:
            result = self.via.get_to_connect_list(depth=depth - 1)
        if not self.connected and not exclude_self:
            result.append(self)
        return result

    def get_unconnected_parents(self) -> list:
        """Get unconnected ssh indirections.

        Same as :meth:`get_to_connect_list` with ``exclude_self=True``.
        """
        return self.get_to_connect_list(exclude_self=True)


class Contribution(models.Model):
    """People who contributed to a :class:`DigitalObject` in some way.

    This is mostly used for contributions to a :class:`Project`.
    """

    class ContributionTypes(models.TextChoices):
        """List of contribution types as defined by DataCite."""

        CONTACT_PERSON = "ContactPerson", "Contact Person"
        DATA_COLLECTOR = "DataCollector", "Data Collector"
        DATA_CURATOR = "DataCurator", "Data Curator"
        DATA_MANAGER = "DataManager", "Data Manager"
        DISTRIBUTOR = "Distributor", "Distributor"
        EDITOR = "Editor", "Editor"
        HOSTING_INSTITUTION = "HostingInstitution", "Hosting Institution"
        PRODUCER = "Producer", "Producer"
        PROJECT_LEADER = "ProjectLeader", "Project Leader"
        PROJECT_MANAGER = "ProjectManager", "Project Manager"
        PROJECT_MEMBER = "ProjectMember", "Project Member"
        REGISTRATION_AGENCY = "RegistrationAgency", "Registration Agency"
        REGISTRATION_AUTHORITY = "RegistrationAuthority", "Registration Authority"
        RELATED_PERSON = "RelatedPerson", "Related Person"
        RESEARCHER = "Researcher", "Researcher"
        RESEARCH_GROUP = "ResearchGroup", "Research Group"
        RIGHTS_HOLDER = "RightsHolder", "Rights Holder"
        SPONSOR = "Sponsor", "Sponsor"
        SUPERVISOR = "Supervisor", "Supervisor"
        WORK_PACKAGE_LEADER = "WorkPackageLeader", "Work Package Leader"
        OTHER = "Other", "Other"

    contribution_id = models.AutoField(primary_key=True)
    contributor = models.ForeignKey(HeliportUser, on_delete=models.CASCADE)
    contribution_to = models.ForeignKey(DigitalObject, on_delete=models.CASCADE)
    type = models.CharField(
        max_length=50,
        choices=ContributionTypes.choices,
        default=ContributionTypes.PROJECT_MEMBER,
    )
    uuid = models.UUIDField(
        primary_key=False, default=generate_object_id, editable=False
    )

    class Meta:
        """Database constraints.

        Each person can contribute to something only in one way.
        """

        constraints = [
            models.UniqueConstraint(
                fields=["contributor", "contribution_to"],
                name="unique_digital_object_contribution",
            )
        ]

    def __str__(self):
        """Get string representation."""
        return f"{self.contributor}/{self.contribution_to}"

    def to_rdflib_node(self):
        """Represent contribution as RDF.

        Contributions are represented as BNodes using the uuid property of contribution.
        """
        return BNode(remove_special_chars(self.uuid))

    @classmethod
    def type_from_str(cls, the_type):
        """Get contribution type identifier given human readable label."""
        for t, label in cls.ContributionTypes.choices:
            if t == the_type or label == the_type:
                return t
        return None


class Image(DigitalObject):
    """Represent an Image in HELIPORT."""

    class ImageStates(models.IntegerChoices):
        """When creating new image state is "UPLOADING".

        After uploading it is either "OK" or "FAILED".
        """

        OK = 1
        UPLOADING = 2
        FAILED = 3

    directory = models.TextField()
    filename = models.TextField()
    login = models.ForeignKey(LoginInfo, on_delete=models.SET_NULL, null=True)
    status = models.IntegerField(
        choices=ImageStates.choices, default=ImageStates.UPLOADING
    )
    message = models.TextField(blank=True)
    replace_if_exists = models.BooleanField(default=False)  # always replacing currently
    download_url_is_public = models.BooleanField(default=False)

    @property
    def path(self):
        """Get pathlib ``Path`` of image at computer specified by ``login``."""
        return Path(self.directory) / self.filename

    @property
    def mimetype(self):
        """Image mimetype."""
        result, encoding = guess_type(self.filename)
        return result

    def fail(self, message):
        """Set status to `FAILED` with user message."""
        self.status = self.ImageStates.FAILED
        self.message = message
        self.save()
        return

    def upload_image(self, image_url):
        """Upload image to computer defined by ``login``.

        The image data is taken from base46 encoded ``image_url`` parameter.
        """
        self.status = self.ImageStates.UPLOADING
        self.save()

        data_url_parts = image_url.split(";base64,", maxsplit=1)
        if len(data_url_parts) != 2:
            logger.warning(f"Invalid base64 data url: {image_url[:40]}")
            return self.fail("Invalid base64 data url.")

        with (
            self.login.build_command_executor() as executor,
            executor.file(str(self.path), "wb") as file,
        ):
            file.write(b64decode(data_url_parts[1]))

        self.status = self.ImageStates.OK
        self.save()
        return None

    def download_data(self):
        """Get image data as bytes.

        Download from server specified by ``login``. If download fails status is set to
        ``FAILED``.
        """
        try:
            with (
                self.login.build_command_executor() as executor,
                executor.file(str(self.path)) as file,
            ):
                return file.read()
        except FileNotFoundError:
            if self.status == self.ImageStates.OK:
                self.status = self.ImageStates.FAILED
                self.message = "file not found"
                self.save()
            return b""

    def get_custom_url(self, user=None):
        """Get custom landing page URL.

        This function is recognized when showing the landing page and the landing page
        becomes the image data itself when using the default landing page type.

        If the user does not have access the normal landing page is shown.
        """
        if user in self.members():
            return reverse("core:image_data", kwargs={"pk": self.pk})
        return None

    def access(self, context: Context):
        """Get permission checker for this object and context.

        See :meth:`heliport.core.models.DigitalObject.access` for base implementation.

        There is also a check that only gives write permission if the current user owns
        the login.
        """
        from .permissions import LoginPermissionChecker

        return LoginPermissionChecker(self, context)


class Tag(DigitalObject):
    """A Digital Object D is tagged with a tag if relation (D, attribute, value) exists.

    The :attr:`attribute` and :attr:`value` are set/defined as properties of the tag.
    """

    tag_id = models.AutoField(primary_key=True)
    attribute = models.ForeignKey(
        DigitalObject,
        on_delete=models.CASCADE,
        related_name="tags_where_property",
        null=True,
    )
    value = models.ForeignKey(
        DigitalObject,
        on_delete=models.CASCADE,
        related_name="tags_where_value",
        null=True,
    )
    hex_color = models.CharField(max_length=8)
    requires_type = models.ForeignKey(
        DigitalObject,
        on_delete=models.CASCADE,
        null=True,
        related_name="tags_where_type_required",
    )
    last_use_date = models.DateTimeField(auto_now=True)

    @property
    def html_color(self):
        """Color to be used in html e.g. #00FF35."""
        return f"#{self.hex_color}"

    @html_color.setter
    def html_color(self, value):
        """Setter for :attr:`html_color`."""
        if value.startswith("#"):
            value = value[1:]
        self.hex_color = value

    @property
    def html_foreground_color(self):
        """Color to be used for text if background has :attr:`html_color`."""
        return pick_foreground_color(self.html_color)

    @property
    def rgb_color(self):
        """Same as :attr:`html_color` but returns a tuple of values up to 255."""
        try:
            return tuple(int(self.hex_color[i : i + 2], 16) for i in (0, 2, 4))
        except ValueError:
            return 0, 0, 0

    @rgb_color.setter
    def rgb_color(self, value):
        """Setter for :attr:`rgb_color`."""
        r, g, b = value
        self.hex_color = f"{r:02x}{g:02x}{b:02x}"

    @property
    def requires_type_display(self):
        """Displayable version of :attr:`requires_type`.

        Tag might be for specific type or any type. This property can be used in an
        HTML template.
        """
        if self.requires_type is None:
            return "Anything"
        return self.requires_type

    def update_usage_date(self):
        """Set last used to now."""
        self.save()

    def get_key_value(self):
        """Return tuple of attribute and value.

        Attribute defaults to has_tag and value to the tag itself if they are None.
        """
        vocab = Vocabulary()
        attribute = self.attribute
        value = self.value
        if attribute is None:
            attribute = vocab.has_tag
        if value is None:
            value = self
        return attribute, value

    def relation_query_set(self):
        """Get a django ORM query set for this tag.

        The query set contains the :class:`DigitalObjectRelation` instances that make a
        :class:`DigitalObject` tagged by this tag.
        """
        attribute, value = self.get_key_value()
        vocab = Vocabulary()
        return DigitalObjectRelation.objects.filter(
            Q(predicate=attribute, object=value)
            | Q(predicate=vocab.has_tag, object=self)
        )

    def applicable_for(self, obj: DigitalObject):
        """Determine if ``obj`` has type required by the tag."""
        if self.requires_type is None:
            return True
        vocab = Vocabulary()
        return DigitalObjectRelation.objects.filter(
            subject=obj, predicate=vocab.type, object=self.requires_type
        ).exists()

    def set_on(self, obj: DigitalObject):
        """Tag ``obj`` with this tag. See also :meth:`DigitalObject.add_tag`."""
        attribute, value = self.get_key_value()
        assert_relation(obj, attribute, value)
        self.update_usage_date()

    def unset_on(self, obj: DigitalObject):
        """Remove tag from ``obj``. See also :meth:`DigitalObject.remove_tag`."""
        vocab = Vocabulary()
        attribute, value = self.get_key_value()
        assert_not_relation(obj, attribute, value)
        assert_not_relation(obj, vocab.has_tag, self)
        self.update_usage_date()

    @property
    def as_dict(self):
        """Return a serialized version of this tag as dict to be converted to JSON."""
        return {
            "label": self.label,
            "tag_id": self.tag_id,
            "attribute": self.attribute_id,
            "value": self.value_id,
            "requires_type": self.requires_type_id,
            "color": self.html_color,
            "foreground": self.html_foreground_color,
        }


class NamedToken(models.Model):
    """HELIPORT API Token that is assigned a name by the user."""

    token_id = models.AutoField(primary_key=True)
    name = models.TextField()
    auth_token = models.OneToOneField(knox_models.AuthToken, on_delete=models.CASCADE)

    def __str__(self):
        """Get name as string representation."""
        return self.name


class MetadataField:
    """Defines a python descriptor for metadata properties.

    This descriptor should be used on :class:`DigitalObject` subclasses.

    In principle most properties of subclasses of :class:`DigitalObject` can be
    considered "Metadata Properties" however using ``MetadataFiled`` has the added
    convenience that metadata serialization is automatically taken care of just by
    specifying the metadata property in the class definition. On top of that Storage if
    the property is public and can be shown on the landing page to everybody is handled
    without extra code.

    Use it for example like this::

        class MyClass(DigitalObject):
            link = MetadataField(Vocabulary.primary_topic, url_normalizer)

    The first argument comes from :class:`Vocabulary` and defines the metadata property
    to use for serialization. The second argument is optional and is just a function
    that is called before setting the value.
    :func:`heliport.core.utils.normalization.url_normalizer` is used in the example.

    The values are stored as :class:`DigitalObjectAttributes` and identified by thir
    property as well as an additional identification string that is by default
    `"attribute"`. This means if you have multiple properties with the same metadata
    property you need to set the ``role_name_in_db`` parameter uniquely for each.
    Also if you change the metadata attribute or the ``role_name_in_db``
    you need to write a database migration.
    """

    def __init__(self, attribute_vocab, normalizer=str, *, role_name_in_db="attribute"):
        """Initialize."""
        self.attribute = attribute_vocab
        self.normalizer = normalizer
        self.role = role_name_in_db

    def query(self, instance):
        """Get the Django ORM query parameters to query relevant values.

        These query parameters are for querying :class:`DigitalObjectAttributes`.
        """
        return {
            "subject": instance,
            "predicate": self.attribute.obj,
            "special_heliport_role": self.role,
        }

    def __get__(self, instance, owner=None):
        """Define Python descriptor __get__."""
        attribute_object = DigitalObjectAttributes.objects.filter(
            **self.query(instance)
        ).first()
        if attribute_object is None:
            return None
        return attribute_object.value

    def __set__(self, instance, value):
        """Define python descriptor __set__."""
        if value is None:
            attr = self.__get__(instance)
            if attr is not None:
                attr.delete()
        else:
            attr, is_new = DigitalObjectAttributes.objects.get_or_create(
                **self.query(instance)
            )
            attr.value = self.normalizer(value)
            attr.save()


class FromDB:
    """Get a digital object by special_heliport_role.

    This is a python descriptor used in :class:`Vocabulary`. It hanldes
    getting objects lazily (need to call :attr:`obj` later) when attribute is
    accessed on class and get object directly when attribute is accessed on instance.
    """

    def __init__(self, role_name_in_db):
        """Initialize."""
        self.role_name_in_db = role_name_in_db

    @property
    def obj(self):
        """Get the digital object."""
        result = DigitalObject.objects.filter(
            special_heliport_role=self.role_name_in_db
        ).first()
        assert result is not None, (
            f"add {self.role_name_in_db} to vocabulary_core.py and migrate db"
        )
        return result

    def __get__(self, instance, owner):
        """Implement python descriptor __get__."""
        if instance is None:
            return self
        return self.obj


class Vocabulary:
    """To get a digital object use `Vocabulary.<name>.obj` or `Vocabulary().<name>`."""

    Property = FromDB("Property")
    Class = FromDB("Class")
    Namespace = FromDB("Namespace")
    HELIPORT = FromDB("HELIPORT")
    Project = FromDB("Project")

    type = FromDB("type")
    suggested_property = FromDB("suggested_property")
    subnamespace_of = FromDB("subnamespace_of")
    subclass_of = FromDB("subclass_of")
    label = FromDB("label")
    description = FromDB("description")
    sub_property_of = FromDB("sub_property_of")
    has_tag = FromDB("has_tag")
    has_part = FromDB("has_part")
    input_of = FromDB("input_of")
    # input parameter in a workflow description
    has_input = FromDB("has_input")
    # something is used as the input parameter defined by has_input
    described_by_parameter = FromDB("described_by_parameter")
    # a job is described by some workflow/tool
    described_by_process = FromDB("described_by_process")
    primary_topic = FromDB("primary_topic")
    status = FromDB("status")
    has_identifier = FromDB("has_identifier")
    filename = FromDB("filename")
    is_version_of = FromDB("is_version_of")


def digital_object_form_label_and_pk(label, pk) -> typing.Optional[DigitalObject]:
    """Resolve a DigitalObject from pk.

    If pk is not truthy, label is used if it uniquely identifies a DigitalObject.
    This is useful for getting a digital object from an autocomplete.
    if pk is not an integer or a string representing an integer and label is not truthy,
    this function returns ``None``.
    """
    result = None
    if pk:
        result = DigitalObject.objects.filter(pk=pk).first()
    if result is None or label not in result.label:
        possibilities = []
        if label:
            possibilities = DigitalObject.objects.filter(label=label)
        if len(possibilities) == 1:
            result = list(possibilities)[0]

    return result


def get_or_create_with_relation(label, category, pred, obj):
    """Get or create a digital object that has a specific relation.

    The returned digital object (that might be newly created) is the subject in the
    relation. The subject must have the ``label``. When the subject is newly created
    also the ``category`` is set on it along with the ``label``. The relation has
    subject (the digital object of interest), ``pred`` and ``obj`` which form a triple
    of digital objects. See also :class:`DigitalObjectRelation`.
    """
    rel = DigitalObjectRelation.objects.filter(
        subject__label=label, predicate=pred, object=obj
    ).first()
    if rel is None:
        new_obj = DigitalObject.objects.create(label=label)
        rel, created = DigitalObjectRelation.objects.get_or_create(
            subject=new_obj, predicate=pred, object=obj
        )
        new_obj.category_str = category  # note the recursion: category_str calls get_or_create_with relation  # noqa: E501
        new_obj.save()
    return rel.subject


def assert_relation(subj: DigitalObject, pred: DigitalObject, obj: DigitalObject):
    """Insert the triple (subj, pred, obj) into :class:`DigitalObjectRelation` table.

    Semantically this makes the statement considered true in HELIPORT.
    Another way to view this is to set the key-value-pair "pred: obj" in the metadata
    of subj.

    Consider the example, where ``HELIPORT``, ``uses`` and ``Django``
    are :class:`DigitalObject` instances::

        assert_relation(HELIPORT, uses, Django)

    This means that the statement "HELIPORT uses Django" is assumed to be true or that
    the metadata of HELIPORT includes the key-value-pair "uses: Django".
    """
    rel, created = DigitalObjectRelation.objects.get_or_create(
        subject=subj, predicate=pred, object=obj
    )
    return created


def assert_not_relation(subj: DigitalObject, pred: DigitalObject, obj: DigitalObject):
    """Opposite of :func:`assert_relation`.

    Means that the statement is no longer assumed to be true.
    """
    relations = DigitalObjectRelation.objects.filter(
        subject=subj, predicate=pred, object=obj
    )
    if relations.exists():
        relations.delete()
        return True

    return False


def assert_relation_truth(
    subj: DigitalObject, pred: DigitalObject, obj: DigitalObject, truth: bool
):
    """Assert the truthiness of a relation based on a boolean.

    Same as :func:`assert_relation` and :func:`assert_not_relation` but which one is
    decided by ``truth``.
    """
    if truth:
        return assert_relation(subj, pred, obj)
    return assert_not_relation(subj, pred, obj)


def get_special_object(special_heliport_role: str, allow_create_with: dict = None):
    """Get a digital object by ``special_heliport_role``.

    The ``special_heliport_role`` is a string to identify a digital object in the
    database inside the source code itself. If the object does not exist ``None`` is
    returned.

    If ``allow_create_with`` is set to a ``dict`` the object is created with the default
    values specified in the dict.

    This function only exists for special cases. The normal approach is to use
    :mod:`heliport.core.vocabulary_core` to define what objects should exist and with
    which parameters. (These are created when migrating the database) And then use
    :class:`Vocabulary` to get the objects in code.
    """
    if allow_create_with is None:
        return DigitalObject.objects.filter(
            special_heliport_role=special_heliport_role
        ).first()
    return DigitalObject.objects.get_or_create(
        defaults=allow_create_with, special_heliport_role=special_heliport_role
    )
