# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from django.db import migrations


def build_set_role_command(model):
    def set_role(label, role):
        obj = model.objects.filter(label=label).order_by("pk").first()
        if obj is not None:
            obj.special_heliport_role = role
            obj.save()

    return set_role


def set_special_heliport_role(apps, schema_editor):
    set_role = build_set_role_command(apps.get_model("core", "DigitalObject"))

    set_role("Property", "Property")
    set_role("Class", "Class")
    set_role("Namespace", "Namespace")
    set_role("HELIPORT", "HELIPORT")

    set_role("type", "type")
    set_role("suggested property", "suggested_property")
    set_role("subnamespace of", "subnamespace_of")
    set_role("subclass of", "subclass_of")


def unset_special_heliport_role(apps, schema_editor):
    digital_object_model = apps.get_model("core", "DigitalObject")
    for digital_object in digital_object_model.objects.all():
        digital_object.special_heliport_role = None
        digital_object.save()


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0010_contribution_type"),
    ]

    operations = [
        migrations.RunPython(set_special_heliport_role, unset_special_heliport_role),
    ]
