# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Generated by Django 4.2.14 on 2024-09-05 07:17

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0031_remove_project_hzdr_id"),
    ]

    operations = [
        migrations.AddField(
            model_name="digitalobjectidentifier",
            name="url",
            field=models.URLField(blank=True),
        ),
    ]
