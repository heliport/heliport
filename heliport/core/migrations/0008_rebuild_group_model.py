# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


def copy_groups(apps, schema_editor):
    group_old_model = apps.get_model("core", "GroupOld")
    group_new_model = apps.get_model("core", "Group")
    for group in group_old_model.objects.all():
        group_new_model(display_name=group.pk).save()


def update_project_groups(apps, schema_editor):
    project_model = apps.get_model("core", "Project")
    group_model = apps.get_model("core", "Group")
    for project in project_model.objects.exclude(group_name__isnull=True).exclude(
        group_name__exact=""
    ):
        group = group_model.objects.get(display_name=project.group_name)
        project.group = group
        project.save()


def sync_from_ldap(apps, schema_editor):
    if not settings.LDAP_ENABLED:
        return

    group_model = apps.get_model("core", "Group")
    if not group_model.objects.all():
        # There is nothing to do
        return

    import ldap

    server_url = settings.LDAP_SERVER_URL
    bind_user = settings.LDAP_BIND_USER_DN
    bind_password = settings.LDAP_BIND_PASSWORD
    group_base_dn = settings.LDAP_GROUP_BASE_DN
    group_id_attribute = settings.LDAP_GROUP_ID_ATTRIBUTE
    group_display_name_attribute = settings.LDAP_GROUP_DISPLAY_NAME_ATTRIBUTE
    group_filter = settings.LDAP_GROUP_FILTER
    group_object_class = settings.LDAP_GROUP_OBJECT_CLASS

    connection = ldap.initialize(server_url)
    connection.simple_bind_s(bind_user, bind_password)

    group_filterstr = f"(objectClass={group_object_class})"
    if group_filter is not None:
        group_filterstr = f"(&{group_filterstr}{group_filter})"
    results = connection.search_s(
        group_base_dn,
        ldap.SCOPE_SUBTREE,
        filterstr=group_filterstr,
        attrlist=[group_id_attribute, group_display_name_attribute],
    )

    groups = (
        (
            attributes[group_id_attribute][0].decode(),
            attributes[group_display_name_attribute][0].decode(),
        )
        for dn, attributes in results
    )

    for group_id, group_name in groups:
        # We only have the name at this point; want to populate backend_id
        group, group_is_new = group_model.objects.get_or_create(display_name=group_name)
        group.backend_id = group_id
        group.save()


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0007_remove_logininfo_password"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="Group",
            new_name="GroupOld",
        ),
        migrations.CreateModel(
            name="Group",
            fields=[
                ("group_id", models.AutoField(primary_key=True, serialize=False)),
                ("backend_id", models.CharField(max_length=100, null=True)),
                ("display_name", models.CharField(blank=True, max_length=100)),
            ],
        ),
        migrations.RunPython(copy_groups),
        migrations.RenameField(
            model_name="project",
            old_name="group",
            new_name="group_name",
        ),
        migrations.AlterField(
            model_name="project",
            name="group_name",
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name="project",
            name="group",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="core.group",
            ),
        ),
        migrations.RunPython(update_project_groups),
        migrations.RunPython(sync_from_ldap),
        migrations.RemoveField(
            model_name="project",
            name="group_name",
        ),
        migrations.DeleteModel(
            name="GroupOld",
        ),
    ]
