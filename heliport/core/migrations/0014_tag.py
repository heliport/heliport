# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Generated by Django 3.2.13 on 2022-05-02 11:46

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0013_timelineconfiguration"),
    ]

    operations = [
        migrations.CreateModel(
            name="Tag",
            fields=[
                (
                    "digitalobject_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        to="core.digitalobject",
                    ),
                ),
                ("tag_id", models.AutoField(primary_key=True, serialize=False)),
                ("hex_color", models.CharField(max_length=8)),
                ("last_use_date", models.DateTimeField(auto_now=True)),
                (
                    "attribute",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="tags_where_property",
                        to="core.digitalobject",
                    ),
                ),
                (
                    "requires_type",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="tags_where_type_required",
                        to="core.digitalobject",
                    ),
                ),
                (
                    "value",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="tags_where_value",
                        to="core.digitalobject",
                    ),
                ),
            ],
            bases=("core.digitalobject",),
        ),
    ]
