# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Additional settings for this app.

See also `django appconf <https://django-appconf.readthedocs.io>`_
"""

import environ
from appconf import AppConf
from django.core.exceptions import ImproperlyConfigured

env = environ.Env()


class HeliportAppConf(AppConf):
    """General settings of the :mod:`heliport.core` app.

    All settings in this class can be overwritten in settings.py
    """

    MAINTENANCE_MESSAGE_LEVEL = env.int(
        "HELIPORT_CORE_MAINTENANCE_MESSAGE_LEVEL", default=0
    )
    MAINTENANCE_MESSAGE = env.str(
        "HELIPORT_CORE_MAINTENANCE_MESSAGE", default="No maintenance today"
    )

    #: URL pointing to an external about page
    ABOUT_URL = "https://heliport.hzdr.de"

    #: URL pointing to an external documentation page
    DOCS_URL = "https://heliport.hzdr.de/documentation/"

    #: URL pointing to an external imprint page
    IMPRINT_URL = "https://www.hzdr.de/impressum"

    #: URL pointing to an external privacy policy page
    PRIVACY_URL = "https://heliport.hzdr.de/privacy/"

    #: URL pointing to an external terms of service page
    TOS_URL = "https://heliport.hzdr.de/terms/"

    #: URL pointing to an external page with citation instructions
    CITE_URL = "https://heliport.hzdr.de/cite/"

    #: The number of entries returned from autocomplete views
    AUTOCOMPLETE_COUNT = 10

    #: Despite the name ``HOST``, this setting contains the protocol, host, and port of
    #: the HELIPORT instance. A HELIPORT instance at
    #: ``https://heliport.example.com/app/`` has the ``HOST`` value
    #: ``https://heliport.example.com``.
    HOST = env.str("HELIPORT_CORE_HOST").rstrip("/")

    #: The key used for encryption of credentials in the database.
    #: (The key shown in the docs is just the dummy value from the CI.)
    CRYPTO_KEY = env.str("HELIPORT_CORE_CRYPTO_KEY")
    if CRYPTO_KEY == "":
        raise ImproperlyConfigured("HELIPORT_CORE_CRYPTO_KEY must not be empty")

    class Meta:  # noqa: D106
        prefix = "HELIPORT_CORE"
