# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Module for HELIPORT actions.

Register your actions in apps.py like this::

    from django.apps import AppConfig
    from heliport.core.digital_object_actions import actions

    class MyConfig(AppConfig):
        name = "my_app_name"

        def ready(self):
            from .views import MyAction, my_action_generation_func
            actions.register(MyAction)
            actions.register(my_action_generation_func)

where ``my_cation_generation_func`` is a function returning a list of actions, given
optionally a :class:`heliport.core.digital_object_interface.GeneralDigitalObject`, a
:class:`heliport.core.models.Project` and/or a
:class:`heliport.core.models.HeliportUser`
(same arguments as :class:`Action` constructor), and `MyAction` is a subclass of
:class:`Action`.
"""

from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING, Callable, List, Type, Union

from heliport.core.utils.collections import DecoratorList

if TYPE_CHECKING:
    from heliport.core.digital_object_interface import GeneralDigitalObject
    from heliport.core.models import DigitalObject, HeliportUser, Project


class Action(ABC):
    """Represent an action that can be executed on some digital objects.

    The properties:

    - ``obj`` - The :class:`heliport.core.models.DigitalObject` or
      :class:`heliport.core.digital_object_interface.GeneralDigitalObject`
      to perform the action on
    - ``project`` - The current project
    - ``user`` - The current user

    are available on the action and can be passed to the constructor. They may also be
    ``None``.
    """

    def __init__(  # noqa: D107
        self, obj: ObjType = None, project: Project = None, user: HeliportUser = None
    ):
        self.obj = obj
        self.project = project
        self.user = user

    @property
    @abstractmethod
    def name(self) -> str:
        """Display name of this action."""

    @property
    @abstractmethod
    def icon(self) -> str:
        """Name of a fontawesome icon."""

    @property
    @abstractmethod
    def link(self) -> str:
        """Link to where the action can be executed."""

    @property
    @abstractmethod
    def applicable(self) -> bool:
        """Determine if the action is executable for the given obj, project and user."""


if TYPE_CHECKING:
    ObjType = Union[DigitalObject, GeneralDigitalObject]
    ActionBuilder = Callable[[ObjType, Project, HeliportUser], List[Action]]

#: Register :class:`Action` subclasses to this in apps.py of your django app.
#: Call :meth:`heliport.core.utils.collections.DecoratorIndex.register` with your
#: :class:`Action` or action generation function for this:
#: ``actions.register(MyAction)``.
actions: DecoratorList[Union[Type[Action], ActionBuilder]] = DecoratorList()


def get_actions(
    obj: ObjType = None, project: Project = None, user: HeliportUser = None
) -> List[Action]:
    """Get all registered actions that are applicable for the given parameters.

    See also :meth:`heliport.core.digital_object_interface.actions`.
    """
    result = []
    for action in actions:
        possible_actions = action(obj, project, user)
        if isinstance(possible_actions, Action):
            possible_actions = [possible_actions]

        result.extend(a for a in possible_actions if a.applicable)

    return result
