# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from django.apps import apps
from rdflib import DCTERMS, FOAF, RDF, RDFS, URIRef
from rdflib.namespace import DCAM, Namespace

HELIPORT_NS = Namespace("https://hdl.handle.net/20.500.12865/heliport_vocab.v1#")


def set_attribs(  # noqa: D103
    role_name_in_db, label, persistent_id=None, append_to=None
):
    # can't import model directly because this is imported inside apps.py
    model = apps.get_model("core", "DigitalObject")
    obj, is_new = model.objects.get_or_create(special_heliport_role=role_name_in_db)
    obj.label = label
    if persistent_id is not None:
        obj.persistent_id = persistent_id
    obj.save()
    if append_to is not None:
        append_to.append(obj)
    return obj


def set_property(obj, predicate, value):  # noqa: D103
    model = apps.get_model("core", "DigitalObjectRelation")
    model.objects.get_or_create(subject=obj, predicate=predicate, object=value)


def insert_core_vocabulary():
    """To be called in post migration signal handler."""
    class_list = []
    property_list = []
    property_obj = set_attribs(
        role_name_in_db="Property",
        label="Property",
        persistent_id=RDF.Property,
        append_to=class_list,
    )
    class_obj = set_attribs(
        role_name_in_db="Class",
        label="Class",
        persistent_id=RDFS.Class,
        append_to=class_list,
    )
    set_attribs(
        role_name_in_db="Namespace",
        label="Namespace",
        persistent_id=DCAM.VocabularyEncodingScheme,
        append_to=class_list,
    )
    set_attribs(
        role_name_in_db="HELIPORT",
        persistent_id="https://hdl.handle.net/20.500.12865/HELIPORT",
        label="HELIPORT",
        append_to=class_list,
    )
    set_attribs(
        role_name_in_db="Project",
        label="Project",
        persistent_id=FOAF.Project,
        append_to=class_list,
    )

    type_obj = set_attribs(
        role_name_in_db="type",
        label="type",
        persistent_id=RDF.type,
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="suggested_property",
        label="suggested property",
        persistent_id=DCAM.domainIncludes,
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="subnamespace_of",
        label="subnamespace of",
        persistent_id=HELIPORT_NS.subNamespaceOf,
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="subclass_of",
        label="subclass of",
        persistent_id=RDFS.subClassOf,
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="label",
        label="label",
        persistent_id=RDFS.label,
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="description",
        label="description",
        persistent_id=DCTERMS.description,
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="sub_property_of",
        label="subPropertyOf",
        persistent_id=RDFS.subPropertyOf,
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="has_tag",
        label="has Tag",
        persistent_id=URIRef("http://rdfs.org/scot/ns#has_tag"),
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="has_part",
        label="has Part",
        persistent_id=DCTERMS.hasPart,
        append_to=property_list,
    )
    # something is used in a job
    # A is input_of JOB if JOB is described_by_process TOOL which has_input I and A is described_by_parameter I  # noqa: E501
    set_attribs(role_name_in_db="input_of", label="input of", append_to=property_list)
    # input parameter in a workflow description
    set_attribs(
        role_name_in_db="has_input",
        label="hasInput",
        persistent_id=URIRef("http://purl.org/wf4ever/wfdesc#hasInput"),
        append_to=property_list,
    )
    # something is used as the input parameter defined by has_input
    set_attribs(
        role_name_in_db="described_by_parameter",
        label="describedByParameter",
        persistent_id=URIRef("http://purl.org/wf4ever/wfprov#describedByParameter"),
        append_to=property_list,
    )
    # a job is described by some workflow/tool
    set_attribs(
        role_name_in_db="described_by_process",
        label="describedByProcess",
        persistent_id=URIRef("http://purl.org/wf4ever/wfprov#describedByProcess"),
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="primary_topic",
        label="primaryTopic",
        persistent_id=FOAF.primaryTopic,
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="status",
        label="status",
        persistent_id=URIRef("http://purl.org/ontology/bibo/status"),
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="has_identifier",
        label="has Identifier",
        persistent_id=URIRef("http://purl.org/spar/datacite/hasIdentifier"),
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="filename",
        label="filename",
        persistent_id=URIRef(
            "https://www.semanticdesktop.org/ontologies/2007/03/22/nfo/#fileName"
        ),
        append_to=property_list,
    )
    set_attribs(
        role_name_in_db="is_version_of",
        label="isVersionOf",
        persistent_id=DCTERMS.isVersionOf,
        append_to=property_list,
    )

    for p in property_list:
        set_property(p, type_obj, property_obj)
    for c in class_list:
        set_property(c, type_obj, class_obj)
