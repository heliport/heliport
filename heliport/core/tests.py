# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

import datetime
import json
import logging
from html.parser import HTMLParser
from itertools import islice
from types import SimpleNamespace
from typing import Dict, Optional
from unittest import mock
from urllib.parse import urlencode

import pytest
import requests_mock
from django.conf import settings
from django.contrib.auth.models import User
from django.forms import ValidationError
from django.template import Context as TemplateContext
from django.template import Template
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import timezone
from rdflib import BNode, URIRef
from rest_framework import status
from rest_framework.viewsets import ModelViewSet

from heliport.core.forms import validate_orcid
from heliport.core.utils.collections import RootedRDFGraph, first, merge_iterables
from heliport.core.utils.string_tools import (
    format_byte_count,
    format_time,
    parse_date_time,
    remove_prefix,
)
from heliport.core.utils.version import (
    get_project_url,
    heliport_homepage,
    heliport_metadata,
    heliport_version_string,
)

from .digital_object_actions import Action, get_actions
from .digital_object_aspects import File
from .digital_object_interface import GeneralDigitalObject
from .digital_object_resolution import resolve, url_for
from .models import (
    DigitalObject,
    DigitalObjectAttributes,
    DigitalObjectIdentifier,
    DigitalObjectRelation,
    HeliportGroup,
    HeliportUser,
    Image,
    Project,
    Tag,
    Vocabulary,
    assert_relation,
    digital_object_form_label_and_pk,
)
from .serializers import types
from .utils.context import Context, digital_object_breadcrumbs
from .utils.exceptions import AccessDenied, LoginInfoMissing, UserMessage
from .utils.json_ld import LdContext, TargetDescription, reverse_context_mapping
from .utils.queries import (
    at_least_one_group,
    create_empty_project,
    delete_by_id,
    digital_objects_by_type,
    get_or_create_from_label,
    get_or_create_tag,
    tags_of,
)
from .views.api import HeliportModelViewSet, ProjectViewSet
from .vocabulary import SemanticDesktop


class IconsParser(HTMLParser):  # noqa: D101
    def __init__(self, *args, **kwargs):  # noqa: D107
        super().__init__(*args, **kwargs)
        self.found_favicon = False
        self.found_apple_touch_icon = False

    def handle_startendtag(self, tag, attrs):  # noqa: D102
        attributes = dict(attrs)
        if tag == "link" and attributes.get("href"):
            rel = attributes.get("rel")
            if rel == "icon":
                self.found_favicon = True
            elif rel == "apple-touch-icon":
                self.found_apple_touch_icon = True


class WithHeliportUser(TestCase):  # noqa: D101
    def setUp(self):
        """Set up logged-in heliport user."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(
            auth_user=self.auth_user, display_name="HELIPORTUSER321"
        )
        self.client.force_login(self.auth_user)


class WithProject(WithHeliportUser):  # noqa: D101
    def setUp(self):
        """Set up project and logged-in heliport user."""
        super().setUp()
        self.project, is_new = Project.objects.get_or_create(owner=self.user)


class SearchTest(WithHeliportUser):  # noqa: D101
    def test_access_search(self):  # noqa: D102
        response = self.client.get(reverse("core:search_base"))
        self.assertEqual(200, response.status_code)

    def test_search_valid_redirect_urls(self):  # noqa: D102
        response = self.client.get(reverse("core:search_base"))
        self.assertEqual(200, response.status_code)
        urls = response.context_data["search_urls"]
        self.assertIn(reverse("core:search"), urls)
        self.assertGreater(
            len(urls), 1, "modules other than heliport app are not searchable"
        )

        for url in urls:
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

    def test_group_findable(self):  # noqa: D102
        HeliportGroup.objects.create(display_name="GROUP321")
        response = self.client.get(f"{reverse('core:search')}?q=321")
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "GROUP321")

    def test_heliport_user_and_project_findable(self):  # noqa: D102
        Project.objects.create(owner=self.user, label="PROJECT321")
        response = self.client.get(f"{reverse('core:search')}?q=321")
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "HELIPORTUSER321")
        self.assertContains(response, "PROJECT321")

    def test_digital_object_autocomplete(self):  # noqa: D102
        def d(obj):
            return {"label": obj.label, "pk": obj.digital_object_id}

        def q(term=None, **kwargs):
            if term is not None:
                kwargs["term"] = term
            response = self.client.get(
                f"{reverse('core:digital_object_autocomplete')}", kwargs
            )
            self.assertEqual(200, response.status_code)
            return response.json()

        type_prop = Vocabulary().type
        o1 = DigitalObject.objects.create(label="obj1", persistent_id="https://obj1")
        o1_2 = DigitalObject.objects.create(label="obj1_2")
        o2 = Image.objects.create(label="obj2")
        DigitalObject.objects.create(label="obj1", owner=HeliportUser.objects.create())
        DigitalObjectRelation.objects.create(subject=o1, predicate=type_prop, object=o2)
        DigitalObjectRelation.objects.create(
            subject=o1_2, predicate=type_prop, object=o2
        )
        DigitalObjectRelation.objects.create(subject=o2, predicate=type_prop, object=o1)

        self.assertEqualNoOrder([d(o1), d(o1_2)], q(q="obj1"))
        self.assertEqualNoOrder([d(o1), d(o1_2)], q(term="obj1"))
        self.assertEqualNoOrder([d(o1), d(o1_2)], q("Obj1", exact=False))
        self.assertEqual([d(o1)], q("Obj1", exact="case_insensitive"))
        self.assertEqual([], q("Obj1", exact=True))
        self.assertEqual([d(o1_2)], q("obj1_2", type=o2.digital_object_id))
        self.assertEqualNoOrder([d(o1), d(o1_2)], q(type=o2.digital_object_id))
        self.assertEqualNoOrder(
            [d(o1_2), d(o2)],
            self.client.get(
                f"{reverse('core:digital_object_autocomplete')}?q=2"
                f"&type={o1.digital_object_id}&type={o2.digital_object_id}"
            ).json(),
        )
        self.assertEqual([d(o2)], q(type="https://obj1"))
        self.assertEqual([d(o2)], q(type=SemanticDesktop.NFO.Image))
        self.assertEqual([d(o1)], q("https://obj1"))

        o1.deleted = timezone.now()
        o1.save()
        self.assertEqual([d(o1_2)], q("obj1"))

        for i in range(settings.HELIPORT_CORE_AUTOCOMPLETE_COUNT + 3):
            DigitalObject.objects.create(label=f"test_max_count{i}")
        self.assertEqual(
            settings.HELIPORT_CORE_AUTOCOMPLETE_COUNT, len(q("test_max_count"))
        )

        for o in DigitalObject.objects.exclude(
            digital_object_id__in=[o2.digital_object_id, o1_2.digital_object_id]
        ):
            o.deleted = timezone.now()
            o.save()
        self.assertEqualNoOrder([d(o1_2), d(o2)], q())

    def test_landing_page_by_persistent_id(self):  # noqa: D102
        DigitalObject.objects.create(
            label="obj_test_doi",
            persistent_id="https://doi.org/123/456",
            label_is_public=True,
        )
        DigitalObject.objects.create(
            label="obj_test_handle",
            persistent_id="https://hdl.handle.net/234/567",
            generated_persistent_id="xxx123yyy",
            label_is_public=True,
        )

        url = reverse("core:resolve_digital_object_id", args=["123/456"])

        response = self.client.get(url, follow=True)
        content_type = response.headers["Content-Type"]
        self.assertTrue(content_type.startswith("application/ld+json"), content_type)
        self.assertEqual(200, response.status_code)
        self.assertEqual("obj_test_doi", response.json().get("rdfs:label"))

        response = self.client.get(f"{url}?format=landing_page", follow=True)
        content_type = response.headers["Content-Type"]
        self.assertTrue(content_type.startswith("text/html"), content_type)
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "obj_test")

        response = self.client.get(
            reverse("core:resolve_digital_object_id", args=["xxx123yyy"]), follow=True
        )
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "obj_test_handle")

        response = self.client.get(
            reverse("core:resolve_digital_object_id", args=["https://doi.org/123/456"]),
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "obj_test_doi")

        response = self.client.get(
            reverse("core:resolve_digital_object_id", args=["123/45"]), follow=True
        )
        self.assertNotContains(response, "obj_test_doi", status_code=404)

        # check that pid has priority over generated_pid
        for i in range(100):
            if i == 50:
                pid = "https://hdl.handle.net/111/111"
                generated_pid = None
            else:
                generated_pid = "https://hdl.handle.net/111/111"
                pid = None
            DigitalObject.objects.create(
                label=f"digital_object_{i}",
                persistent_id=pid,
                generated_persistent_id=generated_pid,
                label_is_public=True,
            )

        response = self.client.get(
            reverse("core:resolve_digital_object_id", args=["111/111"]), follow=True
        )
        self.assertContains(response, "digital_object_50", status_code=200)

    def assertEqualNoOrder(self, list_goal, list_actual):  # noqa: D102
        missing = [o for o in list_goal if o not in list_actual]
        unexpected = [o for o in list_actual if o not in list_goal]
        self.assertEqual([], missing, "Some output is missing")
        self.assertEqual([], unexpected, "Some output is unexpected")


class FormTest(TestCase):
    """Tests for form validation."""

    def test_orcid_validation_valid_orcid(self):
        """A valid ORCID with full URL passes validation."""
        validate_orcid("https://orcid.org/0000-0002-3145-9880")

    def test_orcid_validation_only_id(self):
        """A valid ORCID without the full URL does not pass validation."""
        with self.assertRaises(ValidationError) as c:
            validate_orcid("0000-0002-3145-9880")
        self.assertEqual(c.exception.code, "orcid_invalid_format")

    def test_orcid_validation_wrong_checksum(self):
        """An ORCID with an invalid checksum does not pass validation."""
        with self.assertRaises(ValidationError) as c:
            # one instead of zero here in last spot
            validate_orcid("https://orcid.org/0000-0002-3145-9881")
        self.assertEqual(c.exception.code, "orcid_invalid_checksum")

    def test_orcid_validation_x_check_digit(self):
        """An ORCID with an X as the check digit passes validation."""
        validate_orcid("https://orcid.org/0000-0001-5556-838X")


class HELIPORTTest(WithHeliportUser):  # noqa: D101
    def setUp(self):  # noqa: D102
        super().setUp()
        self.project, is_new = Project.objects.get_or_create(
            owner=self.user, label="xabac"
        )

    def test_icons_exist_main_page(self):  # noqa: D102
        response = self.client.get(
            reverse("core:index"), follow=True, headers={"Accept": "text/html"}
        )
        self.assertEqual(200, response.status_code)

        parser = IconsParser()
        parser.feed(response.content.decode())

        self.assertTrue(parser.found_favicon, "Main page doesn't have a favicon.")
        self.assertTrue(
            parser.found_apple_touch_icon, "Main page doesn't have an apple-touch-icon."
        )

    def test_icons_exist_landing_page(self):  # noqa: D102
        obj = DigitalObject.objects.create(
            label="obj_test_handle",
            persistent_id="https://hdl.handle.net/123/456",
            generated_persistent_id="xxx123yyy",
            label_is_public=True,
        )
        response = self.client.get(
            reverse("core:landing_page", kwargs={"pk": obj.digital_object_id}),
            headers={"Accept": "text/html"},
        )
        self.assertEqual(200, response.status_code)

        parser = IconsParser()
        parser.feed(response.content.decode())

        self.assertTrue(parser.found_favicon, "Landing page doesn't have a favicon.")
        self.assertTrue(
            parser.found_apple_touch_icon,
            "Landing page doesn't have an apple-touch-icon.",
        )

    def test_maintenance_message_api(self):
        """On maintenance level > 0, the correct level and message should be returned."""  # noqa: E501
        with override_settings(
            HELIPORT_CORE_MAINTENANCE_MESSAGE_LEVEL=1,
            HELIPORT_CORE_MAINTENANCE_MESSAGE="foobar",
        ):
            response = self.client.get(
                reverse("core:maintenance_message"),
                headers={"Accept": "application/json"},
            )
            data = response.json()
            self.assertEqual(data["level"], 1)
            self.assertEqual(data["message"], "foobar")

    def test_no_maintenance_message_api(self):
        """On maintenance level 0, the message in the response should be null."""
        with override_settings(
            HELIPORT_CORE_MAINTENANCE_MESSAGE_LEVEL=0,
            HELIPORT_CORE_MAINTENANCE_MESSAGE="foobar",
        ):
            response = self.client.get(
                reverse("core:maintenance_message"),
                headers={"Accept": "application/json"},
            )
            data = response.json()
            self.assertEqual(data["level"], 0)
            self.assertEqual(data["message"], None)

    def test_maintenance_message_partial(self):
        """On maintenance level > 0, HTMX requests should receive a partial containing the message. The ``div`` should have ``alert`` classes."""  # noqa: E501
        with override_settings(
            HELIPORT_CORE_MAINTENANCE_MESSAGE_LEVEL=1,
            HELIPORT_CORE_MAINTENANCE_MESSAGE="foobar",
        ):
            response = self.client.get(
                reverse("core:maintenance_message"), headers={"HX-Request": "true"}
            )
            self.assertContains(response, 'class="alert alert-')
            self.assertContains(response, "<small>foobar</small>")

    def test_no_maintenance_message_partial(self):
        """On maintenance message level 0, HTMX requests should receive a partial without any code."""  # noqa: E501
        with override_settings(
            HELIPORT_CORE_MAINTENANCE_MESSAGE_LEVEL=0,
            HELIPORT_CORE_MAINTENANCE_MESSAGE="foobar",
        ):
            response = self.client.get(
                reverse("core:maintenance_message"), headers={"HX-Request": "true"}
            )
            self.assertHTMLEqual(response.content.decode(), "")
            self.assertNotContains(response, "<")
            self.assertNotContains(response, ">")

    def test_maintenance_message_no_partial_on_media_type(self):
        """HTMX requests asking for non-html media type should not receive partials."""
        with override_settings(
            HELIPORT_CORE_MAINTENANCE_MESSAGE_LEVEL=1,
            HELIPORT_CORE_MAINTENANCE_MESSAGE="foobar",
        ):
            response = self.client.get(
                reverse("core:maintenance_message"),
                headers={"HX-Request": "true", "Accept": "application/json"},
            )
            data = response.json()
            self.assertEqual(data["level"], 1)
            self.assertEqual(data["message"], "foobar")

    def test_project_graph(self):  # noqa: D102
        response = self.client.get(
            reverse("core:project_graph", kwargs={"project": self.project.pk})
        )
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "xabac")

    def test_simple_api_docu(self):
        """Mirroring the way it is described in projects api view."""
        response = self.client.get("/api/")
        self.assertEqual(200, response.status_code)
        urls = json.loads(response.content.decode())

        response = self.client.get(urls["projects"])
        self.assertEqual(200, response.status_code)

    def test_importing_digital_objects(self):  # noqa: D102
        vocab = Vocabulary()
        import_project_url = reverse(
            "core:import_project", kwargs={"project": self.project.pk}
        )
        other_user = HeliportUser.objects.create(display_name="other_user")
        source_project = Project.objects.create(
            label="source_project", owner=other_user
        )
        obj1 = Image.objects.create(label="obj1")
        obj1.projects.add(source_project)
        obj1_id = obj1.digital_object_id
        obj2 = Image.objects.create(label="obj2")
        obj2.projects.add(source_project)
        obj2_id = obj2.digital_object_id
        obj3 = Image.objects.create(label="obj3")
        obj3.projects.add(source_project)
        obj3_id = obj3.digital_object_id
        import_request_data = {
            "import_objects": [str(obj1_id), str(obj2_id), str(obj3_id)],
            f"import-type-{obj1_id}": "import",
            f"import-type-{obj2_id}": "copy",
            f"import-type-{obj3_id}": "ignore",
        }

        self.assertNotContains(
            self.client.get(f"{import_project_url}?from={source_project.pk}"),
            "obj1",
            status_code=403,
        )
        self.client.post(
            f"{import_project_url}?from={source_project.pk}",
            follow=True,
            data=import_request_data,
        )
        self.assertFalse(self.project.parts.exists())
        self.assertFalse(
            DigitalObjectRelation.objects.filter(predicate=vocab.is_version_of).exists()
        )
        self.assertEqual(3, source_project.parts.count())

        source_project.co_owners.add(self.user)

        self.assertContains(
            self.client.get(f"{import_project_url}?from={source_project.pk}"), "obj1"
        )
        self.assertContains(
            self.client.get(f"{import_project_url}?from={source_project.pk}"), "Image"
        )
        import_response = self.client.post(
            f"{import_project_url}?from={source_project.pk}",
            follow=True,
            data=import_request_data,
        )
        self.assertEqual(200, import_response.status_code)

        self.assertEqual(3, source_project.parts.count())
        self.assertEqual(2, self.project.parts.count())
        obj2_copy_query = self.project.parts.exclude(digital_object_id=obj1_id)
        self.assertEqual(1, obj2_copy_query.count())
        obj2_copy = obj2_copy_query.first().casted_or_self()
        self.assertNotEqual(obj2, obj2_copy)
        self.assertTrue(
            DigitalObjectRelation.objects.filter(
                subject=obj2_copy, predicate=vocab.is_version_of, object=obj2
            ).exists()
        )

    def test_copy_digital_object(self):  # noqa: D102
        vocab = Vocabulary()
        existing_objects = [obj.pk for obj in DigitalObject.objects.all()]
        obj = DigitalObject.objects.create(
            label="digital_object_to_copy", description="123", persistent_id="x"
        )
        DigitalObjectRelation.objects.create(
            subject=obj, predicate=vocab.type, object=vocab.Class
        )
        DigitalObjectAttributes.objects.create(
            subject=obj,
            predicate=vocab.primary_topic,
            value="x",
            special_heliport_role="r",
        )
        obj_copy = obj.create_copy()
        existing_objects.extend([obj_copy.pk, obj.pk])
        self.assertNotEqual(obj, obj_copy)
        self.assertEqual("digital_object_to_copy", obj_copy.label)
        self.assertEqual("123", obj_copy.description)
        self.assertIsNone(obj_copy.persistent_id)
        self.assertIsNone(obj_copy.generated_persistent_id)
        self.assertTrue(
            DigitalObjectRelation.objects.filter(
                subject=obj_copy, predicate=vocab.is_version_of, object=obj
            ).exists()
        )
        self.assertTrue(
            DigitalObjectRelation.objects.filter(
                subject=obj_copy, predicate=vocab.type, object=vocab.Class
            ).exists()
        )
        self.assertTrue(
            DigitalObjectAttributes.objects.filter(
                subject=obj_copy,
                predicate=vocab.primary_topic,
                value="x",
                special_heliport_role="r",
            ).exists()
        )

        im = Image.objects.create(filename="fn", label="test_image")
        im_copy = im.create_copy()
        self.assertNotEqual(im, im_copy)
        self.assertEqual("fn", im_copy.filename)
        self.assertEqual("test_image", im_copy.label)
        existing_objects.extend([im.digital_object_id, im_copy.digital_object_id])

        self.assertFalse(
            DigitalObject.objects.exclude(
                digital_object_id__in=existing_objects
            ).exists()
        )
        for o in existing_objects:
            self.assertTrue(DigitalObject.objects.get(digital_object_id=o))

    def test_tag_view(self):  # noqa: D102
        tag_response = self.client.get(
            reverse("core:tag_list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(status.HTTP_200_OK, tag_response.status_code)
        self.assertNotContains(tag_response, "my_special_tag")
        t = Tag.objects.create(label="my_special_tag")
        t.projects.add(self.project)
        tag_response = self.client.get(
            reverse("core:tag_list", kwargs={"project": self.project.pk})
        )
        self.assertContains(tag_response, "my_special_tag")
        self.assertNotContains(tag_response, "other_tag")

        tag_response = self.client.post(
            reverse("core:tag_list", kwargs={"project": self.project.pk}),
            urlencode(
                {
                    "form_data_object_label_attribute": "other_special_attribute",
                    "form_data_object_id_attribute": "",
                    "form_data_field_label": "other_tag",
                }
            ),
            content_type="application/x-www-form-urlencoded",
            follow=True,
        )
        self.assertEqual(status.HTTP_200_OK, tag_response.status_code)
        self.assertContains(tag_response, "other_tag")
        self.assertContains(tag_response, "other_special_attribute")

        tag_response = self.client.get(
            reverse("core:tag_list", kwargs={"project": self.project.pk})
        )
        self.assertContains(tag_response, "other_special_attribute")
        self.assertContains(tag_response, "other_tag")

    def test_tag_json_view(self):  # noqa: D102
        obj = DigitalObject.objects.create()
        obj.projects.add(self.project)
        tag_response = self.client.get(
            reverse("core:tag_json", kwargs={"project": self.project.pk}),
            {"pk": obj.pk},
        )
        self.assertEqual([], tag_response.json())

        t1 = Tag.objects.create(label="t1")
        t1.projects.add(self.project)
        t2 = Tag.objects.create(label="t2")
        t2.projects.add(self.project)

        t1.set_on(obj)
        tag_response = self.client.get(
            reverse("core:tag_json", kwargs={"project": self.project.pk}),
            {"pk": obj.pk},
        )
        self.assertEqual(["t1"], [t["label"] for t in tag_response.json()])

        tag_response = self.client.post(
            reverse("core:tag_json", kwargs={"project": self.project.pk}),
            {
                "pk": obj.pk,
                "attribute_label": "",
                "attribute_pk": "",
                "value_label": "",
                "value_pk": "",
            },
        )
        self.assertEqual(["new tag", "t1"], [t["label"] for t in tag_response.json()])

        tag_response = self.client.post(
            reverse("core:tag_json", kwargs={"project": self.project.pk}),
            {"pk": obj.pk, "tag": t1.pk, "delete": True},
        )
        self.assertEqual(["new tag"], [t["label"] for t in tag_response.json()])

        t2.requires_type = DigitalObject.objects.create()
        t2.save()
        self.assertFalse(t2.applicable_for(obj))
        tag_response = self.client.post(
            reverse("core:tag_json", kwargs={"project": self.project.pk}),
            {"pk": obj.pk, "tag": t2.pk, "check_type": True},
        )
        self.assertEqual(["new tag"], [t["label"] for t in tag_response.json()])

        assert_relation(obj, Vocabulary().type, t2.requires_type)
        tag_response = self.client.post(
            reverse("core:tag_json", kwargs={"project": self.project.pk}),
            {"pk": obj.pk, "tag": t2.pk, "check_type": True},
        )
        self.assertEqual(["new tag", "t2"], [t["label"] for t in tag_response.json()])

    def test_tag_autocomplete(self):  # noqa: D102
        t1 = Tag.objects.create(label="t1abcxy")
        t2 = Tag.objects.create(
            label="t2abcx", requires_type=DigitalObject.objects.create()
        )
        t3 = Tag.objects.create(label="t3xy")
        t1.projects.add(self.project)
        t2.projects.add(self.project)
        t3.projects.add(self.project)
        t1d = {"label": t1.label, "pk": t1.pk}
        t2d = {"label": t2.label, "pk": t2.pk}
        t3d = {"label": t3.label, "pk": t3.pk}
        with mock.patch(
            "django.utils.timezone.now",
            mock.Mock(
                return_value=datetime.datetime(2022, 1, 1, tzinfo=datetime.timezone.utc)
            ),
        ):
            t1.update_usage_date()
        with mock.patch(
            "django.utils.timezone.now",
            mock.Mock(
                return_value=datetime.datetime(2023, 1, 1, tzinfo=datetime.timezone.utc)
            ),
        ):
            t3.update_usage_date()
        with mock.patch(
            "django.utils.timezone.now",
            mock.Mock(
                return_value=datetime.datetime(2024, 1, 1, tzinfo=datetime.timezone.utc)
            ),
        ):
            t2.update_usage_date()

        self.assertEqual(
            [t2d, t3d, t1d],
            self.client.get(
                reverse("core:tag_autocomplete", kwargs={"project": self.project.pk}),
                {"q": "x"},
            ).json(),
        )

        obj = DigitalObject.objects.create()
        obj.projects.add(self.project)
        self.assertEqual(
            [t3d, t1d],
            self.client.get(
                reverse("core:tag_autocomplete", kwargs={"project": self.project.pk}),
                {"q": "x", "pk": obj.pk},
            ).json(),
        )
        self.assertEqual(
            [t2d],
            self.client.get(
                reverse("core:tag_autocomplete", kwargs={"project": self.project.pk}),
                {"term": "t2", "pk": obj.pk},
            ).json(),
        )
        assert_relation(obj, Vocabulary().type, t2.requires_type)
        self.assertEqual(
            [t2d, t3d, t1d],
            self.client.get(
                reverse("core:tag_autocomplete", kwargs={"project": self.project.pk}),
                {"q": "x", "pk": obj.pk},
            ).json(),
        )

        self.assertEqual(
            [t2d, t1d],
            self.client.get(
                reverse("core:tag_autocomplete", kwargs={"project": self.project.pk}),
                {"q": "abc"},
            ).json(),
        )

    def test_project_independent_tag_autocomplete(self):  # noqa: D102
        tag_not_in_project = Tag.objects.create(label="tnip")
        tag_in_project = Tag.objects.create(label="tip")
        tag_in_project.projects.add(self.project)

        self.assertEqual(
            [{"label": "tip", "pk": tag_in_project.pk}],
            self.client.get(
                reverse("core:tag_autocomplete", kwargs={"project": self.project.pk}),
                {"q": "t", "pk": self.project.digital_object_id},
            ).json(),
        )
        self.assertEqual(
            {"tip"},
            {
                t["label"]
                for t in self.client.get(
                    reverse(
                        "core:tag_autocomplete", kwargs={"project": self.project.pk}
                    ),
                    {
                        "q": "t",
                        "pk": self.project.digital_object_id,
                        "project_independent": "True",
                    },
                ).json()
            },
        )
        other_project = Project.objects.create(owner=self.user)
        tag_not_in_project.projects.add(other_project)
        self.assertEqual(
            {"tip", "tnip"},
            {
                t["label"]
                for t in self.client.get(
                    reverse(
                        "core:tag_autocomplete", kwargs={"project": self.project.pk}
                    ),
                    {
                        "q": "t",
                        "pk": self.project.digital_object_id,
                        "project_independent": "True",
                    },
                ).json()
            },
        )

    def test_tag_is_added_when_tagging_project(self):  # noqa: D102
        tag_not_in_project = Tag.objects.create(label="my_tnip")
        tag_in_project = Tag.objects.create(label="my_tip")
        tag_in_project.projects.add(self.project)

        tag_response = self.client.post(
            reverse("core:tag_json", kwargs={"project": self.project.pk}),
            {"pk": self.project.digital_object_id, "tag": tag_in_project.pk},
        )
        self.assertEqual(["my_tip"], [t["label"] for t in tag_response.json()])

        tag_response = self.client.post(
            reverse("core:tag_json", kwargs={"project": self.project.pk}),
            {"pk": self.project.digital_object_id, "tag": tag_not_in_project.pk},
        )
        self.assertIn("my_tnip", [t["label"] for t in tag_response.json()])

        self.assertEqual(
            {tag_not_in_project, tag_in_project},
            set(Tag.objects.filter(projects=self.project)),
        )

    def test_as_digital_object(self):  # noqa: D102
        from heliport.core.digital_object_resolution import object_types

        object_types.register(DummyResolvable)

        r = self.client.post(
            reverse("core:as_digital_object", kwargs={"project": self.project.pk})
            + "?type=dummy&a=x&b=y"
        )
        self.assertEqual(status.HTTP_200_OK, r.status_code)
        self.assertEqual("dummy", r.json()["label"])

        r = self.client.post(
            reverse("core:as_digital_object", kwargs={"project": self.project.pk}),
            {"type": "dummy", "a": "x", "b": "y"},
        )
        self.assertEqual(status.HTTP_200_OK, r.status_code)
        self.assertEqual("dummy", r.json()["label"])

        r = self.client.post(
            reverse("core:as_digital_object", kwargs={"project": self.project.pk}),
            {"type": "dummy", "a": "x", "b": "z"},
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, r.status_code)
        self.assertEqual("Object not found", r.json()["error"])


class ModelsTest(WithHeliportUser):  # noqa: D101
    def test_user(self):  # noqa: D102
        self.auth_user.email = "mail@mail.com"
        self.assertIsNone(self.user.public_email)
        self.user.email_is_public = True
        self.assertEqual("mail@mail.com", self.user.public_email)
        self.assertIsNone(HeliportUser(email_is_public=True).public_email)

        self.assertIsInstance(self.user.to_rdflib_node(), BNode)

        with pytest.raises(ValidationError):
            # garbage input
            self.user.set_orcid("abc")

        with pytest.raises(ValidationError):
            # wrong check digit
            self.user.set_orcid("https://orcid.org/0000-0002-1825-009X")

        with pytest.raises(ValidationError):
            # not a full URL
            self.user.set_orcid("0000-0002-1825-0097")

        self.user.set_orcid(None)
        self.assertIsNone(self.user.orcid)
        self.user.set_orcid("")
        self.assertIsNone(self.user.orcid)
        self.user.set_orcid("https://orcid.org/0000-0002-1825-0097")
        self.assertEqual(self.user.orcid, "https://orcid.org/0000-0002-1825-0097")

        self.assertIsInstance(self.user.to_rdflib_node(), URIRef)

    def test_digital_object(self):  # noqa: D102
        d = DigitalObject()
        with Context(user=self.user) as context:
            d = d.as_digital_object(context)
        self.assertTrue(d.digital_object_id > 0)

        self.assertTrue(d.as_text())
        d.persistent_id = "abc"
        self.assertEqual("abc", d.as_text())
        d.description = "def"
        self.assertEqual("def", d.as_text())
        d.label = "xyz"
        self.assertEqual("xyz", d.as_text())

        self.assertIsInstance(d.as_rdf().root, URIRef)
        d.persistent_id = None
        self.assertIsInstance(d.as_rdf(), RootedRDFGraph)

        self.assertIn("xyz", d.as_html())

        self.assertEqual(self.user, d.viewing_user)

        project = Project.objects.create(owner=self.user)
        d.projects.add(project)
        other_user = HeliportUser.objects.create()
        with Context(other_user) as context:
            self.assertRaises(AccessDenied, d.access(context).assert_permission)
            self.assertRaises(AccessDenied, d.access(context).assert_read_permission)

        with Context(self.user) as context:
            self.assertIsNone(d.access(context).assert_permission())

        d.save()
        self.assertEqual(d, digital_object_form_label_and_pk("", d.digital_object_id))

    def test_project(self):  # noqa: D102
        self.assertIsNone(Project().last_modified_aggregate())

        dates = [
            datetime.datetime(1800 + i, 1, 1, tzinfo=datetime.timezone.utc)
            for i in range(20)
        ]
        with mock.patch("django.utils.timezone.now", mock.Mock(return_value=dates[4])):
            project = Project.objects.create()
            self.assertEqual(dates[4], project.last_modified_aggregate())
            image = Image.objects.create()
            digital_object = DigitalObject.objects.create()

        def set_last_modified(obj: DigitalObject, date):
            with mock.patch("django.utils.timezone.now", mock.Mock(return_value=date)):
                obj.save()

        set_last_modified(image, dates[5])
        image.projects.add(project)
        self.assertEqual(dates[5], project.last_modified_aggregate())

        set_last_modified(digital_object, dates[6])
        digital_object.projects.add(project)
        self.assertEqual(dates[6], project.last_modified_aggregate())

        set_last_modified(project, dates[7])
        self.assertEqual(dates[7], project.last_modified_aggregate())

    def test_tag(self):  # noqa: D102
        vocab = Vocabulary()
        t = Tag()
        t.html_color = "#s"
        self.assertEqual(3, len(t.rgb_color))
        t.html_color = "#00FF0A"
        self.assertEqual((0, 255, 10), t.rgb_color)
        self.assertEqual("Anything", t.requires_type_display)
        t.requires_type = DigitalObject.objects.create(label="x")
        self.assertIn("x", str(t.requires_type_display))

        self.assertEqual((vocab.has_tag, t), t.get_key_value())
        obj = DigitalObject.objects.create()
        self.assertFalse(t.applicable_for(obj))
        assert_relation(obj, vocab.type, t.requires_type)
        self.assertTrue(t.applicable_for(obj))


class APITest(WithHeliportUser):  # noqa: D101
    def test_digital_object_api(self):  # noqa: D102
        # api should be available at this path even when not logged in
        self.client.logout()
        result = self.client.get("/api/digital-objects/complete", follow=True)
        self.assertEqual(200, result.status_code)

    def test_deletion(self):  # noqa: D102
        self.assertFalse(Project.objects.exists())
        new_project = Project.objects.create(owner=self.user, label="to delete project")
        self.assertIsNone(new_project.deleted)
        response = self.client.delete(f"/api/projects/{new_project.pk}/")
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertTrue(Project.objects.exists())
        new_project.refresh_from_db()
        self.assertIsNotNone(new_project.deleted)

        # ensure all DigitalObject ViewSets are HELIPORT ViewSets like project ViewSet
        view_set_to_model_path = ["serializer_class", "Meta", "model"]
        self.assertEqual(
            Project, self.getattr_path(ProjectViewSet, view_set_to_model_path)
        )
        self.assertTrue(issubclass(Project, DigitalObject))
        invalid = []
        valid = []
        other_count = 0
        for model_view_set in self.subclasses_recursive(ModelViewSet):
            the_model = self.getattr_path(model_view_set, view_set_to_model_path)
            if the_model is not None and issubclass(the_model, DigitalObject):
                description = f"{model_view_set.__name__} of {the_model.__name__}"
                if issubclass(model_view_set, HeliportModelViewSet):
                    valid.append(description)
                else:
                    invalid.append(description)
            else:
                other_count += 1

        self.assertEqual(
            [],
            invalid,
            f"{len(invalid)}/{len(invalid) + len(valid)} (ignored {other_count}) are invalid\n"  # noqa: E501
            f"Digital Object ModelViewSets must be subclasses of {HeliportModelViewSet.__name__}",  # noqa: E501
        )

    @staticmethod
    def getattr_path(obj, name_path, default=None):  # noqa: D102
        for name in name_path:
            obj = getattr(obj, name, default)
        return obj

    def subclasses_recursive(self, the_class):  # noqa: D102
        yield the_class
        for subclass in the_class.__subclasses__():
            yield from self.subclasses_recursive(subclass)

    def test_token_auth(self):  # noqa: D102
        # Note that these absolute urls may be used in HELIPORT:
        token_response = self.client.post("/api/auth/login/")
        token1 = token_response.json()["token"]
        token_response = self.client.post("/api/auth/login/")
        token2 = token_response.json()["token"]
        self.assertEqual(status.HTTP_200_OK, token_response.status_code)
        self.assertEqual(
            status.HTTP_204_NO_CONTENT,
            self.client.post(
                "/api/auth/logout/", headers={"Authorization": f"Token {token1}"}
            ).status_code,
        )
        self.assertEqual(
            status.HTTP_401_UNAUTHORIZED,
            self.client.post(
                "/api/auth/logout/", headers={"Authorization": f"Token {token1}"}
            ).status_code,
        )
        self.assertEqual(
            status.HTTP_204_NO_CONTENT,
            self.client.post(
                "/api/auth/logoutall/", headers={"Authorization": f"Token {token2}"}
            ).status_code,
        )

    def test_users(self):  # noqa: D102
        user_response = self.client.get("/api/users/")
        self.assertEqual(
            status.HTTP_200_OK,
            user_response.status_code,
        )
        self.client.logout()
        self.assertEqual(
            status.HTTP_401_UNAUTHORIZED,
            self.client.get("/api/users/").status_code,
        )

    def test_projects(self):  # noqa: D102
        project_response = self.client.get("/api/projects/")
        self.assertEqual(
            status.HTTP_200_OK,
            project_response.status_code,
        )
        self.client.logout()
        self.assertEqual(
            status.HTTP_401_UNAUTHORIZED,
            self.client.get("/api/projects/").status_code,
        )

        # this should work in the future:
        # user_without_heliport_user = User.objects.create()
        # self.client.force_login(user_without_heliport_user)
        # self.assertEqual(
        #     status.HTTP_401_UNAUTHORIZED,
        #     self.client.get("/api/projects/").status_code,
        # )


class CoreVocabularyTest(TestCase):  # noqa: D101
    def test_vocabularies_exist_and_are_property_or_class(self):  # noqa: D102
        vocab = Vocabulary()
        for voc in dir(vocab):
            if not voc.startswith("_"):
                obj = getattr(vocab, voc)
                self.assertIsNotNone(obj.label)
                property_or_class = vocab.Class in types(
                    obj
                ) or vocab.Property in types(obj)
                if not property_or_class:
                    self.fail(
                        f"{obj} is not a property or class. types are: {types(obj)}"
                    )

    def test_query_by_type(self):  # noqa: D102
        vocab = Vocabulary()
        p1 = Project.objects.create()
        p2 = Project.objects.create()
        i1o = Image.objects.create()
        i2o = Image.objects.create()
        t = DigitalObject.objects.create()
        i1 = DigitalObject.objects.get(digital_object_id=i1o.digital_object_id)
        i2 = DigitalObject.objects.get(digital_object_id=i2o.digital_object_id)

        self.assertEqual(
            [{p1, p2}], list(map(set, digital_objects_by_type(vocab.Project)))
        )
        DigitalObjectRelation.objects.create(
            subject=i1, predicate=vocab.type, object=vocab.Project
        )
        self.assertEqual(
            [{i1}, {p1, p2}], list(map(set, digital_objects_by_type(vocab.Project)))
        )
        self.assertEqual(
            [{i1}, {p1, p2}],
            list(map(set, digital_objects_by_type(vocab.Project.digital_object_id))),
        )
        self.assertEqual(
            [{i1}, {p1, p2}],
            list(
                map(set, digital_objects_by_type(str(vocab.Project.digital_object_id)))
            ),
        )
        self.assertEqual(
            [{i1}, {p1, p2}],
            list(map(set, digital_objects_by_type(vocab.Project.persistent_id))),
        )
        self.assertEqual([], digital_objects_by_type("https://abc"))

        t.persistent_id = vocab.Project.persistent_id
        t.save()
        DigitalObjectRelation.objects.create(subject=i2, predicate=vocab.type, object=t)

        self.assertEqual(
            [{i1}, {p1, p2}], list(map(set, digital_objects_by_type(vocab.Project)))
        )
        self.assertEqual(
            [{i1, i2}, {p1, p2}],
            list(map(set, digital_objects_by_type(vocab.Project.persistent_id))),
        )
        self.assertEqual(
            [{i1o, i2o}],
            list(map(set, digital_objects_by_type(SemanticDesktop.NFO.Image))),
        )


class LdJsonContextTest(TestCase):  # noqa: D101
    def test_basic_set(self):  # noqa: D102
        context = LdContext()
        a = {}
        b = {}
        context.tentative_set(a, "x1", 1, "https://x")
        context.tentative_set(a, "y1", 2, "https://y")
        context.tentative_set(a, "y2", 3, "https://y")
        context.tentative_set(b, "x1", 4, "https://x")
        context.tentative_set(b, "y1", 5, "https://y")
        context.tentative_set(b, "y2", 6, "https://y")
        self.assertEqual({"x1": 1, "y1": 2, "y2": 3}, a)
        self.assertEqual({"x1": 4, "y1": 5, "y2": 6}, b)
        self.assertEqual(
            {"x1": "https://x", "y1": "https://y", "y2": "https://y"},
            context.get_context_dict(),
        )

    def test_unique_keys(self):  # noqa: D102
        context = LdContext()
        a = {}
        b = {}
        context.tentative_set(a, "x", 1, "https://x0")
        context.tentative_set(a, "x", 2, "https://x1")
        context.tentative_set(b, "x", 3, "https://x1")
        context.tentative_set(b, "x", 4, "https://x0")
        context.tentative_set(a, "x", 5, "https://x1")

        # note https://x1 comes alphabetically before https://x2
        self.assertEqual({"x": 1, "x_1": 5}, a)
        self.assertEqual({"x": 4, "x_1": 3}, b)
        self.assertEqual(
            {"x": "https://x0", "x_1": "https://x1"}, context.get_context_dict()
        )

    def test_rename_keys(self):  # noqa: D102
        context = LdContext()
        a = {}
        b = {}
        context.tentative_set(a, "x", 1, "https://x1")
        context.tentative_set(b, "x", 2, "https://x1")
        self.assertEqual({"x": 1}, a)
        self.assertEqual({"x": 2}, b)
        # renames old x to x1 because https://x0 comes alphabetically before https://x1:
        context.tentative_set(b, "x", 3, "https://x0")
        context.tentative_set(a, "x", 4, "https://x0")
        context.tentative_set(a, "x", 5, "https://x1")

        self.assertEqual({"x": 4, "x_1": 5}, a)
        self.assertEqual({"x": 3, "x_1": 2}, b)
        self.assertEqual(
            {"x": "https://x0", "x_1": "https://x1"}, context.get_context_dict()
        )

    def test_chained_renaming(self):  # noqa: D102
        context = LdContext()
        a = {}
        context.tentative_set(a, "x_2", 3, "https://x3")
        context.tentative_set(a, "x", 2, "https://x2")
        # renames x -> x1 because "x1" < "x2":
        context.tentative_set(a, "x", 1, "https://x1")
        # renames x3 -> x31; x1 -> x2; x -> x1 because "x0" < "x1" < "x2" < "x3":
        context.tentative_set(a, "x", 0, "https://x0")

        self.assertEqual({"x": 0, "x_1": 1, "x_2": 2, "x_2_1": 3}, a)

    def test_add_to_end_of_chain(self):  # noqa: D102
        context = LdContext()
        a = {}
        context.tentative_set(a, "x_2", 3, "https://x3")
        context.tentative_set(a, "x", 0, "https://x0")
        # adds x as x1
        context.tentative_set(a, "x", 1, "https://x1")
        # add x as x2 and renames x2 -> x21
        context.tentative_set(a, "x", 2, "https://x2")

        self.assertEqual({"x": 0, "x_1": 1, "x_2": 2, "x_2_1": 3}, a)
        self.assertEqual(
            {
                "x": "https://x0",
                "x_1": "https://x1",
                "x_2": "https://x2",
                "x_2_1": "https://x3",
            },
            context.get_context_dict(),
        )

    def test_match_context(self):  # noqa: D102
        target1 = TargetDescription("a", "https://a", 1)
        self.assertEqual(set(), set(reverse_context_mapping([], {})))
        self.assertEqual(set(), set(reverse_context_mapping([target1], {})))
        self.assertEqual(set(), set(reverse_context_mapping([], {"a": 1})))

        self.assertEqual(
            {(1, -1), (2, -2), (3, -3)},
            set(
                reverse_context_mapping(
                    [
                        TargetDescription("a", "https://a", 1),
                        TargetDescription("b", "https://b", 2),
                        TargetDescription("c_1", "https://c", 3),
                    ],
                    {"a": -1, "b_2": -2, "c_1_3": -3},
                )
            ),
        )

        self.assertEqual(
            {(1, -1), (2, -2), (3, -3), (4, -4)},
            set(
                reverse_context_mapping(
                    [
                        TargetDescription("a", "https://a0", 1),
                        TargetDescription("a", "https://a1", 2),
                        TargetDescription("a_1", "https://a2", 3),
                        TargetDescription("a", "https://a3", 4),
                    ],
                    {"a": -1, "a_1": -2, "a_1_1": -3, "a_2": -4},
                )
            ),
        )

        self.assertEqual(
            {(1, -1), (2, -2), (3, -3)},
            set(
                reverse_context_mapping(
                    [
                        TargetDescription("a", "https://a2", 3),
                        TargetDescription("a", "https://a1", 2),
                        TargetDescription("a_1", "https://a0", 1),
                    ],
                    {"a": -2, "a_2": -3, "a_1": -1},
                )
            ),
        )

    def test_reverse_compatible_with_generation(self):  # noqa: D102
        context = LdContext()
        a = {}
        b = {}

        context.tentative_set(b, "a", 0, "https://a0")
        context.tentative_set(a, "a", 1, "https://a1")
        context.tentative_set(a, "a_1", 2, "https://a2")
        context.tentative_set(a, "a_1", 3, "https://a3")
        context.tentative_set(a, "b", 4, "https://b")

        self.assertEqual({"a": 0}, b)
        self.assertEqual({"a_1": 1, "a_1_1": 2, "a_1_2": 3, "b": 4}, a)

        self.assertEqual(
            {(-1, 1), (-2, 2), (-3, 3), (dict, 4)},
            set(
                reverse_context_mapping(
                    [
                        TargetDescription("a", "https://a1", -1),
                        TargetDescription("a_1", "https://a2", -2),
                        TargetDescription("a_1", "https://a3", -3),
                        TargetDescription("b", "https://b", dict),
                    ],
                    a,
                )
            ),
        )


class DummyContext:  # noqa: D101
    def __init__(self):  # noqa: D107
        self.state = "Build"

    def __enter__(self):  # noqa: D105
        self.state = "Open"

    def __exit__(self, exc_type, exc_val, exc_tb):  # noqa: D105
        self.state = "Closed"

    def generate(self, unused_context):  # noqa: D102
        return self


class UtilsTest(TestCase):  # noqa: D101
    def test_manually_closing_context(self):  # noqa: D102
        def create_context(to_close, error=False, close=False):
            with Context() as c:
                _ = c[to_close]
                if error:
                    raise ValueError("Some example error")
                c.activate_manual_closing()
                if close:
                    c.close()
                return c

        ctx = DummyContext()
        self.assertEqual("Build", ctx.state)
        with create_context(ctx):
            self.assertEqual("Open", ctx.state)
        self.assertEqual("Closed", ctx.state)

        ctx = DummyContext()
        self.assertEqual("Build", ctx.state)
        self.assertRaises(ValueError, create_context, ctx, error=True)
        self.assertEqual("Closed", ctx.state)

        ctx = DummyContext()
        self.assertEqual("Build", ctx.state)
        create_context(ctx, close=True)
        self.assertEqual("Closed", ctx.state)

    def test_context(self):  # noqa: D102
        context = Context()
        self.assertRaises(AssertionError, lambda: context[DummyContext()])
        self.assertRaises(AssertionError, lambda: context.user)
        self.assertEqual("user", Context(user="user").user)
        with context:
            dummy = DummyContext()
            context.cache(dummy, 1)
            self.assertEqual(1, context[dummy])
            self.assertEqual("Build", dummy.state)

    def test_digital_object_breadcrumb(self):  # noqa: D102
        session = self.client.session

        def breadcrumb(label, link, reset=False):
            get_params = {} if reset else {"breadcrumb": True}
            result = digital_object_breadcrumbs(session, get_params, label, link)
            return [tuple(b) for b in result]

        self.assertEqual([("a", "al")], breadcrumb("a", "al"))
        self.assertEqual([("a", "al"), ("b", "bl")], breadcrumb("b", "bl"))
        self.assertEqual([("a", "al"), ("b", "bl")], breadcrumb("b", "bl"))
        self.assertEqual([("a", "al")], breadcrumb("a", "al"))
        self.assertEqual([("c", "cl")], breadcrumb("c", "cl", reset=True))

    def test_exceptions(self):  # noqa: D102
        self.assertEqual("abc", UserMessage("abc").message)
        self.assertEqual("abc", LoginInfoMissing("abc").message)
        self.assertEqual("abc", AccessDenied("abc").message)
        self.assertTrue(AccessDenied().message)

    def test_first(self):  # noqa: D102
        self.assertIsNone(first([]))
        self.assertEqual(32, first(range(32, 44, 1)))

    def test_merge_iterables(self):  # noqa: D102
        ab_generator = merge_iterables("A" * 100, "BBB", block_size=2)
        ab_max_len_10 = islice(ab_generator, 10)
        self.assertEqual("BBB", "".join(a for a in ab_max_len_10 if a != "A"))

    def test_remove_prefix(self):  # noqa: D102
        self.assertRaises(ValueError, remove_prefix, "abc", "def", raise_on_error=True)

    def test_parse_date_time(self):  # noqa: D102
        self.assertIsNone(parse_date_time(None))
        self.assertIsNone(parse_date_time("sjdjfiejfoij"))
        self.assertEqual(datetime.datetime(1901, 10, 9), parse_date_time("9. 10. 1901"))

    def test_format_byte_count(self):  # noqa: D102
        self.assertEqual("5 Bytes", format_byte_count(5))
        self.assertEqual("5 KiB", format_byte_count(5 * 1024**1))
        self.assertEqual("5 MiB", format_byte_count(5 * 1024**2))
        self.assertEqual("5 PiB", format_byte_count(5 * 1024**5))
        self.assertEqual("5120 PiB", format_byte_count(5 * 1024**6))

    def test_format_time(self):  # noqa: D102
        offset = (
            timezone.get_current_timezone()
            .utcoffset(datetime.datetime(1970, 1, 1))
            .seconds
        )
        self.assertEqual("1970-01-02 00:00:00", format_time(24 * 60 * 60 - offset))
        self.assertEqual("2000-11-10 22:33:00", format_time("2000-11-10 22-33"))
        self.assertEqual("1970-03-04 00:00:00", format_time("4. 3. 1970"))
        self.assertEqual("2022-06-24 10:03:20", format_time("2022-06-24 10:03:20"))
        self.assertEqual("invalid date", format_time("xxxxxx"))


class QueryTest(WithHeliportUser):  # noqa: D101
    def test_create_project(self):  # noqa: D102
        dummy_request = SimpleNamespace()
        dummy_request.user = self.auth_user
        authentication = SimpleNamespace()
        authentication.get_group = lambda user: "abc_group"
        project = create_empty_project(dummy_request, lambda: authentication)
        self.assertIsInstance(project, Project)
        self.assertEqual(self.user, project.owner)
        self.assertEqual("abc_group", project.group.display_name)

    def test_group(self):  # noqa: D102
        with at_least_one_group():
            self.assertTrue(HeliportGroup.objects.exists())

    def test_tags(self):  # noqa: D102
        # create tags
        project = Project.objects.create(owner=self.user)
        vocab = Vocabulary()
        t1 = get_or_create_tag(project, None, "t1", "a1", None, "v1", None)
        t2 = get_or_create_tag(project, None, "t2")
        t22 = get_or_create_tag(project, None, "t2")
        self.assertEqual(t2, t22)
        a1 = get_or_create_from_label("a1", None, None)
        v1 = get_or_create_from_label("v1", None, None)

        # test tag properties
        d1 = t1.as_dict
        d2 = t2.as_dict
        self.assertTrue(d1["color"].startswith("#"))
        self.assertTrue(d2["color"].startswith("#"))
        self.assertTrue(d1["foreground"].startswith("#"))
        self.assertTrue(d2["foreground"].startswith("#"))
        del d1["color"]
        del d2["color"]
        del d1["foreground"]
        del d2["foreground"]
        self.assertEqual(
            {
                "attribute": a1.digital_object_id,
                "label": "t1",
                "requires_type": None,
                "tag_id": 1,
                "value": v1.digital_object_id,
            },
            d1,
        )
        self.assertEqual(
            {
                "attribute": vocab.has_tag.digital_object_id,
                "label": "t2",
                "requires_type": None,
                "tag_id": 2,
                "value": t2.digital_object_id,
            },
            d2,
        )

        # get and set
        self.assertEqual(set(), tags_of(project, project))
        t1.set_on(project)
        self.assertEqual({t1}, tags_of(project, project))
        t2.set_on(project)
        t1.unset_on(project)
        self.assertEqual({t2}, tags_of(project, project))
        t2.unset_on(project)
        self.assertEqual(set(), tags_of(project, project))
        t1.set_on(project)
        t2.set_on(project)
        self.assertEqual({t1, t2}, tags_of(project, project))

    def test_deletion(self):  # noqa: D102
        p = Project.objects.create(owner=self.user)
        self.assertIsNone(p.deleted)
        delete_by_id(str(p.digital_object_id), user=self.user)
        p = Project.objects.get(pk=p.pk)
        self.assertIsNotNone(p.deleted)


class DummyResolvable(GeneralDigitalObject):  # noqa: D101
    @staticmethod
    def type_id() -> str:  # noqa: D102
        return "dummy"

    def get_identifying_params(self) -> Dict[str, str]:  # noqa: D102
        return {"type": self.type_id(), "a": "x", "b": "y"}

    @classmethod
    def resolve(  # noqa: D102
        cls, params: Dict[str, str], context: Context
    ) -> Optional[GeneralDigitalObject]:
        assert params["type"] == cls.type_id()
        if params["a"] == "x" and params["b"] == "y":
            return DummyResolvable()
        return None

    def as_digital_object(self, context: Context) -> DigitalObject:  # noqa: D102
        return DigitalObject.objects.create(label="dummy")

    def as_text(self) -> str:  # noqa: D102
        return "dummy"

    def as_rdf(self) -> RootedRDFGraph:  # noqa: D102
        return RootedRDFGraph.from_atomic("dummy")

    def as_html(self) -> str:  # noqa: D102
        return "dummy"

    def __hash__(self):  # noqa: D105
        return 0

    def __eq__(self, other):  # noqa: D105
        return isinstance(other, DummyResolvable)


class TemplateRenderingTest(TestCase):  # noqa: D101
    def test_zip(self):  # noqa: D102
        t = Template(
            """
            {% load heliport %}
            {% for a, b in al|zip:bl %}{{ a }};{{ b }} {% endfor %}
        """
        )
        c = TemplateContext({"al": ["a", "b", "c"], "bl": range(3)})
        self.assertEqual("a;0 b;1 c;2", t.render(c).strip())

        t = Template(
            """
            {% load heliport %}
            {% for a, b in al|zip_dict:bv %}{{ a }};{{ b }} {% endfor %}
        """
        )
        c = TemplateContext({"al": ["a", "b", "c"], "bv": 1})
        self.assertEqual("a;1 b;None c;None", t.render(c).strip())

        t = Template(
            """
            {% load heliport %}
            {% for a, b in al|zip_dict:bd %}{{ a }};{{ b }} {% endfor %}
        """
        )
        c = TemplateContext({"al": ["a", "b", "c"], "bd": {1: 4, 2: 5}})
        self.assertEqual("a;None b;4 c;5", t.render(c).strip())

    def test_query_params(self):  # noqa: D102
        t = Template(
            """
            {% load heliport %}
            https://www.example.com/abc{% url_query a=1 b=x c=y d=not_defined %}
        """
        )
        c = TemplateContext({"x": r"a/\?", "y": None})
        self.assertEqual(
            "https://www.example.com/abc?a=1&amp;b=a%2F%5C%3F&amp;d=",
            t.render(c).strip(),
        )
        t = Template("{% load heliport %}x{% url_query b=n %}x")
        c = TemplateContext({"n": None})
        self.assertEqual("xx", t.render(c).strip())

        t = Template("{% load heliport %}abc{% params_for obj b=3 %}")
        obj = DigitalObject.objects.create()
        c = TemplateContext({"obj": obj})
        self.assertEqual(f"abc?digital_object_id={obj.pk}&amp;b=3", t.render(c).strip())

        t = Template("{% load heliport %}abc{% params_for obj f=3 %}")
        obj = DummyResolvable()
        c = TemplateContext({"obj": obj})
        self.assertEqual("abc?type=dummy&amp;a=x&amp;b=y&amp;f=3", t.render(c).strip())

    def test_isinstance(self):  # noqa: D102
        t = Template("{% load heliport %}abc{% if obj|is_digital_object %}x{% endif %}")
        obj = DigitalObject.objects.create()
        c = TemplateContext({"obj": obj})
        self.assertEqual("abcx", t.render(c).strip())

        t = Template("{% load heliport %}abc{% if obj|is_digital_object %}x{% endif %}")
        c = TemplateContext({"obj": DummyResolvable()})
        self.assertEqual("abc", t.render(c).strip())

    def test_type(self):  # noqa: D102
        t = Template("{% load heliport %}abc{{ x|type_name }}x")
        c = TemplateContext({"x": DummyResolvable()})
        self.assertEqual("abcDummyResolvablex", t.render(c).strip())

    def test_repr(self):  # noqa: D102
        t = Template("{% load heliport %}abc{{ x|repr }}x")
        c = TemplateContext({"x": "abc"})
        self.assertEqual("abc&#x27;abc&#x27;x", t.render(c).strip())


class DummyAction(Action):  # noqa: D101
    name = "dummy"
    icon = "fa-kiwi-bird"
    link = "dummy/action"
    applicable = True


def build_action(obj, project, user):  # noqa: D103
    action = DummyAction(obj, project, user)
    action.self_made = True
    return [action]


class DummyFile(File):  # noqa: D101
    mimetype = "text/text"

    def __init__(self):  # noqa: D107
        self.status = "Build"

    def read(self, number_of_bytes=None) -> bytes:  # noqa: D102
        return b""

    def open(self):  # noqa: D102
        self.status = "Open"

    def close(self):  # noqa: D102
        self.status = "Closed"

    def size(self) -> Optional[int]:  # noqa: D102
        return 0

    def get_file_info(self) -> Dict[str, str]:  # noqa: D102
        return {"file_property": "nice_file_value"}


class DigitalObjectInterfaceTest(WithHeliportUser):  # noqa: D101
    def test_actions(self):  # noqa: D102
        project = Project.objects.create(owner=self.user)
        from heliport.core.digital_object_actions import actions

        actions.register(DummyAction)
        actions.register(build_action)
        project_actions = get_actions(project, project, self.user)

        self.assertEqual(2, len(project_actions))
        self.assertEqual(
            1, len([a for a in project_actions if hasattr(a, "self_made")])
        )
        self.assertTrue(all(isinstance(a, DummyAction) for a in project_actions))
        self.assertEqual({project}, {a.project for a in project_actions})
        self.assertEqual({project}, {a.obj for a in project_actions})
        self.assertEqual({self.user}, {a.user for a in project_actions})

        self.assertEqual(2, len(DummyResolvable().actions(self.user, project)))

    def test_file(self):  # noqa: D102
        f = DummyFile()
        self.assertEqual("Build", f.status)
        with f:
            self.assertEqual("Open", f.status)
        self.assertEqual("Closed", f.status)

    def test_resolution(self):  # noqa: D102
        from heliport.core.digital_object_resolution import object_types

        object_types.register(DummyResolvable)

        r = DummyResolvable()
        with Context() as context:
            self.assertIsNot(r, r.as_digital_object_if_exists(context))
            self.assertEqual(r, r.as_digital_object_if_exists(context))
            o = r.as_digital_object(context)
            self.assertIs(o, o.as_digital_object_if_exists(context))
            self.assertEqual(o, o.as_digital_object(context))

            self.assertEqual(o, resolve({"digital_object_id": o.pk}, context))
            self.assertEqual(r, resolve(r.get_identifying_params(), context))

            def test_logging(resolution_parameters, log):
                with self.assertLogs(level=logging.DEBUG) as log_output:
                    self.assertIsNone(resolve(resolution_parameters, context))
                    self.assertIn(log, log_output.output[0])

            test_logging(
                {"digital_object_id": "-2"},
                "digital object id -2 does not exist",
            )
            test_logging(
                {"type": "not_registered"},
                'type not registered: "not_registered"',
            )
            test_logging({}, "resolution failed: no type specified")
            test_logging({"type": "dummy", "a": "x", "b": "z"}, "resolution failed")

            self.assertIsInstance(
                resolve({"type": "dummy", "a": "x", "b": "y"}, context), DummyResolvable
            )

        self.assertEqual("/login/?type=dummy&a=x&b=y", url_for("core:login", r))
        self.assertEqual(
            "/accounts/login/?type=dummy&a=x&b=y", url_for("account_login", r)
        )


class DigitalObjectIdentifierTest(TestCase):
    """Tests for :class:`DigitalObjectIdentifier`."""

    def test_heliport_identifier_created(self):
        """Test automatic creation of ``heliport``-scheme identifier."""
        do = DigitalObject(label="obj1")
        do.namespace_str = "foo/bar"
        do.save()
        identifier = DigitalObjectIdentifier.objects.filter(
            digital_object=do, scheme="heliport"
        ).first()
        self.assertTrue(str(identifier).startswith("foo.bar."))

    def test_preferred_identifier(self):
        """Test priority rules in preferred_identifier method."""
        do = DigitalObject(label="obj1")
        do.namespace_str = "foo/bar"
        do.save()

        pid = do.preferred_identifier()
        self.assertEqual(pid.digital_object, do)
        self.assertFalse(pid.immutable)
        self.assertEqual(pid.identifier, f"foo.bar.{do.digital_object_id}")
        self.assertEqual(pid.scheme, "heliport")

        DigitalObjectIdentifier.objects.create(
            digital_object=do,
            scheme="url",
            identifier="https://heliport.example.com/do",
        )

        pid = do.preferred_identifier()
        self.assertFalse(pid.immutable)
        self.assertEqual(pid.identifier, "https://heliport.example.com/do")
        self.assertEqual(pid.scheme, "url")

        DigitalObjectIdentifier.objects.create(
            digital_object=do,
            scheme="hdl",
            identifier="https://hdl.handle.net/20.500.99999/do",
        )

        pid = do.preferred_identifier()
        self.assertFalse(pid.immutable)
        self.assertEqual(pid.identifier, "https://hdl.handle.net/20.500.99999/do")
        self.assertEqual(pid.scheme, "hdl")

        DigitalObjectIdentifier.objects.create(
            digital_object=do,
            scheme="doi",
            identifier="https://doi.org/20.500.99999/do",
        )

        pid = do.preferred_identifier()
        self.assertFalse(pid.immutable)
        self.assertEqual(pid.identifier, "https://doi.org/20.500.99999/do")
        self.assertEqual(pid.scheme, "doi")

        # Immutable has priority over non-immutable
        DigitalObjectIdentifier.objects.create(
            digital_object=do,
            scheme="url",
            identifier="https://heliport.example.com/do2",
            immutable=True,
        )

        pid = do.preferred_identifier()
        self.assertTrue(pid.immutable)
        self.assertEqual(pid.identifier, "https://heliport.example.com/do2")
        self.assertEqual(pid.scheme, "url")

        DigitalObjectIdentifier.objects.all().delete()

        # Fallback to any other identifier
        DigitalObjectIdentifier.objects.create(
            digital_object=do,
            scheme="unknown-type",
            identifier="https://heliport.example.com/do3",
        )

        pid = do.preferred_identifier()
        self.assertFalse(pid.immutable)
        self.assertEqual(pid.identifier, "https://heliport.example.com/do3")
        self.assertEqual(pid.scheme, "unknown-type")

    def test_digital_object_set_identifier(self):
        """Test DigitalObjectIdentifier model instances created in set_identifier."""
        do = DigitalObject(label="obj1")
        do.namespace_str = "a/b"
        do.save()

        with requests_mock.Mocker():
            # no requests should be performed
            do.set_identifier("https://doi.org/20.500.99999/foo.1")
        self.assertEqual(do.persistent_id, "https://doi.org/20.500.99999/foo.1")
        identifier: DigitalObjectIdentifier = do.identifiers.filter(
            scheme="doi"
        ).first()
        self.assertTrue(identifier.immutable)
        self.assertEqual(identifier.url, "https://doi.org/20.500.99999/foo.1")
        self.assertEqual(identifier.identifier, "20.500.99999/foo.1")
        self.assertEqual(identifier.display_text, "20.500.99999/foo.1")

        with requests_mock.Mocker():
            # no requests should be performed
            do.set_identifier("https://heliport.example.com/foo.1")
        self.assertEqual(do.persistent_id, "https://heliport.example.com/foo.1")
        identifier = do.identifiers.filter(scheme="url").first()
        self.assertTrue(identifier.immutable)
        self.assertEqual(identifier.url, "https://heliport.example.com/foo.1")
        self.assertEqual(identifier.identifier, "https://heliport.example.com/foo.1")
        self.assertFalse(identifier.display_text)

        with requests_mock.Mocker() as mock:
            mock.head(
                "https://api.datacite.org/dois/20.500.99999/bar.1", status_code=200
            )
            do.set_identifier("20.500.99999/bar.1")
        self.assertEqual(do.persistent_id, "https://doi.org/20.500.99999/bar.1")

        with requests_mock.Mocker() as mock:
            mock.head(
                "https://api.datacite.org/dois/20.500.99999/bar.2", status_code=404
            )
            mock.head("https://hdl.handle.net/20.500.99999/bar.2", status_code=200)
            do.set_identifier("20.500.99999/bar.2")
        self.assertEqual(do.persistent_id, "https://hdl.handle.net/20.500.99999/bar.2")

        # NOTE: https://n2t.net/.info/zzztestprefix
        with requests_mock.Mocker() as mock:
            mock.head(
                "https://api.datacite.org/dois/zzztestprefix:bar.3", status_code=404
            )
            mock.head("https://hdl.handle.net/zzztestprefix:bar.3", status_code=404)
            mock.head("https://n2t.net/zzztestprefix:bar.3", status_code=200)
            do.set_identifier("zzztestprefix:bar.3")
        self.assertEqual(do.persistent_id, "https://n2t.net/zzztestprefix:bar.3")
        self.assertIsNotNone(
            do.identifiers.filter(
                scheme="url", url="https://n2t.net/zzztestprefix:bar.3"
            )
        )


class PackageMetadataTest(TestCase):
    """Tests for :mod:`heliport.core.utils.version`."""

    def test_project_urls(self):
        """Test for ``get_project_urls``."""
        self.assertIsNotNone(get_project_url(heliport_metadata, "Repository"))
        self.assertIsNone(get_project_url(heliport_metadata, "Secret other URL"))

    def test_heliport_homepage(self):
        """Make sure a HELIPORT homepage is linked."""
        self.assertIn("https://", heliport_homepage)


class HeliportVersionTest(TestCase):
    """Test version handling and reporting."""

    def test_version_header(self):
        """Test existence of the version header.

        Implemented in :class:`heliport.core.middleware.HeliportVersionMiddleware`.
        """
        response = self.client.get(reverse("core:index"))
        self.assertTrue(response.has_header("X-HELIPORT-Version"))
        self.assertEqual(response.get("X-HELIPORT-Version"), heliport_version_string)

    def test_version_string_on_index_page(self):
        """Test existence of version string on the index page."""
        response = self.client.get(reverse("core:index"))
        self.assertIn(heliport_version_string, response.content.decode())


class RedocApiDocsTest(TestCase):
    """Test Redoc API docs."""

    def test_no_google_fonts(self):
        """Make sure no Google Fonts are used."""
        response = self.client.get(reverse("core:redoc"))
        self.assertNotIn("fonts.googleapis.com", response.content.decode())
        self.assertNotIn("fonts.gstatic.com", response.content.decode())
