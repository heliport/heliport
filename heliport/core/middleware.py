# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later


from django.utils.cache import add_never_cache_headers

from heliport.core.utils.version import heliport_version_string


class HeliportVersionMiddleware:
    """Middleware that adds a HELIPORT version number header to all reponses."""

    def __init__(self, get_response):
        """Initialize the middleware as per the Django docs."""
        self.get_response = get_response

    def __call__(self, request):
        """Add ``X-HELIPORT-Version`` header to the response."""
        response = self.get_response(request)
        response["X-HELIPORT-Version"] = heliport_version_string
        return response


class DisableClientSideCachingMiddleware:  # noqa: D101
    def __init__(self, get_response):  # noqa: D107
        self.get_response = get_response

    def __call__(self, request):  # noqa: D102
        response = self.get_response(request)
        add_never_cache_headers(response)
        return response
