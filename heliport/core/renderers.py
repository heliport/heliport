# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Renderers for the HELIPORT API."""

from rest_framework.renderers import TemplateHTMLRenderer


class HeliportPartialRenderer(TemplateHTMLRenderer):
    """A :class:`TemplateHTMLRenderer` that can be used to specifically ask for partials."""  # noqa: E501

    format = "partial"
