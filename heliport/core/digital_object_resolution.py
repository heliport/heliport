# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Functionality to allow resolving objects from url parameters.

Register your objects in apps.py like this::

    from django.apps import AppConfig
    from heliport.core.digital_object_resolution import object_types

    class MyConfig(AppConfig):
        name = "my_app_name"

        def ready(self):
            from .models import MyObject
            object_types.register(MyObject)

where MyObject is a :class:`Resolvable` but more specifically a
:class:`heliport.core.digital_object_interface.GeneralDigitalObject` in most cases.
"""

from __future__ import annotations

import logging
from abc import abstractmethod
from typing import TYPE_CHECKING, Dict, Optional, Union
from urllib.parse import urlencode

from django.urls import reverse

from heliport.core.digital_object_class import digital_object_class, digital_objects
from heliport.core.utils.collections import FDecoratorIndex
from heliport.core.utils.context import Context

if TYPE_CHECKING:
    from heliport.core.digital_object_interface import GeneralDigitalObject
    from heliport.core.models import DigitalObject

logger = logging.getLogger(__name__)


class Resolvable:
    """This class does not inherit form ABC in order to avoid metaclass conflicts."""

    @staticmethod
    @abstractmethod
    def type_id() -> str:
        """The name used to reference this type in URLs and elsewhere."""  # noqa: D401

    @abstractmethod
    def get_identifying_params(self) -> Dict[str, str]:
        """The params that can be passed to :func:`resolve` to recreate this object."""  # noqa: D401, E501

    @staticmethod
    @abstractmethod
    def resolve(
        params: Dict[str, str], context: Context
    ) -> Optional[GeneralDigitalObject]:
        """Given some parameters, construct or find a matching object and return it."""

    def as_digital_object_if_exists(self, context: Context):
        """Get digital object from this resolvable.

        Uses the fact that :meth:`resolve` returns existing digital objects, to return
        it if a :class:`heliport.core.models.DigitalObject` instance exists for this
        resolvable.
        """  # noqa: D205, D401
        return self.resolve(self.get_identifying_params(), context)


#: Register :class:`Resolvable` subclasses to this in apps.py of your django app.
#: Call :meth:`heliport.core.utils.collections.DecoratorIndex.register` with your
#: :class:`Resolvable` or
#: :class:`heliport.core.digital_object_interface.GeneralDigitalObject` for this:
#: ``object_types.register(MyObject)``.
object_types: FDecoratorIndex[str, Resolvable] = FDecoratorIndex("type_id")


def resolve(
    params: Dict[str, str], context: Context
) -> Union[GeneralDigitalObject, DigitalObject, None]:
    """Resolve an object based on some params.

    If "digital_object_id" in ``params`` a :class:`heliport.core.models.DigitalObject`
    instance with that id is returned, casted as the proper subclass.
    If "type" in ``params``, the registered :class:`Resolvable` instance for that
    type is used to :meth:`Resolvable.resolve` the ``params``.

    :param params: Parameters to construct the object from
    :param context: You must make sure to close the context yourself.
    :return: The resolved GeneralDigitalObject or ``None`` if nothing was found.
    """  # noqa: D205
    digital_object_id = params.get("digital_object_id")
    if digital_object_id is not None:
        obj = digital_objects().filter(digital_object_id=digital_object_id).first()
        if obj is None:
            logger.debug(
                "resolution failed: "
                f"digital object id {digital_object_id} does not exist"
            )
            return None
        return obj.casted_or_self()

    t = params.get("type")
    if t is None:
        logger.debug("resolution failed: no type specified")
        return None
    if t not in object_types:
        logger.debug(f'resolution failed: type not registered: "{t}"')
        return None
    result = object_types[t].resolve(params, context)
    if result is None:
        logger.debug(f"resolution failed: params {params} not valid")
    return result


def params_for(resolvable: Resolvable):
    """
    Get a dict of params for some resolvable. This uses
    :meth:`Resolvable.get_identifying_params` but has special handling for
    :class:`heliport.core.models.DigitalObject` instances. Because they can just be
    resolved by digital_object_id.
    """  # noqa: D205
    if isinstance(resolvable, digital_object_class()):
        return {"digital_object_id": resolvable.digital_object_id}
    if isinstance(resolvable, Resolvable):
        return {**resolvable.get_identifying_params(), "type": resolvable.type_id()}
    raise TypeError(
        f"Expected DigitalObject or Resolvable. Got {type(resolvable).__name__}"
    )


def url_for(url_str: str, resolvable: Resolvable, **kwargs):
    """
    Generates a URL for a view that takes a :class:`Resolvable` as input via get params.

    :param url_str: The name of the url. This is normally a combination of app name and
        url name separated by ":". An example would be: ``"core:as_digital_object"``
    :param resolvable: The object that should be provided as parameter to the view.
    :param kwargs: kwargs that are needed for generating the url from the url name.
        E.g. if you have ``".../<int:project>/..."`` as url config you need to provide
        ``project`` as key word argument.
    :return: URL as string
    """  # noqa: D401
    url = reverse(url_str, kwargs=kwargs)
    params = params_for(resolvable)
    return f"{url}?{urlencode(params)}"
