# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Forms for usage in core HELIPORT views."""

import re
from functools import reduce

from allauth.account import forms as account_forms
from allauth.socialaccount import forms as socialaccount_forms
from django.forms import (
    CharField,
    Select,
    URLField,
    URLInput,
    ValidationError,
)

# This is a demonstration ORCID of a ficticious person
_DEMONSTRATION_ORCID = "https://orcid.org/0000-0002-1825-0097"


# Free function which can be reused in different forms.
def validate_orcid(input: str) -> None:
    """Validate whether a given input string is an ORCID.

    We only ensure the input looks plausible, i.e. it looks like
    ``https://orcid.org/0000-0002-1825-0097`` and the checksum is correct. More
    information on the format and th checksum algorithm can be found `on the ORCID
    support page <https://support.orcid.org/hc/en-us/articles/360006897674-Structure-of-the-ORCID-Identifier>`_.
    """
    orcid_pattern = r"^https:\/\/orcid.org\/([0-9]{4}-){3}[0-9]{3}[0-9X]$"
    if re.match(orcid_pattern, input) is None:
        raise ValidationError(
            f"ORCID must be of format {_DEMONSTRATION_ORCID!r}. "
            "The last digit may be an X.",
            code="orcid_invalid_format",
        )

    id_str = input[-19:-1].replace("-", "")
    digits = [int(digit) for digit in id_str]
    provided_checksum = int(input[-1].replace("X", "10"))
    calculated_checksum = (12 - reduce(lambda t, x: 2 * (t + x), digits, 0) % 11) % 11

    if calculated_checksum != provided_checksum:
        raise ValidationError(
            "Invalid checksum. Did you make a typo?", code="orcid_invalid_checksum"
        )


class OrcidInput(URLInput):
    """URL input specialized for entry of ORCIDs."""

    _orcid_input_attrs = {
        "title": _DEMONSTRATION_ORCID,
        "pattern": "https://orcid.org/([0-9]{4}-){3}[0-9]{3}[0-9X]",
        "placeholder": _DEMONSTRATION_ORCID,
    }

    def __init__(self, attrs=None):
        """Initialize the widget using sane defaults if not specified otherwise."""
        attrs = attrs or {}
        for key, value in self._orcid_input_attrs.items():
            if key not in attrs:
                attrs[key] = value
        super().__init__(attrs)


class OrcidField(URLField):
    """URL field specialized for ORCIDs."""

    _orcid_field_kwargs = {
        "label": "ORCID",
        "validators": [validate_orcid],
        "widget": OrcidInput,
    }

    def __init__(self, *args, **kwargs):
        """Initialize the field using sane defaults if not specified otherwise."""
        for key, value in self._orcid_field_kwargs.items():
            if key not in kwargs:
                kwargs[key] = value
        super().__init__(*args, **kwargs)


class HeliportAllauthFormMixin:
    """Cosmetic changes for ``allauth`` forms used in HELIPORT."""

    def __init__(self, *args, **kwargs):
        """Initialize the form.

        This overrides the email and password2 field labels and placeholders from the
        parent class.

        - label "Email (optional)" → "Email"
        - label "Password (again)" → "Repeat Your Password"
        - placeholder "Password (again)" → "Password"
        """
        super().__init__(*args, **kwargs)
        if "email" in self.fields:
            self.fields["email"].label = "Email"
        if "password2" in self.fields:
            self.fields["password2"].label = "Repeat Your Password"
            self.fields["password2"].widget.attrs["placeholder"] = "Password"

    #: Order of the form fields. Fields not present are ignored.
    field_order = [
        "email",
        "username",
        "oldpassword",
        "password1",
        "password2",
        "first_name",
        "last_name",
        "email2",
        "orcid",
    ]


class HeliportAllauthPasswordChangeFormMixin(HeliportAllauthFormMixin):
    """Cosmetic changes for ``allauth`` password change forms used in HELIPORT."""

    def __init__(self, *args, **kwargs):
        """Initialize the form.

        This overrides the password2 field label and placeholder from the parent class.

        - label "New Password (again)" → "Repeat Your New Password"
        - placeholder "New Password (again)" → "New Password"
        """
        super().__init__(*args, **kwargs)
        if "password2" in self.fields:
            self.fields["password2"].label = "Repeat Your New Password"
            self.fields["password2"].widget.attrs["placeholder"] = "New Password"


class ChangePasswordForm(
    HeliportAllauthPasswordChangeFormMixin, account_forms.ChangePasswordForm
):
    """Allauth's ``ChangePasswordForm``, adapted for HELIPORT."""


class ResetPasswordKeyForm(
    HeliportAllauthPasswordChangeFormMixin, account_forms.ResetPasswordKeyForm
):
    """Allauth's ``ResetPasswordKeyForm``, adapted for HELIPORT."""


class SetPasswordForm(HeliportAllauthFormMixin, account_forms.SetPasswordForm):
    """Allauth's ``SetPasswordForm``, adapted for HELIPORT."""


class SignupForm(HeliportAllauthFormMixin, account_forms.SignupForm):
    """Form for user self registration."""

    first_name = CharField(label="Given name", required=False)
    last_name = CharField(label="Surname", required=False)
    orcid = OrcidField(required=False)


class HelmholtzIDSignupForm(HeliportAllauthFormMixin, socialaccount_forms.SignupForm):
    """Form for account setup after Helmholtz ID login."""

    def __init__(self, *args, **kwargs):
        """Initialize the form.

        This disables the email field from the parent class so that users are not able
        to change it.
        """
        super().__init__(*args, **kwargs)
        if "email" in self.fields:
            self.fields["email"].disabled = True

    orcid = OrcidField(required=False)


class LoginSelectWidget(Select):
    """A select widget for Login infos to be used in Django forms.

    This automatically displays a message that links to the login info page when there
    are no login infos for the given user.
    """

    template_name = "core/base/login_select.html"
