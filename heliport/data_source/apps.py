# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Django app configuration.

Some HELIPORT hooks can be registered in app config :meth:`django.apps.AppConfig.ready`.

See also :ref:`Django documentation <django:ref/applications:applications>`
"""

from django.apps import AppConfig

from heliport.core.digital_object_actions import actions


class DataSourceConfig(AppConfig):
    """App configuration for data source app."""

    name = "heliport.data_source"

    def ready(self):
        """Register object types."""
        from .views import DownloadFile, OpenDirectory

        actions.register(OpenDirectory)
        actions.register(DownloadFile)
