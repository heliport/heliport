# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

import logging
import pathlib
from abc import ABC
from datetime import datetime, timedelta
from stat import S_ISDIR
from typing import Type
from urllib.parse import quote

from django.conf import settings
from django.contrib import messages
from django.db.models import Q
from django.forms import BaseForm, Media
from django.http import (
    FileResponse,
    Http404,
    JsonResponse,
)
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone
from django.views.generic import DetailView, TemplateView

from heliport.core.digital_object_actions import Action
from heliport.core.digital_object_aspects import DirectoryObj, FileObj
from heliport.core.digital_object_resolution import Resolvable, resolve, url_for
from heliport.core.mixins import (
    HeliportLoginRequiredMixin,
    HeliportObjectMixin,
    HeliportProjectMixin,
)
from heliport.core.models import (
    DigitalObject,
    Project,
    Vocabulary,
    assert_relation,
)
from heliport.core.permissions import is_object_member
from heliport.core.utils.context import Context, digital_object_breadcrumbs
from heliport.core.utils.exceptions import UserMessage
from heliport.core.utils.string_tools import format_byte_count, format_time
from heliport.core.views import HeliportModelViewSet

from ..core.app_interaction import DigitalObjectModule, get_modules_for_type
from ..core.utils.http import get_streaming_file_response
from ..core.utils.queries import delete_by_id
from ..core.views.generic import HeliportCreateAndUpdateView
from .forms import DataSourceForm
from .models import DataSource
from .serializers import DataSourceSerializer

logger = logging.getLogger(__name__)


def data_source_modules() -> dict[str, DigitalObjectModule]:
    """Get all HELIPORT modules that represent a file or directory.

    The result is a dict mapping module_id to module instance.
    """
    return {**get_modules_for_type(DirectoryObj), **get_modules_for_type(FileObj)}


def data_source_forms() -> dict[str, Type[BaseForm]]:
    """Get form classes for data sources.

    The result is a dict mapping module_id to form class. Only modules that have a form
    class are included.
    """
    return {
        module_id: module.form_class
        for module_id, module in data_source_modules().items()
        if module.form_class
    }


def data_source_classes() -> dict[Type[DigitalObject], DigitalObjectModule]:
    """Get all data source classes in HELIPORT.

    The result is a dict mapping the classes to a module that manges them.
    """
    return {
        data_source_cls: module
        for module in data_source_modules().values()
        for data_source_cls in module.object_classes
        if issubclass(data_source_cls, (DirectoryObj, FileObj))
        and issubclass(data_source_cls, DigitalObject)
    }


def form_module_for(data_source: DigitalObject):
    """Get module that can handle ``data_source`` and that has a form class.

    Return ``None`` if no such module is found.
    """
    for module in data_source_modules().values():
        if module.can_handle(data_source) and module.form_class is not None:
            return module
    return None


class DataSourceView(HeliportCreateAndUpdateView):
    """View To list and edit data sources including those from other apps."""

    template_name = "data_source/data_source.html"
    list_url = "data_source:list"
    update_url = "data_source:update"

    def get_context_data(self, **kwargs):
        """Get extra rendering context."""
        context = super().get_context_data(**kwargs)
        context["vocab"] = Vocabulary()

        context["sources"] = [
            {
                "data_source_id": source.data_source_id,
                "digital_object_id": source.digital_object_id,
                "protocol": source.protocol,
                "kind": source.protocol,
                "label": source.label,
                "description": source.description,
                "uri": source.uri,
                "browser_supported_uri": source.browser_supported_uri,
                "owner": source.login is None or source.login.user == self.user,
                "actions": source.actions(self.user, self.project),
                "directly_editable": True,
                "icon": "fa-circle-question",
                "is_helper": bool(source.generated_inside),
            }
            for source in DataSource.objects.filter(
                projects=self.project, deleted__isnull=True
            )
        ]

        for source_cls, module in data_source_classes().items():
            for source in source_cls.objects.filter(
                projects=self.project, deleted__isnull=True
            ):
                context["sources"].append(
                    {
                        "data_source_id": source.pk,
                        "digital_object_id": source.digital_object_id,
                        "kind": module.name,
                        "module_url": module.get_url(self.project),
                        "label": source.label,
                        "description": source.description,
                        "owner": source.access(self.context).check_permission(),
                        "actions": source.actions(self.user, self.project),
                        "directly_editable": form_module_for(source) is not None,
                        "icon": "fa-folder"
                        if isinstance(source, DirectoryObj)
                        else "fa-file",
                        "is_helper": source.is_helper,
                    }
                )

        context["sources"].sort(key=lambda x: x["icon"], reverse=True)
        context["helpers_exist"] = any(s["is_helper"] for s in context["sources"])

        context["no_form_modules"] = [
            {
                "url": module.get_url(self.project),
                "icon": module.icon,
                "name": module.name,
            }
            for module in data_source_modules().values()
            if module.form_class is None
        ]

        # modules may be pre instantiated if rendering an error during form validation
        pre_instantiated_forms = context.get("pre_instantiated_forms", {})
        if self.object and not pre_instantiated_forms:
            if isinstance(self.object, DataSource):
                pre_instantiated_forms[None] = DataSourceForm(
                    instance=self.object, context=self.context
                )
            else:
                module = form_module_for(self.object)
                pre_instantiated_forms[module.module_id] = module.form_class(
                    instance=self.object, context=self.context
                )
        context["form_modules"] = [
            {
                "module": module,
                "form": pre_instantiated_forms.get(
                    module.module_id, module.form_class(context=self.context)
                ),
            }
            for module in data_source_modules().values()
            if module.form_class is not None
        ]
        context["generic_form"] = pre_instantiated_forms.get(
            None, DataSourceForm(context=self.context)
        )
        edit_modules = list(pre_instantiated_forms)
        if edit_modules:
            context["edit_module"] = edit_modules[0]
        elif context["form_modules"]:
            context["edit_module"] = context["form_modules"][0]["module"].module_id
        else:
            context["edit_module"] = None

        context["form_media"] = context["generic_form"].media + sum(
            (f["form"].media for f in context["form_modules"]),
            start=Media(),
        )

        return context

    def post(self, request, *args, **kwargs):
        """Handle deletion, creation and updating using the appropriate form."""
        to_remove = request.POST.get("remove")
        if to_remove:
            delete_by_id(to_remove, self.context)
            return self.get_response()

        module_form = request.POST.get("module-form")
        if module_form is None:
            form_class = DataSourceForm
            if self.object is not None and not isinstance(self.object, DataSource):
                raise UserMessage(f"{self.object} is not a DataSource")
        else:
            form_class = data_source_forms().get(module_form)
            if form_class is None:
                raise UserMessage(f"Not a registered data source form: {module_form}")
            module = data_source_modules()[module_form]
            if self.object is not None and not module.can_handle(self.object):
                raise UserMessage(
                    f"Can not edit {type(self.object).__name__} using {module.name}"
                )

        form = form_class(self.request.POST, instance=self.object, context=self.context)
        if not form.is_valid():
            return self.render_to_response(
                self.get_context_data(pre_instantiated_forms={module_form: form})
            )

        form.save()
        return self.get_response()


class SSHDataSourceOpenView(HeliportObjectMixin, DetailView):
    """
    This is the legacy way of showing a directory or downloading a file that is a
    data source with ssh protocol. The new way is to register the ssh files/directories
    in :class:`heliport.ssh.views.SSHListView` and open it using
    :class:`GeneralDataSourceOpenView`.
    """  # noqa: D205

    model = DataSource

    def get(self, request, *args, **kwargs):
        """Respond to GET request.

        Check if data source is file directory or directory. Download Files using a
        streaming response (ssh connection is closed when django closes file). Show
        directory using template.
        """
        self.object = self.get_object()
        data_source = self.object
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        login = data_source.login
        source_path = pathlib.Path(data_source.path)
        relative_path = pathlib.Path("")
        if "path" in request.GET:
            relative_path = pathlib.Path(request.GET["path"])
            assert not any(n.startswith(".") for n in relative_path.parts), (
                "access of hidden directory"
            )

        path = source_path / relative_path
        assert source_path == path or source_path in path.parents, (
            "access of non sub directory"
        )

        logger.debug(f"opening ssh connection datasource={data_source.pk}")
        ssh_connection = login.build_command_executor()
        error_message = ssh_connection.connect_error_message(
            f' - try reconnecting the "{login.name}" ssh connection'
        )
        if error_message is not None:
            messages.error(request, error_message)
            return redirect("data_source:list", project=project.pk)
        file = ssh_connection.file(str(path))

        file_stat = file.stat()
        if hasattr(file_stat, "st_mode") and S_ISDIR(file_stat.st_mode):
            logger.info(f"opening ssh datasource {data_source.pk} (Directory)")
            context = super().get_context_data()
            dir_content_files = ssh_connection.list_dir(str(path))
            dir_content = [
                {
                    "name": s.filename,
                    "path": relative_path / s.filename,
                    "quoted_path": quote(str(relative_path / s.filename)),
                    "digital_object": data_source_by_path(
                        project, path / s.filename, data_source
                    ),
                    "size": hasattr(s, "st_size") and self.format_size(s.st_size),
                    "mtime": hasattr(s, "st_mtime") and self.format_time(s.st_mtime),
                    "is_dir": hasattr(s, "st_mode") and S_ISDIR(s.st_mode),
                }
                for s in dir_content_files
                if not s.filename.startswith(".")
            ]
            context["dir_content"] = sorted(
                dir_content, key=lambda n: (not n["is_dir"], n["name"].lower())
            )
            data_source_to_remove = DataSource.objects.filter(
                projects=project,
                uri__startswith=f"ssh://{path}/",
                deleted__isnull=True,
                generated_inside=data_source,
            )
            for f in dir_content_files:
                data_source_to_remove = data_source_to_remove.exclude(
                    uri__startswith=f"ssh://{path / f.filename}"
                )
            for data_source in data_source_to_remove:
                if path not in pathlib.Path(data_source.path).parents:
                    continue
                logger.debug(f"removing data source {data_source.uri}")
                data_source.deleted = timezone.now()
                data_source.save()

            parent_paths = []
            for p in reversed(relative_path.parents):
                if p != pathlib.Path():
                    parent_paths.append({"name": p.name, "path": str(p)})

            context["parent_paths"] = parent_paths
            context["directory_name"] = path.name
            context["has_relative_path"] = path != pathlib.Path(data_source.path)
            context["vocab"] = Vocabulary()
            return render(request, "data_source/directory.html", context)

        old_close = file.close

        def new_close():
            """
            File response only adds ``file.close`` to functions that need to be called
            to close things. Therefore, it is not completely unnatural to redefine this.
            """  # noqa: D205
            old_close()
            ssh_connection.close()
            logger.debug(f"closing ssh connection datasource={data_source.pk}")

        file.close = new_close
        response = FileResponse(file, filename=path.name, as_attachment=True)
        file_stat = file.stat()
        if hasattr(file_stat, "st_size"):
            logger.info(
                f"opening ssh datasource {data_source.pk} ({self.format_size(file_stat.st_size)})"  # noqa: E501
            )
            response["Content-Length"] = file_stat.st_size
        else:
            logger.info(f"opening ssh datasource {data_source.pk}")
        return response

    @staticmethod
    def format_size(size):
        """Format file size in bytes to human-readable units."""
        return format_byte_count(size)

    @staticmethod
    def format_time(mtime):
        """Formats absolute time in seconds to human-readable string."""  # noqa: D401
        return format_time(mtime)


class GeneralDataSourceOpenView(HeliportProjectMixin, DetailView):
    """
    Can handle all :class:`heliport.core.digital_object_aspects.DirectoryObj` and
    :class:`heliport.core.digital_object_aspects.FileObj` instances. They could be
    subclasses of :class:`heliport.core.models.DigitalObject` or just generally
    :class:`heliport.core.digital_object_resolution.Resolvable`.

    Files are presented for download and content directories is visualized.
    """  # noqa: D205

    model = Project
    pk_url_kwarg = "project"

    def get_digital_object(self, context):
        """Get a :class:`heliport.core.digital_object_interface.GeneralDigitalObject`
        instance from the provided URL query parameters using typical resolution.
        """  # noqa: D205
        obj = resolve(self.request.GET, context)
        if obj is None:
            raise UserMessage(f"Object not found: {self.request.GET}")
        return obj

    def get_directory_response(self, obj: DirectoryObj, exit_context: Context):
        """Construct context and render template to show content of directory."""
        render_context = self.get_context_data()
        start_time = datetime.now()
        parts = []
        for part in obj.as_directory(exit_context).get_parts():
            part = part.as_digital_object_if_exists(exit_context)
            self.extend_by_file_info(part, exit_context, start_time)
            parts.append(part)

        render_context["contents"] = sorted(
            parts, key=lambda p: (isinstance(p, FileObj), str(p).casefold())
        )
        render_context["directory"] = obj
        render_context["digital_object_breadcrumbs"] = digital_object_breadcrumbs(
            self.request.session, self.request.GET, obj, self.request.get_full_path()
        )
        return render(
            self.request, "data_source/general_directory.html", render_context
        )

    def extend_by_file_info(self, path_obj, context, start_time):
        """
        Set ``info`` attribute on ``path_obj`` to dict returned by
        :meth:`heliport.core.digital_object_aspects.FileOrDirectory.get_file_info`.

        Does nothing if current time is already more than three seconds after
        ``start_time`` because some ``get_file_info`` implementations might be slow.
        """  # noqa: D205
        timeout = timedelta(seconds=3)
        if datetime.now() > start_time + timeout:
            path_obj.info = {}
            return

        if isinstance(path_obj, DirectoryObj):
            path_obj.info = path_obj.as_directory(context).get_file_info()
        elif isinstance(path_obj, FileObj):
            path_obj.info = path_obj.as_file(context).get_file_info()

    def get_file_response(self, file_obj: FileObj, context):
        """Construct a streaming response for downloading the file."""
        file = file_obj.as_file(context)
        return get_streaming_file_response(file, str(file_obj), context)

    def get(self, request, *args, **kwargs):
        """Respond to http get (this is called by django)."""
        project = self.get_object()
        self.object = project
        with Context(getattr(request.user, "heliportuser", None), project) as context:
            try:
                obj = self.get_digital_object(context)
                if isinstance(obj, DirectoryObj):
                    return self.get_directory_response(obj, context)
                if isinstance(obj, FileObj):
                    return self.get_file_response(obj, context)
                raise UserMessage(
                    f"{obj} did not implement the file or directory interface"
                )
            except UserMessage as e:
                messages.error(request, e.message)
                raise Http404(e.message) from e


def data_source_by_path(project, path, parent_data_source):  # noqa: D103
    uri = f"ssh://{path}"
    return (
        DataSource.objects.filter(
            uri=uri,
            login=parent_data_source.login,
            projects=project,
            deleted__isnull=True,
        )
        .order_by("pk")
        .first()
    )


class SubDataSourceCreateView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = DataSource

    def post(self, request, *args, **kwargs):  # noqa: D102
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        data_source = self.get_object()
        assert data_source.uri.startswith("ssh://")
        login = data_source.login
        path = data_source.sub_path(request.GET.get("path"))
        existing_data_source = data_source_by_path(project, path, data_source)
        if existing_data_source is None:
            new_data_source = DataSource(
                login=login,
                uri=f"ssh://{path}",
                label=path.name,
                generated_inside=data_source,
            )
            new_data_source.category_str = settings.DATA_SOURCE_NAMESPACE
            new_data_source.save()
            new_data_source.projects.add(*data_source.projects.all())
        else:
            new_data_source = existing_data_source
        assert_relation(data_source, Vocabulary().has_part, new_data_source)
        return JsonResponse(
            {
                "pk": new_data_source.pk,
                "digital_object_id": new_data_source.digital_object_id,
                "uri": new_data_source.uri,
                "label": new_data_source.label,
            }
        )


class SearchView(HeliportLoginRequiredMixin, TemplateView):  # noqa: D101
    template_name = "data_source/search.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the search results.
        """
        context = super().get_context_data(**kwargs)
        user = self.request.user.heliportuser
        query = self.request.GET.get("q", "")

        data_source_results = set()

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            data_source_results.update(
                DataSource.objects.filter(
                    is_object_member(user)
                    & Q(projects__deleted__isnull=True, deleted__isnull=True)
                    & (
                        Q(data_source_id=num)
                        | Q(uri__icontains=word)
                        | Q(description__icontains=word)
                        | Q(label__icontains=word)
                        | Q(login__machine__icontains=word)
                        | Q(login__username__icontains=word)
                        | Q(projects__label__icontains=word)
                    )
                )
            )

        context["data_source_results"] = data_source_results
        return context


##########################################
#               REST API                 #
##########################################


class DataSourceViewSet(HeliportModelViewSet):
    """Data Source."""

    serializer_class = DataSourceSerializer
    filterset_fields = ["data_source_id", "label", "uri", "persistent_id", "projects"]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return DataSource.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()


##########################################
#               Actions                  #
##########################################


class OpenAction(Action, ABC):
    """
    Base class for ``Action`` that opens digital objects using
    :class:`GeneralDataSourceOpenView`.
    """  # noqa: D205

    @property
    def link(self) -> str:
        """
        Construct a link to :class:`GeneralDataSourceOpenView` specific to current
        project and digital object.
        """  # noqa: D205
        if isinstance(self.obj, Resolvable):
            return url_for("data_source:open", self.obj, project=self.project.pk)
        return "#"

    def applicable(self) -> bool:
        """
        Check applicability by testing if :meth:`link` is successful on this instance.

        This should be overwritten in subclasses to be more specific.
        """
        if hasattr(self.obj, "unsupported") and self.obj.unsupported:
            return False
        return self.link != "#"


class OpenDirectory(OpenAction):
    """``Action`` that opens some ``DirectoryObj`` by showing a preview (see base class)."""  # noqa: E501

    #: The name shown to user
    name = "Open"
    #: The font awesome icon possibly shown to user
    icon = "fa-folder-open"

    @property
    def applicable(self) -> bool:
        """
        This action is applicable for
        :class:`heliport.core.digital_object_aspects.DirectoryObj` instances.
        """  # noqa: D205
        return isinstance(self.obj, DirectoryObj) and super().applicable()


class DownloadFile(OpenAction):
    """``Action`` that downloads some ``FileObj`` (see base class)."""

    #: The name shown to user
    name = "Download"
    #: The font awesome icon possibly used for this action
    icon = "fa-file"

    @property
    def applicable(self) -> bool:
        """
        This action is applicable for
        :class:`heliport.core.digital_object_aspects.FileObj` instances.
        """  # noqa: D205
        return isinstance(self.obj, FileObj) and super().applicable()
