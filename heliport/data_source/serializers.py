# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""This module is for serialization into Datacite, RDF and JSON.

The JSON format is used for API endpoints via
`Django rest framework <https://www.django-rest-framework.org/>`_. Using this, it is
typical to put the serializer classes into a "serializers.py" file.

For the serialization into RDF, attributes are described.
See :class:`heliport.core.attribute_description.BaseAttribute` for more detail.
"""

from django.conf import settings
from rest_framework import serializers

from heliport.core.models import LoginInfo
from heliport.core.permissions import projects_of
from heliport.core.serializers import NamespaceField
from heliport.data_source.models import DataSource


class DataSourceSerializer(serializers.ModelSerializer):  # noqa: D101
    login = serializers.PrimaryKeyRelatedField(queryset=LoginInfo.objects.none())
    namespace_str = NamespaceField(settings.DATA_SOURCE_NAMESPACE)

    def __init__(self, *args, **kwargs):
        """Override querysets to only allow current user's objects."""
        super().__init__(*args, **kwargs)
        user = self.context["request"].user.heliportuser
        self.fields["login"].queryset = LoginInfo.objects.filter(user=user)
        self.fields["projects"].child_relation.queryset = projects_of(user)

    class Meta:  # noqa: D106
        model = DataSource
        fields = [
            "data_source_id",
            "uri",
            "description",
            "label",
            "login",
            "projects",
            "persistent_id",
            "namespace_str",
        ]
