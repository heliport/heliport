# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains :class:`django.db.models.Model` classes for Django ORM.

See :ref:`django:topics/db/models:quick example` from Django documentation.
In HELIPORT the :class:`heliport.core.models.DigitalObject` can be subclassed for models
containing metadata in a project.
"""

import logging
import pathlib

from django.db import models

from heliport.core.models import DigitalObject, LoginInfo
from heliport.core.utils.context import Context
from heliport.core.utils.exceptions import AccessDenied

logger = logging.getLogger(__name__)


class DataSource(DigitalObject):  # noqa: D101
    PROTOCOL_CHOICES = ["https", "http", "ftp", "ftps", "file", "ssh", "other"]

    data_source_id = models.AutoField(primary_key=True)
    uri = models.CharField(max_length=500, blank=True)
    login = models.ForeignKey(
        LoginInfo, on_delete=models.SET_NULL, null=True, blank=True
    )
    generated_inside = models.ForeignKey(
        "DataSource", null=True, on_delete=models.CASCADE
    )

    @property
    def protocol_path(self):  # noqa: D102
        result = self.uri.split("://", maxsplit=1)
        while len(result) < 2:
            result.append("")
        return result

    @property
    def protocol(self):  # noqa: D102
        protocol, uri = self.protocol_path
        return protocol

    @property
    def path(self):  # noqa: D102
        protocol, uri = self.protocol_path
        return uri

    @property
    def server_address(self):
        """The server address part of the path.

        This is always the first part of the path before the first "/".
        """
        return self.path.split("/")[0]

    @property
    def browser_supported_uri(self):  # noqa: D102
        return self.protocol in {"http", "https", "ftp"}

    def sub_path(self, relative_path_str=None):
        """
        This function is deprecated as it is specific to ssh directories.
        The new place is :meth:`heliport.ssh.SSHDirectory.sub_path`.
        """  # noqa: D205, D401
        source_path = pathlib.Path(self.path)
        relative_path = pathlib.Path("")
        if relative_path_str:
            relative_path = pathlib.Path(relative_path_str)
            if any(n.startswith(".") for n in relative_path.parts):
                raise AccessDenied("Access of hidden directory")

        path = source_path / relative_path
        if source_path != path and source_path not in path.parents:
            raise AccessDenied("Access of parent directory not allowed")
        return path

    def access(self, context: Context):
        """Get permission checker for this object and context.

        See :meth:`heliport.core.models.DigitalObject.access` for base implementation.

        There is also a check that only gives write permission if the current user owns
        the login.
        """
        from heliport.core.permissions import LoginPermissionChecker

        return LoginPermissionChecker(self, context)
