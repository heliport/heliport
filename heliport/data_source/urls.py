# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path
from rest_framework import routers

from .views import (
    DataSourceView,
    DataSourceViewSet,
    GeneralDataSourceOpenView,
    SearchView,
    SSHDataSourceOpenView,
    SubDataSourceCreateView,
)

app_name = "data_source"
urlpatterns = [
    path(
        "project/<int:project>/data-source/list/", DataSourceView.as_view(), name="list"
    ),
    path(
        "project/<int:project>/data-source/<int:pk>/update/",
        DataSourceView.as_view(),
        name="update",
    ),
    path(
        "project/<int:project>/data-source/<int:pk>/download/",
        SSHDataSourceOpenView.as_view(),
        name="download",
    ),
    path(
        "project/<int:project>/data-source/open/",
        GeneralDataSourceOpenView.as_view(),
        name="open",
    ),
    path(
        "project/<int:project>/data-source/<int:pk>/sub-data-source/",
        SubDataSourceCreateView.as_view(),
        name="sub_data_source",
    ),
    path("data-source/search/", SearchView.as_view(), name="search"),
]

# REST API
router = routers.DefaultRouter()
router.register(r"data-sources", DataSourceViewSet, basename="data_sources")
