# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Forms for usage in core HELIPORT views."""

from django import forms
from django.conf import settings
from django.forms import ModelForm

from heliport.core.forms import LoginSelectWidget
from heliport.core.models import LoginInfo
from heliport.core.utils.collections import key_to_end, key_to_start
from heliport.core.utils.context import Context
from heliport.core.utils.string_tools import conditional_text, remove_prefix
from heliport.data_source.models import DataSource


class DataSourceForm(ModelForm):
    """Form for creating and updating data sources."""

    template_name = "core/base/form.html"

    alpine_data = "{protocol: '', path: '', url: ''}"
    protocol = forms.ChoiceField(
        choices=list(zip(DataSource.PROTOCOL_CHOICES, DataSource.PROTOCOL_CHOICES)),
        widget=forms.Select(attrs={"class": "form-select", "x-model.fill": "protocol"}),
    )
    label = forms.CharField(
        label="name", widget=forms.TextInput(attrs={"class": "form-control"})
    )
    description = forms.CharField(
        widget=forms.Textarea(attrs={"class": "form-control", "rows": 5}),
        required=False,
    )
    link = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "data-row-show": "['http', 'https'].includes(protocol)",
                "x-model": "url",
            }
        ),
        required=False,
    )
    server_address = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "data-row-show": "['ftp', 'ftps'].includes(protocol)",
            }
        ),
        required=False,
    )
    path = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "data-row-show": "['ftp', 'ftps', 'file', 'ssh'].includes(protocol)",
                "x-model.fill": "path",
            }
        ),
        required=False,
        help_text=conditional_text(
            "Please use absolute paths.", "path.startsWith('~')"
        ),
    )
    uri = forms.CharField(
        label="URI",
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "data-row-show": "protocol === 'other'",
                "x-model.fill": "url",
            }
        ),
        required=False,
    )
    ssh_login = forms.ModelChoiceField(
        label="SSH Login",
        queryset=LoginInfo.objects.none(),
        widget=LoginSelectWidget(
            attrs={"class": "form-select", "data-row-show": "protocol === 'ssh'"}
        ),
        required=False,
        empty_label="Choose a Login",
    )
    login = forms.ModelChoiceField(
        queryset=LoginInfo.objects.none(),
        widget=LoginSelectWidget(
            attrs={"class": "form-select", "data-row-show": "protocol !== 'ssh'"}
        ),
        required=False,
        empty_label="Choose a Login",
    )

    def __init__(self, *args, context: Context, instance: DataSource = None, **kwargs):
        """Init."""
        # initialize derived values
        if instance:
            initial = kwargs.get("initial", {})
            initial["protocol"] = instance.protocol
            initial["server_address"] = instance.server_address
            initial["path"] = instance.path
            initial["ssh_login"] = instance.login
            kwargs["initial"] = initial

        super().__init__(*args, instance=instance, **kwargs)
        self.context = context

        key_to_start(self.fields, "protocol")
        key_to_end(self.fields, "login")

        self.fields["login"].queryset = self.context.user.logininfo_set.exclude(
            type=LoginInfo.LoginTypes.SSH
        )
        self.fields["ssh_login"].queryset = self.context.user.logininfo_set.filter(
            type=LoginInfo.LoginTypes.SSH
        )

    def clean_path(self):
        """Validate path."""
        result = self.cleaned_data["path"]
        protocol = self.cleaned_data["protocol"]
        if protocol in ("ftp", "ftps", "ssh", "file") and not result:
            raise forms.ValidationError("Path is required", code="required")
        if not result.startswith("/") and not result.startswith("\\") and result:
            result = f"/{result}"
        return result

    def clean_server_address(self):
        """Validate server_address."""
        result = self.cleaned_data["server_address"]
        protocol = self.cleaned_data["protocol"]
        if protocol in ("ftp", "ftps") and not result:
            raise forms.ValidationError("Server Address is required.", code="required")
        return result

    def clean_uri(self):
        """Validate uri."""
        result = self.cleaned_data["uri"]
        protocol = self.cleaned_data["protocol"]
        if protocol in ("other", "http", "https") and not result:
            raise forms.ValidationError("URL is required.", code="required")
        return result

    def clean_ssh_login(self):
        """Validate ssh_login."""
        result = self.cleaned_data["ssh_login"]
        protocol = self.cleaned_data["protocol"]
        if protocol == "ssh" and not result:
            raise forms.ValidationError("SSH Login is required.", code="required")
        return result

    def save(self, commit=True):
        """Save the data source setting the relevant properties."""
        instance: DataSource = super().save(commit=False)
        data = self.cleaned_data
        protocol = data["protocol"]
        if protocol == "ssh":
            instance.login = data["ssh_login"]
        if protocol in ("ftp", "ftps"):
            instance.uri = data["server_address"] + data["path"]
        if protocol in ("file", "ssh"):
            instance.uri = data["path"]

        if protocol != "other":
            link = remove_prefix(instance.uri, f"{protocol}://")
            instance.uri = f"{protocol}://{link}"
        elif "://" not in instance.uri:
            instance.uri = f"://{instance.uri}"

        if instance.pk is None:
            instance.save_with_namespace(
                settings.DATA_SOURCE_NAMESPACE, self.context.project
            )
        else:
            instance.access(self.context).assert_permission()
        return super().save(commit)

    class Meta:
        """Form config."""

        model = DataSource
        fields = ["label", "description", "uri", "login"]

    class Media:
        """Specify javascript that is required by the form."""

        js = ["core/js/row_show.js"]
