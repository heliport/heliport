# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from heliport.core.models import HeliportUser, LoginInfo, Project
from heliport.data_source.models import DataSource


class DataSourceEditTest(TestCase):
    """Test editing of data sources."""

    def setUp(self):
        """Set up project, instances and logged-in heliport users."""
        self.user1, _ = User.objects.get_or_create(username="user1")
        self.user2, _ = User.objects.get_or_create(username="user2")
        self.heliuser1, _ = HeliportUser.objects.get_or_create(
            auth_user=self.user1, display_name="HELI 1"
        )
        self.heliuser2, _ = HeliportUser.objects.get_or_create(
            auth_user=self.user2, display_name="HELI 2"
        )
        self.login1, _ = LoginInfo.objects.get_or_create(
            name="Login 1", user=self.heliuser1, type="username and password"
        )
        self.login2, _ = LoginInfo.objects.get_or_create(
            name="Login 2", user=self.heliuser2, type="username and password"
        )

        # project which is owned by heliuser1 and co-owned by heliuser2
        self.project, _ = Project.objects.get_or_create(owner=self.heliuser1)
        self.project.co_owners.add(self.heliuser2)

        # data source which is owned by heliuser2 and its login is owned by heliuser1
        self.data_source, _ = DataSource.objects.get_or_create(
            label="Example",
            uri="https://example.com",
            login=self.login1,
            owner=self.heliuser2,
        )
        self.data_source.projects.add(self.project)

        # helpers
        self.url = reverse(
            "data_source:update",
            kwargs={
                "project": self.project.pk,
                "pk": self.data_source.digital_object_id,
            },
        )

    def test_own_datasource(self):
        """Allow editing own data source."""
        self.client.force_login(self.user1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        # login1 is a suggested option in dropdown
        self.assertInHTML(self.login1.name, response.content.decode())
        # login2 is not a suggested option
        self.assertNotContains(response, self.login2.name)
        self.client.logout()

    def test_foreign_datasource(self):
        """Forbid editing someone else's data source."""
        self.client.force_login(self.user2)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)
        self.client.logout()

    def test_add_login(self):
        """Can add login to an "unowned" data source."""
        self.data_source.login = None
        self.data_source.save()
        self.assertEqual(self.data_source.login, None)

        self.client.force_login(self.user1)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertInHTML(self.login1.name, response.content.decode())

        response = self.client.post(
            self.url,
            data={
                "label": "Example",
                "protocol": "https",
                "server_address": "",
                "uri": "https://example.com",
                "login": self.login1.pk,
            },
        )
        self.assertEqual(response.status_code, 302)
        self.data_source.refresh_from_db()
        self.assertEqual(self.data_source.login, self.login1)
        self.client.logout()

    def test_other_login(self):
        """Can't set data source login to someone else's login."""
        self.client.force_login(self.user1)
        response = self.client.post(
            self.url,
            data={
                "label": "Example",
                "protocol": "https",
                "server_address": "",
                "uri": "https://example.com",
                "login": self.login2.pk,
            },
        )
        # show the same form again
        self.assertEqual(response.status_code, 200)
        self.data_source.refresh_from_db()
        self.assertNotEqual(self.data_source.login, self.login2)
        self.client.logout()
