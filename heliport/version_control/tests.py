# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from heliport.core.models import HeliportUser, Project

from .models import VersionControl


class VersionControlTests(TestCase):  # noqa: D101
    def setUp(self):
        """Set up project and logged-in heliport user."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.client.force_login(self.auth_user)
        self.project = Project.objects.create(owner=self.user)

    def test_list(self):  # noqa: D102
        vc = VersionControl.objects.create()
        vc.projects.add(self.project)

        response = self.client.get(
            reverse("version_control:list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(200, response.status_code)
        test_obj = VersionControl.objects.get(pk=vc.pk)
        self.assertEqual(test_obj, vc)
        self.assertIn(response.context_data["project"], test_obj.projects.all())
        self.assertIn(vc, response.context_data["vc_list"])

        response = self.client.get(
            reverse(
                "version_control:update",
                kwargs={"project": self.project.pk, "pk": vc.pk},
            )
        )
        self.assertEqual(200, response.status_code)
        test_obj = VersionControl.objects.get(pk=vc.pk)
        self.assertEqual(test_obj, vc)
        self.assertIn(response.context_data["project"], test_obj.projects.all())
        self.assertIn(vc, response.context_data["vc_list"])

    def test_create(self):  # noqa: D102
        self.client.post(
            reverse("version_control:list", kwargs={"project": self.project.pk}),
            data={"label": "test_version_control321", "link": "www.google.com"},
        )
        test_obj = VersionControl.objects.filter(
            label="test_version_control321"
        ).first()
        self.assertIsNotNone(test_obj)
        self.assertIn(self.project, test_obj.projects.all())
        self.assertEqual("https://www.google.com", test_obj.link)

    def test_update(self):  # noqa: D102
        vc, is_new = VersionControl.objects.get_or_create(
            label="test_version_control456"
        )
        vc.link = "www.google.com"
        vc.projects.add(self.project)
        response = self.client.post(
            reverse(
                "version_control:update",
                kwargs={"project": self.project.pk, "pk": vc.pk},
            ),
            data={"label": "test_version_control789", "link": "www.bing.com"},
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        vc = VersionControl.objects.get(pk=vc.pk)
        self.assertIn(response.context_data["project"], vc.projects.all())
        self.assertEqual("test_version_control789", vc.label)
        self.assertEqual("https://www.bing.com", vc.link)

    def test_delete(self):  # noqa: D102
        vc = VersionControl.objects.create()
        vc.projects.add(self.project)
        response = self.client.post(
            reverse("version_control:list", kwargs={"project": self.project.pk}),
            data={"remove": vc.pk},
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        test_obj = VersionControl.objects.filter(pk=vc.pk, deleted__isnull=True).first()
        self.assertIsNone(test_obj)

    def test_add_protocol_create(self):  # noqa: D102
        self.client.post(
            reverse("version_control:list", kwargs={"project": self.project.pk}),
            data={"label": "test_version_controlA", "link": "www.google.com"},
        )
        test_obj = VersionControl.objects.filter(label="test_version_controlA").first()
        self.assertIsNotNone(test_obj)
        self.assertIn(self.project, test_obj.projects.all())
        self.assertEqual("https://www.google.com", test_obj.link)

    def test_add_protocol_update(self):  # noqa: D102
        vc, is_new = VersionControl.objects.get_or_create(label="test_version_controlB")
        vc.link = "http://www.google.com"
        vc.projects.add(self.project)
        response = self.client.post(
            reverse(
                "version_control:update",
                kwargs={"project": self.project.pk, "pk": vc.pk},
            ),
            data={"label": "test_version_controlB", "link": "www.bing.com"},
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        vc = VersionControl.objects.get(pk=vc.pk)
        self.assertEqual("https://www.bing.com", vc.link)
        self.assertIn(response.context_data["project"], vc.projects.all())


class SearchAndAPITest(TestCase):
    """Test search including via API."""

    def setUp(self):
        """Set up project and logged-in heliport user."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.project, is_new = Project.objects.get_or_create(owner=self.user)
        self.client.force_login(self.auth_user)

    def test_access_search(self):
        """Test that search URL is accessible."""
        response = self.client.get(reverse("version_control:search"))
        self.assertEqual(200, response.status_code)

    def test_version_control_findable(self):
        """Test that version control project metadata is findable."""
        vc = VersionControl.objects.create(label="VERSION_CONTROL321")
        vc.projects.add(self.project)
        response = self.client.get(f"{reverse('version_control:search')}?q=321")
        self.assertContains(response, "VERSION_CONTROL321")

    def test_api(self):
        """Test api."""
        vc = VersionControl.objects.create(label="VERSION_CONTROL654")
        vc.projects.add(self.project)
        response = self.client.get("/api/version-control/")
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "VERSION_CONTROL654")

        link = "https://example.com/version-control/"
        response = self.client.post(
            "/api/version-control/",
            data={"label": "My Version Control", "link": link},
        )
        self.assertEqual(201, response.status_code)
        self.assertEqual(VersionControl.objects.last().link, link)
