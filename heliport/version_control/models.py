# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains :class:`django.db.models.Model` classes for Django ORM.

See :ref:`django:topics/db/models:quick example` from Django documentation.
In HELIPORT the :class:`heliport.core.models.DigitalObject` can be subclassed for models
containing metadata in a project.
"""

import io
import logging
import re
from abc import ABC, abstractmethod
from base64 import b64decode
from dataclasses import dataclass
from mimetypes import guess_type
from typing import Any, Dict, Iterable, Optional, Type, Union
from urllib.parse import quote, urlencode, urlparse

import requests
from django.conf import settings
from django.db import models
from django.template import Context as TemplateContext
from django.template import Template

from heliport.core.digital_object_aspects import Directory, DirectoryObj, File, FileObj
from heliport.core.digital_object_interface import GeneralDigitalObject
from heliport.core.models import (
    DigitalObject,
    DigitalObjectAttributes,
    HeliportUser,
    LoginInfo,
    MetadataField,
    Vocabulary,
)
from heliport.core.permissions import allowed_objects
from heliport.core.utils.collections import DecoratorDict, RootedRDFGraph
from heliport.core.utils.context import Context
from heliport.core.utils.exceptions import UserMessage
from heliport.core.utils.normalization import url_normalizer
from heliport.core.utils.string_tools import format_byte_count, remove_prefix

logger = logging.getLogger(__name__)


class VersionControl(DigitalObject, DirectoryObj):
    """Represent a version control project in the database as digital object."""

    version_control_id = models.AutoField(primary_key=True)
    link = MetadataField(Vocabulary.primary_topic, url_normalizer)

    def as_directory(self, context: Context) -> Directory:
        """Return a directory object to brows this version control."""
        self.access(context).assert_read_permission()
        return build_directory(self.link, context)

    @property
    def is_subdir(self) -> Optional[bool]:
        """
        Determine if this is a version control project itself
        or a directory inside another version control project.
        """  # noqa: D205
        link = self.link
        if link is None:
            return None
        for regex in settings.HELIPORT_VERSION_CONTROL_API_MAPPING:
            match = regex.fullmatch(self.link)
            if match:
                return "path" in match.groupdict()
        return None

    @property
    def icon(self):
        """The icon of the system where this version control resides in."""
        icons = {
            "GitHub": "fa-brands fa-github",
            "GitLab": "fa-brands fa-gitlab",
        }
        link = self.link
        if not link:
            return "fa-solid fa-face-meh-blank"
        for regex, (
            vc_system,
            _,
        ) in settings.HELIPORT_VERSION_CONTROL_API_MAPPING.items():
            match = regex.fullmatch(link)
            if match and vc_system in icons:
                return icons[vc_system]
        return "fa-solid fa-triangle-exclamation"

    @property
    def unsupported(self):  # noqa: D102
        return not self.link or not any(
            regex.fullmatch(self.link) is not None
            for regex in settings.HELIPORT_VERSION_CONTROL_API_MAPPING
        )


class VersionControlFile(DigitalObject, FileObj):
    """represent a file of a version control system inside the database."""

    git_hub_file_id = models.AutoField(primary_key=True)
    link = models.TextField()
    system = models.CharField(max_length=10)

    def as_file(self, context: Context) -> File:
        """Return as a file that can be opened and read."""
        assert self.system in file_types
        return file_types[self.system](self.link, context)


directory_types = DecoratorDict()
file_types = DecoratorDict()


def build_directory(link: str, context: Context) -> Directory:
    """Build an instance of the appropriate type of browsable directory for the given link."""  # noqa: E501
    for regex, (
        vc_system,
        api_url,
    ) in settings.HELIPORT_VERSION_CONTROL_API_MAPPING.items():
        match = regex.fullmatch(link)
        if match and vc_system in directory_types:
            return directory_types[vc_system](match, api_url, context)
    raise UserMessage("The type of version control url is not supported")


class BaseVersionControlObj(ABC, GeneralDigitalObject):
    """Base class for files and directories inside version control systems."""

    def __init__(self, link, label=None, description=None):  # noqa: D107
        self.label = label
        self.description = description
        self.link = link

    @staticmethod
    @abstractmethod
    def get_existing(link, context: Context):
        """Get existing object in database if it exists."""

    @property
    @abstractmethod
    def get_model_class(self) -> Type[DigitalObject]:
        """Get the digital object subclass where this type can be stored in the database."""  # noqa: E501

    @classmethod
    def build_instance(cls, params, **kwargs):
        """Get a new instance of this type, from identifying parameters.

        See :mod:`heliport.core.digital_object_resolution` for reason.

        See :meth:`get_identifying_params` for where the params come from.
        """
        return cls(
            params["link"],
            label=params.get("label"),
            description=params.get("description"),
            **kwargs,
        )

    def get_new(self, context, **kwargs):
        """Get a new database object for the current instance."""
        new = self.get_model_class(
            label=self.label, description=self.description, **kwargs
        )
        new.category_str = settings.VERSION_CONTROL_NAMESPACE
        new.save()
        if context.project_or_none:
            new.projects.add(context.project_or_none)
        return new

    def as_digital_object(self, context: Context):
        """
        Transform the current object into a digital object in the database;
        reuse existing.
        """  # noqa: D205
        existing = self.get_existing(self.link, context)
        return existing or self.get_new(context)

    def as_text(self) -> str:
        """Use label if it exists, link otherwise."""
        return self.label or self.link

    def as_rdf(self) -> RootedRDFGraph:
        """Transform to rdf by link."""
        return RootedRDFGraph.from_uri_str(self.link)

    def as_html(self) -> str:
        """Generate clickable link with :meth:`as_text`."""
        t = Template('<a href="{{ link }}">{{ s }}</a>')
        c = TemplateContext({"link": self.link, "s": self.as_text()})
        return t.render(c)

    def __hash__(self):
        """Hashable."""
        return hash(self.link)

    def __eq__(self, other):
        """Comparable."""  # noqa: D401
        if isinstance(other, type(self)):
            return other.link == self.link
        return False

    def get_identifying_params(self) -> Dict[str, str]:
        """Parameters which can be used to resolve this object."""
        return {
            "type": self.type_id(),
            "link": self.link,
            "label": self.label,
            "description": self.description,
        }

    @classmethod
    def resolve(
        cls, params: Dict[str, str], context: Context
    ) -> Optional[GeneralDigitalObject]:
        """Resolve from identifying parameters.

        See :mod:`heliport.core.digital_object_resolution`
        """
        assert params["type"] == cls.type_id()
        if "link" not in params:
            return None
        existing = cls.get_existing(params["link"], context)
        return existing or cls.build_instance(params)


class VersionControlObj(BaseVersionControlObj, DirectoryObj):
    """Represent a version control or version control directory not stored in the database."""  # noqa: E501

    def __init__(self, link, label=None, description=None):  # noqa: D107
        super().__init__(link, label, description)

    def as_directory(self, context: Context) -> Directory:
        """Return an object for browsing the directory."""
        return build_directory(self.link, context)

    @staticmethod
    def get_existing(link, context):
        """Implement how to get existing equivalent object from database if it exists."""  # noqa: E501
        vocab = Vocabulary()
        links = DigitalObjectAttributes.objects.filter(
            predicate=vocab.primary_topic, value=link
        )
        return (
            allowed_objects(context.user_or_none, VersionControl)
            .filter(deleted__isnull=True, attributes__in=links)
            .first()
        )

    @property
    def get_model_class(self) -> Type[DigitalObject]:
        """Define what django model class should be used to store this object in DB."""
        return VersionControl

    def get_new(self, context, **kwargs):
        """Also use link as parameter; see also base class implementation."""
        new = super().get_new(context, **kwargs)
        new.link = self.link
        return new

    @staticmethod
    def type_id() -> str:
        """Use this string to represent the version control type in URLs."""
        return "VersionControl"


class VersionControlFileObj(FileObj, BaseVersionControlObj):
    """represent a file from version control not stored in the database."""

    def __init__(  # noqa: D107
        self, link: str, system: str, label=None, description=None
    ):
        super().__init__(link, label=label, description=description)
        self.system = system

    def as_file(self, context: Context) -> File:
        """Return a file object that can be opened and read."""
        assert self.system in file_types
        return file_types[self.system](self.link, context)

    @staticmethod
    def get_existing(link, context):
        """Define how to get an existing equivalent existing file from the database."""
        possibilities = allowed_objects(context.user_or_none, VersionControlFile)
        if context.project_or_none:
            possibilities = possibilities.filter(projects=context.project_or_none)
        return possibilities.filter(link=link).first()

    @property
    def get_model_class(self) -> Type[DigitalObject]:
        """
        Version control file objects can be stored in the database as
        :class:`VersionControlFile`.
        """  # noqa: D205
        return VersionControlFile

    def get_new(self, context, **kwargs):
        """Include system and link in new instances; see also base implementation."""
        return super().get_new(context, system=self.system, link=self.link, **kwargs)

    def get_identifying_params(self) -> Dict[str, str]:
        """Use also system to resolve files; see also base implementation."""
        ps = super().get_identifying_params()
        ps["system"] = self.system
        return ps

    @classmethod
    def build_instance(cls, params, **kwargs):
        """Use also system to resolve files; see also base implementation."""
        return super().build_instance(params, system=params["system"], **kwargs)

    @staticmethod
    def type_id() -> str:
        """Represent type as "VersionControlFile" in urls."""
        return "VersionControlFile"


def fetch_json(
    url,
    system,
    obj_for_error: str = "version control",
    only_list=False,
    context=None,
    user: HeliportUser = None,
) -> Any:
    """Fetch json including error handling."""
    if user is None:
        response = requests.get(url)
    else:
        response = get_with_token(system, url, user, context)

    if not response.ok:
        logger.debug(f"url access failed: {url}")
        raise UserMessage(
            f"error while fetching {obj_for_error} contents: {response.status_code}"
        )
    try:
        response_json = response.json()
    except requests.exceptions.JSONDecodeError:
        logger.error(f"url was not json: {url}")
        raise UserMessage(
            f"error while fetching {obj_for_error} contents: invalid json"
        ) from None
    if not isinstance(response_json, list) and only_list:
        logger.error(f"url was not list: {url}")
        raise UserMessage(
            f"error while fetching {obj_for_error} contents: invalid response"
        )
    return response_json


@directory_types.register("GitHub")
class GitHubDirectory(Directory):
    """GitHub specific directory object."""

    def __init__(  # noqa: D107
        self, regex_match: re.Match, api_url: str, context: Context
    ):
        self.match = regex_match
        self.api_url = api_url
        self.context = context

    def get_parts(self) -> Iterable[Union[FileObj, DirectoryObj]]:
        """Get files and directories inside this directory via api."""
        group = self.match.group("group")
        project = self.match.group("project")
        path = self.match.groupdict().get("path")
        branch = self.match.groupdict().get("branch")
        url = f"{self.api_url}/repos/{group}/{project}/contents"
        if path:
            url = f"{url}/{path}"
        if branch:
            url = f"{url}?ref={branch}"

        for directory_item in fetch_json(
            url,
            "GitHub",
            only_list=True,
            context=self.context,
            user=self.context.user_or_none,
        ):
            self.context.cache(
                VersionControlFileMetadata(
                    self.context.user_or_none, directory_item["url"], "GitHub"
                ),
                directory_item,
            )
            if directory_item["type"] == "dir":
                yield VersionControlObj(
                    directory_item["html_url"], directory_item["name"]
                )
            else:
                yield VersionControlFileObj(
                    directory_item["url"], "GitHub", directory_item["name"]
                )

    def get_file_info(self) -> Dict[str, str]:
        """No file info available."""
        return {}


class StreamOrSmallFile(File, ABC):
    """Base class for files that can be streamed if large or can use cached content."""

    def __init__(self):  # noqa: D107
        self.stream = None
        self.stream_generator = None
        self.file_response = None

    def read(self, number_of_bytes=None) -> bytes:
        """Read bytes from the stream or cached content."""
        if self.stream is not None:
            return self.stream.read(number_of_bytes)
        if self.file_response is not None:
            if not self.stream_generator:
                self.stream_generator = self.file_response.iter_content(number_of_bytes)
            try:
                return next(self.stream_generator)
            except StopIteration:
                return b""
        else:
            raise AssertionError("Open file before reading!")

    def close(self):
        """Close streams and set back to ``None``."""
        if self.file_response is not None:
            self.file_response.close()
        if self.stream is not None:
            self.stream.close()
        self.file_response = None
        self.stream_generator = None
        self.stream = None


@file_types.register("GitHub")
class GitHubFile(StreamOrSmallFile, File):
    """GitHub specific readable file."""

    #: The GitHub API link where the file metadata can be downloaded
    link: str

    def __init__(self, link, context):  # noqa: D107
        super().__init__()
        self.link = link
        self.context = context

    @property
    def metadata(self):
        """Get cached or new :class:`VersionControlFileMetadata`."""
        return self.context[
            VersionControlFileMetadata(self.context.user_or_none, self.link, "GitHub")
        ]

    def mimetype(self) -> str:
        """Guess mimetype from file name; default to text/plain."""
        mimetype, encoding = guess_type(self.metadata["name"])
        if mimetype is None:
            mimetype = "text/plain"  # just text data is good guess for git repo
        return mimetype

    def open(self):
        """Open file; use cached content for small files or make a streaming request."""
        self.close()
        if self.metadata["encoding"] == "base64":  # large files have encoding None
            self.stream = io.BytesIO(b64decode(self.metadata["content"]))
        else:
            self.file_response = requests.get(
                self.metadata["download_url"], stream=True
            )

    def size(self) -> Optional[int]:
        """Get file size in bytes."""
        return self.metadata["size"]

    def get_file_info(self) -> Dict[str, str]:
        """Include the size for the user to see."""
        return {"size": format_byte_count(self.size())}


@directory_types.register("GitLab")
class GitLabDirectory(Directory):
    """gitlab specific directory implementation."""

    def __init__(  # noqa: D107
        self, regex_match: re.Match, api_url: str, context: Context
    ):
        self.match = regex_match
        self.api_url = api_url
        self.context = context

    def get_parts(self) -> Iterable[Union[FileObj, DirectoryObj]]:
        """Retrieve files and directories inside this directory from gitlab."""
        project_path = self.match.group("project")
        project = project_path.replace("/", "%2F")
        path = self.match.groupdict().get("path")
        branch = self.match.groupdict().get("branch")
        repository_url = f"{self.api_url}/projects/{project}/repository"
        tree_url = f"{repository_url}/tree"
        query = {}
        if path:
            query["path"] = path
        if branch:
            query["ref"] = branch
        else:
            branches_url = f"{self.api_url}/projects/{project}"
            repo_info = fetch_json(
                branches_url,
                "GitLab",
                "GitLab-branch",
                context=self.context,
                user=self.context.user_or_none,
            )
            branch = repo_info["default_branch"]
        url = f"{tree_url}?{urlencode(query)}" if query else tree_url

        for directory_item in fetch_json(
            url,
            "GitLab",
            only_list=True,
            context=self.context,
            user=self.context.user_or_none,
        ):
            if directory_item["type"] == "tree":
                info = urlparse(self.api_url)
                sub_path = directory_item["path"]
                child_url = f"{info.scheme}://{info.hostname}/{project_path}/-/tree/{branch}/{sub_path}"
                yield VersionControlObj(child_url, directory_item["name"])
            else:
                url_path = quote(directory_item["path"]).replace("/", "%2F")
                child_url = f"{repository_url}/files/{url_path}/raw?ref={branch}"
                yield VersionControlFileObj(child_url, "GitLab", directory_item["name"])

    def get_file_info(self) -> Dict[str, str]:
        """No useful file info available."""
        return {}


@file_types.register("GitLab")
class GitLabFile(StreamOrSmallFile, File):
    """gitlab specific readable file."""

    #: The GitLab API url where file can be downloaded. This must contain
    #: ".../raw?ref=..." because the metadata url is obtained by removing "/raw" from
    #: before the query part of the url
    url_in_api: str

    def __init__(self, link, context):  # noqa: D107
        super().__init__()
        self.url_in_api = link
        self.context = context

    @property
    def metadata(self):
        """Get metadata json from gitlab or use cached one."""
        url = self.url_in_api.replace("/raw?", "?")
        return self.context[
            VersionControlFileMetadata(self.context.user_or_none, url, "GitLab")
        ]

    def mimetype(self) -> str:
        """Guess mimetype from file name; default is text/plain."""
        mimetype, encoding = guess_type(self.metadata["file_name"])
        if mimetype is None:
            mimetype = "text/plain"  # just text data is good guess for git repo
        return mimetype

    def open(self):
        """Open cached content or make a streaming request."""
        self.close()
        if self.metadata["encoding"] == "base64":
            self.stream = io.BytesIO(b64decode(self.metadata["content"]))
        else:
            self.file_response = requests.get(self.url_in_api, stream=True)

    def size(self) -> Optional[int]:
        """File size in bytes."""
        return self.metadata["size"]

    def get_file_info(self) -> Dict[str, str]:
        """Size is not included in file info because it is slow (many requests)."""
        return {}


@dataclass(frozen=True)
class TokenInfo:
    """to cache the correct GitLab or GitHub token."""

    #: e.g. "GitHub" or "GitLab"
    system: str
    #: the user
    user: HeliportUser


def make_token_request(url, login):
    """Make a GET request to the url using authorization header from token login."""
    token = remove_prefix(
        login.key, ["Authorization: Bearer ", "Bearer ", "Authorization: "]
    )
    return requests.get(url, headers={"Authorization": "Bearer " + token})


def get_with_token(system: str, url: str, user: HeliportUser, context: Context = None):
    """
    Make an API GET request to the url and return the result.
    Try to find a matching token; use context for caching it.
    """  # noqa: D205
    info = TokenInfo(system, user)
    if context is not None:
        login = context.get_cached(info)
        if login is not None:
            return make_token_request(url, login)

    logins = LoginInfo.objects.filter(user=user, type=LoginInfo.LoginTypes.TOKEN)
    logins = sorted(
        logins,
        key=lambda login: system.casefold() in str(login.name).casefold(),
        reverse=True,
    )
    for login in logins:
        result = make_token_request(url, login)
        if result.ok:
            if context is not None:
                context.cache(info, login)
            return result

    return requests.get(url)


@dataclass(frozen=True)
class VersionControlFileMetadata:
    """implement :class:`heliport.core.utils.context.Description`."""

    user: HeliportUser
    link: str
    system: str

    def generate(self, context):
        """Fetch json from link."""
        return fetch_json(
            self.link,
            self.system,
            f"{self.system} file metadata",
            context=context,
            user=context.user_or_none,
        )
