# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Django app configuration.

Some HELIPORT hooks can be registered in app config :meth:`django.apps.AppConfig.ready`.

See also :ref:`Django documentation <django:ref/applications:applications>`
"""

from django.apps import AppConfig

from heliport.core.digital_object_resolution import object_types


class VersionControlConfig(AppConfig):
    """App configuration for the main HELIPORT app."""

    name = "heliport.version_control"

    def ready(self):
        """Register object types and import settings."""
        from .conf import VersionControlAppConf
        from .models import VersionControlFileObj, VersionControlObj

        assert VersionControlAppConf
        object_types.register(VersionControlObj)
        object_types.register(VersionControlFileObj)
