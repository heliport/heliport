# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Module with special name "interface" hooks into HELIPORT.

Some functions and :class:`heliport.core.app_interaction.Module` subclasses are detected
by HELIPORT and control how HELIPORT uses this app.

Note that this module must be imported in __init__.py of the django app.
"""

from django.urls import reverse

from heliport.core.app_interaction import DigitalObjectModule


class VersionControlModule(DigitalObjectModule):
    """Show version controls in HELIPORT project graph."""

    name = "Version Control"
    module_id = "version_control"
    icon = "fa-brands fa-git-alt"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("version_control:list", kwargs={"project": project.pk})

    @property
    def object_class(self):
        """VersionControl."""
        from .models import VersionControl

        return VersionControl


def get_search_url():
    """Return the search URL for this app.

    This URL is used to implement the global HELIPORT string search.
    """
    return reverse("version_control:search")
