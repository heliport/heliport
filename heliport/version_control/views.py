# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

import json

import django_filters
import requests
from django.conf import settings
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import CreateView, TemplateView, UpdateView
from rest_framework import filters as rest_filters

from heliport.core.mixins import (
    HeliportLoginRequiredMixin,
    HeliportObjectMixin,
    HeliportProjectMixin,
)
from heliport.core.models import LoginInfo, Project
from heliport.core.permissions import is_object_member
from heliport.core.views import HeliportModelViewSet

from .models import VersionControl
from .serializers import VersionControlSerializer


class VersionControlView(HeliportProjectMixin, CreateView):
    """Show list of version control projects linked to a HELIPORT project.

    See also :class:`VersionControlUpdateView` which contains overlapping functionality.
    """

    model = VersionControl
    template_name = "version_control/version_control.html"
    fields = ["label"]
    projects_per_page = 30

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the users projects that are queried via API from
        version control.
        """
        context = super().get_context_data(**kwargs)
        project = context["project"]
        vc_list = list(
            VersionControl.objects.filter(projects=project, deleted__isnull=True)
        )
        context["vc_actions"] = [
            vc.actions(self.request.user.heliportuser, project) for vc in vc_list
        ]
        context["vc_list"] = vc_list
        context["update"] = False

        page = int(self.request.GET.get("page", "1"))
        search = self.request.GET.get("q")
        starred = self.request.GET.get("starred", "false").lower() != "false"
        if self.request.GET.get("toggle_starred", "false").lower() != "false":
            starred = not starred

        projects = self.download_projects(page, search, starred)
        context["projects_found"] = projects is not None
        if projects is None:
            projects = []
        context["projects"] = [self.insert_import_info(p) for p in projects]
        context["is_last_page"] = len(projects) < self.projects_per_page
        context["page"] = page
        context["next_page"] = page + 1
        context["prev_page"] = page - 1
        context["prev_prev_page"] = page - 2
        context["search"] = search
        context["starred"] = starred
        context["project"] = project
        context["already_added_urls"] = {vc.link for vc in vc_list}
        return context

    @staticmethod
    def insert_import_info(project):
        """Add info to VC project that can be used to import it to HELIPORT."""
        import_info = {}
        import_info["link"] = project.get(
            "web_url", "data:application/JSON;charset=utf-8,%22no%20repository%20URL%22"
        )
        import_info["label"] = project.get("name", "unnamed")
        project["import_info"] = json.dumps(import_info)
        return project

    def download_projects(self, page, search=None, starred=False):
        """Get version control projects of user via API."""
        projects = None
        for login in LoginInfo.objects.filter(
            user=self.request.user.heliportuser, type=LoginInfo.LoginTypes.TOKEN
        ):
            header = "PRIVATE-TOKEN: "
            token = login.key
            if token.startswith(header):
                token = token[len(header) :]
            query = {
                "simple": True,
                "membership": True,
                "order_by": "last_activity_at",
                "per_page": self.projects_per_page,
                "page": page,
            }
            if search:
                query["search"] = search
            if starred:
                query["starred"] = True

            result = requests.get(
                "https://codebase.helmholtz.cloud/api/v4/projects",
                headers={"PRIVATE-TOKEN": token},
                params=query,
            )
            if result.status_code == 200:
                projects = result.json()
                break

        return projects

    def post(self, request, *args, **kwargs):
        """Handle post request.

        Handle addition or removal of Version control projects to HELIPORT.
        """
        version_control_id = request.POST.get("remove")
        import_info = request.POST.get("import")
        if version_control_id:
            version_control = get_object_or_404(VersionControl, pk=version_control_id)
            version_control.mark_deleted(self.request.user.heliportuser)
            return redirect("version_control:list", **self.kwargs)
        if import_info:
            project = get_object_or_404(Project, pk=self.kwargs["project"])
            import_data = json.loads(import_info)
            all_existing = filter(
                lambda v: v.link == import_data["link"],
                VersionControl.objects.filter(
                    projects=project, attributes__value=import_data["link"]
                ),
            )
            existing = next(all_existing, None)
            if existing:
                existing.deleted = None
                existing.save()
            else:
                new_vc = VersionControl()
                for key, value in import_data.items():
                    if key != "link":
                        setattr(new_vc, key, value)
                new_vc.category_str = settings.VERSION_CONTROL_NAMESPACE
                new_vc.save()
                new_vc.link = import_data["link"]
                new_vc.projects.add(project)
            return redirect("version_control:list", project=project.pk)
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        """Set additional info for newly created version control project metadata.

        Called by Django after form is valid.
        """
        form.instance.category_str = settings.VERSION_CONTROL_NAMESPACE

        form.instance.save()
        form.instance.link = self.request.POST.get("link")
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        form.instance.projects.add(project)
        return super().form_valid(form)

    def get_success_url(self):
        """Reload the list. Called by Django."""
        return reverse("version_control:list", kwargs=self.kwargs)


class VersionControlUpdateView(HeliportObjectMixin, UpdateView):
    """Show list of version control projects linked to a HELIPORT project and edit one.

    See also :class:`VersionControlView` which contains overlapping functionality.
    """

    model = VersionControl
    template_name = "version_control/version_control.html"
    fields = ["label"]

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view)."""
        context = super().get_context_data(**kwargs)
        context["vc_list"] = list(
            VersionControl.objects.filter(
                projects=context["project"], deleted__isnull=True
            )
        )
        context["vc_actions"] = [
            vc.actions(self.request.user.heliportuser, context["project"])
            for vc in context["vc_list"]
        ]
        context["update"] = True
        return context

    def form_valid(self, form):
        """Set link, which must be set after save.

        The link is a :class:`heliport.core.models.MetadataField`.

        Called by Django after form is valid.
        """
        form.instance.save()
        form.instance.link = self.request.POST.get("link")
        return super().form_valid(form)

    def get_success_url(self):  # noqa: D102
        """Go back to list. Called by Django."""
        return reverse(
            "version_control:list", kwargs={"project": self.kwargs["project"]}
        )


class SearchView(HeliportLoginRequiredMixin, TemplateView):
    """Get and render search results."""

    template_name = "version_control/search.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the search results.
        """
        context = super().get_context_data(**kwargs)
        user = self.request.user.heliportuser
        query = self.request.GET.get("q", "")

        version_control_results = set()

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            version_control_results.update(
                VersionControl.objects.filter(
                    is_object_member(user)
                    & Q(projects__deleted__isnull=True, deleted__isnull=True)
                    & (
                        Q(version_control_id=num)
                        | Q(label__icontains=word)
                        | Q(attributes__value__icontains=word)
                    )
                )
            )

        context["version_control_results"] = version_control_results
        return context


##########################################
#               REST API                 #
##########################################


class VersionControlViewSet(HeliportModelViewSet):
    """Version Control."""

    serializer_class = VersionControlSerializer
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    filterset_fields = ["version_control_id", "label"]
    search_fields = ["attributes__value"]

    def get_queryset(self):
        """Get version controls that current user has access to."""
        user = self.request.user.heliportuser
        return VersionControl.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()
