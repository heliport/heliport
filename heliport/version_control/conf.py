# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Additional settings for this app.

See also `django appconf <https://django-appconf.readthedocs.io>`_
"""

import re

import environ
from appconf import AppConf

env = environ.Env()


class VersionControlAppConf(AppConf):
    """Settings of the :mod:`heliport.version_control` app.

    All settings in this class can be overwritten in settings.py
    """

    gitlab_project_pattern = "(?P<project>[^/]+(/[^/]*[^-/][^/]*)*)"
    path_pattern = "(?P<branch>[^/]+)/(?P<path>.*)"
    gitlab_path_pattern = f"{gitlab_project_pattern}/-/tree/{path_pattern}"
    API_MAPPING = {
        # codebase
        re.compile(rf"https://codebase\.helmholtz\.cloud/{gitlab_project_pattern}/?"): (
            "GitLab",
            "https://codebase.helmholtz.cloud/api/v4",
        ),
        re.compile(rf"https://codebase\.helmholtz\.cloud/{gitlab_path_pattern}"): (
            "GitLab",
            "https://codebase.helmholtz.cloud/api/v4",
        ),
        # github
        re.compile(r"https://github\.com/(?P<group>[^/]+)/(?P<project>[^/]+)/?"): (
            "GitHub",
            "https://api.github.com",
        ),
        re.compile(
            r"https://github\.com/(?P<group>[^/]+)/(?P<project>[^/]+)/tree/(?P<branch>[^/]+)/(?P<path>.*)"
        ): ("GitHub", "https://api.github.com"),
        # gitlab
        re.compile(rf"https://gitlab\.com/{gitlab_project_pattern}/?"): (
            "GitLab",
            "https://gitlab.com/api/v4",
        ),
        re.compile(rf"https://gitlab\.com/{gitlab_path_pattern}"): (
            "GitLab",
            "https://gitlab.com/api/v4",
        ),
        # old codebase domain
        re.compile(rf"https://gitlab\.hzdr\.de/{gitlab_project_pattern}/?"): (
            "GitLab",
            "https://codebase.helmholtz.cloud/api/v4",
        ),
        re.compile(rf"https://gitlab\.hzdr\.de/{gitlab_path_pattern}"): (
            "GitLab",
            "https://codebase.helmholtz.cloud/api/v4",
        ),
    }

    class Meta:  # noqa: D106
        prefix = "HELIPORT_VERSION_CONTROL"
