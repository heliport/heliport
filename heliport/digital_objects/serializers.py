# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""This module is for serialization into Datacite, RDF and JSON.

The JSON format is used for API endpoints via
`Django rest framework <https://www.django-rest-framework.org/>`_. Using this, it is
typical to put the serializer classes into a "serializers.py" file.

For the serialization into RDF, attributes are described.
See :class:`heliport.core.attribute_description.BaseAttribute` for more detail.
"""

from rest_framework import serializers

from heliport.core.models import DigitalObject, DigitalObjectRelation
from heliport.core.permissions import projects_of


class DigitalObjectSerializer(serializers.ModelSerializer):  # noqa: D101
    def __init__(self, *args, **kwargs):
        """Override querysets to only allow current user's objects."""
        super().__init__(*args, **kwargs)
        user = self.context["request"].user.heliportuser
        self.fields["projects"].child_relation.queryset = projects_of(user)

    class Meta:  # noqa: D106
        model = DigitalObject
        fields = [
            "digital_object_id",
            "projects",
            "persistent_id",
            "category",
            "label",
            "description",
            "co_owners",
            "owner",
        ]


class DigitalObjectRelationSerializer(serializers.ModelSerializer):
    """Serializes a relation between two objects for the relation API.

    A relation is serialized as a triple of subject predicate and object.
    The serializer returns some read-only extra information. To create a new relation
    only the digital object ids of subject, predicate and object can be used.
    """

    subject__label = serializers.CharField(source="subject.label", read_only=True)
    subject__persistent_id = serializers.CharField(
        source="subject.persistent_id", read_only=True
    )
    predicate__label = serializers.CharField(source="predicate.label", read_only=True)
    predicate__persistent_id = serializers.CharField(
        source="predicate.persistent_id", read_only=True
    )
    object__label = serializers.CharField(source="object.label", read_only=True)
    object__persistent_id = serializers.CharField(
        source="object.persistent_id", read_only=True
    )

    class Meta:
        """Define the serializer using Django Rest Framework."""

        model = DigitalObjectRelation
        fields = [
            "digital_object_relation_id",
            "subject",
            "subject__label",
            "subject__persistent_id",
            "predicate",
            "predicate__label",
            "predicate__persistent_id",
            "object",
            "object__label",
            "object__persistent_id",
        ]
