# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains :class:`django.db.models.Model` classes for Django ORM.

See :ref:`django:topics/db/models:quick example` from Django documentation.
In HELIPORT the :class:`heliport.core.models.DigitalObject` can be subclassed for models
containing metadata in a project.
"""

from django.db import models

from heliport.core.models import DigitalObject


class ObjectGraph(DigitalObject):  # noqa: D101
    object_graph_id = models.AutoField(primary_key=True)

    def delete_all_edges(self):  # noqa: D102
        edges = set()
        for edge in self.visualizationedge_set.all():
            path = set()
            path_obj = edge.path
            while path_obj is not None and path_obj not in path:
                path.add(path_obj)
                path_obj = path_obj.tail

            for path_obj in path:
                path_obj.delete()
            edges.add(edge)

        for edge in edges:
            edge.delete()


class PropertyPath(models.Model):  # noqa: D101
    property_path_id = models.AutoField(primary_key=True)
    property = models.ForeignKey(DigitalObject, on_delete=models.CASCADE)
    property_in_reverse = models.BooleanField(default=False)
    tail = models.ForeignKey("PropertyPath", on_delete=models.SET_NULL, null=True)

    def __str__(self):  # noqa: D105
        return self.get_label()

    def get_label(self):  # noqa: D102
        result = ""
        if self.property is not None:
            result = self.property.label
        if self.property_in_reverse:
            result = f"!{result}"
        return result


class VisualizationEdge(models.Model):  # noqa: D101
    edge_id = models.AutoField(primary_key=True)
    hex_color = models.CharField(max_length=8)
    dashed = models.BooleanField(default=False)
    path = models.ForeignKey(PropertyPath, on_delete=models.SET_NULL, null=True)
    visualization = models.ForeignKey(ObjectGraph, on_delete=models.CASCADE)

    def __str__(self):  # noqa: D105
        dashed_str = ""
        if self.dashed:
            dashed_str = ", dashed=True"
        return (
            f"VisualizationEdge({self.edge_id}, hex_color={self.hex_color}{dashed_str})"
        )

    @property
    def html_color(self):  # noqa: D102
        return f"#{self.hex_color}"

    @html_color.setter
    def html_color(self, value):
        if value.startswith("#"):
            value = value[1:]
        self.hex_color = value

    @property
    def edge_label(self):  # noqa: D102
        if self.path is None:
            return ""
        return " > ".join(obj.get_label() for obj in self.path_list)

    @property
    def path_list(self):  # noqa: D102
        path = []
        path_obj = self.path
        while path_obj is not None and path_obj not in path:
            path.append(path_obj)
            path_obj = path_obj.tail
        return path
