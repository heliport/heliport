# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

import json
from collections import Counter, defaultdict, namedtuple

import django_filters.rest_framework
from django.conf import settings
from django.db.models import Q
from django.http.response import Http404, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.views.generic import DetailView, TemplateView, View
from rest_framework import filters as rest_filters
from rest_framework import permissions as rest_permissions
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response as RestResponse

from heliport.core.mixins import (
    HeliportLoginRequiredMixin,
    HeliportObjectMixin,
    HeliportProjectMixin,
)
from heliport.core.models import (
    DigitalObject,
    DigitalObjectRelation,
    Project,
    Vocabulary,
    assert_not_relation,
    assert_relation,
    assert_relation_truth,
    digital_object_form_label_and_pk,
)
from heliport.core.permissions import has_permission_for, is_object_member
from heliport.core.views import HeliportModelViewSet, HeliportObjectListView

from .models import ObjectGraph, PropertyPath, VisualizationEdge
from .serializers import DigitalObjectRelationSerializer, DigitalObjectSerializer


class ObjectsView(HeliportProjectMixin, TemplateView):  # noqa: D101
    template_name = "digital_objects/digital_objects.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the digital objects to show including e.g. categories
        to filter by.
        """
        context = super().get_context_data(**kwargs)
        project = context["project"]

        edit_object = str(self.request.GET.get("edit_object"))
        if edit_object == "project":
            edit_object = project.digital_object_id
        if isinstance(edit_object, int) or edit_object.isdecimal():
            context["digital_object"] = get_object_or_404(DigitalObject, pk=edit_object)
        context["update"] = "digital_object" in context

        categories = {}
        others_exist = False
        filter_category = "filter_categories" in self.request.GET
        others_selected = (
            "include_category_other" in self.request.GET or not filter_category
        )
        texts = []
        filtered_digital_objects = []

        for digital_object in project.parts.filter(deleted__isnull=True):
            c = digital_object.category_list
            if not c or not c[-1]:
                category_selected = others_selected
                others_exist = True
            else:
                category_selected = f"include_category_{c[-1].pk}" in self.request.GET
                if not filter_category:
                    category_selected = not digital_object.is_subclass
                categories[c[-1]] = category_selected

            if category_selected:
                texts.append((str(digital_object.label).lower(), digital_object))
                texts.append((str(digital_object.description).lower(), digital_object))
                texts.append(
                    (str(digital_object.persistent_id).lower(), digital_object)
                )
                filtered_digital_objects.append(digital_object)

        context["filter_categories"] = {
            c: categories[c]
            for c in sorted(categories, key=lambda o: str(o.label).lower())
        }
        context["other_category_exists"] = others_exist
        context["other_category_selected"] = others_selected

        query = self.request.GET.get("q")
        if not query:
            digital_object_list = sorted(filtered_digital_objects, key=lambda o: o.pk)
        else:
            digital_object_list = []

            triples = defaultdict(set)
            for text, d in texts:
                for i in range(len(text) - 2):
                    triples[text[i : i + 3]].add(d)

            digital_object_counter = Counter()
            if len(query) > 2:
                q = str(query).lower()
                for i in range(len(q) - 2):
                    digital_object_counter.update(triples[q[i : i + 3]])
            else:
                f = filtered_digital_objects
                digital_object_counter.update(
                    d for d in f if d.label and query in d.label
                )
                digital_object_counter.update(
                    d for d in f if d.description and query in d.description
                )
                digital_object_counter.update(
                    d for d in f if d.persistent_id and query in d.persistent_id
                )

            working_list = []
            current_count = None
            for digital_object, count in digital_object_counter.most_common():
                if count != current_count:
                    digital_object_list.extend(sorted(working_list, key=lambda o: o.pk))
                    working_list = []
                working_list.append(digital_object)
            digital_object_list.extend(sorted(working_list, key=lambda o: o.pk))

        context["digital_objects"] = digital_object_list
        context["digital_relations"] = project.relations.all()

        if "highlight" in self.request.GET:
            context["highlight"] = self.request.GET["highlight"]

        context["vocabulary"] = Vocabulary()
        return context

    def post(self, request, *args, **kwargs):  # noqa: D102
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        obj_data = save_get(request.POST, "main_object", "json", {})
        asserted_relations_data = save_get(
            request.POST, "asserted_relations", "json", []
        )
        asserted_not_relations_data = save_get(
            request.POST, "asserted_not_relations", "json", []
        )
        category_list = save_get(request.POST, "category_list", "digital_object_list")
        obj_pk = save_get(obj_data, "pk", "int")
        label = save_get(obj_data, "label", "nonempty_str")
        persistent_id = save_get(obj_data, "persistent_id", "nonempty_str")
        description = save_get(obj_data, "description", "str", "")

        if label is None:
            return JsonResponse({"success": False, "message": "could not find label"})
        if category_list is None:
            return JsonResponse(
                {"success": False, "message": "could not find category"}
            )

        asserted_relations = self.fetch_digital_objects_relations(
            asserted_relations_data
        )
        asserted_not_relations = self.fetch_digital_objects_relations(
            asserted_not_relations_data
        )
        if not isinstance(asserted_relations, list):
            return asserted_relations
        if not isinstance(asserted_relations, list):
            return asserted_not_relations

        digital_object, object_existed = get_digital_object_save(obj_pk, create=True)
        digital_object.label = label
        digital_object.description = description
        digital_object.category_list = category_list
        if persistent_id is not None:
            digital_object.set_identifier(persistent_id)
        else:
            digital_object.persistent_id = digital_object.generated_persistent_id
        digital_object.save()
        digital_object.projects.add(project)

        for p, o in asserted_not_relations:
            assert_not_relation(digital_object, p, o)

        for p, o in asserted_relations:
            assert_relation(digital_object, p, o)

        digital_object.save()

        return JsonResponse(
            {
                "success": True,
                "message": "saved object",
                "object": DigitalObjectPropertiesView.serialize_digital_object(
                    digital_object
                ),
            }
        )

    @staticmethod
    def fetch_digital_objects_relations(relation_pk_list):  # noqa: D102
        result = []
        for prop in relation_pk_list:
            pred, existed = get_digital_object_save(prop.get("property_pk"))
            val, existed = get_digital_object_save(prop.get("value_pk"))
            if pred is None:
                return JsonResponse(
                    {
                        "success": False,
                        "message": "could not find some property-digital_object",
                    }
                )
            if val is None:
                return JsonResponse(
                    {"success": False, "message": "could not find some digital_object"}
                )
            result.append((pred, val))
        return result


class DigitalObjectDeleteView(HeliportLoginRequiredMixin, View):  # noqa: D101
    def post(self, request, *args, **kwargs):  # noqa: D102
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        auth_user = request.user
        user = None
        if hasattr(auth_user, "heliportuser"):
            user = auth_user.heliportuser

        if user is not None and user in project.members():
            obj = get_object_or_404(
                DigitalObject, pk=request.POST.get("delete_digital_object")
            )
            if project in obj.projects.all():
                obj.projects.remove(project)
        return redirect("digital_objects:list", project=project.pk)


class DigitalObjectPropertiesView(HeliportLoginRequiredMixin, View):  # noqa: D101
    AttributeInfo = namedtuple("AttributeInfo", "predicate values reverse")

    def get(self, request, *args, **kwargs):  # noqa: D102
        digital_object = self.extract_digital_object(request, param_name="pk")
        self.check_access_allowed(digital_object, request)
        get_reverse_relations = self.extract_boolean_param(
            request, param_name="get_reverse_relations"
        )
        get_forward_relations = self.extract_boolean_param(
            request, param_name="get_forward_relations"
        )
        properties, reverse_properties = self.extract_properties(
            request, param_name="property", reverse_relation_prefix="-"
        )
        not_properties, not_reverse_properties = self.extract_properties(
            request, param_name="has_not_property", reverse_relation_prefix="-"
        )
        forward_relations = []
        reverse_relations = []

        if properties or reverse_properties:
            forward_relations, reverse_relations = self.get_relations(
                digital_object, properties, reverse_properties
            )
        if get_forward_relations:
            forward_relations = self.get_all_forward_relations(digital_object)
        if get_reverse_relations:
            reverse_relations = self.get_all_reverse_relations(digital_object)

        if not_properties or not_reverse_properties:
            if not isinstance(forward_relations, list):
                forward_relations = self.filter_object_has_not_properties(
                    forward_relations, not_properties, not_reverse_properties
                )
            if not isinstance(reverse_relations, list):
                reverse_relations = self.filter_subject_has_not_properties(
                    reverse_relations, not_properties, not_reverse_properties
                )

        if not isinstance(forward_relations, list):
            forward_relations = forward_relations.distinct()
        if not isinstance(reverse_relations, list):
            reverse_relations = reverse_relations.distinct()

        attribute_infos = [
            *self.group_by_property(forward_relations, is_reverse_relation=False),
            *self.group_by_property(reverse_relations, is_reverse_relation=True),
        ]

        return JsonResponse(
            {
                "relations": self.serialize_results(attribute_infos),
                "digital_object": self.serialize_digital_object(digital_object),
            }
        )

    @staticmethod
    def get_all_forward_relations(digital_object):  # noqa: D102
        return DigitalObjectRelation.objects.filter(subject=digital_object)

    @staticmethod
    def get_all_reverse_relations(digital_object):  # noqa: D102
        return DigitalObjectRelation.objects.filter(object=digital_object)

    @staticmethod
    def get_relations(digital_object, properties, reverse_properties):  # noqa: D102
        forward_relations = DigitalObjectRelation.objects.filter(
            subject=digital_object, predicate_id__in=properties
        )
        reverse_relations = DigitalObjectRelation.objects.filter(
            object=digital_object, predicate_id__in=reverse_properties
        )
        return forward_relations, reverse_relations

    @staticmethod
    def filter_subject_has_not_properties(  # noqa: D102
        query, properties, reverse_properties
    ):
        if properties:
            query = query.exclude(subject__relations__predicate_id__in=properties)
        if reverse_properties:
            query = query.exclude(
                subject__reverse_relations__predicate_id__in=reverse_properties
            )
        return query

    @staticmethod
    def filter_object_has_not_properties(  # noqa: D102
        query, properties, reverse_properties
    ):
        if properties:
            query = query.exclude(object__relations__predicate_id__in=properties)
        if reverse_properties:
            query = query.exclude(
                object__reverse_relations__predicate_id__in=reverse_properties
            )
        return query

    @staticmethod
    def check_access_allowed(digital_object, request):  # noqa: D102
        auth_user = request.user
        user = None
        if hasattr(auth_user, "heliportuser"):
            user = auth_user.heliportuser
        if not (
            digital_object.owner is None or has_permission_for(user, digital_object)
        ):
            raise Http404("no DigitalObject matching access rights found")

    @staticmethod
    def extract_digital_object(request, param_name):  # noqa: D102
        pk = request.GET.get(param_name, "None")
        if not str(pk).isdecimal():
            raise Http404(f"DigitalObject-pk is not a decimal number: {pk}")
        return get_object_or_404(DigitalObject, pk=pk)

    @staticmethod
    def extract_boolean_param(request, param_name):  # noqa: D102
        bool_str = str(request.GET.get(param_name, "false"))
        return bool_str.lower() != "false"

    @staticmethod
    def extract_properties(request, param_name, reverse_relation_prefix):  # noqa: D102
        all_properties = request.GET.getlist(param_name)
        properties = [
            p for p in all_properties if not str(p).startswith(reverse_relation_prefix)
        ]
        reverse_properties = [
            p[len(reverse_relation_prefix) :]
            for p in all_properties
            if str(p).startswith(reverse_relation_prefix)
        ]
        for p in properties:
            if not str(p).isdecimal():
                raise Http404(
                    f"DigitalObject-pk of the property is not a decimal number: {p}"
                )
        for p in reverse_properties:
            if not str(p).isdecimal():
                raise Http404(
                    f"DigitalObject-pk of the reverse property is not a decimal number: {p}"  # noqa: E501
                )
        return properties, reverse_properties

    @classmethod
    def group_by_property(  # noqa: D102
        cls, digital_object_relations, is_reverse_relation
    ):
        results = {}
        for rel in digital_object_relations:
            info = results.get(rel.predicate_id)
            if info is None:
                info = cls.AttributeInfo(
                    rel.predicate, values=[], reverse=is_reverse_relation
                )
                results[rel.predicate_id] = info

            if not is_reverse_relation:
                info.values.append(rel.object)
            else:
                info.values.append(rel.subject)
        return results.values()

    @classmethod
    def serialize_results(cls, all_results):  # noqa: D102
        return [
            {
                "predicate": cls.serialize_digital_object(r.predicate),
                "values": [cls.serialize_digital_object(obj) for obj in r.values],
                "reverse": r.reverse,
            }
            for r in all_results
        ]

    @staticmethod
    def serialize_digital_object(obj):  # noqa: D102
        return {
            "pk": obj.pk,
            "persistent_id": obj.persistent_id,
            "label": obj.label,
            "category": obj.category_str,
            "category_list": [
                c.pk if c is not None else None for c in obj.category_list
            ],
            "description": obj.description,
            "id_label": obj.identifier_label,
            "id_link": obj.identifier_link,
            "id_display": obj.identifier_display,
        }


class SaveCategoryView(HeliportLoginRequiredMixin, View):  # noqa: D101
    def post(self, request, *args, **kwargs):  # noqa: D102
        is_type = save_get(request.POST, "is_type", "bool", False)
        is_subclass = save_get(request.POST, "is_subclass", "bool", False)
        category = save_get(request.POST, "main_object", "json", {})
        description = save_get(category, "description", "str", "")
        label = save_get(category, "label", "nonempty_str")
        persistent_id = save_get(category, "persistent_id", "nonempty_str")
        parent_pk = save_get(request.POST, "parent_category_pk", "int")
        pk = save_get(category, "pk", "int")
        vocab = Vocabulary()
        category_list = save_get(
            category,
            "category_list",
            "digital_object_list",
            [vocab.HELIPORT, vocab.Namespace],
        )

        if label is None:
            return JsonResponse({"success": False, "message": "label not found"})

        digital_object, object_existed = get_digital_object_save(pk, create=True)
        parent, parent_existed = get_digital_object_save(parent_pk)

        digital_object.label = label
        digital_object.description = description
        digital_object.category_list = category_list
        digital_object.save()

        assert_relation(digital_object, vocab.type, vocab.Namespace)
        assert_relation_truth(
            digital_object,
            vocab.type,
            vocab.Class,
            truth=is_type,
        )
        if parent_existed:
            assert_relation(digital_object, vocab.subnamespace_of, parent)
            assert_relation_truth(
                digital_object,
                vocab.subclass_of,
                parent,
                truth=is_subclass,
            )

        if persistent_id is not None:
            digital_object.persistent_id = persistent_id
        digital_object.save()

        result = {
            "success": True,
            "message": "saved category",
            "object": DigitalObjectPropertiesView.serialize_digital_object(
                digital_object
            ),
            "is_namespace": True,
            "is_type": is_type,
        }
        if parent_existed:
            result["is_subnamespace_of"] = (
                DigitalObjectPropertiesView.serialize_digital_object(parent)
            )
        if parent_existed and is_subclass:
            result["is_subclass_of"] = (
                DigitalObjectPropertiesView.serialize_digital_object(parent)
            )
        return JsonResponse(result)


def save_get(collection, key, the_type, default=None):  # noqa: D103
    value = collection.get(key, default)

    if the_type == "bool":
        return str(value).lower() != "false"
    if the_type == "json":
        value = str(value)
        try:
            return json.loads(value)
        except (ValueError, TypeError):
            return default
    elif the_type == "str":
        return str(value)
    elif the_type == "nonempty_str":
        value = str(value)
        return default if value == "" else value
    elif the_type == "int":
        value = str(value)
        return default if not value.isdecimal() else int(value)
    elif the_type == "digital_object_list":
        if isinstance(value, str):
            try:
                value = json.loads(value)
            except (ValueError, TypeError):
                return default
        if not isinstance(value, list):
            return default
        if not value:
            return default
        if not all(str(obj).isdecimal() for obj in value):
            return default
        result = [DigitalObject.objects.filter(pk=p).first() for p in value]
        if None in result:
            return default
        return result
    else:
        raise ValueError(f"not a valid type: {the_type}")


def get_digital_object_save(pk, create=False):  # noqa: D103
    digital_object = None
    existed = False
    if pk is not None and str(pk).isdecimal():
        digital_object = DigitalObject.objects.filter(pk=pk).first()

    if digital_object is not None:
        existed = True
    elif create:
        digital_object = DigitalObject()

    return digital_object, existed


class SearchView(HeliportLoginRequiredMixin, TemplateView):  # noqa: D101
    template_name = "digital_objects/search.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the search results.
        """
        context = super().get_context_data(**kwargs)
        user = self.request.user.heliportuser
        query = self.request.GET.get("q", "")

        digital_objects_results = set()

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            digital_objects_results.update(
                DigitalObject.objects.filter(
                    is_object_member(user)
                    & Q(projects__deleted__isnull=True, deleted__isnull=True)
                    & (
                        Q(digital_object_id=num)
                        | Q(description__icontains=word)
                        | Q(label__icontains=word)
                        | Q(persistent_id__icontains=word)
                        | Q(category__icontains=word)
                        | Q(relations__predicate__label__icontains=word)
                        | Q(reverse_relations__predicate__label__icontains=word)
                    )
                )
            )

        context["digital_object_results"] = digital_objects_results
        return context


class ObjectGraphView(HeliportObjectMixin, DetailView):  # noqa: D101
    template_name = "digital_objects/object_graph.html"
    model = ObjectGraph

    EdgeType = namedtuple("EdgeType", "obj path")
    Edge = namedtuple("Edge", "start end viz")

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the generated graph.
        """
        context = super().get_context_data(**kwargs)
        self.project = context["project"]
        visualization: ObjectGraph = self.object
        context["edge_types"] = [
            self.EdgeType(edge, edge.path_list)
            for edge in visualization.visualizationedge_set.all()
        ]
        context["nodes"], context["edges"] = self.generate_graph()
        context["Property"] = Vocabulary().Property
        return context

    def generate_graph(self):  # noqa: D102
        visualization: ObjectGraph = self.object
        edges = []
        nodes = set()

        for vis_edge in visualization.visualizationedge_set.all():
            new_edges = self.generate_edges(vis_edge)

            for new_edge in new_edges:
                nodes.add(new_edge.start)
                nodes.add(new_edge.end)
            edges.extend(new_edges)

        node_objects = [
            DigitalObject.objects.filter(digital_object_id=pk).first() for pk in nodes
        ]
        return node_objects, edges

    def generate_edges(self, vis_edge):  # noqa: D102
        path = vis_edge.path_list
        if not path:
            return []

        index_by_end = self.build_index(path[0])
        for path_step in path[1:]:
            index_by_end = self.extend_index(index_by_end, path_step)
        return self.index_to_edge_list(index_by_end, vis_edge)

    def index_to_edge_list(self, index_by_end, vis_edge):  # noqa: D102
        result = []
        for end, starts in index_by_end.items():
            for start in starts:
                result.append(self.Edge(start, end, vis_edge))
        return result

    def build_index(self, path_step):  # noqa: D102
        index_by_end = defaultdict(list)
        for start, end in self.query_relations(path_step):
            index_by_end[end].append(start)
        return index_by_end

    def extend_index(self, index_by_end, path_step):  # noqa: D102
        new_index_by_end = defaultdict(list)
        for start, end in self.query_relations(path_step):
            new_index_by_end[end].extend(index_by_end[start])
        return new_index_by_end

    def query_relations(self, path_step):  # noqa: D102
        query = DigitalObjectRelation.objects.filter(
            predicate=path_step.property,
            subject__projects=self.project,
            object__projects=self.project,
        )
        if path_step.property_in_reverse:
            return query.values_list("object_id", "subject_id")
        return query.values_list("subject_id", "object_id")

    def post(self, request, *args, **kwargs):  # noqa: D102
        message = None
        edges = []
        for edge in request.POST.getlist("edges"):
            vis_edge = VisualizationEdge()
            vis_edge.html_color = request.POST[f"color_{edge}"]
            vis_edge.dashed = f"dashed_{edge}" in request.POST

            path = []
            for prop in request.POST.getlist(f"properties_{edge}"):
                path_obj = PropertyPath()

                pk = request.POST.get(f"property_pk_{prop}")
                label = request.POST.get(f"property_name_{prop}")
                property_object = digital_object_form_label_and_pk(label, pk)
                if property_object:
                    path_obj.property = property_object
                else:
                    message = f'Property {label} with id "{pk}" not found.'

                path_obj.property_in_reverse = f"in_reverse_{prop}" in request.POST

                path.append(path_obj)

            edges.append(self.EdgeType(vis_edge, path))

        if message:
            self.object = self.get_object()
            context = self.get_context_data()
            context["message"] = message
            context["edge_types"] = edges
            return render(request, self.template_name, context=context)

        visualization = self.get_object()
        visualization.delete_all_edges()
        for edge in edges:
            last_path_obj = None
            for path_obj in reversed(edge.path):
                path_obj.tail = last_path_obj
                last_path_obj = path_obj
                path_obj.save()
            edge.obj.path = last_path_obj
            edge.obj.visualization = visualization
            edge.obj.save()

        return redirect(
            "digital_objects:object_graph", project=kwargs["project"], pk=kwargs["pk"]
        )


class ObjectGraphListView(HeliportObjectListView):  # noqa: D101
    model = ObjectGraph
    category = settings.GRAPH_VISUALIZATION_NAMESPACE
    columns = [
        ("ID", "object_graph_id", "small"),
        ("Name", "label", "normal"),
        ("Description", "description", "large"),
    ]
    actions = [
        ("Open", "action_open", "link"),
        ("Edit", "action_edit", "link_secondary"),
        ("Delete", "action_delete", "danger"),
    ]
    edit_fields = [
        ("Name", "label", "normal"),
        ("Description", "description", "large"),
    ]
    list_url = "digital_objects:object_graph_list"
    update_url = "digital_objects:object_graph_update"
    list_heading = "Object Graphs"
    create_heading = "Add an Object Graph"

    def action_open(self, obj):  # noqa: D102
        project = obj.projects.first()
        project_pk = None
        if project is not None:
            project_pk = project.pk
        if project_pk:
            return reverse(
                "digital_objects:object_graph",
                kwargs={"project": project_pk, "pk": obj.pk},
            )
        return None


##########################################
#               REST API                 #
##########################################


class DigitalObjectViewSet(HeliportModelViewSet):
    """
    Digital Objects
    To Query landing page URL append /landing_page_url/ to the url.
    """  # noqa: D205

    serializer_class = DigitalObjectSerializer
    filterset_fields = ["digital_object_id", "persistent_id", "category", "owner"]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return DigitalObject.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()

    @action(detail=True, methods=["get"])
    def landing_page_url(self, request, pk=None):  # noqa: D102
        obj = get_object_or_404(DigitalObject, pk=pk)
        result = {
            "url": obj.full_url(),
            "relative_url": obj.relative_url,
            "local_url": obj.local_url(request),
        }
        return RestResponse(result)


class DigitalObjectRelationViewSet(viewsets.ModelViewSet):
    """Digital Object Relations."""

    serializer_class = DigitalObjectRelationSerializer
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    permission_classes = [rest_permissions.IsAuthenticated]
    filterset_fields = [
        "subject",
        "predicate",
        "object",
        "subject__persistent_id",
        "predicate__persistent_id",
        "object__persistent_id",
    ]
    search_fields = [
        "subject__label",
        "predicate__label",
        "object__label",
        "subject__description",
        "predicate__description",
        "object__description",
    ]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return DigitalObjectRelation.objects.filter(
            (
                Q(subject__projects__owner=user)
                | Q(subject__projects__co_owners=user)
                | Q(predicate__projects__owner=user)
                | Q(predicate__projects__co_owners=user)
                | Q(object__projects__owner=user)
                | Q(object__projects__co_owners=user)
            )
            & (
                Q(subject__deleted__isnull=True)
                | Q(predicate__deleted__isnull=True)
                | Q(object__deleted__isnull=True)
            )
        ).distinct()
