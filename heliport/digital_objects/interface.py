# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Module with special name "interface" hooks into HELIPORT.

Some functions and :class:`heliport.core.app_interaction.Module` subclasses are detected
by HELIPORT and control how HELIPORT uses this app.

Note that this module must be imported in __init__.py of the django app.
"""

from django.urls import reverse

from heliport.core.app_interaction import Module


class DigitalObjectsModule(Module):  # noqa: D101
    name = "Digital Objects"
    module_id = "digital_objects"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("digital_objects:list", kwargs={"project": project.pk})

    def is_configured(self, project):
        """Return whether the module is configured, i.e. to show it in the graph."""
        from heliport.core.models import DigitalObject

        return bool(
            project.relations.all()
            or project.parts.filter(**DigitalObject.not_subclass_query())
        )


class ObjectGraphModule(Module):  # noqa: D101
    name = "Object Graph"
    module_id = "object_graph"
    icon = "fa-solid fa-diagram-project"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse(
            "digital_objects:object_graph_list", kwargs={"project": project.pk}
        )

    def is_configured(self, project):
        """Return whether the module is configured, i.e. to show it in the graph."""
        from .models import ObjectGraph

        return ObjectGraph.objects.filter(projects=project)


def get_search_url():
    """Return the search URL for this app.

    This URL is used to implement the global HELIPORT string search.
    """
    return reverse("digital_objects:search")
