# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path
from rest_framework import routers

from .views import (
    DigitalObjectDeleteView,
    DigitalObjectPropertiesView,
    DigitalObjectRelationViewSet,
    DigitalObjectViewSet,
    ObjectGraphListView,
    ObjectGraphView,
    ObjectsView,
    SaveCategoryView,
    SearchView,
)

app_name = "digital_objects"
urlpatterns = [
    path(
        "project/<int:project>/digital-object/list/",
        ObjectsView.as_view(),
        name="list",
    ),
    path(
        "project/<int:project>/digital-object/delete/",
        DigitalObjectDeleteView.as_view(),
        name="delete",
    ),
    path(
        "digital-object/properties/",
        DigitalObjectPropertiesView.as_view(),
        name="properties",
    ),
    path(
        "digital-object/category/save/",
        SaveCategoryView.as_view(),
        name="save_category",
    ),
    path(
        "project/<int:project>/digital-object/object-graph/<int:pk>/",
        ObjectGraphView.as_view(),
        name="object_graph",
    ),
    path(
        "project/<int:project>/digital-object/object-graph/list/",
        ObjectGraphListView.as_view(),
        name="object_graph_list",
    ),
    path(
        "project/<int:project>/digital-object/object-graph/<int:pk>/update/",
        ObjectGraphListView.as_view(),
        name="object_graph_update",
    ),
    path("digital-object/search/", SearchView.as_view(), name="search"),
]

# REST API
router = routers.DefaultRouter()
router.register(
    r"digital-objects/minimal", DigitalObjectViewSet, basename="digital_objects"
)
router.register(
    r"digital-objects/relations",
    DigitalObjectRelationViewSet,
    basename="digital_object_relations",
)
