/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class PropertyValue {
  constructor(property = null, value = null, suggestion = false) {
    this._property = property;
    this._value = value;
    if (this._property === null) {
      this._property = global_digital_object_manager.get_digital_object();
      this._property.is_property = true;
    }
    if (this._value === null)
      this._value = global_digital_object_manager.get_digital_object();
    this.original_property = this._property;
    this.original_value = this._value;

    this.edit_callback = null;
    this.suggestion_callback = null;
    this.validation_callback = null;
    this.delete_callback = null;
    this.save_callback = null;
    this.edit_mode_callback = null;

    this.reset_components();

    this._disabled = false;
    this._saved = false;
    this._is_suggestion = suggestion;
    this._is_suggested = false;
    this.validation_message = null;
    this.property_change_handler = () => this.handle_change();
  }

  reset_components() {
    this.ui_container = null;
    this.property_container = null;
    this.value_container = null;
    this.property_box = null;
    this.value_box = null;
    this.property_edit_button = null;
    this.value_edit_button = null;
    this.suggested_button = null;
    this.delete_button = null;
    this.save_button = null;
    this.edit_button = null;
  }

  async init_components(parent_div) {
    const html = html_templates;
    this.property_container = html.div(null, ["property-div"]);
    this.value_container = html.div(null, ["value-div"]);
    this.ui_container = html.key_value_row(
      parent_div,
      this.property_container,
      this.value_container,
    );

    this.property_box = html.textbox(
      this.property_container,
      "search for property",
      ["property-box"],
    );
    this.value_box = html.textbox(
      this.value_container,
      "search for corresponding value",
      ["value-box"],
    );
    this.property_edit_button = html.button(
      this.property_container,
      "edit property as digital object",
      ["btn-outline-secondary", "property-edit-button"],
      "arrow-right",
    );
    this.suggested_button = html.button(
      this.property_container,
      "suggest property for selected category",
      ["btn-outline-success", "property-suggested-button"],
      "bookmark",
    );
    this.value_edit_button = html.button(
      this.value_container,
      "edit value as digital object",
      ["btn-outline-secondary", "value-edit-button"],
      "arrow-right",
    );
    this.delete_button = html.button(
      this.value_container,
      "delete",
      ["btn-outline-danger", "value-delete-button"],
      "trash",
    );
    this.save_button = html.button(
      this.value_container,
      "finish changing this property and value",
      ["btn-primary", "value-save-button"],
      "check",
    );
    this.edit_button = html.button(
      this.value_container,
      "edit this property and value",
      ["btn-outline-secondary", "property-value-edit-button"],
      "pencil",
    );

    this.property_edit_button.on("click", (event) => this.on_edit_property());
    this.suggested_button.on("click", (event) =>
      this.suggestion_callback(this.property, !this.is_suggested),
    );
    this.value_edit_button.on("click", (event) => this.on_edit_value());
    this.delete_button.on("click", (event) => this.delete_callback(this));
    this.save_button.on("click", (event) => this.on_save());
    this.edit_button.on("click", (event) => this.edit_mode_callback(this));
    this.property_box.on("input", (event) =>
      this.on_textbox_input(
        "property",
        this.property_box.val(),
        this.original_property,
      ),
    );
    this.value_box.on("input", (event) =>
      this.on_textbox_input("value", this.value_box.val(), this.original_value),
    );

    this.property_box.autocomplete({
      autoFocus: true,
      source: `${global_url_configuration.autocomplete_url}?type=${global_vacabulary_configuration.OProperty.pk}`,
      select: (event, ui) => {
        this.property = global_digital_object_manager.get_digital_object(
          ui.item.pk,
        );
        this.property.label = ui.item.value;
        this.validation_callback();
      },
      response: (event, ui) => this.on_autocomplete_response(event, ui),
    });

    this.value_box.autocomplete({
      autoFocus: true,
      source: global_url_configuration.autocomplete_url,
      select: (event, ui) => {
        this.value = global_digital_object_manager.get_digital_object(
          ui.item.pk,
        );
        this.value.label = ui.item.value;
        this.validation_callback();
      },
      response: (event, ui) => this.on_autocomplete_response(event, ui),
    });

    this.saved = this.property.saved && this.value.saved;
    this.is_suggestion = this._is_suggestion;

    await this.handle_change();
    if (this._property !== null)
      this._property.register(this.property_change_handler);
    if (this._value !== null)
      this._value.register(this.property_change_handler);
  }

  get disabled() {
    return this._disabled;
  }

  set disabled(value) {
    this._disabled = value;
    this.property_box.prop("disabled", this.saved || this._disabled);
    this.value_box.prop("disabled", this.saved || this._disabled);
    this.property_edit_button.prop("disabled", value);
    this.suggested_button.prop("disabled", value);
    this.value_edit_button.prop("disabled", value);
    this.delete_button.prop("disabled", value);
    this.save_button.prop("disabled", value);
    this.edit_button.prop("disabled", value);

    if (this.saved && !this._disabled) {
      this.property_box.addClass("overwrite-disabled");
      this.value_box.addClass("overwrite-disabled");
    } else {
      this.property_box.removeClass("overwrite-disabled");
      this.value_box.removeClass("overwrite-disabled");
    }
  }

  destroy() {
    if (this.ui_container !== null) this.ui_container.remove();
    this._property.unregister(this.property_change_handler);
    this._value.unregister(this.property_change_handler);
    this.reset_components();
  }

  async handle_change() {
    if (!this.is_initialized) return;
    if (this.property !== null)
      this.property_box.val(await this.property.label);
    if (this.value !== null) this.value_box.val(await this.value.label);
  }

  get is_initialized() {
    return this.ui_container !== null;
  }

  get property() {
    return this._property;
  }

  set property(value) {
    this._property.unregister(this.property_change_handler);
    this._property = value;
    this._property.register(this.property_change_handler);
    this.handle_change();
  }

  get value() {
    return this._value;
  }

  set value(value) {
    this._value.unregister(this.property_change_handler);
    this._value = value;
    this._value.register(this.property_change_handler);
    this.handle_change();
  }

  get saved() {
    return this._saved;
  }

  set saved(value) {
    this._saved = value;
    this.property_box.prop("disabled", value);
    this.value_box.prop("disabled", value);
    this.save_button.prop("hidden", value);
    this.edit_button.prop("hidden", !value);
    if (value) this.ui_container.addClass("property-is_saved");
    else this.ui_container.removeClass("property-is_saved");
    if (!this.disabled && value) {
      this.property_box.addClass("overwrite-disabled");
      this.value_box.addClass("overwrite-disabled");
    } else {
      this.property_box.removeClass("overwrite-disabled");
      this.value_box.removeClass("overwrite-disabled");
    }
  }

  get is_suggestion() {
    return this._is_suggestion;
  }

  set is_suggestion(value) {
    this._is_sugestion = value;
    if (value) this.ui_container.addClass("property-is_suggestion");
    else this.ui_container.removeClass("property-is_suggestion");
  }

  get is_suggested() {
    return this._is_suggested;
  }

  set is_suggested(value) {
    this._is_suggested = value;
    if (value) {
      this.ui_container.addClass("property-is_suggested");
      this.suggested_button.addClass("btn-success");
      this.suggested_button.removeClass("btn-success-outline");
    } else {
      this.ui_container.removeClass("property-is_suggested");
      this.suggested_button.removeClass("btn-success");
      this.suggested_button.addClass("btn-success-outline");
    }
  }

  on_textbox_input(property_str, label, original_digital_object) {
    if (this[property_str].saved)
      this[property_str] = global_digital_object_manager.get_digital_object();
    this[property_str].label = label;
    if (this[property_str].label === original_digital_object.label)
      this[property_str] = original_digital_object;
    this.is_suggestion = false;
    this.validation_callback();
  }

  on_autocomplete_response(event, ui) {
    let label_index = new Map();
    for (let suggestion of ui.content) {
      let label_unique = !label_index.has(suggestion.label);
      label_index.set(suggestion.label, label_unique);
    }
    for (let suggestion of ui.content) {
      if (!label_index.get(suggestion.label))
        suggestion.label = `${suggestion.label} (${suggestion.pk})`;
    }
  }

  async on_edit_property() {
    this.edit_callback(await this.parse_property());
  }

  async on_edit_value() {
    this.edit_callback(await this.parse_value());
  }

  async on_save() {
    await this.parse_property();
    await this.parse_value();
    this.save_callback(this);
  }

  async parse_property() {
    const vocab = global_vacabulary_configuration;

    if (!this.property.saved) {
      let parsed = await this.string_to_object(
        await this.property.label,
        vocab.OProperty.pk,
      );
      if (parsed !== null) this.property = parsed;
    }

    return this.property;
  }

  async parse_value() {
    if (!this.value.saved) {
      let parsed = await this.string_to_object(await this.value.label);
      if (parsed !== null) this.value = parsed;
    }
    return this.value;
  }

  async string_to_object(text, type_pk = null, minimum_exactness = 0) {
    if (minimum_exactness >= 3) return null;

    let query = { term: text };
    if (minimum_exactness === 0) query.exact = false;
    if (minimum_exactness === 1) query.exact = "case_insensitive";
    if (minimum_exactness === 2) query.exact = true;
    if (type_pk !== null) query.type = type_pk;

    let data = await $.getJSON(
      global_url_configuration.autocomplete_url,
      query,
    );
    if (!("length" in data)) return null;
    if (data.length < 1) return null;
    if (data.length > 1)
      return this.string_to_object(text, type_pk, minimum_exactness + 1);

    let item = data[0];
    let new_object = null;
    if ("label" in item && "pk" in item) {
      new_object = global_digital_object_manager.get_digital_object(item.pk);
      new_object.label = item.label;
    }
    return new_object;
  }
}
