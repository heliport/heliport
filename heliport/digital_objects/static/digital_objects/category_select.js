/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class CategorySelect {
  constructor(category = null, parent_category = null, is_root = true) {
    this._category = category;
    this.parent_category = parent_category;
    this.is_root = is_root;

    this.goto_callback = null;
    this.message_callback = null;
    this.selection_callback = null;
    this.validation_callback = null;
    this._edit_mode = false;
    this._show_edit_button = false;
    this._disabled = false;
    this._option_callbacks = [];
    this.user_preferes_edit_mode = false;

    this.parent_change_handler = () => this.handle_parent_change();
    this.category_change_handler = () => this.handle_category_change();
    this.option_change_handler = (digital_object) =>
      this.handle_option_changed(digital_object);

    if (category === null || category.saved)
      this.new_category = global_digital_object_manager.get_digital_object();
    else this.new_category = category;

    this.reset_components();
  }

  reset_components() {
    this.ui_container = null;
    this.edit_container = null;
    this.select_box = null;
    this.label_box = null;
    this.label_box_label = null;
    this.description_box = null;
    this.description_box_label = null;
    this.is_type_check_box = null;
    this.is_type_check_box_label = null;
    this.is_subclass_check_box = null;
    this.is_subclass_check_box_label = null;
    this.edit_button = null;
    this.reset_button = null;
    this.save_button = null;
    this.goto_button = null;
  }

  async init_components(parent_div) {
    const html = html_templates;

    this.ui_container = html.div(parent_div, ["category-selection-container"]);
    this.edit_container = html.div(this.ui_container, [
      "category-edit-container",
    ]);
    this.edit_container.prop("hidden", true);

    this.edit_button = html.button(
      this.ui_container,
      "edit",
      ["btn-outline-secondary", "category-edit-button"],
      "pencil",
    );

    this.label_box_label = html.label("label", this.edit_container, [
      "category-label",
    ]);
    this.label_box = html.textbox(this.label_box_label);

    this.description_box_label = html.label(
      "description",
      this.edit_container,
      ["category-description"],
    );
    this.description_box = html.textarea(this.description_box_label);

    this.is_type_check_box_label = html.label(
      " is class",
      this.edit_container,
      ["category-is_type"],
    );
    this.is_type_check_box = html.checkbox();
    this.is_type_check_box.prependTo(this.is_type_check_box_label);

    this.is_subclass_check_box_label = html.label(
      " is subclass",
      this.edit_container,
      ["category-is_subclass"],
    );
    this.is_subclass_check_box = html.checkbox();
    this.is_subclass_check_box.prependTo(this.is_subclass_check_box_label);
    this.is_subclass_check_box_label.prop("hidden", true);

    this.goto_button = html.button(this.edit_container, "more", [
      "btn-secondary",
      "category-button",
    ]);
    this.reset_button = html.button(this.edit_container, "reset", [
      "btn-danger",
      "category-button",
    ]);
    this.save_button = html.button(this.edit_container, "save", [
      "btn-primary",
      "category-button",
    ]);

    this.edit_button.on("click", (event) => this.on_edit());
    this.goto_button.on("click", (event) => this.on_open());
    this.save_button.on("click", (event) => this.on_save());
    this.reset_button.on("click", (event) => this.on_reset());

    this.label_box.on("input", (event) => this.on_form_input());
    this.description_box.on("input", (event) => this.on_form_input());
    this.is_type_check_box.on("change", (event) => this.on_form_input());
    this.is_subclass_check_box.on("change", (event) => this.on_form_input());

    this.edit_mode = this.category === this.new_category;
    this.show_edit_button =
      this.category !== null && this.category !== this.new_category;

    await this.init_new_category();
    await this.update();

    if (this.category !== null)
      this.category.register(this.parent_change_handler);
    if (this.parent_category !== null)
      this.parent_category.register(this.parent_change_handler);
  }

  get disabled() {
    return this._disabled;
  }

  set disabled(value) {
    this._disabled = value;
    this.select_box.prop("disabled", value);
    this.label_box.prop("disabled", value);
    this.description_box.prop("disabled", value);
    this.is_type_check_box.prop("disabled", value);
    this.is_subclass_check_box.prop("disabled", value);
    this.reset_button.prop("disabled", value);
    this.save_button.prop("disabled", value);
    this.goto_button.prop("disabled", value);
  }

  destroy() {
    if (this.category !== null)
      this.category.unregister(this.category_change_handler);
    if (this.parent_category !== null)
      this.parent_category.unregister(this.parent_change_handler);
    this.set_option_callbacks([]);
    if (this.ui_container !== null) this.ui_container.remove();
    this.reset_components();
  }

  get is_initialized() {
    return this.ui_container !== null;
  }

  async init_new_category() {
    const vocab = global_vacabulary_configuration;

    if (this.parent_category !== null)
      this.new_category.set_has_parent_namespace(this.parent_category, true);
    let category_step_of_new_category =
      await this.new_category.first_category_step;
    let second_category_step_of_new_category =
      await category_step_of_new_category.next;
    let category_of_new_category = await category_step_of_new_category.category;
    if (category_of_new_category === null) {
      this.new_category.select_category(
        category_step_of_new_category,
        vocab.OHeliport,
      );
      this.new_category.select_category(
        second_category_step_of_new_category,
        vocab.ONamespace,
      );
    }
  }

  async update_select_element() {
    if (!this.is_initialized) return;
    let parent_label;
    let option_data;

    if (this.parent_category !== null) {
      parent_label = await this.parent_category.label;
      option_data = await this.parent_category.subnamespaces;
    } else {
      parent_label = "";
      option_data = await global_digital_object_manager.get_root_namespaces();
    }

    let option_labels = new Map();
    for (let option of option_data) {
      let option_label = await option.label;
      if (option_labels.has(option_label))
        option_labels.set(option_label, "many");
      else option_labels.set(option_label, "one");
    }

    let options = [];
    for (let option of option_data) {
      let option_text = await option.label;
      if (option_labels.get(option_text) === "many")
        option_text = `${option_text} (${option.pk})`;
      options.push(new Option(option_text, option.pk));
    }

    if (this.select_box !== null) this.select_box.remove();
    this.select_box = html_templates.select();
    this.select_box.prependTo(this.ui_container);

    let default_text = this.is_root ? "choose category" : "choose subcategory";
    let disable_default = true;
    if (
      this.parent_category !== null &&
      (await this.parent_category.is_class)
    ) {
      default_text = `⊱ use ${parent_label} ⊰`;
      disable_default = false;
    }

    let default_option = new Option(default_text, "");
    default_option.disabled = disable_default;
    this.select_box.append(default_option);
    for (let option of options) this.select_box.append(option);
    this.select_box.append(new Option("⋇⋇⋇ create new category ⋇⋇⋇", "new"));

    if (this.category === null) this.select_box.val("");
    else if (this.category === this.new_category) this.select_box.val("new");
    else this.select_box.val(this.category.pk);

    this.set_option_callbacks(option_data);
    this.select_box.on("change", (event) => this.on_select());
  }

  async update_category_form() {
    if (!this.is_initialized) return;
    const vocab = global_vacabulary_configuration;

    let label = "";
    let parent_label = "";
    let description = "";
    let is_type = false;
    let parent_is_type = false;
    let is_subclass = false;

    if (this.category !== null) {
      label = await this.category.label;
      description = await this.category.description;
      is_type = await this.category.is_class;
    }
    if (this.parent_category !== null) {
      parent_label = await this.parent_category.label;
      parent_is_type = await this.parent_category.is_class;
    }
    if (is_type && parent_is_type) {
      is_subclass = await this.category.get_relation(
        vocab.p_subclass_of,
        this.parent_category,
      );
    }
    this.is_subclass_check_box_label.prop(
      "hidden",
      !(is_type && parent_is_type),
    );

    let nonempty_label = label;
    let nonempty_parent_label = parent_label;
    if (nonempty_label === "")
      nonempty_label = "digital object of this category";
    if (nonempty_parent_label === "")
      nonempty_parent_label = "digital object of the parent category";
    let a = "a";
    for (let p of "aeiou") if (nonempty_label.startsWith(p)) a = "an";
    let are_a = `are ${a}`;
    let is_a = `is ${a}`;
    let is_a_class = "is a class";
    let is_a_subclass_of = "is a subclass of";
    let some_things = "Some things";
    let every = "Every";
    let is_type_text = ` ${some_things} ${are_a} ${nonempty_label} ("${label}" ${is_a_class})`;
    let is_subclass_text = ` ${every} ${nonempty_parent_label} ${is_a} ${nonempty_label} ("${label}" ${is_a_subclass_of} "${parent_label}")`;

    this.label_box.val(label);
    this.description_box.val(description);
    this.is_type_check_box.prop("checked", is_type);
    this.is_subclass_check_box.prop("checked", is_subclass);
    this.is_type_check_box_label.children("span").text(is_type_text);
    this.is_subclass_check_box_label.children("span").text(is_subclass_text);
  }

  async handle_parent_change() {
    await this.update();
  }

  async handle_category_change() {
    await this.update();
  }

  async update() {
    await this.update_select_element();
    await this.update_category_form();
  }

  async handle_option_changed(digital_object) {
    if (digital_object !== this.category) await this.update_select_element();
  }

  set_option_callbacks(digital_objects_list) {
    for (let oc of this._option_callbacks)
      oc.unregister(this.option_change_handler);
    this._option_callbacks = digital_objects_list;
    for (let oc of this._option_callbacks)
      oc.register(this.option_change_handler);
  }

  get category() {
    return this._category;
  }

  set category(value) {
    if (this._category !== null)
      this._category.unregister(this.category_change_handler);
    this._category = value;
    if (this._category !== null)
      this._category.register(this.category_change_handler);
    this.update().then();
  }

  get edit_mode() {
    return this._edit_mode;
  }

  set edit_mode(value) {
    this._edit_mode = value;
    this.edit_container.prop("hidden", !value);
  }

  get show_edit_button() {
    return this._show_edit_button;
  }

  set show_edit_button(value) {
    this._show_edit_button = value;
    this.edit_button.prop("hidden", !value);
  }

  async on_edit() {
    this.edit_mode = !this.edit_mode;
    this.user_preferes_edit_mode = this.edit_mode;
  }

  async on_open() {
    this.goto_callback(this.category);
  }

  async on_save() {
    let data = await global_digital_object_manager.save_category(
      this.category,
      this.parent_category,
    );
    this.new_category = global_digital_object_manager.get_digital_object();
    await this.init_new_category();
    this.message_callback(data.message, data.success);
    this.validation_callback();
  }

  async on_reset() {
    await this.category.refresh();
    this.message_callback("category successfully reset", true);
  }

  async on_form_input() {
    const vocab = global_vacabulary_configuration;

    if (this.category !== null) {
      this.category.label = this.label_box.val();
      this.category.description = this.description_box.val();
      let is_type = this.is_type_check_box.prop("checked");
      let is_subclass = this.is_subclass_check_box.prop("checked");

      this.category.set_relation(vocab.p_type, vocab.OClass, is_type);
      if (this.parent_category !== null)
        this.category.set_relation(
          vocab.p_subclass_of,
          this.parent_category,
          is_subclass,
        );
    }
  }

  async on_select() {
    let selection = this.select_box.val();
    let category;

    if (selection === "new") {
      this.edit_mode = true;
      this.show_edit_button = true;
      category = this.new_category;
    } else if (selection === "") {
      this.edit_mode = false;
      this.show_edit_button = false;
      category = null;
    } else {
      this.edit_mode = this.user_preferes_edit_mode;
      this.show_edit_button = true;
      category = global_digital_object_manager.get_digital_object(selection);
    }

    this.category = category;
    this.selection_callback(this.parent_category, category);
  }
}
