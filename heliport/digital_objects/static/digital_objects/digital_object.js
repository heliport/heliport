/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class DigitalObject {
  constructor(pk = null) {
    this._pk = pk === null ? null : parseInt(pk);
    if (isNaN(this._pk)) throw TypeError(`"${pk}" is not an integer`);

    this._label = this.saved ? null : "";
    this._description = this.saved ? null : "";
    this._persistent_id = this.saved ? null : "";
    this._id_link = this.saved ? null : "";
    this._id_label = this.saved ? null : "Persistent ID";
    this._id_display = this.saved ? null : "";
    this._category = this.saved ? null : [];

    this._properties = [];
    this.properties_downloaded = false;

    this.change_handlers = new Set();
  }

  // properties based on core properties
  get label() {
    return this.get_core_property("label");
  }

  set label(value) {
    this.set_core_property("label", value);
  }

  get description() {
    return this.get_core_property("description");
  }

  set description(value) {
    this.set_core_property("description", value);
  }

  get persistent_id() {
    return this.get_core_property("persistent_id");
  }

  set persistent_id(value) {
    this.set_core_property("persistent_id", value);
  }

  get id_link() {
    return this.get_core_property("id_link");
  }

  set id_link(value) {
    this.set_core_property("id_link", value);
  }

  get id_label() {
    return this.get_core_property("id_label");
  }

  set id_label(value) {
    this.set_core_property("id_label", value);
  }

  get id_display() {
    return this.get_core_property("id_display");
  }

  set id_display(value) {
    return this.set_core_property("id_display", value);
  }

  // properties based on category
  get first_category_step() {
    return this.access_category().then(() => new CategoryStep(this, 0));
  }

  get last_category_step() {
    return this.access_category().then(() => {
      if (this._category.length === 0) return null;
      else return new CategoryStep(this, this._category.length - 1);
    });
  }

  get last_category() {
    return this.last_category_step.then((s) => {
      if (s === null) return null;
      return s.category;
    });
  }

  get category_length() {
    return this.access_category().then(() => this._category.length);
  }

  get category_class() {
    return this.get_category_class();
  }

  async get_category_class() {
    let c = await this.last_category;
    if (c === null) return null;
    let is_class = await c.is_class;
    if (!is_class) return null;
    return c;
  }

  async get_next_category_step(category_step = null) {
    let new_idx = this._category.length;
    if (category_step !== null) new_idx = category_step.i + 1;

    return new CategoryStep(this, new_idx);
  }

  async get_previous_category_step(category_step) {
    if (category_step.i === 0) return null;

    return new CategoryStep(this, category_step.i - 1);
  }

  select_category(category_step, category) {
    if (category_step.i > this._category.length) {
      if (category !== null) throw Error("previous category not selected");
      return;
    }

    this._category.splice(category_step.i);
    if (category !== null) this._category.push(category);
    this.invoke_change_handlers().then();
  }

  get_category_at(category_step) {
    if (!(category_step instanceof CategoryStep) || !category_step.is_for(this))
      throw TypeError(
        'index has to be an object from the "first_category_index" property of this object',
      );

    if (category_step.i >= this._category.length) return null;
    else return this._category[category_step.i];
  }

  // boolean properties
  assert_relation(predicate, object) {
    this.set_relation(predicate, object, true);
  }

  assert_not_relation(predicate, object) {
    this.set_relation(predicate, object, false);
  }

  get is_class() {
    return this.get_relation(
      global_vacabulary_configuration.p_type,
      global_vacabulary_configuration.OClass,
    );
  }

  set is_class(value) {
    this.set_relation(
      global_vacabulary_configuration.p_type,
      global_vacabulary_configuration.OClass,
      value,
    );
  }

  get is_property() {
    return this.get_relation(
      global_vacabulary_configuration.p_type,
      global_vacabulary_configuration.OProperty,
    );
  }

  set is_property(value) {
    this.set_relation(
      global_vacabulary_configuration.p_type,
      global_vacabulary_configuration.OProperty,
      value,
    );
  }

  async get_has_suggested_property(property) {
    return this.get_relation(
      global_vacabulary_configuration.p_suggested_property,
      property,
    );
  }

  set_suggested_property(property, truth) {
    this.set_relation(
      global_vacabulary_configuration.p_suggested_property,
      property,
      truth,
    );
  }

  async get_has_parent_namespace(namespace) {
    return this.get_relation(
      global_vacabulary_configuration.p_subnamespace_of,
      namespace,
    );
  }

  set_has_parent_namespace(namespace, truth) {
    this.set_relation(
      global_vacabulary_configuration.p_subnamespace_of,
      namespace,
      truth,
    );
  }

  // properties returning list of values
  get suggested_properties() {
    return this.get_relation_values(
      global_vacabulary_configuration.p_suggested_property,
    );
  }

  get subnamespaces() {
    return this.get_relation_values(
      global_vacabulary_configuration.p_subnamespace_of,
      true,
    );
  }

  get parent_namespaces() {
    return this.get_relation_values(
      global_vacabulary_configuration.p_subnamespace_of,
    );
  }

  get types() {
    return this.get_relation_values(global_vacabulary_configuration.p_type);
  }

  // getting property values
  get property_tuples() {
    return this.get_relation_property_values().then((p) => {
      return p.map((pv) => [pv.predicate, pv.object]);
    });
  }

  get properties() {
    return this.get_relation_property_values();
  }

  /*
   * core data manipulation functions (all data manipulation should go through thees functions)
   */

  async refresh() {
    this.reset();
    if (this.saved) await this.access_category(); //possible race condition if category accessed while not there
    await this.invoke_change_handlers();
  }

  set_core_property(name, value) {
    if (value === null)
      throw new TypeError("properties of digital object can not be null");
    if (!this.core_property_names.has(name))
      throw Error(`unknown property: ${name}`);
    name = "_" + name;
    let old_value = this[name];
    this[name] = value;
    if (old_value !== value) this.invoke_change_handlers();
  }

  set_relation(predicate, object, truth) {
    let idx = this._properties.findIndex(
      (e) => predicate.eq(e.predicate) && object.eq(e.object),
    );
    if (idx >= 0) {
      let relation = this._properties[idx];
      let old_value = relation.truth;
      relation.truth = truth;
      if (old_value !== truth) this.invoke_change_handlers();
    } else {
      let relation = new RelationRepresentation(
        predicate,
        object,
        false,
        truth,
      );
      this._properties.push(relation);
      this.invoke_change_handlers();
    }
  }

  async access_category() {
    if (this._category === null)
      await global_digital_object_manager.set_core_properties(this);
  }

  async get_core_property(name) {
    if (!this.core_property_names.has(name))
      throw Error(`unknown property: ${name}`);
    name = "_" + name;
    if (this[name] === null)
      await global_digital_object_manager.set_core_properties(this);
    return this[name];
  }

  async get_relation(predicate, object) {
    let properties = this._properties;
    let idx = properties.findIndex(
      (p) => predicate.eq(p.predicate) && object.eq(p.object),
    );
    if (idx < 0 && !this.properties_downloaded) {
      properties = await this.get_relation_property_values();
      idx = properties.findIndex(
        (p) => predicate.eq(p.predicate) && object.eq(p.object),
      );
    }

    if (idx >= 0) return properties[idx].truth;
    else return false;
  }

  async get_relation_values(predicate, reverse = false) {
    if (!reverse) {
      let properties = await this.get_relation_property_values();
      return properties
        .filter((p) => p.predicate.eq(predicate))
        .map((p) => p.object);
    } else
      return global_digital_object_manager.get_values_for_reverse(
        this,
        predicate,
      );
  }

  async get_relation_property_values() {
    if (!this.properties_downloaded && this.saved) {
      let new_properties =
        await global_digital_object_manager.get_properties(this);
      let property_index = new Map();
      for (let p of new_properties)
        property_index.set(`${p.predicate.pk}/${p.object.pk}`, p);
      for (let p of this._properties)
        if (!p.reverse)
          property_index.delete(`${p.predicate.pk}/${p.object.pk}`);

      for (let p of property_index.values()) this._properties.push(p);
      this.properties_downloaded = true;
    }
    return this._properties.filter((p) => !p.reverse && p.truth);
  }

  async save() {
    return await global_digital_object_manager.save(this);
  }

  /*
   * management functionality (without touching data)
   */

  eq(other) {
    if (!this.saved) return this === other;
    return this.pk === other.pk;
  }

  get pk() {
    return this._pk;
  }

  get saved() {
    return this.pk != null;
  }

  reset() {
    this._label = this.saved ? null : "";
    this._description = this.saved ? null : "";
    this._persistent_id = this.saved ? null : "";
    this._category = this.saved ? null : [];

    this._properties = [];
    this.properties_downloaded = false;
  }

  async invoke_change_handlers() {
    for (let callback of this.change_handlers) await callback(this);
  }

  register(callback) {
    this.change_handlers.add(callback);
  }

  unregister(callback) {
    this.change_handlers.delete(callback);
  }

  get core_property_names() {
    return new Set([
      "label",
      "description",
      "persistent_id",
      "id_link",
      "id_label",
      "id_display",
    ]);
  }

  async get_json() {
    await this.access_category();
    return {
      pk: this.pk,
      label: await this.label,
      description: await this.description,
      persistent_id: await this.persistent_id,
      category_list: this._category.map((c) => c.pk),
    };
  }

  get asserted_relations() {
    return this._properties.filter((r) => r.truth && !r.reverse);
  }

  get asserted_not_relations() {
    return this._properties.filter((r) => !r.truth && !r.reverse);
  }
}

class CategoryStep {
  constructor(digital_object, index) {
    this._digital_object = digital_object;
    this._index = index;
  }

  get category() {
    return this.get_category();
  }

  async get_category() {
    return this._digital_object.get_category_at(this);
  }

  is_for(digital_object) {
    return digital_object === this._digital_object;
  }

  get index() {
    return this._index;
  }

  get i() {
    return this.index;
  }

  get next() {
    return this._digital_object.get_next_category_step(this);
  }

  get previous() {
    return this._digital_object.get_previous_category_step(this);
  }

  get parent_category() {
    return this.get_parent_category();
  }

  async get_parent_category() {
    let prev = await this.previous;

    if (prev === null) return null;
    else return this._digital_object.get_category_at(prev);
  }

  get options() {
    return this.get_options();
  }

  async get_options() {
    let parent = await this.get_parent_category();

    if (parent === null)
      return global_digital_object_manager.get_root_namespaces();
    else return parent.subnamespaces;
  }

  get parent_is_class() {
    return this.get_parent_is_class();
  }

  async get_parent_is_class() {
    let parent = await this.get_parent_category();

    if (parent === null) return false;
    else return parent.is_class;
  }

  get is_class() {
    return this.get_is_class();
  }

  async get_is_class() {
    let c = await this.get_category();
    if (c === null) return false;
    return c.is_class;
  }

  get is_subclass() {
    return this.get_is_subclass();
  }

  async get_is_subclass() {
    let parent = await this.get_parent_category();
    let obj = await this.get_category();
    if (parent === null || obj === null) return false;
    return obj.get_relation(
      global_vacabulary_configuration.p_subclass_of,
      parent,
    );
  }

  get parent_class() {
    return this.get_parent_class();
  }

  async get_parent_class() {
    let parent = await this.get_parent_category();
    if (parent === null || !(await parent.is_class)) return null;
    return parent;
  }

  get category_json() {
    return this.get_category_json();
  }

  async get_category_json() {
    let c = await this.get_category();
    if (c === null) throw Error("no category selected");
    return c.get_json();
  }

  register(callback) {
    this._digital_object.register(callback);
  }

  unregister(callback) {
    this._digital_object.unregister(callback);
  }

  select_category_pk(pk) {
    pk = parseInt(pk);
    this.select_category(global_digital_object_manager.get_digital_object(pk));
  }

  select_category(category) {
    this._digital_object.select_category(this, category);
  }
}

class RelationRepresentation {
  constructor(predicate, object, reverse = false, truth = true) {
    this.predicate = predicate;
    this.object = object;
    this.reverse = reverse;
    this.truth = truth;
  }
}

class DigitalObjectManager {
  constructor() {
    this.objects_by_pk = new Map();
  }

  async save(digital_object) {
    await digital_object.access_category();
    let data = await $.post(
      global_url_configuration.object_save,
      {
        main_object: JSON.stringify(await digital_object.get_json()),
        asserted_relations: JSON.stringify(
          digital_object.asserted_relations.map((r) => {
            return { property_pk: r.predicate.pk, value_pk: r.object.pk };
          }),
        ),
        asserted_not_relations: JSON.stringify(
          digital_object.asserted_not_relations.map((r) => {
            return { property_pk: r.predicate.pk, value_pk: r.object.pk };
          }),
        ),
        category_list: JSON.stringify(
          digital_object._category.map((c) => c.pk),
        ),
        csrfmiddlewaretoken: global_url_configuration.csrf_token,
      },
      (data) => data,
      "json",
    );

    if (!digital_object.saved && "object" in data && "pk" in data.object) {
      digital_object._pk = data.object.pk;
      this.objects_by_pk.set(digital_object.pk, digital_object);
      digital_object.invoke_change_handlers();
    }

    return data;
  }

  async save_category(category, parent = null) {
    const vocab = global_vacabulary_configuration;

    let post_data = {
      is_type: await category.is_class,
      main_object: JSON.stringify(await category.get_json()),
      csrfmiddlewaretoken: global_url_configuration.csrf_token,
    };
    if (parent !== null) {
      post_data.parent_category_pk = parent.pk;
      post_data.is_subclass = await category.get_relation(
        vocab.p_subclass_of,
        parent,
      );
    }

    let data = await $.post(
      global_url_configuration.category_save,
      post_data,
      (data) => data,
      "json",
    );

    if (!category.saved && "object" in data && "pk" in data.object) {
      category._pk = parseInt(data.object.pk);
      this.objects_by_pk.set(category.pk, category);
      await category.invoke_change_handlers();
    }

    return data;
  }

  async get_root_namespaces() {
    let namespace = global_vacabulary_configuration.ONamespace;
    let type = global_vacabulary_configuration.p_type;
    let subnamespace_of = global_vacabulary_configuration.p_subnamespace_of;
    return this.query(namespace, false, type, true, subnamespace_of).then((l) =>
      l.map((pv) => pv.object),
    );
  }

  async get_properties(digital_object) {
    return this.query(digital_object, true);
  }

  async set_core_properties(digital_object) {
    return this.query(digital_object);
  }

  async get_values_for_reverse(digital_object, predicate) {
    return this.query(digital_object, false, predicate, true).then((l) =>
      l.map((pv) => pv.object),
    );
  }

  async query(
    digital_object,
    all_forward_properties = false,
    property = null,
    reverse = false,
    has_not_property = null,
  ) {
    let object_text = `${digital_object._label}(${digital_object.pk})`;
    let forward_text = all_forward_properties ? "forward" : "";
    let property_text =
      property !== null ? `property ${property._label}(${property.pk})` : "";
    let reverse_text = reverse ? "reverse" : "";
    let not_property_text =
      has_not_property !== null
        ? `not_property ${has_not_property._label}(${has_not_property.pk})`
        : "";

    //console.log(`query  ${object_text} ${property_text} ${not_property_text} ${forward_text} ${reverse_text}`);

    if (!digital_object.saved) throw Error("access to unsaved digital_object");
    if (property !== null && property.pk === null)
      throw Error("access to unsaved digital_object");
    if (has_not_property !== null && has_not_property.pk === null)
      throw Error("access to unsaved digital_object");

    let reverse_str = reverse ? "-" : "";
    let query = { pk: digital_object.pk };
    if (all_forward_properties) query["get_forward_relations"] = true;
    if (property !== null) query["property"] = `${reverse_str}${property.pk}`;
    if (has_not_property !== null)
      query["has_not_property"] = has_not_property.pk;

    let data = await $.getJSON(global_url_configuration.property_url, query);
    if ("digital_object" in data)
      this.parse_into_digital_object(digital_object, data.digital_object);

    let relations = [];
    if ("relations" in data)
      for (let rel of data.relations)
        if ("predicate" in rel && "values" in rel) {
          let predicate = this.parse_new(rel.predicate);
          let this_relations = rel.values.map(
            (v) =>
              new RelationRepresentation(
                predicate,
                this.parse_new(v),
                "reverse" in rel && rel.reverse,
                true,
              ),
          );
          relations = relations.concat(this_relations);
        }

    return relations;
  }

  parse_new(obj) {
    let digital_object = null;
    if ("pk" in obj) digital_object = this.get_digital_object(obj.pk);
    else digital_object = this.get_digital_object();

    this.parse_into_digital_object(digital_object, obj);
    return digital_object;
  }

  parse_into_digital_object(digital_object, obj) {
    if (
      !(
        "label" in obj &&
        "description" in obj &&
        "persistent_id" in obj &&
        "category_list" in obj
      )
    ) {
      console.log("digital object missing some properties:", obj);
      throw Error("got invalid data from backend");
    }

    if (digital_object._label === null)
      digital_object.label = typeof obj.label === "string" ? obj.label : "";
    if (digital_object._description === null)
      digital_object.description =
        typeof obj.description === "string" ? obj.description : "";
    if (digital_object._persistent_id === null)
      digital_object.persistent_id =
        typeof obj.persistent_id === "string" ? obj.persistent_id : "";
    if (digital_object._id_label === null)
      digital_object.id_label =
        typeof obj.id_label === "string" ? obj.id_label : "";
    if (digital_object._id_display === null)
      digital_object.id_display =
        typeof obj.id_display === "string" ? obj.id_display : "";
    if (digital_object._id_link === null)
      digital_object.id_link =
        typeof obj.id_link === "string" ? obj.id_link : "";
    if (digital_object._category === null) {
      if (obj.category_list instanceof Array)
        digital_object._category = obj.category_list.map((c) =>
          this.get_digital_object(c),
        );
      else {
        console.log(
          "got category from backend that was not an Array:",
          obj.category,
        );
        throw Error("got invalid data from backend");
      }
    }
  }

  get_digital_object(pk = null) {
    if (pk === null) return new DigitalObject();
    else {
      pk = parseInt(pk);
      if (this.objects_by_pk.has(pk)) return this.objects_by_pk.get(pk);
    }

    let new_object = new DigitalObject(pk);
    this.objects_by_pk.set(pk, new_object);
    return new_object;
  }
}

global_digital_object_manager = new DigitalObjectManager();
