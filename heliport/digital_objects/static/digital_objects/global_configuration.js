/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class URLConfig {
  constructor() {
    this.autocomplete_url = null;
    this.property_url = null;
    this.category_save = null;
    this.object_save = null;
    this.csrf_token = null;
  }

  is_configured() {
    return (
      this.autocomplete_url !== null &&
      this.property_url !== null &&
      this.category_save !== null &&
      this.object_save !== null &&
      this.csrf_token !== null
    );
  }
}

global_url_configuration = new URLConfig();

class VocabConfig {
  constructor() {
    this.OProperty = null;
    this.ONamespace = null;
    this.OClass = null;
    this.OHeliport = null;
    this.p_type = null;
    this.p_suggested_property = null;
    this.p_subnamespace_of = null;
    this.p_subclass_of = null;
  }

  is_configured() {
    return (
      this.OProperty instanceof DigitalObject &&
      this.ONamespace instanceof DigitalObject &&
      this.OClass instanceof DigitalObject &&
      this.OHeliport instanceof DigitalObject &&
      this.p_type instanceof DigitalObject &&
      this.p_suggested_property instanceof DigitalObject &&
      this.p_subnamespace_of instanceof DigitalObject &&
      this.p_subclass_of instanceof DigitalObject
    );
  }

  download_labels() {
    this.OProperty.label;
    this.ONamespace.label;
    this.OClass.label;
    this.OHeliport.label;
    this.p_type.label;
    this.p_suggested_property.label;
    this.p_subnamespace_of.label;
    this.p_subclass_of.label;
  }
}

global_vacabulary_configuration = new VocabConfig();

class HTMLTemplates {
  div(parent = null, classes = []) {
    return this.html_component("<div></div>", parent, classes);
  }

  select(parent = null, classes = []) {
    classes.unshift("form-select");
    return this.html_component("<select></select>", parent, classes);
  }

  table(parent, columns = null, classes = []) {
    classes.unshift("table");
    let result = this.html_component("<table></table>", parent, classes);

    if (columns !== null) {
      let head = this.html_component("<thead></thead>", result);
      let row = this.html_component("<tr></tr>", head);
      for (let [name, column_classes] of columns) {
        let heading = this.html_component("<th></th>", row, column_classes);
        heading.text(name);
      }
    }
    return result;
  }

  button(
    parent,
    name,
    classes = [],
    icon_symbol = null,
    icon_size = "1em",
    icon_classes = [],
  ) {
    classes.unshift("btn");
    let result = this.html_component("<button></button>", parent, classes);
    if (icon_symbol === null) {
      result.text(name);
    } else {
      result.prop("title", name);
      this.icon(icon_symbol, result, icon_size, icon_classes);
    }
    return result;
  }

  table_row(parent = null, classes = []) {
    return this.html_component("<tr></tr>", parent, classes);
  }

  checkbox(parent = null, classes = []) {
    return this.html_component('<input type="checkbox">', parent, classes);
  }

  textbox(parent = null, placeholder = null, classes = []) {
    classes.unshift("form-control");
    let result = this.html_component("<input/>", parent, classes);
    if (placeholder !== null) result.prop("placeholder", placeholder);
    return result;
  }

  textarea(parent = null, classes = []) {
    classes.unshift("form-control");
    return this.html_component("<textarea></textarea>", parent, classes);
  }

  icon(symbol, parent = null, size = "1em", classes = []) {
    let html_str = `<i class="fa-solid fa-${symbol}" style="font-size: ${size};"></i>`;
    return this.html_component(html_str, parent, classes);
  }

  small(text, parent = null, classes = []) {
    let result = this.html_component("<small></small>", parent, classes);
    result.text(text);
    return result;
  }

  label(text, parent = null, classes = []) {
    let result = this.html_component("<label></label>", parent, classes);
    let span = this.html_component("<span></span>", result);
    span.text(text);
    return result;
  }

  key_value_row(parent, key, value, classes = []) {
    let result = this.table_row(parent, classes);
    let heading = this.html_component('<th class="align-middle"></th>', result);
    if (typeof key === "string") heading.text(key);
    else heading.append(key);

    let data = this.html_component("<td></td>", result);
    if (typeof value === "string") data.text(value);
    else data.append(value);
    return result;
  }

  html_component(html_str, parent = null, classes = []) {
    let component = $(html_str);
    for (let style_class of classes) component.addClass(style_class);
    if (parent !== null) component.appendTo(parent);
    return component;
  }
}

html_templates = new HTMLTemplates();
