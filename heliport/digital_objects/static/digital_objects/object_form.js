/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class ObjectForm {
  constructor(
    digital_object = null,
    message_callback = null,
    close_callback = null,
    is_root = true,
  ) {
    this.digital_object = digital_object;
    this.is_root = is_root;
    this.close_callback = close_callback;
    this.message_callback = message_callback;
    if (this.digital_object === null)
      this.digital_object = global_digital_object_manager.get_digital_object();

    this._disabled = false;
    this._is_valid = false;
    this._validation_messages = [];
    this.property_values = [];
    this.sub_form = null;
    this.selected_category = null;
    this.categories = [];
    this.selected_category_change_handler = () =>
      this.on_selected_category_changed();
    this.obj_change_handler = () => this.handle_change();

    this.reset_components();
    this.digital_object.register(this.obj_change_handler);
  }

  reset_components() {
    this.ui_container = null;
    this.values_container = null;
    this.values_container_body = null;
    this.child_container = null;
    this.category_container = null;
    this.controls_container = null;
    this.validation_container = null;
    this.save_button = null;
    this.cancel_button = null;
    this.add_button = null;
    this.label_tr = null;
    this.label_box = null;
    this.description_tr = null;
    this.description_box = null;
    this.persistent_id_tr = null;
    this.persistent_id_box = null;
  }

  async init_components(parent_div) {
    const html = html_templates;
    let classes = ["object-form"];
    if (!this.is_root) classes.push("object-form-child");
    this.ui_container = html.div(parent_div, classes);

    this.category_container = html.div(this.ui_container);
    this.values_container = html.table(
      this.ui_container,
      [
        ["Property", ["key-column"]],
        ["Value", ["value-column"]],
      ],
      ["property-value-table"],
    );
    this.values_container_body = html.html_component(
      "<tbody></tbody>",
      this.values_container,
    );
    this.validation_container = html.div(this.ui_container);
    this.controls_container = html.div(this.ui_container, [
      "controls-container",
    ]);
    this.child_container = html.div(this.ui_container, ["child-container"]);

    this.add_button = html.button(this.controls_container, "Add Property", [
      "object-form-add-button",
      "btn-primary",
    ]);
    this.save_button = html.button(this.controls_container, "Save", [
      "object-form-save-button",
      "btn-primary",
    ]);
    this.cancel_button = html.button(this.controls_container, "Cancel", [
      "object-form-cancel-button",
      "btn-secondary",
    ]);
    this.add_button.on("click", (event) => this.on_add());
    this.save_button.on("click", (event) => this.on_save());
    this.cancel_button.on("click", (event) => this.on_cancel());

    this.label_box = html.textbox(null, null, ["value-input"]);
    this.description_box = html.textarea(null, ["value-input"]);
    this.persistent_id_box = html.textbox(
      null,
      "generate persistent identifier",
      ["value-input"],
    );
    this.label_tr = html.key_value_row(
      this.values_container_body,
      "Label",
      this.label_box,
    );
    this.description_tr = html.key_value_row(
      this.values_container_body,
      "Description",
      this.description_box,
    );
    this.persistent_id_tr = html.key_value_row(
      this.values_container_body,
      "Identifier",
      this.persistent_id_box,
    );
    this.label_box.on(
      "input",
      (event) => (this.digital_object.label = this.label_box.val()),
    );
    this.description_box.on(
      "input",
      (event) => (this.digital_object.description = this.description_box.val()),
    );
    this.persistent_id_box.on(
      "input",
      (event) =>
        (this.digital_object.persistent_id = this.persistent_id_box.val()),
    );

    await this.handle_change();
  }

  get disabled() {
    return this._disabled;
  }

  set disabled(value) {
    this._disabled = value;
    for (let c of this.categories) if (c.is_initialized) c.disabled = value;
    for (let v of this.property_values)
      if (v.is_initialized) v.disabled = value;
    this.save_button.prop("disabled", value);
    this.cancel_button.prop("disabled", value);
    this.add_button.prop("disabled", value);
    this.label_box.prop("disabled", value);
    this.description_box.prop("disabled", value);
    this.persistent_id_box.prop("disabled", value);

    if (value) this.ui_container.addClass("object-form-disabled");
    else this.ui_container.removeClass("object-form-disabled");
  }

  destroy() {
    this.digital_object.unregister(this.obj_change_handler);
    if (this.selected_category !== null)
      this.selected_category.unregister(this.selected_category_change_handler);
    for (let c of this.categories) c.destroy();
    for (let v of this.property_values) v.destroy();
    this.ui_container.remove();
    this.reset_components();
  }

  scroll_into_view(smooth = true) {
    if (this.ui_container !== null)
      if (smooth) this.ui_container[0].scrollIntoView({ behavior: "smooth" });
      else this.ui_container[0].scrollIntoView();
  }

  async on_selected_category_changed() {
    await this.update_category();
    await this.update_suggested();
  }

  async handle_change() {
    let new_category = await this.digital_object.last_category;
    if (new_category !== this.selected_category) {
      if (this.selected_category !== null)
        this.selected_category.unregister(
          this.selected_category_change_handler,
        );
      this.selected_category = new_category;
      if (this.selected_category !== null)
        this.selected_category.register(this.selected_category_change_handler);
    }

    await this.update_category();
    await this.update_core_properties();
    await this.update_property_values();
    await this.update_suggested();

    await this.validate();
  }

  async update_category() {
    let i = await this.digital_object.first_category_step;
    let categories = [];
    let category = await i.category;
    while (category !== null) {
      categories.push(category);
      i = await i.next;
      category = await i.category;
    }

    let parent = null;
    let j = 0;
    for (; j < this.categories.length; j++) {
      let category = j < categories.length ? categories[j] : null;
      let category_select = this.categories[j];

      let category_end = parent == null && j !== 0;
      let parent_editing = parent !== null && !parent.saved;
      let selection_differs = category !== category_select.category;
      let parent_differs = parent !== category_select.parent_category;

      if (category_end || parent_editing || selection_differs || parent_differs)
        break;
      parent = category;
    }

    for (let c of this.categories.splice(j)) c.destroy();

    while ((parent !== null && parent.saved) || j === 0) {
      let category = j < categories.length ? categories[j] : null;
      this.append_category(category, parent);
      parent = category;
      j++;
    }

    await this.initialize_categories();
  }

  async initialize_categories() {
    for (let c of this.categories)
      if (!c.is_initialized) {
        await c.init_components(this.category_container);
        if (!c.is_initialized) break;
        c.disabled = this.disabled;
      }
  }

  append_category(category, parent) {
    let category_select = new CategorySelect(
      category,
      parent,
      this.categories.length === 0,
    );
    category_select.goto_callback = (digital_object) =>
      this.on_edit(digital_object);
    category_select.selection_callback = (parent, category) =>
      this.on_category_select(parent, category);
    category_select.message_callback = this.message_callback;
    category_select.validation_callback = () => this.validate();
    this.categories.push(category_select);
  }

  async update_property_values() {
    let property_values = await this.digital_object.property_tuples;
    let category_class = await this.digital_object.category_class;
    let suggested_properties =
      category_class !== null ? await category_class.suggested_properties : [];

    let property_index = new Map();
    for (let pv of this.property_values) {
      if (!property_index.has(pv.property))
        property_index.set(pv.property, new Set());

      let value_set = property_index.get(pv.property);
      value_set.add(pv.value);
    }

    let property_added = false;
    for (let [p, v] of property_values) {
      if (!property_index.has(p)) property_added = true;
      else {
        let value_set = property_index.get(p);
        if (!value_set.has(v)) property_added = true;
        else value_set.delete(v);
      }
    }
    let property_deleted = false;
    for (let value_set of property_index.values())
      if (value_set.size > 0) property_deleted = true;

    if (property_added || property_deleted) {
      let saved_property_values = this.property_values.filter(
        (pv) => pv.saved || pv.is_suggestion,
      );
      let unsaved_property_values = this.property_values.filter(
        (pv) => !pv.saved && !pv.is_suggestion,
      );
      this.property_values = [];

      for (let pv of saved_property_values) pv.destroy();
      for (let [p, v] of property_values) this.add_property_value(p, v);
      for (let pv of unsaved_property_values) {
        this.property_values.push(pv);
        if (pv.is_initialized) this.values_container.append(pv.ui_container);
      }

      let property_set = new Set(this.property_values.map((pv) => pv.property));
      for (let p of suggested_properties) {
        if (!property_set.has(p)) this.add_property_value(p, null, true);
      }
    }

    let target_suggestions = new Set(suggested_properties);
    for (let pv of this.property_values) {
      if (!pv.is_suggestion) target_suggestions.delete(pv.property);
    }

    for (let pv of this.property_values) {
      if (pv.is_suggestion) {
        if (target_suggestions.has(pv.property))
          target_suggestions.delete(pv.property);
        else pv.destroy();
      }
    }

    for (let p of target_suggestions) this.add_property_value(p, null, true);

    await this.initialize_property_values();
  }

  async initialize_property_values() {
    for (let pv of this.property_values)
      if (!pv.is_initialized) {
        await pv.init_components(this.values_container);
        if (!pv.is_initialized) break;
        if (pv.property !== null && this.selected_category !== null)
          pv.is_suggested =
            await this.selected_category.get_has_suggested_property(
              pv.property,
            );
        pv.disabled = this.disabled;
      }
  }

  add_property_value(property = null, value = null, suggestion = false) {
    let new_pv = new PropertyValue(property, value, suggestion);
    new_pv.edit_callback = (o) => this.on_edit(o);
    new_pv.suggestion_callback = (property, is_suggested) =>
      this.on_suggest(property, is_suggested);
    new_pv.delete_callback = (pv) => this.on_delete_property(pv);
    new_pv.save_callback = (pv) => this.on_save_property(pv);
    new_pv.validation_callback = () => this.validate();
    new_pv.edit_mode_callback = (pv) => this.on_edit_property_value(pv);
    this.property_values.push(new_pv);
  }

  async update_core_properties() {
    this.label_box.val(await this.digital_object.label);
    this.description_box.val(await this.digital_object.description);
    this.persistent_id_box.val(await this.digital_object.persistent_id);
    this.persistent_id_tr
      .children("th")
      .text(await this.digital_object.id_label);
  }

  async update_suggested() {
    for (let pv of this.property_values)
      if (pv.is_initialized) {
        pv.is_suggested =
          this.selected_category !== null &&
          (await this.selected_category.get_has_suggested_property(
            pv.property,
          ));
      }
  }

  get is_valid() {
    return this._is_valid;
  }

  set is_valid(value) {
    this._is_valid = value;
    if (value) this.save_button.removeClass("disabled");
    else this.save_button.addClass("disabled");

    if (value) this.ui_container.addClass("object-form-valid");
    else this.ui_container.removeClass("object-form-valid");
  }

  set validation_messages(value) {
    let messages = value.map((message) => new ValidationMessage(message));
    let change = messages.length !== this._validation_messages.length;
    if (!change)
      for (let i = 0; i < messages.length; i++) {
        if (!messages[i].eq(this._validation_messages[i])) change = true;
      }

    if (change) {
      for (let message of this._validation_messages) message.destroy();
      this._validation_messages = messages;
      for (let message of this._validation_messages)
        message.init_components(this.validation_container);
    }
  }

  async validate() {
    let validation_messages = [];

    if ("" === (await this.digital_object.label))
      validation_messages.push(["label is empty"]);

    let category = await this.digital_object.last_category;
    let category_class = await this.digital_object.category_class;
    if (category === null) validation_messages.push(["no category selected"]);
    else {
      if (category_class === null)
        validation_messages.push([
          "the selected category is not a class; select another one or use the ",
          "edit-button",
          "-button next to the category (",
          await category.label,
          ") to convert the category into a class; save the category after modification",
        ]);
      let i = await this.digital_object.first_category_step;
      let current_category = i === null ? null : await i.category;
      if (!category.saved)
        validation_messages.push(["the selected category is not saved"]);
      else
        while (current_category !== null) {
          if (!current_category.saved)
            validation_messages.push([
              'the category "',
              await current_category.label,
              '" is not saved',
            ]);
          i = await i.next;
          current_category = await i.category;
        }
    }

    for (let pv of this.property_values) {
      if (pv.validation_message !== null)
        validation_messages.push(pv.validation_message);
      else if (!pv.saved && !pv.is_suggestion)
        validation_messages.push([
          "a property/value is currently being changed; use the ",
          "save-button",
          "-button to finish changing or discard changes with the ",
          "delete-button",
          "-button",
        ]);
    }

    this.validation_messages = validation_messages;
    this.is_valid = validation_messages.length === 0;
    return this.is_valid;
  }

  on_add() {
    this.add_property_value();
    this.initialize_property_values().then();
  }

  async on_save() {
    if (!this.is_valid) {
      await this.validate();
      return;
    }
    if (!(await this.validate())) return;

    await this.selected_category.save();
    let data = await this.digital_object.save();
    if (
      "message" in data &&
      "success" in data &&
      this.message_callback !== null
    )
      this.message_callback(data.message, data.success);

    if (this.close_callback !== null) this.close_callback();
  }

  on_cancel() {
    this.digital_object.refresh();
    if (this.close_callback !== null) this.close_callback();
  }

  on_edit(digital_object) {
    this.disabled = true;
    this.sub_form = new ObjectForm(
      digital_object,
      this.message_callback,
      () => this.on_sub_close(),
      false,
    );
    this.sub_form.init_components(this.child_container).then(() => {
      if (this.sub_form !== null) this.sub_form.scroll_into_view();
    });
  }

  on_suggest(property, truth) {
    if (property !== null && this.selected_category !== null)
      this.selected_category.set_suggested_property(property, truth);
  }

  on_sub_close() {
    this.disabled = false;
    if (this.sub_form !== null) this.sub_form.destroy();
    this.sub_form = null;
  }

  async on_category_select(parent, category) {
    let i = await this.digital_object.first_category_step;
    let c;
    if (parent !== null)
      do {
        c = await i.category;
        i = await i.next;
      } while (c !== null && !c.eq(parent));

    this.digital_object.select_category(i, category);
  }

  async on_delete_property(property_value) {
    let property = property_value.property;
    let value = property_value.value;
    this.property_values = this.property_values.filter(
      (pv) => pv !== property_value,
    );
    property_value.destroy();
    if (property.saved && value.saved)
      this.digital_object.assert_not_relation(property, value);
    await this.validate();
  }

  async on_save_property(property_value) {
    let property = property_value.property;
    let value = property_value.value;
    let property_label = await property.label;
    let value_label = await value.label;
    let message = [];
    if (!property.saved) {
      message.push(`Property "${property_label}" `);
      if (value_label !== "") message.push(`(with value "${value_label}") `);
      message.push("is not a digital object! Use the ");
      message.push("goto-button");
      message.push(
        "-button to create one or use the autocomplete to select an existing digital object from the list. ",
      );
    }
    if (!value.saved) {
      if (!property.saved)
        message.push("The same is true for the corresponding value. ");
      else {
        message.push(`Value "${value_label}" `);
        if (property.label !== "")
          message.push(`for property "${property_label}" `);
        message.push("is not a digital object! Use the ");
        message.push("goto-button");
        message.push(
          "-button to create one or use the autocomplete to select an existing digital object from the list. ",
        );
      }
    }
    if (!property.saved || !value.saved) {
      message.push(
        "Note that property and value each must be a digital object. ",
      );
    }

    if (property.saved && value.saved) {
      this.digital_object.assert_relation(property, value);
      property_value.saved = true;
    }
    property_value.validation_message = message.length === 0 ? null : message;
    await this.update_property_values();
    await this.validate();
  }

  on_edit_property_value(property_value) {
    let property = property_value.property;
    let value = property_value.value;
    if (!property.saved || !value.saved)
      throw Error(
        "unexpected unsaved objects when trying to switch property value to edit mode",
      );
    property_value.saved = false;
    this.digital_object.assert_not_relation(property, value);
  }
}

class ValidationMessage {
  constructor(message_string_list) {
    this.message_string_list = message_string_list;

    this._disabled = false;
    this.reset_components();
  }

  reset_components() {
    this.ui_container = null;
  }

  init_components(parent_div) {
    const html = html_templates;
    this.ui_container = html.div(parent_div, ["alert", "alert-warning"]);

    for (let message of this.message_string_list) {
      if (message === "edit-button")
        html_templates.icon("pencil", this.ui_container);
      else if (message === "goto-button")
        html_templates.icon("arrow-right", this.ui_container);
      else if (message === "delete-button")
        html_templates.icon("trash", this.ui_container);
      else if (message === "save-button")
        html_templates.icon("check", this.ui_container);
      else html.small(message, this.ui_container);
    }

    this.ui_container.appendTo(parent_div);
  }

  get disabled() {
    return this._disabled;
  }

  set disabled(value) {
    this._disabled = value;
  }

  destroy() {
    this.ui_container.remove();
    this.reset_components();
  }

  eq(other) {
    if (!(other instanceof ValidationMessage)) return this === other;
    if (this.message_string_list.length !== other.message_string_list.length)
      return false;
    for (let i = 0; i < this.message_string_list.length; i++)
      if (this.message_string_list[i] !== other.message_string_list[i])
        return false;
    return true;
  }
}
