# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from heliport.core.models import (
    DigitalObject,
    DigitalObjectRelation,
    HeliportUser,
    Project,
)


class DigitalObjectTests(TestCase):
    """Test digital objects app."""

    def setUp(self):
        """Set up project, instances and logged-in heliport user."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.client.force_login(self.auth_user)
        self.project = Project.objects.create(owner=self.user)
        self.p1 = DigitalObject.objects.create(label="123yyy_predicate")

    def test_list(self):
        """Test listing digital objects."""
        obj_project = DigitalObject.objects.create(
            label="123xxx_project", description="a description"
        )
        obj_relation = DigitalObject.objects.create(label="123xxx_relation")
        obj_user = DigitalObject.objects.create(label="123xxx_user")
        obj_none = DigitalObject.objects.create(label="123xxx_none")
        obj_project.projects.add(self.project)
        r = DigitalObjectRelation(
            subject=self.project, predicate=self.p1, object=obj_relation
        )
        r.save()
        obj_user.owner = self.user
        obj_user.save()

        response = self.client.get(
            reverse("digital_objects:list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(200, response.status_code)
        self.assertContains(response, obj_project.label)
        self.assertContains(response, obj_relation.label)
        self.assertContains(response, self.p1.label)
        self.assertNotContains(response, obj_none.label)


class SearchAndAPITest(TestCase):
    """Test search including via API."""

    def setUp(self):
        """Set up project and logged-in heliport user."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.project, is_new = Project.objects.get_or_create(owner=self.user)
        self.client.force_login(self.auth_user)

    def test_access_search(self):
        """Test that search URL is accessible."""
        response = self.client.get(reverse("digital_objects:search"))
        self.assertEqual(200, response.status_code)

    def test_findable(self):
        """Test that digital object is findable."""
        d = DigitalObject.objects.create(label="DIGITAL_OBJECKKKT_123")
        d.projects.add(self.project)
        response = self.client.get(f"{reverse('digital_objects:search')}?q=OBJECKKKT")
        self.assertContains(response, "DIGITAL_OBJECKKKT_123")

    def test_api(self):
        """Test api."""
        d = DigitalObject.objects.create(description="OBJ654", label="a label")
        d.projects.add(self.project)
        response = self.client.get("/api/digital-objects/minimal/")
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "OBJ654")
