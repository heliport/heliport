# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path
from rest_framework import routers

from .views import (
    ArbitraryCWLListView,
    ArbitraryCWLUpdateView,
    ExecutableRawJsonView,
    ExecutableRawYamlView,
    ExecutableViewSet,
    ExecutionLogViewSet,
    JobCreateView,
    JobDebugView,
    JobDetailView,
    JobListView,
    JobOutputView,
    JobParameterValueView,
    JobProfilingJsonView,
    JobRawYamlView,
    JobStartView,
    JobStopView,
    JobViewSet,
    PublicationWorkflowCreateView,
    RunningJobsView,
    SearchView,
    ToolCreateView,
    ToolJsonDescriptionView,
    ToolListView,
    ToolUpdateView,
    UserJobConfigViewSet,
    VisualizationDataView,
    WorkflowCreateView,
    WorkflowListView,
    WorkflowUpdateView,
)

app_name = "cwl_execution"
urlpatterns = [
    path(
        "project/<int:project>/cwl-execution/arbitrary-cwl/list/",
        ArbitraryCWLListView.as_view(),
        name="arbitrary_cwl_list",
    ),
    path(
        "project/<int:project>/cwl-execution/arbitrary-cwl/<int:pk>/update/",
        ArbitraryCWLUpdateView.as_view(),
        name="arbitrary_cwl_update",
    ),
    path(
        "project/<int:project>/cwl-execution/job/create-from/<int:executable>/",
        JobCreateView.as_view(),
        name="job_create",
    ),
    path(
        "project/<int:project>/cwl-execution/job/publication-workflow/new/",
        PublicationWorkflowCreateView.as_view(),
        name="publication_workflow_create",
    ),
    path(
        "project/<int:project>/cwl-execution/job/parameter-value/new/",
        JobParameterValueView.as_view(),
        name="parameter_value",
    ),
    path(
        "project/<int:project>/cwl-execution/executable/<int:executable>/job/<int:pk>/update/",
        JobCreateView.as_view(),
        name="job_update",
    ),
    path(
        "project/<int:project>/cwl-execution/job/list/",
        JobListView.as_view(),
        name="job_list",
    ),
    path(
        "project/<int:project>/cwl-execution/job/running/",
        RunningJobsView.as_view(),
        name="job_running",
    ),
    path(
        "project/<int:project>/cwl-execution/job/<int:pk>/start/",
        JobStartView.as_view(),
        name="job_start",
    ),
    path(
        "project/<int:project>/cwl-execution/job/<int:pk>/CWL/YAML/",
        JobRawYamlView.as_view(),
        name="job_cwl_yaml",
    ),
    path(
        "project/<int:project>/cwl-execution/job/<int:pk>/detail/",
        JobDetailView.as_view(),
        name="job_detail",
    ),
    path(
        "project/<int:project>/cwl-execution/job/<int:pk>/output/",
        JobOutputView.as_view(),
        name="job_output",
    ),
    path(
        "project/<int:project>/cwl-execution/job/<int:pk>/debug/",
        JobDebugView.as_view(),
        name="job_debug",
    ),
    path(
        "project/<int:project>/cwl-execution/job/<int:pk>/stop/",
        JobStopView.as_view(),
        name="job_stop",
    ),
    path(
        "project/<int:project>/cwl-execution/job/<int:pk>/profiling/json/",
        JobProfilingJsonView.as_view(),
        name="job_profiling_json",
    ),
    path(
        "project/<int:project>/cwl-execution/tool/list/",
        ToolListView.as_view(),
        name="tool_list",
    ),
    path(
        "cwl-execution/tool/<int:pk>/CWL/YAML/",
        ExecutableRawYamlView.as_view(),
        name="exe_cwl_yaml",
    ),
    path(
        "cwl-execution/tool/<int:pk>/CWL/YAML/<str:part>/",
        ExecutableRawYamlView.as_view(),
        name="exe_cwl_yaml",
    ),
    path(
        "cwl-execution/tool/<int:pk>/CWL/JSON/",
        ExecutableRawJsonView.as_view(),
        name="exe_cwl_json",
    ),
    path(
        "cwl-execution/tool/<int:pk>/CWL/JSON/<str:part>/",
        ExecutableRawJsonView.as_view(),
        name="exe_cwl_json",
    ),
    path(
        "project/<int:project>/cwl-execution/tool/<int:pk>/description/",
        ToolJsonDescriptionView.as_view(),
        name="tool_description",
    ),
    path(
        "project/<int:project>/cwl-execution/tool/<int:pk>/update/",
        ToolUpdateView.as_view(),
        name="tool_update",
    ),
    path(
        "project/<int:project>/cwl-execution/tool/create/",
        ToolCreateView.as_view(),
        name="tool_create",
    ),
    path(
        "project/<int:project>/cwl-execution/workflow/list/",
        WorkflowListView.as_view(),
        name="workflow_list",
    ),
    path(
        "project/<int:project>/cwl-execution/workflow/<int:pk>/update/",
        WorkflowUpdateView.as_view(),
        name="workflow_update",
    ),
    path(
        "project/<int:project>/cwl-execution/workflow/create/",
        WorkflowCreateView.as_view(),
        name="workflow_create",
    ),
    path(
        "project/<int:project>/cwl-execution/executable/<int:pk>/visualization-data/",
        VisualizationDataView.as_view(),
        name="visualization_data",
    ),
    path("cwl-execution/search/", SearchView.as_view(), name="search"),
]

# REST API
router = routers.DefaultRouter()
router.register(r"cwl-execution/executables", ExecutableViewSet, basename="executables")
router.register(r"cwl-execution/jobs", JobViewSet, basename="job")
router.register(
    r"cwl-execution/user-job-configs", UserJobConfigViewSet, basename="user_job_configs"
)
router.register(
    r"cwl-execution/execution-logs", ExecutionLogViewSet, basename="execution_logs"
)
