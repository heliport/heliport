# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Additional settings for this app.

See also `django appconf <https://django-appconf.readthedocs.io>`_
"""

from appconf import AppConf


class CwlExecutionAppConf(AppConf):
    """Settings of the :mod:`heliport.cwl_execution` app.

    All settings in this class can be overwritten in settings.py
    """

    USE_SRUN = True
    SSH_DATA_TRANSFER_BATCH_SIZE = 10_000  # in bytes
    DEFAULT_JOB_SETUP_COMMAND = """ml python/3.9
@HELIPORT load venv  # or conda activate, ..."""
    DEFAULT_JOB_OPTIONS = """--time=10  # min or day-hour:min
#--partition=gpu  # part of cluster
#--nodes=8  # number compute nodes
# ... many more options possible"""
    DEFAULT_JOB_PATH = "~/heliport_jobs_3_9"

    class Meta:  # noqa: D106
        prefix = "HELIPORT_CWL"
