# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""This module is for serialization into Datacite, RDF and JSON.

The JSON format is used for API endpoints via
`Django rest framework <https://www.django-rest-framework.org/>`_. Using this, it is
typical to put the serializer classes into a "serializers.py" file.

For the serialization into RDF, attributes are described.
See :class:`heliport.core.attribute_description.BaseAttribute` for more detail.
"""

from django.conf import settings
from django.urls import reverse
from rdflib import FOAF, RDF, URIRef
from rest_framework import serializers

from heliport.core.models import LoginInfo
from heliport.core.permissions import has_read_permission, projects_of
from heliport.core.serializers import (
    DigitalObjectDATACITESerializer,
    DigitalObjectRDFSerializer,
    NamespaceField,
    datacite_serializers,
    rdf_serializers,
)
from heliport.core.vocabulary import FABIO
from heliport.core.vocabulary_descriptor import RelationTriple
from heliport.cwl_execution.cwl_helpers.cwl2rdf import cwl_to_rdf
from heliport.cwl_execution.models import Executable, ExecutionLog, Job, UserJobConfig


class ExecutableSerializer(serializers.ModelSerializer):  # noqa: D101
    namespace_str = NamespaceField(settings.EXECUTABLE_ARBITRARY_CWL_NAMESPACE)

    def __init__(self, *args, **kwargs):
        """Override querysets to only allow current user's objects."""
        super().__init__(*args, **kwargs)
        user = self.context["request"].user.heliportuser
        self.fields["projects"].child_relation.queryset = projects_of(user)

    class Meta:  # noqa: D106
        model = Executable
        fields = [
            "executable_id",
            "cwl_type",
            "projects",
            "type",
            "cwl",
            "cwl_json",
            "namespace_str",
        ]


class JobSerializer(serializers.ModelSerializer):  # noqa: D101
    namespace_str = NamespaceField(settings.JOB_NAMESPACE)

    class Meta:  # noqa: D106
        model = Job
        fields = [
            "job_id",
            "projects",
            "label",
            "status",
            "description",
            "output_text",
            "output_code",
            "cwl",
            "repeat",
            "interval",
            "base_executable",
            "setup_command",
            "namespace_str",
        ]


class UserJobConfigSerializer(serializers.ModelSerializer):  # noqa: D101#
    ssh_login = serializers.PrimaryKeyRelatedField(queryset=LoginInfo.objects.none())

    def __init__(self, *args, **kwargs):
        """Override LoginInfo queryset to only allow current user's objects."""
        super().__init__(*args, **kwargs)
        user = self.context["request"].user.heliportuser
        self.fields["ssh_login"].queryset = LoginInfo.objects.filter(user=user)

    class Meta:  # noqa: D106
        model = UserJobConfig
        fields = ["user_job_config_id", "job", "ssh_login", "directory"]


class ExecutionLogSerializer(serializers.ModelSerializer):  # noqa: D101
    class Meta:  # noqa: D106
        model = ExecutionLog
        fields = [
            "execution_log_id",
            "config",
            "start_time",
            "end_time",
            "memory_usage",
            "execution_id",
            "aborted",
            "step_times",
            "output_code",
            "error_message",
            "user",
        ]


@datacite_serializers.register(Executable)
class ExecutableDATACITESerializer(DigitalObjectDATACITESerializer):  # noqa: D101
    def resource_type(self, exe):  # noqa: D102
        return exe.get_type_display(), "Workflow"


@rdf_serializers.register(Executable)
class ExecutableRDFSerializer(DigitalObjectRDFSerializer):  # noqa: D101
    def static_triples(self, exe, user):  # noqa: D102
        yield from super().static_triples(exe, user)
        yield RelationTriple(exe, RDF.type, FABIO.Workflow)

        cwl_url = reverse("cwl_execution:exe_cwl_json", kwargs={"pk": exe.pk})
        main_cwl_url = f"{settings.HELIPORT_CORE_HOST}{cwl_url}{'' if cwl_url.endswith('/') else '/'}main"  # noqa: E501
        if exe.download_url_is_public or has_read_permission(user, exe):
            yield RelationTriple(exe, FOAF.primaryTopic, URIRef(main_cwl_url))
        if exe.cwl_is_public or has_read_permission(user, exe):
            for s, p, o in cwl_to_rdf(exe.cwl, main_cwl_url):
                yield RelationTriple(s, p, o)


@datacite_serializers.register(Job)
class JobDATACITESerializer(DigitalObjectDATACITESerializer):  # noqa: D101
    def resource_type(self, job):  # noqa: D102
        return "job", "Workflow"

    def related_identifiers(self, job, user):  # noqa: D102
        related = super().related_identifiers(job, user)
        exe = job.base_executable
        if exe.persistent_id:
            related.append(
                {
                    "relatedIdentifier": exe.persistent_id,
                    "relatedIdentifierType": exe.identifier_scheme.datacite_id,
                    "relationType": "Requires",
                }
            )
        return related
