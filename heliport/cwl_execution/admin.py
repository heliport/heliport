# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Registers Django admin pages.

See :class:`django.contrib.admin.ModelAdmin` from Django documentation.
"""

from django.contrib import admin

from .models import Executable, ExecutionLog, Job, UserJobConfig

admin.site.register(Job)
admin.site.register(Executable)
admin.site.register(UserJobConfig)
admin.site.register(ExecutionLog)
