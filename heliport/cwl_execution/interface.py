# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Module with special name "interface" hooks into HELIPORT.

Some functions and :class:`heliport.core.app_interaction.Module` subclasses are detected
by HELIPORT and control how HELIPORT uses this app.

Note that this module must be imported in __init__.py of the django app.
"""

from django.urls import reverse

from heliport.core.app_interaction import Module


class ArbitraryCWLModule(Module):  # noqa: D101
    name = "CWL File"
    module_id = "arbitrary_cwl"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse(
            "cwl_execution:arbitrary_cwl_list", kwargs={"project": project.pk}
        )

    def is_configured(self, project):
        """Return whether the module is configured, i.e. to show it in the graph."""
        from .models import Executable

        cwl_t = Executable.ExecutableTypes.ARBITRARY_CWL
        return Executable.objects.filter(
            projects=project, type=cwl_t, deleted__isnull=True
        ).exists()


class ToolModule(Module):  # noqa: D101
    name = "Tool"
    module_id = "tool"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("cwl_execution:tool_list", kwargs={"project": project.pk})

    def is_configured(self, project):
        """Return whether the module is configured, i.e. to show it in the graph."""
        from .models import Executable

        cwl_t = Executable.ExecutableTypes.TOOL
        return Executable.objects.filter(
            projects=project, type=cwl_t, deleted__isnull=True
        ).exists()


class WorkflowModule(Module):  # noqa: D101
    name = "Workflow"
    module_id = "workflow"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("cwl_execution:workflow_list", kwargs={"project": project.pk})

    def is_configured(self, project):
        """Return whether the module is configured, i.e. to show it in the graph."""
        from .models import Executable

        cwl_t = Executable.ExecutableTypes.WORKFLOW
        return Executable.objects.filter(
            projects=project, type=cwl_t, deleted__isnull=True
        ).exists()


class JobModule(Module):  # noqa: D101
    name = "CWL Jobs"
    module_id = "job"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("cwl_execution:job_list", kwargs={"project": project.pk})

    def is_configured(self, project):
        """Return whether the module is configured, i.e. to show it in the graph."""
        from .models import Job

        return Job.objects.filter(projects=project, deleted__isnull=True).exists()


def get_search_url():
    """Return the search URL for this app.

    This URL is used to implement the global HELIPORT string search.
    """
    return reverse("cwl_execution:search")
