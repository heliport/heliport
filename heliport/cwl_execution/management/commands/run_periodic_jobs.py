# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from django.core.management.base import BaseCommand

from heliport.cwl_execution.tasks import run_periodic_jobs


class Command(BaseCommand):  # noqa: D101
    help = "Run periodic CWL jobs"

    def handle(self, *args, **options):  # noqa: D102
        run_periodic_jobs.delay()
        self.stdout.write(
            self.style.SUCCESS(
                f"Task {run_periodic_jobs.__name__!r} added to the queue"
            )
        )
