# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

import json

import yaml
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from heliport.core.models import HeliportUser, Project

from .cwl_helpers import cwl_tool, cwl_workflow
from .models import Executable


class ArbitraryCWLTests(TestCase):  # noqa: D101
    def setUp(self):
        """Set up project and logged-in heliport users."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.client.force_login(self.auth_user)
        self.project = Project.objects.create(owner=self.user)

    def test_list(self):  # noqa: D102
        t = Executable(type=Executable.ExecutableTypes.TOOL)
        w = Executable(type=Executable.ExecutableTypes.WORKFLOW)
        t.save()
        w.save()
        t.projects.add(self.project)
        w.projects.add(self.project)
        response = self.client.get(
            reverse(
                "cwl_execution:arbitrary_cwl_list", kwargs={"project": self.project.pk}
            )
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(0, len(response.context_data["arbitrary_cwls"]))

        exe1 = Executable(type=Executable.ExecutableTypes.ARBITRARY_CWL)
        exe2 = Executable(
            type=Executable.ExecutableTypes.ARBITRARY_CWL, cwl_json='{"label":"abc"}'
        )
        exe1.save()
        exe1.projects.add(self.project)
        response = self.client.get(
            reverse(
                "cwl_execution:arbitrary_cwl_list", kwargs={"project": self.project.pk}
            )
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual({exe1}, set(response.context_data["arbitrary_cwls"]))

        exe2.save()
        exe2.projects.add(self.project)
        response = self.client.get(
            reverse(
                "cwl_execution:arbitrary_cwl_list", kwargs={"project": self.project.pk}
            )
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual({exe1, exe2}, set(response.context_data["arbitrary_cwls"]))

    def test_post_fail(self):  # noqa: D102
        exe = Executable(type=Executable.ExecutableTypes.ARBITRARY_CWL)
        exe.save()
        exe.projects.add(self.project)

        for cwl_type in Executable.CWLTypes.values:
            for url in [
                reverse(
                    "cwl_execution:arbitrary_cwl_list",
                    kwargs={"project": self.project.pk},
                ),
                reverse(
                    "cwl_execution:arbitrary_cwl_update",
                    kwargs={"project": self.project.pk, "pk": exe.pk},
                ),
            ]:
                response = self.client.post(
                    url, data={"cwl": "123", "cwl_type": cwl_type}
                )
                self.assertContains(response, "expected dict")

                response = self.client.post(
                    url, data={"cwl": "{", "cwl_type": cwl_type}
                )
                self.assertContains(response, "parse failed")
                self.assertEqual(cwl_type, response.context["cwl_type"])
                self.assertEqual("{", response.context["cwl"])

    def test_post_success(self):  # noqa: D102
        json_type = Executable.CWLTypes.JSON
        yaml_type = Executable.CWLTypes.YAML
        exe = Executable(type=Executable.ExecutableTypes.ARBITRARY_CWL)
        exe.save()
        exe.projects.add(self.project)

        for url, cwl_type, pk in [
            (
                reverse(
                    "cwl_execution:arbitrary_cwl_list",
                    kwargs={"project": self.project.pk},
                ),
                json_type,
                -1,
            ),
            (
                reverse(
                    "cwl_execution:arbitrary_cwl_list",
                    kwargs={"project": self.project.pk},
                ),
                yaml_type,
                -1,
            ),
            (
                reverse(
                    "cwl_execution:arbitrary_cwl_update",
                    kwargs={"project": self.project.pk, "pk": exe.pk},
                ),
                json_type,
                exe.pk,
            ),
            (
                reverse(
                    "cwl_execution:arbitrary_cwl_update",
                    kwargs={"project": self.project.pk, "pk": exe.pk},
                ),
                yaml_type,
                exe.pk,
            ),
        ]:
            cwl = {"label": str(json_type), "doc": str(pk)}
            cwl_json = json.dumps(cwl)
            response = self.client.post(
                url, data={"cwl": cwl_json, "cwl_type": cwl_type}, follow=True
            )
            new_exe = Executable.objects.filter(
                projects=self.project,
                type=Executable.ExecutableTypes.ARBITRARY_CWL,
                cwl_type=cwl_type,
                cwl_json__contains="{",
            ).first()

            self.assertEqual(200, response.status_code)
            self.assertIsNotNone(new_exe)
            self.assertEqual(cwl, new_exe.cwl)
            raw_response = self.client.get(
                reverse(
                    "cwl_execution:exe_cwl_yaml",
                    kwargs={"pk": new_exe.pk},
                )
            )
            self.assertEqual(200, raw_response.status_code)

            if pk >= 0:
                self.assertEqual(pk, new_exe.pk)
            else:
                new_exe.delete()

    def test_remove(self):  # noqa: D102
        exe = Executable(
            type=Executable.ExecutableTypes.ARBITRARY_CWL, cwl_json='{"label":"xxx"}'
        )
        exe.save()
        exe.projects.add(self.project)
        self.assertIsNotNone(
            Executable.objects.filter(
                projects=self.project, deleted__isnull=True
            ).first()
        )

        response = self.client.post(
            reverse(
                "cwl_execution:arbitrary_cwl_list", kwargs={"project": self.project.pk}
            ),
            data={"remove": exe.pk},
            follow=True,
        )
        self.assertEqual(200, response.status_code)

        self.assertIsNone(
            Executable.objects.filter(
                projects=self.project, deleted__isnull=True
            ).first()
        )

    def test_update_list(self):  # noqa: D102
        e = Executable(
            type=Executable.ExecutableTypes.ARBITRARY_CWL, cwl_json='{"label": "aaa"}'
        )
        e.save()
        e.projects.add(self.project)

        response = self.client.get(
            reverse(
                "cwl_execution:arbitrary_cwl_update",
                kwargs={"project": self.project.pk, "pk": e.pk},
            )
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual([e], list(response.context["arbitrary_cwls"]))
        self.assertContains(response, "aaa")

    def test_update_show(self):  # noqa: D102
        json_type = Executable.CWLTypes.JSON
        yaml_type = Executable.CWLTypes.YAML
        example_json = """{
   "label": "bbb",
   "inputs": [],
   "doc": "doc1"
}"""
        example_yaml = """doc: doc2
inputs: []
label: ccc
"""
        example_yaml_json = json.dumps(yaml.safe_load(example_yaml))

        for cwl_type, example_in, example_out in [
            (json_type, example_json, example_json),
            (yaml_type, example_yaml_json, example_yaml),
        ]:
            e = Executable(
                type=Executable.ExecutableTypes.ARBITRARY_CWL,
                cwl_type=cwl_type,
                cwl_json=example_in,
            )
            e.save()
            e.projects.add(self.project)
            response = self.client.get(
                reverse(
                    "cwl_execution:arbitrary_cwl_update",
                    kwargs={"project": self.project.pk, "pk": e.pk},
                )
            )
            self.assertEqual(200, response.status_code)
            self.assertEqual(example_out, response.context["cwl"])


class ToolTests(TestCase):  # noqa: D101
    def setUp(self):
        """Set up project and logged-in heliport users."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.client.force_login(self.auth_user)
        self.project = Project.objects.create(owner=self.user)

    def test_list_and_delete(self):  # noqa: D102
        a = Executable(type=Executable.ExecutableTypes.ARBITRARY_CWL)
        w = Executable(type=Executable.ExecutableTypes.WORKFLOW)
        a.save()
        w.save()
        a.projects.add(self.project)
        w.projects.add(self.project)
        response = self.client.get(
            reverse("cwl_execution:tool_list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(0, len(response.context_data["tools"]))

        exe1 = Executable(type=Executable.ExecutableTypes.TOOL)
        exe2 = Executable(
            type=Executable.ExecutableTypes.TOOL, cwl_json='{"label":"abc"}'
        )
        exe1.save()
        exe1.projects.add(self.project)
        response = self.client.get(
            reverse("cwl_execution:tool_list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual({exe1}, set(response.context_data["tools"]))

        exe2.save()
        exe2.projects.add(self.project)
        response = self.client.get(
            reverse("cwl_execution:tool_list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual({exe1, exe2}, set(response.context_data["tools"]))

        response = self.client.post(
            reverse("cwl_execution:tool_list", kwargs={"project": self.project.pk}),
            data={"delete": exe1.pk},
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual({exe2}, set(response.context_data["tools"]))

    def test_create_tool_and_update_tool(self):  # noqa: D102
        def build_url(action, pk=None, no_project=False):
            kwargs = {}
            if not no_project:
                kwargs["project"] = self.project.pk
            if pk is not None:
                kwargs["pk"] = pk
            return reverse(f"cwl_execution:{action}", kwargs=kwargs)

        # create view
        response = self.client.get(build_url("tool_create"))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "Create a Tool")

        # create tool
        response = self.client.post(build_url("tool_create"), follow=True)
        self.assertEqual(200, response.status_code)
        new_tool_list = [
            tool for tool in response.context["tools"] if "cwlVersion" in tool.cwl
        ]
        self.assertEqual(1, len(new_tool_list))
        (new_tool,) = new_tool_list

        # open raw
        response = self.client.get(
            build_url("exe_cwl_yaml", pk=new_tool.pk, no_project=True)
        )
        self.assertEqual(200, response.status_code)

        # update view
        self.assertTrue(new_tool.pk)
        response = self.client.get(build_url("tool_update", pk=new_tool.pk))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "Edit a Tool")

        # query json
        response = self.client.get(build_url("tool_description", pk=new_tool.pk))
        self.assertEqual(200, response.status_code)
        json_data = json.loads(response.content)
        json_data["main"]["baseCommand"] = "echo hi"

        # update tool
        response = self.client.post(
            build_url("tool_update", pk=new_tool.pk),
            data={"tool_json": json.dumps(json_data)},
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        new_tool = Executable.objects.get(pk=new_tool.pk)
        self.assertEqual(["echo", "hi"], new_tool.cwl["baseCommand"])

    def test_split_base_command(self):  # noqa: D102
        f = cwl_tool.base_command_list_encode

        self.assertEqual("abc", f("abc"))
        self.assertEqual("", f(""))
        self.assertEqual(["a", "b", ".", "xxx"], f("a b . xxx"))
        self.assertEqual(["a", "2", "n", "x"], f("a\t2\nn x"))
        self.assertEqual(["a", "abc def"], f('a "abc def"'))
        self.assertEqual(
            ["a", "abc def", "xyz", "o\tx", 'o" o"'],
            f("""a 'abc def' "xyz" "o\tx"\t'o" o"'"""),
        )
        self.assertEqual(
            ["git", "commit", "-m", "a new commit"], f('git commit -m "a new commit"')
        )

    def test_concat_base_command(self):  # noqa: D102
        f = cwl_tool.base_command_decode_to_str

        self.assertEqual("abc", f("abc"))
        self.assertEqual("", f(""))
        self.assertEqual("abc", f(["abc"]))
        self.assertEqual("abc def", f(["abc", "def"]))
        self.assertEqual("abc_def ghi_jkl", f(["abc_def", "ghi_jkl"]))
        self.assertEqual(
            "git commit -m 'a new commit'", f(["git", "commit", "-m", "a new commit"])
        )
        self.assertEqual(
            """abc 'def car' "hi's nice" 'it\\'s a nice "-symbol'""",
            f(["abc", "def car", "hi's nice", "it's a nice \"-symbol"]),
        )

    def test_json_description(self):  # noqa: D102
        to_cwl = cwl_tool.tool_description_to_cwl
        to_description = cwl_tool.cwl_to_tool_description

        cwl = yaml.safe_load(
            """
            baseCommand:
            - echo
            - hi
            class: CommandLineTool
            cwlVersion: v1.0
            doc: a description
            inputs:
              a:
                inputBinding:
                  position: 0
                  prefix: -xyz
                label: hi
                type: boolean
              b:
                inputBinding:
                  position: 1
                type: string
              c:
                inputBinding:
                  position: 2
                type: File
            label: echo_tool
            outputs:
              X:
                label: aaa
                outputBinding:
                  glob: xxx
                type: File
              Y:
                outputBinding:
                  glob: '*.txt'
                type:
                  items: File
                  type: array
              stderr:
                type: stderr
              stdout:
                type: stdout
            stderr: stderr.txt
            stdout: stdout.txt
            """
        )

        description = {
            "main": {
                "baseCommand": "echo hi",
                "label": "echo_tool",
                "doc": "a description",
            },
            "inputs": [
                {
                    "id": "a",
                    "label": "hi",
                    "type": "boolean",
                    "prefix": "-xyz",
                    "count": "",
                },
                {"id": "b", "label": "", "type": "string", "prefix": "", "count": ""},
                {"id": "c", "label": "", "type": "File", "prefix": "", "count": ""},
            ],
            "outputs": [
                {"id": "X", "label": "aaa", "type": "File", "glob": "xxx"},
                {"id": "Y", "label": "", "type": "File_array", "glob": "*.txt"},
            ],
            "capture": {
                "stdout": "stdout",
                "stdout_file": "stdout.txt",
                "stderr": "stderr",
                "stderr_file": "stderr.txt",
            },
        }

        self.assertEqual(description, to_description(cwl))
        self.assertEqual(cwl, to_cwl(description))
        self.assertTrue(isinstance(to_description({}), dict))
        self.assertTrue(isinstance(to_cwl({}), dict))
        self.assertNotIn("doc", to_cwl({"main": {"doc": ""}}))

        result_path = to_cwl({"inputs": [{"id": "path"}]})
        self.assertNotEqual("path", next(iter(result_path["inputs"])))

        # for backwards compatibility: parameters list not dict
        result_list = to_description(
            {"inputs": [{"id": "a"}], "outputs": [{"id": "b"}]}
        )
        self.assertEqual(
            [{"id": "a", "label": "", "type": "", "prefix": "", "count": ""}],
            result_list["inputs"],
        )
        self.assertEqual(
            [{"id": "b", "label": "", "type": "File_array", "glob": ""}],
            result_list["outputs"],
        )


class WorkflowTests(TestCase):  # noqa: D101
    def setUp(self):
        """Set up project and logged-in heliport users."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.client.force_login(self.auth_user)
        self.project = Project.objects.create(owner=self.user)

    def build_url(self, action, pk=None, no_project=False):  # noqa: D102
        kwargs = {}
        if not no_project:
            kwargs["project"] = self.project.pk
        if pk is not None:
            kwargs["pk"] = pk
        return reverse(f"cwl_execution:{action}", kwargs=kwargs)

    def test_list_and_delete(self):  # noqa: D102
        a = Executable(type=Executable.ExecutableTypes.ARBITRARY_CWL)
        t = Executable(type=Executable.ExecutableTypes.TOOL)
        a.save()
        t.save()
        a.projects.add(self.project)
        t.projects.add(self.project)
        response = self.client.get(self.build_url("workflow_list"))
        self.assertEqual(200, response.status_code)
        self.assertEqual(0, len(response.context_data["workflows"]))

        exe1 = Executable(type=Executable.ExecutableTypes.WORKFLOW)
        exe2 = Executable(
            type=Executable.ExecutableTypes.WORKFLOW, cwl_json='{"label":"abc"}'
        )
        exe1.save()
        exe1.projects.add(self.project)
        response = self.client.get(self.build_url("workflow_list"))
        self.assertEqual(200, response.status_code)
        self.assertEqual({exe1}, set(response.context_data["workflows"]))

        exe2.save()
        exe2.projects.add(self.project)
        response = self.client.get(self.build_url("workflow_list"))
        self.assertEqual(200, response.status_code)
        self.assertEqual({exe1, exe2}, set(response.context_data["workflows"]))

        response = self.client.post(
            self.build_url("workflow_list"), data={"delete": exe1.pk}, follow=True
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual({exe2}, set(response.context_data["workflows"]))

    def test_create_tool_and_update_tool(self):  # noqa: D102
        # create view
        exe1 = Executable(
            type=Executable.ExecutableTypes.ARBITRARY_CWL,
            cwl_json='{"label": "querystring_abc"}',
        )
        exe1.save()
        exe1.projects.add(self.project)
        exe2 = Executable(
            type=Executable.ExecutableTypes.TOOL, cwl_json='{"label": "not query"}'
        )
        exe2.save()
        exe2.projects.add(self.project)
        response = self.client.get(
            self.build_url("workflow_create"), data={"q": "querystring"}
        )
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "Create a Scientific Workflow")
        self.assertContains(response, "querystring")
        self.assertIn(exe1, response.context["executables"])
        self.assertNotIn(exe2, response.context["executables"])

        response = self.client.get(self.build_url("workflow_create"))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "Create a Scientific Workflow")
        self.assertIn(exe1, response.context["executables"])
        self.assertIn(exe2, response.context["executables"])

        # create workflow
        response = self.client.post(self.build_url("workflow_create"), follow=True)
        self.assertEqual(200, response.status_code)
        new_workflow_list = [
            tool for tool in response.context["workflows"] if "cwlVersion" in tool.cwl
        ]
        self.assertEqual(1, len(new_workflow_list))
        (new_exe,) = new_workflow_list

        # open raw
        response = self.client.get(
            self.build_url("exe_cwl_yaml", pk=new_exe.pk, no_project=True)
        )
        self.assertEqual(200, response.status_code)

        # update view
        response = self.client.get(
            self.build_url("workflow_update", pk=new_exe.pk), data={"q": "querystring"}
        )
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "Edit a Scientific Workflow")
        self.assertContains(response, "querystring")
        self.assertIn(exe1, response.context["executables"])
        self.assertNotIn(exe2, response.context["executables"])

        response = self.client.get(self.build_url("workflow_update", pk=new_exe.pk))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "Edit a Scientific Workflow")
        self.assertIn(exe1, response.context["executables"])
        self.assertIn(exe2, response.context["executables"])

        # query json
        response = self.client.get(
            self.build_url("visualization_data", pk=new_exe.pk),
            data={"full_workflow": True},
        )
        self.assertEqual(200, response.status_code)
        json_data = json.loads(response.content)
        self.assertEqual({"canvas", "edges", "nodes"}, set(json_data))
        json_data["nodes"] = [
            {
                "label": "abc",
                "user_data": {"run": "xxx"},
                "inputs": [],
                "outputs": [],
                "type": "yyy",
            }
        ]

        for exe, false_value in [
            (exe1, False),
            (exe2, "False"),
            (new_exe, "false"),
            (exe1, None),
        ]:
            res = self.client.get(
                self.build_url("visualization_data", pk=exe.pk),
                data={"full_workflow": false_value} if false_value is not None else {},
            )
            self.assertEqual(200, res.status_code)
            data = json.loads(res.content)
            self.assertEqual(
                {"inputs", "outputs", "label", "type", "run", "id"}, set(data)
            )

        # update tool
        response = self.client.post(
            self.build_url("workflow_update", pk=new_exe.pk),
            data={"graph_cwl": json.dumps(json_data), "name": "s", "description": "t"},
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        new_exe = Executable.objects.get(pk=new_exe.pk)
        self.assertEqual("abc", next(iter(new_exe.cwl["steps"]))["id"])
        self.assertEqual("s", new_exe.cwl["label"])
        self.assertEqual("t", new_exe.cwl["doc"])

    def test_id_generation(self):  # noqa: D102
        def f(i):
            return cwl_workflow.generate_cwl_nonempty_id({"label": i})

        self.assertEqual("abc", f("abc"))
        self.assertEqual("abc_def", f("abc def"))
        self.assertEqual("ab_c", f("ab+c"))
        for empty_val in [cwl_workflow.generate_cwl_nonempty_id({}), f("")]:
            self.assertIsNotNone(empty_val)
            self.assertIsInstance(empty_val, str)
            self.assertNotEqual(0, len(empty_val))

    def test_generate_port_data(self):  # noqa: D102
        f = cwl_workflow.generate_port_data

        self.assertEqual([{"label": "abc", "type": "xyz"}], f({"abc": {"type": "xyz"}}))
        self.assertEqual(
            [{"label": "abc_def", "type": "xyz"}], f({"abc%def": {"type": "xyz"}})
        )
        self.assertEqual(
            [{"label": "abc", "type": "xyz"}, {"label": "a", "type": "x"}],
            f({"abc": {"type": "xyz"}, "a": {"type": "x"}}),
        )
        self.assertEqual(
            [{"label": "a", "type": "xyz"}],
            f([{"id": "a", "type": "xyz"}]),
            "backwards compatibility",
        )

    def test_node_visualization_data(self):  # noqa: D102
        cwl = {
            "inputs": {"abc": {"type": "tx"}},
            "outputs": {"def": {"type": "ty"}},
            "class": "CommandLineTool",
        }
        exe = Executable(
            cwl_json=json.dumps(cwl), type=Executable.ExecutableTypes.WORKFLOW
        )
        exe.save()
        exe.projects.add(self.project)
        out = cwl_workflow.data_for_visualization_node(exe)

        self.assertEqual([{"label": "abc", "type": "tx"}], out["inputs"])
        self.assertEqual([{"label": "def", "type": "ty"}], out["outputs"])
        self.assertEqual("tool", out["type"])
        self.assertTrue(out["run"].endswith(".cwl"))
        self.assertEqual(exe.pk, out["id"])
        self.assertIsInstance(out["label"], str)
        self.assertNotEqual(0, len(out["label"]))

        cwl["class"] = "Workflow"
        exe.cwl_json = json.dumps(cwl)
        out = cwl_workflow.data_for_visualization_node(exe)
        self.assertEqual([{"label": "abc", "type": "tx"}], out["inputs"])
        self.assertEqual([{"label": "def", "type": "ty"}], out["outputs"])
        self.assertEqual("workflow", out["type"])
        self.assertTrue(out["run"].endswith(".cwl"))
        self.assertEqual(exe.pk, out["id"])
        self.assertIsInstance(out["label"], str)
        self.assertNotEqual(0, len(out["label"]))

    def test_unique_labels(self):  # noqa: D102
        def call(*args, name="unique_label"):
            nodes = [{} if label is None else {"label": label} for label in args]
            cwl_workflow.add_unique_labels(nodes)
            return [element.get(name) for element in nodes]

        self.assertEqual(["aba", "abb", "abc"], call("aba", "abb", "abc"))
        self.assertEqual(["a", "a_1", "b"], call("a", "a", "b"))
        self.assertEqual(
            ["x", "x_1", "x_2", "x_3", "x_4"], call("x", "x", "x", "x", "x")
        )
        self.assertEqual(["a", "b", "a_1"], call("a", "b", "a"))
        self.assertEqual(["a_b"], call("a_b"))
        self.assertEqual(["ab", "c_d", "_ef", "xy_"], call("ab", "c/d", "/ef", "xy/"))
        self.assertEqual(["a_b", "a_b_1"], call("a_b", "a/b"))
        self.assertEqual(["a_b", "a_b_1"], call("a/b", "a_b"))
        self.assertEqual(
            ["a_", "thing", "thing_1", "a__1", "a"], call("a_", None, None, "a/", "a")
        )
        self.assertEqual(
            ["a_", None, None, "a/", "a"],
            call("a_", None, None, "a/", "a", name="label"),
        )

    def test_node_categorization(self):  # noqa: D102
        def node(t):
            return {"type": t}

        nodes = [
            node("input"),
            node("file_input"),
            node("output"),
            node("file_output"),
            node("workflow"),
            node("tool"),
            node("blibla blub"),
            {},
            node("ERROR"),
            node("input"),
            node("output"),
            node("tool"),
        ]

        inputs = [node("input"), node("file_input"), node("input")]
        outputs = [node("output"), node("file_output"), node("output")]
        steps = [
            node("workflow"),
            node("tool"),
            node("blibla blub"),
            {},
            node("ERROR"),
            node("tool"),
        ]

        self.assertEqual(
            (inputs, outputs, steps), cwl_workflow.differentiate_nodes(nodes)
        )

    def test_label_index(self):  # noqa: D102
        inputs = [
            {"outputs": [{"id": 1}], "unique_label": "a"},
            {"outputs": [{"id": 2}], "unique_label": "b"},
        ]

        steps = [
            {
                "outputs": [{"id": 3, "label": "x"}, {"id": 4, "label": "y"}],
                "unique_label": "c",
            },
            {},
        ]

        label_index = {1: "a", 2: "b", 3: "c/x", 4: "c/y"}

        self.assertEqual(label_index, cwl_workflow.get_label_index(inputs, steps))

    @staticmethod
    def generate_cwl():  # noqa: D102
        return yaml.safe_load(
            """
                    $namespaces:
                      sbg: https://heliport/visualization
                    class: Workflow
                    cwlVersion: v1.0
                    doc: def
                    inputs:
                    - id: message1
                      sbg:x: -109.50838088989258
                      sbg:y: -8.116668701171875
                      type: string
                    - id: message2
                      sbg:x: -134.50838088989258
                      sbg:y: 141.88333129882812
                      type: string
                    label: abc
                    outputs:
                    - id: out
                      outputSource: echo_capture/out
                      sbg:x: 236.4666976928711
                      sbg:y: 255.88333129882812
                      type: File
                    - id: out2
                      outputSource: echo_capture/out2
                      sbg:x: 287.5000686645508
                      sbg:y: 123.88333129882812
                      type: File
                    - id: message1_1
                      outputSource: message1
                      sbg:x: 64.4999771118164
                      sbg:y: -88.11666870117188
                      type: string
                    requirements:
                    - class: SubworkflowFeatureRequirement
                    sbg:x: -519.8458404541016
                    sbg:y: -276.05833435058594
                    steps:
                    - id: echo_capture
                      in:
                      - id: message1
                        sbg:type: string
                        source: message1
                      - id: message2
                        sbg:type: string
                        source: message2
                      out:
                      - id: out2
                        sbg:type: File
                      - id: out
                        sbg:type: stdout
                      run: echo_capture_77.cwl
                      sbg:type: tool
                      sbg:x: 68.99268341064453
                      sbg:y: 71
                    - id: cat
                      in:
                      - id: file
                        sbg:type: File
                        source: echo_capture/out2
                      out: []
                      run: cat_42.cwl
                      sbg:type: tool
                      sbg:x: 255.99998474121094
                      sbg:y: -31
                    - id: echo_workflow
                      in:
                      - id: message
                        sbg:type: string
                        source: message2
                      out: []
                      run: echo_workflow_79.cwl
                      sbg:type: workflow
                      sbg:x: 92.9832763671875
                      sbg:y: 264"""
        )

    def test_workflow_to_cwl(self):  # noqa: D102
        f = cwl_workflow.visualization_data_to_cwl

        empty = f({}, "a", "b")
        self.assertIn("cwlVersion", empty)
        self.assertEqual("Workflow", empty["class"])
        self.assertEqual("a", empty["label"])
        self.assertEqual("b", empty["doc"])
        empty = f({}, "a", None)
        self.assertNotIn("doc", empty)
        self.assertNotIn("sbg:scale", empty)
        self.assertNotIn("sbg:x", empty)
        self.assertNotIn("sbg:y", empty)
        view = f({"canvas": {"x": 1, "y": 3, "scale": 2}}, None, None)
        self.assertEqual(2, view["sbg:scale"])
        self.assertEqual(1, view["sbg:x"])
        self.assertEqual(3, view["sbg:y"])
        self.assertIn("sbg", view["$namespaces"])

        step = f(
            {"nodes": [{"user_data": {"run": "x.cwl"}, "type": "workflow"}]}, None, None
        )
        self.assertIn({"class": "SubworkflowFeatureRequirement"}, step["requirements"])
        self.assertIn("id", step["steps"][0])
        self.assertIn("run", step["steps"][0])
        self.assertIn("sbg:type", step["steps"][0])
        self.assertIn("sbg", step["$namespaces"])

        data = {
            "canvas": {"x": -519.8458404541016, "y": -276.05833435058594, "scale": 1},
            "nodes": [
                {
                    "label": "message1",
                    "type": "input",
                    "inputs": [],
                    "outputs": [{"label": "message1", "type": "string", "id": 0}],
                    "x": -109.50838088989258,
                    "y": -8.116668701171875,
                    "user_data": {
                        "label": "message1",
                        "type": "input",
                        "x": -109.50838088989258,
                        "y": -8.116668701171875,
                        "outputs": [
                            {"id": "message1", "label": "message1", "type": "string"}
                        ],
                    },
                },
                {
                    "label": "message2",
                    "type": "input",
                    "inputs": [],
                    "outputs": [{"label": "message2", "type": "string", "id": 1}],
                    "x": -134.50838088989258,
                    "y": 141.88333129882812,
                    "user_data": {
                        "label": "message2",
                        "type": "input",
                        "x": -134.50838088989258,
                        "y": 141.88333129882812,
                        "outputs": [
                            {"id": "message2", "label": "message2", "type": "string"}
                        ],
                    },
                },
                {
                    "label": "out",
                    "type": "file_output",
                    "inputs": [{"label": "out", "type": "File", "id": 2}],
                    "outputs": [],
                    "x": 236.4666976928711,
                    "y": 255.88333129882812,
                    "user_data": {
                        "label": "out",
                        "type": "file_output",
                        "x": 236.4666976928711,
                        "y": 255.88333129882812,
                        "inputs": [
                            {
                                "id": "a1ec1186-41e2-4021-a46d-90a9a432aa4b",
                                "label": "out",
                                "type": "File",
                            }
                        ],
                    },
                },
                {
                    "label": "out2",
                    "type": "file_output",
                    "inputs": [{"label": "out2", "type": "File", "id": 3}],
                    "outputs": [],
                    "x": 287.5000686645508,
                    "y": 123.88333129882812,
                    "user_data": {
                        "label": "out2",
                        "type": "file_output",
                        "x": 287.5000686645508,
                        "y": 123.88333129882812,
                        "inputs": [
                            {
                                "id": "e07c1bff-6f6f-45ec-95cf-9f7abc203712",
                                "label": "out2",
                                "type": "File",
                            }
                        ],
                    },
                },
                {
                    "label": "message1_1",
                    "type": "output",
                    "inputs": [{"label": "message1_1", "type": "string", "id": 4}],
                    "outputs": [],
                    "x": 64.4999771118164,
                    "y": -88.11666870117188,
                    "user_data": {
                        "label": "message1_1",
                        "type": "output",
                        "x": 64.4999771118164,
                        "y": -88.11666870117188,
                        "inputs": [
                            {
                                "id": "1765d979-f971-43a4-8417-e2d2436cf397",
                                "label": "message1_1",
                                "type": "string",
                            }
                        ],
                    },
                },
                {
                    "label": "echo_capture",
                    "type": "tool",
                    "inputs": [
                        {"label": "message1", "type": "string", "id": 5},
                        {"label": "message2", "type": "string", "id": 6},
                    ],
                    "outputs": [
                        {"label": "out2", "type": "File", "id": 7},
                        {"label": "out", "type": "stdout", "id": 8},
                    ],
                    "x": 68.99268341064453,
                    "y": 71,
                    "user_data": {
                        "label": "echo_capture",
                        "type": "tool",
                        "x": 68.99268341064453,
                        "y": 71,
                        "run": "echo_capture_77.cwl",
                        "inputs": [
                            {
                                "id": "2bdc888d-09c8-46f6-8c15-7947b4d215ca",
                                "label": "message1",
                                "type": "string",
                            },
                            {
                                "id": "b6ad89eb-9f09-4a4e-bf1c-b96ec8de2055",
                                "label": "message2",
                                "type": "string",
                            },
                        ],
                        "outputs": [
                            {
                                "id": "echo_capture/out2",
                                "label": "out2",
                                "type": "File",
                            },
                            {
                                "id": "echo_capture/out",
                                "label": "out",
                                "type": "stdout",
                            },
                        ],
                    },
                },
                {
                    "label": "cat",
                    "type": "tool",
                    "inputs": [{"label": "file", "type": "File", "id": 9}],
                    "outputs": [],
                    "x": 255.99998474121094,
                    "y": -31,
                    "user_data": {
                        "label": "cat",
                        "type": "tool",
                        "x": 255.99998474121094,
                        "y": -31,
                        "run": "cat_42.cwl",
                        "inputs": [
                            {
                                "id": "3125fdb0-dc88-4d67-9ed1-1cba7a157044",
                                "label": "file",
                                "type": "File",
                            }
                        ],
                        "outputs": [],
                    },
                },
                {
                    "label": "echo_workflow",
                    "type": "workflow",
                    "inputs": [{"label": "message", "type": "string", "id": 10}],
                    "outputs": [],
                    "x": 92.9832763671875,
                    "y": 264,
                    "user_data": {
                        "label": "echo_workflow",
                        "type": "workflow",
                        "x": 92.9832763671875,
                        "y": 264,
                        "run": "echo_workflow_79.cwl",
                        "inputs": [
                            {
                                "id": "200a8c1a-3d97-4499-9baf-893d6f097fba",
                                "label": "message",
                                "type": "string",
                            }
                        ],
                        "outputs": [],
                    },
                },
            ],
            "edges": [
                {"start": 8, "end": 2},
                {"start": 7, "end": 3},
                {"start": 0, "end": 4},
                {"start": 0, "end": 5},
                {"start": 1, "end": 6},
                {"start": 7, "end": 9},
                {"start": 1, "end": 10},
            ],
        }

        self.assertEqual(self.generate_cwl(), f(data, "abc", "def"))

    def test_cwl_to_workflow(self):  # noqa: D102
        data = {
            "canvas": {"scale": 1, "x": -519.8458404541016, "y": -276.05833435058594},
            "edges": [
                {
                    "start": "echo_capture/out",
                    "end": "482d7ab3-63a0-49fc-be1d-7cd6008e0af2",
                },
                {
                    "start": "echo_capture/out2",
                    "end": "93212f57-eb7a-4d04-a603-072afcbf826c",
                },
                {"start": "message1", "end": "be4d8368-ccd0-43eb-9fcf-09edd33182be"},
                {"start": "message1", "end": "a33827ba-a24b-4d45-8f6c-edafc43d9585"},
                {"start": "message2", "end": "6ee78895-09fd-4123-98b4-42d2837e301a"},
                {
                    "start": "echo_capture/out2",
                    "end": "3535d36e-e5d6-4c0f-be05-34cfd4ac4f4f",
                },
                {"start": "message2", "end": "4cd8d5ad-7cc9-41dd-9285-22e736586b1d"},
            ],
            "nodes": [
                {
                    "label": "message1",
                    "type": "input",
                    "x": -109.50838088989258,
                    "y": -8.116668701171875,
                    "outputs": [
                        {"id": "message1", "label": "message1", "type": "string"}
                    ],
                },
                {
                    "label": "message2",
                    "type": "input",
                    "x": -134.50838088989258,
                    "y": 141.88333129882812,
                    "outputs": [
                        {"id": "message2", "label": "message2", "type": "string"}
                    ],
                },
                {
                    "label": "out",
                    "type": "file_output",
                    "x": 236.4666976928711,
                    "y": 255.88333129882812,
                    "inputs": [
                        {
                            "id": "482d7ab3-63a0-49fc-be1d-7cd6008e0af2",
                            "label": "out",
                            "type": "File",
                        }
                    ],
                },
                {
                    "label": "out2",
                    "type": "file_output",
                    "x": 287.5000686645508,
                    "y": 123.88333129882812,
                    "inputs": [
                        {
                            "id": "93212f57-eb7a-4d04-a603-072afcbf826c",
                            "label": "out2",
                            "type": "File",
                        }
                    ],
                },
                {
                    "label": "message1_1",
                    "type": "output",
                    "x": 64.4999771118164,
                    "y": -88.11666870117188,
                    "inputs": [
                        {
                            "id": "be4d8368-ccd0-43eb-9fcf-09edd33182be",
                            "label": "message1_1",
                            "type": "string",
                        }
                    ],
                },
                {
                    "label": "echo_capture",
                    "type": "tool",
                    "x": 68.99268341064453,
                    "y": 71,
                    "run": "echo_capture_77.cwl",
                    "inputs": [
                        {
                            "id": "a33827ba-a24b-4d45-8f6c-edafc43d9585",
                            "label": "message1",
                            "type": "string",
                        },
                        {
                            "id": "6ee78895-09fd-4123-98b4-42d2837e301a",
                            "label": "message2",
                            "type": "string",
                        },
                    ],
                    "outputs": [
                        {"id": "echo_capture/out2", "label": "out2", "type": "File"},
                        {"id": "echo_capture/out", "label": "out", "type": "stdout"},
                    ],
                },
                {
                    "label": "cat",
                    "type": "tool",
                    "x": 255.99998474121094,
                    "y": -31,
                    "run": "cat_42.cwl",
                    "inputs": [
                        {
                            "id": "3535d36e-e5d6-4c0f-be05-34cfd4ac4f4f",
                            "label": "file",
                            "type": "File",
                        }
                    ],
                    "outputs": [],
                },
                {
                    "label": "echo_workflow",
                    "type": "workflow",
                    "x": 92.9832763671875,
                    "y": 264,
                    "run": "echo_workflow_79.cwl",
                    "inputs": [
                        {
                            "id": "4cd8d5ad-7cc9-41dd-9285-22e736586b1d",
                            "label": "message",
                            "type": "string",
                        }
                    ],
                    "outputs": [],
                },
            ],
        }

        cwl = self.generate_cwl()
        first_output = cwl["outputs"][0]
        first_output["outputSource"] = [first_output["outputSource"]]
        generated_data = cwl_workflow.cwl_to_visualization_data(cwl)
        self.assertEqual(data["canvas"], generated_data["canvas"])
        self.assertEqual(len(data["edges"]), len(generated_data["edges"]))

        id_translator = {}
        for generated_edge, edge in zip(generated_data["edges"], data["edges"]):
            self.assertIn("start", generated_edge)
            self.assertIn("end", generated_edge)
            id_translator[edge["start"]] = generated_edge["start"]
            id_translator[edge["end"]] = generated_edge["end"]
        data["edges"] = generated_data["edges"]

        for node in data["nodes"]:
            for i in node.get("inputs", []):
                i["id"] = id_translator[i["id"]]
            for o in node.get("outputs", []):
                o["id"] = id_translator[o["id"]]

        self.assertEqual(data, generated_data)
