# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import json
import logging
import os
import re
import string
from stat import S_ISDIR
from time import time

from django.conf import settings
from django.db.models import Q
from knox.models import AuthToken

from heliport.core.models import DigitalObjectRelation, NamedToken, Tag, Vocabulary
from heliport.core.utils.command_line_execution import (
    CommandExecutor,
    create_command_executor,
)
from heliport.core.utils.string_tools import remove_prefix
from heliport.cwl_execution.models import Executable, ExecutionLog, UserJobConfig
from heliport.data_source.models import DataSource

from .cwl_generation import generate_cwl_filename

logger = logging.getLogger(__name__)


def save(commandline_argument: str) -> str:  # noqa: D103
    allowed_symbols = string.digits + string.ascii_letters
    return re.sub(rf"[^{allowed_symbols}]", "_", commandline_argument)


class CWLExecutorManager:  # noqa: D101
    executor: CommandExecutor
    job_config: UserJobConfig

    def __init__(  # noqa: D107
        self, job_config, debug=False, auto_run=True, execution_id=None
    ):
        self.job_config = job_config
        self.execution_id = execution_id
        self.debug = debug
        self.tool_filename = "tool.cwl"
        self.job_filename = "job.json"

        self.executor = self.build_ssh_shell()
        self.execution_log = ExecutionLog(config=self.job_config)
        self.return_message = None
        self.pwd = None
        self.start_time = time()
        self.profiling = {}
        if auto_run:
            self.run()

    def config_invalid(self):  # noqa: D102
        if self.login is None:
            self.configure_job_error("no ssh connection specified")
            return True

        directory = self.execution_directory
        if directory.startswith("~/"):
            directory = directory[2:]
        invalid_letters = set(directory) - set(
            string.ascii_letters + string.digits + "_/"
        )
        if invalid_letters:
            error_message = f"invalid letters in directory {repr(self.execution_directory)}: {invalid_letters}"  # noqa: E501
            self.configure_job_error(error_message)
            return True
        return False

    @staticmethod
    def datasource_has_native_cwl_support(source: DataSource):  # noqa: D102
        return source.protocol in {"http", "https"} and source.login is None

    def save_connect(self, executor: CommandExecutor):  # noqa: D102
        error_message = executor.connect_error_message(
            f' - try reconnecting the "{self.login.name}" ssh connection'
        )
        if error_message is not None:
            self.configure_job_error(error_message)
            return False
        return True

    @property
    def job(self):  # noqa: D102
        return self.job_config.job

    @property
    def user(self):  # noqa: D102
        return self.job_config.ssh_login.user

    @property
    def login(self):  # noqa: D102
        return self.job_config.ssh_login

    @property
    def execution_directory(self):  # noqa: D102
        return self.job_config.directory

    @property
    def job_folder(self):  # noqa: D102
        return os.path.join(
            self.execution_directory, f"job_{save(self.job.label)}_{self.job.pk}"
        )

    @property
    def absolute_job_folder(self):  # noqa: D102
        if self.pwd is None:
            raise ValueError("call prepare_job_folder() first")

        location = self.job_folder
        if location.startswith("~/"):
            location = os.path.join(self.pwd, location[2:])

        return location

    @property
    def srun(self):  # noqa: D102
        options = self.strip_comments(self.job.cmd_options)
        return (
            f'echo "" | srun {" ".join(options.split())} '
            if settings.HELIPORT_CWL_USE_SRUN and self.login.slurm
            else ""
        )

    @staticmethod
    def strip_comments(option_string):  # noqa: D102
        striped_lines = []
        for line in option_string.splitlines():
            single_comment = False
            double_comment = False
            for i, c in enumerate(line):
                if single_comment and c != "'":
                    continue
                if double_comment and c != '"':
                    continue
                if c == "'":
                    single_comment = not single_comment
                if c == '"':
                    double_comment = not double_comment
                if c == "#":
                    striped_lines.append(line[:i])
                    break
            else:
                striped_lines.append(line)

        return "\n".join(striped_lines)

    def configure_job_running(self):  # noqa: D102
        logger.debug("set job status to running")
        self.job.output_text = f"execution by {self.user.display_name}\n...\n"
        self.job.output_code = self.job.OutputCodes.WARNING
        self.job.status = self.job.StatusCodes.RUNNING
        self.job.save()
        self.execution_log.user = self.user
        self.execution_log.output_code = self.job.output_code
        self.execution_log.save()

    def configure_job_error(self, message="an undefined error occurred"):  # noqa: D102
        self.job.output_text = message
        self.job.output_code = self.job.OutputCodes.ERROR
        self.job.status = self.job.StatusCodes.STOPPED
        self.execution_log.output_code = self.job.output_code
        self.execution_log.error_message = message
        self.execution_log.stop()
        self.execution_log.save()
        self.job.save()

    def configure_job_stopped(self, status_code=0):  # noqa: D102
        self.job.output_code = self.job.OutputCodes.ERROR
        self.job.status = self.job.StatusCodes.STOPPED

        if status_code == 0:
            self.job.output_code = self.job.OutputCodes.MESSAGE
        self.execution_log.output_code = self.job.output_code
        self.execution_log.save()
        self.job.save()

    def append_job_output_text(self, message):  # noqa: D102
        self.job.output_text += message
        self.job.save()

    def execute(self, command: str):  # noqa: D102
        return self.executor.execute(command)

    def transfer_cwl(self, cwl, relative_location):  # noqa: D102
        file_path = os.path.join(self.absolute_job_folder, relative_location)
        logger.debug(file_path)
        with self.executor.file(file_path, "w") as f:
            print(json.dumps(cwl), file=f)

    def run(self):  # noqa: D102
        if self.config_invalid():
            self.return_message = "config invalid"
            return self.return_message

        may_require_node_js = self.test_node_requirement()
        self.profiling["validate config"] = time()

        logger.debug("STARTING JOB EXECUTION")
        self.execution_log.start(self.execution_id)
        self.configure_job_running()
        if not self.save_connect(self.executor):
            self.return_message = "ssh connection failed"
            return self.return_message
        self.profiling["connect to cluster"] = time()

        with self.executor:
            logger.debug("calling setup command")
            for cmd in self.job.setup_command.splitlines():
                clean_cmd = cmd.split("#", maxsplit=1)[0].strip()
                if clean_cmd == "@HELIPORT load venv":
                    self.load_venv()
                else:
                    self.execute(clean_cmd)

            logger.debug("preparing environment on cluster")
            self.load_cwltool()
            self.prepare_job_folder()
            if may_require_node_js:
                self.load_node_js()
            self.profiling["setup environment"] = time()

            logger.debug("transferring files")
            tool_cwl, token_inputs = self.extract_pure_executable_cwl_and_token_inputs(
                self.job.base_executable
            )
            self.transfer_cwl(tool_cwl, self.tool_filename)
            for exe in Executable.objects.filter(projects__in=self.job.projects.all()):
                name = generate_cwl_filename(exe.cwl_json, exe)
                (
                    cwl,
                    other_token_inputs,
                ) = self.extract_pure_executable_cwl_and_token_inputs(exe)
                self.transfer_cwl(cwl, name)

            job_cwl, data_sources = self.extract_pure_job_cwl_and_data_sources()
            job_cwl = self.set_tokens(job_cwl, token_inputs)
            self.transfer_cwl(job_cwl, self.job_filename)
            for data_source in data_sources:
                self.transfer_data_source(data_source)
            self.profiling["transfer files"] = time()

            logger.debug("run cwltool")
            status = self.run_cwl_tool()
            self.profiling["execute workflow"] = time()

        self.configure_job_stopped(status)
        self.save_metadata()
        self.return_message = "success" if status == 0 else "cwl error"
        self.profiling["collect and store metadata"] = time()
        self.save_profiling()
        return self.return_message

    def save_profiling(self):  # noqa: D102
        last_time = self.start_time
        deltas = {}
        for step, seconds in self.profiling.items():
            deltas[step] = seconds - last_time
            last_time = seconds

        self.execution_log.step_times = json.dumps(deltas)
        self.execution_log.save()

    def save_metadata(self):  # noqa: D102
        find_memory_usage = re.compile(r"\[([^]\[]+)] Max memory used: ([^ ]+)")
        memory_usage = {}
        for line in self.job.output_text.splitlines():
            match = find_memory_usage.search(line)
            if match:
                memory_usage[match[1]] = match[2]

        self.execution_log.memory_usage = json.dumps(memory_usage)
        self.execution_log.stop()

    def run_cwl_tool(self):  # noqa: D102
        debug_cmd = "--debug " if self.debug else ""
        env_cmd = "--preserve-entire-environment "
        cmd = f"{self.srun}cwltool {env_cmd}{debug_cmd}{self.tool_filename} {self.job_filename}"  # noqa: E501
        return self.execute(cmd).status

    def transfer_data_source(self, data_source):  # noqa: D102
        if data_source.uri.startswith("ssh://"):
            self.transfer_ssh_data_source(data_source)
        elif not self.datasource_has_native_cwl_support(data_source):
            self.download_data_with_curl(data_source)

    def download_data_with_curl(self, data_source: DataSource):  # noqa: D102
        unset = []

        if data_source.login is None:
            login_info_command = ""

        elif data_source.login.type == data_source.login.LoginTypes.USER_PASSWORD:
            login_info_command = '--user "$USERNAME:$PASSWORD" '
            uname = data_source.login.username
            password = data_source.login.password
            self.execute(f"read -N {len(uname)} -r USERNAME\n{uname}")
            self.execute(f"read -N {len(password)} -r -s PASSWORD\n{password}")
            unset.append("USERNAME")
            unset.append("PASSWORD")

        elif data_source.login.type == data_source.login.LoginTypes.TOKEN:
            login_info_command = '-H "$TOKEN_HEADER" '
            token = data_source.login.key
            self.execute(f"read -N {len(token)} -r -s TOKEN_HEADER\n{token}")
            unset.append("TOKEN_HEADER")

        else:
            login_info_command = f'--unsupported_login_type "{data_source.login.type}"'

        data_path = self.data_source_filename(data_source)
        logger.debug(data_path)
        self.execute(f'curl {login_info_command}"{data_source.uri}" > "{data_path}"')
        for u in unset:
            self.execute(f"unset {u}")

    def transfer_ssh_data_source(self, data_source: DataSource):  # noqa: D102
        if data_source.login is None:
            self.execute(f'echo "ERROR: no ssh login for {save(data_source.label)}"')
            return

        self.execute(f'echo "Transferring ssh data source: {save(data_source.label)}"')
        remote_login = data_source.login.build_command_executor()
        if not self.save_connect(remote_login):
            self.execute(
                f'echo "ERROR: failed to connect to {save(data_source.label)}"'
            )

        file_name = os.path.join(
            self.absolute_job_folder, self.data_source_filename(data_source)
        )
        logger.debug(file_name)
        with remote_login, self.executor.file(file_name, "w") as destination:
            try:
                source = remote_login.file(data_source.path)
                data = source.read(settings.HELIPORT_CWL_SSH_DATA_TRANSFER_BATCH_SIZE)
            except IOError as e:
                self.execute(f'echo "file not found: {save(str(e))}"')
                data = None
            while data:
                destination.write(data)
                data = source.read(settings.HELIPORT_CWL_SSH_DATA_TRANSFER_BATCH_SIZE)

    def extract_pure_job_cwl_and_data_sources(self):  # noqa: D102
        job_cwl = self.job.cwl
        data_sources_to_copy = []
        result_cwl = {}
        for param_id, param in job_cwl.items():
            tag_resolved_params = []

            if not isinstance(param, list):
                # only lists support getting values from tags
                tag_resolved_params.append(param)
            else:
                for sub_param in param:
                    from_tags = self.extract_inputs_from_tags(sub_param, param_id)
                    if from_tags is not None:
                        tag_resolved_params.extend(from_tags)
                    else:
                        tag_resolved_params.append(sub_param)

            pure_params = []
            for sub_param in tag_resolved_params:
                (
                    pure_param,
                    to_copy,
                ) = self.make_parameter_pure_and_extract_data_source_to_copy(sub_param)
                pure_params.append(pure_param)
                if to_copy is not None:
                    data_sources_to_copy.append(to_copy)

            if isinstance(param, list):
                result_cwl[param_id] = pure_params
            else:
                result_cwl[param_id] = pure_params[0]

        return result_cwl, data_sources_to_copy

    @staticmethod
    def get_inputs_dict(exe):  # noqa: D102
        exec_cwl = exe.cwl
        inputs = exec_cwl.get("inputs", {})
        if not isinstance(inputs, dict):
            inputs = {i.get("id"): i for i in inputs}
        return inputs

    def extract_inputs_from_tags(self, param, param_id):  # noqa: D102
        if isinstance(param, dict) and param.get("class") == "FromTags":
            tags = []
            for tag_id in str(param.get("tags")).split(","):
                tag = Tag.objects.filter(pk=tag_id).first()
                if tag is not None:
                    tags.append(tag)

            cwl_class = "File"
            exe_param = self.get_inputs_dict(self.job.base_executable).get(param_id)
            if isinstance(exe_param, dict) and str(exe_param.get("type")).startswith(
                "Directory"
            ):
                cwl_class = "Directory"

            result = []

            tagged_objects = set()
            for tag in tags:
                tagged_objects.update(
                    DigitalObjectRelation.objects.filter(
                        Q(predicate=tag.attribute, object=tag.value)
                        | Q(predicate=Vocabulary().has_tag, object=tag)
                    ).values_list("subject_id", flat=True)
                )

            for data_source in DataSource.objects.filter(
                deleted__isnull=True,
                projects__in=self.job.projects.all(),
                digital_object_id__in=tagged_objects,
            ):
                tested_cwl_class = self.test_directory_cwl_class(data_source)
                if tested_cwl_class is None or tested_cwl_class == cwl_class:
                    result.append(
                        {
                            "class": cwl_class,
                            "location": f"{data_source.pk}_{data_source.uri}",
                        }
                    )
            return result
        return None

    def test_directory_cwl_class(self, data_source):  # noqa: D102
        try:
            file = self.executor.file(data_source.path)
            file_stat = file.stat()
        except IOError:
            return None
        if not hasattr(file_stat, "st_mode"):
            return None
        return "Directory" if S_ISDIR(file_stat.st_mode) else "File"

    def make_parameter_pure_and_extract_data_source_to_copy(self, param):  # noqa: D102
        data_source = self.extract_data_source_from_cwl_parameter(param)
        data_source_to_copy = None
        if data_source is None:
            return param, data_source_to_copy

        if isinstance(param, dict) and param.get("class") == "Directory":
            uri = self.data_source_path(data_source)
            try:
                self.executor.file(uri).close()
            except IOError:
                self.execute(
                    f'echo "{save(uri)} not found on '
                    f"{save(self.login.machine)} as {save(str(self.login.user))} "
                    '(automatic copying not supported for directories)"'
                )
        else:
            uri = self.data_source_path(data_source)
            try:
                self.executor.file(uri).close()
            except IOError:
                data_source_to_copy = data_source
                uri = self.data_source_filename(data_source)

        return {**param, "location": uri}, data_source_to_copy

    @classmethod
    def data_source_filename(cls, data_source: DataSource):  # noqa: D102
        uri = data_source.uri
        if not cls.datasource_has_native_cwl_support(data_source):
            uri = save(uri)
        return uri

    @staticmethod
    def data_source_path(data_source: DataSource):  # noqa: D102
        uri = data_source.uri
        return remove_prefix(uri, "ssh://")

    @staticmethod
    def extract_data_source_from_cwl_parameter(param):  # noqa: D102
        if not isinstance(param, dict):
            return None
        if param.get("class") not in ["File", "Directory"]:
            return None
        data_source_id = param.get("location", "").split("_")[0]
        try:
            data_source_id_int = int(data_source_id)
        except ValueError:
            return None
        try:
            data_source = DataSource.objects.get(pk=data_source_id_int)
        except DataSource.DoesNotExist:
            return None

        return data_source

    def extract_pure_executable_cwl_and_token_inputs(self, exe):  # noqa: D102
        tool_cwl = exe.cwl
        if self.job.base_executable.type == Executable.ExecutableTypes.WORKFLOW:
            self.update_renamed_workflow_steps(tool_cwl)
        inputs = self.get_inputs_dict(exe)
        token_inputs = [
            input_id
            for input_id, value in inputs.items()
            if value == "HELIPORTToken"
            or isinstance(value, dict)
            and value.get("type") == "HELIPORTToken"
        ]
        for input_id in token_inputs:
            cwl_input = inputs[input_id]
            if isinstance(cwl_input, dict):
                cwl_input["type"] = "string"
            else:
                cwl_input = "string"
            inputs[input_id] = cwl_input
        tool_cwl["inputs"] = inputs
        if token_inputs:
            namespaces = tool_cwl.get("$namespaces", {})
            namespaces["cwltool"] = "http://commonwl.org/cwltool#"
            tool_cwl["$namespaces"] = namespaces
            hints = tool_cwl.get("hints", {})
            secret_hint = hints.get("cwltool:Secrets", {})
            secrets = secret_hint.get("secrets", [])
            secrets.extend(token_inputs)
            secret_hint["secrets"] = secrets
            hints["cwltool:Secrets"] = secret_hint
            tool_cwl["hints"] = hints
        return tool_cwl, token_inputs

    def set_tokens(self, job_cwl, token_inputs):  # noqa: D102
        if token_inputs:
            user = self.login.user.auth_user
            auth_token, token = AuthToken.objects.create(user)
            NamedToken.objects.create(
                name="token for job execution", auth_token=auth_token
            )
            for input_id in token_inputs:
                job_cwl[input_id] = str(token)
        return job_cwl

    @staticmethod
    def update_renamed_workflow_steps(tool_cwl):  # noqa: D102
        regex = re.compile(r".*_(\d+).cwl$")
        for step in tool_cwl.get("steps", []):
            run = step.get("run", "")
            match = regex.match(run)
            try:
                exe_id = int(match.group(1))
            except AttributeError:
                continue
            try:
                exe = Executable.objects.get(pk=exe_id)
            except Executable.DoesNotExist:
                continue
            step["run"] = generate_cwl_filename(exe.cwl, exe)

    def prepare_job_folder(self):  # noqa: D102
        self.execute(f"mkdir -p {self.job_folder}")
        self.pwd = self.execute("pwd").last_complete_line
        self.execute(f"cd {self.job_folder}")

    def load_cwltool(self):  # noqa: D102
        test_command = "pip list | grep cwltool"
        cwl_installed = self.execute(test_command).status == 0

        if not cwl_installed:
            self.execute('echo "CWL TOOL NOT INSTALLED - GOING TO INSTALL IT"')
            self.execute("pip install cwltool")

    def load_venv(self):  # noqa: D102
        test_command = f"source {self.execution_directory}/venv/bin/activate"
        venv_exists = self.execute(test_command).status == 0

        if not venv_exists:
            self.execute('echo "GOING TO CREATE VENV"')
            self.execute(f"python -m venv {self.execution_directory}/venv")
            self.execute(f"source {self.execution_directory}/venv/bin/activate")

    def load_node_js(self):  # noqa: D102
        self.execute("export NODEJS_HOME=$PWD/node-v8.4.0-linux-x64")
        self.execute("export PATH=$PATH:$NODEJS_HOME/bin")
        node_installed = self.execute("node --version").status == 0

        if not node_installed:
            self.execute(
                'echo "NODE.js NOT FOUND BUT REQUIRED FOR InlineJavascriptRequirement - GOING TO INSTALL IT"'  # noqa: E501
            )
            self.execute(
                "wget https://nodejs.org/download/release/v8.4.0/node-v8.4.0-linux-x64.tar.gz"
            )
            self.execute("tar -xf node-v8.4.0-linux-x64.tar.gz")
            self.execute("export NODEJS_HOME=$PWD/node-v8.4.0-linux-x64")
            self.execute("echo $NODEJS_HOME")
            self.execute("export PATH=$PATH:$NODEJS_HOME/bin")
            self.execute("node --version")

    def test_node_requirement(self):  # noqa: D102
        for exe in Executable.objects.filter(projects__in=self.job.projects.all()):
            try:
                for requirement in exe.cwl.get("requirements", []):
                    if (
                        hasattr(requirement, "get")
                        and requirement.get("class") == "InlineJavascriptRequirement"
                    ):
                        return True
                    if requirement == "InlineJavascriptRequirement":
                        return True
            except TypeError:
                return (
                    True  # cwl uses unexpected features - install node just to be save
                )

        return False

    def build_ssh_shell(self):  # noqa: D102
        if self.login.type != self.login.LoginTypes.SSH:
            raise ValueError("not a ssh login")

        return create_command_executor(
            host=self.login.machine,
            username=self.login.username,
            key=self.login.key,
            indirection=self.login.build_command_executor_indirection(),
            shell=True,
            auto_connect=False,
            default_result_callback=self.append_job_output_text,
        )
