# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
from uuid import uuid4

from .cwl_generation import clean, generate_cwl_filename

logger = logging.getLogger(__name__)


def generate_cwl_nonempty_id(cwl):  # noqa: D103
    name = cwl.get("label")
    if not name:
        name = "new step"
    return clean(name)


def default_str_to_type_dict(port):  # noqa: D103
    if isinstance(port, str):
        return {"type": port}
    return port


def generate_port_data(cwl_ports):  # noqa: D103
    if isinstance(cwl_ports, dict):
        cwl_ports = [
            {"id": key, **default_str_to_type_dict(values)}
            for key, values in cwl_ports.items()
        ]

    return [{"label": clean(p.get("id")), "type": p.get("type")} for p in cwl_ports]


def data_for_visualization_node(executable):  # noqa: D103
    cwl = executable.cwl
    result = {}
    inputs = generate_port_data(cwl.get("inputs", []))
    outputs = generate_port_data(cwl.get("outputs", []))
    cwl_type = cwl.get("class", "no")
    if cwl_type == "CommandLineTool":
        cwl_type = "tool"
    elif cwl_type == "Workflow":
        cwl_type = "workflow"

    result["inputs"] = inputs
    result["outputs"] = outputs
    result["label"] = generate_cwl_nonempty_id(cwl)
    result["type"] = cwl_type
    result["run"] = generate_cwl_filename(cwl, executable)
    result["id"] = executable.executable_id
    return result


def add_unique_labels(nodes):  # noqa: D103
    labels = set()

    for node in nodes:
        label = node.get("label", "thing").replace("/", "_")
        num = 1
        unique_label = label
        while unique_label in labels:
            unique_label = f"{label}_{num}"
            num += 1
        labels.add(unique_label)
        node["unique_label"] = unique_label


def differentiate_nodes(nodes):  # noqa: D103
    inputs = []
    outputs = []
    steps = []
    for node in nodes:
        if node.get("type") in ["input", "file_input"]:
            inputs.append(node)
        elif node.get("type") in ["output", "file_output"]:
            outputs.append(node)
        else:
            steps.append(node)
    return inputs, outputs, steps


def get_label_index(inputs, steps):  # noqa: D103
    label_index = {}
    for input_node in inputs:
        port = input_node["outputs"][0]
        label_index[port["id"]] = input_node["unique_label"]
    for s in steps:
        for port in s.get("outputs", []):
            label_index[port["id"]] = f"{s['unique_label']}/{port['label']}"

    return label_index


def get_edge_sources(edges, label_index):  # noqa: D103
    # if multiple sources, use last one - this may be changed to support array types
    edge_sources = {}
    for edge in edges:
        if "end" not in edge:
            logger.error(f"edge has no end {edges}")
        edge_sources[edge["end"]] = label_index[edge["start"]]
    return edge_sources


def visualization_data_to_cwl(data, name, description):  # noqa: D103
    nodes = data.get("nodes", [])
    edges = data.get("edges", [])
    view = data.get("canvas", {})

    add_unique_labels(nodes)
    inputs, outputs, steps = differentiate_nodes(nodes)
    edge_sources = get_edge_sources(edges, get_label_index(inputs, steps))

    cwl = {
        "cwlVersion": "v1.0",
        "class": "Workflow",
        "label": name,
        "doc": description if description else None,
        "sbg:scale": view.get("scale", 1),
        "sbg:x": view.get("x", 0),
        "sbg:y": view.get("y", 0),
        "requirements": [{"class": "SubworkflowFeatureRequirement"}],
        "inputs": [
            {
                "id": i["unique_label"],
                "type": i["outputs"][0]["type"],
                "sbg:x": i.get("x", 0),
                "sbg:y": i.get("y", 0),
            }
            for i in inputs
        ],
        "outputs": [
            {
                "id": o["unique_label"],
                "type": o["inputs"][0]["type"],
                "outputSource": edge_sources.get(o["inputs"][0]["id"]),
                "sbg:x": o.get("x", 0),
                "sbg:y": o.get("y", 0),
            }
            for o in outputs
        ],
        "steps": [
            {
                "id": s["unique_label"],
                "run": s["user_data"]["run"],
                "sbg:type": s["type"],
                "in": [
                    {
                        "id": port["label"],
                        "sbg:type": port["type"],
                        "source": edge_sources.get(port["id"]),
                    }
                    for port in s.get("inputs", [])
                ],
                "out": [
                    {"id": port["label"], "sbg:type": port["type"]}
                    for port in s.get("outputs", [])
                ],
                "sbg:x": s.get("x", 0),
                "sbg:y": s.get("y", 0),
            }
            for s in steps
        ],
        "$namespaces": {"sbg": "https://heliport/visualization"},
    }

    if cwl["doc"] is None:
        del cwl["doc"]
    if cwl["sbg:scale"] == 1:
        del cwl["sbg:scale"]
    if cwl["sbg:x"] == 0:
        del cwl["sbg:x"]
    if cwl["sbg:y"] == 0:
        del cwl["sbg:y"]
    return cwl


def cwl_to_visualization_data(cwl):  # noqa: D103
    nodes = []
    edges = []

    for i in cwl.get("inputs", []):
        nodes.append(
            {
                "label": i["id"],
                "type": "file_input" if i.get("type") == "File" else "input",
                "x": i.get("sbg:x", 0),
                "y": i.get("sbg:y", 0),
                "outputs": [
                    {"id": i["id"], "label": i["id"], "type": i.get("type", "ERROR")}
                ],
            }
        )

    for o in cwl.get("outputs", []):
        output_id = str(uuid4())
        nodes.append(
            {
                "label": o["id"],
                "type": "file_output" if o.get("type") == "File" else "output",
                "x": o.get("sbg:x", 0),
                "y": o.get("sbg:y", 0),
                "inputs": [
                    {"id": output_id, "label": o["id"], "type": o.get("type", "ERROR")}
                ],
            }
        )

        start = o["outputSource"]
        if isinstance(start, list):
            start = start[0]
        if start is not None:
            edges.append({"start": start, "end": output_id})

    for s in cwl.get("steps", []):
        for port in s["in"]:
            port["uuid"] = str(uuid4())
            if port["source"] is None:
                continue

            edges.append({"start": port["source"], "end": port["uuid"]})

        nodes.append(
            {
                "label": s["id"],
                "type": s.get("sbg:type", "ERROR"),
                "x": s.get("sbg:x", 0),
                "y": s.get("sbg:y", 0),
                "run": s.get("run", "ERROR no tool"),
                "inputs": [
                    {
                        "id": port["uuid"],
                        "label": port["id"],
                        "type": port.get("sbg:type", "ERROR"),
                    }
                    for port in s["in"]
                ],
                "outputs": [
                    {
                        "id": f"{s['id']}/{port['id']}",
                        "label": port["id"],
                        "type": port.get("sbg:type", "ERROR"),
                    }
                    for port in s["out"]
                ],
            }
        )

    return {
        "canvas": {
            "scale": cwl.get("sbg:scale", 1),
            "x": cwl.get("sbg:x", 0),
            "y": cwl.get("sbg:y", 0),
        },
        "edges": edges,
        "nodes": nodes,
    }
