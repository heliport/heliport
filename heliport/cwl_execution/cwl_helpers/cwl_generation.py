# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import json
import re
from collections import defaultdict

import environ
import yaml
from django.shortcuts import get_object_or_404

from heliport.cwl_execution.models import Executable, LoginInfo, json_load_dict

env = environ.Env()


def separate_prefix(underscore_dict, list_keys=None):  # noqa: D103
    list_keys = set() if list_keys is None else set(list_keys)

    categories = defaultdict(dict)
    for key, value in underscore_dict.items():
        key_path = key.split("_", 1)
        if len(key_path) == 2:
            category, sub_key = key_path
            if key not in list_keys:
                categories[category][sub_key] = value
            else:
                categories[category][sub_key] = underscore_dict.getlist(key)
    return categories


def set_attrs_job(job, attrs):  # noqa: D103
    for key, value in attrs.items():
        if key == "ssh_login":
            job.ssh_login = get_object_or_404(LoginInfo, pk=int(value))
        elif key == "interval":
            if value:
                job.interval = value
                job.repeat = True
            else:
                job.repeat = False
        else:
            setattr(job, key, value)


def clean(s):  # noqa: D103
    return re.sub("[^0-9a-zA-Z_]", "_", str(s))


def filter_executables(executables, query_string):  # noqa: D103
    for word in query_string.split():
        w = word.lower()
        exe_type = -1
        if w in "tool":
            exe_type = Executable.ExecutableTypes.TOOL
        elif w in "workflow":
            exe_type = Executable.ExecutableTypes.WORKFLOW
        elif w in "arbitrary cwl":
            exe_type = Executable.ExecutableTypes.ARBITRARY_CWL

        executables = filter(
            lambda e: w in e.cwl_json.lower() or e.type == exe_type, executables
        )
    return executables


def parse_user_cwl(cwl, cwl_type):  # noqa: D103
    if not isinstance(cwl, str):
        return None, "please use cwl of type str"

    if not cwl:
        return None, "please specify cwl"

    if cwl_type == Executable.CWLTypes.YAML or cwl_type.lower() == "yaml":
        try:
            obj = yaml.safe_load(cwl)
        except yaml.YAMLError as e:
            if hasattr(e, "problem_mark"):
                if e.context is not None:
                    return (
                        None,
                        f"yaml parse failed: {e.problem_mark}, {e.problem} {e.context}",
                    )
                return None, f"yaml parse failed: {e.problem_mark}, {e.problem}"
            return None, f"yaml parse failed: {e}"

    elif cwl_type == Executable.CWLTypes.JSON or cwl_type.lower() == "json":
        try:
            obj = json.loads(cwl)
        except ValueError as e:
            return None, f"json parse failed: {e}"

    else:
        return None, f"cwl type {cwl_type} not supported"

    if not isinstance(obj, dict):
        return None, f"expected dict as cwl but got {type(obj)}"

    return obj, None


def generate_cwl_filename(cwl, exe):  # noqa: D103
    if isinstance(cwl, str):
        cwl = json_load_dict(cwl)

    label = clean(cwl.get("label", "unnamed"))
    return f"{label}_{exe.executable_id}.cwl"
