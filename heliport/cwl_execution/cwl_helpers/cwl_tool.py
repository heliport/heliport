# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import re
import string

from .cwl_parameter import cwl_split_type


def base_command_list_encode(base_command):  # noqa: D103
    sub_commands = re.findall(r"""([^\s"']+)|"([^"]*)"|'([^']*)'""", base_command)
    if len(sub_commands) > 1:
        return [c[0] or c[1] or c[2] for c in sub_commands]
    return base_command


def base_command_decode_to_str(base_command):  # noqa: D103
    if isinstance(base_command, list):
        return " ".join(
            repr(part)
            if set(part) - set(string.digits + string.ascii_letters + "-_")
            else part
            for part in base_command
        )
    return base_command


def tool_description_to_cwl(description_dict):  # noqa: D103
    main = description_dict.get("main", {})
    inputs = description_dict.get("inputs", [])
    outputs = description_dict.get("outputs", [])
    capture = description_dict.get("capture", {})
    for param in inputs:
        # in cwl all parameters called 'path' are assumed to be of type 'File'
        if param.get("id") == "path" and param.get("type") != "File":
            param["id"] = "path_str"

    cwl = {
        "cwlVersion": "v1.0",
        "class": "CommandLineTool",
        "baseCommand": base_command_list_encode(
            main.get("baseCommand", "echo ERROR: this tool has no base command")
        ),
        "label": main.get("label", ""),
        "doc": main.get("doc"),
        "inputs": {
            param.get("id", f"ERROR: no id specified{i}"): {
                "type": f"{param.get('type', 'string')}{param.get('count', '')}",
                "inputBinding": {"position": i, "prefix": param.get("prefix")},
                "label": param.get("label", ""),
            }
            for i, param in enumerate(inputs)
        },
        "outputs": {
            param.get("id", f"ERROR: no id specified{i}"): {
                "type": "File"
                if param.get("type") == "File"
                else {"type": "array", "items": "File"},
                "outputBinding": {"glob": param.get("glob", "*")},
                "label": param.get("label", ""),
            }
            for i, param in enumerate(outputs)
        },
    }

    if not cwl["doc"]:
        del cwl["doc"]
    for i in cwl["inputs"].values():
        if not i["label"]:
            del i["label"]
        if not i["inputBinding"]["prefix"]:
            del i["inputBinding"]["prefix"]
    for o in cwl["outputs"].values():
        if not o["label"]:
            del o["label"]

    if capture.get("stdout"):
        cwl["outputs"][capture["stdout"]] = {"type": "stdout"}
    if capture.get("stdout_file"):
        cwl["stdout"] = capture["stdout_file"]
    if capture.get("stderr"):
        cwl["outputs"][capture["stderr"]] = {"type": "stderr"}
    if capture.get("stdout_file"):
        cwl["stderr"] = capture["stderr_file"]

    return cwl


def cwl_to_tool_description(cwl_dict):  # noqa: D103
    inputs = cwl_dict.get("inputs", [])
    outputs = cwl_dict.get("outputs", [])

    if isinstance(inputs, dict):
        inputs = [{"id": key, **values} for key, values in inputs.items()]
    if isinstance(outputs, dict):
        outputs = [{"id": key, **values} for key, values in outputs.items()]
    stdout = None
    stderr = None
    for output in outputs:
        if output.get("type") == "stdout":
            stdout = output
        if output.get("type") == "stderr":
            stderr = output

    return {
        "main": {
            "baseCommand": base_command_decode_to_str(cwl_dict.get("baseCommand", "")),
            "label": cwl_dict.get("label", ""),
            "doc": cwl_dict.get("doc", ""),
        },
        "inputs": [
            {
                "id": param.get("id", ""),
                "label": param.get("label", ""),
                "type": cwl_split_type(param.get("type", "")).type,
                "count": cwl_split_type(param.get("type", "")).count,
                "prefix": param.get("inputBinding", {}).get("prefix", ""),
            }
            for param in sorted(
                inputs, key=lambda p: p.get("inputBinding", {}).get("position", 0)
            )
        ],
        "outputs": [
            {
                "id": param.get("id", ""),
                "label": param.get("label", ""),
                "type": "File" if param.get("type") == "File" else "File_array",
                "glob": param.get("outputBinding", {}).get("glob", ""),
            }
            for param in outputs
            if param.get("type") not in ["stdout", "stderr"]
        ],
        "capture": {
            "stdout": "" if stdout is None else stdout.get("id", ""),
            "stdout_file": cwl_dict.get("stdout", ""),
            "stderr": "" if stderr is None else stderr.get("id", ""),
            "stderr_file": cwl_dict.get("stderr", ""),
        },
    }
