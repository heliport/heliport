# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import json
from collections import namedtuple
from typing import Any, Dict, List, Union

import yaml

from .cwl_parameter import CwlParameter, ParameterValue


class CwlJob:  # noqa: D101
    def __init__(  # noqa: D107
        self,
        parameters: List[CwlParameter],
        job_dict: Dict[str, Union[Any, ParameterValue]] = None,
        prefer_json=False,
        tag_class=None,
    ):
        if job_dict is None:
            job_dict = {}
        self.prefer_json = prefer_json
        self.original_parameters = parameters
        self.tag_class = tag_class

        self.dict = {}
        self.unsupported_dict = {}

        for parameter in parameters:
            p = job_dict.get(parameter.name)
            if parameter.supported:
                self.dict[parameter.name] = parameter.generate_parameter_value(p)
            elif isinstance(p, ParameterValue):
                self.unsupported_dict[parameter.name] = p.cwl
            else:
                self.unsupported_dict[parameter.name] = p

    def get_value(self, name):  # noqa: D102
        parameter_value = self.dict.get(name)
        if parameter_value is None:
            return None
        return parameter_value.value

    @property
    def is_valid(self):  # noqa: D102
        return all(p.is_valid for p in self.dict.values())

    @property
    def cwl(self):  # noqa: D102
        result = {key: value.cwl for key, value in self.dict.items()}
        result.update(self.unsupported_dict)
        return result

    @property
    def json(self):  # noqa: D102
        return json.dumps(self.cwl, indent=3)

    @property
    def yaml(self):  # noqa: D102
        return yaml.safe_dump(self.cwl)

    @property
    def cwl_str(self):  # noqa: D102
        if self.prefer_json:
            return self.json
        return self.yaml

    @property
    def unsupported_cwl_str(self):  # noqa: D102
        if self.prefer_json:
            return json.dumps(self.unsupported_dict)
        return yaml.safe_dump(self.unsupported_dict)

    @property
    def has_parameters(self):  # noqa: D102
        return bool(self.dict or self.unsupported_dict)

    @property
    def parameter_details(self):  # noqa: D102
        details = namedtuple(
            "ParameterDetails",
            [
                "name",
                "description",
                "type",
                "count",
                "string_typed",
                "type_name",
                "value",
                "supported_type",
                "value_list",
                "addable",
                "tag_list",
            ],
        )
        string_typed_types = ["string", "long", "int", "double", "float"]
        name_dict = {
            "string": "String",
            "boolean": "Boolean",
            "long": "Integer",
            "int": "int32",
            "double": "Floating Point Number",
            "float": "float32",
        }
        return [
            details(
                name=p.name,
                description=p.description,
                type=p.type_type,
                count=p.type_count,
                string_typed=p.type_type in string_typed_types,
                type_name=name_dict.get(p.type_type, p.type_type),
                supported_type=p.supported,
                value=self.get_value(p.name),
                value_list=self.as_list(self.get_value(p.name), p.type_count),
                addable=p.type_count == "[]"
                or len(self.as_list(self.get_value(p.name), p.type_count)) == 0,
                tag_list=self.get_tag_list(p.name),
            )
            for p in self.original_parameters
        ]

    @staticmethod
    def as_list(value, count):  # noqa: D102
        if count == "":
            return [value]
        if count == "?":
            if value is None:
                return []
            return [value]
        if isinstance(value, list):
            return value
        return []

    def get_tag_list(self, name):  # noqa: D102
        v = self.dict.get(name)
        if v is None or self.tag_class is None:
            return []
        result = []
        for tag_id in v.tag_list:
            tag = self.tag_class.objects.filter(pk=tag_id).first()
            if tag is not None:
                result.append(
                    {
                        "color": tag.html_color,
                        "name": tag.label,
                        "id": tag.pk,
                        "foreground": tag.html_foreground_color,
                    }
                )
        return result
