# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import abc
import typing
from collections import namedtuple

from heliport.data_source.models import DataSource


def cwl_split_type(type_str):  # noqa: D103
    TypeInfo = namedtuple("TypeInfo", "type count")
    for count in ["?", "[]"]:
        if type_str.endswith(count):
            return TypeInfo(type_str[: -len(count)], count)
    return TypeInfo(type_str, "")


class CwlParameter:  # noqa: D101
    def __init__(self, name, value_type, description=""):  # noqa: D107
        self.name = name
        self.type = value_type
        self.description = description

    def generate_parameter_value(self, value=None):  # noqa: D102
        return supported_parameter_types[self.type](value)

    @property
    def supported(self):  # noqa: D102
        try:
            return self.type in supported_parameter_types
        except TypeError:
            return False

    @property
    def type_type(self):  # noqa: D102
        return cwl_split_type(self.type).type

    @property
    def type_count(self):  # noqa: D102
        return cwl_split_type(self.type).count


class ParameterValue:  # noqa: D101
    @property
    @abc.abstractmethod
    def cwl(self):  # noqa: D102
        pass

    @property
    @abc.abstractmethod
    def is_valid(self):  # noqa: D102
        pass

    @property
    @abc.abstractmethod
    def value(self):
        """Value used in template."""
        pass

    @property
    def tag_list(self):  # noqa: D102
        return []


def simple_parameter_value(value_type):  # noqa: D103
    class SimpleParameterValue(ParameterValue):
        def __init__(self, value=None):
            if value is None:
                self._value = None
            else:
                try:
                    self._value = value_type(value)
                except (TypeError, ValueError):
                    self._value = None

        @property
        def cwl(self):
            assert self.is_valid
            return self._value

        @property
        def is_valid(self):
            return isinstance(self._value, value_type)

        @property
        def value(self):
            return self._value

    return SimpleParameterValue


class BooleanParameterValue(ParameterValue):  # noqa: D101
    def __init__(self, value=None):  # noqa: D107
        if value is None:
            self._value = None
        elif isinstance(value, str):
            self._value = value.lower() == "true"
        else:
            self._value = bool(value)

    @property
    def cwl(self):  # noqa: D102
        assert self.is_valid
        return self._value

    @property
    def is_valid(self):  # noqa: D102
        return isinstance(self._value, bool)

    @property
    def value(self):  # noqa: D102
        return self._value


class AutoValue(ParameterValue):  # noqa: D101
    cwl = "auto_value"
    is_valid = True
    value = "auto_value"

    def __init__(self, value=None):  # noqa: D107
        pass


def file_or_directory(type_str):  # noqa: D103
    class FileParameterValue(ParameterValue):
        data_source: typing.Optional[DataSource]

        def __init__(self, data_source=None):
            if data_source is None:
                self.data_source = None

            elif isinstance(data_source, DataSource):
                self.data_source = data_source

            elif isinstance(data_source, dict):
                if (
                    data_source.get("class") != type_str
                    or "location" not in data_source
                    or "_" not in data_source["location"]
                ):
                    self.data_source = None
                else:
                    pk = data_source["location"].split("_")[0]
                    self.data_source = DataSource.objects.get(pk=pk)

            elif str(data_source).isdecimal():
                self.data_source = DataSource.objects.get(pk=str(data_source))

            else:
                self.data_source = None

        @property
        def cwl(self):
            assert self.is_valid
            return {
                "class": type_str,
                "location": f"{self.data_source.pk}_{self.data_source.uri}",
            }

        @property
        def is_valid(self):
            return isinstance(self.data_source, DataSource)

        @property
        def value(self):
            if self.data_source is None:
                return None
            return self.data_source.pk

    return FileParameterValue


def make_optional(parameter_value_class):  # noqa: D103
    class OptionalParameterValue(ParameterValue):
        def __init__(self, value=None):
            if value is None:
                self.child = None
            else:
                self.child = parameter_value_class(value)

        @property
        def cwl(self):
            if self.child is None:
                return None
            return self.child.cwl

        @property
        def is_valid(self):
            return self.child is None or self.child.is_valid

        @property
        def value(self):
            if self.child is None:
                return None
            return self.child.value

    return OptionalParameterValue


def make_array(parameter_value_class):  # noqa: D103
    class ArrayParameterValue(ParameterValue):
        def __init__(self, value=None):
            self.children = []
            self.tags = []

            if value is not None:
                if not isinstance(value, list):
                    value = [value]
                for v in value:
                    if isinstance(v, dict) and v.get("class") == "FromTags":
                        self.tags = sorted(
                            set(str(v.get("tags")).split(",")), key=try_int
                        )
                    else:
                        self.children.append(parameter_value_class(v))

        @property
        def cwl(self):
            return [child.cwl for child in self.children]

        @property
        def is_valid(self):
            return all(child.is_valid for child in self.children)

        @property
        def value(self):
            return [child.value for child in self.children]

        @property
        def tag_list(self):
            return self.tags

    return ArrayParameterValue


def try_int(s: str) -> int:  # noqa: D103
    try:
        return int(s)
    except (ValueError, TypeError):
        return 0


basic_supported_parameter_types = {
    "string": simple_parameter_value(str),
    "int": simple_parameter_value(int),
    "long": simple_parameter_value(int),
    "float": simple_parameter_value(float),
    "double": simple_parameter_value(float),
    "boolean": BooleanParameterValue,
    "File": file_or_directory("File"),
    "Directory": file_or_directory("Directory"),
}
supported_parameter_types = {}
for key, value_class in basic_supported_parameter_types.items():
    supported_parameter_types[key] = value_class
    supported_parameter_types[f"{key}?"] = make_optional(value_class)
    supported_parameter_types[f"{key}[]"] = make_array(value_class)

supported_parameter_types["HELIPORTToken"] = AutoValue
