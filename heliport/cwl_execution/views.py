# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

import json
import logging
from collections import defaultdict

import requests
import yaml
from ansi2html.converter import Ansi2HTMLConverter
from celery import current_app as celery_app
from django.conf import settings
from django.db.models import Q
from django.http import Http404, HttpResponse, HttpResponseForbidden, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils import timezone
from django.views.generic import DetailView, ListView, TemplateView, UpdateView
from rest_framework import permissions as rest_permissions
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.views import Response as RestResponse

from heliport.core.mixins import (
    HeliportLoginRequiredMixin,
    HeliportObjectMixin,
    HeliportProjectMixin,
)
from heliport.core.models import (
    DigitalObject,
    DigitalObjectRelation,
    HeliportUser,
    LoginInfo,
    Project,
    Tag,
    Vocabulary,
    assert_relation,
)
from heliport.core.permissions import (
    HeliportObjectPermission,
    has_permission_for,
    is_object_member,
)
from heliport.core.utils.queries import get_or_create_tag
from heliport.core.views import HeliportModelViewSet
from heliport.data_source.models import DataSource

from . import tasks
from .cwl_helpers import cwl_generation, cwl_tool, cwl_workflow
from .cwl_helpers.cwl_job import CwlJob
from .models import Executable, ExecutionLog, Job, UserJobConfig, json_load_dict
from .serializers import (
    ExecutableSerializer,
    ExecutionLogSerializer,
    JobSerializer,
    UserJobConfigSerializer,
)

logger = logging.getLogger(__name__)


class ArbitraryCWLListView(HeliportProjectMixin, ListView):  # noqa: D101
    model = Executable
    template_name = "cwl_execution/arbitrary_cwl.html"
    context_object_name = "arbitrary_cwls"

    def get_queryset(self):  # noqa: D102
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        cwl_t = Executable.ExecutableTypes.ARBITRARY_CWL
        return Executable.objects.filter(
            type=cwl_t, projects=project, deleted__isnull=True
        )

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view)."""
        context = super().get_context_data(**kwargs)
        context["cwl_types"] = Executable.CWLTypes.choices
        context["JSON_TYPE"] = Executable.CWLTypes.JSON
        context["YAML_TYPE"] = Executable.CWLTypes.YAML
        context["update"] = False
        return context

    def post(self, request, *args, **kwargs):  # noqa: D102
        cwl_type = request.POST.get("cwl_type")
        cwl = request.POST.get("cwl")
        executable_id = request.POST.get("remove")

        if executable_id:
            executable = get_object_or_404(Executable, pk=executable_id)
            executable.mark_deleted(self.request.user.heliportuser)
        elif cwl is not None:
            cwl_type = int(cwl_type)
            super().get(request, *args, **kwargs)
            context = self.get_context_data()
            context["cwl_type"] = cwl_type
            context["cwl"] = cwl

            if cwl_type == Executable.CWLTypes.JSON:
                try:
                    cwl = json.loads(cwl)
                    if not isinstance(cwl, dict):
                        context["message"] = f"expected dict as cwl but got {type(cwl)}"
                        return render(request, self.template_name, context)
                except ValueError as e:
                    context["message"] = f"JSON parse failed: {e}"
                    return render(request, self.template_name, context)

            elif cwl_type == Executable.CWLTypes.YAML:
                try:
                    cwl = yaml.safe_load(cwl)
                    if not isinstance(cwl, dict):
                        context["message"] = f"expected dict as cwl but got {type(cwl)}"
                        return render(request, self.template_name, context)
                except yaml.YAMLError as e:
                    context["message"] = f"YAML parse failed: {e}"
                    return render(request, self.template_name, context)
            else:
                return render(request, self.template_name, context)

            new_arbitrary_cwl = Executable(
                type=Executable.ExecutableTypes.ARBITRARY_CWL, cwl_type=cwl_type
            )
            new_arbitrary_cwl.category_str = settings.EXECUTABLE_ARBITRARY_CWL_NAMESPACE
            new_arbitrary_cwl.cwl = cwl
            new_arbitrary_cwl.save()
            new_arbitrary_cwl.projects.add(context["project"])

        return redirect(
            "cwl_execution:arbitrary_cwl_list", project=self.kwargs["project"]
        )


class ArbitraryCWLUpdateView(HeliportObjectMixin, UpdateView):  # noqa: D101
    model = Executable
    template_name = "cwl_execution/arbitrary_cwl.html"
    fields = ["cwl_json", "cwl_type"]

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the CWL.
        """
        context = super().get_context_data(**kwargs)
        project = context["project"]
        cwl_t = Executable.ExecutableTypes.ARBITRARY_CWL
        context["arbitrary_cwls"] = Executable.objects.filter(
            type=cwl_t, projects=project, deleted__isnull=True
        )
        context["cwl_types"] = Executable.CWLTypes.choices
        context["JSON_TYPE"] = Executable.CWLTypes.JSON
        context["YAML_TYPE"] = Executable.CWLTypes.YAML
        context["cwl_type"] = self.object.cwl_type

        cwl_dict = json_load_dict(
            self.object.cwl_json,
            {
                "baseCommand": [
                    "echo",
                    "INVALID SYNTAX! This Arbitrary CWL does not work because of a syntax error.",  # noqa: E501
                ],
                "class": "CommandLineTool",
                "cwlVersion": "v1.0",
                "inputs": [],
                "outputs": [],
                "label": "SYNTAX ERROR",
                "doc": "SYNTAX ERROR",
            },
        )

        if self.object.cwl_type == Executable.CWLTypes.YAML:
            context["cwl"] = yaml.safe_dump(cwl_dict, sort_keys=False)
        else:
            context["cwl"] = json.dumps(cwl_dict, indent=3, sort_keys=False)

        context["update"] = True
        return context

    def post(self, request, *args, **kwargs):  # noqa: D102
        cwl_type = request.POST.get("cwl_type")
        cwl = request.POST.get("cwl")

        if cwl is not None:
            cwl_type = int(cwl_type)
            super().get(request, *args, **kwargs)
            context = self.get_context_data()
            context["cwl_type"] = cwl_type
            context["cwl"] = cwl

            if cwl_type == Executable.CWLTypes.JSON:
                try:
                    cwl = json.loads(cwl)
                    if not isinstance(cwl, dict):
                        context["message"] = f"expected dict as cwl but got {type(cwl)}"
                        return render(request, self.template_name, context)
                except ValueError as e:
                    context["message"] = f"JSON parse failed: {e}"
                    return render(request, self.template_name, context)

            elif cwl_type == Executable.CWLTypes.YAML:
                try:
                    cwl = yaml.safe_load(cwl)
                    if not isinstance(cwl, dict):
                        context["message"] = f"expected dict as cwl but got {type(cwl)}"
                        return render(request, self.template_name, context)
                except yaml.YAMLError as e:
                    context["message"] = f"YAML parse failed: {e}"
                    return render(request, self.template_name, context)
            else:
                return render(request, self.template_name, context)

            executable = self.get_object()
            executable.cwl_type = cwl_type
            executable.cwl = cwl
            executable.save()

        return redirect(
            "cwl_execution:arbitrary_cwl_list", project=self.kwargs["project"]
        )


class ToolListView(HeliportProjectMixin, ListView):  # noqa: D101
    model = Executable
    template_name = "cwl_execution/tool_list.html"
    context_object_name = "tools"

    def get_queryset(self):  # noqa: D102
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        cwl_t = Executable.ExecutableTypes.TOOL
        return Executable.objects.filter(
            projects=project, type=cwl_t, deleted__isnull=True
        )

    def post(self, request, *args, **kwargs):  # noqa: D102
        tool_to_delete = get_object_or_404(Executable, pk=request.POST.get("delete"))
        tool_to_delete.mark_deleted(self.request.user.heliportuser)
        return redirect("cwl_execution:tool_list", project=self.kwargs["project"])


class ExecutableRawYamlView(DetailView):  # noqa: D101
    model = Executable

    def get(self, request, *args, **kwargs):  # noqa: D102
        exe = self.get_object()
        self.check_permission(exe, request)
        result = self.redirect_part(exe, kwargs)
        if result is not None:
            return result

        try:
            generated_cwl_yml = yaml.safe_dump(
                json.loads(exe.cwl_json), sort_keys=False
            )
        except ValueError as e:
            generated_cwl_yml = f"cannot convert json to yaml: {e}\n\n{exe.cwl}"

        return HttpResponse(generated_cwl_yml, content_type="text/plain")

    @staticmethod
    def redirect_part(exe, kwargs):  # noqa: D102
        result = None
        if "part" in kwargs and kwargs["part"] != "main":
            url = reverse("cwl_execution:exe_cwl_json", kwargs={"pk": exe.pk})
            result = redirect(f"{url}#{kwargs['part']}")
        return result

    @staticmethod
    def check_permission(exe, request):  # noqa: D102
        user = request.user
        if not hasattr(user, "heliportuser"):
            raise Http404()
        if not has_permission_for(user.heliportuser, exe):
            raise Http404()


class ExecutableRawJsonView(DetailView):
    """URLs of this view also used in cwl identifiers."""

    model = Executable

    def get(self, request, *args, **kwargs):  # noqa: D102
        exe = self.get_object()
        ExecutableRawYamlView.check_permission(exe, request)
        result = ExecutableRawYamlView.redirect_part(exe, kwargs)
        if result is not None:
            return result
        return JsonResponse(exe.cwl)


class ToolJsonDescriptionView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Executable

    def get(self, request, *args, **kwargs):  # noqa: D102
        obj = self.get_object()
        description = cwl_tool.cwl_to_tool_description(obj.cwl)
        return JsonResponse(description)


class ToolUpdateView(HeliportObjectMixin, DetailView):  # noqa: D101
    template_name = "cwl_execution/tool_edit.html"
    model = Executable

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes to render template for editing.
        """
        context = super().get_context_data(**kwargs)
        context["edit_mode"] = True
        return context

    def post(self, request, *args, **kwargs):  # noqa: D102
        tool = self.get_object()
        tool_description = json.loads(request.POST.get("tool_json", "{}"))
        tool.cwl = cwl_tool.tool_description_to_cwl(tool_description)
        tool.save()

        return redirect("cwl_execution:tool_list", project=self.kwargs["project"])


class ToolCreateView(HeliportProjectMixin, TemplateView):  # noqa: D101
    template_name = "cwl_execution/tool_edit.html"

    def post(self, request, *args, **kwargs):  # noqa: D102
        project = get_object_or_404(Project, pk=kwargs["project"])
        new_tool = Executable(type=Executable.ExecutableTypes.TOOL)
        tool_description = json.loads(request.POST.get("tool_json", "{}"))
        tool_cwl = cwl_tool.tool_description_to_cwl(tool_description)
        new_tool.category_str = settings.EXECUTABLE_TOOL_NAMESPACE
        new_tool.cwl = tool_cwl
        new_tool.save()
        new_tool.projects.add(project)

        return redirect("cwl_execution:tool_list", project=kwargs["project"])


class WorkflowListView(HeliportProjectMixin, ListView):  # noqa: D101
    model = Executable
    template_name = "cwl_execution/workflow_list.html"
    context_object_name = "workflows"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the data sources to warn about if no exist and the
        Workflow uses some.
        """
        context = super().get_context_data(**kwargs)
        context["data_sources"] = DataSource.objects.filter(
            projects=context["project"], deleted__isnull=True
        )

        return context

    def get_queryset(self):  # noqa: D102
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        cwl_t = Executable.ExecutableTypes.WORKFLOW
        return Executable.objects.filter(
            type=cwl_t, projects=project, deleted__isnull=True
        )

    def post(self, request, *args, **kwargs):  # noqa: D102
        workflow = request.POST.get("delete")
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        if workflow:
            exe = get_object_or_404(Executable, pk=workflow)
            exe.mark_deleted(self.request.user.heliportuser)

        return redirect("cwl_execution:workflow_list", project=project.pk)


class VisualizationDataView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Executable

    def get(self, request, *args, **kwargs):  # noqa: D102
        exe = self.get_object()
        full_workflow = request.GET.get("full_workflow")
        if full_workflow is not None and full_workflow.lower() != "false":
            if exe.type != Executable.ExecutableTypes.WORKFLOW:
                logger.error(
                    f'accessed "full_workflow" visualization data for type={exe.type}'
                )
            data = cwl_workflow.cwl_to_visualization_data(exe.cwl)
        else:
            data = cwl_workflow.data_for_visualization_node(exe)
        return JsonResponse(data)


class WorkflowUpdateView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Executable
    context_object_name = "workflow"
    template_name = "cwl_execution/workflow_edit.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes executables that can be added to workflow.
        """
        context = super().get_context_data(**kwargs)
        context["edit_mode"] = True
        query = self.request.GET.get("q")

        executables = set(
            Executable.objects.filter(projects=context["project"], deleted__isnull=True)
        )
        if query:
            executables = cwl_generation.filter_executables(executables, query)

        context["executables"] = list(executables)

        return context

    def post(self, request, *args, **kwargs):  # noqa: D102
        workflow = self.get_object()
        visualization_data = json.loads(request.POST.get("graph_cwl", "{}"))
        name = request.POST.get("name")
        description = request.POST.get("description")

        workflow.cwl = cwl_workflow.visualization_data_to_cwl(
            visualization_data, name, description
        )
        workflow.save()
        return redirect("cwl_execution:workflow_list", project=kwargs["project"])


class WorkflowCreateView(HeliportProjectMixin, TemplateView):  # noqa: D101
    model = Executable
    context_object_name = "workflow"
    template_name = "cwl_execution/workflow_edit.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes executables that can be added to workflow.
        """
        context = super().get_context_data(**kwargs)
        query = self.request.GET.get("q")

        project = get_object_or_404(Project, pk=self.kwargs["project"])
        executables = set(
            Executable.objects.filter(projects=project, deleted__isnull=True)
        )
        if query:
            executables = cwl_generation.filter_executables(executables, query)
        context["executables"] = list(executables)

        return context

    def post(self, request, *args, **kwargs):  # noqa: D102
        name = request.POST.get("name", "")
        description = request.POST.get("description")
        visualization_data = json.loads(request.POST.get("graph_cwl", "{}"))
        project = get_object_or_404(Project, pk=self.kwargs["project"])

        cwl = cwl_workflow.visualization_data_to_cwl(
            visualization_data, name, description
        )

        # create new workflow
        new_workflow = Executable(type=Executable.ExecutableTypes.WORKFLOW)
        new_workflow.category_str = settings.EXECUTABLE_WORKFLOW_NAMESPACE
        new_workflow.cwl = cwl
        new_workflow.save()
        new_workflow.projects.add(project)

        return redirect("cwl_execution:workflow_list", project=project.pk)


class JobParameterValueView(HeliportProjectMixin, TemplateView):  # noqa: D101
    template_name = "cwl_execution/job_parameter_input.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view)."""
        context = super().get_context_data(**kwargs)
        vocab = Vocabulary()
        param_name = self.request.GET["name"]
        values = self.request.GET.getlist(f"value_{param_name}")
        if self.request.GET.get("type", "") in ["File", "Directory"]:
            values = [int(v) if v.isdecimal() else v for v in values]
        remove = self.request.GET.get("remove")
        add_new = self.request.GET.get("add_new")
        if remove is not None:
            remove = int(remove)
            if remove < len(values):
                values.pop(remove)
        elif add_new:
            values.append(None)

        tag_list = self.request.GET.getlist(f"tags_{param_name}")
        tag_operation = self.request.GET.get("tag_operation")
        tag_remove = self.request.GET.get("tag_remove")
        tag_id = self.request.GET.get(f"tag_id_{param_name}")

        tags = []
        for tag in tag_list:
            if tag == "default":
                tags.append(tag)
                continue
            tag_obj = Tag.objects.filter(pk=tag).first()
            if tag_obj is not None:
                tags.append(self.serialize(tag_obj))

        if tag_operation == "add":
            params = self.request.GET
            tag = get_or_create_tag(
                project=context["project"],
                request=self.request,
                label=params.get(f"tag_input_{param_name}"),
                attribute_pk=params.get(f"tag_property_input_{param_name}"),
                attribute_label=params.get(f"tag_property_label_{param_name}"),
                value_pk=params.get(f"tag_value_input_{param_name}"),
                value_label=params.get(f"tag_value_label_{param_name}"),
            )
            tags.append(self.serialize(tag))
        elif tag_operation == "job_input":
            tags.append("default")
        elif str(tag_remove).isdecimal():
            tag_remove = int(tag_remove)
            if tag_remove < len(tags):
                tags.pop(tag_remove)
        elif str(tag_id).isdecimal():
            tag_obj = get_object_or_404(Tag, pk=tag_id)
            tags.append(self.serialize(tag_obj))

        count = self.request.GET.get("count", "")
        context["p"] = {
            "value_list": values,
            "type": self.request.GET.get("type", ""),
            "name": self.request.GET.get("name", ""),
            "count": count,
            "addable": len(values) == 0 or count == "[]",
            "tag_list": tags,
        }
        context["data_sources"] = DataSource.objects.filter(
            projects=context["project"], deleted__isnull=True
        )
        context["vocab"] = vocab
        return context

    @staticmethod
    def serialize(tag_obj):  # noqa: D102
        return {
            "color": tag_obj.html_color,
            "name": tag_obj.label,
            "id": tag_obj.pk,
            "foreground": tag_obj.html_foreground_color,
        }


class JobCreateView(HeliportObjectMixin, DetailView):  # noqa: D101
    template_name = "cwl_execution/job_create.html"
    model = Executable
    pk_url_kwarg = "executable"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the job to edit if it exists.
        """
        context = super().get_context_data(**kwargs)
        if "pk" in self.kwargs:
            job = Job.objects.get(pk=self.kwargs["pk"])
            assert job.base_executable == self.object
            context["job"] = job
            context["update"] = True

            context["job_setup_command"] = job.setup_command
            context["job_cmd_options"] = job.cmd_options
            default_values = job.cwl
        else:
            context["job_setup_command"] = (
                settings.HELIPORT_CWL_DEFAULT_JOB_SETUP_COMMAND
            )
            context["job_cmd_options"] = settings.HELIPORT_CWL_DEFAULT_JOB_OPTIONS
            default_values = None

        context["cwl_job"] = CwlJob(
            self.object.parameters, default_values, tag_class=Tag
        )

        context["data_sources"] = DataSource.objects.filter(
            projects=context["project"], deleted__isnull=True
        )
        context["vocab"] = Vocabulary()

        return context

    def post(self, request, *args, **kwargs):  # noqa: D102
        executable = self.get_object()
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        if "pk" in self.kwargs:
            job_object = get_object_or_404(Job, pk=self.kwargs["pk"])
        else:
            job_object = Job(
                output_text="Successfully created a workflow. To start it press the play button",  # noqa: E501
                output_code=Job.OutputCodes.MESSAGE,
                base_executable=executable,
            )
            job_object.category_str = settings.JOB_NAMESPACE
        user_inputs = cwl_generation.separate_prefix(
            request.POST,
            list_keys=[
                f"value_{parameter.name}"
                for parameter in executable.parameters
                if parameter.type_count == "[]"
            ]
            + [key for key in request.POST if key.startswith("tags_")],
        )

        cwl_generation.set_attrs_job(job_object, user_inputs["job"])
        try:
            cwl_job = CwlJob(
                executable.parameters,
                user_inputs["value"],
                prefer_json=request.POST.get("cwl_type") == executable.CWLTypes.JSON,
                tag_class=Tag,
            )
            generated_cwl = cwl_job.cwl
            validation_message = None
        except ValueError as e:
            validation_message = str(e)
            cwl_job = None
            generated_cwl = {}

        if "cwl" in request.POST:
            arbitrary_cwl, message = cwl_generation.parse_user_cwl(
                request.POST["cwl"], request.POST.get("cwl_type")
            )
        else:
            arbitrary_cwl = {}
            message = None
        if message is not None or validation_message is not None:
            super().get(request, *args, **kwargs)
            context = self.get_context_data()
            context["message"] = validation_message or message
            context["job"] = job_object
            if cwl_job is not None:
                context["cwl_job"] = cwl_job
            context["cwl"] = request.POST.get("cwl")
            context["job_setup_command"] = job_object.setup_command
            context["job_cmd_options"] = job_object.cmd_options
            context["vocab"] = Vocabulary()
            return render(request, self.template_name, context)

        generated_cwl.update(arbitrary_cwl)

        job_object.cwl_json = json.dumps(generated_cwl)
        job_object.save()
        job_object.cwl_json = json.dumps(
            self.add_tags(user_inputs["tags"], job_object.cwl, job_object)
        )
        job_object.save()

        if "pk" not in self.kwargs:
            job_object.projects.add(project)

        return redirect("cwl_execution:job_list", project=project.pk)

    def get_default_tag(self, already_saved_job):  # noqa: D102
        vocab = Vocabulary()
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        result = (
            Tag.objects.filter(
                attribute=vocab.input_of,
                value=already_saved_job,
                projects=project,
                deleted__isnull=True,
            )
            .order_by("-pk")
            .first()
        )
        if result is not None:
            return result
        result = Tag(
            label=f"{already_saved_job.label} input",
            attribute=vocab.input_of,
            value=already_saved_job,
        )
        result.html_color = "#00FF00"  # alternatively use pick_tag_color()
        result.category_str = settings.TAG_NAMESPACE
        result.save()
        result.projects.add(project)
        return result

    def add_tags(self, tag_dict, cwl, job):  # noqa: D102
        if not isinstance(cwl, dict):
            logger.warning("job cwl was not a dict")
            return cwl
        for key, value in cwl.items():
            if key in tag_dict and isinstance(value, list):
                tag_ids = [
                    str(tag_id if tag_id != "default" else self.get_default_tag(job).pk)
                    for tag_id in tag_dict[key]
                ]
                for tag_obj in Tag.objects.filter(tag_id__in=tag_ids):
                    tag_obj.update_usage_date()
                value.append({"class": "FromTags", "tags": ",".join(tag_ids)})
        return cwl


class JobListView(HeliportProjectMixin, ListView):  # noqa: D101
    template_name = "cwl_execution/job_list.html"
    model = UserJobConfig
    context_object_name = "userjobconfigs"

    def get_queryset(self):  # noqa: D102
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        user = self.request.user.heliportuser
        return (
            UserJobConfig.objects.filter(
                Q(job__projects=project, job__deleted__isnull=True)
                & (
                    Q(ssh_login__user=user)
                    | (
                        Q(ssh_login__isnull=True)
                        & ~Q(job__userjobconfig__ssh_login__user=user)
                    )
                )
            )
            .order_by("pk")
            .distinct()
        )

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the default login calculated from statistics of past
        executions for the user.
        """
        project = Project.objects.get(pk=self.kwargs["project"])
        user = self.request.user.heliportuser

        default_login = None
        logins = LoginInfo.objects.filter(user=user, type=LoginInfo.LoginTypes.SSH)
        if len(logins) == 1:
            default_login = logins.first()
        else:
            last_logins = ExecutionLog.objects.filter(
                start_time__gt=timezone.now() - timezone.timedelta(days=20),
                end_time__isnull=False,
                aborted=False,
                config__ssh_login__user=user,
            ).values_list("config__ssh_login", flat=True)
            histogram = defaultdict(int)
            for login in last_logins:
                histogram[login] += 1
            threshold = len(last_logins) * 0.9
            for login, count in histogram.items():
                if count > threshold:
                    default_login = LoginInfo.objects.get(pk=login)

        for job in Job.objects.filter(projects=project, deleted__isnull=True):
            if (
                UserJobConfig.objects.filter(
                    Q(job=job) & (Q(ssh_login__user=user) | Q(ssh_login__isnull=True))
                ).first()
                is None
            ):
                UserJobConfig(
                    job=job,
                    directory=settings.HELIPORT_CWL_DEFAULT_JOB_PATH,
                    ssh_login=default_login,
                ).save()

        context = super().get_context_data(**kwargs)
        context["running"] = [
            job.pk
            for job in Job.objects.filter(
                status=Job.StatusCodes.RUNNING, projects=project
            )
        ]

        user = self.request.user.heliportuser
        context["ssh_logins"] = user.logininfo_set.filter(type=LoginInfo.LoginTypes.SSH)
        return context

    def post(self, request, *args, **kwargs):  # noqa: D102
        job = get_object_or_404(Job, pk=request.POST["delete"])
        job.mark_deleted(self.request.user.heliportuser)

        return redirect("cwl_execution:job_list", project=self.kwargs["project"])


class JobRawYamlView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Job

    def get(self, request, *args, **kwargs):  # noqa: D102
        job = self.get_object()

        try:
            raw_yml = yaml.safe_dump(json.loads(job.cwl_json))
        except ValueError as e:
            raw_yml = f"cannot convert json to yaml: {e}\n\n{job.cwl_json}"

        return HttpResponse(raw_yml, content_type="text/plain")


class JobDetailView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Job
    template_name = "cwl_execution/job_detail.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the job output in html format.
        """
        context = super().get_context_data(**kwargs)
        converter = Ansi2HTMLConverter(dark_bg=False)
        context["output_text"] = converter.convert(self.object.output_text, full=False)
        context["execution_count"] = ExecutionLog.objects.filter(
            config__job=self.object
        ).count()
        context["running_executions"] = ExecutionLog.objects.filter(
            config__job=self.object, end_time__isnull=True
        )
        context["execution_logs"] = ExecutionLog.objects.filter(
            config__job=self.object
        ).order_by("-start_time")
        context["last_execution"] = (
            ExecutionLog.objects.filter(config__job=self.object, end_time__isnull=False)
            .order_by("-start_time")
            .first()
        )

        names = ExecutionLog.objects.filter(config__job=self.object).values_list(
            "config__ssh_login__user__display_name", flat=True
        )
        name_counts = defaultdict(int)
        for name in names:
            if name is not None:
                name = "Unknown Person"
            name_counts[name] += 1
        context["collaborators"] = sorted(
            [(count, name) for name, count in name_counts.items()], reverse=True
        )
        return context


class JobOutputView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = Job

    def get(self, request, *args, **kwargs):  # noqa: D102
        job = self.get_object()
        converter = Ansi2HTMLConverter(dark_bg=False)
        html = converter.convert(job.output_text, full=False)
        return HttpResponse(html)


def start_job(job_config, request, project_pk=None, debug=False):  # noqa: D103
    login = request.GET.get("login")
    directory = request.GET.get("directory")
    if login is not None and login != "null":
        job_config.ssh_login = LoginInfo.objects.get(pk=login)
    if directory is not None:
        job_config.directory = directory
    job_config.save()

    job = job_config.job
    user = request.user.heliportuser
    if job_config.ssh_login is None:
        job.output_code = job_config.job.OutputCodes.ERROR
        job.output_text = f"Permission denied: no login selected (access by {user})"
        job.save()
        return HttpResponseForbidden()

    if job_config.ssh_login.user != user:
        job.output_code = job_config.job.OutputCodes.ERROR
        job.output_text += (
            f"\nPermission denied: {user} used login by {job_config.ssh_login.user}"
        )
        job.save()
        return HttpResponseForbidden()

    if debug:
        logger.info(f"DEBUGGING JOB {job_config.job.pk}")
    else:
        logger.info(f"STARTING JOB {job.pk}")

    tasks.job_start.delay(job_config.pk, debug=debug)

    if project_pk is not None:
        return redirect("cwl_execution:job_list", project=project_pk)
    return None


class JobStartView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = UserJobConfig

    def get(self, request, *args, **kwargs):  # noqa: D102
        return start_job(self.get_object(), self.request, kwargs["project"])


class JobDebugView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = UserJobConfig

    def get(self, request, *args, **kwargs):  # noqa: D102
        return start_job(self.get_object(), self.request, kwargs["project"], debug=True)


class JobStopView(HeliportObjectMixin, DetailView):  # noqa: D101
    model = UserJobConfig

    def get(self, request, *args, **kwargs):  # noqa: D102
        job = self.get_object().job
        job.status = job.StatusCodes.STOPPED
        running = ExecutionLog.objects.filter(end_time__isnull=True, config__job=job)

        for log in running:
            logger.debug(
                f"{request.user.heliportuser.display_name} killed {job.label} job"
            )
            if log.execution_id:
                celery_app.control.revoke(log.execution_id)
            log.stop(kill=True)
            job.output_text += (
                f"\nkilled by {request.user.heliportuser.display_name}.\n"
            )
        job.save()
        return redirect("cwl_execution:job_list", project=kwargs["project"])


class RunningJobsView(HeliportProjectMixin, DetailView):  # noqa: D101
    model = Project
    pk_url_kwarg = "project"

    def get(self, request, *args, **kwargs):  # noqa: D102
        project = self.get_object()

        return JsonResponse(
            {
                "running": [
                    job.pk
                    for job in Job.objects.filter(
                        status=Job.StatusCodes.RUNNING, projects=project
                    )
                ]
            }
        )


class JobProfilingJsonView(HeliportProjectMixin, DetailView):  # noqa: D101
    model = Project
    pk_url_kwarg = "project"

    def get(self, request, *args, **kwargs):  # noqa: D102
        project = self.get_object()
        job = get_object_or_404(Job, pk=self.kwargs["pk"])
        assert project in job.projects.all()

        memory_usages = defaultdict(list)
        heliport_times = defaultdict(list)

        for log in ExecutionLog.objects.filter(config__job=job).order_by("start_time"):
            if log.memory_usage_bytes:
                for key, value in log.memory_usage_bytes.items():
                    memory_usages[key].append(
                        (
                            f"{log.start_time.year}-{log.start_time.month}-{log.start_time.day} ({log.pk})",  # noqa: E501
                            value / 1024**2,
                        )
                    )
            if log.profiling:
                for key, value in log.profiling.items():
                    heliport_times[key].append(
                        (
                            f"{log.start_time.year}-{log.start_time.month}-{log.start_time.day} ({log.pk})",  # noqa: E501
                            value,
                        )
                    )

        return JsonResponse(
            {
                "memory_usage": [
                    {
                        "x": [v[0] for v in value],
                        "y": [v[1] for v in value],
                        "name": key,
                    }
                    for key, value in memory_usages.items()
                ],
                "heliport_time": [
                    {
                        "x": [v[0] for v in value],
                        "y": [v[1] for v in value],
                        "name": key,
                    }
                    for key, value in heliport_times.items()
                ],
            }
        )


class PublicationWorkflowCreateView(HeliportProjectMixin, TemplateView):  # noqa: D101
    template_name = "cwl_execution/publication_workflow.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view)."""
        context = super().get_context_data(**kwargs)
        user = get_object_or_404(HeliportUser, auth_user=self.request.user)
        context["rodare_login_exists"] = LoginInfo.objects.filter(
            type=LoginInfo.LoginTypes.TOKEN, name__icontains="rodare", user=user
        ).exists()
        context["job_exists"] = Job.objects.filter(
            label="RODARE Publication",
            projects=context["project"],
            deleted__isnull=True,
        ).exists()
        return context

    def post(self, request, *args, **kwargs):  # noqa: D102
        vocab = Vocabulary()
        project = get_object_or_404(Project, pk=self.kwargs.get("project"))
        zip_output_dir = request.POST.get("zip_output_dir", "")
        not_a_test = bool(request.POST.get("not_a_test"))

        script_uri = "https://codebase.helmholtz.cloud/heliport/heliport-workflows/-/raw/main/scripts/rodare_publish.py"
        cwl_tool_uri = "https://codebase.helmholtz.cloud/heliport/heliport-workflows/-/raw/main/cwl/rodare_publish.yaml"

        script = DataSource.objects.filter(
            deleted__isnull=True, projects=project, uri=script_uri
        ).first()
        if script is None:
            script = DataSource(
                label="rodare_publish.py",
                description="A python script for creating RODARE publications "
                "and adding metadata as well as uploading data. "
                "Integration with HELIPORT is also supported.",
                uri=script_uri,
            )
            script.save_with_namespace(settings.DATA_SOURCE_NAMESPACE, project)

        logger.debug("downloading publication tool cwl")
        gitlab_response = requests.get(cwl_tool_uri)
        cwl_dict = yaml.safe_load(gitlab_response.text)
        cwl_json = json.dumps(cwl_dict)
        tool = Executable.objects.filter(
            deleted__isnull=True, projects=project, cwl_json=cwl_json
        ).first()
        if tool is None:
            tool = Executable(
                label="Create RODARE Publication",
                description="This tool allows to create a RODARE publication "
                "including metadata from HELIPORT and uploading data. "
                "The tool uses the rodare_publish.py script for RODARE interaction.",
                cwl_json=cwl_json,
                type=Executable.ExecutableTypes.TOOL,
            )
            tool.save_with_namespace(settings.EXECUTABLE_TOOL_NAMESPACE, project)

        job_params = {
            "not_a_test": not_a_test,
            "quiet": True,
            "wait": True,
            "project": str(project.pk),
            "rodare_publish_script": {
                "class": "File",
                "location": f"{script.pk}_{script.uri}",
            },
            "help": False,
            "closed_access": False,
        }

        if zip_output_dir:
            if not zip_output_dir.startswith("/"):
                zip_output_dir = f"/{zip_output_dir}"
            if not zip_output_dir.startswith("/bigdata/"):
                zip_output_dir = f"/bigdata{zip_output_dir}"
            job_params["zip_output_dir"] = zip_output_dir

        def get_input(label):
            relation = DigitalObjectRelation.objects.filter(
                subject=tool,
                predicate=vocab.has_input,
                object__label=label,
                object__deleted__isnull=True,
                object__projects=project,
            ).first()
            if relation and relation.object:
                result = relation.object
            else:
                result = DigitalObject(label=label)
                result.save_with_namespace("Execution", project)
                assert_relation(tool, vocab.has_input, result)
            return result

        def get_tag(label, color, input_obj):
            tag = Tag.objects.filter(
                label=label,
                attribute=vocab.described_by_parameter,
                value=input_obj,
                deleted__isnull=True,
                projects=project,
            ).first()
            if tag is None:
                tag = Tag(
                    label=label,
                    attribute=vocab.described_by_parameter,
                    value=input_obj,
                )
                tag.html_color = color
                tag.save_with_namespace(settings.TAG_NAMESPACE, project)
            return tag

        files_input = get_input("Publication Files Input")
        file_tag = get_tag("Publish", "#79dcd2", files_input)
        job_params["files"] = [{"class": "FromTags", "tags": str(file_tag.pk)}]

        directory_input = get_input("Publication Directories Input")
        directory_tag = get_tag("Publish Zipped", "#fce94f", directory_input)
        job_params["zipped"] = [{"class": "FromTags", "tags": str(directory_tag.pk)}]

        job = Job(
            label="RODARE Publication",
            description="This job allows to create a RODARE publication "
            "including metadata in this project and uploading data.",
            cwl_json=json.dumps(job_params),
            base_executable=tool,
            setup_command="ml python/3.9\n@HELIPORT load venv",
            cmd_options="--time=100",
            output_code=Job.OutputCodes.MESSAGE,
            output_text="Successfully created publication job",
        )
        job.save_with_namespace(settings.JOB_NAMESPACE, project)
        assert_relation(job, vocab.described_by_process, tool)

        return redirect("cwl_execution:job_list", project=project.pk)


class SearchView(HeliportLoginRequiredMixin, TemplateView):  # noqa: D101
    template_name = "cwl_execution/search.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the search results.
        """
        context = super().get_context_data(**kwargs)
        user = self.request.user.heliportuser
        query = self.request.GET.get("q", "")

        cwl_execution_results = set()
        job_results = set()

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            cwl_execution_results.update(
                Executable.objects.filter(
                    is_object_member(user)
                    & Q(projects__deleted__isnull=True, deleted__isnull=True)
                    & (Q(executable_id=num) | Q(cwl_json__icontains=word) | Q(type=num))
                )
            )
            job_results.update(
                Job.objects.filter(
                    is_object_member(user)
                    & Q(projects__deleted__isnull=True, deleted__isnull=True)
                    & (
                        Q(job_id=num)
                        | Q(label__icontains=word)
                        | Q(status=num)
                        | Q(description__icontains=word)
                        | Q(output_text__icontains=word)
                        | Q(output_code=num)
                        | Q(cwl_json__icontains=word)
                        | Q(interval=num, repeat=True)
                        | Q(base_executable__executable_id=num)
                        | Q(setup_command__icontains=word)
                    )
                )
            )

        context["job_results"] = job_results
        context["cwl_execution_results"] = cwl_execution_results
        return context


##########################################
#               REST API                 #
##########################################


class ExecutableViewSet(HeliportModelViewSet):
    """Tools, Workflows and Arbitrary CWL."""

    serializer_class = ExecutableSerializer
    filterset_fields = ["executable_id", "cwl_type", "type"]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return Executable.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()


class JobViewSet(HeliportModelViewSet):
    """
    Jobs.
    To execute a job go to its UserJobConfig like .../user_job_config/user_job_config id/execute/.
    """  # noqa: D205, E501

    serializer_class = JobSerializer
    filterset_fields = [
        "job_id",
        "label",
        "status",
        "output_code",
        "repeat",
        "interval",
        "base_executable",
        "setup_command",
    ]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return Job.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()


class UserJobConfigViewSet(viewsets.ModelViewSet):
    """User specific config for a job.

    You can Execute your config by appending /execute/ to the URL. For example:
    .../user_job_config/123/execute/.
    """  # noqa: D205

    serializer_class = UserJobConfigSerializer
    permission_classes = [rest_permissions.IsAuthenticated, HeliportObjectPermission]
    filterset_fields = ["user_job_config_id", "job", "ssh_login", "directory"]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return UserJobConfig.objects.filter(
            ssh_login__user=user, job__deleted__isnull=True
        ).distinct()

    @action(detail=True, methods=["get"])
    def execute(self, request, pk=None):  # noqa: D102
        config = self.get_object()
        start_job(config, request, debug=request.GET.get("debug", False))
        return RestResponse(f"job {config.job.pk} started")


class ExecutionLogPermission(rest_permissions.BasePermission):
    """Permission class to be used for execution logs. See :class:`ExecutionLogViewSet`."""  # noqa: E501

    def has_object_permission(self, request, view, obj: ExecutionLog):
        """
        Permission is granted if user
        :func:`heliport.core.permissions.has_permission_for` the job this log belongs to.
        """  # noqa: D205, E501
        if not hasattr(request.user, "heliportuser") or obj.config.job is None:
            return False
        return has_permission_for(request.user.heliportuser, obj.config.job)


class ExecutionLogViewSet(HeliportModelViewSet):
    """Concrete executions of a UserJobConfig."""

    permission_classes = [ExecutionLogPermission]
    serializer_class = ExecutionLogSerializer
    filterset_fields = [
        "execution_log_id",
        "config",
        "execution_id",
        "aborted",
        "output_code",
        "user",
    ]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return ExecutionLog.objects.filter(config__ssh_login__user=user).distinct()
