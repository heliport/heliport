# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains :class:`django.db.models.Model` classes for Django ORM.

See :ref:`django:topics/db/models:quick example` from Django documentation.
In HELIPORT the :class:`heliport.core.models.DigitalObject` can be subclassed for models
containing metadata in a project.
"""

import json

from django.db import models
from django.utils import timezone

from heliport.core.models import DigitalObject, HeliportUser, LoginInfo

from .cwl_helpers.cwl_parameter import CwlParameter


def json_load_dict(json_string, error_dict=None):  # noqa: D103
    if error_dict is None:
        error_dict = {}

    try:
        cwl = json.loads(json_string)
    except ValueError:
        cwl = error_dict
    except TypeError:
        cwl = error_dict

    if not isinstance(cwl, dict):
        cwl = error_dict

    return cwl


class Executable(DigitalObject):  # noqa: D101
    class ExecutableTypes(models.IntegerChoices):  # noqa: D106
        TOOL = 1
        WORKFLOW = 2
        ARBITRARY_CWL = 3

    class CWLTypes(models.IntegerChoices):  # noqa: D106
        YAML = 2
        JSON = 1

    executable_id = models.AutoField(primary_key=True)
    cwl_json = models.TextField(blank=True, default="")
    cwl_is_public = models.BooleanField(default=False)
    cwl_type = models.IntegerField(choices=CWLTypes.choices, default=CWLTypes.JSON)
    type = models.IntegerField(choices=ExecutableTypes.choices)
    download_url_is_public = models.BooleanField(default=False)

    original_id = models.CharField(max_length=100, null=True, blank=True)  # noqa: DJ001

    def uses_files(self):  # noqa: D102
        try:
            cwl = json_load_dict(self.cwl_json, {"inputs": {}})
            inputs = cwl["inputs"]
            if isinstance(inputs, dict):
                inputs = inputs.values()
            return any(param["type"] == "File" for param in inputs)
        except KeyError:
            return False

    @property
    def cwl(self):  # noqa: D102
        return json_load_dict(
            self.cwl_json, {"label": "SYNTAX ERROR", "doc": "SYNTAX ERROR IN JSON"}
        )

    @cwl.setter
    def cwl(self, cwl_dict):
        assert isinstance(cwl_dict, dict), "can only set dict as cwl attribute"
        self.cwl_json = json.dumps(cwl_dict, sort_keys=False)
        self.label = cwl_dict.get("label", "")
        self.description = cwl_dict.get("doc", "")

    @property
    def job_count(self):  # noqa: D102
        return len(self.job_set.filter(deleted__isnull=True))

    @property
    def parameters(self):  # noqa: D102
        parameters = self.cwl.get("inputs", [])
        if isinstance(parameters, dict):
            parameters = [{"id": key, **values} for key, values in parameters.items()]

        return [
            CwlParameter(
                p.get("id", "INVALID"), p.get("type", "no_type"), p.get("label", "")
            )
            for p in parameters
        ]


class Job(DigitalObject):  # noqa: D101
    class OutputCodes(models.IntegerChoices):  # noqa: D106
        UNDEFINED = -1
        MESSAGE = 0
        WARNING = 1
        ERROR = 2

    class StatusCodes(models.TextChoices):  # noqa: D106
        RUNNING = "Running"
        PAUSED = "Paused"
        STOPPED = "Stopped"

    job_id = models.AutoField(primary_key=True)
    status = models.CharField(
        max_length=80, choices=StatusCodes.choices, default=StatusCodes.STOPPED
    )
    output_text = models.TextField()
    output_code = models.IntegerField(
        choices=OutputCodes.choices, default=OutputCodes.UNDEFINED
    )
    cwl_json = models.TextField(blank=True, default="")
    repeat = models.BooleanField(default=False)
    interval = models.IntegerField(default=0)
    base_executable = models.ForeignKey(Executable, on_delete=models.CASCADE)
    setup_command = models.TextField(blank=True)
    cmd_options = models.TextField(blank=True, default="--time=10")

    original_id = models.CharField(max_length=100, null=True, blank=True)  # noqa: DJ001

    def __str__(self):  # noqa: D105
        return str(self.label)

    @property
    def cwl(self):  # noqa: D102
        return json_load_dict(self.cwl_json, {"SYNTAX ERROR": "SYNTAX ERROR IN JSON"})


class UserJobConfig(models.Model):  # noqa: D101
    user_job_config_id = models.AutoField(primary_key=True)
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    ssh_login = models.ForeignKey(LoginInfo, on_delete=models.SET_NULL, null=True)
    directory = models.TextField()

    original_id = models.CharField(max_length=100, null=True, blank=True)  # noqa: DJ001

    def __str__(self):  # noqa: D105
        return f"{self.job} config ({self.pk})"

    @property
    def projects(self):  # noqa: D102
        return self.job.projects


class ExecutionLog(models.Model):  # noqa: D101
    execution_log_id = models.AutoField(primary_key=True)
    config = models.ForeignKey(UserJobConfig, on_delete=models.CASCADE)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(null=True, blank=True)
    memory_usage = models.TextField(default="")
    execution_id = models.TextField(null=True, blank=True)  # noqa: DJ001
    aborted = models.BooleanField(default=False)
    step_times = models.TextField(null=True, blank=True)  # noqa: DJ001
    output_code = models.IntegerField(
        choices=Job.OutputCodes.choices, default=Job.OutputCodes.UNDEFINED
    )
    error_message = models.TextField(blank=True)
    user = models.ForeignKey(HeliportUser, null=True, on_delete=models.SET_NULL)

    original_id = models.CharField(max_length=100, null=True, blank=True)  # noqa: DJ001

    def __str__(self):  # noqa: D105
        return f"{str(self.config.job)} log ({self.pk})"

    def get_user(self):  # noqa: D102
        config = self.config
        login = config.ssh_login if config is not None else None
        user = login.user if login is not None else None
        return self.user or user

    def start(self, execution_id=None):  # noqa: D102
        self.start_time = timezone.now()
        self.execution_id = execution_id
        self.save()

    def stop(self, kill=False):  # noqa: D102
        self.end_time = timezone.now()
        self.aborted = kill
        self.save()

    @property
    def duration(self):  # noqa: D102
        if self.end_time is not None:
            return self.end_time - self.start_time
        return timezone.now() - self.start_time

    @property
    def seconds_since_start(self):  # noqa: D102
        return (timezone.now() - self.start_time).total_seconds()

    @property
    def duration_str(self):  # noqa: D102
        total_seconds = int(self.duration.total_seconds())

        days = total_seconds // 86400
        remaining_hours = total_seconds % 86400
        remaining_minutes = remaining_hours % 3600
        hours = remaining_hours // 3600
        minutes = remaining_minutes // 60
        seconds = remaining_minutes % 60

        days_str = f"{days}d " if days else ""
        hours_str = f"{hours}h " if hours else ""
        minutes_str = f"{minutes}m " if minutes else ""
        seconds_str = f"{seconds}s" if seconds and not hours_str else ""

        return f"{days_str}{hours_str}{minutes_str}{seconds_str}"

    @property
    def memory_usage_dict(self):  # noqa: D102
        return json_load_dict(self.memory_usage)

    @property
    def memory_usage_bytes(self):  # noqa: D102
        return {
            key: self.parse_memory_units(value)
            for key, value in self.memory_usage_dict.items()
        }

    @staticmethod
    def parse_memory_units(memory_with_unit):  # noqa: D102
        if memory_with_unit.isdecimal():
            return int(memory_with_unit)
        for unit, factor in {
            "B": 1,
            "KiB": 1024**1,
            "MiB": 1024**2,
            "GiB": 1024**3,
            "TiB": 1024**4,
            "PiB": 1024**5,
            "KB": 1000**1,
            "MB": 1000**2,
            "GB": 1000**3,
            "TB": 1000**4,
            "PB": 1000**5,
        }.items():
            memory_str = memory_with_unit.split(unit)[0]
            if memory_str.isdecimal():
                return int(memory_str) * factor
        return None

    @property
    def profiling(self):  # noqa: D102
        if self.step_times:
            return json.loads(self.step_times)
        return None
