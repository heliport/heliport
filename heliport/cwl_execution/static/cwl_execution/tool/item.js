/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class InputItem {
  /*
     Represent a single data filed (key-value pair) to the user in a simple div or a table row.
     */

  constructor(
    html_parent,
    is_table,
    serialization_key,
    label,
    input_filed,
    value = null,
  ) {
    this.serialization_key = serialization_key;
    this.table = is_table;
    this.label = label;
    this.input_field = input_filed;
    this.html_parent = html_parent;
    this.html = null;
    if (this.table)
      this.html = $(`<tr class="item-table-row"><th></th> <td></td></tr>`);
    else this.html = $(`<div><b style="margin-bottom:0.25em"></b></div>`);
    this.initHTML(value);
  }

  initHTML(value = null) {
    this.html.children().first().text(`${this.label}:`);
    let input_container = this.html;
    if (this.table) input_container = this.html.children("td");
    let input_filed = $(this.input_field).appendTo(input_container);
    if (value !== null) {
      input_filed.val(value);
    }
    this.html.appendTo(this.html_parent);
  }

  serialize() {
    return $(this.input_field).val();
  }
}
