/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class OutputObj extends ItemsObj {
  /*
     Represents an output parameter of a cwl tool and presents it to the user as item object.
     */

  constructor(html_parent, values = {}) {
    super(html_parent);
    this.initItems(values);
    this.initHTML();
  }

  initItems(values = {}) {
    let name_in = $(`<input class="form-control" required>`);
    let glob_in = $(
      `<input class="form-control" placeholder="*.txt" required>`,
    );
    this.addItem("id", "Name", name_in, values.id || null);
    this.addItem("glob", "File Pattern", glob_in, values.glob || null);
    this.addItem(
      "type",
      "Type",
      $(`<select class="form-select" required>
                <option value="File">Normal File</option>
                <option value="File_array">List of Files</option>
            </select>`),
      values.type || null,
    );
    this.addItem(
      "label",
      "Description",
      $(`<input class="form-control">`),
      values.label || null,
    );

    dynamic_default(glob_in, name_in, (s) =>
      s.startsWith("*.") ? s.substr(2) : s.split(".")[0],
    );
  }
}
