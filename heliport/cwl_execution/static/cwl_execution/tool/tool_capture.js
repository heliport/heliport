/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class CaptureObj extends ItemsObj {
  /*
     Represents the capturing of stdout and stderr of a cwl tool and presents it to the user as item object.
     */

  constructor(html_parent, values = {}) {
    super(html_parent, true);
    this.initItems(values);
    this.initHTML();
  }

  initItems(values = {}) {
    let stdout_in = $(
      '<input class="form-control" placeholder="stdout output parameter">',
    );
    let stdout_file_in = $(
      '<input class="form-control" placeholder="file to store stdout in">',
    );
    let stderr_in = $(
      '<input class="form-control" placeholder="stderr output parameter">',
    );
    let stderr_file_in = $(
      '<input class="form-control" placeholder="file to store stderr in">',
    );

    this.addItem(
      "stdout",
      "Name of Output Parameter",
      stdout_in,
      values.stdout || null,
    );
    this.addItem(
      "stdout_file",
      "Filename",
      stdout_file_in,
      values.stdout_file || null,
    );
    this.addItem(
      "stderr",
      "Name of Error Output Parameter",
      stderr_in,
      values.stderr || null,
    );
    this.addItem(
      "stderr_file",
      "Error Filename",
      stderr_file_in,
      values.stderr_file || null,
    );

    dynamic_default(stdout_in, stdout_file_in, (s) => s + ".txt");
    dynamic_default(stderr_in, stderr_file_in, (s) => s + ".txt");
  }
}
