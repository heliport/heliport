/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

function dynamic_default(source_box, target_box, translate_func = (i) => i) {
  /*
     Set the default value of the input field "target_box" to the translation ("translate_func")
     of the value of "source_box" while the user is Typing.
     */
  let old_value = source_box.val();
  source_box.on("input", () => {
    let still_default = translate_func(old_value) === target_box.val();
    let still_empty = target_box.val() === "";
    if (still_default || still_empty) {
      let target_val = translate_func(source_box.val());
      if (source_box.val() === "") target_val = "";
      target_box.val(target_val);
    }
    old_value = source_box.val();
  });
}
