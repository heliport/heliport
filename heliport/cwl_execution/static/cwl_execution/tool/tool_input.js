/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class InputObj extends ItemsObj {
  /*
     Represents an input parameter of a cwl tool and presents it to the user as item object.
     */

  constructor(html_parent, values = {}) {
    super(html_parent);
    this.initItems(values);
    this.initHTML();
  }

  initItems(values = {}) {
    let name_in = $(`<input class="form-control" required>`);
    let prefix_in = $(`<input class="form-control" placeholder="-xyz">`);
    this.addItem("id", "Name", name_in, values.id || null);
    this.addItem("prefix", "Prefix", prefix_in, values.prefix || null);
    this.addItem(
      "type",
      "Type",
      $(`<select class="form-select" required>
                <option value="string">String</option>
                <option value="boolean">Boolean</option>
                <option value="long">Integer</option>
                <option value="double">Floating Point Number</option>
                <option value="File">File</option>
                <option value="Directory">Directory</option>
                <option value="HELIPORTToken">HELIPORT API Token</option>
            </select>`),
      values.type || null,
    );
    this.addItem(
      "count",
      "Count",
      $(`<select class="form-select">
                <option value="">One (Required)</option>
                <option value="?">Zero or One (Optional)</option>
                <option value="[]">Multiple (Array)</option>
            </select>`),
      values.count || null,
    );
    this.addItem(
      "label",
      "Description",
      $(`<input class="form-control">`),
      values.label || null,
    );

    dynamic_default(prefix_in, name_in, (s) => s.replaceAll("-", " ").trim());
  }
}
