/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class Tool {
  /*
     Represents a CWL tool and generates HTML for displaying it
     */

  constructor(html_parent, values = {}) {
    if (typeof values === "string") values = JSON.parse(values);
    this.html_parent = html_parent;
    this.html = $(`
            <table class="table"><tbody id="main_info"></tbody></table><br>
            <h4>Input Parameters for Command</h4>
            <table class="table table-bordered"><tbody id="input_info"></tbody></table><br>
            <div style="padding-left: 0.75rem" id="input_add"></div><br><br>
            <h4>Output Parameters for Command</h4>
            <table class="table table-bordered"><tbody id="output_info"></tbody></table><br>
            <div style="padding-left: 0.75rem" id="output_add"></div><br><br>
            <h4>Capture Output of Command to Execute</h4>
            <table class="table"><tbody id="capture_info"></tbody></table><br>
        `);
    this.input_add_button = $(
      '<button class="btn btn-outline-primary" type="button">Add</button>',
    );
    this.output_add_button = $(
      '<button class="btn btn-outline-primary" type="button">Add</button>',
    );
    this.main_info = null;
    this.capture_info = null;
    this.input_infos = [];
    this.output_infos = [];
    this.initHTML(values);
  }

  initHTML(values = {}) {
    this.html.filter("#input_add").append(this.input_add_button);
    this.html.filter("#output_add").append(this.output_add_button);
    this.main_info = new MainObj(
      this.html.children("#main_info"),
      values.main || {},
    );
    this.capture_info = new CaptureObj(
      this.html.children("#capture_info"),
      values.capture || {},
    );

    for (const param of values.inputs || []) this.addInput(param);
    for (const param of values.outputs || []) this.addOutput(param);
    this.html.appendTo(this.html_parent);

    this.input_add_button.click(() => this.addInput());
    this.output_add_button.click(() => this.addOutput());
  }

  addInput(values = {}) {
    let new_input = new InputObj(this.html.children("#input_info"), values);
    this.input_infos.push(new_input);
    new_input.register_callbacks(
      (input) => this.removeInput(input),
      (input) => this.moveUp(input),
      (input) => this.moveDown(input),
    );
  }

  addOutput(values = {}) {
    let new_output = new OutputObj(this.html.children("#output_info"), values);
    this.output_infos.push(new_output);
    new_output.register_callbacks((output) => this.removeOutput(output));
  }

  removeInput(obj) {
    obj.html.remove();
    let idx = this.input_infos.indexOf(obj);
    if (idx >= 0) this.input_infos.splice(idx, 1);
    else console.log("ERROR removing not existing input", obj);
  }

  removeOutput(obj) {
    obj.html.remove();
    let idx = this.output_infos.indexOf(obj);
    if (idx >= 0) this.output_infos.splice(idx, 1);
    else console.log("ERROR removing not existing output", obj);
  }

  moveUp(obj) {
    let idx = this.input_infos.indexOf(obj);
    if (idx < 0) console.log("ERROR moving non existing input", obj);
    else if (idx > 0) {
      let obj = this.input_infos[idx];
      let before = this.input_infos[idx - 1];
      this.input_infos[idx - 1] = obj;
      this.input_infos[idx] = before;
      before.html.before(obj.html);
    }
  }

  moveDown(obj) {
    let idx = this.input_infos.indexOf(obj);
    if (idx < 0) console.log("ERROR moving non existing input", obj);
    else if (idx < this.input_infos.length - 1) {
      let obj = this.input_infos[idx];
      let after = this.input_infos[idx + 1];
      this.input_infos[idx + 1] = obj;
      this.input_infos[idx] = after;
      after.html.after(obj.html);
    }
  }

  serialize() {
    return {
      main: this.main_info.serialize(),
      capture: this.capture_info.serialize(),
      inputs: this.input_infos.map((param) => param.serialize()),
      outputs: this.output_infos.map((param) => param.serialize()),
    };
  }

  get json() {
    return JSON.stringify(this.serialize());
  }
}
