/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class ItemsObj {
  /*
     Abstract class for presenting an object (list of key-value pairs aka. items) as a css-grid or table to the user.
     */

  constructor(html_parent, is_table = false) {
    this.html_parent = html_parent;
    this.is_table = is_table;
    this.items = [];
    this.remove_callback = null;
    this.up_callback = null;
    this.down_callback = null;

    this.html = $(`<tr class="border-bottom">
            <td class="item-main-column"></td>
            <td class="item-control-column"></td>
        </tr>`);
    this.html_delete = $(
      `<button class="btn btn-danger" type="button">⨯</button>`,
    );
    this.html_up = $(
      `<div class="up-down-button"><i class="fa-solid fa-caret-up" title="Up"></i></div>`,
    );
    this.html_down = $(
      `<div class="up-down-button"><i class="fa-solid fa-caret-down" title="Down"></i></div>`,
    );
    this.html_items = $(`<div class="item-container"></div>`);
  }

  initHTML() {
    if (!this.is_table) {
      this.html.children(".item-main-column").append(this.html_items);
      this.html.appendTo(this.html_parent);
    }
  }

  register_callbacks(on_delete = null, on_up = null, on_down = null) {
    if (on_delete !== null) {
      this.remove_callback = on_delete;
      this.html.children(".item-control-column").append(this.html_delete);
      this.html_delete.click(() => this.remove_callback(this));
    }
    if (on_up !== null) {
      this.up_callback = on_up;
      this.html.children(".item-control-column").append(this.html_up);
      this.html_up.click(() => this.up_callback(this));
    }
    if (on_down !== null) {
      this.down_callback = on_down;
      this.html.children(".item-control-column").append(this.html_down);
      this.html_down.click(() => this.down_callback(this));
    }
  }

  serialize() {
    let result = {};
    for (const item of this.items) {
      result[item.serialization_key] = item.serialize();
    }
    return result;
  }

  addItem(serialization_key, label, input_field, value = null) {
    let item_container = this.is_table ? this.html_parent : this.html_items;
    this.items.push(
      new InputItem(
        item_container,
        this.is_table,
        serialization_key,
        label,
        input_field,
        value,
      ),
    );
  }
}
