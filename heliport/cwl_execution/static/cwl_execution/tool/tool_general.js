/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class MainObj extends ItemsObj {
  /*
     Represents general information of a cwl tool and presents it to the user as item object.
     */

  constructor(html_parent, values = {}) {
    super(html_parent, true);
    this.initItems(values);
    this.initHTML();
  }

  initItems(values = {}) {
    let cmd_in = $(`<input class="form-control" required>`);
    let name_in = $(`<input class="form-control">`);
    this.addItem(
      "baseCommand",
      "Command to Execute",
      cmd_in,
      values.baseCommand || null,
    );
    this.addItem("label", "Name", name_in, values.label || null);
    this.addItem(
      "doc",
      "Description",
      $(`<textarea class="form-control" rows="5"></textarea>`),
      values.doc || null,
    );

    dynamic_default(cmd_in, name_in, (s) => {
      let sections = s.split(" ");
      let result = sections[0];
      if (sections.length > 1) result += "_tool";
      return result;
    });
  }
}
