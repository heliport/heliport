/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class Edge {
  constructor(source_port, destination_port) {
    this.element_type = "edge";
    this.cleanup();

    this._start = null;
    this._end = null;
    this.update_start_callback = (port) => this.updateSVG();
    this.update_end_callback = (port) => this.updateSVG();
    this.start = source_port;
    this.end = destination_port;
  }

  initSVG(is_proto = false) {
    this.svg = document.createElementNS(ns, "g");
    this.svg.setAttribute("class", "edge_container");

    this.svg_border = document.createElementNS(ns, "path");
    this.svg_border.setAttribute("class", "edge edge_border");
    this.svg.appendChild(this.svg_border);

    this.svg_line = document.createElementNS(ns, "path");
    this.svg_line.setAttribute("class", "edge edge_line");
    this.svg.appendChild(this.svg_line);

    this.svg_label = document.createElementNS(ns, "text");
    this.svg_label.setAttribute("class", "label edge_label");
    this.updateLabelText();
    this.svg.appendChild(this.svg_label);

    this.updateSVG();

    $(this.svg)
      .on("mousemove", (event) => this.onMouseMove(event))
      .on("click", (event) => global_events.element_click.trigger(this, event));

    this.is_proto = is_proto;
    return this.svg;
  }

  updateSVG() {
    if (this.svg === null) return;

    this.svg_border.setAttribute("d", this.makePath());
    this.svg_line.setAttribute("d", this.makePath());

    // updating the position of the invisible label when edge is moved leads to more intuitive bounding box
    this.updateLabelPos(this.start.location);
  }

  updateLabelText() {
    if (this.start === null || this.end === null) return;
    $(this.svg_label).text(
      `${this.start.description} → ${this.end.description}`,
    );
  }

  updateLabelPos(label_pos) {
    this.svg_label.setAttribute("x", label_pos.x);
    this.svg_label.setAttribute("y", label_pos.y);
  }

  makePath() {
    let p1 = this.start.location;
    let p2 = this.end.location;
    let x_dist = Math.abs(p1.x - p2.x);
    let out_dir = p1.x + x_dist / 2;
    let in_dir = p2.x - x_dist / 2;

    return `M ${p1.x} ${p1.y} C ${out_dir} ${p1.y} ${in_dir} ${p2.y} ${p2.x} ${p2.y}`;
  }

  cleanup() {
    if ("svg" in this && this.svg) this.svg.remove();
    this.svg = null;
    this.svg_line = null;
    this.svg_border = null;
    this.svg_label = null;

    global_events.move.remove(this.update_start_callback, this.start);
    global_events.move.remove(this.update_end_callback, this.end);
  }

  serialize() {
    return {
      start: this.start.id,
      end: this.end.id,
    };
  }

  get is_proto() {
    return this._is_proto;
  }
  set is_proto(value) {
    this._is_proto = value;
    if (this.is_proto) $(this.svg_line).addClass("edge_ghost");
    else $(this.svg_line).removeClass("edge_ghost");
  }

  set selected(selected) {
    if (selected === null) {
      $(this.svg).removeClass("edge_not_selected");
      $(this.svg).removeClass("edge_selected");
    } else if (selected) {
      $(this.svg).removeClass("edge_not_selected");
      $(this.svg).addClass("edge_selected");
    } else {
      $(this.svg).addClass("edge_not_selected");
      $(this.svg).removeClass("edge_selected");
    }
  }

  get start() {
    return this._start;
  }
  set start(value) {
    global_events.move.remove(this.update_start_callback, this.start);
    this._start = value;
    this.updateLabelText();
    this.updateSVG();
    global_events.move.register(this.update_start_callback, this.start);
  }

  get end() {
    return this._end;
  }
  set end(value) {
    global_events.move.remove(this.update_end_callback, this.end);
    this._end = value;
    this.updateLabelText();
    this.updateSVG();
    global_events.move.register(this.update_end_callback, this.end);
  }

  onMouseMove(event) {
    let label_location = global_points.createFromWindowPoint(
      event.clientX,
      event.clientY - config.edge_label_offset,
    );
    this.updateLabelPos(label_location);
  }
}
