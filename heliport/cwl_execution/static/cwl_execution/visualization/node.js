/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class GraphNode {
  constructor(
    label,
    type,
    inputs = [],
    outputs = [],
    x = 0,
    y = 0,
    user_data = null,
  ) {
    this.element_type = "node";
    this.cleanup();
    this._is_proto = false;

    this.label = label;
    this.type = type;
    this.inputs = inputs;
    this.outputs = outputs;
    this._location = global_points.create(x, y);
    this.user_data = user_data;
  }

  initSVG(is_proto = false, location = null) {
    if (!location) location = this._location;

    this.svg = document.createElementNS(ns, "g");
    this.svg.setAttribute("class", `node node_${this.type}`);
    this.svg.setAttribute("style", "position: absolute;");
    this.location = location;

    this.svg_outer = document.createElementNS(ns, "circle");
    this.svg_outer.setAttribute("class", "node_outer");
    this.svg_outer.setAttribute("r", this.radius);
    this.svg.appendChild(this.svg_outer);

    this.svg_inner = document.createElementNS(ns, "circle");
    this.svg_inner.setAttribute("class", "node_inner");
    this.svg_inner.setAttribute("r", this.radius * 0.75);
    this.svg.appendChild(this.svg_inner);

    this.svg_ghost = document.createElementNS(ns, "circle");
    this.svg_ghost.setAttribute("class", "node_ghost");
    this.svg_ghost.setAttribute("r", this.radius * 0.6);
    this.svg.appendChild(this.svg_ghost);

    this.svg_label = document.createElementNS(ns, "text");
    this.svg_label.setAttribute(
      "transform",
      `translate(0 ${this.radius + config.node_label_offset})`,
    );
    this.svg_label.setAttribute("class", "label node_label");
    $(this.svg_label).text(this.label);
    this.svg.appendChild(this.svg_label);

    if (this.type in global_graph_node_icons)
      $(global_graph_node_icons[this.type]).appendTo(this.svg);

    this.initPorts(this.inputs, true);
    this.initPorts(this.outputs, false);

    $(this.svg)
      .draggable()
      .on("click", (event) => global_events.element_click.trigger(this, event))
      .on("dragstop", (event) => {
        if (this.is_proto) global_events.edge_connect_ends.trigger(this, event);
      })
      .on("drag", (event, ui) => this.onDrag(event, ui));

    this.is_proto = is_proto;
    return this.svg;
  }

  initPorts(port_list, is_input) {
    for (let i = 0; i < port_list.length; i++) {
      let port = port_list[i];
      port.is_input = is_input;
      if (port.description !== this.label)
        port.description = `${this.label} (${port.description})`;
      this.svg.appendChild(port.initSVG(port_list.length, i, this.radius));
    }
  }

  cleanup() {
    if ("svg" in this && this.svg) this.svg.remove();
    this.svg = null;
    this.svg_outer = null;
    this.svg_inner = null;
    this.svg_label = null;
    this.svg_ghost = null;
  }

  removePortSnapping() {
    this.ports.forEach((port) => (port.snapping = false));
  }

  removePortSuggest() {
    this.ports.forEach((port) => (port.suggested = false));
  }

  serialize() {
    return {
      label: this.label,
      type: this.type,
      inputs: this.inputs.map((port) => port.serialize()),
      outputs: this.outputs.map((port) => port.serialize()),
      x: this.location.x,
      y: this.location.y,
      user_data: this.user_data,
    };
  }

  get location() {
    return this._location;
  }
  set location(value) {
    this._location = value;
    if (this.svg)
      this.svg.setAttribute(
        "transform",
        `translate(${this.location.x} ${this.location.y})`,
      );
  }

  get radius() {
    let max_ports = Math.max(this.inputs.length, this.outputs.length);
    return config.node_radius + max_ports * config.port_radius;
  }

  get is_proto() {
    return this._is_proto;
  }
  set is_proto(value) {
    this._is_proto = value;
    if (this.is_proto) $(this.svg).addClass("proto");
    else $(this.svg).removeClass("proto");
  }

  get ports() {
    return this.inputs.concat(this.outputs);
  }

  set selected(selected) {
    if (selected === null) {
      $(this.svg).removeClass("node_not_selected");
      $(this.svg).removeClass("node_selected");
    } else if (selected) {
      $(this.svg).removeClass("node_not_selected");
      $(this.svg).addClass("node_selected");
    } else {
      $(this.svg).addClass("node_not_selected");
      $(this.svg).removeClass("node_selected");
    }
  }

  set snapping(snap) {
    if (snap) $(this.svg_ghost).addClass("ghost_snapping");
    else $(this.svg_ghost).removeClass("ghost_snapping");
  }

  onDrag(event, ui) {
    let corner_location = global_points.createFromWindowPoint(
      ui.offset.left - window.pageXOffset,
      ui.offset.top - window.pageYOffset,
    );
    let center_to_corner = global_points.create(
      this.svg.getBBox().x,
      this.svg.getBBox().y,
    );
    const empirical_correction = global_points.create(2, 2);
    this.location = corner_location
      .sub(center_to_corner)
      .add(empirical_correction);

    global_events.move.trigger(this);
    this.inputs.forEach((port) => global_events.move.trigger(port));
    this.outputs.forEach((port) => global_events.move.trigger(port));
  }
}
