/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class EventManager {
  constructor() {
    this.callbacks = new Map();
  }

  register(callback, sender = null) {
    if (!this.callbacks.has(sender)) this.callbacks.set(sender, new Set());
    this.callbacks.get(sender).add(callback);
  }

  remove(callback, sender = null) {
    if (this.callbacks.has(sender)) {
      this.callbacks.get(sender).delete(callback);
      if (this.callbacks.get(sender).size === 0) this.callbacks.delete(sender);
    }
  }

  trigger(sender = null, params = null) {
    if (sender !== null)
      this.forEach(sender, (callback) => callback(sender, params));
    this.forEach(null, (callback) => callback(sender, params));
  }

  forEach(sender, func) {
    if (this.callbacks.has(sender)) this.callbacks.get(sender).forEach(func);
  }
}

class EventHub {
  constructor() {
    this.move = new EventManager();
    this.edge_connect_ends = new EventManager();
    this.edge_connect_starts = new EventManager();
    this.element_click = new EventManager();
    this.canvas_click = new EventManager();
  }
}

let global_events = new EventHub();
