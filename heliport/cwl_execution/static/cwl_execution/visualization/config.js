/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let ns = "http://www.w3.org/2000/svg";
let config = {
  node_radius: 30,
  node_label_offset: 30,
  port_radius: 7,
  edge_label_offset: 16,
  pan_threshold: 20,
  pan_speed: 0.4,
  zoom_factor: 1.05,
  connection_abort_radius: 120,
  connection_snap_radius: 120,
};
