/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class Canvas {
  constructor(width, height) {
    this.svg = null;

    this.do_auto_pan = true;
    this.translation = global_points.create();
    this.scale = 1;
    this.translation_start = null;
    this.last_drag_time = null;
    this.mouse_possition = null;

    this.width = width;
    this.height = height;
  }

  initSVG() {
    this.svg = document.createElementNS(ns, "svg");
    this.svg.setAttribute("class", "workflow");
    this.setSize(this.width, this.height);

    $(this.svg)
      .draggable()
      .on("dragstart", (event, ui) => this.onDragStart(event, ui))
      .on("drag", (event, ui) => this.onDrag(event, ui))
      .on("dragstop", (event, ui) => this.onDragStop(event, ui))
      .on("wheel", (event) => this.onMouseWheel(event))
      .on("click", (event) => {
        if (event.target === this.svg)
          global_events.canvas_click.trigger(this, event);
      });

    return this.svg;
  }

  updateViewBox() {
    let x = this.translation.x;
    let y = this.translation.y;
    let w = this.width * this.scale;
    let h = this.height * this.scale;

    this.svg.setAttribute("viewBox", `${x} ${y} ${w} ${h}`);
  }

  setSize(width, height) {
    this.width = width;
    this.height = height;
    this.svg.setAttribute("width", this.width);
    this.svg.setAttribute("height", this.height);
    this.updateViewBox();
  }

  fitToViewBox() {
    let rect = this.svg.getBBox();
    if (rect.width === 0 || rect.height === 0) {
      this.x = 0;
      this.y = 0;
      this.scale = 1;
    } else {
      let scaleX = rect.width / this.width;
      let scaleY = rect.height / this.height;
      this.scale = Math.max(scaleX, scaleY, 1);

      let free_spaceX = this.width * (this.scale - scaleX);
      let free_spaceY = this.height * (this.scale - scaleY);
      this.translation = global_points.create(
        rect.x - free_spaceX * 0.5,
        rect.y - free_spaceY * 0.5,
      );
    }

    this.updateViewBox();
  }

  toFront(svg, of_class = null) {
    if (of_class === null) $(this.svg).append(svg);
    else $(this.svg).children(`.${of_class}`).last().append(svg);
  }

  toBack(svg) {
    $(this.svg).prepend(svg);
  }

  serialize() {
    return {
      x: this.translation.x,
      y: this.translation.y,
      scale: this.scale,
    };
  }

  fromObj(obj) {
    if ("x" in obj) this.translation.x = obj.x;
    if ("y" in obj) this.translation.y = obj.y;
    if ("scale" in obj) this.scale = obj.scale;
    this.updateViewBox();
  }

  onDragStart(event, ui) {
    if (event.target === this.svg) {
      this.translation_start = this.translation;
    } else if (this.do_auto_pan) {
      this.last_drag_time = window.performance.now();
      window.requestAnimationFrame((timestamp) =>
        this.onPanAnimation(timestamp),
      );
    }
  }

  onDragStop(event, ui) {
    this.translation_start = null;
    this.last_drag_time = null;
    this.mouse_possition = null;
  }

  onDrag(event, ui) {
    if (this.last_drag_time === null && this.translation_start === null)
      this.onDragStart(event, ui);

    if (event.target === this.svg) {
      let position = global_points.create(ui.position.left, ui.position.top);
      let original_position = global_points.create(
        ui.originalPosition.left,
        ui.originalPosition.top,
      );
      let delta = position.sub(original_position);
      this.translation = this.translation_start.sub(delta.scale(this.scale));

      this.svg.setAttribute("style", "");
      this.updateViewBox();
    } else this.supplyMousePositionForPanAnimation(event);
  }

  supplyMousePositionForPanAnimation(event) {
    let rect = this.svg.getBoundingClientRect();
    this.mouse_possition = global_points.create(
      event.clientX - rect.x,
      event.clientY - rect.y,
    );
  }

  onPanAnimation(timestamp) {
    if (this.last_drag_time === null) return;
    if (this.mouse_possition === null) return;

    let delta = timestamp - this.last_drag_time;
    this.last_drag_time = timestamp;

    let pan = global_points.create();
    if (this.mouse_possition.x < config.pan_threshold) pan.x -= 1;
    if (this.mouse_possition.y < config.pan_threshold) pan.y -= 1;
    if (this.mouse_possition.x > this.width + config.pan_threshold) pan.x += 1;
    if (this.mouse_possition.y > this.height + config.pan_threshold) pan.y += 1;

    if (pan.x !== 0 || pan.y !== 0) {
      pan = pan.scale(delta * this.scale * config.pan_speed);
      this.translation = this.translation.add(pan);
      this.updateViewBox();
    }

    window.requestAnimationFrame((timestamp) => this.onPanAnimation(timestamp));
  }

  onMouseWheel(event) {
    let rect = this.svg.getBoundingClientRect();
    let svg_on_screen = global_points.create(rect.x, rect.y);
    let point_on_screen = global_points.create(event.clientX, event.clientY);
    let mouse_on_svg = point_on_screen.sub(svg_on_screen);

    this.translation = this.translation.add(mouse_on_svg.scale(this.scale));

    let delta = Math.sign(event.originalEvent.deltaY);
    let factor = Math.pow(config.zoom_factor, delta);
    this.scale *= factor;

    this.translation = this.translation.sub(mouse_on_svg.scale(this.scale));

    this.updateViewBox();
    return false;
  }
}
