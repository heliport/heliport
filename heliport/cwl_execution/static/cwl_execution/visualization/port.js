/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let global_port_count_for_id_generation = 0; //static variables are not supported in safari

class Port {
  constructor(label, type, is_input = false) {
    this.element_type = "port";
    this.svg = null;
    this.svg_label = null;
    this.svg_port = null;

    this.label = label;
    this.description = label;
    this.type = type;
    this.is_input = is_input;
    this.id = global_port_count_for_id_generation++;
  }

  initSVG(total_port_count, port_index, offset) {
    let angle = this.calcAngle(total_port_count, port_index);

    this.svg = document.createElementNS(ns, "g");
    this.svg.setAttribute(
      "transform",
      `rotate(${-angle}) translate(${offset}) rotate(${angle})`,
    );

    this.svg_port = document.createElementNS(ns, "circle");
    this.svg_port.setAttribute("r", config.port_radius);
    this.svg_port.setAttribute("class", "port");
    $(this.svg_port).on("mousedown", (event) => {
      global_events.edge_connect_starts.trigger(this, event);
      return false;
    });
    this.svg.appendChild(this.svg_port);

    this.svg_label = document.createElementNS(ns, "text");
    let c = this.is_input ? "input_label" : "output_label";
    this.svg_label.setAttribute("class", `label port_label ${c}`);
    $(this.svg_label).text(this.label);
    this.svg.appendChild(this.svg_label);

    return this.svg;
  }

  calcAngle(total, index) {
    let total_angle = 140;
    let port_angle = total_angle / total;
    let port_center = port_angle / 2;

    let start_angle = -total_angle / 2;
    if (this.is_input) start_angle += 180;

    if (!this.is_input) index = total - 1 - index; //ports always top to bottom
    let taken_space = index * port_angle;

    return start_angle + taken_space + port_center;
  }

  serialize() {
    return {
      label: this.label,
      type: this.type,
      id: this.id,
    };
  }

  get location() {
    return global_points.createFromSVGOrigin(this.svg);
  }

  set suggested(suggested) {
    if (suggested) $(this.svg).addClass("port_suggest");
    else $(this.svg).removeClass("port_suggest");
  }

  set snapping(snap) {
    if (snap) $(this.svg).addClass("port_snapping");
    else $(this.svg).removeClass("port_snapping");
  }
}
