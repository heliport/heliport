/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class Workflow {
  constructor(svg_root, width = 1000, height = 500) {
    this.svg_root = svg_root;
    this.canvas = new Canvas(width, height);
    this.nodes = new Set();
    this.edges = new Set();
    this.selection = null;
    this.dependencies = new Map();

    this.svg_root.appendChild(this.canvas.initSVG());
    global_events.element_click.register((node, event) =>
      this.onElementClick(node, event),
    );
    global_events.canvas_click.register((canvas, event) =>
      this.onCanvasClick(canvas, event),
    );
    global_events.edge_connect_starts.register((port, event) =>
      this.onEdgeConnectStarts(port, event),
    );
    global_events.edge_connect_ends.register((node, event) =>
      this.onEdgeConnectEnds(node, event),
    );
    global_events.move.register((element, event) =>
      this.onMove(element, event),
    );
  }

  updateSelection() {
    if (this.selection === null) {
      this.nodes.forEach((node) => (node.selected = null));
      this.edges.forEach((edge) => (edge.selected = null));
    } else {
      this.nodes.forEach((node) => (node.selected = false));
      this.edges.forEach((edge) => (edge.selected = false));
      this.selection.selected = true;
    }
  }

  createPort(data, is_input, port_index = null) {
    let new_port = new Port(
      "label" in data ? data.label : "ERROR",
      "type" in data ? data.type : "ERROR",
      is_input,
    );

    let index_id = "id" in data ? data.id : new_port.id;
    if (port_index !== null) port_index.set(index_id, new_port);
    return new_port;
  }

  createNode(data, collect_port_index = null) {
    let inputs = [];
    let outputs = [];

    if ("inputs" in data)
      for (const input of data.inputs) {
        inputs.push(this.createPort(input, true, collect_port_index));
      }

    if ("outputs" in data)
      for (const output of data.outputs) {
        outputs.push(this.createPort(output, false, collect_port_index));
      }

    let new_node = new GraphNode(
      "label" in data ? data.label : "ERROR",
      "type" in data ? data.type : "ERROR",
      inputs,
      outputs,
      "x" in data ? data.x : 0,
      "y" in data ? data.y : 0,
      data,
    );
    this.nodes.add(new_node);
    this.canvas.toFront(new_node.initSVG());

    this.updateDependencies();
  }

  createNodeFromJSON(json) {
    this.createNode(JSON.parse(json));
  }

  createEdge(data, port_index) {
    if (!("start" in data) || !("end" in data)) {
      console.log("edge has no start and end", data);
      return;
    }
    if (!port_index.has(data.start) || !port_index.has(data.end)) {
      console.log("edge between non existing ports");
      return;
    }

    let new_edge = new Edge(
      port_index.get(data.start),
      port_index.get(data.end),
    );
    this.canvas.toBack(new_edge.initSVG());
    this.edges.add(new_edge);
  }

  deleteSelection() {
    if (this.selection === null) return false;

    if (this.selection.element_type === "node") {
      let ports = new Set(this.selection.ports);
      for (const edge of [...this.edges].filter(
        (edge) => ports.has(edge.start) || ports.has(edge.end),
      )) {
        this.edges.delete(edge);
        edge.cleanup();
      }
      this.nodes.delete(this.selection);
    } else {
      this.edges.delete(this.selection);
    }

    this.selection.cleanup();
    this.selection = null;
    this.updateSelection();
    this.updateDependencies();
    return true;
  }

  fitToViewBox() {
    this.canvas.fitToViewBox();
  }

  serialize() {
    return {
      canvas: this.canvas.serialize(),
      nodes: [...this.nodes].map((node) => node.serialize()),
      edges: [...this.edges].map((edge) => edge.serialize()),
    };
  }

  get json() {
    return JSON.stringify(this.serialize());
  }
  set json(value) {
    let obj = JSON.parse(value);
    this.clearElements();
    this.createElementsFromData(obj);
  }

  createElementsFromData(data) {
    // collect mapping from id to port during node creation and lookup ports during edge creation
    let collected_port_index = new Map();

    if ("canvas" in data) this.canvas.fromObj(data.canvas);
    if ("nodes" in data)
      for (const node of data.nodes)
        this.createNode(node, collected_port_index);
    if ("edges" in data)
      for (const edge of data.edges)
        this.createEdge(edge, collected_port_index);
  }

  clearElements() {
    for (const node of this.nodes) {
      this.nodes.delete(node);
      node.cleanup();
    }
    for (const edge of this.edges) {
      this.edges.delete(edge);
      edge.cleanup();
    }
  }

  setSize(width, height) {
    this.canvas.setSize(width, height);
  }

  onElementClick(element, event) {
    if (element.element_type === "node") this.canvas.toFront(element.svg);
    else this.canvas.toFront(element.svg, "edge");

    this.selection = element;
    this.updateSelection();
  }

  onCanvasClick(canvas, event) {
    this.selection = null;
    this.updateSelection();
  }

  onEdgeConnectStarts(port, event) {
    let new_port = new Port(
      port.label,
      this.typeAliasResolution(port.type),
      !port.is_input,
    );

    let inputs = !port.is_input ? [new_port] : [];
    let outputs = port.is_input ? [new_port] : [];
    let type = port.is_input ? "input" : "output";
    if (this.typeAliasResolution(port.type) === "File") type = `file_${type}`;
    let new_node = new GraphNode(port.label, type, inputs, outputs);

    let location = global_points.createFromWindowPoint(
      event.clientX,
      event.clientY,
    );
    this.canvas.toFront(new_node.initSVG(true, location));
    this.nodes.add(new_node);

    let new_edge = port.is_input
      ? new Edge(new_node, port)
      : new Edge(port, new_node);
    this.canvas.toBack(new_edge.initSVG(true));
    this.edges.add(new_edge);

    this.updateDependencies();
    this.findMatchingPorts(new_port).forEach((port) => (port.suggested = true));

    event.type = "mousedown.draggable";
    event.target = new_node.svg;
    $(new_node.svg).trigger(event);
  }

  onEdgeConnectEnds(node, event) {
    let edge = [...this.edges].find((e) => e.start === node || e.end === node);
    let is_start = edge.start === node;
    let other_port = is_start ? edge.end : edge.start;

    if (
      node.location.distanceTo(other_port.location) <
      config.connection_abort_radius
    ) {
      this.nodes.delete(node);
      this.edges.delete(edge);
      edge.cleanup();
      node.cleanup();
      this.nodes.forEach((node) => node.removePortSnapping());
      this.nodes.forEach((node) => node.removePortSuggest());
      this.updateDependencies();
      return;
    }

    let snap_port = this.findMatchingSnapPort(node);
    if (snap_port !== null) {
      if (is_start) edge.start = snap_port;
      else edge.end = snap_port;
      this.nodes.delete(node);
      node.cleanup();
    } else {
      let node_port = node.ports[0];
      if (is_start) edge.start = node_port;
      else edge.end = node_port;
      node.is_proto = false;
    }

    edge.is_proto = false;
    this.nodes.forEach((node) => node.removePortSnapping());
    this.nodes.forEach((node) => node.removePortSuggest());
    this.updateDependencies();
  }

  onMove(element, event) {
    if (element.element_type !== "node" || !element.is_proto) return;

    let edge = [...this.edges].find(
      (e) => e.start === element || e.end === element,
    );
    let is_start = edge.start === element;
    let other_port = is_start ? edge.end : edge.start;
    let aborting =
      element.location.distanceTo(other_port.location) <
      config.connection_abort_radius;

    let snap_port = this.findMatchingSnapPort(element);

    for (const node of this.nodes) node.removePortSnapping();
    if (snap_port !== null && !aborting) snap_port.snapping = true;
    element.snapping = snap_port !== null || aborting;
  }

  findMatchingSnapPort(node) {
    let port = node.ports[0];
    let closest_port = null;
    let closest_distance = config.connection_snap_radius;
    for (const p of this.findMatchingPorts(port)) {
      let node_location = node.location;
      if (p.svg.getScreenCTM() === null) console.log(p);
      let distance = node_location.distanceTo(p.location);
      if (distance < closest_distance && p !== port) {
        closest_port = p;
        closest_distance = distance;
      }
    }
    return closest_port;
  }

  findMatchingPorts(port) {
    let dependencies = this.dependencies;
    let alias = this.typeAliasResolution;
    return this.ports.filter(function (p) {
      let input_matches = p.is_input === port.is_input;
      let type_matches = alias(p.type) === alias(port.type);
      let not_too_connected = !p.is_input || dependencies.get(p).size === 1;
      let port_not_depending_on_p = ![...dependencies.get(port)].find(
        (dependency) => dependency === p,
      );
      let p_not_depending_on_port = ![...dependencies.get(p)].find(
        (dependency) => dependency === port,
      );

      let not_depending = port_not_depending_on_p && p_not_depending_on_port;
      return (
        input_matches && type_matches && not_too_connected && not_depending
      );
    });
  }

  typeAliasResolution(type) {
    if (type === "stdout") return "File";
    if (type === "stderr") return "File";
    if (
      typeof type === "object" &&
      "type" in type &&
      type.type === "array" &&
      "items" in type
    )
      return `${type.items}[]`;
    return type;
  }

  get ports() {
    let result = [];
    for (const node of this.nodes) result = result.concat(node.ports);
    return result;
  }

  get svg() {
    return this.canvas.svg;
  }

  updateDependencies() {
    let dependencies = new Map(); // all e -> f, where e depends on f
    let constraints = new Map(); // all e -> f, where f depends directly on e
    let updates = new Set(); // elements with unpropagated dependencies

    for (const node of this.nodes) {
      dependencies.set(node, new Set([node]));
      for (const port of node.ports) dependencies.set(port, new Set([port]));

      for (const port of node.inputs) constraints.set(port, new Set([node]));
      for (const port of node.outputs) constraints.set(port, new Set());
      constraints.set(node, new Set(node.outputs));
    }
    for (const edge of this.edges) {
      let start = edge.start;
      let end = edge.end;
      if (start.element_type === "node" && start.is_proto)
        start = start.ports[0];
      if (end.element_type === "node" && end.is_proto) end = end.ports[0];
      constraints.get(start).add(end);
    }
    for (const v of constraints.keys()) {
      updates.add(v);
    }

    function extend_set(set, extension) {
      let changed = false;
      for (const element of extension) {
        if (set.has(element)) continue;
        changed = true;
        set.add(element);
      }

      return changed;
    }

    function pop_set(set) {
      let element = set.values().next().value;
      set.delete(element);
      return element;
    }

    while (updates.size > 0) {
      let this_element = pop_set(updates);
      let directly_dependent_elements = constraints.get(this_element);
      let this_dependencies = dependencies.get(this_element);

      for (const other_element of directly_dependent_elements) {
        let other_dependencies = dependencies.get(other_element);
        if (extend_set(other_dependencies, this_dependencies))
          updates.add(other_element);
      }
    }

    this.dependencies = dependencies;
  }
}
