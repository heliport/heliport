/*
 * SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class Vector {
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  add(other_vector) {
    return new Vector(this.x + other_vector.x, this.y + other_vector.y);
  }

  sub(other_vector) {
    return new Vector(this.x - other_vector.x, this.y - other_vector.y);
  }

  scale(scalar) {
    return new Vector(this.x * scalar, this.y * scalar);
  }

  abs() {
    return Math.sqrt(this.x * this.x + this.y * this.y);
  }

  distanceTo(other_vector) {
    return this.sub(other_vector).abs();
  }
}

class PointManager {
  constructor() {
    this.svg = null;
  }

  registerSVG(points_are_relative_to_this_svg) {
    if (
      !("createSVGPoint" in points_are_relative_to_this_svg) ||
      !("getScreenCTM" in points_are_relative_to_this_svg)
    )
      throw "registerSVG called not with a SVG";

    this.svg = points_are_relative_to_this_svg;
  }

  create(x = 0, y = 0) {
    return new Vector(x, y);
  }

  createFromWindowPoint(x, y) {
    if (this.svg === null)
      throw "Please call global_points.registerSVG(<your svg>) first.";

    let p = this.svg.createSVGPoint();
    p.x = x;
    p.y = y;
    p = p.matrixTransform(this.svg.getScreenCTM().inverse());
    return new Vector(p.x, p.y);
  }

  createFromSVGOrigin(svg_element) {
    let window_transform = svg_element.getScreenCTM();
    if (window_transform === null) console.log(svg_element);
    return this.createFromWindowPoint(window_transform.e, window_transform.f);
  }
}

let global_points = new PointManager();
