# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Django app configuration.

Some HELIPORT hooks can be registered in app config :meth:`django.apps.AppConfig.ready`.

See also :ref:`Django documentation <django:ref/applications:applications>`
"""

from django.apps import AppConfig


class CwlExecutionConfig(AppConfig):
    """App configuration for cwl execution app."""

    name = "heliport.cwl_execution"

    def ready(self):
        """Import settings."""
        from .conf import CwlExecutionAppConf

        assert CwlExecutionAppConf
