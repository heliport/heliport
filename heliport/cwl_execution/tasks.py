# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from celery import shared_task

from .cwl_helpers.cwl_execution import CWLExecutorManager
from .models import ExecutionLog, Job, UserJobConfig

logger = logging.getLogger(__name__)


@shared_task
def job_start(job_config_id, debug=False):  # noqa: D103
    job_config = UserJobConfig.objects.get(pk=job_config_id)
    executor = CWLExecutorManager(
        job_config, debug=debug, auto_run=False, execution_id=job_start.request.id
    )
    try:
        status = executor.run()
    except Exception as e:
        job = job_config.job
        job.output_text += "\nINTERNAL SERVER ERROR!\n"
        job.status = job.StatusCodes.STOPPED
        job.save()
        raise e
    logger.debug(
        f"finished {job_config.job} with id={job_config.job.pk} status={status}"
    )
    return status


@shared_task
def run_periodic_jobs():  # noqa: D103
    logger.info("=== RUN PERIODIC JOBS ===")
    periodic_jobs = Job.objects.filter(repeat=True, projects__deleted__isnull=True)
    to_do = []
    for job in periodic_jobs:
        last_execution = (
            ExecutionLog.objects.filter(config__job=job).order_by("-start_time").first()
        )
        seconds_per_day = 24 * 60 * 60
        seconds_threshold = 20 * 60
        wait_seconds = job.interval * seconds_per_day - seconds_threshold
        if (
            last_execution is None
            or last_execution.end_time is not None
            and last_execution.seconds_since_start > wait_seconds
        ):
            for config in job.userjobconfig_set.all():
                if config.ssh_login is not None and config.ssh_login.connected:
                    to_do.append(config)
                    break

    for config in to_do:
        job_start.delay(config.pk)

    return len(to_do)
