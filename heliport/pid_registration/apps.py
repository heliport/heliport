# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Django app configuration module for the ``heliport.pid_registration`` app."""

import logging

from django.apps import AppConfig
from django.db.models.signals import post_save

logger = logging.getLogger("heliport.pid_registration")


class PidRegistrationConfig(AppConfig):
    """Django app config for the ``heliport.pid_registration`` app."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "heliport.pid_registration"

    def ready(self):
        """Register hooks and import settings."""
        from heliport.core.models import DigitalObject
        from heliport.pid_registration.conf import HeliportPidRegistrationAppConf
        from heliport.pid_registration.signal_handlers import register_pid_handler

        assert HeliportPidRegistrationAppConf

        # NOTE: This only handles immediate subclasses!
        post_save.connect(register_pid_handler, DigitalObject)
        for subclass in DigitalObject.__subclasses__():
            post_save.connect(register_pid_handler, subclass)
