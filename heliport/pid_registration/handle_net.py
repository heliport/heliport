# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""PID registration interface for handle.net servers."""

import logging
from typing import Optional

import requests
from django.conf import settings

from heliport.core.models import DigitalObject, DigitalObjectIdentifier
from heliport.core.utils.http import heliport_user_agent

logger = logging.getLogger(__name__)


class HandleServerSession(requests.Session):
    """Custom ``requests.Session`` subclass for handle server interaction."""

    def __init__(self) -> None:
        """Initialize the session with auth settings."""
        super().__init__()
        self.headers.update(
            {
                "User-Agent": heliport_user_agent,
                "Authorization": 'Handle clientCert="true"',
            }
        )
        self.cert = (
            settings.HELIPORT_HANDLE_CERTIFICATE_PATH,
            settings.HELIPORT_HANDLE_PRIVATE_KEY_PATH,
        )

    def request(self, method, url, **kwargs) -> requests.Response:
        """Handle request, preventing sending the certificate to the wrong server."""
        server_url = settings.HELIPORT_HANDLE_SERVER_URL
        if not url.startswith(server_url):
            raise ValueError(
                f"Requests to other domains than {server_url} are not allowed: {url}"
            )
        return super().request(method, url, **kwargs)


class HandleNetRegistry:
    """Class for PID registration on Handle.Net servers."""

    #: A unique name that can be used to refer to this registry and its identifiers
    #: throughout HELIPORT. In accordance with `N2T <https://n2t.net/_schemes.html>`_
    #: and `RFC 3651 <https://doi.org/10.17487/RFC3651>`_ which use `hdl`/`hdl:`, we use
    #: `hdl` as the registry name.
    registry_scheme = "hdl"

    def __init__(self):
        """Initialize the registry."""
        self.session = HandleServerSession()
        self.api_url = settings.HELIPORT_HANDLE_API_URL
        self.prefix = settings.HELIPORT_HANDLE_PREFIX

    def mint_identifier(self, name: str, target: str) -> str:
        """Mint an identifier in the Handle registry.

        Try to obtain an ID with the name ``name`` and make it point to ``target``.
        Return the ID that was actually minted, including the prefix.
        """
        request_data = {
            "index": 1,
            "type": "URL",
            "data": {"format": "string", "value": target},
        }

        response = self.session.put(
            f"{self.api_url}/{self.prefix}/{name}",
            json=request_data,
            params={"overwrite": "false"},
        )

        if response.status_code == 409:
            response = self.session.put(
                f"{self.api_url}/{self.prefix}",
                json=request_data,
                params={"mintNewSuffix": "true"},
            )

        logger.info(f"created handle {response.status_code} {response.json()}")
        return response.json().get("handle")

    def full_url(self, identifier: str) -> str:
        """Return the full URL for ``identifier``.

        ``identifier`` is assumed to be valid and containing the server prefix. The URL
        is simply constructed and returned with no request to the server being sent.
        """
        return f"https://hdl.handle.net/{identifier}"


def register_handle(digital_object: DigitalObject) -> Optional[str]:
    """Mint a new handle for the given digital object.

    The newly minted handle is returned as a full URL, or ``None`` if anything goes
    wrong. If the digital object already has a handle, this handle is returned and no
    new handle is minted. If the digital object has no primary key (i.e. it was not
    saved in the database), or no category, ``None`` is returned.

    The handle is also stored in :class:`DigitalObjectIdentifier` using the ``hdl``
    scheme.
    """
    if digital_object.persistent_id:
        # object already has a PID
        return digital_object.persistent_id

    if (
        not digital_object.pk  # object was not saved
        or not digital_object.category  # object has no category
    ):
        return None

    registry = HandleNetRegistry()
    pid = registry.mint_identifier(
        digital_object.suffix_for_pid_generation, digital_object.full_url()
    )

    if pid is not None:
        pid_url = registry.full_url(pid)

        DigitalObjectIdentifier.objects.create(
            scheme=registry.registry_scheme,
            identifier=pid,
            display_text=pid,
            url=pid_url,
            digital_object=digital_object,
        )

        digital_object.persistent_id = pid_url
        digital_object.generated_persistent_id = pid_url
        digital_object.save()

        logger.info(
            f"Minted handle {pid} for digital object {digital_object.digital_object_id}"
        )

    return registry.full_url(pid) if pid is not None else None
