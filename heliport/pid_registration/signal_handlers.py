# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Signal handlers for the app."""

from django.conf import settings

from heliport.core.models import DigitalObject
from heliport.pid_registration.handle_net import register_handle


def register_pid_handler(
    sender, instance: DigitalObject, created: bool, using, update_fields, **kwargs
):
    """Post-save handler for digital object PID registration.

    If the saved object was newly created, does not yet have any persistent identifier,
    and handle registration is enabled, a handle is registered for the digital object.

    Currently, only an implementation for Handle.Net PIDs exists. If more
    implementations are added to the code base, this function may change in order to
    perform registration with other (and potentially multiple) PID providers.
    """
    if (
        not settings.HELIPORT_HANDLE_REGISTER_HANDLES
        or not created
        or not instance.pk
        or not instance.category
        or instance.persistent_id
    ):
        return

    # The only implementation we currently have.
    _ = register_handle(instance)
