# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Tests for the ``heliport.pid_registration`` app."""

import requests_mock
from django.test import TestCase, override_settings

from heliport.pid_registration.handle_net import HandleNetRegistry, HandleServerSession


@override_settings(
    HELIPORT_HANDLE_SERVER_URL="https://hdl.example.com",
    HELIPORT_HANDLE_API_URL="https://hdl.example.com/api/handles",
    HELIPORT_HANDLE_PREFIX="20.500.99999",
    HELIPORT_HANDLE_CERTIFICATE_PATH="/does/not/exist",
    HELIPORT_HANDLE_PRIVATE_KEY_PATH="/does/not/exist/either",
)
class HandleNetTest(TestCase):
    """Tests for ``handle_net`` module."""

    def test_handle_server_session(self):
        """Disallow request to wrong server."""
        session = HandleServerSession()
        with self.assertRaises(ValueError):
            session.get("https://not-hdl.example.com")

    def test_mint_identifier(self):
        """Registration of a new handle."""
        registry = HandleNetRegistry()

        with requests_mock.Mocker() as mock:
            mock.put(
                "https://hdl.example.com/api/handles/20.500.99999/Example.1234?overwrite=false",
                status_code=200,
                json={"responseCode": 1, "handle": "20.500.99999/Example.1234"},
            )
            handle_pid = registry.mint_identifier(
                "Example.1234", "https://heliport.example.com/object/Example.1234"
            )
            handle_url = registry.full_url(handle_pid)

            request = mock.request_history[0]
            self.assertEqual(handle_pid, "20.500.99999/Example.1234")
            self.assertEqual(
                handle_url, "https://hdl.handle.net/20.500.99999/Example.1234"
            )
            self.assertEqual(mock.call_count, 1)
            self.assertEqual(
                request.headers.get("Authorization"), 'Handle clientCert="true"'
            )
            self.assertIn("heliport", request.headers.get("User-Agent").casefold())
            self.assertEqual(
                request.json(),
                {
                    "index": 1,
                    "type": "URL",
                    "data": {
                        "format": "string",
                        "value": "https://heliport.example.com/object/Example.1234",
                    },
                },
            )

    def test_register_existing_handle(self):
        """Trying to register a handle that already exists."""
        registry = HandleNetRegistry()

        with requests_mock.Mocker() as mock:
            mock.put(
                "https://hdl.example.com/api/handles/20.500.99999/Example.1234?overwrite=false",
                status_code=409,
            )
            mock.put(
                "https://hdl.example.com/api/handles/20.500.99999?mintNewSuffix=true",
                status_code=200,
                json={"responseCode": 1, "handle": "20.500.99999/random-uuid"},
            )
            handle_pid = registry.mint_identifier(
                "Example.1234", "https://heliport.example.com/object/Example.1234"
            )
            handle_url = registry.full_url(handle_pid)

            self.assertEqual(handle_pid, "20.500.99999/random-uuid")
            self.assertEqual(
                handle_url, "https://hdl.handle.net/20.500.99999/random-uuid"
            )
            self.assertEqual(mock.call_count, 2)
