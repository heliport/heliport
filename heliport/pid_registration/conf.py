# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""App settings for the ``heliport.pid_registration`` app."""

from appconf import AppConf

from heliport.core.conf import env


class HeliportPidRegistrationAppConf(AppConf):
    """Settings of the :mod:`heliport.pid_registration` app.

    All settings in this class can be overwritten in settings.py
    """

    #: Enable Handle registration
    REGISTER_HANDLES = env.bool("HELIPORT_HANDLE_REGISTER_HANDLES", default=False)

    #: The URL to your local Handle.Net server.
    SERVER_URL = env.str("HELIPORT_HANDLE_SERVER_URL", default="https://handle.hzdr.de")
    #: The API URL of your local Handle.Net server.
    API_URL = f"{SERVER_URL}/api/handles"
    #: The prefix used by your local Handle.Net server.
    PREFIX = env.str("HELIPORT_HANDLE_PREFIX", default="20.500.12865")

    #: The path to the certificate file for authentication.
    CERTIFICATE_PATH = env.str(
        "HELIPORT_HANDLE_CERTIFICATE_PATH",
        default="/etc/ssl/certs/heliport_handle_admin.crt",
    )
    #: The path to the private key file for authentication.
    PRIVATE_KEY_PATH = env.str(
        "HELIPORT_HANDLE_PRIVATE_KEY_PATH",
        default="/etc/ssl/private/heliport_handle_admin.key",
    )

    class Meta:  # noqa: D106
        prefix = "HELIPORT_HANDLE"
