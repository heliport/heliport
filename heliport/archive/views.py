# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

import django_filters.rest_framework
from django.conf import settings
from django.db.models import Q
from django.views.generic import TemplateView
from rest_framework import filters as rest_filters

from heliport.core import permissions
from heliport.core.mixins import HeliportLoginRequiredMixin
from heliport.core.views import HeliportModelViewSet, HeliportObjectListView

from .models import Archive
from .serializers import ArchiveSerializer


class ArchiveView(HeliportObjectListView):  # noqa: D101
    model = Archive
    category = settings.ARCHIVE_NAMESPACE
    columns = [
        ("Archive Number", "archive_number", "small"),
        ("Name", "label", "normal"),
        ("Description", "description", "large"),
    ]
    actions = [
        ("Open", "action_open", "link"),
        ("Edit", "action_edit", "link_secondary"),
        ("Delete", "action_delete", "danger"),
    ]
    edit_fields = [
        ("Name", "label", "normal"),
        ("Archive Number", "archive_number", "normal"),
        ("Link", "link", "normal"),
        ("Description", "description", "large"),
    ]
    after_save_attributes = {"link", "archive_number"}
    list_url = "archive:list"
    update_url = "archive:update"
    list_heading = "Archives"
    create_heading = "Add an Archive"
    add_text = [
        "or",
        {
            "text": "create",
            "link": "https://www.hzdr.de/db/!FzrTools.Data_Archive.List",
        },
        "one first",
    ]

    def action_open(self, obj):  # noqa: D102
        if obj.link:
            return obj.link
        return None


class SearchView(HeliportLoginRequiredMixin, TemplateView):  # noqa: D101
    template_name = "archive/search.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the search results.
        """
        context = super().get_context_data(**kwargs)
        user = self.request.user.heliportuser
        query = self.request.GET.get("q", "")

        archive_results = set()

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            archive_results.update(
                Archive.objects.filter(
                    permissions.is_object_member(user)
                    & Q(deleted__isnull=True)
                    & (
                        Q(archive_id=num)
                        | Q(description__icontains=word)
                        | Q(label__icontains=word)
                    )
                )
            )

        context["archive_results"] = archive_results
        return context


##########################################
#               REST API                 #
##########################################


class ArchiveViewSet(HeliportModelViewSet):
    """Archive."""

    serializer_class = ArchiveSerializer
    filterset_fields = ["archive_id", "persistent_id"]
    search_fields = ["label", "description", "owner__display_name", "attributes__value"]
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return Archive.objects.filter(
            permissions.is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()
