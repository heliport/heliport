# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from heliport.core.models import HeliportUser, Project
from heliport.core.serializers import get_all_attributes

from .models import Archive


class ArchiveTests(TestCase):
    """Test archive app."""

    def setUp(self):
        """Set up projects and logged in users."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.client.force_login(self.auth_user)
        self.project = Project.objects.create(owner=self.user)
        self.project2 = Project.objects.create(owner=self.user)

    def test_list(self):
        """Test listing archives."""
        a = Archive(description="xxxabc")
        a.save()
        a.link = "www.myarchive.de"
        a.projects.add(self.project)
        b = Archive(description="xyzaaa")
        b.save()
        b.projects.add(self.project2)
        c = Archive(description="xxxccc")
        c.save()
        c.projects.add(self.project)
        response = self.client.get(
            reverse("archive:list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(200, response.status_code)
        obj = response.context_data["project"].parts.filter(archive__pk=a.pk).first()
        self.assertEqual(a, obj.archive)
        self.assertContains(response, "xxxabc")
        self.assertContains(response, "xxxccc")
        self.assertNotContains(response, "xyzaaa")
        self.assertContains(response, "https://www.myarchive.de")

    def test_create(self):
        """Test creating archive."""
        self.client.post(
            reverse("archive:list", kwargs={"project": self.project.pk}),
            data=urlencode({"form_data_field_description": "test_archive321"}),
            content_type="application/x-www-form-urlencoded",
        )
        self.assertIsNotNone(
            self.project.parts.filter(description="test_archive321").first()
        )

    def test_url(self):
        """Test url normalization of archive."""
        a = Archive()
        a.save()
        a.link = "www.google.de"
        self.assertEqual("https://www.google.de", a.link)


class SearchTest(TestCase):
    """Test search functionality."""

    def setUp(self):
        """Set up project and logged-in heliport user."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.project, is_new = Project.objects.get_or_create(owner=self.user)
        self.client.force_login(self.auth_user)

    def test_access_search(self):
        """Test search url is accessible."""
        response = self.client.get(reverse("archive:search"))
        self.assertEqual(200, response.status_code)

    def test_archive_findable(self):
        """Test search results."""
        a = Archive(description="ARCHIVE321")
        a.save()
        a.projects.add(self.project)
        response = self.client.get(f"{reverse('archive:search')}?q=321")
        self.assertContains(response, "ARCHIVE321")


class ProjectSerializeTest(TestCase):
    """Test metadata serialization."""

    def setUp(self):
        """Set up projects, instances and logged-in users."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.project, is_new = Project.objects.get_or_create(owner=self.user)
        self.client.force_login(self.auth_user)
        self.archive_description = "ARCHIVE TO SERIALIZE"
        self.archive = Archive(
            description=self.archive_description,
            persistent_id="https://hdl.handle.net/aaa",
        )
        self.archive.save()
        self.archive.archive_number = "123"
        self.archive.link = "https://www.archive.de"
        self.archive.projects.add(self.project)
        for attr in get_all_attributes(self.archive):
            attr.set_is_public(self.archive, True)

    @staticmethod
    def landing_page_url(obj):
        """Construct landing page url for an object."""
        return reverse("core:landing_page", kwargs={"pk": obj.digital_object_id})

    def test_json_ld(self):
        """Test JSON LD generation."""
        response = self.client.get(
            f"{self.landing_page_url(self.project)}?format=jsonld"
        )
        self.assertContains(response, self.archive_description)
        response = self.client.get(
            f"{self.landing_page_url(self.archive)}?format=jsonld"
        )
        archive = response.json()
        self.assertIn("@id", archive)
        self.assertEqual(self.archive_description, archive["dcterms:description"])
        self.assertEqual("123", archive["datacite:hasIdentifier"])
        self.assertContains(response, "https://www.archive.de")
        self.assertContains(response, "https://hdl.handle.net/aaa")

    def test_datacite(self):
        """Test datacite generation."""
        response = self.client.get(
            f"{self.landing_page_url(self.archive)}?format=datacite_json"
        )
        metadata = response.json()
        self.assertEqual(
            [
                {
                    "description": self.archive_description,
                    "descriptionType": "Abstract",
                    "lang": "en",
                }
            ],
            metadata["descriptions"],
        )
        self.assertEqual(
            {"resourceType": "archive", "resourceTypeGeneral": "Dataset"},
            metadata["types"],
        )

    def test_rest_api(self):
        """Test REST API including serialization."""
        response = self.client.get("/api/archives/")
        archive = response.json()["results"][0]
        self.assertEqual(
            {
                "archive_id": 1,
                "label": "",
                "description": self.archive_description,
                "persistent_id": "https://hdl.handle.net/aaa",
                "url": "https://www.archive.de",
                "archive_number": "123",
            },
            archive,
        )

        archive_number = "abc123"
        response = self.client.post(
            "/api/archives/",
            data={
                "label": "My Archive",
                "url": "https://example.com/archives/",
                "archive_number": archive_number,
            },
        )
        self.assertEqual(201, response.status_code)
        self.assertEqual(Archive.objects.last().archive_number, archive_number)

    def test_not_public(self):
        """Test to not see private information on landing page."""
        self.client.logout()
        response = self.client.get(
            reverse("core:landing_page", kwargs={"pk": self.archive.digital_object_id})
        )
        self.assertContains(response, self.archive_description)

        self.client.force_login(self.auth_user)
        for attr in get_all_attributes(self.archive):
            if attr.value_from(self.archive) == self.archive_description:
                attr.set_is_public(self.archive, False)

        self.client.logout()
        response = self.client.get(
            reverse("core:landing_page", kwargs={"pk": self.archive.digital_object_id})
        )
        self.assertNotContains(response, self.archive_description)
