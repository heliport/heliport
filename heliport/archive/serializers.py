# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""This module is for serialization into Datacite, RDF and JSON.

The JSON format is used for API endpoints via
`Django rest framework <https://www.django-rest-framework.org/>`_. Using this, it is
typical to put the serializer classes into a "serializers.py" file.

For the serialization into RDF, attributes are described.
See :class:`heliport.core.attribute_description.BaseAttribute` for more detail.
"""

from django.conf import settings
from rdflib import Namespace
from rest_framework import serializers

from heliport.core.attribute_description import TypeAttribute
from heliport.core.serializers import (
    DigitalObjectDATACITESerializer,
    NamespaceField,
    datacite_serializers,
    register_digital_object_attributes,
)
from heliport.core.vocabulary import FABIO

from .models import Archive

hzdr_namespace = Namespace("https://hdl.handle.net/20.500.12865/hzdr_vocab.v1#")


class ArchiveSerializer(serializers.ModelSerializer):  # noqa: D101
    archive_number = serializers.CharField()
    url = serializers.CharField(source="link")
    namespace_str = NamespaceField(settings.ARCHIVE_NAMESPACE)

    def create(self, validated_data):  # noqa: D102
        link = validated_data.pop("link")
        n = validated_data.pop("archive_number")
        instance = super().create(validated_data)
        instance.link = link
        instance.archive_number = n
        return instance

    class Meta:  # noqa: D106
        model = Archive
        fields = [
            "archive_id",
            "label",
            "description",
            "persistent_id",
            "archive_number",
            "url",
            "namespace_str",
        ]


@datacite_serializers.register(Archive)
class ArchiveDATACITESerializer(DigitalObjectDATACITESerializer):  # noqa: D101
    def resource_type(self, archive):  # noqa: D102
        return "archive", "Dataset"


@register_digital_object_attributes(Archive)
def archive_attributes():  # noqa: D103
    return [
        TypeAttribute(FABIO.ArchivalDocument),
    ]
