# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path
from rest_framework import routers

from .views import ArchiveView, ArchiveViewSet, SearchView

app_name = "archive"
urlpatterns = [
    path("project/<int:project>/archive/list/", ArchiveView.as_view(), name="list"),
    path(
        "project/<int:project>/archive/<int:pk>/update/",
        ArchiveView.as_view(),
        name="update",
    ),
    path("archive/search/", SearchView.as_view(), name="search"),
]

# REST API
router = routers.DefaultRouter()
router.register(r"archives", ArchiveViewSet, basename="archives")
