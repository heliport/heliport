# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

import django_filters
from django.conf import settings
from django.db.models import Q
from django.views.generic import TemplateView
from rest_framework import filters as rest_filters

from heliport.core.mixins import HeliportLoginRequiredMixin
from heliport.core.permissions import is_object_member
from heliport.core.views import HeliportModelViewSet, HeliportObjectListView

from .models import ShareLaTeX
from .serializers import LatexSerializer


class LaTeXView(HeliportObjectListView):  # noqa: D101
    model = ShareLaTeX
    category = settings.SHARE_LATEX_NAMESPACE
    columns = [("ID", "latex_id", "small"), ("Name", "label", "large")]
    actions = [
        ("Open", "action_open", "link"),
        ("Edit", "action_edit", "link_secondary"),
        ("Delete", "action_delete", "danger"),
    ]
    edit_fields = [("Link", "link", "normal"), ("Name", "label", "normal")]
    after_save_attributes = {"link"}
    list_url = "sharelatex:list"
    update_url = "sharelatex:update"
    list_heading = "ShareLaTeX"
    create_heading = "Add a ShareLaTex Project"
    add_text = [
        "or create one first with",
        {
            "text": "ColLabTeX",
            "link": "https://collabtex.helmholtz.cloud",
        },
    ]

    def action_open(self, obj):  # noqa: D102
        if obj.link:
            return obj.link
        return None


class SearchView(HeliportLoginRequiredMixin, TemplateView):  # noqa: D101
    template_name = "sharelatex/search.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the search results.
        """
        context = super().get_context_data(**kwargs)
        user = self.request.user.heliportuser
        query = self.request.GET.get("q", "")

        latex_results = set()

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            latex_results.update(
                ShareLaTeX.objects.filter(
                    is_object_member(user)
                    & Q(projects__deleted__isnull=True, deleted__isnull=True)
                    & (
                        Q(latex_id=num)
                        | Q(label__icontains=word)
                        | Q(attributes__value__icontains=word)
                    )
                )
            )

        context["latex_results"] = latex_results
        return context


##########################################
#               REST API                 #
##########################################


class LatexViewSet(HeliportModelViewSet):
    """ShareLaTeX."""

    serializer_class = LatexSerializer
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    filterset_fields = ["latex_id", "label"]
    search_fields = ["attributes__value"]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return ShareLaTeX.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()
