# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path
from rest_framework import routers

from .views import LaTeXView, LatexViewSet, SearchView

app_name = "sharelatex"
urlpatterns = [
    path("project/<int:project>/sharelatex/list/", LaTeXView.as_view(), name="list"),
    path(
        "project/<int:project>/sharelatex/<int:pk>/update/",
        LaTeXView.as_view(),
        name="update",
    ),
    path("sharelatex/search/", SearchView.as_view(), name="search"),
]

# REST API
router = routers.DefaultRouter()
router.register(r"sharelatex", LatexViewSet, basename="sharelatex")
