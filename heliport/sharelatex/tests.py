# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from heliport.core.models import HeliportUser, Project

from .models import ShareLaTeX


class LatexTest(TestCase):  # noqa: D101
    def setUp(self):
        """Set up project, instances and logged-in heliport user."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.project, is_new = Project.objects.get_or_create(owner=self.user)
        self.client.force_login(self.auth_user)
        self.lat = ShareLaTeX(persistent_id="https://hdl.handle.net/lat")
        self.lat.save()
        self.lat.projects.add(self.project)
        self.lat2 = ShareLaTeX()
        self.lat2.save()
        self.lat2.link = "www.lat.de"
        self.lat2.projects.add(self.project)

    def test_list(self):  # noqa: D102
        response = self.client.get(
            reverse("sharelatex:list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'href="https://www.lat.de"')

    def test_create(self):  # noqa: D102
        response = self.client.post(
            reverse("sharelatex:list", kwargs={"project": self.project.pk}),
            data=urlencode(
                {
                    "form_data_field_label": "lat3_label",
                    "form_data_field_link": "www.lat3.de",
                }
            ),
            content_type="application/x-www-form-urlencoded",
            follow=True,
        )
        self.assertEqual(response.status_code, 200)
        lat3 = ShareLaTeX.objects.filter(label="lat3_label").first()
        self.assertIsNotNone(lat3)
        self.assertEqual("https://www.lat3.de", lat3.link)
        self.assertIn(self.project, lat3.projects.all())
        self.assertContains(response, 'href="https://www.lat3.de"')
        self.assertContains(response, "lat3_label")

    def test_access_search(self):  # noqa: D102
        response = self.client.get(reverse("sharelatex:search"))
        self.assertEqual(200, response.status_code)

    def test_lat_findable(self):  # noqa: D102
        response = self.client.get(f"{reverse('sharelatex:search')}?q=lat%2ede")
        self.assertContains(response, self.landing_page_url(self.lat2))

    def test_api(self):  # noqa: D102
        lats = self.client.get("/api/sharelatex/").json()["results"]
        self.assertIn(
            {
                "latex_id": self.lat2.pk,
                "label": "",
                "projects": [self.project.pk],
                "url": "https://www.lat.de",
            },
            lats,
        )
        self.assertIn(
            {
                "latex_id": self.lat.pk,
                "label": "",
                "projects": [self.project.pk],
                "url": None,
            },
            lats,
        )

        link = "https://example.com/sharelatex/"
        response = self.client.post(
            "/api/sharelatex/",
            data={"label": "My sharelatex", "url": link},
        )
        self.assertEqual(201, response.status_code)
        self.assertEqual(ShareLaTeX.objects.last().link, link)

    @staticmethod
    def landing_page_url(obj):  # noqa: D102
        return reverse("core:landing_page", kwargs={"pk": obj.digital_object_id})

    def test_json_ld(self):  # noqa: D102
        response = self.client.get(
            f"{self.landing_page_url(self.project)}?format=jsonld"
        )
        self.assertContains(response, "https://www.lat.de")
        tex = self.client.get(
            f"{self.landing_page_url(self.lat2)}?format=jsonld"
        ).json()
        self.assertIn("@id", tex)
        self.assertContains(response, "https://www.lat.de")

    def test_datacite(self):  # noqa: D102
        response = self.client.get(
            f"{self.landing_page_url(self.lat)}?format=datacite_json"
        )
        metadata = response.json()
        self.assertIn("descriptions", metadata)
        self.assertEqual(
            {"resourceType": "latex", "resourceTypeGeneral": "Text"},
            metadata["types"],
        )
