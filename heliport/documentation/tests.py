# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from heliport.core.models import HeliportUser, Project

from .models import Documentation


class DocumentationTests(TestCase):  # noqa: D101
    def setUp(self):
        """Set up project and logged-in heliport user."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.client.force_login(self.auth_user)
        self.project = Project.objects.create(owner=self.user)

    def test_list(self):  # noqa: D102
        doc, is_new = Documentation.objects.get_or_create(system=1)
        doc.projects.add(self.project)

        response = self.client.get(
            reverse("documentation:list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(200, response.status_code)
        test_obj = Documentation.objects.get(pk=doc.pk)
        self.assertIn(response.context_data["project"], test_obj.projects.all())
        self.assertEqual(test_obj, doc)

        response = self.client.get(
            reverse(
                "documentation:update",
                kwargs={"project": self.project.pk, "pk": doc.pk},
            )
        )
        self.assertEqual(200, response.status_code)
        test_obj = Documentation.objects.get(pk=doc.pk)
        self.assertIn(response.context_data["project"], test_obj.projects.all())
        self.assertEqual(test_obj, doc)

    def test_create(self):  # noqa: D102
        self.client.post(
            reverse("documentation:list", kwargs={"project": self.project.pk}),
            data={
                "description": "docu321",
                "link": "https://www.google.com",
                "system": 1,
            },
        )
        test_obj = Documentation.objects.filter(description="docu321").first()
        self.assertIsNotNone(test_obj)
        self.assertIn(self.project, test_obj.projects.all())
        self.assertEqual("https://www.google.com", test_obj.link)

    def test_update(self):  # noqa: D102
        doc, is_new = Documentation.objects.get_or_create(
            description="docu456", system=1
        )
        doc.link = "http://www.google.com"
        doc.projects.add(self.project)
        response = self.client.post(
            reverse(
                "documentation:update",
                kwargs={"project": self.project.pk, "pk": doc.pk},
            ),
            data={
                "description": "docu789",
                "link": "www.bing.com",
                "system": 2,
            },
            follow=True,
        )
        self.assertEqual(200, response.status_code)

        doc = Documentation.objects.get(pk=doc.pk)
        self.assertIn(response.context_data["project"], doc.projects.all())
        self.assertEqual("docu789", doc.description)
        self.assertEqual("https://www.bing.com", doc.link)
        self.assertEqual(2, doc.system)

    def test_delete(self):  # noqa: D102
        doc = Documentation.objects.create(system=2)
        doc2 = Documentation.objects.create(system=2)
        doc.projects.add(self.project)
        doc2.projects.add(self.project)
        response = self.client.post(
            reverse("documentation:list", kwargs={"project": self.project.pk}),
            data={"remove": doc2.pk},
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        docu_set = Documentation.objects.filter(
            projects=self.project, deleted__isnull=True
        )
        self.assertIsNotNone(docu_set.filter(pk=doc.pk).first())
        self.assertIsNone(docu_set.filter(pk=doc2.pk).first())

    def test_add_protocol_create(self):  # noqa: D102
        self.client.post(
            reverse("documentation:list", kwargs={"project": self.project.pk}),
            data={"description": "docu321", "link": "www.google.com", "system": 1},
        )
        test_obj = Documentation.objects.filter(description="docu321").first()
        self.assertIsNotNone(test_obj)
        self.assertIn(self.project, test_obj.projects.all())
        self.assertEqual("https://www.google.com", test_obj.link)

    def test_add_protocol_update(self):  # noqa: D102
        doc, is_new = Documentation.objects.get_or_create(
            description="docu456", system=1
        )
        doc.link = "www.google.com"
        doc.projects.add(self.project)
        response = self.client.post(
            reverse(
                "documentation:update",
                kwargs={"project": self.project.pk, "pk": doc.pk},
            ),
            data={"description": "docu789", "link": "www.bing.com", "system": 2},
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        doc = Documentation.objects.get(pk=doc.pk)
        self.assertIn(response.context_data["project"], doc.projects.all())
        self.assertEqual("https://www.bing.com", doc.link)


class SearchAndAPITest(TestCase):  # noqa: D101
    def setUp(self):
        """Set up project and logged-in heliport user."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.project, is_new = Project.objects.get_or_create(owner=self.user)
        self.client.force_login(self.auth_user)

    def test_access_search(self):
        """Test that search URL is accessible."""
        response = self.client.get(reverse("documentation:search"))
        self.assertEqual(200, response.status_code)

    def test_documentation_findable(self):
        """Test that documentation is findable."""
        doc = Documentation.objects.create(description="DOCUMENTATION321", system=1)
        doc.projects.add(self.project)
        response = self.client.get(f"{reverse('documentation:search')}?q=321")
        self.assertContains(response, "DOCUMENTATION321")

    def test_api(self):
        """Test api."""
        doc = Documentation.objects.create(description="DOCU654", system=2)
        doc.projects.add(self.project)
        response = self.client.get("/api/documentations/")
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "DOCU654")

        link = "https://example.com/documentation/"
        response = self.client.post(
            "/api/documentations/",
            data={"link": link, "system": 5},
        )
        self.assertEqual(201, response.status_code)
        self.assertEqual(Documentation.objects.last().link, link)


class ProjectSerializeTest(TestCase):  # noqa: D101
    def setUp(self):
        """Set up project, instances and logged-in heliport user."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.project, is_new = Project.objects.get_or_create(owner=self.user)
        self.client.force_login(self.auth_user)
        self.docu_description = "DOCU TO SERIALIZE"
        self.docu = Documentation(
            description=self.docu_description,
            system=Documentation.DOCU_HEDGEDOC,
            persistent_id="https://hdl.handle.net/aaa",
        )
        self.docu.save()
        self.docu.link = "docu.de"
        self.docu.projects.add(self.project)

    @staticmethod
    def landing_page_url(obj):  # noqa: D102
        return reverse("core:landing_page", kwargs={"pk": obj.digital_object_id})

    def test_json_ld(self):  # noqa: D102
        response = self.client.get(
            f"{self.landing_page_url(self.project)}?format=jsonld"
        )
        self.assertContains(response, self.docu_description)
        response = self.client.get(f"{self.landing_page_url(self.docu)}?format=jsonld")
        docu = response.json()
        self.assertIn("@id", docu)
        self.assertEqual(self.docu_description, docu["dcterms:description"])
        self.assertContains(response, "https://docu.de")
        self.assertContains(response, "https://hdl.handle.net/aaa")

    def test_json_ld_other(self):  # noqa: D102
        d2 = Documentation(system=Documentation.DOCU_OTHER)
        d2.save()
        d2.projects.add(self.project)
        response = self.client.get(f"{self.landing_page_url(d2)}?format=jsonld")
        self.assertEqual(
            ["fabio:PersonalCommunication", "documentation"],
            response.json().get("@type"),
        )

    def test_datacite(self):  # noqa: D102
        response = self.client.get(
            f"{self.landing_page_url(self.docu)}?format=datacite_json"
        )
        metadata = response.json()
        self.assertEqual(
            [
                {
                    "description": self.docu_description,
                    "descriptionType": "Abstract",
                    "lang": "en",
                }
            ],
            metadata["descriptions"],
        )
        self.assertEqual(
            {"resourceType": "HedgeDoc", "resourceTypeGeneral": "Text"},
            metadata["types"],
        )

        def test_types(special, general, system):
            d = Documentation(system=system, persistent_id="a")
            d.save()
            d.projects.add(self.project)
            r = self.client.get(f"{self.landing_page_url(d)}?format=datacite_json")
            self.assertEqual(
                {"resourceType": special, "resourceTypeGeneral": general},
                r.json()["types"],
            )

        test_types("OpenBis", "Collection", 1)
        test_types("MediaWiki", "Text", 2)
        test_types("Lims", "Other", 3)
        test_types("HedgeDoc", "Text", 4)
        test_types("documentation", "Other", 5)

    def test_rest_api(self):  # noqa: D102
        response = self.client.get("/api/documentations/")
        docu = response.json()["results"][0]
        self.assertEqual(
            {
                "documentation_id": 1,
                "description": self.docu_description,
                "system": 4,
                "link": "https://docu.de",
                "projects": [self.project.pk],
            },
            docu,
        )
