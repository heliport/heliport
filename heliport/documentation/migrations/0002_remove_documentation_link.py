# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Generated by Django 4.0.6 on 2022-07-14 14:35

from django.db import migrations, models


def make_link_attribute(apps, schema_editor):
    attr_model = apps.get_model("core", "DigitalObjectAttributes")
    do_model = apps.get_model("core", "DigitalObject")
    docu_model = apps.get_model("documentation", "Documentation")

    if docu_model.objects.first() is not None:
        primary_topic, is_new = do_model.objects.get_or_create(
            special_heliport_role="primary_topic"
        )
        for docu in docu_model.objects.all():
            attr, is_new = attr_model.objects.get_or_create(
                subject=docu,
                predicate=primary_topic,
                special_heliport_role="attribute",
            )
            attr.value = docu.link
            attr.save()


def make_link_db_field(apps, schema_editor):
    attr_model = apps.get_model("core", "DigitalObjectAttributes")
    do_model = apps.get_model("core", "DigitalObject")
    docu_model = apps.get_model("documentation", "Documentation")

    if docu_model.objects.first() is not None:
        primary_topic, is_new = do_model.objects.get_or_create(
            special_heliport_role="primary_topic"
        )
        for docu in docu_model.objects.all():
            link = attr_model.objects.filter(
                subject=docu, predicate=primary_topic, special_heliport_role="attribute"
            ).first()
            link_value = link.value if link is not None else ""
            docu.link = link_value
            docu.save()


class Migration(migrations.Migration):
    dependencies = [
        ("documentation", "0001_initial"),
        ("core", "0017_digitalobjectrelation_is_public_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="documentation",
            name="link",
            field=models.CharField(default="", max_length=300),
        ),
        migrations.RunPython(make_link_attribute, make_link_db_field),
        migrations.RemoveField(
            model_name="documentation",
            name="link",
        ),
    ]
