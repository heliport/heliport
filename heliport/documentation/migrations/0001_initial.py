# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Generated by Django 3.1.14 on 2022-02-17 13:23

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("core", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Documentation",
            fields=[
                (
                    "digitalobject_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        to="core.digitalobject",
                    ),
                ),
                (
                    "documentation_id",
                    models.AutoField(primary_key=True, serialize=False),
                ),
                (
                    "system",
                    models.IntegerField(
                        choices=[
                            (1, "OpenBis"),
                            (2, "MediaWiki"),
                            (3, "Lims"),
                            (4, "HedgeDoc"),
                            (5, "Other"),
                        ]
                    ),
                ),
                ("link", models.CharField(max_length=300)),
            ],
            bases=("core.digitalobject",),
        ),
    ]
