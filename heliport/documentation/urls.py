# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path
from rest_framework import routers

from .views import (
    DocumentationUpdateView,
    DocumentationView,
    DocumentationViewSet,
    SearchView,
)

app_name = "documentation"
urlpatterns = [
    path(
        "project/<int:project>/documentation/list/",
        DocumentationView.as_view(),
        name="list",
    ),
    path(
        "project/<int:project>/documentation/<int:pk>/update/",
        DocumentationUpdateView.as_view(),
        name="update",
    ),
    path("documentation/search/", SearchView.as_view(), name="search"),
]

# REST API
router = routers.DefaultRouter()
router.register(r"documentations", DocumentationViewSet, basename="documentations")
