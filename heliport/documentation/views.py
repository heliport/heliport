# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

import django_filters
from django.conf import settings
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import CreateView, TemplateView, UpdateView
from rest_framework import filters as rest_filters

from heliport.core.mixins import (
    HeliportLoginRequiredMixin,
    HeliportObjectMixin,
    HeliportProjectMixin,
)
from heliport.core.models import Project
from heliport.core.permissions import is_object_member
from heliport.core.views import HeliportModelViewSet

from .models import Documentation
from .serializers import DocumentationSerializer


class DocumentationView(HeliportProjectMixin, CreateView):  # noqa: D101
    model = Documentation
    template_name = "documentation/documentation.html"
    fields = ["description", "system"]

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the documentations to show.
        """
        context = super().get_context_data(**kwargs)
        context["systems"] = Documentation.DOCU_SYSTEMS
        context["docus"] = Documentation.objects.filter(
            projects=context["project"], deleted__isnull=True
        )
        context["update"] = False
        return context

    def post(self, request, *args, **kwargs):  # noqa: D102
        documentation_id = request.POST.get("remove")
        if documentation_id:
            documentation = get_object_or_404(Documentation, pk=documentation_id)
            documentation.mark_deleted(self.request.user.heliportuser)
            return redirect("documentation:list", **self.kwargs)
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):  # noqa: D102
        form.instance.category_str = settings.DOCUMENTATION_NAMESPACE
        form.instance.save()
        form.instance.link = self.request.POST.get("link")
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        form.instance.projects.add(project)
        return super().form_valid(form)

    def get_success_url(self):  # noqa: D102
        return reverse("documentation:list", kwargs=self.kwargs)


class DocumentationUpdateView(HeliportObjectMixin, UpdateView):  # noqa: D101
    model = Documentation
    template_name = "documentation/documentation.html"
    fields = ["description", "system"]

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the documentation pages to show.
        """
        context = super().get_context_data(**kwargs)
        context["systems"] = Documentation.DOCU_SYSTEMS
        context["docus"] = Documentation.objects.filter(
            projects=context["project"], deleted__isnull=True
        )
        context["update"] = True
        return context

    def form_valid(self, form):  # noqa: D102
        form.instance.save()
        form.instance.link = self.request.POST.get("link")
        return super().form_valid(form)

    def get_success_url(self):  # noqa: D102
        return reverse("documentation:list", kwargs={"project": self.kwargs["project"]})


class SearchView(HeliportLoginRequiredMixin, TemplateView):  # noqa: D101
    template_name = "documentation/search.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the search results.
        """
        context = super().get_context_data(**kwargs)
        user = self.request.user.heliportuser
        query = self.request.GET.get("q", "")

        documentation_results = set()

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            documentation_results.update(
                Documentation.objects.filter(
                    is_object_member(user)
                    & Q(projects__deleted__isnull=True, deleted__isnull=True)
                    & (
                        Q(documentation_id=num)
                        | Q(description__icontains=word)
                        | Q(system=num)
                        | Q(attributes__value__icontains=word)
                    )
                )
            )

        context["documentation_results"] = documentation_results
        return context


##########################################
#               REST API                 #
##########################################


class DocumentationViewSet(HeliportModelViewSet):
    """Documentation."""

    serializer_class = DocumentationSerializer
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    filterset_fields = ["documentation_id", "system"]
    search_fields = ["attributes__value"]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return Documentation.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()
