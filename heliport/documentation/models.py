# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains :class:`django.db.models.Model` classes for Django ORM.

See :ref:`django:topics/db/models:quick example` from Django documentation.
In HELIPORT the :class:`heliport.core.models.DigitalObject` can be subclassed for models
containing metadata in a project.
"""

from django.db import models

from heliport.core.models import DigitalObject, MetadataField, Vocabulary
from heliport.core.utils.normalization import url_normalizer


class Documentation(DigitalObject):  # noqa: D101
    DOCU_OPEN_BIS = 1
    DOCU_MEDIA_WIKI = 2
    DOCU_LIMS = 3
    DOCU_HEDGEDOC = 4
    DOCU_OTHER = 5
    DOCU_SYSTEMS = [
        (DOCU_OPEN_BIS, "OpenBis"),
        (DOCU_MEDIA_WIKI, "MediaWiki"),
        (DOCU_LIMS, "Lims"),
        (DOCU_HEDGEDOC, "HedgeDoc"),
        (DOCU_OTHER, "Other"),
    ]

    documentation_id = models.AutoField(primary_key=True)
    system = models.IntegerField(choices=DOCU_SYSTEMS)
    link = MetadataField(Vocabulary.primary_topic, url_normalizer)
