# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Django app configuration.

Some HELIPORT hooks can be registered in app config :meth:`django.apps.AppConfig.ready`.

See also :ref:`Django documentation <django:ref/applications:applications>`
"""

from django.apps import AppConfig

from heliport.core.app_interaction import heliport_modules
from heliport.core.digital_object_resolution import object_types


class SmbFilesConfig(AppConfig):
    """The SMB APP."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "heliport.smb_files"

    def ready(self):
        """Register types for resolving them."""
        from .models import SMBDirectoryObj, SMBFileObj, SMBShareModule

        object_types.register(SMBFileObj)
        object_types.register(SMBDirectoryObj)
        heliport_modules.register(SMBShareModule)
