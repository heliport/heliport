# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

import django_filters
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import TemplateView
from rest_framework import filters as rest_filters
from rest_framework.decorators import action

from heliport.core.mixins import HeliportLoginRequiredMixin
from heliport.core.models import LoginInfo
from heliport.core.permissions import is_object_member
from heliport.core.utils.exceptions import UserMessage
from heliport.core.utils.form_description import FormDescription
from heliport.core.utils.http import (
    directory_api_response,
    directory_from_description_api_response,
    file_api_response,
    file_from_description_api_response,
)
from heliport.core.views import HeliportModelViewSet, HeliportObjectListView
from heliport.smb_files.models import DBSMBDirectory, DBSMBFile, SMBFileObj
from heliport.smb_files.serializers import DirectorySerializer, FileSerializer


class SMBView(HeliportObjectListView):
    """View for showing and editing SMB shares."""

    model = DBSMBDirectory
    category = settings.SMB_DIRECTORY_NAMESPACE
    list_url = "smb_files:list"
    update_url = "smb_files:update"
    list_heading = "SMB Shares"
    create_heading = "Add a SMB Share"
    info_snippet_add = "core/partials/data_source_warning.html"

    def register_form_columns(self, form: FormDescription):
        """Select which (and how) attributes should be included in edit form."""
        form.add_column("Server Name", "server_name", {"required": True})

        form.add_column("Share Name", "share_name", {"required": True})
        form.add_column(
            "Login",
            "login",
            {
                "widget": "login",
                "type": LoginInfo.LoginTypes.USER_PASSWORD,
                "required": True,
            },
        )
        form.add_column(
            "Domain",
            "domain",
            {
                "placeholder": "can usually be left blank",
                "help_text": "The network domain. "
                "On windows, it is known as the workgroup. "
                "Usually, it is safe to leave this parameter blank.",
            },
        )
        form.add_column("Path", "path_str", {"placeholder": "without share name"})

    def set_update_attributes(self, obj, form_fields, obj_saved=False):
        """Get login from pk of html form results and set label."""
        if not obj_saved:
            login = get_object_or_404(LoginInfo, pk=form_fields["login"])
            if login.user != self.request.user.heliportuser:
                raise PermissionDenied()
            form_fields["login"] = login
            obj.label = form_fields["path_str"] or form_fields["share_name"]

        super().set_update_attributes(obj, form_fields, obj_saved)


class SearchView(HeliportLoginRequiredMixin, TemplateView):
    """implement HELIPORT search."""

    template_name = "smb_files/search.html"

    def get_context_data(self, **kwargs):
        """Do database query, called by ``TemplateView``."""
        context = super().get_context_data(**kwargs)
        query = self.request.GET.get("q", "")

        smb_file_results = set()
        smb_directory_results = set()

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            query = Q(deleted__isnull=True) & (
                Q(pk=num)
                | Q(description__icontains=word)
                | Q(attributes__value__icontains=word)
                | Q(path_str__icontains=word)
                | Q(share_name__icontains=word)
                | Q(server_name__icontains=word)
                | Q(domain__icontains=word)
                | Q(login__name__icontains=word)
            )

            smb_file_results.update(DBSMBFile.objects.filter(query))
            smb_directory_results.update(DBSMBDirectory.objects.filter(query))

        context["smb_directory_results"] = smb_directory_results
        context["smb_file_results"] = smb_file_results
        return context


##########################################
#               REST API                 #
##########################################


class FileViewSet(HeliportModelViewSet):
    """SMB Files.

    File contents is available for download at /api/smb/files/<file_id>/download
    """

    serializer_class = FileSerializer
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    filterset_fields = [
        "file_id",
        "persistent_id",
        "digital_object_id",
        "share_name",
        "server_name",
        "path_str",
    ]
    search_fields = ["attributes__value", "path_str", "server_name", "share_name"]

    def get_queryset(self):
        """Get files of user that are not deleted."""
        user = self.request.user.heliportuser
        return DBSMBFile.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()

    @action(detail=True, methods=["get"])
    def download(self, request, pk=None):
        """Get streaming file response to download file contents."""
        db_object = self.get_object()
        return file_api_response(request, db_object, db_object.path.name)

    @action(detail=False, methods=["get"])
    def download_from_description(self, request):
        """Get streaming file response to download file contents.

        The file to download is specified by request parameters.
        This enables downloading files not directly stored in heliport e.g. inside
        a directory.
        """

        def get_file_name(file_obj):
            if isinstance(file_obj, DBSMBFile):
                return str(file_obj.path.name)
            if isinstance(file_obj, SMBFileObj):
                return str(file_obj.smb_addr.path.name)

            raise UserMessage(
                f"{file_obj} is a {type(file_obj).__name__} and not an SMB file"
            )

        return file_from_description_api_response(request, get_file_name)


class DirectoryViewSet(HeliportModelViewSet):
    """SMB Directories.

    Directory contents is available at /api/smb/files/<directory_id>/contents
    """

    serializer_class = DirectorySerializer
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    filterset_fields = [
        "directory_id",
        "persistent_id",
        "digital_object_id",
        "share_name",
        "server_name",
        "path_str",
    ]
    search_fields = ["attributes__value", "path_str", "server_name", "share_name"]

    def get_queryset(self):
        """Get directories of user that are not deleted."""
        user = self.request.user.heliportuser
        return DBSMBDirectory.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()

    @action(detail=True, methods=["get"])
    def contents(self, request, pk=None):
        """Get files and directories in directory."""
        db_object = self.get_object()
        file_url = request.build_absolute_uri(
            reverse("smb_files-download-from-description")
        )
        directory_url = request.build_absolute_uri(
            reverse("smb_directories-contents-from-description")
        )

        return directory_api_response(request, db_object, file_url, directory_url)

    @action(detail=False, methods=["get"])
    def contents_from_description(self, request):
        """Get files and directories in directory.

        directory is specified via describing parameters to allow download of
        subdirectories
        """
        file_url = request.build_absolute_uri(
            reverse("smb_files-download-from-description")
        )
        directory_url = request.build_absolute_uri(
            reverse("smb_directories-contents-from-description")
        )
        return directory_from_description_api_response(request, file_url, directory_url)
