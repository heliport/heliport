# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains :class:`django.db.models.Model` classes for Django ORM.

See :ref:`django:topics/db/models:quick example` from Django documentation.
In HELIPORT the :class:`heliport.core.models.DigitalObject` can be subclassed for models
containing metadata in a project.

Models for representing SMB files and directories as Django models (stored in HELIPORT
database) or as objects not stored in HELIPORT database.
"""

import io
import logging
from abc import ABC, abstractmethod
from dataclasses import asdict, dataclass
from mimetypes import guess_type
from pathlib import Path
from typing import Dict, Iterable, Optional, Type, Union

from django.conf import settings
from django.db import models
from django.urls import reverse
from smb.base import SharedFile, SMBTimeout
from smb.smb_structs import OperationFailure
from smb.SMBConnection import SMBConnection

from heliport.core.app_interaction import DigitalObjectModule
from heliport.core.digital_object_aspects import Directory, DirectoryObj, File, FileObj
from heliport.core.digital_object_interface import GeneralDigitalObject
from heliport.core.models import DigitalObject, LoginInfo
from heliport.core.utils.collections import RootedRDFGraph
from heliport.core.utils.context import Context
from heliport.core.utils.exceptions import AccessDenied, UserMessage
from heliport.core.utils.string_tools import format_byte_count, format_time

logger = logging.getLogger(__name__)


@dataclass(frozen=True)
class SMBAddress:
    """hold the information needed to identify and retrieve some SMB path."""

    #: username and password login for the connection
    login: LoginInfo
    #: name of the smb server
    server_name: str
    #: name of the smb share
    share_name: str
    #: path of the file as pathlib path object
    path: Path
    #: address or hostname
    domain: str

    @property
    def path_str(self):
        """Path of the file as string."""
        return str(self.path)

    def __post_init__(self):
        """Normalize."""
        # object.__setatter__ is the normal way to set attributes in init
        # for frozen dataclasses
        if not self.path.is_absolute():
            object.__setattr__(self, "path", Path("/") / self.path)

    def child(self, file_name) -> "SMBAddress":
        """Construct the address of the sub path."""
        sub_path = self.path / file_name
        if sub_path != file_name and self.path not in sub_path.resolve().parents:
            raise AccessDenied("access outside directory")
        return SMBAddress(
            self.login, self.server_name, self.share_name, sub_path, self.domain
        )


class DBSMBPath(models.Model):
    """base class for SMB files and directories stored in heliport database."""

    path_str = models.TextField()
    share_name = models.TextField()
    server_name = models.TextField()
    domain = models.TextField()
    login = models.ForeignKey(LoginInfo, on_delete=models.SET_NULL, null=True)

    class Meta:
        """make this Django model abstract."""

        abstract = True

    @property
    def path(self):
        """The path as pathlib ``Path``."""
        return Path(self.path_str)

    @property
    def smb_addr(self):
        """The information stored as :class:`SMBAddress`."""
        return SMBAddress(
            self.login,
            self.server_name,
            self.share_name,
            self.path,
            self.domain,
        )

    @classmethod
    def from_smb_addr(cls, addr: SMBAddress):
        """Construct a new object from :class:`SMBAddress`."""
        return cls(
            path_str=addr.path_str,
            share_name=addr.share_name,
            server_name=addr.server_name,
            domain=addr.domain,
            login=addr.login,
        )

    @classmethod
    def get_by_smb_addr(cls, addr: SMBAddress):
        """Search for an existing object matching the :class:`SMBAddress`."""
        return (
            cls.objects.filter(
                path_str=addr.path_str,
                share_name=addr.share_name,
                server_name=addr.server_name,
                domain=addr.domain,
                login=addr.login,
            )
            .order_by("pk")
            .first()
        )

    def access(self, context: Context):
        """Get permission checker for this object and context.

        See :meth:`heliport.core.models.DigitalObject.access` for base implementation.

        There is also a check that only gives write permission if the current user owns
        the login.
        """
        from heliport.core.permissions import LoginPermissionChecker

        return LoginPermissionChecker(self, context)


class DBSMBFile(DigitalObject, DBSMBPath, FileObj):
    """Represent SMBFile in database."""

    file_id = models.AutoField(primary_key=True)

    def as_file(self, context: Context) -> File:
        """Makes this into a file recognized by HELIPORT."""  # noqa: D401
        self.access(context).assert_read_permission()
        return SMBFile(context, self.smb_addr)


class DBSMBDirectory(DigitalObject, DBSMBPath, DirectoryObj):
    """Represent SMBDirectory in database."""

    directory_id = models.AutoField(primary_key=True)

    def as_directory(self, context: Context) -> Directory:
        """Makes this into a directory recognized by HELIPORT."""  # noqa: D401
        self.access(context).assert_read_permission()
        return SMBDirectory(context, self.smb_addr)


class SMBShareModule(DigitalObjectModule):
    """The module to show SMB shares in HELIPORT project graph."""

    name = "SMB Shares"
    module_id = "smb_shares"
    object_class = (DBSMBFile, DBSMBDirectory)
    icon = "fa-solid fa-laptop-file"

    def get_url(self, project):
        """List url for SMB shares."""
        return reverse("smb_files:list", kwargs={"project": project.pk})


class SMBPathObj(ABC, GeneralDigitalObject):
    """base class for objects representing SMB files and directories not in database."""

    def __init__(self, smb_addr: SMBAddress):  # noqa: D107
        self.smb_addr = smb_addr

    @staticmethod
    @abstractmethod
    def db_class() -> Type[DBSMBPath]:
        """The related Django model (:class:`DBSMBFile` or :class:`DBSMBDirectory`."""  # noqa: D401, E501

    def as_digital_object(self, context: Context) -> DigitalObject:
        """Store this object in database or return the one that already exists."""
        existing = self.db_class().get_by_smb_addr(self.smb_addr)
        if existing is not None:
            return existing
        new = self.db_class().from_smb_addr(self.smb_addr)
        new.namespace_str = settings.SMB_FILE_NAMESPACE
        new.label = self.as_text()
        new.save()
        assert isinstance(new, DigitalObject), "db_class not DigitalObject subclass"
        return new

    def as_text(self) -> str:
        """Present by path name or fall back to share name."""
        return self.smb_addr.path.name or self.smb_addr.share_name

    def as_rdf(self) -> RootedRDFGraph:
        """Serialize just as string for now."""
        return RootedRDFGraph.from_atomic(self.as_text())

    def as_html(self) -> str:
        """Present in html using :meth:`as_text`."""
        return self.as_text()

    def __hash__(self):
        """Hashable."""
        return hash(self.smb_addr)

    def __eq__(self, other):
        """Comparable."""  # noqa: D401
        if isinstance(other, type(self)):
            return other.smb_addr == self.smb_addr
        return False

    def get_identifying_params(self) -> Dict[str, str]:
        """Parameters from which the object can be resolved using :meth:`resolve`."""
        return {
            **asdict(self.smb_addr),
            "path": self.smb_addr.path_str,
            "login": self.smb_addr.login.pk,
            "type": self.type_id(),
        }

    @classmethod
    def resolve(
        cls, params: Dict[str, str], context: Context
    ) -> Optional[GeneralDigitalObject]:
        """Construct object from identifying parameters; prefer existing one form db."""
        assert params["type"] == cls.type_id(), "resolution failed"

        try:
            login = LoginInfo.objects.get(pk=params.get("login"))
        except LoginInfo.DoesNotExist:
            logger.debug("error during smb object resolution: login does not exist")
            return None

        try:
            smb_addr = SMBAddress(
                login,
                params["server_name"],
                params["share_name"],
                Path(params.get("path", "")),
                params.get("domain"),
            )
        except KeyError as e:
            logger.debug(f"error during smb object resolution: {e}")
            return None

        existing = cls.db_class().get_by_smb_addr(smb_addr)
        return existing or cls(smb_addr)


class SMBFileObj(SMBPathObj, FileObj):
    """represent SMB file not stored in HELIPORT database."""

    def as_file(self, context: Context) -> File:
        """Makes this into a file recognized by HELIPORT."""  # noqa: D401
        return SMBFile(context, self.smb_addr)

    @staticmethod
    def type_id() -> str:
        """Type id for resolution is: SMBFile."""
        return "SMBFile"

    @staticmethod
    def db_class() -> Type[DBSMBPath]:
        """Related Django model is :class:`DBSMBFile`."""
        return DBSMBFile


class SMBDirectoryObj(SMBPathObj, DirectoryObj):
    """represent SMB directory not stored in HELIPORT database."""

    def as_directory(self, context: Context) -> Directory:
        """Makes this into a directory recognized by HELIPORT."""  # noqa: D401
        return SMBDirectory(context, self.smb_addr)

    @staticmethod
    def type_id() -> str:
        """Type id for resolution is: SMBDirectory."""
        return "SMBDirectory"

    @staticmethod
    def db_class() -> Type[DBSMBPath]:
        """Related Django model is :class:`DBSMBDirectory`."""
        return DBSMBDirectory


class SMBFile(File):
    """The actual readable file object for SMB files."""

    #: current position in the file (in bytes)
    pos: int

    def __init__(self, context: Context, smb_addr):  # noqa: D107
        self.pos = 0
        self.addr = smb_addr
        self.context = context

    @property
    def file(self) -> SharedFile:
        """Helper function to get the SMB file metadata."""
        return self.context[SMBStat(self.addr)]

    def mimetype(self) -> str:
        """Guesses the mimetype based on file name."""
        mimetype, encoding = guess_type(self.addr.path)
        if mimetype is None:
            mimetype = "application/octet-stream"  # just binary data
        return mimetype

    def open(self):
        """Reset the position back to the beginning."""
        self.pos = 0

    def close(self):
        """Nothing to close."""

    def read(self, number_of_bytes=None):
        """Read ``number_of_bytes`` starting from :attr:`pos`."""
        conn: SMBConnection
        conn = self.context[SMBConn.from_addr(self.addr)]
        data = io.BytesIO()
        conn.retrieveFileFromOffset(
            self.addr.share_name,
            self.addr.path_str,
            data,
            offset=self.pos,
            max_length=-1 if number_of_bytes is None else number_of_bytes,
        )
        result = data.getvalue()
        self.pos += len(result)
        return result

    def size(self):
        """The file size in bytes."""  # noqa: D401
        return self.file.file_size

    def get_file_info(self):
        """Get human-readable file metadata (size and last access time)."""
        return {
            "size": format_byte_count(self.size()),
            "last access": format_time(self.file.last_access_time),
        }


class SMBDirectory(Directory):
    """The actual readable directory object for SMB directories."""

    def __init__(self, context: Context, smb_addr: SMBAddress):  # noqa: D107
        self.context = context
        self.addr = smb_addr

    @property
    def file(self) -> SharedFile:
        """Get the smb file metadata object."""
        return self.context[SMBStat(self.addr)]

    def get_parts(self) -> Iterable[Union[FileObj, DirectoryObj]]:
        """Yield the children of this directory."""
        conn: SMBConnection
        conn = self.context[SMBConn.from_addr(self.addr)]

        try:
            for file in conn.listPath(self.addr.share_name, self.addr.path_str):
                if file.filename.startswith("."):
                    continue
                child_addr = self.addr.child(file.filename)
                self.context.cache(SMBStat(child_addr), file)
                if file.isDirectory:
                    yield SMBDirectoryObj(child_addr)
                else:
                    yield SMBFileObj(child_addr)
        except OperationFailure:
            raise UserMessage(
                "Error during SMB communication; this can be e.g. because of invalid "
                "username or password or the path does not exist"
            ) from None

    def get_file_info(self) -> Dict[str, str]:
        """Get human-readable metadata for this directory (only last access time)."""
        return {"last access": format_time(self.file.last_access_time)}


@dataclass(frozen=True)
class SMBConn:
    """
    Immutable dataclass to describe an SMB connection.
    Implements the :class:`heliport.core.utils.context.Description` protocol.

    Note that the SMB connection is user specific since the login is user specific.
    This reduces the chance that somebody can use the connection of someone else on top
    of the fact that the context is specific for a request (and therefore user) anyway.
    """  # noqa: D205

    #: username and password login for the connection
    login: LoginInfo
    #: name of the smb server
    server_name: str
    #: address or hostname
    domain: str

    def generate(self, context):
        """
        Open a new connection; it is closed automatically when used with
        :class:`heliport.core.utils.context.Context`.
        """  # noqa: D205
        connection = SMBConnection(
            self.login.username,
            self.login.password,
            "HELIPORT",
            self.server_name,
            self.domain,
        )
        try:
            if not connection.connect(self.server_name, timeout=5):
                raise UserMessage("SMB connection authentication failed")
        except (TimeoutError, SMBTimeout):
            raise UserMessage("SMB connect timed out") from None
        except OSError:
            raise UserMessage("SMB server not available") from None
        return connection

    @staticmethod
    def from_addr(smb_addr: SMBAddress):
        """Get a connection description from :class:`SMBAddress`."""
        return SMBConn(smb_addr.login, smb_addr.server_name, smb_addr.domain)


@dataclass(frozen=True)
class SMBStat:
    """description of SMB file metadata; used for caching."""

    #: address of the SMB file in question
    address: SMBAddress

    def generate(self, context):
        """
        Download the metadata; implements the
        :class:`heliport.core.utils.context.Description` protocol.
        """  # noqa: D205
        conn = context[SMBConn.from_addr(self.address)]
        return conn.getAttributes(self.address.share_name, self.address.path_str)
