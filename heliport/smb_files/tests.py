# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

from pathlib import Path
from types import SimpleNamespace
from urllib.parse import urlencode

from django.urls import reverse

from heliport.core.models import LoginInfo
from heliport.core.tests import WithProject
from heliport.core.utils.context import Context
from heliport.core.utils.exceptions import AccessDenied

from .models import (
    DBSMBDirectory,
    DBSMBFile,
    SMBAddress,
    SMBConn,
    SMBDirectory,
    SMBDirectoryObj,
    SMBFile,
    SMBFileObj,
    SMBStat,
)


class DummyConnection:  # noqa: D101
    def __init__(self, file_contents=b"", dir_content=()):  # noqa: D107
        self.file_contents = file_contents
        self.dir_content = dir_content

    def retrieveFileFromOffset(  # noqa: D102
        self, share, path, data, offset, max_length
    ):
        data.write(self.file_contents[offset : offset + max_length])

    def listPath(self, share, path):  # noqa: D102
        return [
            SimpleNamespace(filename=f, isDirectory=i % 2)
            for i, f in enumerate(self.dir_content)
        ]


class SMBTests(WithProject):  # noqa: D101
    def test_list(self):  # noqa: D102
        LoginInfo.objects.create(
            type=LoginInfo.LoginTypes.USER_PASSWORD, name="login1", user=self.user
        )
        LoginInfo.objects.create(
            type=LoginInfo.LoginTypes.USER_PASSWORD, name="login2", user=self.user
        )
        LoginInfo.objects.create(
            type=LoginInfo.LoginTypes.SSH, name="login3", user=self.user
        )
        smb = DBSMBDirectory.objects.create(label="smb_label")
        smb.projects.add(self.project)

        response = self.client.get(
            reverse("smb_files:list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(response.status_code, 200)
        test_objs = DBSMBDirectory.objects.filter(
            projects=response.context_data["project"], deleted__isnull=True
        )
        self.assertGreater(len(test_objs), 0)

        self.assertContains(response, "smb_label")
        self.assertContains(response, "login1")
        self.assertContains(response, "login2")
        self.assertNotContains(response, "login3")

    def test_create(self):  # noqa: D102
        login = LoginInfo.objects.create(
            type=LoginInfo.LoginTypes.USER_PASSWORD, name="login1", user=self.user
        )
        self.client.post(
            reverse("smb_files:list", kwargs={"project": self.project.pk}),
            data=urlencode(
                {
                    "form_data_field_server_name": "the_server",
                    "form_data_field_domain": "",
                    "form_data_field_share_name": "the_share",
                    "form_data_field_path_str": "",
                    "form_data_field_login": str(login.pk),
                }
            ),
            content_type="application/x-www-form-urlencoded",
        )
        test_obj = DBSMBDirectory.objects.filter(server_name="the_server").first()
        self.assertIsNotNone(test_obj)
        self.assertIn(self.project, test_obj.projects.all())
        self.assertEqual("the_share", test_obj.label)
        self.assertEqual(login, test_obj.login)

    def test_smb_addr(self):  # noqa: D102
        login = LoginInfo.objects.create(
            type=LoginInfo.LoginTypes.USER_PASSWORD, name="login1", user=self.user
        )
        addr = SMBAddress(login, "the_server", "the_share", Path(), "")
        self.assertEqual(login, addr.login)
        self.assertEqual("the_server", addr.server_name)
        self.assertEqual("", addr.domain)
        self.assertEqual("the_share", addr.share_name)
        self.assertEqual(Path("/"), addr.path)
        self.assertEqual("/", addr.path_str)
        self.assertEqual(Path("/abc"), addr.child("abc").path)
        self.assertRaises(AccessDenied, addr.child("abc").child, "../def")

        obj1 = DBSMBDirectory.from_smb_addr(addr)
        obj2 = DBSMBFile.from_smb_addr(addr)
        obj3 = DBSMBDirectory.from_smb_addr(addr.child("abc"))
        obj4 = DBSMBFile.from_smb_addr(addr.child("def"))
        obj1.save()
        obj2.save()
        obj3.save()
        obj4.save()
        self.assertEqual(addr, obj1.smb_addr)
        self.assertEqual(addr, obj2.smb_addr)
        self.assertEqual(obj3, DBSMBDirectory.get_by_smb_addr(addr.child("abc")))
        self.assertEqual(obj2, DBSMBFile.get_by_smb_addr(addr))

    def test_representations(self):  # noqa: D102
        login = LoginInfo.objects.create(
            type=LoginInfo.LoginTypes.USER_PASSWORD, name="login1", user=self.user
        )
        addr = SMBAddress(login, "the_server", "the_share", Path(), "")
        file_obj = SMBFileObj(addr)
        dir_obj = SMBDirectoryObj(addr.child("abc"))
        with Context(user=self.user) as context:
            db_file = file_obj.as_digital_object(context)
            db_dir = dir_obj.as_digital_object(context)
            self.assertEqual(db_file, file_obj.as_digital_object(context))
            self.assertEqual(db_dir, dir_obj.as_digital_object(context))
            self.assertEqual(db_file, file_obj.as_digital_object_if_exists(context))
            self.assertEqual(db_dir, dir_obj.as_digital_object_if_exists(context))

            # this gives permission
            db_file.projects.add(self.project)
            db_dir.projects.add(self.project)

            self.assertEqual(addr, db_file.as_file(context).addr)
            self.assertEqual(addr.child("abc"), db_dir.as_directory(context).addr)
            self.assertEqual(addr, file_obj.as_file(context).addr)
            self.assertEqual(addr.child("abc"), dir_obj.as_directory(context).addr)

        self.assertEqual("the_share", file_obj.as_html())
        self.assertEqual("abc", dir_obj.as_html())
        self.assertEqual(2, len({file_obj, dir_obj}))
        self.assertTrue(file_obj == file_obj)
        self.assertFalse(file_obj == dir_obj)

    def test_file(self):  # noqa: D102
        login = LoginInfo.objects.create(
            type=LoginInfo.LoginTypes.USER_PASSWORD, name="login1", user=self.user
        )
        addr = SMBAddress(login, "the_server", "the_share", Path("/x/a.txt"), "")
        with Context() as context:
            context.cache(SMBConn.from_addr(addr), DummyConnection(file_contents=b"ab"))
            file = SMBFile(context, addr)
            file.pos = 10
            with file:
                self.assertEqual(b"a", file.read(1))
                self.assertEqual(b"b", file.read(10))

            self.assertEqual("text/plain", file.mimetype())
            context.cache(
                SMBStat(addr), SimpleNamespace(file_size=1024, last_access_time=0)
            )
            self.assertEqual("1 KiB", file.get_file_info()["size"])

    def test_directory(self):  # noqa: D102
        login = LoginInfo.objects.create(
            type=LoginInfo.LoginTypes.USER_PASSWORD, name="login1", user=self.user
        )
        addr = SMBAddress(login, "the_server", "the_share", Path("/x/a.txt"), "")
        with Context() as context:
            context.cache(
                SMBConn.from_addr(addr),
                DummyConnection(dir_content=("..", ".hidden", "a", "b")),
            )
            directory = SMBDirectory(context, addr)
            self.assertEqual(
                [SMBFileObj(addr.child("a")), SMBDirectoryObj(addr.child("b"))],
                list(directory.get_parts()),
            )


class SearchAndAPITest(WithProject):
    """Test search including via API."""

    def test_access_search(self):
        """Test that search URL is accessible."""
        response = self.client.get(reverse("smb_files:search"))
        self.assertEqual(200, response.status_code)

    def test_smb_findable(self):
        """Test that digital object is findable."""
        file = DBSMBFile.objects.create(server_name="file_server")
        directory = DBSMBDirectory.objects.create(server_name="directory_server")
        file.projects.add(self.project)
        directory.projects.add(self.project)
        response = self.client.get(f"{reverse('smb_files:search')}?q=server")
        self.assertContains(response, "file_server")
        self.assertContains(response, "directory_server")

    def test_api(self):
        """Test api."""
        file = DBSMBFile.objects.create(server_name="file_server")
        directory = DBSMBDirectory.objects.create(server_name="directory_server")
        file.projects.add(self.project)
        directory.projects.add(self.project)
        response = self.client.get("/api/smb/files/")
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "file_server")
        response = self.client.get("/api/smb/directories/")
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "directory_server")
