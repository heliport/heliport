# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""This module is for serialization into Datacite, RDF and JSON.

The JSON format is used for API endpoints via
`Django rest framework <https://www.django-rest-framework.org/>`_. Using this, it is
typical to put the serializer classes into a "serializers.py" file.

For the serialization into RDF, attributes are described.
See :class:`heliport.core.attribute_description.BaseAttribute` for more detail.
"""

from django.conf import settings
from rest_framework import serializers

from heliport.core.models import LoginInfo
from heliport.core.permissions import projects_of
from heliport.core.serializers import NamespaceField
from heliport.smb_files.models import DBSMBDirectory, DBSMBFile


class DirectorySerializer(serializers.ModelSerializer):
    """serialize an SMB directory."""

    login = serializers.PrimaryKeyRelatedField(queryset=LoginInfo.objects.none())
    namespace_str = NamespaceField(settings.SMB_DIRECTORY_NAMESPACE)

    def __init__(self, *args, **kwargs):
        """Override querysets to only allow current user's objects."""
        super().__init__(*args, **kwargs)
        user = self.context["request"].user.heliportuser
        self.fields["login"].queryset = LoginInfo.objects.filter(user=user)
        self.fields["projects"].child_relation.queryset = projects_of(user)

    class Meta:
        """describe what should be serialized."""

        model = DBSMBDirectory
        fields = [
            "directory_id",
            "digital_object_id",
            "label",
            "description",
            "projects",
            "persistent_id",
            "path_str",
            "share_name",
            "server_name",
            "domain",
            "login",
            "namespace_str",
        ]


class FileSerializer(serializers.ModelSerializer):
    """serialize an SMB file."""

    login = serializers.PrimaryKeyRelatedField(queryset=LoginInfo.objects.none())
    namespace_str = NamespaceField(settings.SMB_FILE_NAMESPACE)

    def __init__(self, *args, **kwargs):
        """Override querysets to only allow current user's objects."""
        super().__init__(*args, **kwargs)
        user = self.context["request"].user.heliportuser
        self.fields["login"].queryset = LoginInfo.objects.filter(user=user)
        self.fields["projects"].child_relation.queryset = projects_of(user)

    class Meta:
        """describe what should be serialized."""

        model = DBSMBFile
        fields = [
            "file_id",
            "digital_object_id",
            "label",
            "description",
            "projects",
            "persistent_id",
            "path_str",
            "share_name",
            "server_name",
            "domain",
            "login",
            "namespace_str",
        ]
