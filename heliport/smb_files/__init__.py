# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""App to store metadata about and allow access of samba shared files.

The ``interface`` module is imported to the top level of the package for HELIPORT app
interface discovery (see :func:`heliport.core.app_interaction.get_heliport_apps`).
"""

from . import interface
