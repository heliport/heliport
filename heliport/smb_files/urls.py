# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path
from rest_framework import routers

from .views import DirectoryViewSet, FileViewSet, SearchView, SMBView

app_name = "smb_files"
urlpatterns = [
    path("project/<int:project>/smb/list/", SMBView.as_view(), name="list"),
    path(
        "project/<int:project>/smb/<int:pk>/update/",
        SMBView.as_view(),
        name="update",
    ),
    path("smb/search/", SearchView.as_view(), name="search"),
]

# REST API
router = routers.DefaultRouter()
router.register(r"smb/files", FileViewSet, basename="smb_files")
router.register(r"smb/directories", DirectoryViewSet, basename="smb_directories")
