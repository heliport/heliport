# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from celery import shared_task

from . import proposal_management
from .models import ProposalQueries, ProposalUserQueries


@shared_task
def update_gate_projects(update_all_users=False):  # noqa: D103
    use_case = proposal_management.UpdateProposalDatabaseUseCase(
        ProposalUserQueries, ProposalQueries, update_all_users
    )
    use_case.execute()

    return "OK"
