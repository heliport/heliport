# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""This module is for serialization into Datacite, RDF and JSON.

The JSON format is used for API endpoints via
`Django rest framework <https://www.django-rest-framework.org/>`_. Using this, it is
typical to put the serializer classes into a "serializers.py" file.

For the serialization into RDF, attributes are described.
See :class:`heliport.core.attribute_description.BaseAttribute` for more detail.
"""

from django.conf import settings
from rest_framework import serializers

from ..core.serializers import NamespaceField
from .models import GateProject, GateUser


class GateProjectSerializer(serializers.ModelSerializer):  # noqa: D101
    namespace_str = NamespaceField(settings.PROPOSAL_NAMESPACE)

    class Meta:  # noqa: D106
        model = GateProject
        fields = [
            "gate_id",
            "label",
            "status",
            "owner",
            "co_owners",
            "description",
            "proposal",
            "restricted",
            "responsible_experimentalist",
            "local_contacts",
            "projects",
            "experimentalists",
            "namespace_str",
        ]


class GateUserSerializer(serializers.ModelSerializer):  # noqa: D101
    class Meta:  # noqa: D106
        model = GateUser
        fields = ["gate_user_id", "gate_id", "heliport_user"]
