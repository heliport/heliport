# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from django.core.management.base import BaseCommand

from heliport.gate_connection.tasks import update_gate_projects


class Command(BaseCommand):  # noqa: D101
    help = "Update GATE projects"

    def add_arguments(self, parser):
        """Add --refresh argument to the command parser."""
        parser.add_argument(
            "--refresh",
            action="store_true",
            help="Download and update even gate user that already exist.",
        )

    def handle(self, *args, **options):
        """Execute the update gate projects process using celery."""
        update_gate_projects.delay(options["refresh"])
        self.stdout.write(
            self.style.SUCCESS(
                f"Task {update_gate_projects.__name__!r} added to the queue"
            )
        )
