# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import DetailView, ListView, TemplateView, View
from rest_framework import permissions as rest_permissions
from rest_framework import viewsets

from heliport.core.mixins import HeliportLoginRequiredMixin, HeliportProjectMixin
from heliport.core.models import Project
from heliport.core.permissions import StaffOrReadOnly, is_object_member
from heliport.core.utils.queries import create_empty_project
from heliport.core.views import HeliportModelViewSet

from .models import GateProject, GateUser
from .serializers import GateProjectSerializer, GateUserSerializer


class ConnectToGateProjectView(HeliportProjectMixin, ListView):  # noqa: D101
    template_name = "gate_connection/gate_connect.html"
    model = GateProject
    context_object_name = "gate_project_list"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the search query.
        """
        context = super().get_context_data(**kwargs)
        context["project_is_already_connected"] = GateProject.objects.filter(
            projects=context["project"]
        ).first()
        return context

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        user_gate_projects = GateProject.objects.filter(
            Q(owner=user) | Q(co_owners=user)
        ).distinct()

        query = self.request.GET.get("q")
        if query:
            user_gate_projects = set(user_gate_projects)
            for word in query.split():
                num = int(word) if word.isdecimal() else -1
                user_gate_projects.intersection_update(
                    GateProject.objects.filter(
                        (Q(owner=user) | Q(co_owners=user))
                        & (
                            Q(gate_id=num)
                            | Q(label__icontains=word)
                            | Q(owner__display_name__icontains=word)
                            | Q(co_owners__display_name__icontains=word)
                            | Q(description__icontains=word)
                            | Q(proposal__icontains=word)
                            | Q(
                                responsible_experimentalist__display_name__icontains=word
                            )
                            | Q(local_contacts__display_name__icontains=word)
                        )
                    )
                )

        return user_gate_projects

    def post(self, request, *args, **kwargs):  # noqa: D102
        gate_id = request.POST.get("connect")
        gate_project = get_object_or_404(GateProject, pk=gate_id)
        project = get_object_or_404(Project, pk=kwargs["project"])

        project_already_in = gate_project.projects.first()
        if project_already_in is not None:
            return redirect("gate_connection:connect", project=project_already_in.pk)

        gate_project.projects.add(project)

        if not project.label:
            project.label = gate_project.title
            project.save()

        # add co_proposers to owners
        project.co_owners.add(*gate_project.co_proposer.exclude(pk=project.owner.pk))

        # add proposer to owners
        if project.owner != gate_project.proposer:
            project.co_owners.add(gate_project.proposer)

        return redirect(
            "gate_connection:connected", project=project.pk, pk=gate_project.pk
        )


class ConnectedToGateProjectView(HeliportProjectMixin, DetailView):  # noqa: D101
    template_name = "gate_connection/gate_connected.html"
    model = Project
    pk_url_kwarg = "project"

    def get(self, request, *args, **kwargs):  # noqa: D102
        if not GateProject.objects.filter(projects=self.get_object()):
            return redirect("gate_connection:connect", project=self.get_object().pk)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes information about the GATE project.
        """

        def co_proposer_sort_key(co_proposer):
            position = 3
            if co_proposer == self.object.owner:
                position -= 2
            if co_proposer in self.object.co_owners.all():
                position -= 1
            return position, str(co_proposer)

        context = super().get_context_data(**kwargs)
        context["is_owner"] = self.request.user.heliportuser == self.object.owner
        gate_project = GateProject.objects.filter(projects=self.object).first()
        context["gate_project"] = gate_project
        context["co_proposers"] = sorted(
            gate_project.co_proposer.all(), key=co_proposer_sort_key
        )
        context["experimentalists"] = sorted(
            gate_project.experimentalists.all(), key=co_proposer_sort_key
        )
        member_set = set(gate_project.experimentalists.all())
        member_set.update(gate_project.members())
        context["import"] = bool(member_set - self.object.members())
        return context

    def post(self, request, *args, **kwargs):  # noqa: D102
        action = request.POST.get("action")
        project = get_object_or_404(Project, pk=kwargs["project"])
        gate_project = GateProject.objects.filter(projects=project).first()

        if action == "import":
            # add co_proposers to owners
            project.co_owners.add(
                *gate_project.co_proposer.exclude(pk=project.owner.pk)
            )

            # add proposer to owners
            if project.owner != gate_project.proposer:
                project.co_owners.add(gate_project.proposer)

            # add experimentalists to owners
            project.co_owners.add(
                *gate_project.experimentalists.exclude(pk=project.owner.pk)
            )

            return redirect("gate_connection:connected", **kwargs)

        if action == "import_name":
            project.label = gate_project.title
            project.save()
            return redirect("gate_connection:connected", **kwargs)

        if action == "disconnect":
            gate_project.projects.remove(project)
            return redirect("gate_connection:connect", project=project.pk)

        return None


class ProjectCreateView(HeliportLoginRequiredMixin, View):  # noqa: D101
    def get(self, request, *args, **kwargs):  # noqa: D102
        project = create_empty_project(request)
        return redirect("gate_connection:connect", project=project.pk)


class SearchView(HeliportLoginRequiredMixin, TemplateView):  # noqa: D101
    template_name = "gate_connection/search.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the search results.
        """
        context = super().get_context_data(**kwargs)
        user = self.request.user.heliportuser
        query = self.request.GET.get("q", "")

        gate_connection_results = set()

        if self.request.user.is_staff:
            gate_projects = GateProject.objects
        else:
            gate_projects = GateProject.objects.filter(
                Q(owner=user)
                | Q(co_owners=user)
                | Q(responsible_experimentalist=user)
                | Q(local_contacts=user)
            )

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            gate_connection_results.update(
                gate_projects.filter(
                    Q(gate_id=num)
                    | Q(label__icontains=word)
                    | Q(owner__display_name__icontains=word)
                    | Q(co_owners__display_name__icontains=word)
                    | Q(description__icontains=word)
                    | Q(responsible_experimentalist__display_name__icontains=word)
                    | Q(local_contacts__display_name__icontains=word)
                )
            )

        context["gate_connection_results"] = gate_connection_results
        return context


##########################################
#               REST API                 #
##########################################


class GateProjectViewSet(HeliportModelViewSet):
    """Gate Project."""

    serializer_class = GateProjectSerializer
    filterset_fields = [
        "gate_id",
        "label",
        "owner",
        "co_owners",
        "proposal",
        "restricted",
        "responsible_experimentalist",
        "local_contacts",
        "projects",
        "status",
        "experimentalists",
    ]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return GateProject.objects.filter(is_object_member(user)).distinct()


class GateUserViewSet(viewsets.ModelViewSet):
    """Gate User."""

    serializer_class = GateUserSerializer
    permission_classes = [rest_permissions.IsAuthenticated, StaffOrReadOnly]
    filterset_fields = ["gate_user_id", "gate_id", "heliport_user"]

    def get_queryset(self):  # noqa: D102
        return GateUser.objects.all()
