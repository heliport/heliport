# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Module with special name "interface" hooks into HELIPORT.

Some functions and :class:`heliport.core.app_interaction.Module` subclasses are detected
by HELIPORT and control how HELIPORT uses this app.

Note that this module must be imported in __init__.py of the django app.
"""

from django.urls import reverse

from heliport.core.app_interaction import Module


class GATEConnectionModule(Module):  # noqa: D101
    name = "GATE Connection"
    module_id = "gate_connection"
    icon = "fa-solid fa-clipboard"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        from .models import GateProject

        gate_project = GateProject.objects.filter(projects=project).first()
        if gate_project is None:
            return reverse("gate_connection:connect", kwargs={"project": project.pk})

        return reverse(
            "gate_connection:connected",
            kwargs={"project": project.pk, "pk": gate_project.pk},
        )

    def is_configured(self, project):
        """Return whether the module is configured, i.e. to show it in the graph."""
        from .models import GateProject

        return GateProject.objects.filter(projects=project)


def get_search_url():
    """Return the search URL for this app.

    This URL is used to implement the global HELIPORT string search.
    """
    return reverse("gate_connection:search")


def get_project_create_urls():
    """Return URLs to views in this app that can be used to create a new project."""
    return {"Create Project from GATE": reverse("gate_connection:create_project")}
