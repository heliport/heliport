# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import abc
import logging
import typing

import requests
from django.conf import settings

from heliport.core.user_logic.user_information import LdapUserInformationManager

from .gate_user_management import GateUser
from .ldap_user_management import LDAPUser

logger = logging.getLogger(__name__)


class GateProject:  # noqa: D101
    def __init__(self, main_dict, user_dict):  # noqa: D107
        self.gate_id = main_dict["id"]
        self.title = main_dict["title"]
        self.abstract = main_dict["abstract"]
        self.proposal = main_dict["proposalnumber"]
        self.restricted = main_dict["restricted"] == "true"
        self.status = main_dict.get("status", "no status")
        self.proposer = user_dict["proposer"]
        self.responsible_experimentalist = user_dict["responsible_experimentalist"]
        self.co_proposers = user_dict["coproposers"]
        self.experimentalists = user_dict["experimentalist"]
        self.local_contacts = user_dict["local_contacts"]


class GateQueries:  # noqa: D101
    @staticmethod
    @abc.abstractmethod
    def create_or_update_gate_project(gate_project: GateProject):  # noqa: D102
        pass


class ProposalManager:  # noqa: D101
    session: typing.Optional[requests.Session]

    def __init__(self):  # noqa: D107
        self.session = None

    def __enter__(self):  # noqa: D105
        self.session = requests.Session()
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):  # noqa: D105
        self.session = None

    @abc.abstractmethod
    def download_proposals(self):  # noqa: D102
        pass

    @abc.abstractmethod
    def save_proposal(self, proposal, gate_queries: GateQueries):  # noqa: D102
        pass

    @abc.abstractmethod
    def import_users(self, projects, user_queries, force_update_users=False):  # noqa: D102
        pass


class HZDRGateProposalManager(ProposalManager):  # noqa: D101
    def __init__(self):  # noqa: D107
        super().__init__()

        self.gate_project_url = (
            "https://www.hzdr.de/db/proposalmanagement/proposals/views"
        )
        self.user_relation_url = (
            "https://www.hzdr.de/db/proposalmanagement/proposals/users"
        )
        self.token_url = "https://www.hzdr.de/db/proposalmanagement/oauth/token"
        self.gate_user = settings.HELIPORT_GATE_USER
        self.gate_pass = settings.HELIPORT_GATE_PASS

    def __enter__(self):  # noqa: D105
        self.session = requests.Session()
        self.session.auth = (self.gate_user, self.gate_pass)
        self.retrieve_token()
        return self

    def retrieve_token(self):  # noqa: D102
        # example data:
        # {
        #     "access_token": "V5prXY7o8DidohGe02DXnw",
        #     "token_type": "bearer",
        #     "expires_in": 3600
        # }
        logger.debug("Request bearer token")
        response = self.session.post(
            self.token_url,
            data={"grant_type": "client_credentials"},
        )
        response.raise_for_status()
        data = response.json()
        self.session.headers = {"Authorization": f"Bearer {data['access_token']}"}
        self.session.auth = None

    def get_paged_data(self, url, object_name):  # noqa: D102
        # example data:
        # {
        #    "items": [... ...],
        #    "hasMore": true, "limit": 50, "offset": 0, "count": 50, "links": [
        #        ...
        #        {
        #            "rel": "next",
        #            "href": "https://www.hzdr.de/db/proposalmanagement/proposals/views?offset=50"
        #        }
        #    ]
        # }
        logger.debug(f"Request data for {object_name}")
        has_more = True
        offset = 0
        all_items = []

        while has_more:
            response = self.session.get(url, params={"offset": offset})
            response.raise_for_status()
            data = response.json()

            all_items.extend(data["items"])
            has_more = data["hasMore"]
            offset = data["offset"] + data["count"]
            logger.debug(
                f"Retrieved {offset} items - {'has more' if has_more else 'done'}"
            )

        return all_items

    def insert_user_objects(self, users):  # noqa: D102
        # example users
        # {
        #    'id': 1789,
        #    'proposer': 2000123,
        #    'coproposers': [{'id': 2000456}],
        #    'responsible_experimentalist': 2000123,
        #    'local_contacts': [{'id': 101234}]
        # }
        if "coproposers" not in users:
            logger.error(f"user relation has no coproposers: {users}")
        if "local_contacts" not in users:
            logger.error(f"user relation has no local_contacts: {users}")
        if "experimentalist" not in users:
            logger.error(f"user relation has no experimentalist field: {users}")

        users["proposer"] = GateUser(users["proposer"], self)
        users["responsible_experimentalist"] = GateUser(
            users["responsible_experimentalist"], self
        )
        users["coproposers"] = [
            GateUser(user["id"], self) for user in users["coproposers"]
        ]
        users["experimentalist"] = [
            GateUser(user["id"], self) for user in users["experimentalist"]
        ]
        users["local_contacts"] = [
            LDAPUser(user["id"]) for user in users["local_contacts"]
        ]

    def download_proposals(self):  # noqa: D102
        gate_project_dicts = self.get_paged_data(self.gate_project_url, "gate_projects")
        user_relation = self.get_paged_data(self.user_relation_url, "user_relations")

        flawed_data_count = sum("coproposers" not in users for users in user_relation)
        if flawed_data_count:
            logger.error(f"{flawed_data_count} user-relations have no co-proposers")

        user_index = {
            gate_project["id"]: gate_project for gate_project in user_relation
        }

        gate_projects = []
        for project in gate_project_dicts:
            users = user_index[project["id"]]
            self.insert_user_objects(users)
            gate_projects.append(GateProject(project, users))

        return gate_projects

    def save_proposal(self, gate_project, gate_queries):  # noqa: D102
        gate_queries.create_or_update_gate_project(gate_project)

    def import_users(self, projects, user_queries, force_update_users=False):  # noqa: D102
        ldap_manager = LdapUserInformationManager()
        GateUser.import_users(
            projects, user_queries, self.session, ldap_manager, force_update_users
        )
        LDAPUser.import_users(
            projects, user_queries, self.session, ldap_manager, force_update_users
        )
