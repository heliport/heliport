# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

from heliport.core.user_logic.user_information import LdapUserInformationManager

from .user_management import User, UserQueries


class LDAPUser(User):  # noqa: D101
    @classmethod
    def extract_users(cls, project):  # noqa: D102
        return project.local_contacts

    def __init__(self, ldap_id):  # noqa: D107
        self.ldap_id = str(ldap_id)
        self.name = None
        self.email = None

    def __hash__(self):  # noqa: D105
        return hash(self.ldap_id)

    def __eq__(self, other):  # noqa: D105
        return isinstance(other, LDAPUser) and self.ldap_id == other.ldap_id

    def download(self, session, user_info_manager: LdapUserInformationManager):  # noqa: D102
        db_user = user_info_manager.get_user_by_id(self.ldap_id, force_fetch=True)
        assert db_user is not None, f"invalid ldap info for user {self.ldap_id}"
        self.name = db_user.display_name
        self.email = db_user.email

    def get_db_user_from(self, user_queries: UserQueries):  # noqa: D102
        user = user_queries.get_user_by_ldap_id(self.ldap_id)
        if user is None:
            user = user_queries.create_user()
        return user

    def save_to(self, user_queries: UserQueries):  # noqa: D102
        user = self.get_db_user_from(user_queries)
        user_queries.update_user(
            user, display_name=self.name, ldap_id=self.ldap_id, email=self.email
        )

    def check_new(self, user_queries: UserQueries):  # noqa: D102
        return user_queries.get_user_by_ldap_id(self.ldap_id) is None
