# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later
import logging

from .user_management import User, UserQueries

logger = logging.getLogger(__name__)


class GateUser(User):  # noqa: D101
    @staticmethod
    def extract_users(project):  # noqa: D102
        return [
            project.proposer,
            project.responsible_experimentalist,
            *project.co_proposers,
            *project.experimentalists,
        ]

    def __init__(self, gate_id, gate_manager):  # noqa: D107
        self.gate_manager = gate_manager
        self.gate_id = gate_id
        self.url = "https://www.hzdr.de/db/proposalmanagement/user/details"
        self.ldap_cn = None
        self.name = None
        self.affiliation = None
        self.email = None

    def __hash__(self):  # noqa: D105
        return hash(self.gate_id)

    def __eq__(self, other):  # noqa: D105
        return isinstance(other, GateUser) and self.gate_id == other.gate_id

    def download(self, session, ldap_manager):  # noqa: D102
        # example data
        # {
        #    'id': 2000123,
        #    'geco': 'Dipl.-Ing. Dr. John Doe',
        #    'affiliation': 'Helmholtz-Zentrum Dresden-Rossendorf',
        #    'userdb': 101234,
        #    'userdb_name': 'Doe, Dr. John (OncoRay) - 101234',
        #    'links': [{'rel': 'collection', 'href': 'https://www.hzdr.de/db/proposalmanagement/user/'}]
        # }
        response = session.get(self.url, params={"uid": self.gate_id})
        response.raise_for_status()
        data = response.json()

        alt_name = f"{data['geco']} (extern)"
        self.ldap_cn = data["userdb"]
        self.name = data["userdb_name"] or alt_name
        self.affiliation = data.get("affiliation")

        if self.ldap_cn:
            db_user = ldap_manager.get_user_by_id(str(self.ldap_cn), force_fetch=True)
            if db_user:
                self.email = db_user.email
            else:
                logger.warning(f"user not found {self.ldap_cn=}, {self.gate_id=}")

    def get_db_user_from(self, user_queries: UserQueries):  # noqa: D102
        user = None

        if self.ldap_cn is not None:
            # if user has logged in before having gate project
            # protect if gate id is duplicated (happens in very rare cases)
            user = user_queries.get_user_by_ldap_id_check_gate_duplication(
                ldap_id=self.ldap_cn, gate_id=self.gate_id
            )

        if user is None:
            user = user_queries.get_user_by_gate_id(self.gate_id)

        if user is None:
            user = user_queries.create_user()

        return user

    def save_to(self, user_queries: UserQueries):  # noqa: D102
        user = self.get_db_user_from(user_queries)
        user_queries.update_user(
            user,
            display_name=self.name,
            ldap_id=self.ldap_cn,
            gate_id=self.gate_id,
            affiliation=self.affiliation,
            email=self.email,
        )

    def check_new(self, user_queries: UserQueries):  # noqa: D102
        return user_queries.get_user_by_gate_id(self.gate_id) is None
