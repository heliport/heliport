# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from .hzdr_gate_proposal_management import HZDRGateProposalManager

logger = logging.getLogger(__name__)


class UpdateProposalDatabaseUseCase:
    """A class representing the Gate proposal import process.

    Start the process by using the :meth:`execute` method.

    :param user_queries: The
        :class:`heliport.gate_connection.models.ProposalUserQueries` class.
    :param gate_queries: The
        :class:`heliport.gate_connection.models.ProposalQueries` class.
    :param force_update_users: Also download and update users that already exist.
    """

    def __init__(self, user_queries, gate_queries, force_update_users=False):
        """Initialize."""
        self.user_queries = user_queries
        self.gate_queries = gate_queries
        self.force_update_users = force_update_users

    def execute(self):  # noqa: D102
        with HZDRGateProposalManager() as proposal_manager:
            self.update_gate_database(proposal_manager)

    def update_gate_database(self, proposal_manager):  # noqa: D102
        logger.info("Update GATE data")

        proposal_data = proposal_manager.download_proposals()
        proposal_manager.import_users(
            proposal_data, self.user_queries, self.force_update_users
        )

        logger.info("Write GATE projects to DB")
        for i, project in enumerate(proposal_data):
            proposal_manager.save_proposal(project, self.gate_queries)
            if i % 100 == 0:
                logger.debug(f"{i} / {len(proposal_data)} gate projects updated")

        logger.info(f"{len(proposal_data)} GATE projects imported")
        return len(proposal_data)
