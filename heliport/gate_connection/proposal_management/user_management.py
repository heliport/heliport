# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import abc
import logging

import requests

from heliport.core.user_logic.user_information import LdapUserInformationManager

logger = logging.getLogger(__name__)


class UserQueries:  # noqa: D101
    @staticmethod
    @abc.abstractmethod
    def create_user():  # noqa: D102
        pass

    @staticmethod
    @abc.abstractmethod
    def get_user_by_gate_id(gate_id):  # noqa: D102
        pass

    @staticmethod
    @abc.abstractmethod
    def get_user_by_ldap_id(ldap_id):  # noqa: D102
        pass

    @staticmethod
    @abc.abstractmethod
    def get_user_by_ldap_id_check_gate_duplication(ldap_id, gate_id):  # noqa: D102
        pass

    @staticmethod
    @abc.abstractmethod
    def update_user(
        user, display_name, ldap_id, gate_id=None, affiliation=None, email=None
    ):
        """Update the heliport user with the provided attributes.

        Additionally, the :class:`heliport.gate_connection.models.GateUser` is updated
        if ``gate_id`` is provided.
        """


class User:  # noqa: D101
    @abc.abstractmethod
    def check_new(self, user_queries):  # noqa: D102
        pass

    @abc.abstractmethod
    def save_to(self, user_queries):  # noqa: D102
        pass

    @abc.abstractmethod
    def get_db_user_from(self, user_queries):  # noqa: D102
        pass

    @abc.abstractmethod
    def download(
        self, session: requests.Session, ldap_manager: LdapUserInformationManager
    ):
        """Download all information to update this user later."""

    @staticmethod
    @abc.abstractmethod
    def extract_users(project):  # noqa: D102
        pass

    @classmethod
    def collect_into_set(cls, gate_projects):  # noqa: D102
        result = set()

        for project in gate_projects:
            result.update(cls.extract_users(project))

        return result

    @classmethod
    def import_users(  # noqa: D102
        cls,
        gate_projects,
        user_queries,
        session: requests.Session,
        ldap_manager: LdapUserInformationManager,
        force_update_users=False,
    ):
        logger.info(f"Import new users of class {cls.__name__}")
        users = cls.collect_into_set(gate_projects)
        new_users = [user for user in users if user.check_new(user_queries)]
        users_to_import = users if force_update_users else new_users
        for i, user in enumerate(users_to_import):
            user.download(session, ldap_manager)
            user.save_to(user_queries)
            if i % 50 == 0:
                logger.debug(f"{i} / {len(users_to_import)} users imported")
        logger.info(
            f"{len(users_to_import)} users imported of {len(users)} total users"
        )
