# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Additional settings for this app.

See also `django appconf <https://django-appconf.readthedocs.io>`_
"""

import environ
from appconf import AppConf

env = environ.Env()


class GateConnectionAppConf(AppConf):
    """Settings of the :mod:`heliport.gate_connection` app.

    All settings in this class can be overwritten in settings.py
    """

    USER = env.str("HELIPORT_GATE_USER")
    PASS = env.str("HELIPORT_GATE_PASS")

    class Meta:  # noqa: D106
        prefix = "HELIPORT_GATE"
