# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Admin interfaces for the GATE connection app."""

from django.contrib import admin

from heliport.gate_connection.models import GateProject, GateUser


@admin.register(GateProject)
class GateProjectAdmin(admin.ModelAdmin):
    """Admin page to list GATE projects with a variety of search options."""

    list_display = ["gate_id", "proposal", "status", "responsible_experimentalist"]
    search_fields = [
        "gate_id",
        "proposal",
        "status",
        "responsible_experimentalist__display_name",
        "responsible_experimentalist__orcid",
        "responsible_experimentalist__affiliation",
        "responsible_experimentalist__auth_user__username",
        "responsible_experimentalist__auth_user__first_name",
        "responsible_experimentalist__auth_user__last_name",
        "responsible_experimentalist__auth_user__email",
    ]


@admin.register(GateUser)
class GateUserAdmin(admin.ModelAdmin):
    """Admin page to list GATE users with a variety of search options."""

    list_display = ["gate_user_id", "gate_id", "heliport_user"]
    search_fields = [
        "gate_user_id",
        "gate_id",
        "heliport_user__display_name",
        "heliport_user__orcid",
        "heliport_user__affiliation",
        "heliport_user__auth_user__username",
        "heliport_user__auth_user__first_name",
        "heliport_user__auth_user__last_name",
        "heliport_user__auth_user__email",
    ]
