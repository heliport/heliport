# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Generated by Django 3.1.14 on 2022-02-17 13:23

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("core", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="GateUser",
            fields=[
                ("gate_user_id", models.AutoField(primary_key=True, serialize=False)),
                ("gate_id", models.IntegerField(null=True)),
                (
                    "heliport_user",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="core.heliportuser",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="GateProject",
            fields=[
                (
                    "digitalobject_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        to="core.digitalobject",
                    ),
                ),
                ("gate_id", models.IntegerField(primary_key=True, serialize=False)),
                ("status", models.CharField(max_length=100, null=True)),
                ("proposal", models.CharField(max_length=14)),
                ("restricted", models.BooleanField(default=False)),
                (
                    "experimentalists",
                    models.ManyToManyField(
                        related_name="co_experimentalist_gate_projects",
                        to="core.HeliportUser",
                    ),
                ),
                (
                    "local_contacts",
                    models.ManyToManyField(
                        related_name="local_contact_gate_projects",
                        to="core.HeliportUser",
                    ),
                ),
                (
                    "responsible_experimentalist",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="experimentalist_gate_projects",
                        to="core.heliportuser",
                    ),
                ),
            ],
            bases=("core.digitalobject",),
        ),
    ]
