# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains :class:`django.db.models.Model` classes for Django ORM.

See :ref:`django:topics/db/models:quick example` from Django documentation.
In HELIPORT the :class:`heliport.core.models.DigitalObject` can be subclassed for models
containing metadata in a project.
"""

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q

from heliport.core.models import DigitalObject, HeliportUser

from . import proposal_management
from .proposal_management.hzdr_gate_proposal_management import (
    GateProject as ProposalObject,
)


class GateProject(DigitalObject):  # noqa: D101
    disable_copy = True
    gate_id = models.IntegerField(primary_key=True)
    status = models.CharField(max_length=100, null=True)  # noqa: DJ001
    proposal = models.CharField(max_length=14)
    restricted = models.BooleanField(default=False)
    responsible_experimentalist = models.ForeignKey(
        HeliportUser,
        on_delete=models.SET_NULL,
        null=True,
        related_name="experimentalist_gate_projects",
    )
    experimentalists = models.ManyToManyField(
        HeliportUser, related_name="co_experimentalist_gate_projects"
    )
    local_contacts = models.ManyToManyField(
        HeliportUser, related_name="local_contact_gate_projects"
    )

    def __str__(self):  # noqa: D105
        return str(self.title)

    @property
    def title(self):  # noqa: D102
        return self.label

    @title.setter
    def title(self, value):
        self.label = value

    @property
    def abstract(self):  # noqa: D102
        return self.description

    @abstract.setter
    def abstract(self, value):
        self.description = value

    @property
    def proposer(self):  # noqa: D102
        return self.owner

    @proposer.setter
    def proposer(self, value):
        self.owner = value

    @property
    def co_proposer(self):  # noqa: D102
        return self.co_owners


class GateUser(models.Model):  # noqa: D101
    gate_user_id = models.AutoField(primary_key=True)
    gate_id = models.IntegerField(null=True)
    heliport_user = models.OneToOneField(HeliportUser, on_delete=models.CASCADE)

    def __str__(self):  # noqa: D105
        return f"GateUser {self.gate_id}"


class ProposalUserQueries(proposal_management.UserQueries):  # noqa: D101
    @staticmethod
    def get_user_by_gate_id(gate_id):  # noqa: D102
        return HeliportUser.objects.filter(gateuser__gate_id=gate_id).first()

    @staticmethod
    def update_user(
        user, display_name, ldap_id, gate_id=None, affiliation=None, email=None
    ):
        """Update the heliport user with the provided attributes.

        Additionally, the :class:`GateUser` is updated if ``gate_id`` is provided.
        """
        user.display_name = display_name
        user.authentication_backend_id = ldap_id
        if affiliation is not None:
            user.affiliation = affiliation
        if email is not None:
            user.stored_email = email
        try:
            user.full_clean()
        except ValidationError as e:
            if "stored_email" in e.error_dict:
                user.stored_email = ""
        user.save()

        if gate_id is not None:
            gate_user, created = GateUser.objects.get_or_create(heliport_user=user)
            gate_user.gate_id = gate_id
            gate_user.save()

    @staticmethod
    def get_user_by_ldap_id(ldap_id):  # noqa: D102
        return HeliportUser.objects.filter(authentication_backend_id=ldap_id).first()

    @staticmethod
    def get_user_by_ldap_id_check_gate_duplication(ldap_id, gate_id):  # noqa: D102
        return HeliportUser.objects.filter(
            Q(authentication_backend_id=ldap_id)
            & (Q(gateuser__gate_id=gate_id) | Q(gateuser__gate_id__isnull=True))
        ).first()

    @staticmethod
    def create_user():  # noqa: D102
        return HeliportUser()


class ProposalQueries(proposal_management.GateQueries):  # noqa: D101
    @staticmethod
    def create_or_update_gate_project(gate_project: ProposalObject):  # noqa: D102
        db_obj, is_new = GateProject.objects.get_or_create(gate_id=gate_project.gate_id)
        db_obj.title = gate_project.title or ""
        db_obj.status = gate_project.status
        db_obj.proposer = ProposalUserQueries.get_user_by_gate_id(
            gate_project.proposer.gate_id
        )
        db_obj.abstract = gate_project.abstract or ""
        db_obj.proposal = gate_project.proposal
        db_obj.restricted = gate_project.restricted
        db_obj.responsible_experimentalist = ProposalUserQueries.get_user_by_gate_id(
            gate_project.responsible_experimentalist.gate_id
        )
        db_obj.category_str = settings.PROPOSAL_NAMESPACE
        db_obj.save()
        for user in gate_project.co_proposers:
            user_db_obj = ProposalUserQueries.get_user_by_gate_id(user.gate_id)
            assert user_db_obj is not None
            db_obj.co_proposer.add(user_db_obj)
        for user in gate_project.experimentalists:
            user_db_obj = ProposalUserQueries.get_user_by_gate_id(user.gate_id)
            assert user_db_obj is not None
            db_obj.experimentalists.add(user_db_obj)
        for user in gate_project.local_contacts:
            user_db_obj = ProposalUserQueries.get_user_by_ldap_id(user.ldap_id)
            assert user_db_obj is not None
            db_obj.local_contacts.add(user_db_obj)
