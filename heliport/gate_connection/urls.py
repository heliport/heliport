# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path
from rest_framework import routers

from .views import (
    ConnectedToGateProjectView,
    ConnectToGateProjectView,
    GateProjectViewSet,
    GateUserViewSet,
    ProjectCreateView,
    SearchView,
)

app_name = "gate_connection"
urlpatterns = [
    path(
        "project/<int:project>/gate-connection/list/",
        ConnectToGateProjectView.as_view(),
        name="connect",
    ),
    path(
        "project/<int:project>/gate-connection/<int:pk>/",
        ConnectedToGateProjectView.as_view(),
        name="connected",
    ),
    path("gate-connection/search/", SearchView.as_view(), name="search"),
    path(
        "project/create/gate-connection/",
        ProjectCreateView.as_view(),
        name="create_project",
    ),
]

# REST API
router = routers.DefaultRouter()
router.register(r"gate/projects", GateProjectViewSet, basename="gate_projects")
router.register(r"gate/users", GateUserViewSet, basename="gate_users")
