# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

import json
import logging

from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseNotFound
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils import timezone
from django.utils.dateparse import parse_datetime
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.generic import DetailView, TemplateView, View
from pyunicore.client import Job, Storage, Transport
from pyunicore.credentials import AuthenticationFailedException, UsernamePassword
from requests import HTTPError

from heliport.core.mixins import HeliportObjectMixin, HeliportProjectMixin
from heliport.core.models import DigitalObject, Project
from heliport.core.utils.context import Context
from heliport.core.utils.string_tools import remove_prefix

from .decorators import require_content_type
from .mixins import UnicoreMixin, UnicoreServerAuthenticationRequiredMixin
from .models import DBJob, DBPathDir, DBPathFile, UnicoreClientFor, UnicorePathDirObj

logger = logging.getLogger(__name__)


class JobsView(HeliportProjectMixin, UnicoreMixin, TemplateView):
    """List of the UNICORE jobs that belong to a project.

    :param unicore_job_tag_project: Jobs with this tag are automatically associated with
        this project.
    :type unicore_job_tag_project: str
    """

    template_name = "unicore/jobs.html"

    def __init__(self):  # noqa: D107
        super().__init__()
        self.unicore_job_tag_project = None

    def setup(self, request, *args, **kwargs):  # noqa: D102
        super().setup(request, *args, **kwargs)
        self.unicore_job_tag_project = (
            f"{self.unicore_job_tag_prefix}-{kwargs['project']}"
        )

    def get_context_data(self, **kwargs):
        """Create method context.

        For more info see :meth:`django.views.generic.base.ContextMixin.get_context_data`.
        """  # noqa: E501
        context = super().get_context_data(**kwargs)
        context["unicore_job_tag_project"] = self.unicore_job_tag_project

        if self.unicore_client is None:
            return context

        all_jobs = set(self.unicore_client.get_jobs())
        for job in all_jobs:
            job.properties["submissionTimeDateTime"] = parse_datetime(
                job.properties["submissionTime"]
            )

        tagged_jobs = {
            job
            for job in all_jobs
            if self.unicore_job_tag_project in job.properties["tags"]
        }
        untagged_jobs = all_jobs - tagged_jobs

        project = Project.objects.get(pk=kwargs["project"])
        DBJob.save_job_list(tagged_jobs, project=project)

        context["unicore_project_job_list"] = sorted(
            tagged_jobs,
            key=lambda j: j.properties["submissionTimeDateTime"],
            reverse=True,
        )
        context["unicore_other_job_list"] = sorted(
            untagged_jobs,
            key=lambda j: j.properties["submissionTimeDateTime"],
            reverse=True,
        )

        return context


class JobDetailView(HeliportObjectMixin, UnicoreMixin, DetailView):  # noqa: D101
    model = DBJob
    pk_url_kwarg = "job"
    template_name = "unicore/job.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the unicore client unicore job object.
        """
        context = super().get_context_data(**kwargs)
        job: DBJob = self.get_object()
        unicore_job = Job(self.unicore_transport, job.resource_url)
        unicore_job.properties["pk"] = job.pk
        context["unicore_job"] = unicore_job
        return context


class StorageView(HeliportProjectMixin, UnicoreMixin, DetailView):
    """shows a list of storages and allows creation and editing."""

    model = Project
    pk_url_kwarg = "project"
    template_name = "unicore/storages.html"

    def get_context_data(self, **kwargs):
        """Called by Django to get rendering context; get files and directories."""  # noqa: D401, E501
        context = super().get_context_data(**kwargs)
        user = self.request.user.heliportuser
        project = context["project"]

        dirs = DBPathDir.objects.filter(projects=project, deleted__isnull=True)
        files = DBPathFile.objects.filter(projects=project, deleted__isnull=True)
        context["storages"] = list(dirs) + list(files)
        context["storage_actions"] = [
            s.actions(user, project) for s in context["storages"]
        ]
        context["helper_exists"] = any(s.is_helper for s in context["storages"])
        storage_urls = {s.resource_url for s in context["storages"] if not s.path}

        context["available_storages"] = []
        if self.unicore_client is None:
            storage_objects = []
        else:
            storage_objects = self.unicore_client.get_storages()
        for storage_obj in storage_objects:
            storage = UnicorePathDirObj(
                storage_obj.resource_url, "", self.label_for(storage_obj)
            )
            storage.is_imported = storage.storage_url in storage_urls
            context["available_storages"].append(storage)
        context["available_storages_actions"] = [
            s.actions(user, project) for s in context["available_storages"]
        ]
        context["storages_from_job_available"] = any(
            s.for_job for s in context["available_storages"]
        )

        return context

    def post(self, request, *args, **kwargs):
        """Handle post request; add or delete a storage."""
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        user = self.request.user.heliportuser
        add_url = request.POST.get("add")
        delete = request.POST.get("delete")

        if add_url:
            storage = DBPathDir.objects.create(resource_url=add_url, owner=user)
            with Context(user, project) as context:
                storage_obj = self.from_url(add_url, context)
                storage.label = self.label_for(storage_obj)
                storage.description = storage_obj.properties.get("description")
            storage.save_with_namespace(settings.UNICORE_DIRECTORY_NAMESPACE, project)
        elif delete:
            obj = get_object_or_404(DigitalObject, digital_object_id=delete)
            obj.mark_deleted(user)

        return redirect("unicore:storage-list", project=project.pk)

    def label_for(self, storage):
        """Return a string label to present the ``storage``."""
        return f"{storage.properties['mountPoint']} @ {storage.properties['siteName']}"

    def from_url(self, url, context):
        """Create pyunicore ``Storage`` from url, reusing existing client in context."""
        client = context[UnicoreClientFor(context.user)]
        return Storage(client.transport, url)


class JobActionView(UnicoreMixin, View):
    """View that allows the user to perform some actions on a UNICORE job.

    :param allowed_actions: The actions that can be performed on a job
    :type allowed_actions: List[str]
    """

    def __init__(self):  # noqa: D107
        super().__init__()
        self.allowed_actions = ["abort", "delete", "restart"]

    def get(self, request, job_id, action):
        """Method called on GET request.

        For more info see :meth:`django.views.generic.base.View.dispatch`.
        """  # noqa: D401
        if action not in self.allowed_actions:
            return HttpResponseNotFound()
        logger.debug(f"Action {action!r} requested for UNICORE job {job_id}")
        job = get_object_or_404(DBJob, job_id=job_id)
        if self.unicore_transport is not None:
            getattr(Job(self.unicore_transport, job.resource_url), action)()
        if action == "delete":
            job.deleted = timezone.now()
        return redirect("unicore:jobs", request.GET["project"])


class TestJobView(UnicoreMixin, View):
    """View that, on behalf of the user, sends a job to the cluster.

    This is meant for testing purposes. The job runs on the login node and executes the
    following code:

    .. code-block:: bash

       echo Hello World

    The job will be tagged so that it can be found in the appropriate HELIPORT project.
    """

    def get(self, request):
        """Method called on GET request.

        For more info see :meth:`django.views.generic.base.View.dispatch`.
        """  # noqa: D401
        project = request.GET["project"]

        if self.unicore_client is not None:
            job_description = {
                "Name": "HELIPORT Test Job",
                # Emailing doesn't work in "INTERACTIVE" jobs
                # "User email": request.user.heliportuser.email,
                "Notification": request.build_absolute_uri(reverse("unicore:callback")),
                "Job type": "INTERACTIVE",  # run on login node
                "Executable": "echo",
                "Arguments": ["Hello", "World"],
                "Tags": ["test"],
            }
            job_description["Tags"].append(f"{self.unicore_job_tag_prefix}-{project}")
            self.unicore_client.new_job(job_description)

        return redirect("unicore:jobs", project)


@method_decorator(csrf_exempt, name="dispatch")
@method_decorator(require_POST, name="dispatch")
@method_decorator(require_content_type("application/json"), name="dispatch")
class CallbackView(UnicoreServerAuthenticationRequiredMixin, View):
    """Can be called by UNICORE to notify HELIPORT about a job's status."""

    def post(self, request, *args, **kwargs):
        """Method called on POST request.

        For more info see :meth:`django.views.generic.base.View.dispatch`.

        This method is can be called by a UNICORE instance to give updates about a job's
        status. If the job can be associated with a user and a project, it is imported
        into the HELIPORT database. The user that owns the job is identified by the DN
        stored in the data field of their "UNICORE" :class:`LoginInfo`. The project is
        identified by the job tags. The job is only imported and added to the project if
        the identified user is a member of said project.
        """  # noqa: D401
        user = self.heliport_user or self.unicore_user_dn
        logger.info(
            f"Notification from UNICORE server {self.unicore_server_dn} for {user!r}"
        )

        try:
            data = json.loads(request.body)
            job_url = data["href"]
        except json.decoder.JSONDecodeError:
            logger.debug("Received malformed data")
            return HttpResponseBadRequest()
        except KeyError:
            logger.debug("Received data does not contain 'href' attribute")
            return HttpResponseBadRequest()

        # From here on we always indicate success because the UNICORE server did
        # everything correctly.
        response = HttpResponse(status=204)

        if (
            self.unicore_login is None
            or not self.unicore_login.username
            or not self.unicore_login.password
            or not self.heliport_user
        ):
            return response

        credential = UsernamePassword(
            self.unicore_login.username, self.unicore_login.password
        )
        transport = Transport(credential)

        try:
            job = Job(transport, job_url)
        except (AuthenticationFailedException, HTTPError) as e:
            logger.error(f"Could not authenticate with UNICORE instance: {e}")
            return response

        tags = job.properties.get("tags")
        if tags is None:
            logger.debug(f"Job {job} has no tags")
            return response

        prefix = settings.HELIPORT_UNICORE_JOB_TAG_PREFIX
        # If multiple job tags match the prefix, we use only the first one.
        tag = list(filter(lambda t: t.startswith(prefix), tags))[0]
        project_id_str = remove_prefix(tag, prefix).strip("-")

        try:
            project_id = int(project_id_str)
        except ValueError:
            logger.debug(f"Job {job} has invalid tag {tag!r}")
            return response

        project = Project.objects.filter(pk=project_id).first()
        if project is None:
            logger.debug(f"Project {project_id} does not exist")
            return response

        if self.heliport_user not in project.members():
            logger.debug(
                f"User {self.heliport_user!r} is not a member of project {project!r}"
            )
            return response

        logger.info(f"Adding job {job.job_id} to project {project!r}")
        DBJob.save_job_list([job], project)

        return response
