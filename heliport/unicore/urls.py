# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path, re_path

from .views import (
    CallbackView,
    JobActionView,
    JobDetailView,
    JobsView,
    StorageView,
    TestJobView,
)

app_name = "unicore"
urlpatterns = [
    path("project/<int:project>/unicore/job/list/", JobsView.as_view(), name="jobs"),
    path(
        "project/<int:project>/unicore/job/<int:job>/",
        JobDetailView.as_view(),
        name="job-detail",
    ),
    path(
        "project/<int:project>/unicore/storage/list/",
        StorageView.as_view(),
        name="storage-list",
    ),
    path(
        "unicore/job/<str:job_id>/<str:action>/",
        JobActionView.as_view(),
        name="job-action",
    ),
    re_path("unicore/notify/?", CallbackView.as_view(), name="callback"),
    path("unicore/test-job/", TestJobView.as_view(), name="test-job"),
]
