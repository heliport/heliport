# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Module with special name "interface" hooks into HELIPORT.

Some functions and :class:`heliport.core.app_interaction.Module` subclasses are detected
by HELIPORT and control how HELIPORT uses this app.

Note that this module must be imported in __init__.py of the django app.
"""

from django.urls import reverse

from heliport.core.app_interaction import DigitalObjectModule


class UnicoreJobs(DigitalObjectModule):  # noqa: D101
    name = "UNICORE Jobs"
    module_id = "unicore_jobs"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("unicore:jobs", kwargs={"project": project.pk})

    @property
    def object_class(self):
        """DBJob."""
        from .models import DBJob

        return DBJob


class UnicoreStorages(DigitalObjectModule):  # noqa: D101
    name = "UNICORE Storages"
    module_id = "unicore_storages"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("unicore:storage-list", kwargs={"project": project.pk})

    @property
    def object_class(self):
        """DBPathDir and DBPathFile."""
        from .models import DBPathDir, DBPathFile

        return DBPathDir, DBPathFile
