# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Additional settings for this app.

See also `django appconf <https://django-appconf.readthedocs.io>`_
"""

import environ
from appconf import AppConf
from cryptography.exceptions import UnsupportedAlgorithm
from cryptography.hazmat.primitives.serialization import load_pem_public_key
from django.core.exceptions import ImproperlyConfigured

env = environ.Env()


class UnicoreAppConf(AppConf):
    """Settings of the :mod:`heliport.unicore` app.

    All settings in this class can be overwritten in settings.py
    """

    JOB_TAG_PREFIX = env.str("HELIPORT_UNICORE_JOB_TAG_PREFIX", "heliport-project")
    REST_CORE_URL = env.str("HELIPORT_UNICORE_REST_CORE_URL")
    if REST_CORE_URL == "":
        raise ImproperlyConfigured("HELIPORT_UNICORE_REST_CORE_URL must not be empty")
    SERVER_DN = env.str("HELIPORT_UNICORE_SERVER_DN")
    if SERVER_DN == "":
        raise ImproperlyConfigured("HELIPORT_UNICORE_SERVER_DN must not be empty")
    SERVER_PUBLIC_KEY = env.str("HELIPORT_UNICORE_SERVER_PUBLIC_KEY", multiline=True)
    if SERVER_PUBLIC_KEY == "":
        raise ImproperlyConfigured(
            "HELIPORT_UNICORE_SERVER_PUBLIC_KEY must not be empty"
        )
    try:
        load_pem_public_key(SERVER_PUBLIC_KEY.encode())
    except (ValueError, UnsupportedAlgorithm):
        raise ImproperlyConfigured(
            "HELIPORT_UNICORE_SERVER_PUBLIC_KEY is invalid"
        ) from None

    class Meta:  # noqa: D106
        prefix = "HELIPORT_UNICORE"
