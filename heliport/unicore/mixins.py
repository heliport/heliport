# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

import jwt
from django.conf import settings
from django.contrib import messages
from django.http import HttpResponseForbidden
from django.urls import reverse
from pyunicore.client import Client, Transport
from pyunicore.credentials import AuthenticationFailedException, UsernamePassword
from requests import HTTPError

from heliport.core.models import LoginInfo
from heliport.core.utils.string_tools import remove_prefix

logger = logging.getLogger(__name__)


class UnicoreMixin:
    """Mixin that provides common UNICORE functionality for class-based views.

    :param unicore_login: Login used for interaction with the UNICORE/X server
    :type unicore_login: heliport.core.models.LoginInfo
    :param unicore_transport: Transport layer for ``client``
    :type unicore_transport: pyunicore.client.Transport
    :param unicore_client: UNICORE client
    :type unicore_client: pyunicore.client.Client
    :param unicore_user_dn: The DN of the UNICORE user as returned by UNICORE
    :type uncicore_user_dn: Optional[str]
    :param unicore_job_tag_prefix: Prefix for UNICORE job tags that HELIPORT is supposed
        to pick up.
    :type unicore_job_tag_prefix: str
    """

    def __init__(self):  # noqa: D107
        super().__init__()
        self.unicore_login = None
        self.unicore_transport = None
        self.unicore_client = None
        self.unicore_user_dn = None
        self.unicore_job_tag_prefix = None
        self.unicore_authentication_failed = False

    def setup(self, request, *args, **kwargs):
        """Initializes the view.

        For more info see :meth:`django.views.generic.base.View.setup`.
        """  # noqa: D401
        super().setup(request, *args, **kwargs)
        self.unicore_login = LoginInfo.objects.filter(
            user=request.user.heliportuser, name="UNICORE"
        ).first()
        if (
            self.unicore_login is not None
            and self.unicore_login.username
            and self.unicore_login.password
        ):
            credential = UsernamePassword(
                self.unicore_login.username, self.unicore_login.password
            )
            self.unicore_transport = Transport(credential)
            try:
                self.unicore_client = Client(
                    self.unicore_transport, settings.HELIPORT_UNICORE_REST_CORE_URL
                )
            except AuthenticationFailedException:
                self.unicore_authentication_failed = True
            except HTTPError as e:
                messages.error(request, f"UNICORE server returned error: {e}")

        if self.unicore_client is not None:
            self.unicore_user_dn = self.unicore_client.access_info().get("dn")

        # Save the user DN in the LoginInfo object so that we can use it later to find
        # the correct LoginInfo when the UNICORE server sends delegation requests.
        if (
            self.unicore_user_dn is not None
            and self.unicore_user_dn != self.unicore_login.data
        ):
            self.unicore_login.data = self.unicore_user_dn
            self.unicore_login.save()

        self.unicore_job_tag_prefix = settings.HELIPORT_UNICORE_JOB_TAG_PREFIX

    def get_context_data(self, **kwargs):
        """Create method context.

        For more info see :meth:`django.views.generic.base.ContextMixin.get_context_data`.
        """  # noqa: E501
        context = super().get_context_data(**kwargs)
        # We'll put this URL in the context because it is needlessly complicated to get
        # an absolute URL with reverse from within a template ...
        context["unicore_callback_url"] = self.request.build_absolute_uri(
            reverse("unicore:callback")
        )
        context["unicore_rest_core_url"] = settings.HELIPORT_UNICORE_REST_CORE_URL
        context["unicore_has_login"] = self.unicore_login is not None
        context["unicore_job_tag_prefix"] = self.unicore_job_tag_prefix
        context["unicore_authentication_failed"] = self.unicore_authentication_failed
        return context


class UnicoreServerAuthenticationRequiredMixin:
    """Mixin for views that require authentication from the UNICORE server.

    The UNICORE server authenticates by sending a JWT in the Authorization header. This
    token is signed with the UNICORE server certificate and can be validated using its
    public key. This mixin reads the claims from the signed token and adds the UNICORE
    server DN and UNICORE user DN (as given by the issuer and subject claim
    respectively) to the view.

    This mixin should not be used together with the :class:`UnicoreMixin` as both of
    them try to provide member variables that can be used to interact with a UNICORE
    server on behalf of the user.

    :param unicore_server_is_authenticated: Whether the UNICORE server authentication
        was successful.
    :type unicore_server_is_authenticated: bool
    :param unicore_server_dn: The DN of the server as given by the "iss" claim.
    :type unicore_server_dn: Optional[str]
    :param unicore_user_dn: The DN of the UNICORE user as given by the "sub" claim.
    :type unicore_user_dn: Optional[str]
    :param unicore_login: The login of the UNICORE user for whom this request was sent
    :type unicore_login: :class:`LoginInfo`
    :param heliport_user: The HELIPORT user associated with the UNICORE user for whom
        this request was sent
    :type heliport_user: heliport.core.user.HeliportUser
    """

    def __init__(self, *args, **kwargs):  # noqa: D107
        super().__init__(*args, **kwargs)
        self.unicore_server_is_authenticated = False
        self.unicore_server_dn = None
        self.unicore_user_dn = None
        self.unicore_login = None
        self.heliport_user = None

    def setup(self, request, *args, **kwargs):  # noqa: D102
        super().setup(request, *args, **kwargs)

        auth_header = request.headers.get("Authorization")
        if auth_header is None or not auth_header.startswith("Bearer "):
            logger.debug(f"Unauthorized party requested {request.get_full_path()}")
            return

        token = remove_prefix(auth_header, "Bearer ")
        decode_options = {
            "require": ["iss", "sub", "etd", "exp", "iat"],
            "verify_signature": True,
            "verify_iss": True,
            "verify_exp": True,
            "verify_iat": True,
        }

        try:
            claims = jwt.decode(
                token,
                settings.HELIPORT_UNICORE_SERVER_PUBLIC_KEY,
                algorithms=["RS256"],
                issuer=settings.HELIPORT_UNICORE_SERVER_DN,
                options=decode_options,
            )
            # UNICORE sets "etd" to "true" for delegation tokens
            if claims["etd"] != "true":
                raise jwt.exceptions.InvalidTokenError()
        except jwt.exceptions.InvalidTokenError:
            logger.debug(f"Invalid token for {request.get_full_path()}")
            return

        self.unicore_server_is_authenticated = True
        self.unicore_server_dn = claims["iss"]
        self.unicore_user_dn = claims["sub"]

        # TODO: Multiple users may have registered a LoginInfo for this user DN (e.g.
        # because the UNICORE user is a special system user). What to do in this case?
        self.unicore_login = LoginInfo.objects.filter(data=self.unicore_user_dn).first()
        if self.unicore_login is not None:
            self.heliport_user = self.unicore_login.user

    def dispatch(self, request, *args, **kwargs):  # noqa: D102
        if not self.unicore_server_is_authenticated:
            return HttpResponseForbidden()

        return super().dispatch(request, *args, **kwargs)
