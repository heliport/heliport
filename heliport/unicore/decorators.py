# noqa: D100
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from django.http import HttpResponse

logger = logging.getLogger(__name__)


def require_content_type(content_type):
    """Decorator factory to ensure a request uses the correct Content-Type."""  # noqa: D401, E501

    def decorator(func):
        """The actual decorator."""  # noqa: D401

        def wrapper(request, *args, **kwargs):
            if request.method == "POST":
                actual_content_type = request.headers.get("Content-Type")
                if actual_content_type != content_type:
                    logger.debug(
                        f"{request.get_full_path()} called with wrong Content-Type "
                        f"{actual_content_type} instead of {content_type}"
                    )
                    return HttpResponse(status=415, headers={"Accept": content_type})

            return func(request, *args, **kwargs)

        return wrapper

    return decorator
