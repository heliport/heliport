# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains :class:`django.db.models.Model` classes for Django ORM.

See :ref:`django:topics/db/models:quick example` from Django documentation.
In HELIPORT the :class:`heliport.core.models.DigitalObject` can be subclassed for models
containing metadata in a project.
"""

from abc import ABC, abstractmethod
from dataclasses import dataclass
from mimetypes import guess_type
from pathlib import Path
from typing import Any, Dict, Iterable, Optional

from django.conf import settings
from django.db import models
from pyunicore.client import Client, Job, PathDir, PathFile, Storage, Transport
from pyunicore.credentials import AuthenticationFailedException, UsernamePassword

from heliport.core.digital_object_aspects import (
    Directory,
    DirectoryObj,
    File,
    FileObj,
    FileOrDirectory,
)
from heliport.core.digital_object_interface import GeneralDigitalObject
from heliport.core.models import DigitalObject, HeliportUser, LoginInfo, Project
from heliport.core.permissions import allowed_objects
from heliport.core.utils.collections import RootedRDFGraph
from heliport.core.utils.context import Context
from heliport.core.utils.exceptions import LoginInfoMissing, UserMessage
from heliport.core.utils.string_tools import format_byte_count, format_time


class UnicoreResource(models.Model):
    """Base class for UNICORE Resources as Digital Objects.

    ``UnicoreResource`` can not inherit from DigitalObject because DigitalObjects don't
    support subclasses of subclasses yet.
    """

    unicore_resource_id = models.AutoField(primary_key=True)
    resource_url = models.TextField()

    class Meta:  # noqa: D106
        abstract = True


class DBJob(UnicoreResource, DigitalObject):
    """Django model for UNICORE jobs."""

    job_id = models.TextField()

    # TODO: Potentially make a celery task out of this so that it can also be executed
    # in the background.
    @classmethod
    def save_job_list(cls, jobs: Iterable[Job], project: Project = None):
        """Store a list of :class:`Job` s in the database as digital objects.

        Optionally, if a :class:`Project` is given, add the digital objects to this
        project.
        """
        for job in jobs:
            job_obj, created = cls.objects.get_or_create(
                job_id=job.job_id,
                resource_url=job.resource_url,
                defaults={"label": job.properties.get("name", "")},
            )
            if project is not None and created:
                # TODO: Also add the UNICORE storage associated with this job as a
                # datasource.
                job_obj.projects.add(project)
            job.properties["pk"] = job_obj.pk


class DBPathFile(UnicoreResource, DigitalObject, FileObj):
    """Django model for file in unicore storage."""

    path = models.TextField(blank=True, default="")

    def as_file(self, context: Context) -> File:
        """This makes it a file in HELIPORT. Returns a :class:`UnicoreFile`."""  # noqa: D401, E501
        return UnicoreFile(self.resource_url, self.path, context)


class DBPathDir(UnicoreResource, DigitalObject, DirectoryObj):
    """Django model for directory in unicore storage."""

    path = models.TextField(blank=True, default="")

    def as_directory(self, context: Context) -> Directory:
        """This makes it a directory in HELIPORT. Returns a :class:`UnicoreDirectory`."""  # noqa: D401, E501
        return UnicoreDirectory(self.resource_url, self.path, context)


# === EXTERNAL RESOURCES ===
class UnicorePathObj(GeneralDigitalObject, ABC):
    """Abstract base class for files and directory in unicore storages not stored in DB."""  # noqa: E501

    def __init__(self, storage_url: str, path: str, label: str = None):  # noqa: D107
        self.storage_url = storage_url
        self.path = path
        self.label = label

    @property
    def for_job(self):
        """Determine if this file or directory is created for a specific UNICORE job."""
        return "-uspace" in self.storage_url

    def as_text(self) -> str:
        """
        Represent by label or filename if no label provided. So normally label is
        unnecessary. But storages itself have no file name (path is "").
        """  # noqa: D205
        if self.label is not None:
            return self.label
        file_name = Path(self.path).name
        if file_name:
            return file_name
        return "UNICORE Storage"

    def as_rdf(self) -> RootedRDFGraph:
        """Dummy implementation that represents this as string."""  # noqa: D401
        return RootedRDFGraph.from_atomic(self.as_text())

    def as_html(self) -> str:
        """Represent just as string in html."""
        return self.as_text()

    def as_tuple(self):
        """For ``__hash__`` and ``__eq__`` implementation."""
        return self.storage_url, self.path

    def __hash__(self):
        """Hashable."""
        return hash(self.as_tuple())

    def __eq__(self, other):
        """Comparable."""  # noqa: D401
        return isinstance(other, type(self)) and self.as_tuple() == other.as_tuple()

    def get_identifying_params(self) -> Dict[str, str]:
        """Parameters which can be used to resolve this instance."""
        return {
            "type": self.type_id(),
            "storage_url": self.storage_url,
            "path": self.path,
        }

    @classmethod
    def resolve(
        cls, params: Dict[str, str], context: Context
    ) -> Optional[GeneralDigitalObject]:
        """Resolve an instance by its identifying params
        (:meth:`get_identifying_params`).

        Tries to return something in the database before returning something not in DB.
        """  # noqa: D205
        assert params.get("type") == cls.type_id()
        storage_url = params.get("storage_url")
        path = params.get("path")
        if storage_url is None or path is None:
            return None

        digital_object = cls.resolve_digital_object(
            storage_url, path, context.user_or_none
        )
        if digital_object is not None:
            return digital_object

        return cls(storage_url, path)

    @staticmethod
    @abstractmethod
    def resolve_digital_object(
        storage_url: str, path: str, user: Optional[HeliportUser]
    ) -> Optional[Any]:
        """Implement this in subclass to handle resolution to object in database."""


class UnicorePathFileObj(UnicorePathObj, FileObj):
    """Represents a file in unicore storage that is not in the HELIPORT database."""

    @staticmethod
    def type_id() -> str:
        """Used in resolution to identify this type."""  # noqa: D401
        return "UnicorePathFile"

    @staticmethod
    def resolve_digital_object(
        storage_url: str, path: str, user: Optional[HeliportUser]
    ) -> Optional[DBPathFile]:
        """Try to find a matching :class:`DBPAthFile` instance."""
        user_dirs = allowed_objects(user, DBPathFile)
        return user_dirs.filter(
            resource_url=storage_url, path=path, deleted__isnull=True
        ).first()

    def as_digital_object(self, context: Context) -> DigitalObject:
        """Return this as a :class:`DBPathFile` in the database (create if none exists)."""  # noqa: E501
        obj = self.resolve_digital_object(
            self.storage_url, self.path, context.user_or_none
        )
        if obj is not None:
            return obj

        obj = DBPathFile(
            resource_url=self.storage_url,
            path=self.path,
            owner=context.user_or_none,
            label=str(self),
        )
        obj.category_str = settings.UNICORE_FILE_NAMESPACE
        obj.save()
        return obj

    def as_file(self, context: Context) -> File:
        """Makes this an object representing a file; returns a :class:`UnicoreFile`."""  # noqa: D401, E501
        return UnicoreFile(self.storage_url, self.path, context)


class UnicorePathDirObj(UnicorePathObj, DirectoryObj):
    """Represent a UNICORE directory that is not in the HELIPORT database."""

    @staticmethod
    def type_id() -> str:
        """Used to identify this type during resolution."""  # noqa: D401
        return "UnicorePathDir"

    @staticmethod
    def resolve_digital_object(
        storage_url: str, path: str, user: Optional[HeliportUser]
    ) -> Optional[DBPathDir]:
        """Try to get a :class:`DBPathDir` version of this directory."""
        user_dirs = allowed_objects(user, DBPathDir)
        return user_dirs.filter(
            resource_url=storage_url, path=path, deleted__isnull=True
        ).first()

    def as_digital_object(self, context: Context) -> DigitalObject:
        """Return this as a :class:`DBPathDir` in the database (create if none exists)."""  # noqa: E501
        obj = self.resolve_digital_object(
            self.storage_url, self.path, context.user_or_none
        )
        if obj is not None:
            return obj

        obj = DBPathDir(
            resource_url=self.storage_url,
            path=self.path,
            owner=context.user_or_none,
            label=str(self),
        )
        obj.category_str = settings.UNICORE_DIRECTORY_NAMESPACE
        obj.save()
        return obj

    def as_directory(self, context: Context) -> Directory:
        """Makes this usable as a directory in HELIPORT; returns :class:`UnicoreDirectory`."""  # noqa: D401, E501
        return UnicoreDirectory(self.storage_url, self.path, context)


# === BASIC CLASSES ===


class UnicorePath(FileOrDirectory, ABC):
    """Base class for paths inside a UNICORE storage."""

    #: The unicore api url for a storage that contains the :attr:`path`
    storage_url: str
    #: The path of this file in the storage returned by :meth:`get_storage`
    path: str
    #: Context to cache the connection and stat infos
    context: Context

    def __init__(self, storage_url: str, path: str, context: Context):  # noqa: D107
        self.storage_url = storage_url
        self.path = path
        self.context = context

    def get_client(self) -> Client:
        """Get or create a UNICORE client from the current context."""
        return self.context[UnicoreClientFor(self.context.user)]

    def get_storage(self) -> Storage:
        """The UNICORE storage in which this file can be found under :attr:`path`."""  # noqa: D401, E501
        client = self.get_client()
        return Storage(client.transport, self.storage_url)

    @property
    def path_obj(self):
        """The pathlib path of :attr:`path`."""
        return Path(self.path)


class UnicoreFile(UnicorePath, File):
    """An actual readable UNICORE file."""

    def __init__(  # noqa: D107
        self,
        storage_url: str,
        path: str,
        context: Context,
    ):
        super().__init__(storage_url, path, context)
        self.file_response = None
        self.stream_generator = None

    def get_path_file(self) -> PathFile:
        """Get pyunicore ``PathFile`` of this object."""
        result = self.get_storage().stat(self.path)
        if not isinstance(result, PathFile):
            raise UserMessage("The ID did not refer to a File")
        return result

    def mimetype(self) -> str:
        """Guess mimetype from file name."""
        mimetype, encoding = guess_type(self.path)
        if mimetype is None:
            mimetype = "application/octet-stream"  # just binary data
        return mimetype

    def read(self, number_of_bytes=None) -> bytes:
        """Read bytes from this file; needs to be :meth:`open`."""
        assert self.file_response is not None, "please open file before reading"
        if self.stream_generator is None:
            self.stream_generator = self.file_response.iter_content(number_of_bytes)
        try:
            return next(self.stream_generator)
        except StopIteration:
            return b""

    def open(self):
        """Opens the file for reading bytes; :meth:`close` this afterwards."""  # noqa: D401, E501
        if self.file_response is not None:
            self.close()

        file = self.get_path_file()
        self.file_response = file.transport.get(
            url=file.resource_url,
            headers={"Accept": "application/octet-stream"},
            stream=True,
            to_json=False,
        )

    def close(self):
        """Closes file if open; you can also use ``with`` block instead of calling this."""  # noqa: D401, E501
        if self.file_response is not None:
            self.file_response.close()
        self.file_response = None
        self.stream_generator = None

    def get_file_info(self) -> Dict[str, str]:
        """Get the size and last accessed date for the user to see (if available)."""
        file = self.get_path_file()
        result = {}
        size = file.properties.get("size")
        last_accessed = file.properties.get("lastAccessed")
        if size:
            result["size"] = format_byte_count(size)
        if last_accessed:
            result["last accessed"] = format_time(last_accessed)
        return result

    def size(self) -> Optional[int]:
        """File size in bytes."""
        file = self.get_path_file()
        return file.properties.get("size")


class UnicoreDirectory(UnicorePath, Directory):
    """The actual directory with :meth:`get_parts` implemented."""

    def get_path_dir(self) -> PathDir:
        """Pyunicore ``PathDir`` of this directory."""
        result = self.get_storage().stat(self.path)
        if not isinstance(result, PathDir):
            raise UserMessage("The id did not refer to a Directory")
        return result

    def get_parts(self) -> Iterable[FileOrDirectory]:
        """
        Get the contents of this directory as :class:`UnicorePathDirObj` or
        :class:`UnicorePathFileObj`. Ignores hidden files and directories.
        """  # noqa: D205
        storage = self.get_storage()
        for file in storage.listdir(self.path).values():
            if Path(file.name).name.startswith("."):
                # remove hidden files
                # also a file name like ..xyz results in an error in UNICORE
                continue
            if file.isdir():
                yield UnicorePathDirObj(self.storage_url, file.name)
            else:
                yield UnicorePathFileObj(self.storage_url, file.name)

    def get_file_info(self) -> Dict[str, str]:
        """Get last access date for the user to see."""
        file = self.get_path_dir()

        result = {}
        last_accessed = file.properties.get("lastAccessed")
        if last_accessed:
            result["last accessed"] = format_time(last_accessed)
        return result


def unicore_client(user: HeliportUser) -> Client:
    """Create a pyunicore ``Client`` for the given user."""
    login = LoginInfo.objects.filter(user=user, name="UNICORE").first()
    if login is None or not login.username or not login.password:
        raise LoginInfoMissing(
            "You have no LoginInfo with name UNICORE and username and password"
        )
    credential = UsernamePassword(login.username, login.password)
    transport = Transport(credential)
    try:
        return Client(transport, settings.HELIPORT_UNICORE_REST_CORE_URL)
    except AuthenticationFailedException:
        raise UserMessage(
            "Authentication using your UNICORE LoginInfo Failed. "
            "LoginInfos can be edited under user > settings."
        ) from None


@dataclass(frozen=True)
class UnicoreClientFor:
    """wraps :func:`unicore_client` in a :class:`heliport.core.utils.context.Description`."""  # noqa: E501

    user: HeliportUser

    def generate(self, context):  # noqa: D102
        return unicore_client(self.user)
