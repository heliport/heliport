# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

import datetime
import json

import pytest
from cryptography import x509
from cryptography.hazmat.primitives import asymmetric, hashes, serialization
from django.test import TestCase, override_settings
from django.urls import reverse

from heliport.core.tests import WithProject

jwt = pytest.importorskip("jwt")
models = pytest.importorskip("heliport.unicore.models")


class TestCallbackView(TestCase):
    """Tests for :class:`unicore.views.CallbackView`."""

    path = "/unicore/notify/"

    def __init__(self, *args, **kwargs):  # noqa: D107
        super().__init__(*args, **kwargs)
        self.now = datetime.datetime.now(tz=datetime.timezone.utc)

        key = asymmetric.rsa.generate_private_key(public_exponent=65537, key_size=2048)

        self.private_key = key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption(),
        ).decode()

        self.public_key = (
            key.public_key()
            .public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo,
            )
            .decode()
        )

        subject = issuer = x509.Name(
            [
                x509.NameAttribute(x509.oid.NameOID.COUNTRY_NAME, "DE"),
                x509.NameAttribute(x509.oid.NameOID.STATE_OR_PROVINCE_NAME, "Saxony"),
                x509.NameAttribute(x509.oid.NameOID.LOCALITY_NAME, "Dresden"),
                x509.NameAttribute(x509.oid.NameOID.ORGANIZATION_NAME, "foo"),
                x509.NameAttribute(x509.oid.NameOID.COMMON_NAME, "unicore.foo.de"),
            ]
        )

        self.issuer = subject.rfc4514_string()

        certificate = (
            x509.CertificateBuilder()
            .subject_name(subject)
            .issuer_name(issuer)
            .public_key(key.public_key())
            .serial_number(x509.random_serial_number())
            .not_valid_before(self.now)
            .not_valid_after(self.now + datetime.timedelta(days=1))
            .add_extension(
                x509.SubjectAlternativeName([x509.DNSName("localhost")]),
                critical=False,
            )
            .sign(key, hashes.SHA256())
        )

        self.certificate = certificate.public_bytes(
            encoding=serialization.Encoding.PEM
        ).decode()

    @property
    def default_claims(self):  # noqa: D102
        return {
            "iss": self.issuer,
            "sub": "uid=johndoe",
            "etd": "true",  # string!
            "exp": self.now + datetime.timedelta(minutes=10),
            "iat": self.now,
        }

    def post_notification(self, claims=None, path=None, data=None):
        """POST a notification to the endpoint."""
        if claims is None:
            claims = self.default_claims

        if path is None:
            path = self.path

        if data is None:
            data = {
                "href": "https://unicore.foo.de/HPC-SITE/rest/core/jobs/6630bd76-54de-4f9d-95bb-901d6158e557"
            }

        token = jwt.encode(claims, self.private_key, algorithm="RS256")
        with override_settings(
            HELIPORT_UNICORE_SERVER_DN=self.issuer,
            HELIPORT_UNICORE_SERVER_PUBLIC_KEY=self.public_key,
        ):
            return self.client.post(
                path,
                content_type="application/json",
                data=data if isinstance(data, str) else json.dumps(data),
                headers={"Authorization": f"Bearer {token}"},
            )

    def test_not_post(self):
        """All request methods except POST should receive HTTP 405.

        CONNECT is not tested as it doesn't seem to be supported by the test client.
        """
        methods = ["get", "head", "put", "delete", "options", "trace", "patch"]
        for method in methods:
            response = getattr(self.client, method)(self.path)
            self.assertEqual(response.status_code, 405)

    def test_wrong_content_type(self):
        """Requests with the wrong Content-Type should receive HTTP 415."""
        response = self.client.post(self.path, content_type="text/plain", data="foobar")
        self.assertEqual(response.status_code, 415)
        self.assertIn("Accept", response.headers)
        self.assertEqual(response.headers["Accept"], "application/json")

    def test_no_auth(self):
        """Requests without Authorization header should receive HTTP 403."""
        response = self.client.post(
            self.path, content_type="application/json", data=json.dumps({})
        )
        self.assertEqual(response.status_code, 403)

    def test_wrong_auth_method(self):
        """Requests using the wrong authentication method should receive HTTP 403."""
        response = self.client.post(
            self.path,
            content_type="application/json",
            data=json.dumps({}),
            headers={"Authorization": "Basic X"},
        )
        self.assertEqual(response.status_code, 403)

    def test_wrong_auth_token(self):
        """Requests using a token which can't be decoded should receive HTTP 403."""
        response = self.client.post(
            self.path,
            content_type="application/json",
            data=json.dumps({}),
            headers={"Authorization": "Bearer X"},
        )
        self.assertEqual(response.status_code, 403)

    def test_missing_jwt_claims(self):
        """Requests with missing required JWT claims should receive HTTP 403."""
        claims = {"iss": self.issuer}
        response = self.post_notification(claims=claims)
        self.assertEqual(response.status_code, 403)

    def test_expired_token(self):
        """Requests using an expired token should receive HTTP 403."""
        claims = self.default_claims
        claims["exp"] = self.now - datetime.timedelta(seconds=10)
        claims["iat"] = self.now - datetime.timedelta(seconds=20)
        response = self.post_notification(claims=claims)
        self.assertEqual(response.status_code, 403)

    def test_as_by_unicore(self):
        """Correct requests as UNICORE would do them should receive HTTP 204."""
        response = self.post_notification()
        self.assertEqual(response.status_code, 204)

    def test_missing_slash(self):
        """Posting to the correct path but with the trailing slash missing must work, too."""  # noqa: E501
        response = self.post_notification(path=self.path.rstrip("/"))
        self.assertEqual(response.status_code, 204)

    def test_malformed_data(self):
        """Requests sending malformed JSON data should receive HTTP 400."""
        response = self.post_notification(data="{}}")
        self.assertEqual(response.status_code, 400)

    def test_data_missing_href(self):
        """Requests sending data without "href" field should receive HTTP 400."""
        response = self.post_notification(data={})
        self.assertEqual(response.status_code, 400)


class StorageTests(WithProject):  # noqa: D101
    def test_list(self):  # noqa: D102
        file = models.DBPathFile.objects.create()
        directory = models.DBPathDir.objects.create()
        file.projects.add(self.project)
        directory.projects.add(self.project)

        response = self.client.get(
            reverse("unicore:storage-list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(200, response.status_code)
        test_file = models.DBPathFile.objects.get(pk=file.pk)
        test_directory = models.DBPathDir.objects.get(pk=directory.pk)
        self.assertEqual(file, test_file)
        self.assertEqual(directory, test_directory)
        self.assertIn(response.context_data["project"], file.projects.all())
        self.assertIn(file, response.context_data["storages"])
        self.assertIn(directory, response.context_data["storages"])

    def test_delete(self):  # noqa: D102
        directory = models.DBPathDir.objects.create()
        directory.projects.add(self.project)
        response = self.client.post(
            reverse("unicore:storage-list", kwargs={"project": self.project.pk}),
            data={"delete": directory.digital_object_id},
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        test_obj = models.DBPathDir.objects.filter(
            pk=directory.pk, deleted__isnull=True
        ).first()
        self.assertIsNone(test_obj)
