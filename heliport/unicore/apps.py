# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Django app configuration.

Some HELIPORT hooks can be registered in app config :meth:`django.apps.AppConfig.ready`.

See also :ref:`Django documentation <django:ref/applications:applications>`
"""

from django.apps import AppConfig

from heliport.core.digital_object_resolution import object_types


class UnicoreConfig(AppConfig):
    """App configuration for the unicore app."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "heliport.unicore"

    def ready(self):
        """Register object types and import settings."""
        from .conf import UnicoreAppConf
        from .models import UnicorePathDirObj, UnicorePathFileObj

        assert UnicoreAppConf

        object_types.register(UnicorePathFileObj)
        object_types.register(UnicorePathDirObj)
