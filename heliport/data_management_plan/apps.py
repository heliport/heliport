# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Django app configuration.

Some HELIPORT hooks can be registered in app config :meth:`django.apps.AppConfig.ready`.

See also :ref:`Django documentation <django:ref/applications:applications>`
"""

from django.apps import AppConfig


class DataManagementPlanConfig(AppConfig):
    """App configuration for data management plan app."""

    name = "heliport.data_management_plan"
