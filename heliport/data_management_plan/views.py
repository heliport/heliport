# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains Django View classes to handle HTTP requests.

See :ref:`django:topics/class-based-views/intro:using class-based views` from Django
documentation.
In HELIPORT :mod:`heliport.core.mixins` are used to create uniform views. Also
:class:`heliport.core.views.generic.HeliportObjectListView` is used to quicly create a
typical HELIPORT list view.
"""

import django_filters
from django.conf import settings
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import CreateView, TemplateView, UpdateView
from rest_framework import filters as rest_filters

from heliport.core.mixins import (
    HeliportLoginRequiredMixin,
    HeliportObjectMixin,
    HeliportProjectMixin,
)
from heliport.core.models import Project
from heliport.core.permissions import is_object_member
from heliport.core.views import HeliportModelViewSet

from .models import DataManagementPlan
from .serializers import DataManagementPlanSerializer


class DataManagementPlanView(HeliportProjectMixin, CreateView):  # noqa: D101
    model = DataManagementPlan
    template_name = "data_management_plan/data_management_plan.html"
    fields = ["description"]

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the DMPs to show.
        """
        context = super().get_context_data(**kwargs)
        context["plans"] = DataManagementPlan.objects.filter(
            projects=context["project"], deleted__isnull=True
        )
        context["update"] = False
        return context

    def post(self, request, *args, **kwargs):  # noqa: D102
        data_management_plan_id = request.POST.get("remove")
        if data_management_plan_id:
            data_management_plan = get_object_or_404(
                DataManagementPlan, pk=data_management_plan_id
            )
            data_management_plan.mark_deleted(self.request.user.heliportuser)
            return redirect("data_management_plan:list", **self.kwargs)
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):  # noqa: D102
        form.instance.category_str = settings.DATA_MANAGEMENT_PLAN_NAMESPACE
        project = get_object_or_404(Project, pk=self.kwargs["project"])
        form.instance.save()
        form.instance.link = self.request.POST.get("link")
        form.instance.projects.add(project)
        return super().form_valid(form)

    def get_success_url(self):  # noqa: D102
        return reverse("data_management_plan:list", kwargs=self.kwargs)


class DataManagementPlanUpdateView(HeliportObjectMixin, UpdateView):  # noqa: D101
    model = DataManagementPlan
    template_name = "data_management_plan/data_management_plan.html"
    fields = ["description"]

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the DMPs to show.
        """
        context = super().get_context_data(**kwargs)
        context["plans"] = DataManagementPlan.objects.filter(
            projects=context["project"], deleted__isnull=True
        )
        context["update"] = True
        return context

    def form_valid(self, form):  # noqa: D102
        form.instance.save()
        form.instance.link = self.request.POST.get("link")
        return super().form_valid(form)

    def get_success_url(self):  # noqa: D102
        return reverse(
            "data_management_plan:list", kwargs={"project": self.kwargs["project"]}
        )


class SearchView(HeliportLoginRequiredMixin, TemplateView):  # noqa: D101
    template_name = "data_management_plan/search.html"

    def get_context_data(self, **kwargs):
        """Add extra context for rendering template (Called by django base view).

        The extra context includes the search results.
        """
        context = super().get_context_data(**kwargs)
        user = self.request.user.heliportuser
        query = self.request.GET.get("q", "")

        data_management_plan_results = set()

        for word in query.split():
            num = int(word) if word.isdecimal() else -1

            data_management_plan_results.update(
                DataManagementPlan.objects.filter(
                    is_object_member(user)
                    & Q(projects__deleted__isnull=True, deleted__isnull=True)
                    & (
                        Q(data_management_plan_id=num)
                        | Q(description__icontains=word)
                        | Q(attributes__value__icontains=word)
                    )
                )
            )

        context["data_management_plan_results"] = data_management_plan_results
        return context


##########################################
#               REST API                 #
##########################################


class DataManagementPlanViewSet(HeliportModelViewSet):
    """Data Management Plan."""

    serializer_class = DataManagementPlanSerializer
    filter_backends = [
        rest_filters.SearchFilter,
        django_filters.rest_framework.DjangoFilterBackend,
    ]
    filterset_fields = ["data_management_plan_id"]
    search_fields = ["attributes__value"]

    def get_queryset(self):  # noqa: D102
        user = self.request.user.heliportuser
        return DataManagementPlan.objects.filter(
            is_object_member(user) & Q(deleted__isnull=True)
        ).distinct()
