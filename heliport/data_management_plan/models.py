# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Contains :class:`django.db.models.Model` classes for Django ORM.

See :ref:`django:topics/db/models:quick example` from Django documentation.
In HELIPORT the :class:`heliport.core.models.DigitalObject` can be subclassed for models
containing metadata in a project.
"""

from django.db import models

from heliport.core.models import DigitalObject, MetadataField, Vocabulary
from heliport.core.utils.normalization import url_normalizer


class DataManagementPlan(DigitalObject):  # noqa: D101
    data_management_plan_id = models.AutoField(primary_key=True)
    link = MetadataField(Vocabulary.primary_topic, url_normalizer)
