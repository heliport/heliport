# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""This module is for serialization into Datacite, RDF and JSON.

The JSON format is used for API endpoints via
`Django rest framework <https://www.django-rest-framework.org/>`_. Using this, it is
typical to put the serializer classes into a "serializers.py" file.

For the serialization into RDF, attributes are described.
See :class:`heliport.core.attribute_description.BaseAttribute` for more detail.
"""

from django.conf import settings
from rest_framework import serializers

from heliport.core.attribute_description import TypeAttribute
from heliport.core.permissions import projects_of
from heliport.core.serializers import (
    DigitalObjectDATACITESerializer,
    NamespaceField,
    datacite_serializers,
    register_digital_object_attributes,
)
from heliport.core.vocabulary import FABIO

from .models import DataManagementPlan


class DataManagementPlanSerializer(serializers.ModelSerializer):  # noqa: D101
    link = serializers.CharField()
    namespace_str = NamespaceField(settings.DATA_MANAGEMENT_PLAN_NAMESPACE)

    def __init__(self, *args, **kwargs):
        """Override querysets to only allow current user's objects."""
        super().__init__(*args, **kwargs)
        user = self.context["request"].user.heliportuser
        self.fields["projects"].child_relation.queryset = projects_of(user)

    def create(self, validated_data):  # noqa: D102
        link = validated_data.pop("link")
        instance = super().create(validated_data)
        instance.link = link
        return instance

    class Meta:  # noqa: D106
        model = DataManagementPlan
        fields = [
            "data_management_plan_id",
            "description",
            "link",
            "projects",
            "namespace_str",
        ]


@datacite_serializers.register(DataManagementPlan)
class DmpDATACITESerializer(DigitalObjectDATACITESerializer):  # noqa: D101
    def resource_type(self, dmp):  # noqa: D102
        return "DataManagementPlan", "Text"


@register_digital_object_attributes(DataManagementPlan)
def dmp_attributes():  # noqa: D103
    return [
        TypeAttribute(FABIO.DataMangementPlan),
    ]
