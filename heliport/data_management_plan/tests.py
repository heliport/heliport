# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test the behaviour of this app.

This follows the :ref:`django:topics/testing/overview:writing tests` guide in Django.
"""

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from heliport.core.models import HeliportUser, Project

from .models import DataManagementPlan


class DataManagementPlanTests(TestCase):
    """Test functionality of data_management_plan app."""

    def setUp(self):
        """Set up project and logged-in heliport users."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.client.force_login(self.auth_user)
        self.project = Project.objects.create(owner=self.user)

    def test_list(self):
        """Test listing DMPs."""
        dmp = DataManagementPlan()
        dmp.save()
        dmp.projects.add(self.project)

        response = self.client.get(
            reverse("data_management_plan:list", kwargs={"project": self.project.pk})
        )
        self.assertEqual(200, response.status_code)
        found_dmp = DataManagementPlan.objects.get(
            pk=dmp.pk, projects=response.context_data["project"]
        )
        self.assertEqual(found_dmp, dmp)

        response = self.client.get(
            reverse(
                "data_management_plan:update",
                kwargs={"project": self.project.pk, "pk": dmp.pk},
            )
        )
        self.assertEqual(200, response.status_code)
        found_dmp = DataManagementPlan.objects.get(
            pk=dmp.pk, projects=response.context_data["project"]
        )
        self.assertEqual(found_dmp, dmp)

    def test_create(self):
        """Test creating new DMP."""
        self.client.post(
            reverse("data_management_plan:list", kwargs={"project": self.project.pk}),
            data={
                "description": "data_management321",
                "link": "https://www.google.com",
            },
        )
        test_obj = DataManagementPlan.objects.filter(
            description="data_management321", projects=self.project
        ).first()
        self.assertIsNotNone(test_obj)
        self.assertEqual("https://www.google.com", test_obj.link)

    def test_update(self):
        """Test editing DMP."""
        dmp = DataManagementPlan.objects.create(description="data_management456")
        dmp.link = "http://www.google.com"
        dmp.projects.add(self.project)
        response = self.client.post(
            reverse(
                "data_management_plan:update",
                kwargs={"project": self.project.pk, "pk": dmp.pk},
            ),
            data={"description": "data_plan789", "link": "https://www.bing.com"},
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        dmp = DataManagementPlan.objects.get(pk=dmp.pk)
        self.assertIn(self.project, dmp.projects.all())
        self.assertEqual("data_plan789", dmp.description)
        self.assertEqual("https://www.bing.com", dmp.link)

    def test_delete(self):
        """Test deleting DMP."""
        data_plan = DataManagementPlan.objects.create()
        data_plan2 = DataManagementPlan.objects.create()
        data_plan.projects.add(self.project)
        data_plan2.projects.add(self.project)
        response = self.client.post(
            reverse("data_management_plan:list", kwargs={"project": self.project.pk}),
            data={"remove": data_plan2.pk},
            follow=True,
        )
        self.assertEqual(200, response.status_code)
        obj1 = DataManagementPlan.objects.filter(
            pk=data_plan.pk, deleted__isnull=True
        ).first()
        obj2 = DataManagementPlan.objects.filter(
            pk=data_plan2.pk, deleted__isnull=True
        ).first()
        self.assertIsNotNone(obj1)
        self.assertIsNone(obj2)
        self.assertIn(self.project, obj1.projects.all())

    def test_add_protocol_create(self):
        """Test that url is normalized when creating a DMP."""
        self.client.post(
            reverse("data_management_plan:list", kwargs={"project": self.project.pk}),
            data={"description": "data_management321", "link": "www.google.com"},
        )
        test_obj = DataManagementPlan.objects.filter(
            description="data_management321", projects=self.project
        ).first()
        self.assertIsNotNone(test_obj)
        self.assertEqual("https://www.google.com", test_obj.link)

    def test_add_protocol_update(self):
        """Test that url is normalized when updating a DMP."""
        dmp, is_new = DataManagementPlan.objects.get_or_create(
            description="data_management456"
        )
        dmp.link = "http://www.google.com"
        dmp.projects.add(self.project)

        response = self.client.post(
            reverse(
                "data_management_plan:update",
                kwargs={"project": self.project.pk, "pk": dmp.pk},
            ),
            data={"description": "data_plan789", "link": "www.bing.com"},
            follow=True,
        )
        dmp = DataManagementPlan.objects.get(pk=dmp.pk)
        self.assertIn(response.context_data["project"], dmp.projects.all())
        self.assertEqual("https://www.bing.com", dmp.link)


class SearchAndAPITest(TestCase):
    """Test search including via API."""

    def setUp(self):
        """Set up project and logged-in heliport users."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.project, is_new = Project.objects.get_or_create(owner=self.user)
        self.client.force_login(self.auth_user)

    def test_access_search(self):
        """Test that search URL is accessible."""
        response = self.client.get(reverse("data_management_plan:search"))
        self.assertEqual(200, response.status_code)

    def test_data_management_plan_findable(self):
        """Test that dmp is findable."""
        dmp = DataManagementPlan.objects.create(description="DMP321")
        dmp.projects.add(self.project)
        response = self.client.get(f"{reverse('data_management_plan:search')}?q=321")
        self.assertContains(response, "DMP321")

    def test_api(self):
        """Test api."""
        dmp = DataManagementPlan.objects.create(description="DMP654")
        dmp.projects.add(self.project)
        response = self.client.get("/api/data-management-plans/")
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "DMP654")

        link = "https://example.com/data-management-plans/"
        response = self.client.post(
            "/api/data-management-plans/",
            data={"link": link},
        )
        self.assertEqual(201, response.status_code)
        self.assertEqual(DataManagementPlan.objects.last().link, link)


class ProjectSerializeTest(TestCase):
    """Test metadat serialization."""

    def setUp(self):
        """Set up project, instance and logged-in heliport users."""
        self.auth_user, is_new = User.objects.get_or_create(username="testuser")
        self.user, is_new = HeliportUser.objects.get_or_create(auth_user=self.auth_user)
        self.project, is_new = Project.objects.get_or_create(owner=self.user)
        self.client.force_login(self.auth_user)
        self.dmp_description = "DMP TO SERIALIZE"
        self.dmp_link = "https://DMP_LINK"
        self.dmp = DataManagementPlan(
            description=self.dmp_description,
            persistent_id="https://hdl.handle.net/aaa",
        )
        self.dmp.save()
        self.dmp.link = self.dmp_link
        self.dmp.projects.add(self.project)

    @staticmethod
    def landing_page_url(obj):
        """Get landing page URL for given object."""
        return reverse("core:landing_page", kwargs={"pk": obj.digital_object_id})

    def test_json_ld(self):
        """Test JSON LD serialization."""
        response = self.client.get(
            f"{self.landing_page_url(self.project)}?format=jsonld"
        )
        self.assertContains(response, self.dmp_description)
        response = self.client.get(f"{self.landing_page_url(self.dmp)}?format=jsonld")
        dmp = response.json()
        self.assertIn("@id", dmp)
        self.assertEqual(self.dmp_description, dmp["dcterms:description"])
        self.assertContains(response, self.dmp_link)
        self.assertContains(response, "https://hdl.handle.net/aaa")

    def test_datacite(self):
        """Test datacite serialization."""
        response = self.client.get(
            f"{self.landing_page_url(self.dmp)}?format=datacite_json"
        )
        metadata = response.json()
        self.assertEqual(
            [
                {
                    "description": self.dmp_description,
                    "descriptionType": "Abstract",
                    "lang": "en",
                }
            ],
            metadata["descriptions"],
        )
        self.assertEqual(
            {"resourceType": "DataManagementPlan", "resourceTypeGeneral": "Text"},
            metadata["types"],
        )

    def test_rest_api(self):
        """Test REST API with its JSON serialization."""
        response = self.client.get("/api/data-management-plans/")
        dmp = response.json()["results"][0]
        self.assertEqual(
            {
                "data_management_plan_id": 1,
                "description": self.dmp_description,
                "link": self.dmp_link,
                "projects": [self.project.pk],
            },
            dmp,
        )
