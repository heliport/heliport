# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Module with special name "interface" hooks into HELIPORT.

Some functions and :class:`heliport.core.app_interaction.Module` subclasses are detected
by HELIPORT and control how HELIPORT uses this app.

Note that this module must be imported in __init__.py of the django app.
"""

from django.urls import reverse

from heliport.core.app_interaction import Module


class DataManagementPlanModule(Module):  # noqa: D101
    name = "Data Management Plan"
    module_id = "data_management_plan"

    def get_url(self, project):
        """Return the URL for the entry point of this module."""
        return reverse("data_management_plan:list", kwargs={"project": project.pk})

    def is_configured(self, project):
        """Return whether the module is configured, i.e. to show it in the graph."""
        from .models import DataManagementPlan

        return DataManagementPlan.objects.filter(
            projects=project, deleted__isnull=True
        ).exists()


def get_search_url():
    """Return the search URL for this app.

    This URL is used to implement the global HELIPORT string search.
    """
    return reverse("data_management_plan:search")
