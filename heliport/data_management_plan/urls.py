# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Map django views to urls.

See this :ref:`django:topics/http/urls:example` including explanation from the Django
documentation.
"""

from django.urls import path
from rest_framework import routers

from .views import (
    DataManagementPlanUpdateView,
    DataManagementPlanView,
    DataManagementPlanViewSet,
    SearchView,
)

app_name = "data_management_plan"
urlpatterns = [
    path(
        "project/<int:project>/data-management-plan/list/",
        DataManagementPlanView.as_view(),
        name="list",
    ),
    path(
        "project/<int:project>/data-management-plan/<int:pk>/update/",
        DataManagementPlanUpdateView.as_view(),
        name="update",
    ),
    path("data-management-plan/search/", SearchView.as_view(), name="search"),
]

# REST API
router = routers.DefaultRouter()
router.register(
    r"data-management-plans",
    DataManagementPlanViewSet,
    basename="data_management_plans",
)
