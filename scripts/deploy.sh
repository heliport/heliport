#!/bin/bash

# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -eo pipefail

export DJANGO_SETTINGS_MODULE=heliport_config.settings

helptext () {
    echo "usage: sh deploy.sh  <deploy|restart|status>"
    echo -e "\nCommands:"
    echo "      deploy: teardown, install, restarting services, healthcheck"
    echo "      restart: restarting all services only"
    echo "      status: healthcheck only"
    exit 1
}

command -v poetry > /dev/null || { echo "poetry not installed in \$PATH"; exit 1; }

if [ $# -ne 1 ]
then
    helptext
fi

COMMAND=$1

if [ "$COMMAND" = "deploy" ]
then
    echo -e "\n---------------------------"
    echo "- Deploying on $(hostname) -"
    echo "---------------------------"
elif [ "$COMMAND" = "restart" ]
then
    echo -e "\n-----------------------"
    echo "- Restarting services -"
    echo "-----------------------"
elif [ "$COMMAND" = "status" ]
then
    echo -e "\n---------------------"
    echo "- Checking services -"
    echo "---------------------"
else
    helptext
fi

mkdir -p /run/uwsgi /var/log/heliport /var/lib/heliport
cd /opt/heliport || { echo "Couldn't cd into /opt/heliport"; exit 1; }

if [ "$COMMAND" = "deploy" ]
then
    echo -e "\n--- Copying service files and reloading ---"
    cp /opt/heliport/config/systemd/system/heliport* /etc/systemd/system
    systemctl daemon-reload

    echo -e "\n--- Copying env file ---"
    cp ~/HELIPORT.env /opt/heliport/heliport_config/.env

    echo -e "\n--- Evaluating Poetry environment ---"
    poetry env info

    echo -e "\n--- Installing HELIPORT package ---"
    echo "Selected extras: $HELIPORT_EXTRAS"
    poetry sync ${HELIPORT_EXTRAS:+--extras "$HELIPORT_EXTRAS"}
    yarnpkg install
fi


if [ "$COMMAND" = "deploy" ] || [ "$COMMAND" = "restart" ]
then
    echo -e "\n--- Stopping and disabling services ---"

    for service in heliport heliport-worker heliport-beat
    do
        systemctl --quiet disable --now "$service" || { echo "Error: Tearing down services!"; exit 1; }
    done

    echo "All HELIPORT services offline"
fi

if [ "$COMMAND" = "deploy" ]
then
    echo -e "\n--- Collecting static and vendor files ---"
    mkdir -p /opt/heliport/vendor
    poetry run heliport-cli download_vendor_files
    yarnpkg build
    poetry run heliport-cli collectstatic --noinput
    echo -e "\n--- Migrating database ---"
    poetry run heliport-cli migrate
fi

if [ "$COMMAND" = "deploy" ] || [ "$COMMAND" = "restart" ]
then
    echo -e "\n--- Enabling and starting services ---"

    for service in heliport heliport-worker heliport-beat
    do
        systemctl --quiet enable --now "$service" || { echo "Error: Not all services online!"; exit 1; }
    done

    echo -e "\nAll HELIPORT services online!"

    echo -e "\n--- System status ---\n"
fi

echo -e "HELIPORT Base App: $(poetry run heliport-cli check)\n"

if curl -I -k -f --unix-socket /run/uwsgi/heliport.sock http://127.0.0.1/login &> /dev/null
then
    echo "HELIPORT website online!"
else
    echo "Error: HELIPORT offline!"
    exit 1
fi

echo

echo "Celery Worker: $(poetry run celery -A 'heliport_config' status)"
sleep 5
echo "Celery Beat: $(poetry run celery -A 'heliport_config' inspect registered)"

echo -e "\n--- DONE! ---"
