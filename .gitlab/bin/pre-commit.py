#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""A pre-commit hook script for HELIPORT developers."""

import functools
import subprocess
import sys

# GIT_REPO_ROOT will be replaced with the actual root directory of the repo
CHECKS = [
    ("ruff", "check", "--silent", "GIT_REPO_ROOT"),
    ("ruff", "format", "--silent", "--check", "GIT_REPO_ROOT"),
    ("djlint", "--quiet", "--check", "--lint", "GIT_REPO_ROOT"),
    ("reuse", "lint"),
    ("yarnpkg", "run", "prettier", "--loglevel=silent", "--check", "GIT_REPO_ROOT"),
    ("GIT_REPO_ROOT/.gitlab/bin/version-check.py",),
]


@functools.lru_cache
def git_repo_root():
    """Return the root of the git repository.

    It is determined by running the git command given as ``cmd``.
    If something goes wrong, an Exception is raised.
    """
    cmd = ("git", "rev-parse", "--show-toplevel")
    result = subprocess.run(cmd, capture_output=True, check=True, text=True)
    return result.stdout.strip()


def run_check(check):
    """Run a check.

    The ``check`` command must be given as an iterable.
    This function returns a tuple of two booleans.
    The first boolean states whether an error occured during the execution.
    The second boolean is ``True`` in case the command to be executed was not found.
    """
    command_not_found = False
    error_occured = False

    check = tuple(s.replace("GIT_REPO_ROOT", git_repo_root()) for s in check)
    try:
        subprocess.run(check, capture_output=True, check=True)
    except FileNotFoundError:
        command_not_found = True
    except subprocess.SubprocessError:
        error_occured = True

    return error_occured, command_not_found


def main():
    """The main function of the script."""  # noqa: D401
    print("Running pre-commit hook ...", end="\r")

    command_not_found = False
    error_occured = False

    for check in CHECKS:
        error_occured, command_not_found = run_check(check)
        if not error_occured and not command_not_found:
            continue

        if error_occured:
            print("Failure during:", " ".join(check))
        elif command_not_found:
            print("Command not found:", check[0])

        sys.exit(1)


if __name__ == "__main__":
    main()
