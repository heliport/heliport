#!/usr/bin/env python

# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Ensure equal version specifiers.

This script gets the HELIPORT version specified in various files and makes sure all of
the versions are the same.
"""

import json
import re
from pprint import pprint
from sys import exit, stderr

import tomli
import yaml


def main():
    """The main function of the script."""  # noqa: D401
    version_specifiers = {}

    with open("README.md", "r") as readme_file:
        readme = readme_file.read()
    match = re.search(r"https://img.shields.io/badge/version-(.*)-blue", readme)
    readme_version, *_ = match.groups()
    version_specifiers["README.md"] = readme_version

    with open("pyproject.toml", "rb") as pyproject_file:
        pyproject = tomli.load(pyproject_file)
    pyproject_version = pyproject["tool"]["poetry"]["version"]
    version_specifiers["pyproject.toml"] = pyproject_version

    with open("CITATION.cff", "r") as citation_file:
        citation = yaml.load(citation_file, Loader=yaml.Loader)
    citation_version = citation["version"]
    version_specifiers["CITATION.cff"] = citation_version

    with open("package.json", "r") as package_file:
        package = json.load(package_file)
    package_version = package["version"]
    version_specifiers["package.json"] = package_version

    versions = list(version_specifiers.values())
    if not all(version == versions[0] for version in versions):
        print("Version mismatch:", file=stderr)
        pprint(version_specifiers, stream=stderr)
        exit(1)


if __name__ == "__main__":
    main()
