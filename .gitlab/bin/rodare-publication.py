#!/usr/bin/env python

# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Publish HELIPORT on Rodare.

This script publishes the files passed to it on Rodare. Metadata is taken from the
previous HELIPORT publications and updated with data from the CITATION.cff file.
"""

from os import environ
from pathlib import Path
from sys import argv, exit, stderr

import requests
import yaml


class Rodare:
    """Class wrapping Rodare API functionality."""

    def __init__(self, rodare_token, rodare_id, citation_data):  # noqa: D107
        self.rodare_id = rodare_id
        self.initial_url = f"https://rodare.hzdr.de/api/deposit/depositions/{rodare_id}"

        self.session = requests.Session()
        self.session.headers = {
            "Authorization": f"Bearer {rodare_token}",
            "Accept": "application/json",
        }

        # Get initial information
        response = self.session.get(self.initial_url)
        self.handle_response(
            response, 200, f"Could not retrieve deposition resource {self.rodare_id}"
        )

        links = response.json()["links"]
        self.newversion_url = links["newversion"]
        self.latest_url = links["latest"]

        # Get metadata from the latest publication
        response = self.session.get(self.latest_url)
        self.handle_response(
            response, 200, "Could not retrieve latest publication information"
        )

        self.metadata = response.json()["metadata"]

        # Filter out "isVersionOf" relation which points to Concept ID
        self.metadata["related_identifiers"] = list(
            filter(
                lambda id: id["relation"] != "isVersionOf",
                self.metadata["related_identifiers"],
            )
        )

        # Remove some metadata that we won't need
        self.metadata.pop("doi")
        self.metadata.pop("prereserve_doi")
        self.metadata.pop("publication_date")

        # Update with cff metadata
        self.metadata["title"] = citation_data["title"]
        self.metadata["license"] = citation_data["license"]
        self.metadata["version"] = citation_data["version"]
        self.metadata["creators"] = [
            {
                "affiliation": author["affiliation"],
                "name": f"{author['family-names']}, {author['given-names']}",
                "orcid": author["orcid"].removeprefix("https://orcid.org/"),
            }
            for author in citation_data["authors"]
        ]

        self.latest_draft_url = None
        self.files_url = None
        self.bucket_url = None
        self.publish_url = None
        self.self_url = None

    @staticmethod
    def handle_response(response, expected_status_code, error_message):
        """Helper function to handle HTTP responses."""  # noqa: D401
        if response.status_code == expected_status_code:
            return

        # Print status code and user-supplied error message
        print(f"Error {response.status_code}:", error_message, file=stderr)

        try:
            # See if the server replied with a JSON object
            content = response.json()
            # If the response has a message field, print it
            if (message := content.get("message")) is not None:
                print("Message:", message, file=stderr)
            # If the response has an errors field, print them all (e.g. multiple
            # errors when metadata validation fails)
            if errors := content.get("errors"):
                print("Errors:", file=stderr)
                for error in errors:
                    print("-", error, file=stderr)
        except requests.exceptions.JSONDecodeError:
            # Server did not reply with valid JSON, so we print the response as text
            print("Server response:", response.text, sep="\n", file=stderr)

        exit(1)

    def new_version(self):
        """Create a draft for a new publication."""
        response = self.session.post(self.newversion_url)
        self.handle_response(response, 201, "Could not create new version")
        self.latest_draft_url = response.json()["links"]["latest_draft"]

        response = self.session.get(self.latest_draft_url)
        self.handle_response(response, 200, "Could not retrieve latest draft metadata")

        links = response.json()["links"]
        self.files_url = links["files"]
        self.bucket_url = links["bucket"]
        self.publish_url = links["publish"]
        self.self_url = links["self"]

    def remove_files(self):
        """Remove all existing files from the draft."""
        response = self.session.get(self.files_url)
        self.handle_response(
            response, 200, "Could not retrieve information about existing files"
        )

        for file in response.json():
            response = self.session.delete(file["links"]["download"])
            self.handle_response(response, 204, f"Could not delete {file['filename']}")

    def upload_files(self, files):
        """Upload a list of new files to the draft."""
        for file in files:
            with open(file, "rb") as file_data:
                response = self.session.put(f"{self.bucket_url}/{file}", data=file_data)
            self.handle_response(response, 200, f"Could not upload file {file}")

    def update_metadata(self):
        """Update the metadata on the draft."""
        response = self.session.put(self.self_url, json={"metadata": self.metadata})
        self.handle_response(response, 200, "Could not update deposit metadata")

    def publish(self):
        """Publish the draft."""
        response = self.session.post(self.publish_url)
        self.handle_response(response, 202, "Could not publish record")


def main():
    """The main function of the script."""  # noqa: D401
    rodare_id = environ.get("RODARE_ID")
    rodare_token = environ.get("RODARE_TOKEN")
    assert rodare_id is not None
    assert rodare_token is not None

    files = [Path(arg) for arg in argv[1:]]
    assert len(files) > 0
    assert all(file.exists() for file in files)

    citation_file = Path("CITATION.cff")
    assert citation_file.exists()

    with open(citation_file, "r") as cff:
        citation_data = yaml.load(cff, Loader=yaml.Loader)

    rodare = Rodare(rodare_token, rodare_id, citation_data)
    rodare.new_version()
    rodare.remove_files()
    rodare.upload_files(files)
    rodare.update_metadata()
    rodare.publish()


if __name__ == "__main__":
    main()
