<!--
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: GPL-3.0-or-later
-->

[![HELIPORT HELmholtz ScIentific Project WORkflow PlaTform](heliport/core/static/core/img/heliport_wide.svg)](https://heliport.hzdr.de/)

[![Version](https://img.shields.io/badge/version-0.6.0-blue)](https://codebase.helmholtz.cloud/heliport/heliport/-/tags)
[![Lifecycle](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
[![License](https://img.shields.io/badge/license-GPL--3.0--or--later-blue)](LICENSE)
[![DOI](https://rodare.hzdr.de/badge/DOI/10.14278/rodare.946.svg)](https://doi.org/10.14278/rodare.946)

# HELIPORT

You can find detailed information about the HELIPORT project on
[our website](https://heliport.hzdr.de/)!
For a visual overview take a look at
[the project poster](https://heliport.hzdr.de/docs/resources/#heliport_poster_2022).
The [fully-featured prototype installation](https://heliport.hzdr.de) at HZDR is
accessible to all Helmholtz members via Helmholtz ID.
Interested parties from other institutions may create an account on
[Helmholtz ID](https://login.helmholtz.de) and request access to the HZDR instance
[via email](mailto:heliport@hzdr.de).

Documentation for users, administrators and developers can be found on the
[HELIPORT website](https://heliport.hzdr.de/documentation/).

## Features

HELIPORT accompanies the whole lifecycle of a research project, providing integration
of various systems and resources that researchers interact with on a daily basis, and
collecting information for a fully comprehensive metadata description of the project.

Some highlights include:

- Digital objects with globally unique persistent identifiers
- Retrieval of proposal metadata
- Computational workflow management with UNICORE and CWL
- Automation of recurring tasks
- Access to data sources
- Publication of results

Moreover, HELIPORT provides a REST API to allow programmatic access to all important
resources.

## Development

[![Python Version](https://img.shields.io/badge/python-%5E3.9-blue)](https://www.python.org/)
[![Pipeline Status](https://codebase.helmholtz.cloud/heliport/heliport/badges/master/pipeline.svg)](https://codebase.helmholtz.cloud/heliport/heliport/-/pipelines)
[![Coverage Report](https://codebase.helmholtz.cloud/heliport/heliport/badges/master/coverage.svg)](https://codebase.helmholtz.cloud/heliport/heliport/-/pipelines)

HELIPORT is a web application built in Python using the Django framework.
Functionality is encapsulated in modules to allow for easy adaption and extension.
We make heavy use of CI/CD for static code analysis and testing.
This way, we achieve a consistent code style and reusability throughout the whole
codebase.
The documentation is built and deployed automatically to always reflect the current
state of the code.

## System Architecture

The figure below shows the interaction of some of HELIPORT's components with other
services within the research data management landscape at HZDR.

![System architecture](.gitlab/heliport_architecture.png)

## Attribution

The user interface design of the [workflow visualization](heliport/cwl_execution/static/cwl_execution/visualization) was
taken from the [Rabix cwl-svg library](https://github.com/rabix/cwl-svg) and re-implemented for HELIPORT.
The icons were taken from the same library and are licensed under the Apache license v2.0.
