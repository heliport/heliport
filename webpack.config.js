// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
//
// SPDX-License-Identifier: GPL-3.0-or-later

const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

var config = {
  module: {
    rules: [
      {
        test: require.resolve("jquery"),
        loader: "expose-loader",
        options: {
          exposes: ["$", "jQuery"],
        },
      },
      {
        test: require.resolve("htmx.org"),
        loader: "expose-loader",
        options: {
          exposes: {
            globalName: ["window", "htmx"],
            override: true,
          },
        },
      },
      {
        test: require.resolve("hyperscript.org"),
        loader: "expose-loader",
        options: {
          exposes: {
            globalName: ["window", "_hyperscript"],
            override: true,
          },
        },
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
    ],
  },
  plugins: [new MiniCssExtractPlugin()],
  devServer: {
    devMiddleware: {
      writeToDisk: true,
    },
  },
};

var mainConfig = Object.assign({}, config, {
  entry: {
    base: [
      path.resolve(
        __dirname,
        "heliport",
        "core",
        "static",
        "core",
        "js",
        "base.js",
      ),
      path.resolve(
        __dirname,
        "heliport",
        "core",
        "static",
        "core",
        "css",
        "base.css",
      ),
    ],
  },
  output: {
    path: path.resolve(__dirname, "dist"),
  },
});

/*
 * Here, I tried webpacking some of the existing JavaScript code by building a
 * library that can be used from the global namespace inside <script></script>
 * tags.
 */
var tableConfig = Object.assign({}, config, {
  entry: {
    tables: [
      path.resolve(
        __dirname,
        "heliport",
        "tables",
        "static",
        "tables",
        "expressions.js",
      ),
      path.resolve(
        __dirname,
        "heliport",
        "tables",
        "static",
        "tables",
        "table.js",
      ),
    ],
  },
  output: {
    path: path.resolve(__dirname, "dist", "lib"),
    libraryTarget: "umd",
    library: "Tables",
    umdNamedDefine: true,
  },
});

module.exports = [mainConfig, tableConfig];
