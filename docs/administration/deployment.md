<!--
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Deployment

To deploy HELIPORT on your machine, clone the repo to `/opt` and create an env file by
copying the provided example.
The [deployment script](../../scripts/deploy.sh) expects the env file to be located at
`/root/HELIPORT.env`.

```bash
cp heliport_config/.env.dist /root/HELIPORT.env
```

The env file has to be manually edited to provide secret keys and make the desired
changes.
The comments in the file will guide you in the process.

Then run the deployment script, optionally specifying all desired package extras in the
`HELIPORT_EXTRAS` variable:

```bash
export HELIPORT_EXTRAS="pgsql ldap"
./scripts/deploy.sh deploy
```

This will copy the env file as well as service files into the right place, create
directories which are required, and start all necessary systemd units.

To access the virtual environment created in the deployment script, run
`eval $(poetry env activate)` while inside the heliport directory.
