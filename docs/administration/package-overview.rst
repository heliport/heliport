.. SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Package Overview
================

HELIPORT provides various optional features that can be installed as package extras.
Some of them require additional development packages.
Please make sure to install all development packages for the features you would like to
use.
An overview of the features, their according package extra and the name of the required
development package names on Ubuntu can be found in the table below:

+---------------------------+---------------+----------------------------+
| Feature                   | Package extra | Ubuntu development package |
+===========================+===============+============================+
| PostgreSQL support        | pgsql         | libpq-dev                  |
+---------------------------+---------------+----------------------------+
| MariaDB/MySQL support     | mysql         | libmariadb-dev             |
+---------------------------+---------------+----------------------------+
| LDAP support              | ldap          | libldap2-dev, libsasl2-dev |
+---------------------------+---------------+----------------------------+
| Redis support             | redis         | \-                         |
+---------------------------+---------------+----------------------------+
| UNICORE support           | unicore       | \-                         |
+---------------------------+---------------+----------------------------+

For asynchronous task execution, HELIPORT uses Celery which can be run with different
brokers and result backends.
HELIPORT supports RabbitMQ by default, and Redis if the ``redis`` extra is installed.
