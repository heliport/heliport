.. SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. _prerequisites:

Prerequisites
=============

.. note::

   All steps in this guide can be applied to and were tested on a fresh minimal
   installation of Ubuntu 22.04.
   Other operating systems may require slightly different steps to set up the
   prerequisites correctly.

Poetry
------

HELIPORT is packaged using `Poetry <https://python-poetry.org/>`_.
We require a Poetry version of 2.0 or newer.
If your system does not provide such a new Poetry version yet, you can follow the
install instructions in the `Poetry documentation <https://python-poetry.org/docs/>`_.

Celery Broker
-------------

For asynchronous task execution, HELIPORT requires the task queue Celery.
Celery can be run with different brokers and result backends.
Brokers are used to pass messages around and coordinate task execution, the backends
store results of the executed tasks.
More information on this topic can be found in Celery's :ref:`"First steps with Django"
<celery:django-first-steps>` and :ref:`"Backends and Brokers" <celery:brokers>` docs.
You can choose between RabbitMQ and Redis as your Celery broker.

On Ubuntu, Redis can be installed and started with the following commands:

.. code-block:: bash

   sudo apt-get update && sudo apt-get install redis
   sudo systemctl start redis.service

For RabbitMQ, detailed instructions are provided in the :ref:`"First steps with Celery -
Choosing a Broker" <celery:celerytut-broker>` guide.
It also suggests how to set up either option using Docker.

Yarn
----

To install JavaScript and CSS packages for the HELIPORT frontend, you will need the
`Yarn package manager <https://yarnpkg.com/>`_.

.. note::

   On some systems the command to invoke Yarn is called ``yarn``, on others it might be
   called ``yarnpkg``.

Web Server
----------

To serve the HELIPORT website, you will need a web server.
Different options are available.
For nginx, an :download:`example config <../../config/nginx/heliport.conf.sample>` is
available.

.. warning::

   HELIPORT deals with usernames, passwords, and other login credentials.
   Thus, it is highly discouraged to run a HELIPORT instance behind a web server
   without SSL/TLS encryption in a production environment.
   For local usage, creating a self-signed certificate is sufficient.
   Various guides on this topic can be found online.
   If you need a certificate signed by a trusted authority, non-profit organizations
   like `Let's Encrypt <https://letsencrypt.org/>`_ offer free certificates.
