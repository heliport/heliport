.. SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Admin Commands
==============

Some of the HELIPORT apps provide admin commands to trigger certain tasks.
A list of these commands (as well as all other Django app commands) can be found
by running the following command inside the Poetry environment:

.. code-block:: shell

   heliport-cli help

.. note::

   HELIPORT's admin commands only add tasks to the task queue. Thus, they
   require the Celery broker and result backend to be configured correctly. For
   more information, please refer to the :ref:`prerequisites`. In order for the
   queued tasks to be executed, a Celery worker needs to be running.
