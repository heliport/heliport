.. SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. image:: ../heliport/core/static/core/img/heliport_wide.svg
   :alt: HELIPORT header

Welcome to the HELIPORT documentation!
======================================

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: User Documentation

   usage/resolver
   usage/unicore

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Administrator Documentation

   administration/prerequisites
   administration/package-overview
   administration/deployment
   administration/admin-commands

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Developer Documentation

   Code Documentation (apidoc)<_apidoc/modules>
   modindex
   development/getting-started
   development/setup
   development/conventions
   development/integrate-a-new-module
   development/special-digital-objects
   development/metadata-schema
   development/persistent-identifiers

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: General

   genindex
   search
   changelog

The documentation is split into three sections:
In the user documentation you will find guides for HELIPORT users.
The administrator documentation contains information about setup and configuration of
HELIPORT instances.
The developer documentation consists of documentation about the HELIPORT source code
itself and is largely generated from the docstrings in the code.

General information about the HELIPORT project can be found `on our website
<https://heliport.hzdr.de>`_.
If you are missing any information related to the topics covered on this site, do not
hesitate to reach out to us and ask questions.
You can `create an issue on GitLab
<https://codebase.helmholtz.cloud/heliport/heliport/-/issues/new>`_, `contact our team
via email <mailto:heliport@hzdr.de>`_, or `subscribe and write to our mailing list
<https://www.listserv.dfn.de/sympa/info/heliport>`_.
