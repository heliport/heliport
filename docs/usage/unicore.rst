.. SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
..
.. SPDX-License-Identifier: GPL-3.0-or-later

UNICORE Interaction
===================

To be able to access UNICORE resources from HELIPORT, you first need to add a login info
with your UNICORE credentials.
To do this, click the user settings drop-down menu in the top right and go to
"Remote Server Logins".
Now, in the "Add a Login" section, add a login of type "username and password".
The "Name of this login info" field has to be set to "UNICORE".
In the "Username" and "Password" fields, enter your UNICORE credentials.

After entering your credentials, you can go to your project page, and under Systems →
UNICORE, access your resources.

If you want HELIPORT to automatically associate your UNICORE jobs with the correct
HELIPORT project, the job descriptions must contain a tag ``heliport-project-X``, where
``X`` is the ID of your project.
If you want UNICORE to notify HELIPORT whenever the status of your job changes, you can
set the ``Notification`` field of the job description to the ``/unicore/notify/``
endpoint of the HELIPORT instance.
You can find the full URL on the UNICORE page under Systems.

Here is an example job description.
Please make sure to edit the URL and ``Tags`` section according to your project.

.. code-block:: json
   :caption: hello-world.u

   {
     "Name": "A simple hello world program",
     "Notification": "https://heliport.example.com/unicore/notify/",
     "Executable": "echo",
     "Arguments": ["Hello", "World"],
     "Tags": ["heliport-project-91", "test", "tutorial"]
   }
