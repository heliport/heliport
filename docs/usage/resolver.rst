.. SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
..
.. SPDX-License-Identifier: GPL-3.0-or-later

PID Resolver
============

HELIPORT offers a URL which can be used to resolve persistent identifiers.
It can be found at ``/object/ID``, where ``ID`` is the identifier you want to resolve.
If the identifier exists in your HELIPORT instance, you are redirected to the landing
page for its associated digital object.

.. note::

   **Example:** To resolve the handle ``20.500.12865/Project.123`` on
   ``heliport.example.com``, request
   ``https://heliport.example.com/object/20.500.12865/Project.123`` (with no trailing
   slash).
