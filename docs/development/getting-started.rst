.. SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
..
.. SPDX-License-Identifier: GPL-3.0-or-later

..
   TODO: Maybe add a chapter (or new page) about app_interaction module and what it
   does.

   If it's a new page, mention it here and add a link.

..
   TODO: Add chapter (or new page) on how to contribute.

   If it's a new page, mention it here and add a link.

   Some of this information can be found in development/setup but that's not a good
   place for it.

..
   TODO: Remove Work in Progress note.

.. note::

   This page is work in progress.

Getting Started
===============

HELIPORT is a web application built in `Python <https://www.python.org/>`_
using the `Django <https://www.djangoproject.com/>`_ web framework.

..
   TODO: Mention other technologies and what they are used for!

   - Celery
   - django-rest-framework
   - ...

Modules
-------

HELIPORT is comprised of various modules (Django apps) that encapsulate certain
features.
The core functionality is implemented in :mod:`heliport.core`; other apps can import
and use it.

..
   TODO: Add more info here.

   - Which functionality does heliport.core provide?
   - ...

..
   TODO: Add a diagram of relationships between the apps.

   Something like this: https://mattermost.hzdr.de/fwc/pl/wqu36biwzfnbtr4aekzqwzbmdh
   Don't forget to add an arrow from heliport.cwl_execution to heliport.data_source!

   Maybe use graphviz for this:
   https://www.sphinx-doc.org/en/master/usage/extensions/graphviz.html

Management Commands
-------------------

Django projects contain an auto-generated script ``manage.py`` which can be used to run
management commands.
For convenience, HELIPORT installs this script as ``heliport-cli``.
The command ``heliport-cli`` allows you to run management tasks from HELIPORT apps as
well as tasks from all other Django apps that were used to build HELIPORT.

To see a list of all available management scripts, run this command in the installed
environment:

.. code-block:: shell

   heliport-cli help
