.. SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. _pids:

Persistent Identifiers (PIDs)
=============================

Digital objects in HELIPORT can be identified by globally unique persistent identifiers
(PIDs).

.. note::

   In the past, persistent identifiers were stored in digital objects's
   ``persistent_id`` and ``generated_persistent_id`` attributes.
   These fields are deprecated.
   New code should store and retrieve persistent identifiers in/from
   :class:`heliport.core.models.DigitalObjectIdentifier` and use
   :meth:`heliport.core.models.DigitalObject.preferred_identifier` to get a generic
   preferred identifier for the object.

PID Generation
--------------

HELIPORT can generate globally unique persistent identifiers (PIDs) for all digital
objects.
Names are chosen based on the :ref:`namespace <namespaces>` associated with the digital
object and its primary key.
:meth:`heliport.core.models.DigitalObject.suffix_for_pid_generation` provides a string
that can be used to generate PIDs.

Currently, the only available implementation is for integration with `Handle.Net servers
<https://handle.net/>`_ as a PID provider.
This implementation can be found in :mod:`heliport.pid_registration`.

External PIDs
-------------

HELIPORT can also store external PIDs as identifiers for digital objects.
These may be stored in the :class:`heliport.core.models.DigitalObjectIdentifier` model
with an appropriate string as their ``scheme``.
