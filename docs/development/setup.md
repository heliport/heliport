<!--
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Development Setup

Make sure you have Poetry and a Celery Broker installed as described in
[Prerequisites](../administration/prerequisites.rst).

## Create the Environment

Create the python environment with the required packages.
After the `--extras` flag, provide a quoted, space-separated list with all the package
extras you would like to install.
An overview of all available extras can be found in the [Package Overview](../administration/package-overview.rst).
In this example we're going to install the dependencies for PostgreSQL and LDAP support
as well as the Poetry dependency groups containing development tools and tools for the
generation of docs.

```bash
poetry install --extras "pgsql ldap" --with dev,docs
```

Then open a shell with this environment loaded by executing

```bash
eval $(poetry env activate)
```

:::{note}
All of the following commands need to be executed inside the Poetry environment.
:::

Now create a `.env` file by copying the provided sample.

```bash
cd heliport_config
cp -i .env.dist .env
```

Finally, edit the `.env` file to provide your own settings.
Some settings are required, others are commented out and can be left unchanged.
The comments will guide you in the process.
For a development setup of HELIPORT, `DEVELOPMENT` should be uncommented and set to
`True`.

## Run the Tests

To run the tests, execute pytest in the Poetry environment:

```bash
pytest -v
```

## Run the Development Server

To run the server, first do a database migration and then execute the `runserver`
command.

:::{note}
`migrate` is only required when starting the app for the first time or if the database
schema changed since the last start.
:::

```bash
heliport-cli migrate
heliport-cli runserver
```

## Start the Celery Worker

Start the Celery worker:

```bash
celery -A heliport_config worker
```

Or run it in the background:

```bash
celery -A heliport_config multi start "celery@$HOSTNAME" --logfile=celery.log --pidfile=celery.pid
```

## Build the JavaScript Code Bundles

To build the JavaScript code, the necessary libraries need to be installed.
This can be done by running `yarnpkg install`.
Then, running `yarnpkg build` will create webpacks of the JavaScript and CSS code.
To start the webpack development server which keeps the webpacks up to date during
development, run `yarnpkg dev`.
If you don't want to change any Javascript code, running the build command once will
suffice.

:::{note}
The `yarnpkg` command refers to the Yarn package manager.
On some systems it might simply be called `yarn`.
:::

## Additional Tips

### Admin Interface

If you want an admin for the `/admin/` interface, you can create one:

```bash
heliport-cli createsuperuser
```

## Contributing

If you want to contribute code, please make sure to format it by running the following
commands (`ruff` and `djlint` can be installed via `pipx`):

```bash
# fix lint violations with Ruff (https://docs.astral.sh/ruff/)
ruff check --fix .
# format Python code
ruff format .
# format Django templates with djlint (https://djlint.com/)
djlint --lint --reformat .
# format css, js, yaml, ... with Prettier (https://prettier.io/)
yarnpkg format
```

Also, please make sure to add a license header to all new files.
`reuse` can help you with this (`reuse` can be installed via `pipx`):

```bash
reuse annotate --license GPL-3.0-or-later --copyright "Your institution" path/to/file
```

You can check if the code is up to the [REUSE](https://reuse.software/) spec like this:

```bash
reuse lint
```

For automated checks during `git commit` , the pre-commit hook provided in this
repository can be used.
You can activate it by copying or linking it into the `.git/hooks` directory:

```bash
ln -rs .gitlab/bin/pre-commit.py .git/hooks/pre-commit
```

The checks can be skipped using the `--no-verify` option for `git commit`.
