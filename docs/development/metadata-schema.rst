.. SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
..
.. SPDX-License-Identifier: GPL-3.0-or-later

..
   TODO: This page needs some work!

   - Spell things out in text
   - Where is the relevant code?
   - What are the changes made from DataCite?
   - Explain namespaces in more detail
   - ...
   - Remove Work in Progress note

.. note::

   This page is work in progress.

Metadata Schema
===============

- Based on `DataCite 4.3 <http://schema.datacite.org/meta/kernel-4.3/>`_ and can be
  visualized using ``xsdvi``
- Additional basic vocabularies like `RDF schema <https://www.w3.org/TR/rdf-schema/>`_,
  `OWL time <https://www.w3.org/TR/owl-time/>`_

.. _namespaces:

Namespaces
----------

Digital objects in HELIPORT are distinguished by namespaces.

.. note::

   In some places, "namespaces" are referred to as "categories".
   This is for legacy reasons only.
   We switched away from this term as to not suggest that type information (which could
   change for any given object) is part of the identifier.
