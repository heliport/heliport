# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Configuration file for the Sphinx documentation builder.

For the full list of built-in configuration values, see the documentation:
https://www.sphinx-doc.org/en/master/usage/configuration.html
"""

import os
import sys

import django

sys.path.insert(0, os.path.abspath("../heliport"))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "heliport_config.settings")
django.setup()

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "HELIPORT"
author = (
    "Martin Voigt, Robert Ufer, Wilhelm Schacht, Oliver Knodel, David Pape, "
    "Mani Lokamani, Stefan Müller, Thomas Gruber, Jeffrey Kelling"
)
copyright = "2025, Helmholtz-Zentrum Dresden - Rossendorf"
version = "0.6"
release = ""

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "myst_parser",
]

templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

source_suffix = {
    ".rst": "restructuredtext",
    ".md": "markdown",
}

# -- Configuration for extensions --------------------------------------------

# https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html#configuration
autodoc_inherit_docstrings = False

# https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html#configuration
intersphinx_mapping = {
    "celery": ("https://docs.celeryq.dev/en/v5.2.0/", None),
    "django": (
        "https://docs.djangoproject.com/en/4.2/",
        "https://docs.djangoproject.com/en/4.2/_objects/",
    ),
    "rdflib": ("https://rdflib.readthedocs.io/en/6.2.0/", None),
}

# https://myst-parser.readthedocs.io/en/latest/configuration.html#configuration
myst_enable_extensions = ["colon_fence"]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]
html_favicon = "../heliport/core/static/core/img/favicon.ico"
